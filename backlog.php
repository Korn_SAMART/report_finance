<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ข้อมูลงานค้าง</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="./style/style.css">
    <link rel="stylesheet" href="./dist/css/dash.css">
    <link rel="stylesheet" href="./plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="./plugins/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./dist/DataTables/datatables.min.css" />
    <link rel="stylesheet" href="./dist/DataTables/jquery.datatables.min.css" />

    <script src="./plugins/jquery/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./style/login.css">


    <link rel="shortcut icon" href="#" />

    <style>
        select {
            border: 1px solid #ccc;
            vertical-align: top;
            min-height: 20px;
        }
    </style>

</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div>
        <img id="main-bg" src="./img/dol.png" style="position: absolute;left: 31%;opacity: 0.15;width: 55vw;z-index: -1;">
    </div>
    <div class="page-wrapper chiller-theme toggled">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand">
                    <a href="#" id="landoffice_name"></a>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">Admin</span>
                        <span class="user-role">Administrator</span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-header  -->
                <div class="sidebar-menu">
                    <ul class="nav-pills-main">
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-summary" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-info-circle"></i>
                                <span style="font-size: 17px;">ภาพรวมการถ่ายโอนข้อมูล</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-backlog-reg" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-info-circle"></i>
                                <span style="font-size: 17px;">ข้อมูลงานค้างระบบงานทะเบียน</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-backlog-svo" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-info-circle"></i>
                                <span style="font-size: 17px;">ข้อมูลงานค้างระบบงานรังวัด</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-backlog-exp" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-info-circle"></i>
                                <span style="font-size: 17px;">ข้อมูลงานค้างกลุ่มงานวิชาการ</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="sidebar-footer">
                <button class="btn btn-block" id="sign-out-btn" onclick="location.href='./portal.php';">กลับสู่หน้า Portal</button>
            </div>
        </nav>
        <!-- sidebar-wrapper  -->
        <main class="page-content">
            <div style="width: 100%;">
                <!-- image -->
                <div class="tab-pane fade" id="tab-backlog-reg">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="form-inline">
                                <!-- <div class="col-auto">
                                    <b>ประเภทบัญชีคุม:</b>
                                    <select class="form-control" id="select-booktype-backlog-reg"></select>
                                </div> -->
                                <div class="col-auto">
                                    <b>วันที่รับเรื่อง ตั้งแต่:</b>
                                    <input type="date" id="date-start-backlog-reg" class="form-control">
                                </div>
                                <div class="col-auto">
                                    <b>วันที่รับเรื่อง จนถึง:</b>
                                    <input type="date" id="date-end-backlog-reg" class="form-control">
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default searchBtn" type="submit" id="search-backlog-reg" style="margin-top: 0px" onclick="searchFunctionReg()">ค้นหา</button>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default clearBtn" type="submit" id="clear-backlog-reg" style="margin-top: 0px" onclick="clearFunctionReg()">ล้างข้อมูล</button>
                                </div>
                            </div>                    
                            <div class="card" style="margin-top: 10px;">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-1-backlog-reg">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th colspan="9" class="w-auto th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p1"></th>
                                            <th class="w-auto th-p1">วันที่รับเรื่อง</th>
                                            <th class="w-auto th-p1">ลำดับที่รับเรื่อง</th>
                                            <th class="w-auto th-p1">ประเภทการจดทะเบียน</th>
                                            <th class="w-auto th-p1">รายละเอียดเอกสารสิทธิ</th>
                                            <th class="w-auto th-p1">ชื่อ-นามสกุล ผู้ขอ</th>
                                            <th class="w-auto th-p1">วันที่ดำเนินการ</th>
                                            <th class="w-auto th-p1">สถานะงาน</th>
                                            <th class="w-auto th-p1">ประเภทงาน</th>
                                        </tr>
                                    </thead>
                                </table>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-2-backlog-reg">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th colspan="9" class="w-auto th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2"></th>
                                            <th class="w-auto th-p2">วันที่รับเรื่อง</th>
                                            <th class="w-auto th-p2">ลำดับที่รับเรื่อง</th>
                                            <th class="w-auto th-p2">ประเภทการจดทะเบียน</th>
                                            <th class="w-auto th-p2">รายละเอียดเอกสารสิทธิ</th>
                                            <th class="w-auto th-p2">ชื่อ-นามสกุล ผู้ขอ</th>
                                            <th class="w-auto th-p2">วันที่ดำเนินการ</th>
                                            <th class="w-auto th-p2">สถานะงาน</th>
                                            <th class="w-auto th-p2">ประเภทงาน</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="reg">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-backlog-svo">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="form-inline">
                                <div class="col-auto">
                                    <b>วันที่รับเรื่อง ตั้งแต่:</b>
                                    <input type="date" id="date-start-backlog-svo" class="form-control">
                                </div>
                                <div class="col-auto">
                                    <b>วันที่รับเรื่อง จนถึง:</b>
                                    <input type="date" id="date-end-backlog-svo" class="form-control">
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default searchBtn" type="submit" id="search-backlog-svo" style="margin-top: 0px" onclick="searchFunctionSvo()">ค้นหา</button>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default clearBtn" type="submit" id="clear-backlog-svo" style="margin-top: 0px" onclick="clearFunctionSvo()">ล้างข้อมูล</button>
                                </div>
                            </div>                    
                            <div class="card" style="margin-top: 10px;">
                                <h4>ตารางงานค้างกองกลาง</h4>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-1-backlog-svo">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th colspan="9" class="w-auto th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p1"></th>
                                            <th class="w-auto th-p1">ลำดับที่รับเรื่อง</th>
                                            <th class="w-auto th-p1">วันที่รับเรื่อง</th>
                                            <th class="w-auto th-p1">เลขที่คำขอรังวัด</th>
                                            <th class="w-auto th-p1">ชื่อ-นามสกุล ผู้ถือกรรมสิทธิ์</th>
                                            <th class="w-auto th-p1">ประเภทงานรังวัด</th>
                                            <th class="w-auto th-p1">รายละเอียดการงดรังวัด</th>
                                        </tr>
                                    </thead>
                                </table>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-2-backlog-svo">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th colspan="9" class="w-auto th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2"></th>
                                            <th class="w-auto th-p2">ลำดับที่รับเรื่อง</th>
                                            <th class="w-auto th-p2">วันที่รับเรื่อง</th>
                                            <th class="w-auto th-p2">เลขที่คำขอรังวัด</th>
                                            <th class="w-auto th-p2">ชื่อ-นามสกุล ผู้ถือกรรมสิทธิ์</th>
                                            <th class="w-auto th-p2">ประเภทงานรังวัด</th>
                                            <th class="w-auto th-p2">รายละเอียดการงดรังวัด</th>
                                        </tr>
                                    </thead>
                                </table>
                                <h4>ตารางงานค้างในมือช่าง</h4>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-1-backlog-svo-surveyor">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th colspan="9" class="w-auto th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p1"></th>
                                            <th class="w-auto th-p1">เลขที่คำขอรังวัด</th>
                                            <th class="w-auto th-p1">ชื่อ-นามสกุล ผู้ถือกรรมสิทธิ์</th>
                                            <th class="w-auto th-p1">ชื่อ-นามสกุล ช่างรังวัด</th>
                                            <th class="w-auto th-p1">ประเภทงานรังวัด</th>
                                            <th class="w-auto th-p1">วันที่นัดรังวัด</th>
                                            <th class="w-auto th-p1">สถานะงานรังวัด</th>
                                        </tr>
                                    </thead>
                                </table>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-2-backlog-svo-surveyor">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th colspan="9" class="w-auto th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2"></th>
                                            <th class="w-auto th-p2">เลขที่คำขอรังวัด</th>
                                            <th class="w-auto th-p2">ชื่อ-นามสกุล ผู้ถือกรรมสิทธิ์</th>
                                            <th class="w-auto th-p2">ชื่อ-นามสกุล ช่างรังวัด</th>
                                            <th class="w-auto th-p2">ประเภทงานรังวัด</th>
                                            <th class="w-auto th-p2">วันที่นัดรังวัด</th>
                                            <th class="w-auto th-p2">สถานะงานรังวัด</th>
                                        </tr>
                                    </thead>
                                </table>
                                <h4>ตารางงานค้างช่างรังวัดเอกชน</h4>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-1-backlog-svo-private">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th colspan="9" class="w-auto th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p1"></th>
                                            <th class="w-auto th-p1">เลขที่คำขอรังวัด</th>
                                            <th class="w-auto th-p1">ชื่อ-นามสกุล ผู้ถือกรรมสิทธิ์</th>
                                            <th class="w-auto th-p1">ชื่อ-นามสกุล ช่างรังวัด</th>
                                            <th class="w-auto th-p1">ประเภทงานรังวัด</th>
                                            <th class="w-auto th-p1">วันที่นัดรังวัด</th>
                                            <th class="w-auto th-p1">สถานะงานรังวัด</th>
                                        </tr>
                                    </thead>
                                </table>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-2-backlog-svo-private">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th colspan="9" class="w-auto th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2"></th>
                                            <th class="w-auto th-p2">เลขที่คำขอรังวัด</th>
                                            <th class="w-auto th-p2">ชื่อ-นามสกุล ผู้ถือกรรมสิทธิ์</th>
                                            <th class="w-auto th-p2">ชื่อ-นามสกุล ช่างรังวัด</th>
                                            <th class="w-auto th-p2">ประเภทงานรังวัด</th>
                                            <th class="w-auto th-p2">วันที่นัดรังวัด</th>
                                            <th class="w-auto th-p2">สถานะงานรังวัด</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="svo">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-backlog-exp">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="form-inline">
                                <div class="col-auto">
                                    <b>วันที่รับเรื่อง ตั้งแต่:</b>
                                    <input type="date" id="date-start-backlog-exp" class="form-control">
                                </div>
                                <div class="col-auto">
                                    <b>วันที่รับเรื่อง จนถึง:</b>
                                    <input type="date" id="date-end-backlog-exp" class="form-control">
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default searchBtn" type="submit" id="search-backlog-exp" style="margin-top: 0px" onclick="searchFunctionEXP()">ค้นหา</button>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default clearBtn" type="submit" id="clear-backlog-exp" style="margin-top: 0px" onclick="clearFunctionEXP()">ล้างข้อมูล</button>
                                </div>
                            </div>                    
                            <div class="card" style="margin-top: 10px;">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-1-backlog-exp">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th colspan="6" class="w-auto th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p1"></th>
                                            <th class="w-auto th-p1">วันที่รับเรื่อง</th>
                                            <th class="w-auto th-p1">เลขที่หนังสือ/เลขที่คิว</th>
                                            <th class="w-auto th-p1" style="text-align: center !important;">ชื่อเรื่อง</th>
                                            <th class="w-auto th-p1">สถานะ</th>
                                            <th class="w-auto th-p1">วันที่ยุติ</th>
                                        </tr>
                                    </thead>
                                </table>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-2-backlog-exp">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th colspan="6" class="w-auto th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2"></th>
                                            <th class="w-auto th-p2">วันที่รับเรื่อง</th>
                                            <th class="w-auto th-p2">เลขที่หนังสือ/เลขที่คิว</th>
                                            <th class="w-auto th-p2" style="text-align: center !important;">ชื่อเรื่อง</th>
                                            <th class="w-auto th-p2">สถานะ</th>
                                            <th class="w-auto th-p2">วันที่ยุติ</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="exp">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-summary">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="card" style="margin-top: 10px;">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-summary">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2"></th>
                                            <th class="w-auto th-p2">ประเภท</th>
                                            <th class="w-auto th-p2">รับมอบ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนสำเร็จ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนไม่สำเร็จ</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="summary">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="text" style="display:none;" id="temp" value=""></input>
        </main>
    <!-- page-content" -->
    </div>

    <!-- jQuery -->
    <script src="./plugins/jquery/jquery.min.js"></script>
    <!-- <script src="plugins/vendor/jquery/jquery-3.2.1.min.js"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="./dist/js/adminlte.js"></script>
    <script src="./plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="./plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="./js/mas.js"></script>
    <script src="./js/backlog.js"></script>
    <script src="./js/backlog-reg.js"></script>
    <script src="./js/backlog-exp.js"></script>
    <script src="./js/backlog-svo.js"></script>
    <!-- <script src="./js/select.js"></script> -->
    <script src="./js/global.js"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

</script>

    <script>
       
    </script>
</body>

</html>