<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ข้อมูลภาพลักษณ์ต้นร่างและแบบคำนวณเนื้อที่</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="./style/style.css">
    <link rel="stylesheet" href="./dist/css/dash.css">
    <link rel="stylesheet" href="./plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="./plugins/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./dist/DataTables/datatables.min.css" />
    <link rel="stylesheet" href="./dist/DataTables/jquery.datatables.min.css" />

    <script src="./plugins/jquery/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./style/login.css">


    <link rel="shortcut icon" href="#" />

    <style>
        select {
            border: 1px solid #ccc;
            vertical-align: top;
            min-height: 20px;
        }
    </style>

</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div>
        <img id="main-bg" src="./img/dol.png" style="position: absolute;left: 31%;opacity: 0.15;width: 55vw;z-index: -1;">
    </div>
    <div class="page-wrapper chiller-theme toggled">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand">
                    <a href="#" id="landoffice_name"></a>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">Admin</span>
                        <span class="user-role">Administrator</span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-header  -->
                <div class="sidebar-menu">
                    <ul class="nav-pills-main">
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-summary" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-image"></i>
                                <span style="font-size: 17px;">ภาพรวมการถ่ายโอนข้อมูล</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-cadastral" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-image"></i>
                                <span style="font-size: 17px;">ข้อมูลภาพลักษณ์ต้นร่าง</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="sidebar-footer">
                <button class="btn btn-block" id="sign-out-btn" onclick="location.href='./portal.php';">กลับสู่หน้า Portal</button>
            </div>
        </nav>
        <!-- sidebar-wrapper  -->
        <main class="page-content">
            <div style="width: 100%;">
                <!-- image -->
                <div class="tab-pane fade" id="tab-cadastral">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="form-inline">
                                <div class="col-auto">
                                    <b>ประเภทเอกสารสิทธิ:</b>
                                    <select class="form-control" id="select-printplateType-cadastral"></select>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default searchBtn" type="submit" id="search-cadastral" style="margin-top: 0px">ค้นหา</button>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default clearBtn" type="submit" id="clear-cadastral" style="margin-top: 0px">ล้างข้อมูล</button>
                                </div>
                            </div>
                            <div class="form-inline" id="form-cadastral-parcel">
                                <div class="col-auto">
                                    <b>หมายเลขแฟ้ม:</b>
                                    <input type="text" class="form-control" maxlength="5" id='input-cadastral-sheet' size="3"></input>
                                    <b>-</b>
                                    <input type="text" class="form-control" maxlength="5" id='input-cadastral-box' size="2"></input>
                                </div>
                                <div class="col-auto">
                                    <b>เลขที่ต้นร่าง:</b>
                                    <input type="text" class="form-control" maxlength="8" id='input-cadastral-no'></input>
                                </div>
                                <div class="col-auto">
                                    <b>ครั้งที่รังวัด:</b>
                                    <input type="text" class="form-control" maxlength="3" id='input-cadastral-num' size="2"></input>
                                </div>
                                <div class="col-auto">
                                    <b>อำเภอ/เขต:</b>
                                    <select class="form-control" id="select-amphur-cadastral"></select>
                                    <b style="margin-left: 20px;">ตำบล/แขวง:</b>
                                    <select class="form-control" id="select-tambol-cadastral"></select>
                                </div>
                            </div>
                            <div class="card" style="margin-top: 10px;">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-main-cadastral">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2"></th>
                                            <th class="w-auto th-p2">หมายเลชแฟ้ม</th>
                                            <th class="w-auto th-p2">เลขที่ต้นร่าง</th>
                                            <th class="w-auto th-p2">ครั้งที่รังวัด</th>
                                            <th class="w-auto th-p2">อำเภอ</th>
                                            <th class="w-auto th-p2">ตำบล</th>
                                            <th class="w-auto th-p2">ประเภทการรังวัด</th>
                                            <th class="w-auto th-p2">วันที่รังวัด</th>
                                            <th class="w-auto th-p2">รายละเอียด</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="main">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                            <div class="card" style="margin-top: 10px;" id="card-detail">
                            
                            <!-- <div class="text-danger text-center" style="font-size: 18px">- ภาพลักษณ์ต้นร่าง ของ โครงการพัฒนาระบบสารสนเทศที่ดิน (ระยะที่ 1) ยังไม่สามารถเปิดดูได้ -</div> -->
                                <div class="overlay overlay-main dark" id="detail">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-detail-cadastral">
                                            <thead>
                                                <tr style="text-align: center;">
                                                    <th class="w-auto"></th>
                                                    <th class="w-auto">ภาพลักษณ์ต้นร่าง</th>
                                                    <th class="w-auto th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    <th class="w-auto th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="col text-center">
                                        <div class="spinner-border text-success" id="loading-image"></div>
                                        <div>
                                            <img id="image-cadastral" class="img-fluid" src="./img/image-dol.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-summary">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="card" style="margin-top: 10px;">
                                <div class="form-inline d-flex justify-content-end">
                                    <div class="col-auto">
                                        <b>ประเภทเอกสารสิทธิ:</b>
                                        <select class="form-control" id="select-printplateType-summary"></select>
                                    </div>
                                </div>
                                <div class="form-inline d-flex justify-content-end">
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-success" type="submit" id="report-s-cadastral" style="margin-top: 5px"><i class="fas fa-file-excel"></i>  Export Success</button>
                                    </div>
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-danger" type="submit" id="report-e-cadastral" style="margin-top: 5px"><i class="fas fa-file-excel"></i>  Export Error</button>
                                    </div>
                                </div>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-summary">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2"></th>
                                            <th class="w-auto th-p2">ประเภทเอกสารสิทธิ</th>
                                            <th class="w-auto th-p2">รับมอบ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนสำเร็จ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนไม่สำเร็จ</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="summary">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="text" style="display:none;" id="temp" value=""></input>
        </main>
    <!-- page-content" -->
    </div>

    <!-- jQuery -->
    <script src="./plugins/jquery/jquery.min.js"></script>
    <!-- <script src="plugins/vendor/jquery/jquery-3.2.1.min.js"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="./dist/js/adminlte.js"></script>
    <script src="./plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="./plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="./js/mas.js"></script>
    <script src="./js/cadastral.js"></script>
    <!-- <script src="./js/select.js"></script> -->
    <script src="./js/global.js"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

</script>

    <script>
       
    </script>
</body>

</html>