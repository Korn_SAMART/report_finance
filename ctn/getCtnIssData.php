<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';

    // ข้อมูลหนังสือรับ / ส่ง
    $revhdSeqP1 = !isset($_POST['revhdSeqP1']) ? '' : $_POST['revhdSeqP1'];
    $revhdSeqP2 = !isset($_POST['revhdSeqP2'])? '' : $_POST['revhdSeqP2'];

    $Result = array();

    include 'queryCtnIssData.php';
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
        
    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
