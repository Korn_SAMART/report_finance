<?php
header('Content-Type: application/json; charset=utf-8');
//require_once 'isXmlHttpRequest.php';
include '../database/conn.php';


switch ($check) {
    case 'overall':
        $select = "WITH recvAll AS (
            SELECT TO_NUMBER('1.1') AS NUM, count(RECV_DOC_SEQ)  TOTAL
           FROM MGT1.TB_CTN_RECV_DOC
           WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                and DOC_SECRET_SEQ= '1'
           group by LANDOFFICE_SEQ
       UNION
            SELECT TO_NUMBER('1.2') AS NUM, count(RECV_DOC_SEQ)  TOTAL
                FROM MGT1.TB_CTN_RECV_DOC
                   WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                and DOC_SECRET_SEQ IN('2', '3', '4')
           group by LANDOFFICE_SEQ
       UNION 
           SELECT TO_NUMBER('1.3') AS NUM, count(SEND_DOC_SEQ)  TOTAL
                FROM MGT1.TB_CTN_SEND_DOC
                   WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                and  DOC_SECRET_SEQ = '1'
           group by LANDOFFICE_SEQ
       UNION
           SELECT TO_NUMBER('1.4') AS NUM, count(SEND_DOC_SEQ)  TOTAL
                FROM MGT1.TB_CTN_SEND_DOC
                   WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                and  DOC_SECRET_SEQ IN('2', '3', '4')
           group by LANDOFFICE_SEQ
           UNION
           SELECT TO_NUMBER('1.7') AS NUM, COUNT(LANDOFFICE_SEQ) TOTAL
            FROM(
            SELECT *
            FROM MGT1.TB_CTN_DOCUMENT
            WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS ='N')
            group by LANDOFFICE_SEQ
       UNION
           SELECT TO_NUMBER('2.1') AS NUM, COUNT(LANDOFFICE_SEQ) TOTAL 
           FROM( SELECT * FROM MGT1.TB_INV_REV_HD  T1
           LEFT OUTER JOIN MGT1.TB_INV_REV_BLD_DTL T2
           ON T1.REV_HD_SEQ =  T2.REV_HD_SEQ
           WHERE REV_HD_TYPE = 11 AND LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N')
               group by LANDOFFICE_SEQ
       
       UNION
       SELECT TO_NUMBER('2.3') AS NUM, SUM(TOTAL) TOTAL
           FROM(
           SELECT LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME, COUNT(*)TOTAL 
           FROM MGT1.TB_INV_REV_PLATE_DTL T1
               LEFT OUTER JOIN MGT1.TB_INV_MAS_PLATE T2
               ON T1.PLATE_SEQ = T2.PLATE_SEQ
               LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
               WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ = 2
               GROUP BY LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME)
           group by LANDOFFICE_SEQ
           UNION
       SELECT TO_NUMBER('2.4') AS NUM, SUM(TOTAL) TOTAL
           FROM(
           SELECT LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME, COUNT(*)TOTAL 
           FROM MGT1.TB_INV_REV_PLATE_DTL T1
               LEFT OUTER JOIN MGT1.TB_INV_MAS_PLATE T2
               ON T1.PLATE_SEQ = T2.PLATE_SEQ
               LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
               WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ IN (3,4)
               GROUP BY LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME)
           group by LANDOFFICE_SEQ
       UNION
       SELECT TO_NUMBER('2.5') AS NUM, SUM(TOTAL) TOTAL
           FROM(
           SELECT LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME, COUNT(*)TOTAL 
           FROM MGT1.TB_INV_REV_PLATE_DTL T1
               LEFT OUTER JOIN MGT1.TB_INV_MAS_PLATE T2
               ON T1.PLATE_SEQ = T2.PLATE_SEQ
               LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
               WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ = 1
               GROUP BY LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME)
           group by LANDOFFICE_SEQ
       UNION
       SELECT TO_NUMBER('2.7') AS NUM, SUM(TOTAL) TOTAL
        FROM(
        SELECT LANDOFFICE_SEQ,T1.PIECE_SEQ,PIECE_NAME, COUNT(*)TOTAL 
        FROM MGT1.TB_INV_REV_PIECE_DTL T1
        LEFT OUTER JOIN MGT1.TB_INV_MAS_PIECE T2
        ON T1.PIECE_SEQ = T2.PIECE_SEQ
        LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
            ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
        WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND HD.RECORD_STATUS = 'N'
        GROUP BY LANDOFFICE_SEQ,T1.PIECE_SEQ,PIECE_NAME)
        group by LANDOFFICE_SEQ ),
       OK AS (
                SELECT TO_NUMBER('1.1') AS NUM, count(T1.RECV_DOC_SEQ)  TOTAL
                       FROM CTN.TB_CTN_RECV_DOC T1
                    INNER JOIN  MGT1.TB_CTN_RECV_DOC T2
                    ON T1.RECV_DOC_SEQ = T2.RECV_DOC_SEQ
                       WHERE T1.LANDOFFICE_SEQ = :landoffice  AND T1.RECORD_STATUS = 'N' 
                       and T1.DOC_SECRET_SEQ= '1'
                  group by T1.LANDOFFICE_SEQ
       UNION
           SELECT TO_NUMBER('1.2') AS NUM, count(RECV_DOC_SEQ)  TOTAL
                FROM CTN.TB_CTN_RECV_DOC
                   WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                and DOC_SECRET_SEQ IN('2', '3', '4')
           group by LANDOFFICE_SEQ
       UNION
           SELECT TO_NUMBER('1.3') AS NUM, count(SEND_DOC_SEQ)  TOTAL
                FROM CTN.TB_CTN_SEND_DOC
                   WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                and  DOC_SECRET_SEQ = '1'
           group by LANDOFFICE_SEQ
       UNION
           SELECT TO_NUMBER('1.4') AS NUM, count(SEND_DOC_SEQ)  TOTAL
                FROM CTN.TB_CTN_SEND_DOC
                   WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                and  DOC_SECRET_SEQ IN('2', '3', '4')
           group by LANDOFFICE_SEQ
        UNION
        SELECT TO_NUMBER('1.7') AS NUM, COUNT(LANDOFFICE_SEQ) TOTAL
            FROM(
            SELECT *
            FROM CTN.TB_CTN_DOCUMENT
            WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS ='N')
            group by LANDOFFICE_SEQ
       UNION
           SELECT TO_NUMBER('2.1') AS NUM, COUNT(LANDOFFICE_SEQ) TOTAL 
           FROM( SELECT * FROM INV.TB_INV_REV_HD  T1
           LEFT OUTER JOIN INV.TB_INV_REV_BLD_DTL T2
           ON T1.REV_HD_SEQ =  T2.REV_HD_SEQ
           WHERE REV_HD_TYPE = 11 AND LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N')
               group by LANDOFFICE_SEQ
         
       UNION
            SELECT TO_NUMBER('2.3') AS NUM, SUM(TOTAL) TOTAL
           FROM(
           SELECT LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME, COUNT(*)TOTAL 
           FROM INV.TB_INV_REV_PLATE_DTL T1
               LEFT OUTER JOIN INV.TB_INV_MAS_PLATE T2
               ON T1.PLATE_SEQ = T2.PLATE_SEQ
               LEFT OUTER JOIN INV.TB_INV_REV_HD HD
                ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
               WHERE LANDOFFICE_SEQ = :landoffice AND T2.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ = 2
               GROUP BY LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME)
           group by LANDOFFICE_SEQ
           UNION
       SELECT TO_NUMBER('2.4') AS NUM, SUM(TOTAL) TOTAL
           FROM(
           SELECT LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME, COUNT(*)TOTAL 
           FROM INV.TB_INV_REV_PLATE_DTL T1
               LEFT OUTER JOIN MGT1.TB_INV_MAS_PLATE T2
               ON T1.PLATE_SEQ = T2.PLATE_SEQ
               LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
               WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ IN (3,4)
               GROUP BY LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME)
           group by LANDOFFICE_SEQ
       UNION
       SELECT TO_NUMBER('2.5') AS NUM, SUM(TOTAL) TOTAL
           FROM(
           SELECT LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME, COUNT(*)TOTAL 
           FROM INV.TB_INV_REV_PLATE_DTL T1
               LEFT OUTER JOIN INV.TB_INV_MAS_PLATE T2
               ON T1.PLATE_SEQ = T2.PLATE_SEQ
               LEFT OUTER JOIN INV.TB_INV_REV_HD HD
                ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
               WHERE LANDOFFICE_SEQ = :landoffice AND T2.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ = 1
               GROUP BY LANDOFFICE_SEQ,T1.PLATE_SEQ,PLATE_NAME)
           group by LANDOFFICE_SEQ
       UNION
       SELECT TO_NUMBER('2.7') AS NUM, SUM(TOTAL) TOTAL
        FROM(
        SELECT LANDOFFICE_SEQ,T1.PIECE_SEQ,PIECE_NAME, COUNT(*)TOTAL 
        FROM INV.TB_INV_REV_PIECE_DTL T1
        LEFT OUTER JOIN INV.TB_INV_MAS_PIECE T2
        ON T1.PIECE_SEQ = T2.PIECE_SEQ
        LEFT OUTER JOIN INV.TB_INV_REV_HD HD
            ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
        WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND HD.RECORD_STATUS = 'N'
        GROUP BY LANDOFFICE_SEQ,T1.PIECE_SEQ,PIECE_NAME)
        group by LANDOFFICE_SEQ )
       SELECT recvAll.NUM, nvl(recvALL.TOTAL, 0) as TOTAL, nvl(ok.TOTAL,0) as Y_SEQ, nvl((recvall.TOTAL-ok.TOTAL),0) as N_SEQ from recvAll
       INNER JOIN OK
           ON recvall.num = ok.num
       UNION
           SELECT TO_NUMBER('3.1') AS NUM,
               SUM(ALL_SEQ) TOTAL,
               SUM(Y_SEQ) Y_SEQ,
               SUM(N_SEQ) N_SEQ
           FROM(
           SELECT T1.PSN_SEQ, PSN_PID, PSN_TITLE_SEQ, PSN_FNAME, PSN_LNAME,
                1 ALL_SEQ,
               CASE
                   WHEN NVL( T1.LOG1_MIGRATE_NOTE, 'XXX' ) = 'XXX'  THEN 0
                 ELSE 1
               END  AS  N_SEQ,
                CASE
                   WHEN NVL( T1.LOG1_MIGRATE_NOTE, 'XXX' ) = 'XXX' THEN 1 
                  ELSE 0
               END  AS  Y_SEQ
            FROM MGT1.TB_HRM_PNR_PERSONNEL T1
            INNER JOIN MGT1.TB_HRM_MAS_POSITION T2
           ON T1.PSN_SEQ  =  T2.PSN_SEQ 
           WHERE T2.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )
           AND T1.RECORD_STATUS = 'N' AND T2.RECORD_STATUS = 'N'  )
       UNION
           SELECT TO_NUMBER('3.2') AS NUM,
               SUM(ALL_SEQ) TOTAL,
               SUM(Y_SEQ) Y_SEQ,
               SUM(N_SEQ) N_SEQ
           FROM(
           SELECT T1.PSN_SEQ, PSN_PID, PSN_TITLE_SEQ, PSN_FNAME, PSN_LNAME,
                1 ALL_SEQ,
               CASE
                   WHEN NVL( T1.LOG1_MIGRATE_NOTE, 'XXX' ) = 'XXX'  THEN 0
                 ELSE 1
               END  AS  N_SEQ,
                CASE
                   WHEN NVL( T1.LOG1_MIGRATE_NOTE, 'XXX' ) = 'XXX' THEN 1 
                  ELSE 0
               END  AS  Y_SEQ
            FROM MGT1.TB_HRM_PNR_PERSONNEL T1
            INNER JOIN MGT1.TB_HRM_MAS_POSITION T2
           ON T1.PSN_SEQ  =  T2.PSN_SEQ 
           INNER JOIN MGT1.TB_HRM_FUR_LEAVE_SUMMARY T3
            ON T1.PSN_SEQ  =  T3.PSN_SEQ 
           WHERE T2.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )
           AND T1.RECORD_STATUS = 'N' AND T2.RECORD_STATUS = 'N'   AND T3.RECORD_STATUS = 'N')";
        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;
    case '1':
        $select = "WITH COUNTROW AS (
                    SELECT NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ) AS DOC_SECRET_SEQ, COUNT(P1.DOC_SECRET_SEQ) AS COUNT_P1, COUNT(P2.DOC_SECRET_SEQ) AS COUNT_P2
                        FROM CTN.TB_CTN_RECV_DOC P2
                        INNER JOIN  MGT1.TB_CTN_RECV_DOC P1
                        ON P2.RECV_DOC_SEQ = P1.RECV_DOC_SEQ
                    WHERE P2.LANDOFFICE_SEQ = :landoffice AND P2.RECORD_STATUS = 'N' AND  P2.DOC_SECRET_SEQ = '1'
                    GROUP BY NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ)
                    ORDER BY NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ)
                )
                SELECT NVL(MAS_DOC_SECRET.DOC_SECRET_NAME, 'ไม่มีเลขชั้นความลับ') as NAME, COUNTROW.COUNT_P1, COUNTROW.COUNT_P2
                FROM COUNTROW
                INNER JOIN CTN.TB_CTN_MAS_DOC_SECRET MAS_DOC_SECRET
                ON COUNTROW.DOC_SECRET_SEQ = MAS_DOC_SECRET.DOC_SECRET_SEQ";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;
    case '2':
        $select = "WITH COUNTROW AS (
                        SELECT NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ) AS DOC_SECRET_SEQ, COUNT(P1.DOC_SECRET_SEQ) AS COUNT_P1, COUNT(P2.DOC_SECRET_SEQ) AS COUNT_P2
                            FROM CTN.TB_CTN_RECV_DOC P2
                            INNER JOIN  MGT1.TB_CTN_RECV_DOC P1
                            ON P2.RECV_DOC_SEQ = P1.RECV_DOC_SEQ
                        WHERE P2.LANDOFFICE_SEQ = :landoffice AND P2.RECORD_STATUS = 'N' AND  P2.DOC_SECRET_SEQ IN ('2', '3', '4')
                        GROUP BY NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ)
                        ORDER BY NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ)
                    )
                    SELECT NVL(MAS_DOC_SECRET.DOC_SECRET_NAME, 'ไม่มีเลขชั้นความลับ') as NAME, COUNTROW.COUNT_P1, COUNTROW.COUNT_P2
                    FROM COUNTROW
                    INNER JOIN CTN.TB_CTN_MAS_DOC_SECRET MAS_DOC_SECRET
                    ON COUNTROW.DOC_SECRET_SEQ = MAS_DOC_SECRET.DOC_SECRET_SEQ";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;
    case '3':
        $select = "WITH COUNTROW AS (
                            SELECT NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ) AS DOC_SECRET_SEQ, COUNT(P1.SEND_DOC_SEQ) AS COUNT_P1, COUNT(P2.SEND_DOC_SEQ) AS COUNT_P2
                                FROM CTN.TB_CTN_SEND_DOC P2
                                INNER JOIN  MGT1.TB_CTN_SEND_DOC P1
                                ON P2.SEND_DOC_SEQ = P1.SEND_DOC_SEQ
                            WHERE P2.LANDOFFICE_SEQ = :landoffice AND P2.RECORD_STATUS = 'N' AND P2.DOC_SECRET_SEQ = '1'
                            GROUP BY NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ)
                            ORDER BY NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ)
                        )
                        SELECT NVL(MAS_DOC_SECRET.DOC_SECRET_NAME, 'ไม่มีเลขชั้นความลับ') as NAME, COUNTROW.COUNT_P1, COUNTROW.COUNT_P2
                        FROM COUNTROW
                        INNER JOIN CTN.TB_CTN_MAS_DOC_SECRET MAS_DOC_SECRET
                        ON COUNTROW.DOC_SECRET_SEQ = MAS_DOC_SECRET.DOC_SECRET_SEQ";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;
    case '4':
        $select = "WITH COUNTROW AS (
                        SELECT NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ) AS DOC_SECRET_SEQ, COUNT(P1.SEND_DOC_SEQ) AS COUNT_P1, COUNT(P2.SEND_DOC_SEQ) AS COUNT_P2
                            FROM CTN.TB_CTN_SEND_DOC P2
                            INNER JOIN  MGT1.TB_CTN_SEND_DOC P1
                            ON P2.SEND_DOC_SEQ = P1.SEND_DOC_SEQ
                        WHERE P2.LANDOFFICE_SEQ = :landoffice AND P2.RECORD_STATUS = 'N' AND P2.DOC_SECRET_SEQ IN ('2', '3', '4')
                        GROUP BY NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ)
                        ORDER BY NVL(P2.DOC_SECRET_SEQ, P1.DOC_SECRET_SEQ)
                    )
                    SELECT NVL(MAS_DOC_SECRET.DOC_SECRET_NAME, 'ไม่มีเลขชั้นความลับ') as NAME, COUNTROW.COUNT_P1, COUNTROW.COUNT_P2
                    FROM COUNTROW
                    INNER JOIN CTN.TB_CTN_MAS_DOC_SECRET MAS_DOC_SECRET
                    ON COUNTROW.DOC_SECRET_SEQ = MAS_DOC_SECRET.DOC_SECRET_SEQ";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;
    case '5':
        $select = "SELECT COUNT(P1.SECRET) AS COUNT_P1, COUNT(P2.SECRET) AS COUNT_P2, CASE NVL(P2.SECRET, P1.SECRET) WHEN '1' THEN 'ไม่มีความลับ' ELSE 'มีความลับ' END AS NAME
                        FROM CTN.TB_CTN_PROVINCE_NUMBER P2
                        INNER JOIN  MGT1.TB_CTN_PROVINCE_NUMBER P1
                        ON P2.PROV_NUM_SEQ = P1.PROV_NUM_SEQ
                    WHERE P2.LANDOFFICE_SEQ = :landoffice AND P2.RECORD_STATUS = 'N' AND P2.SECRET = '1'
                    GROUP BY NVL(P2.SECRET, P1.SECRET)";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;
    case '6':
        $select = "SELECT COUNT(P1.SECRET) AS COUNT_P1, COUNT(P2.SECRET) AS COUNT_P2, CASE NVL(P2.SECRET, P1.SECRET) WHEN '1' THEN 'ไม่มีความลับ' ELSE 'มีความลับ' END AS NAME
                        FROM CTN.TB_CTN_PROVINCE_NUMBER P2
                        INNER JOIN  MGT1.TB_CTN_PROVINCE_NUMBER P1
                        ON P2.PROV_NUM_SEQ = P1.PROV_NUM_SEQ
                    WHERE P2.LANDOFFICE_SEQ = :landoffice AND P2.RECORD_STATUS = 'N' AND P2.SECRET = '2'
                    GROUP BY NVL(P2.SECRET, P1.SECRET)";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;
    case '7':
        $select = "SELECT NVL(P2.SUBJECT, P1.SUBJECT) AS NAME, COUNT(P1.DOCUMENT_SEQ) AS COUNT_P1, COUNT(P2.DOCUMENT_SEQ) AS COUNT_P2
                        FROM CTN.TB_CTN_DOCUMENT P2
                        LEFT OUTER JOIN  MGT1.TB_CTN_DOCUMENT P1
                        ON P2.DOCUMENT_SEQ = P1.DOCUMENT_SEQ
                    WHERE P2.LANDOFFICE_SEQ = :landoffice AND P2.RECORD_STATUS = 'N'
                    GROUP BY NVL(P2.SUBJECT, P1.SUBJECT)";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;
    case '8':
        $select = "WITH COUNTROW_P1 AS (
                    SELECT T1.REV_HD_SEQ, T2.BLD_SEQ FROM MGT1.TB_INV_REV_HD  T1
                    LEFT OUTER JOIN MGT1.TB_INV_REV_BLD_DTL T2
                    ON T1.REV_HD_SEQ =  T2.REV_HD_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_BLD BLD
                    ON T2.BLD_SEQ =  BLD.BLD_SEQ
                    WHERE REV_HD_TYPE = 11 AND LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'
                ),
                COUNTROW_P2 AS (
                    SELECT T1.REV_HD_SEQ, T2.BLD_SEQ FROM INV.TB_INV_REV_HD  T1
                    LEFT OUTER JOIN INV.TB_INV_REV_BLD_DTL T2
                    ON T1.REV_HD_SEQ =  T2.REV_HD_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_BLD BLD
                    ON T2.BLD_SEQ =  BLD.BLD_SEQ
                    WHERE REV_HD_TYPE = 11 AND LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'
                )
                SELECT BLD.BLD_NAME AS NAME,  COUNT(P1.BLD_SEQ) AS COUNT_P1, COUNT(P2.BLD_SEQ) AS COUNT_P2
                FROM COUNTROW_P1 P1
                FULL OUTER JOIN COUNTROW_P2 P2
                ON  P1.REV_HD_SEQ = P2.REV_HD_SEQ
                FULL OUTER JOIN INV.TB_INV_MAS_BLD BLD
                ON NVL(P2.BLD_SEQ,P1.BLD_SEQ) = BLD.BLD_SEQ
                GROUP BY BLD.BLD_NAME";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;
    case '9':
        $select = "WITH COUNTROW_P1 AS (
            SELECT STK_ASS.*, MET_REV.MET_REV_NAME, CAT_ASSET.CAT_ASSET_NAME, TOPIC_ASSET.TOPIC_ASSET_SEQ
            FROM  MGT1.TB_INV_STK_ASS STK_ASS
            LEFT OUTER JOIN MGT1.TB_INV_STK_ASS_SUB STK_ASS_SUB
            ON STK_ASS.STK_ASS_SEQ = STK_ASS_SUB.STK_ASS_SEQ
            LEFT OUTER JOIN MGT1.TB_INV_MAS_CAT_ASSET CAT_ASSET
            ON STK_ASS.CAT_ASSET_SEQ = CAT_ASSET.CAT_ASSET_SEQ
            LEFT OUTER JOIN MGT1.TB_INV_MAS_TYPE_ASSET TYPE_ASSET
            ON CAT_ASSET.TYPE_ASSET_SEQ = TYPE_ASSET.TYPE_ASSET_SEQ
            LEFT OUTER JOIN MGT1.TB_INV_MAS_TOPIC_ASSET TOPIC_ASSET
            ON TYPE_ASSET.TOPIC_ASSET_SEQ = TOPIC_ASSET.TOPIC_ASSET_SEQ
            LEFT OUTER JOIN MGT1.TB_INV_MAS_MET_REV MET_REV
            ON STK_ASS.MET_REV_SEQ = MET_REV.MET_REV_SEQ
            WHERE STK_ASS.LANDOFFICE_SEQ = :landoffice AND STK_ASS.RECORD_STATUS ='N'
        ),
        COUNTROW_P2 AS (
            SELECT STK_ASS.*, MET_REV.MET_REV_NAME, CAT_ASSET.CAT_ASSET_NAME, TOPIC_ASSET.TOPIC_ASSET_SEQ
            FROM  INV.TB_INV_STK_ASS STK_ASS
            LEFT OUTER JOIN INV.TB_INV_STK_ASS_SUB STK_ASS_SUB
            ON STK_ASS.STK_ASS_SEQ = STK_ASS_SUB.STK_ASS_SEQ
            LEFT OUTER JOIN INV.TB_INV_MAS_CAT_ASSET CAT_ASSET
            ON STK_ASS.CAT_ASSET_SEQ = CAT_ASSET.CAT_ASSET_SEQ
            LEFT OUTER JOIN INV.TB_INV_MAS_TYPE_ASSET TYPE_ASSET
            ON CAT_ASSET.TYPE_ASSET_SEQ = TYPE_ASSET.TYPE_ASSET_SEQ
            LEFT OUTER JOIN INV.TB_INV_MAS_TOPIC_ASSET TOPIC_ASSET
            ON TYPE_ASSET.TOPIC_ASSET_SEQ = TOPIC_ASSET.TOPIC_ASSET_SEQ
            LEFT OUTER JOIN INV.TB_INV_MAS_MET_REV MET_REV
            ON STK_ASS.MET_REV_SEQ = MET_REV.MET_REV_SEQ
            WHERE STK_ASS.LANDOFFICE_SEQ = :landoffice AND STK_ASS.RECORD_STATUS ='N'
        )
        SELECT NVL(TOPIC_ASSET.TOPIC_ASSET_NAME, 'ไม่มีชื่อหมวดครุภัณฑ์') AS NAME,  COUNT(P1.STK_ASS_SEQ) AS COUNT_P1, COUNT(P2.STK_ASS_SEQ) AS COUNT_P2
        FROM COUNTROW_P1 P1
        FULL OUTER JOIN COUNTROW_P2 P2
        ON  P1.STK_ASS_SEQ = P2.STK_ASS_SEQ
        FULL OUTER JOIN INV.TB_INV_MAS_TOPIC_ASSET TOPIC_ASSET
        ON NVL(P2.TOPIC_ASSET_SEQ,P1.TOPIC_ASSET_SEQ) = TOPIC_ASSET.TOPIC_ASSET_SEQ
        GROUP BY TOPIC_ASSET.TOPIC_ASSET_NAME, NVL(P2.TOPIC_ASSET_SEQ, P1.TOPIC_ASSET_SEQ)
        ORDER BY NVL(P2.TOPIC_ASSET_SEQ, P1.TOPIC_ASSET_SEQ)";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;
    case '10':
        $select = "WITH COUNTROW_P1 AS (
                    SELECT T1.REV_PLATE_SEQ, T2.PLATE_SEQ FROM MGT1.TB_INV_REV_PLATE_DTL T1
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_PLATE T2
                    ON T1.PLATE_SEQ =  T2.PLATE_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                    ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                    WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ = 2 AND HD.RECORD_STATUS = 'N'
                ),
                COUNTROW_P2 AS (
                SELECT T1.REV_PLATE_SEQ, T2.PLATE_SEQ FROM INV.TB_INV_REV_PLATE_DTL T1
                    LEFT OUTER JOIN INV.TB_INV_MAS_PLATE T2
                    ON T1.PLATE_SEQ =  T2.PLATE_SEQ
                    LEFT OUTER JOIN INV.TB_INV_REV_HD HD
                    ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                    WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ = 2 AND HD.RECORD_STATUS = 'N'
                )
                SELECT PLATE.PLATE_SEQ, PLATE.PLATE_NAME AS NAME,  COUNT(P1.REV_PLATE_SEQ) AS COUNT_P1, COUNT(P2.REV_PLATE_SEQ) AS COUNT_P2
                FROM COUNTROW_P1 P1
                FULL OUTER JOIN COUNTROW_P2 P2
                ON  P1.REV_PLATE_SEQ = P2.REV_PLATE_SEQ
                FULL OUTER JOIN INV.TB_INV_MAS_PLATE PLATE
                ON NVL(P2.PLATE_SEQ, P1.PLATE_SEQ) = PLATE.PLATE_SEQ
                WHERE PLATE.PLATE_TYPE_SEQ = 2
                GROUP BY PLATE.PLATE_SEQ, PLATE.PLATE_NAME
                ORDER BY PLATE.PLATE_SEQ";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;

    case '11':
        $select = "WITH COUNTROW_P1 AS (
                        SELECT T1.REV_PLATE_SEQ, T2.PLATE_SEQ FROM MGT1.TB_INV_REV_PLATE_DTL T1
                        LEFT OUTER JOIN MGT1.TB_INV_MAS_PLATE T2
                        ON T1.PLATE_SEQ =  T2.PLATE_SEQ
                        LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                        ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                        WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ IN (3,4) AND HD.RECORD_STATUS = 'N'
                    ),
                    COUNTROW_P2 AS (
                    SELECT T1.REV_PLATE_SEQ, T2.PLATE_SEQ FROM INV.TB_INV_REV_PLATE_DTL T1
                        LEFT OUTER JOIN INV.TB_INV_MAS_PLATE T2
                        ON T1.PLATE_SEQ =  T2.PLATE_SEQ
                        LEFT OUTER JOIN INV.TB_INV_REV_HD HD
                        ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                        WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ IN (3,4) AND HD.RECORD_STATUS = 'N'
                    )
                    SELECT PLATE.PLATE_SEQ, PLATE.PLATE_NAME AS NAME,  COUNT(P1.REV_PLATE_SEQ) AS COUNT_P1, COUNT(P2.REV_PLATE_SEQ) AS COUNT_P2
                    FROM COUNTROW_P1 P1
                    FULL OUTER JOIN COUNTROW_P2 P2
                    ON  P1.REV_PLATE_SEQ = P2.REV_PLATE_SEQ
                    FULL OUTER JOIN INV.TB_INV_MAS_PLATE PLATE
                    ON P1.PLATE_SEQ = PLATE.PLATE_SEQ
                    AND P2.PLATE_SEQ = PLATE.PLATE_SEQ
                    WHERE PLATE.PLATE_TYPE_SEQ IN (3,4)
                    GROUP BY PLATE.PLATE_SEQ, PLATE.PLATE_NAME
                    ORDER BY PLATE.PLATE_SEQ";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;

    case '12':
        $select = "WITH COUNTROW_P1 AS (
                    SELECT T1.REV_PLATE_SEQ, T1.PLATE_SEQ FROM MGT1.TB_INV_REV_PLATE_DTL T1
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_PLATE T2
                    ON T1.PLATE_SEQ =  T2.PLATE_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                    ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                    WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ = 1 AND HD.RECORD_STATUS = 'N'
                ),
                COUNTROW_P2 AS (
                SELECT T1.REV_PLATE_SEQ, T1.PLATE_SEQ FROM INV.TB_INV_REV_PLATE_DTL T1
                    LEFT OUTER JOIN INV.TB_INV_MAS_PLATE T2
                    ON T1.PLATE_SEQ =  T2.PLATE_SEQ
                    LEFT OUTER JOIN INV.TB_INV_REV_HD HD
                    ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                    WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ = 1 AND HD.RECORD_STATUS = 'N'
                )
                SELECT PLATE.PLATE_SEQ, PLATE.PLATE_NAME AS NAME,  COUNT(P1.REV_PLATE_SEQ) AS COUNT_P1, COUNT(P2.REV_PLATE_SEQ) AS COUNT_P2
                FROM COUNTROW_P1 P1
                FULL OUTER JOIN COUNTROW_P2 P2
                ON  P1.REV_PLATE_SEQ = P2.REV_PLATE_SEQ
                FULL OUTER JOIN INV.TB_INV_MAS_PLATE PLATE
                ON P1.PLATE_SEQ = PLATE.PLATE_SEQ
                AND P2.PLATE_SEQ = PLATE.PLATE_SEQ
                WHERE PLATE.PLATE_TYPE_SEQ = 1
                GROUP BY PLATE.PLATE_SEQ, PLATE.PLATE_NAME
                ORDER BY PLATE.PLATE_SEQ";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;

    case '14':
        $select = "WITH COUNTROW_P1 AS (
                    SELECT T1.REV_PIECE_SEQ, T1.PIECE_SEQ FROM MGT1.TB_INV_REV_PIECE_DTL T1
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_PIECE T2
                    ON T1.PIECE_SEQ =  T2.PIECE_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                    ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                    WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND HD.RECORD_STATUS = 'N'
                ),
                COUNTROW_P2 AS (
                SELECT T1.REV_PIECE_SEQ, T1.PIECE_SEQ FROM INV.TB_INV_REV_PIECE_DTL T1
                    LEFT OUTER JOIN INV.TB_INV_MAS_PIECE T2
                    ON T1.PIECE_SEQ =  T2.PIECE_SEQ
                    LEFT OUTER JOIN INV.TB_INV_REV_HD HD
                    ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                    WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND HD.RECORD_STATUS = 'N'
                )
                SELECT PIECE.PIECE_SEQ, PIECE.PIECE_NAME AS NAME, COUNT(P1.REV_PIECE_SEQ) AS COUNT_P1, COUNT(P2.REV_PIECE_SEQ) AS COUNT_P2
                FROM COUNTROW_P1 P1
                FULL OUTER JOIN COUNTROW_P2 P2
                ON  P1.REV_PIECE_SEQ = P2.REV_PIECE_SEQ
                FULL OUTER JOIN INV.TB_INV_MAS_PIECE PIECE
                ON P1.PIECE_SEQ = PIECE.PIECE_SEQ
                AND P2.PIECE_SEQ = PIECE.PIECE_SEQ
                GROUP BY PIECE.PIECE_SEQ, PIECE.PIECE_NAME
                ORDER BY PIECE.PIECE_SEQ";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;

    case '15':
        $select = "SELECT                         
                    PSNTYPE.PSN_TYPE_SEQ AS SEQ,
                    PSNTYPE.PSN_TYPE_NAME AS NAME,
                    NVL(SUM(P1.ALL_SEQ),0) COUNT_P1,
                    NVL(SUM(P1.Y_SEQ), 0) COUNT_P2
                FROM(
                SELECT T1.PSN_SEQ,T2.DIV_POSITION_SEQ, T1.PSN_TYPE_SEQ,  PSN_PID, PSN_TITLE_SEQ, PSN_FNAME, PSN_LNAME,
                    1 ALL_SEQ,
                    CASE
                    WHEN NVL( T1.LOG1_MIGRATE_NOTE, 'XXX' ) = 'XXX'  THEN 0
                    ELSE 1
                    END  AS  N_SEQ,
                    CASE
                        WHEN NVL( T1.LOG1_MIGRATE_NOTE, 'XXX' ) = 'XXX' THEN 1 
                    ELSE 0
                    END  AS  Y_SEQ
                FROM MGT1.TB_HRM_PNR_PERSONNEL T1
                LEFT OUTER JOIN  MGT1.TB_HRM_MAS_POSITION T2
                ON T1.PSN_SEQ  =  T2.PSN_SEQ 
                LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE LANDOFFICE
                ON T2.LANDOFFICE_SEQ = LANDOFFICE.LANDOFFICE_SEQ
                WHERE T2.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )
                AND T1.RECORD_STATUS = 'N' AND T2.RECORD_STATUS = 'N'  ) P1
                FULL OUTER JOIN MGT1.TB_HRM_MAS_PSNTYPE PSNTYPE
                ON P1.PSN_TYPE_SEQ = PSNTYPE.PSN_TYPE_SEQ
                
                GROUP BY  PSNTYPE.PSN_TYPE_SEQ,PSNTYPE.PSN_TYPE_NAME
                ORDER BY  PSNTYPE.PSN_TYPE_SEQ";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);
        break;

    case '16':
        $adm_id_sql = $adm_id == '' ? '' : " AND PERSONNEL.PSN_PID = :adm_id ";
        $adm_firstname_sql = $adm_firstname == '' ? '' : " AND PERSONNEL.PSN_FNAME = :adm_firstname ";
        $adm_lastname_sql = $adm_lastname == '' ? '' : " AND PERSONNEL.PSN_LNAME = :adm_lastname ";
        $year_sql = $year == '' ? '' : " AND RECORD.FISCAL_YEAR = :year ";

        $select = "WITH P1 AS (
            SELECT PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, TITLE.TITLE_NAME, PERSONNEL.PSN_FNAME,PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 1 THEN 1 END) AS SICK_COUNT, 
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 2 THEN 1 END) AS DELIVER_COUNT, 
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 3 THEN 1 END) AS BUSINESS_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 4 THEN 1 END) AS REST_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 5 THEN 1 END) AS ORDAIN_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 6 THEN 1 END) AS HUJJ_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 7 THEN 1 END) AS MILITARY_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 8 THEN 1 END) AS STUDY_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 9 THEN 1 END) AS INTER_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 10 THEN 1 END) AS FOLLOW_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 11 THEN 1 END) AS HELP_WIFE_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 12 THEN 1 END) AS IMPROVE_COUNT
                            FROM MGT1.TB_HRM_PNR_PERSONNEL PERSONNEL
                            LEFT OUTER JOIN MAS.TB_MAS_TITLE TITLE
                            ON PERSONNEL.PSN_TITLE_SEQ = TITLE.TITLE_SEQ
                            LEFT OUTER JOIN MGT1.TB_HRM_MAS_POSITION POSITION
                            ON PERSONNEL.PSN_SEQ = POSITION.PSN_SEQ
                            LEFT OUTER JOIN MGT1.TB_HRM_FUR_LEAVE_RECORD RECORD
                            ON PERSONNEL.PSN_SEQ = RECORD.PSN_SEQ
                            WHERE POSITION.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )"
                            . $adm_id_sql
                            . $adm_firstname_sql
                            . $adm_lastname_sql
                            . $year_sql
                            . "
                            GROUP BY PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, TITLE.TITLE_NAME, PERSONNEL.PSN_FNAME,PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR
                            ORDER BY PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR
            ),
            P2 AS (
            SELECT PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, TITLE.TITLE_NAME, PERSONNEL.PSN_FNAME,PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 1 THEN 1 END) AS SICK_COUNT, 
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 2 THEN 1 END) AS DELIVER_COUNT, 
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 3 THEN 1 END) AS BUSINESS_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 4 THEN 1 END) AS REST_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 5 THEN 1 END) AS ORDAIN_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 6 THEN 1 END) AS HUJJ_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 7 THEN 1 END) AS MILITARY_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 8 THEN 1 END) AS STUDY_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 9 THEN 1 END) AS INTER_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 10 THEN 1 END) AS FOLLOW_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 11 THEN 1 END) AS HELP_WIFE_COUNT,
                                COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 12 THEN 1 END) AS IMPROVE_COUNT
                            FROM HRM.TB_HRM_PNR_PERSONNEL PERSONNEL
                            LEFT OUTER JOIN MGT1.TB_MAS_TITLE TITLE
                            ON PERSONNEL.PSN_TITLE_SEQ = TITLE.TITLE_SEQ
                            LEFT OUTER JOIN HRM.TB_HRM_MAS_POSITION POSITION
                            ON PERSONNEL.PSN_SEQ = POSITION.PSN_SEQ
                            LEFT OUTER JOIN HRM.TB_HRM_FUR_LEAVE_RECORD RECORD
                            ON PERSONNEL.PSN_SEQ = RECORD.PSN_SEQ
                            WHERE POSITION.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )"
                                . $adm_id_sql
                                . $adm_firstname_sql
                                . $adm_lastname_sql
                                . $year_sql
                                . "
                            GROUP BY PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, TITLE.TITLE_NAME, PERSONNEL.PSN_FNAME,PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR
                            ORDER BY PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR
            
            )
            SELECT P1.*, P2.PSN_SEQ AS PSN_SEQ_P2, P2.PSN_PID AS PSN_PID_P2, P2.TITLE_NAME AS TITLE_NAME_P2, P2.PSN_FNAME AS PSN_FNAME_P2, P2.PSN_LNAME AS PSN_LNAME_P2, P2.FISCAL_YEAR AS FISCAL_YEAR_P2,
            P2.SICK_COUNT AS SICK_COUNT_P2, 
           P2.DELIVER_COUNT AS DELIVER_COUNT_P2, 
            P2.BUSINESS_COUNT AS BUSINESS_COUNT_P2,
           P2.REST_COUNT AS REST_COUNT_P2,
            P2.ORDAIN_COUNT AS ORDAIN_COUNT_P2,
            P2.HUJJ_COUNT AS HUJJ_COUNT_P2,
            P2.MILITARY_COUNT AS MILITARY_COUNT_P2,
            P2.STUDY_COUNT AS STUDY_COUNT_P2,
             P2.INTER_COUNT AS INTER_COUNT_P2,
            P2.FOLLOW_COUNT AS FOLLOW_COUNT_P2 ,
            P2.HELP_WIFE_COUNT AS HELP_WIFE_COUNT_P2,
            P2.IMPROVE_COUNT AS IMPROVE_COUNT_P2
           FROM P1
           INNER JOIN P2
           ON P1.PSN_SEQ = P2.PSN_SEQ
           AND P1.FISCAL_YEAR = P2.FISCAL_YEAR";


        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($year != '') oci_bind_by_name($stid, ':year', $year);
        if ($adm_id != '') oci_bind_by_name($stid, ':adm_id', $adm_id);
        if ($adm_firstname != '') oci_bind_by_name($stid, ':adm_firstname', $adm_firstname);
        if ($adm_lastname != '') oci_bind_by_name($stid, ':adm_lastname', $adm_lastname);


        oci_execute($stid);
        break;

        // case '17':
        //     $adm_id_sql = $adm_id == '' ? '' : " AND PERSONNEL.PSN_PID = :adm_id ";
        //     $adm_firstname_sql = $adm_firstname == '' ? '' : " AND PERSONNEL.PSN_FNAME = :adm_firstname ";
        //     $adm_lastname_sql = $adm_lastname == '' ? '' : " AND PERSONNEL.PSN_LNAME = :adm_lastname ";
        //     $year_sql = $year == '' ? '' : " AND RECORD.FISCAL_YEAR = :year ";
        //     $select = "SELECT PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, TITLE.TITLE_NAME, PERSONNEL.PSN_FNAME,PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR,
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 1 THEN 1 END) AS SICK_COUNT, 
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 2 THEN 1 END) AS DELIVER_COUNT, 
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 3 THEN 1 END) AS BUSINESS_COUNT,
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 4 THEN 1 END) AS REST_COUNT,
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 5 THEN 1 END) AS ORDAIN_COUNT,
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 6 THEN 1 END) AS HUJJ_COUNT,
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 7 THEN 1 END) AS MILITARY_COUNT,
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 8 THEN 1 END) AS STUDY_COUNT,
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 9 THEN 1 END) AS INTER_COUNT,
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 10 THEN 1 END) AS FOLLOW_COUNT,
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 11 THEN 1 END) AS HELP_WIFE_COUNT,
        //                 COUNT(CASE WHEN RECORD.LEAVE_TYPE_SEQ = 12 THEN 1 END) AS IMPROVE_COUNT
        //             FROM HRM.TB_HRM_PNR_PERSONNEL PERSONNEL
        //             LEFT OUTER JOIN HRM.TB_MAS_TITLE TITLE
        //             ON PERSONNEL.PSN_TITLE_SEQ = TITLE.TITLE_SEQ
        //             LEFT OUTER JOIN HRM.TB_HRM_MAS_POSITION POSITION
        //             ON PERSONNEL.PSN_SEQ = POSITION.PSN_SEQ
        //             LEFT OUTER JOIN HRM.TB_HRM_FUR_LEAVE_RECORD RECORD
        //             ON PERSONNEL.PSN_SEQ = RECORD.PSN_SEQ
        //             WHERE POSITION.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )"
        //             . $adm_id_sql
        //             . $adm_firstname_sql
        //             . $adm_lastname_sql
        //             . $year_sql
        //           . " GROUP BY PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, TITLE.TITLE_NAME, PERSONNEL.PSN_FNAME,PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR
        //             ORDER BY PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR";

        //     $stid = oci_parse($conn, $select);
        //     if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        //     if ($year != '') oci_bind_by_name($stid, ':year', $year);
        //     if ($adm_id != '') oci_bind_by_name($stid, ':adm_id', $adm_id);
        //     if ($adm_firstname != '') oci_bind_by_name($stid, ':adm_firstname', $adm_firstname);
        //     if ($adm_lastname != '') oci_bind_by_name($stid, ':adm_lastname', $adm_lastname);

        //     oci_execute($stid);
        //     break;
}
