<?php
header('Content-Type: application/json; charset=utf-8');
//require_once 'isXmlHttpRequest.php';
include '../database/conn.php';



switch ($check) {
    case '1':
        $select = "SELECT P1.RECV_DOC_SEQ AS RECV_DOC_SEQ_P1, P2.RECV_DOC_SEQ AS RECV_DOC_SEQ_P2,
                            P1.REGISTER_NO AS REGISTER_NO_P1, P2.REGISTER_NO AS REGISTER_NO_P2,
                            P1.DOC_NUMBER AS DOC_NUMBER_P1, P2.DOC_NUMBER AS DOC_NUMBER_P2,
                            CASE WHEN SUBSTR(P1.DOC_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P1.DOC_DATE) ELSE TO_CHAR(P1.DOC_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS DOC_DATE_P1,
                            CASE WHEN SUBSTR(P2.DOC_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P2.DOC_DATE) ELSE TO_CHAR(P2.DOC_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS DOC_DATE_P2,
                            CASE WHEN SUBSTR(P1.RECEIVE_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P1.RECEIVE_DATE) ELSE TO_CHAR(P1.RECEIVE_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECEIVE_DATE_P1,
                            CASE WHEN SUBSTR(P2.RECEIVE_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P2.RECEIVE_DATE) ELSE TO_CHAR(P2.RECEIVE_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECEIVE_DATE_P2,
                            P1.DOC_FROM_TEXT AS DOC_FROM_TEXT_P1, P2.DOC_FROM_TEXT AS DOC_FROM_TEXT_P2,
                            P1.DOC_TO AS DOC_TO_P1, P2.DOC_TO AS DOC_TO_P2,
                            P1.SUBJECT AS SUBJECT_P1, P2.SUBJECT AS SUBJECT_P2
                    FROM CTN.TB_CTN_RECV_DOC P2
                    FULL OUTER JOIN  MGT1.TB_CTN_RECV_DOC P1
                    ON P2.RECV_DOC_SEQ = P1.RECV_DOC_SEQ
                        WHERE P2.LANDOFFICE_SEQ = :landoffice AND P2.RECORD_STATUS = 'N' 
                    AND P2.DOC_SECRET_SEQ= '1' AND (P1.RECV_DOC_SEQ IS NULL OR P2.RECV_DOC_SEQ IS NULL)
                ORDER BY P1.DOC_DATE, P1.REGISTER_NO, P2.DOC_DATE, P2.REGISTER_NO";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($register_no != '') oci_bind_by_name($stid, ':register_no', $register_no);
        if ($doc_no != '') oci_bind_by_name($stid, ':doc_no', $doc_no);
        if ($doc_date != '') oci_bind_by_name($stid, ':doc_date', $doc_date);

        oci_execute($stid);
        break;
    case '2':
        $secret_type_sql = $secret_type == '' ? " AND P2.DOC_SECRET_SEQ IN('2','3','4') " : " AND P2.DOC_SECRET_SEQ = :secret_type ";

        $select = "SELECT P1.RECV_DOC_SEQ AS RECV_DOC_SEQ_P1, P2.RECV_DOC_SEQ AS RECV_DOC_SEQ_P2,
                            P1.REGISTER_NO AS REGISTER_NO_P1, P2.REGISTER_NO AS REGISTER_NO_P2,
                            P1.DOC_NUMBER AS DOC_NUMBER_P1, P2.DOC_NUMBER AS DOC_NUMBER_P2,
                            CASE WHEN SUBSTR(P1.DOC_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P1.DOC_DATE) ELSE TO_CHAR(P1.DOC_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS DOC_DATE_P1,
                            CASE WHEN SUBSTR(P2.DOC_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P2.DOC_DATE) ELSE TO_CHAR(P2.DOC_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS DOC_DATE_P2,
                            CASE WHEN SUBSTR(P1.RECEIVE_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P1.RECEIVE_DATE) ELSE TO_CHAR(P1.RECEIVE_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECEIVE_DATE_P1,
                            CASE WHEN SUBSTR(P2.RECEIVE_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P2.RECEIVE_DATE) ELSE TO_CHAR(P2.RECEIVE_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECEIVE_DATE_P2,
                            P1.DOC_FROM_TEXT AS DOC_FROM_TEXT_P1, P2.DOC_FROM_TEXT AS DOC_FROM_TEXT_P2,
                            P1.DOC_TO AS DOC_TO_P1, P2.DOC_TO AS DOC_TO_P2,
                                P1.SUBJECT AS SUBJECT_P1, P2.SUBJECT AS SUBJECT_P2
                    FROM CTN.TB_CTN_RECV_DOC P2
                    FULL OUTER JOIN  MGT1.TB_CTN_RECV_DOC P1
                    ON P2.RECV_DOC_SEQ = P1.RECV_DOC_SEQ
                    WHERE P2.LANDOFFICE_SEQ = :landoffice AND P2.RECORD_STATUS = 'N'  AND (P1.RECV_DOC_SEQ IS NULL OR P2.RECV_DOC_SEQ IS NULL)
                     ORDER BY P1.DOC_DATE, P1.REGISTER_NO, P2.DOC_DATE, P2.REGISTER_NO";


        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($secret_type != '') oci_bind_by_name($stid, ':secret_type', $secret_type);
        if ($register_no != '') oci_bind_by_name($stid, ':register_no', $register_no);
        if ($doc_no != '') oci_bind_by_name($stid, ':doc_no', $doc_no);
        if ($doc_date != '') oci_bind_by_name($stid, ':doc_date', $doc_date);

        oci_execute($stid);
        break;
    case '3':
        $select = "SELECT P1.SEND_DOC_SEQ AS SEND_DOC_SEQ_P1, P2.SEND_DOC_SEQ AS SEND_DOC_SEQ_P2,
                        P1.REGISTER_NO AS REGISTER_NO_P1, P2.REGISTER_NO AS REGISTER_NO_P2,
                        P1.DOC_NUMBER AS DOC_NUMBER_P1, P2.DOC_NUMBER AS DOC_NUMBER_P2,
                        CASE WHEN SUBSTR(P1.DOC_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.DOC_DATE) ELSE TO_CHAR(P1.DOC_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS DOC_DATE_P1,
                        CASE WHEN SUBSTR(P2.DOC_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.DOC_DATE) ELSE TO_CHAR(P2.DOC_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS DOC_DATE_P2,
                        CASE WHEN SUBSTR(P1.SEND_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.SEND_DATE) ELSE TO_CHAR(P1.SEND_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS SEND_DATE_P1,
                        CASE WHEN SUBSTR(P2.SEND_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.SEND_DATE) ELSE TO_CHAR(P2.SEND_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS SEND_DATE_P2,
                        P1.DOC_FROM_TEXT AS DOC_FROM_TEXT_P1, P2.DOC_FROM_TEXT AS DOC_FROM_TEXT_P2,
                        P1.SUBJECT AS SUBJECT_P1, P2.SUBJECT AS SUBJECT_P2
                    FROM CTN.TB_CTN_SEND_DOC P2
                    FULL OUTER JOIN  MGT1.TB_CTN_SEND_DOC P1
                    ON P2.SEND_DOC_SEQ = P1.SEND_DOC_SEQ
                    WHERE P2.LANDOFFICE_SEQ = :landoffice  AND P2.RECORD_STATUS = 'N' 
                    AND P2.DOC_SECRET_SEQ='1' AND P2.DOC_SECRET_SEQ='1' AND (P1.SEND_DOC_SEQ IS NULL OR P2.SEND_DOC_SEQ IS NULL)
                    ORDER BY P1.DOC_DATE, P1.REGISTER_NO, P2.DOC_DATE, P2.REGISTER_NO";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($register_no != '') oci_bind_by_name($stid, ':register_no', $register_no);
        if ($doc_no != '') oci_bind_by_name($stid, ':doc_no', $doc_no);
        if ($doc_date != '') oci_bind_by_name($stid, ':doc_date', $doc_date);

        oci_execute($stid);
        break;
    case '4':
        $secret_type_sql = $secret_type == '' ? " AND P2.DOC_SECRET_SEQ IN('2','3','4') " : " AND P2.DOC_SECRET_SEQ = :secret_type ";

        $select = "SELECT P1.SEND_DOC_SEQ AS SEND_DOC_SEQ_P1, P2.SEND_DOC_SEQ AS SEND_DOC_SEQ_P2,
                        P1.REGISTER_NO AS REGISTER_NO_P1, P2.REGISTER_NO AS REGISTER_NO_P2,
                        P1.DOC_NUMBER AS DOC_NUMBER_P1, P2.DOC_NUMBER AS DOC_NUMBER_P2,
                        CASE WHEN SUBSTR(P1.DOC_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.DOC_DATE) ELSE TO_CHAR(P1.DOC_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS DOC_DATE_P1,
                        CASE WHEN SUBSTR(P2.DOC_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.DOC_DATE) ELSE TO_CHAR(P2.DOC_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS DOC_DATE_P2,
                        CASE WHEN SUBSTR(P1.SEND_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.SEND_DATE) ELSE TO_CHAR(P1.SEND_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS SEND_DATE_P1,
                        CASE WHEN SUBSTR(P2.SEND_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.SEND_DATE) ELSE TO_CHAR(P2.SEND_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS SEND_DATE_P2,
                        P1.DOC_FROM_TEXT AS DOC_FROM_TEXT_P1, P2.DOC_FROM_TEXT AS DOC_FROM_TEXT_P2,
                        P1.SUBJECT AS SUBJECT_P1, P2.SUBJECT AS SUBJECT_P2
                    FROM CTN.TB_CTN_SEND_DOC P2
                    FULL OUTER JOIN  MGT1.TB_CTN_SEND_DOC P1
                    ON P2.SEND_DOC_SEQ = P1.SEND_DOC_SEQ
                    WHERE P2.LANDOFFICE_SEQ = :landoffice  AND P2.RECORD_STATUS = 'N'  AND (P1.SEND_DOC_SEQ IS NULL OR P2.SEND_DOC_SEQ IS NULL)
                    ORDER BY P1.DOC_DATE, P1.REGISTER_NO, P2.DOC_DATE, P2.REGISTER_NO";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($secret_type != '') oci_bind_by_name($stid, ':secret_type', $secret_type);
        if ($register_no != '') oci_bind_by_name($stid, ':register_no', $register_no);
        if ($doc_no != '') oci_bind_by_name($stid, ':doc_no', $doc_no);
        if ($doc_date != '') oci_bind_by_name($stid, ':doc_date', $doc_date);

        oci_execute($stid);
        break;
    case '5':
        $ctn_recv_doc_no_sql = str_replace("DOC_NUMBER", "PROV_NUMBER", $ctn_recv_doc_no_sql);

        $select = "WITH P1 AS (
                        SELECT *
                            FROM MGT1.TB_CTN_PROVINCE_NUMBER
                            WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                                AND SECRET = '1'
                    ),
                    P2 AS (
                        SELECT *
                        FROM CTN.TB_CTN_PROVINCE_NUMBER
                        WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                                AND SECRET = '1'
                    )
                    SELECT P1.PROV_NUM_SEQ AS PROV_NUM_SEQ_P1, P2.PROV_NUM_SEQ AS PROV_NUM_SEQ_P2,
                    P1.YEAR AS YEAR_P1, P2.YEAR AS YEAR_P2,
                    P1.PROV_NUMBER AS PROV_NUMBER_P1, P2.PROV_NUMBER AS PROV_NUMBER_P2,
                    P1.STATUS AS STATUS_P1,P2.STATUS AS STATUS_P2
                    FROM P1
                    INNER JOIN P2
                    ON P1.PROV_NUM_SEQ = P2.PROV_NUM_SEQ 
                    WHERE  P1.PROV_NUM_SEQ IS NULL OR P2.PROV_NUM_SEQ IS NULL
                     ORDER BY P1.PROV_NUM_SEQ,P2.PROV_NUM_SEQ";


        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($year != '') oci_bind_by_name($stid, ':year', $year);
        if ($doc_no != '') oci_bind_by_name($stid, ':doc_no', $doc_no);

        oci_execute($stid);
        break;
    case '6':
        $ctn_recv_doc_no_sql = str_replace("DOC_NUMBER", "PROV_NUMBER", $ctn_recv_doc_no_sql);

        $select = "WITH P1 AS (
                        SELECT *
                            FROM MGT1.TB_CTN_PROVINCE_NUMBER
                            WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                                AND SECRET = '2'
                    ),
                    P2 AS (
                        SELECT *
                        FROM CTN.TB_CTN_PROVINCE_NUMBER
                        WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N' 
                                AND SECRET = '2'
                    )
                    SELECT P1.PROV_NUM_SEQ AS PROV_NUM_SEQ_P1, P2.PROV_NUM_SEQ AS PROV_NUM_SEQ_P2,
                    P1.YEAR AS YEAR_P1, P2.YEAR AS YEAR_P2,
                    P1.PROV_NUMBER AS PROV_NUMBER_P1, P2.PROV_NUMBER AS PROV_NUMBER_P2,
                    P1.STATUS AS STATUS_P1,P2.STATUS AS STATUS_P2
                    FROM P1
                    INNER JOIN P2
                    ON  P1.PROV_NUM_SEQ = P2.PROV_NUM_SEQ 
                     ORDER BY P1.PROV_NUM_SEQ,P2.PROV_NUM_SEQ";


        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($year != '') oci_bind_by_name($stid, ':year', $year);
        if ($doc_no != '') oci_bind_by_name($stid, ':doc_no', $doc_no);

        oci_execute($stid);
        break;

    case '7':
        if ($order_no == '' && $doc_date != '')  $ctn_recv_doc_date_sql = str_replace("AND", "WHERE", $ctn_recv_doc_date_sql);

        $select = "WITH P1 AS (
                        SELECT *
                        FROM MGT1.TB_CTN_DOCUMENT
                                WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS ='N'
                    ),
                    P2 AS (
                        SELECT *
                        FROM CTN.TB_CTN_DOCUMENT
                                WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS ='N'
                    )
                    SELECT P1.DOCUMENT_SEQ AS DOCUMENT_SEQ_P1, P2.DOCUMENT_SEQ AS DOCUMENT_SEQ_P2,
                    P1.ORDER_NO AS ORDER_NO_P1, P2.ORDER_NO AS ORDER_NO_P2,
                    CASE WHEN SUBSTR(P1.DOC_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P1.DOC_DATE) ELSE TO_CHAR(P1.DOC_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS DOC_DATE_P1,
                    CASE WHEN SUBSTR(P2.DOC_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P2.DOC_DATE) ELSE TO_CHAR(P2.DOC_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS DOC_DATE_P2,
                    P1.DOC_FROM AS DOC_FROM_P1, P2.DOC_FROM AS DOC_FROM_P2,
                    P1.DOC_TO AS DOC_TO_P1, P2.DOC_TO AS DOC_TO_P2,
                    P1.SUBJECT AS SUBJECT_P1, P2.SUBJECT AS SUBJECT_P2
                    FROM P1
                    FULL OUTER JOIN P2
                    ON  P1.DOCUMENT_SEQ = P2.DOCUMENT_SEQ
                    WHERE P1.DOCUMENT_SEQ IS NULL OR P2.DOCUMENT_SEQ IS NULL
                     ORDER BY P1.DOC_DATE DESC,P2.DOC_DATE DESC";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($order_no != '') oci_bind_by_name($stid, ':order_no', $order_no);
        if ($doc_date != '') oci_bind_by_name($stid, ':doc_date', $doc_date);

        oci_execute($stid);
        break;

    case '8':
        $con_type_sql_p1 = $con_type == '' ? '' : " AND T2.BLD_SEQ = :con_type ";
        $con_type_sql_p2 = $con_type == '' ? '' : " AND T4.BLD_SEQ = :con_type ";

        $con_id_sql = $con_id == '' ? '' : " WHERE NVL(P2.REV_BLD_NO,P1.REV_BLD_NO)  LIKE '%' || :con_id || '%'";
        $con_name_sql = $con_name == '' ? '' : " WHERE NVL(P2.REV_BLD_NAME,P1.REV_BLD_NAME)  LIKE '%' || :con_name || '%'";

        if ($con_id != '' && $con_name != '') $con_name_sql = str_replace("WHERE", "AND", $con_name_sql);

        $select = "WITH P1 AS (
                    SELECT * FROM MGT1.TB_INV_REV_HD  T1
                        LEFT OUTER JOIN MGT1.TB_INV_REV_BLD_DTL T2
                        ON T1.REV_HD_SEQ =  T2.REV_HD_SEQ
                        LEFT OUTER JOIN MGT1.TB_INV_MAS_BLD MAS_BLD
                        ON T2.BLD_SEQ =  MAS_BLD.BLD_SEQ
                        WHERE REV_HD_TYPE = 11 AND LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'
                    ),
                    P2 AS (
                        SELECT * FROM INV.TB_INV_REV_HD T3
                        LEFT OUTER JOIN INV.TB_INV_REV_BLD_DTL T4
                        ON T3.REV_HD_SEQ = T4.REV_HD_SEQ
                        LEFT OUTER JOIN INV.TB_INV_MAS_BLD MAS_BLD
                        ON T4.BLD_SEQ =  MAS_BLD.BLD_SEQ
                        WHERE REV_HD_TYPE = 11 AND LANDOFFICE_SEQ = :landoffice AND T3.RECORD_STATUS = 'N'
                    )
                    SELECT 
                        P1.BLD_NAME AS BLD_NAME_P1, P2.BLD_NAME AS BLD_NAME_P2,
                        P1.REV_BLD_NO AS REV_BLD_NO_P1, P2.REV_BLD_NO AS REV_BLD_NO_P2,
                        P1.REV_BLD_NAME AS REV_BLD_NAME_P1, P2.REV_BLD_NAME AS REV_BLD_NAME_P2,
                        CASE WHEN SUBSTR(P1.REV_BLD_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.REV_BLD_DATE) ELSE TO_CHAR(P1.REV_BLD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REV_BLD_DATE_P1,
                        CASE WHEN SUBSTR(P2.REV_BLD_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.REV_BLD_DATE) ELSE TO_CHAR(P2.REV_BLD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REV_BLD_DATE_P2
                    FROM P1
                    INNER JOIN P2
                    ON P1.REV_BLD_NO = P2.REV_BLD_NO
                    WHERE P1.REV_BLD_NO IS NULL OR P2.REV_BLD_NO IS NULL";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($con_type != '') oci_bind_by_name($stid, ':con_type', $con_type);
        if ($con_id != '') oci_bind_by_name($stid, ':con_id', $con_id);
        if ($con_name != '') oci_bind_by_name($stid, ':con_name', $con_name);
        oci_execute($stid);
        break;
        // ครุภัณฑ์
    case '9':
        $inv_type_sql = $inv_type == '' ? '' : " AND TOPIC_ASSET.TOPIC_ASSET_SEQ = :inv_type ";
        $inv_type_sql = $inv_type == 'null' ? ' AND TOPIC_ASSET.TOPIC_ASSET_SEQ IS NULL ' : $inv_type_sql;

        $inv_id_sql = $inv_id == '' ? '' : " WHERE NVL(P2.STK_ASS_MID_ID,P1.STK_ASS_MID_ID)  LIKE '%' || :inv_id || '%'";
        $inv_name_sql = $inv_name == '' ? '' : " WHERE NVL(P2.STK_ASS_NAME,P1.STK_ASS_NAME)  LIKE '%' || :inv_name || '%'";

        if ($inv_id != '' && $inv_name != '') $inv_name_sql = str_replace("WHERE", "AND", $inv_name_sql);

        $select = "WITH P1 AS (
                SELECT STK_ASS.*, MET_REV.MET_REV_NAME, CAT_ASSET.CAT_ASSET_NAME, TOPIC_ASSET.TOPIC_ASSET_NAME
                FROM  MGT1.TB_INV_STK_ASS STK_ASS
                    LEFT OUTER JOIN MGT1.TB_INV_STK_ASS_SUB STK_ASS_SUB
                    ON STK_ASS.STK_ASS_SEQ = STK_ASS_SUB.STK_ASS_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_CAT_ASSET CAT_ASSET
                    ON STK_ASS.CAT_ASSET_SEQ = CAT_ASSET.CAT_ASSET_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_TYPE_ASSET TYPE_ASSET
                    ON CAT_ASSET.TYPE_ASSET_SEQ = TYPE_ASSET.TYPE_ASSET_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_TOPIC_ASSET TOPIC_ASSET
                    ON TYPE_ASSET.TOPIC_ASSET_SEQ = TOPIC_ASSET.TOPIC_ASSET_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_MET_REV MET_REV
                    ON STK_ASS.MET_REV_SEQ = MET_REV.MET_REV_SEQ
                WHERE STK_ASS.LANDOFFICE_SEQ = :landoffice AND STK_ASS.RECORD_STATUS ='N'
            ),
            P2 AS (
                SELECT STK_ASS.*, MET_REV.MET_REV_NAME, CAT_ASSET.CAT_ASSET_NAME, TOPIC_ASSET.TOPIC_ASSET_NAME
                FROM  INV.TB_INV_STK_ASS STK_ASS
                    LEFT OUTER JOIN INV.TB_INV_STK_ASS_SUB STK_ASS_SUB
                    ON STK_ASS.STK_ASS_SEQ = STK_ASS_SUB.STK_ASS_SEQ
                    LEFT OUTER JOIN INV.TB_INV_MAS_CAT_ASSET CAT_ASSET
                    ON STK_ASS.CAT_ASSET_SEQ = CAT_ASSET.CAT_ASSET_SEQ
                    LEFT OUTER JOIN INV.TB_INV_MAS_TYPE_ASSET TYPE_ASSET
                    ON CAT_ASSET.TYPE_ASSET_SEQ = TYPE_ASSET.TYPE_ASSET_SEQ
                    LEFT OUTER JOIN INV.TB_INV_MAS_TOPIC_ASSET TOPIC_ASSET
                    ON TYPE_ASSET.TOPIC_ASSET_SEQ = TOPIC_ASSET.TOPIC_ASSET_SEQ
                    LEFT OUTER JOIN INV.TB_INV_MAS_MET_REV MET_REV
                    ON STK_ASS.MET_REV_SEQ = MET_REV.MET_REV_SEQ
                WHERE STK_ASS.LANDOFFICE_SEQ = :landoffice AND STK_ASS.RECORD_STATUS ='N'
            )
            SELECT P1.TOPIC_ASSET_NAME AS TOPIC_ASSET_NAME_P1, P2.TOPIC_ASSET_NAME AS TOPIC_ASSET_NAME_P2,
            P1.STK_ASS_NAME AS STK_ASS_NAME_P1, P2.STK_ASS_NAME AS STK_ASS_NAME_P2,
            P1.STK_ASS_MID_ID AS STK_ASS_MID_ID_P1, P2.STK_ASS_MID_ID AS STK_ASS_MID_ID_P2,
            CASE WHEN SUBSTR(P1.STK_ASS_REV_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P1.STK_ASS_REV_DATE) ELSE TO_CHAR(P1.STK_ASS_REV_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS STK_ASS_REV_DATE_P1,
            CASE WHEN SUBSTR(P2.STK_ASS_REV_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P2.STK_ASS_REV_DATE) ELSE TO_CHAR(P2.STK_ASS_REV_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS STK_ASS_REV_DATE_P2,
            P1.MET_REV_NAME AS MET_REV_NAME_P1, P2.MET_REV_NAME AS MET_REV_NAME_P2
            FROM P1
            INNER JOIN P2
            ON  P1.STK_ASS_SEQ = P2.STK_ASS_SEQ "
            . $inv_id_sql
            . $inv_name_sql;

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($inv_type != '') oci_bind_by_name($stid, ':inv_type', $inv_type);
        if ($inv_id != '') oci_bind_by_name($stid, ':inv_id', $inv_id);
        if ($inv_name != '') oci_bind_by_name($stid, ':inv_name', $inv_name);


        oci_execute($stid);
        break;
    case '10':
        $docfin_type_sql_p1 = $docfin_type == '' ? '' : " AND T2.PLATE_SEQ = :docfin_type ";
        $docfin_type_sql_p2 = $docfin_type == '' ? '' : " AND T4.PLATE_SEQ = :docfin_type ";

        $docfin_no_sql = $docfin_no == '' ? '' : " WHERE NVL(P2.STK_PLATE_NO,P1.STK_PLATE_NO)  LIKE '%' || :docfin_no || '%'";

        $select = "WITH P1 AS (
            SELECT T1.REV_PLATE_SEQ, T1.REV_PLATE_QTY, T2.PLATE_NAME, T3.UNIT_NAME, HD.REV_HD_SEQ, HD.REV_HD_DATE
            FROM MGT1.TB_INV_REV_PLATE_DTL T1
                LEFT OUTER JOIN MGT1.TB_INV_MAS_PLATE T2
                ON T1.PLATE_SEQ = T2.PLATE_SEQ
                LEFT OUTER JOIN MGT1.TB_INV_MAS_UNIT T3
                ON T2.UNIT_SEQ = T3.UNIT_SEQ
                LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                WHERE LANDOFFICE_SEQ = :landoffice AND T2.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ = 2 AND HD.RECORD_STATUS = 'N'
            ),
            P2 AS (
            SELECT T3.REV_PLATE_SEQ, T3.REV_PLATE_QTY, T4.PLATE_NAME, T5.UNIT_NAME, HD.REV_HD_SEQ, HD.REV_HD_DATE
            FROM INV.TB_INV_REV_PLATE_DTL T3
                LEFT OUTER JOIN INV.TB_INV_MAS_PLATE T4
                ON T3.PLATE_SEQ = T4.PLATE_SEQ
                LEFT OUTER JOIN INV.TB_INV_MAS_UNIT T5
                ON T4.UNIT_SEQ = T5.UNIT_SEQ
                LEFT OUTER JOIN INV.TB_INV_REV_HD HD
                ON T3.REV_HD_SEQ = HD.REV_HD_SEQ
                WHERE LANDOFFICE_SEQ = :landoffice AND T4.RECORD_STATUS = 'N' AND T4.PLATE_TYPE_SEQ = 2 AND HD.RECORD_STATUS = 'N'
            )
            SELECT P1.REV_PLATE_SEQ AS REV_PLATE_SEQ_P1, P2.REV_PLATE_SEQ AS REV_PLATE_SEQ_P2,
            P1.REV_HD_SEQ AS REV_HD_SEQ_P1, P2.REV_HD_SEQ AS REV_HD_SEQ_P2,
            CASE WHEN SUBSTR(P1.REV_HD_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P1.REV_HD_DATE) ELSE TO_CHAR(P1.REV_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REV_HD_DATE_P1,
            CASE WHEN SUBSTR(P2.REV_HD_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P2.REV_HD_DATE) ELSE TO_CHAR(P2.REV_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REV_HD_DATE_P2,
            P1.PLATE_NAME AS PLATE_NAME_P1, P2.PLATE_NAME AS PLATE_NAME_P2,
            P1.UNIT_NAME AS UNIT_NAME_P1, P2.UNIT_NAME AS UNIT_NAME_P2,
            P1.REV_PLATE_QTY AS REV_PLATE_QTY_P1, P2.REV_PLATE_QTY AS REV_PLATE_QTY_P2
            FROM P1
            FULL OUTER JOIN P2
            ON P1.REV_PLATE_SEQ = P2.REV_PLATE_SEQ
            WHERE P1.REV_PLATE_SEQ IS NULL OR P2.REV_PLATE_SEQ IS NULL";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($docfin_type != '') oci_bind_by_name($stid, ':docfin_type', $docfin_type);
        if ($docfin_no != '') oci_bind_by_name($stid, ':docfin_no', $docfin_no);

        oci_execute($stid);
        break;
    case '11':
        $docfin_type_sql_p1 = $docfin_type == '' ? '' : " AND T2.PLATE_SEQ = :docfin_type ";
        $docfin_type_sql_p2 = $docfin_type == '' ? '' : " AND T4.PLATE_SEQ = :docfin_type ";

        $docfin_no_sql = $docfin_no == '' ? '' : " WHERE NVL(P2.STK_PLATE_NO,P1.STK_PLATE_NO)  LIKE '%' || :docfin_no || '%'";

        $select = "WITH P1 AS (
                        SELECT T1.REV_PLATE_SEQ, T1.REV_PLATE_QTY, T2.PLATE_NAME, T3.UNIT_NAME, HD.REV_HD_SEQ, HD.REV_HD_DATE
                        FROM MGT1.TB_INV_REV_PLATE_DTL T1
                            LEFT OUTER JOIN MGT1.TB_INV_MAS_PLATE T2
                            ON T1.PLATE_SEQ = T2.PLATE_SEQ
                            LEFT OUTER JOIN MGT1.TB_INV_MAS_UNIT T3
                            ON T2.UNIT_SEQ = T3.UNIT_SEQ
                            LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                            ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                            WHERE LANDOFFICE_SEQ = :landoffice AND T2.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ IN (3,4) AND HD.RECORD_STATUS = 'N'
                        ),
                        P2 AS (
                        SELECT T3.REV_PLATE_SEQ, T3.REV_PLATE_QTY, T4.PLATE_NAME, T5.UNIT_NAME, HD.REV_HD_SEQ, HD.REV_HD_DATE
                        FROM INV.TB_INV_REV_PLATE_DTL T3
                            LEFT OUTER JOIN INV.TB_INV_MAS_PLATE T4
                            ON T3.PLATE_SEQ = T4.PLATE_SEQ
                            LEFT OUTER JOIN INV.TB_INV_MAS_UNIT T5
                            ON T4.UNIT_SEQ = T5.UNIT_SEQ
                            LEFT OUTER JOIN INV.TB_INV_REV_HD HD
                            ON T3.REV_HD_SEQ = HD.REV_HD_SEQ
                            WHERE LANDOFFICE_SEQ = :landoffice AND T4.RECORD_STATUS = 'N' AND T4.PLATE_TYPE_SEQ IN (3,4) AND HD.RECORD_STATUS = 'N'
                        )
                        SELECT P1.REV_PLATE_SEQ AS REV_PLATE_SEQ_P1, P2.REV_PLATE_SEQ AS REV_PLATE_SEQ_P2,
                        P1.REV_HD_SEQ AS REV_HD_SEQ_P1, P2.REV_HD_SEQ AS REV_HD_SEQ_P2,
                        CASE WHEN SUBSTR(P1.REV_HD_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.REV_HD_DATE) ELSE TO_CHAR(P1.REV_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REV_HD_DATE_P1,
                        CASE WHEN SUBSTR(P2.REV_HD_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.REV_HD_DATE) ELSE TO_CHAR(P2.REV_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REV_HD_DATE_P2,
                        P1.PLATE_NAME AS PLATE_NAME_P1, P2.PLATE_NAME AS PLATE_NAME_P2,
                        P1.UNIT_NAME AS UNIT_NAME_P1, P2.UNIT_NAME AS UNIT_NAME_P2,
                        P1.REV_PLATE_QTY AS REV_PLATE_QTY_P1, P2.REV_PLATE_QTY AS REV_PLATE_QTY_P2
                        FROM P1
                        FULL OUTER JOIN P2
                        ON P1.REV_PLATE_SEQ = P2.REV_PLATE_SEQ
                        WHERE P1.REV_PLATE_SEQ IS NULL OR P2.REV_PLATE_SEQ IS NULL";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($docfin_type != '') oci_bind_by_name($stid, ':docfin_type', $docfin_type);
        if ($docfin_no != '') oci_bind_by_name($stid, ':docfin_no', $docfin_no);

        oci_execute($stid);
        break;
    case '12':
        $docfin_type_sql_p1 = $docfin_type == '' ? '' : " AND T2.PLATE_SEQ = :docfin_type ";
        $docfin_type_sql_p2 = $docfin_type == '' ? '' : " AND T4.PLATE_SEQ = :docfin_type ";

        $docfin_no_sql = $docfin_no == '' ? '' : " WHERE NVL(P2.STK_PLATE_NO,P1.STK_PLATE_NO)  LIKE '%' || :docfin_no || '%'";

        $select = "WITH P1 AS (
                        SELECT T1.REV_PLATE_SEQ, T1.REV_PLATE_QTY, T2.PLATE_NAME, T3.UNIT_NAME, HD.REV_HD_SEQ, HD.REV_HD_DATE
                        FROM MGT1.TB_INV_REV_PLATE_DTL T1
                            LEFT OUTER JOIN MGT1.TB_INV_MAS_PLATE T2
                            ON T1.PLATE_SEQ = T2.PLATE_SEQ
                            LEFT OUTER JOIN MGT1.TB_INV_MAS_UNIT T3
                            ON T2.UNIT_SEQ = T3.UNIT_SEQ
                            LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                            ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                            WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND T2.PLATE_TYPE_SEQ = 1 AND HD.RECORD_STATUS = 'N'
                        ),
                        P2 AS (
                        SELECT T3.REV_PLATE_SEQ, T3.REV_PLATE_QTY, T4.PLATE_NAME, T5.UNIT_NAME, HD.REV_HD_SEQ, HD.REV_HD_DATE
                        FROM INV.TB_INV_REV_PLATE_DTL T3
                            LEFT OUTER JOIN INV.TB_INV_MAS_PLATE T4
                            ON T3.PLATE_SEQ = T4.PLATE_SEQ
                            LEFT OUTER JOIN INV.TB_INV_MAS_UNIT T5
                            ON T4.UNIT_SEQ = T5.UNIT_SEQ
                            LEFT OUTER JOIN INV.TB_INV_REV_HD HD
                            ON T3.REV_HD_SEQ = HD.REV_HD_SEQ
                            WHERE LANDOFFICE_SEQ = :landoffice AND T4.RECORD_STATUS = 'N' AND T4.PLATE_TYPE_SEQ = 1 AND HD.RECORD_STATUS = 'N'
                        )
                        SELECT P1.REV_PLATE_SEQ AS REV_PLATE_SEQ_P1, P2.REV_PLATE_SEQ AS REV_PLATE_SEQ_P2,
                        P1.REV_HD_SEQ AS REV_HD_SEQ_P1, P2.REV_HD_SEQ AS REV_HD_SEQ_P2,
                        CASE WHEN SUBSTR(P1.REV_HD_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.REV_HD_DATE) ELSE TO_CHAR(P1.REV_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REV_HD_DATE_P1,
                        CASE WHEN SUBSTR(P2.REV_HD_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.REV_HD_DATE) ELSE TO_CHAR(P2.REV_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REV_HD_DATE_P2,
                        P1.PLATE_NAME AS PLATE_NAME_P1, P2.PLATE_NAME AS PLATE_NAME_P2,
                        P1.UNIT_NAME AS UNIT_NAME_P1, P2.UNIT_NAME AS UNIT_NAME_P2,
                        P1.REV_PLATE_QTY AS REV_PLATE_QTY_P1, P2.REV_PLATE_QTY AS REV_PLATE_QTY_P2
            --                    P1.STK_PLATE_NO AS STK_PLATE_NO_P1, P2.STK_PLATE_NO AS STK_PLATE_NO_P2
                        FROM P1
                        FULL OUTER JOIN P2
                        ON P1.REV_PLATE_SEQ = P2.REV_PLATE_SEQ
                        WHERE P1.REV_PLATE_SEQ IS NULL OR P2.REV_PLATE_SEQ IS NULL";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($docfin_type != '') oci_bind_by_name($stid, ':docfin_type', $docfin_type);
        if ($docfin_no != '') oci_bind_by_name($stid, ':docfin_no', $docfin_no);

        oci_execute($stid);
        break;

    case '13':
        $mat_type_sql = $mat_type == '' ? '' : " AND STK_MAT.MAT_SEQ = :mat_type ";

        $year_sql = $year == '' ? '' : " WHERE NVL(P2.STK_MAT_YEAR,P1.STK_MAT_YEAR) = :year";

        $select = "WITH P1 AS (
            SELECT STK_MAT.*, MAS_MAT.MAT_NAME
                FROM MGT1.TB_INV_STK_MAT STK_MAT
                LEFT OUTER JOIN MGT1.TB_INV_MAS_MAT MAS_MAT
                ON STK_MAT.MAT_SEQ = MAS_MAT.MAT_SEQ
                    WHERE STK_MAT.LANDOFFICE_SEQ = :landoffice AND STK_MAT.RECORD_STATUS = 'N'" . $mat_type_sql . "
            ),
            P2 AS (
            SELECT STK_MAT.*, MAS_MAT.MAT_NAME
                FROM INV.TB_INV_STK_MAT STK_MAT
                LEFT OUTER JOIN INV.TB_INV_MAS_MAT MAS_MAT
                ON STK_MAT.MAT_SEQ = MAS_MAT.MAT_SEQ
                    WHERE STK_MAT.LANDOFFICE_SEQ = :landoffice AND STK_MAT.RECORD_STATUS = 'N'" . $mat_type_sql . "
            )
            SELECT P1.STK_MAT_SEQ AS STK_MAT_SEQ_P1, P2.STK_MAT_SEQ AS STK_MAT_SEQ_P2,
            P1.MAT_NAME AS MAT_NAME_P1, P2.MAT_NAME AS MAT_NAME_P2,
            P1.STK_MAT_YEAR AS STK_MAT_YEAR_P1, P2.STK_MAT_YEAR AS STK_MAT_YEAR_P2,
            P1.STK_MAT_BLN_QTY AS STK_MAT_BLN_QTY_P1, P2.STK_MAT_BLN_QTY AS STK_MAT_BLN_QTY_P2
            FROM P1
            FULL OUTER JOIN P2
            ON P1.STK_MAT_SEQ = P2.STK_MAT_SEQ
            WHERE P1.STK_MAT_SEQ IS NULL OR P2.STK_MAT_SEQ IS NULL"
            . $year_sql;

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($mat_type != '') oci_bind_by_name($stid, ':mat_type', $mat_type);
        if ($year != '') oci_bind_by_name($stid, ':year', $year);


        oci_execute($stid);
        break;

    case '14':
        $dis_type_sql = $dis_type == '' ? '' : " AND NVL(P2.PIECE_SEQ,P1.PIECE_SEQ) = :dis_type ";
        $year_sql = $year == '' ? '' : " WHERE NVL(P2.REV_HD_YEAR,P1.REV_HD_YEAR) = :year";

        $select = "WITH COUNTROW_P1 AS (
                    SELECT T1.REV_PIECE_SEQ, T1.PIECE_SEQ, T1.REV_PIECE_QTY, T2.PIECE_NAME, MAS_UNIT.UNIT_NAME, HD.REV_HD_SEQ, HD.REV_HD_YEAR, HD.REV_HD_DATE  FROM MGT1.TB_INV_REV_PIECE_DTL T1
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_PIECE T2
                    ON T1.PIECE_SEQ =  T2.PIECE_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_MAS_UNIT MAS_UNIT
                    ON T2.UNIT_SEQ = MAS_UNIT.UNIT_SEQ
                    LEFT OUTER JOIN MGT1.TB_INV_REV_HD HD
                    ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                    WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND HD.RECORD_STATUS = 'N'
                ),
                COUNTROW_P2 AS (
                SELECT T1.REV_PIECE_SEQ, T1.PIECE_SEQ, T1.REV_PIECE_QTY, T2.PIECE_NAME, MAS_UNIT.UNIT_NAME, HD.REV_HD_SEQ, HD.REV_HD_YEAR, HD.REV_HD_DATE FROM INV.TB_INV_REV_PIECE_DTL T1
                    LEFT OUTER JOIN INV.TB_INV_MAS_PIECE T2
                    ON T1.PIECE_SEQ =  T2.PIECE_SEQ
                    LEFT OUTER JOIN INV.TB_INV_MAS_UNIT MAS_UNIT
                    ON T2.UNIT_SEQ = MAS_UNIT.UNIT_SEQ
                    LEFT OUTER JOIN INV.TB_INV_REV_HD HD
                    ON T1.REV_HD_SEQ = HD.REV_HD_SEQ
                    WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' AND HD.RECORD_STATUS = 'N'
                )
                SELECT P1.REV_PIECE_SEQ AS REV_PIECE_SEQ_P1, P2.REV_PIECE_SEQ AS REV_PIECE_SEQ_P2,
                        P1.REV_HD_SEQ AS REV_HD_SEQ_P1, P2.REV_HD_SEQ AS REV_HD_SEQ_P2,
                        P1.PIECE_NAME AS PIECE_NAME_P1, P2.PIECE_NAME AS PIECE_NAME_P2,
                        CASE WHEN SUBSTR(P1.REV_HD_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.REV_HD_DATE) ELSE TO_CHAR(P1.REV_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REV_HD_DATE_P1,
                        CASE WHEN SUBSTR(P2.REV_HD_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.REV_HD_DATE) ELSE TO_CHAR(P2.REV_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REV_HD_DATE_P2,
                        P1.REV_HD_YEAR AS REV_HD_YEAR_P1, P2.REV_HD_YEAR AS REV_HD_YEAR_P2,
                        P1.UNIT_NAME AS UNIT_NAME_P1, P2.UNIT_NAME AS UNIT_NAME_P2,
                        P1.REV_PIECE_QTY AS REV_PIECE_QTY_P1, P2.REV_PIECE_QTY AS REV_PIECE_QTY_P2
                FROM COUNTROW_P1 P1
                FULL OUTER JOIN COUNTROW_P2 P2
                ON  P1.REV_PIECE_SEQ = P2.REV_PIECE_SEQ
                WHERE P1.REV_PIECE_SEQ IS NULL OR P2.REV_PIECE_SEQ IS NULL";
                


        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($dis_type != '') oci_bind_by_name($stid, ':dis_type', $dis_type);
        if ($year != '') oci_bind_by_name($stid, ':year', $year);

        oci_execute($stid);
        break;

    case '15':
        $landoffice_type_sql = $landoffice_type == '' ? '' : " AND T1.PSN_TYPE_SEQ = :landoffice_type";

        $adm_id_sql = $adm_id == '' ? '' : " WHERE NVL(P2.PSN_PID, P1.PSN_PID) = :adm_id ";
        $adm_firstname_sql = $adm_firstname == '' ? '' : " WHERE NVL(P2.PSN_FNAME, P1.PSN_FNAME) LIKE '%' || :adm_firstname || '%' ";
        $adm_lastname_sql = $adm_lastname == '' ? '' : " WHERE NVL(P2.PSN_LNAME, P1.PSN_LNAME) LIKE '%' || :adm_lastname || '%' ";

        if ($adm_id != '' && $adm_firstname != '')  $adm_firstname_sql = str_replace("WHERE", "AND", $adm_firstname_sql);
        if ($adm_id != '' && $adm_lastname != '')  $adm_lastname_sql = str_replace("WHERE", "AND", $adm_lastname_sql);
        if ($adm_id == '' && $adm_firstname != '' && $adm_lastname != '') $adm_lastname_sql = str_replace("WHERE", "AND", $adm_lastname_sql);


        $select = "WITH P1 AS (
                            SELECT T1.PSN_SEQ, T1.PSN_PID, TITLE.TITLE_NAME, T1.PSN_FNAME, T1.PSN_LNAME, T1.PSN_TYPE_SEQ, T2.POSITION_NO, DIV_POSITION.DIV_POSITION_NAME, PSNTYPE.PSN_TYPE_NAME, POS_LEVEL.POSITION_LEVEL_NAME, LANDTYPE.LANDOFFICE_TYPE_NAME
                            FROM MGT1.TB_HRM_PNR_PERSONNEL T1
                            LEFT OUTER JOIN MGT1.TB_HRM_MAS_POSITION T2
                            ON T1.PSN_SEQ  =  T2.PSN_SEQ 
                            LEFT OUTER JOIN MGT1.TB_MAS_TITLE TITLE
                            ON T1.PSN_TITLE_SEQ = TITLE.TITLE_SEQ
                            LEFT OUTER JOIN MGT1.TB_HRM_MAS_PSNTYPE PSNTYPE
                            ON T1.PSN_TYPE_SEQ = PSNTYPE.PSN_TYPE_SEQ
                            LEFT OUTER JOIN MGT1.TB_HRM_MAS_DIV_POSITION DIV_POSITION
                            ON T2.DIV_POSITION_SEQ = DIV_POSITION.DIV_POSITION_SEQ
                            LEFT OUTER JOIN MGT1.TB_HRM_MAS_POS_LEVEL POS_LEVEL
                            ON T2.POSITION_LEVEL_SEQ = POS_LEVEL.POSITION_LEVEL_SEQ
                            LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE LANDOFFICE
                            ON T2.LANDOFFICE_SEQ = LANDOFFICE.LANDOFFICE_SEQ
                            LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE_TYPE LANDTYPE
                            ON LANDOFFICE.LANDOFFICE_TYPE_SEQ = LANDTYPE.LANDOFFICE_TYPE_SEQ
                            WHERE T2.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )
                            AND T1.RECORD_STATUS = 'N' AND T2.RECORD_STATUS = 'N'" . $landoffice_type_sql . "
                        ),
                        P2 AS (
                            SELECT T1.PSN_SEQ, T1.PSN_PID, TITLE.TITLE_NAME, T1.PSN_FNAME, T1.PSN_LNAME, T1.PSN_TYPE_SEQ, T2.POSITION_NO, DIV_POSITION.DIV_POSITION_NAME, PSNTYPE.PSN_TYPE_NAME, POS_LEVEL.POSITION_LEVEL_NAME, LANDTYPE.LANDOFFICE_TYPE_NAME
                            FROM HRM.TB_HRM_PNR_PERSONNEL T1
                            LEFT OUTER JOIN HRM.TB_HRM_MAS_POSITION T2
                            ON T1.PSN_SEQ  =  T2.PSN_SEQ 
                            LEFT OUTER JOIN MAS.TB_MAS_TITLE TITLE
                            ON T1.PSN_TITLE_SEQ = TITLE.TITLE_SEQ
                            LEFT OUTER JOIN HRM.TB_HRM_MAS_PSNTYPE PSNTYPE
                            ON T1.PSN_TYPE_SEQ = PSNTYPE.PSN_TYPE_SEQ
                            LEFT OUTER JOIN HRM.TB_HRM_MAS_DIV_POSITION DIV_POSITION
                            ON T2.DIV_POSITION_SEQ = DIV_POSITION.DIV_POSITION_SEQ
                            LEFT OUTER JOIN HRM.TB_HRM_MAS_POS_LEVEL POS_LEVEL
                            ON T2.POSITION_LEVEL_SEQ = POS_LEVEL.POSITION_LEVEL_SEQ
                            LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE LANDOFFICE
                            ON T2.LANDOFFICE_SEQ = LANDOFFICE.LANDOFFICE_SEQ
                            LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE_TYPE LANDTYPE
                            ON LANDOFFICE.LANDOFFICE_TYPE_SEQ = LANDTYPE.LANDOFFICE_TYPE_SEQ
                            WHERE T2.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )
                            AND T1.RECORD_STATUS = 'N' AND T2.RECORD_STATUS = 'N'" . $landoffice_type_sql . "
                        )
                        SELECT P1.PSN_SEQ AS PSN_SEQ_P1, P2.PSN_SEQ AS PSN_SEQ_P2,
                        P1.PSN_PID AS PSN_PID_P1, P2.PSN_PID AS PSN_PID_P2,
                        P1.TITLE_NAME AS TITLE_NAME_P1, P2.TITLE_NAME AS TITLE_NAME_P2,
                        NVL(P1.PSN_FNAME,'-')||' '||NVL(P1.PSN_LNAME,'-') AS NAME_P1,
                        NVL(P2.PSN_FNAME,'-')||' '||NVL(P2.PSN_LNAME,'-') AS NAME_P2,
                        P1.POSITION_NO AS POSITION_NO_P1, P2.POSITION_NO AS POSITION_NO_P2,
                        P1.DIV_POSITION_NAME AS DIV_POSITION_NAME_P1, P2.DIV_POSITION_NAME AS DIV_POSITION_NAME_P2,
                        P1.POSITION_LEVEL_NAME AS POSITION_LEVEL_NAME_P1, P2.POSITION_LEVEL_NAME AS POSITION_LEVEL_NAME_P2,
                        P1.LANDOFFICE_TYPE_NAME AS LANDOFFICE_TYPE_NAME_P1, P2.LANDOFFICE_TYPE_NAME AS LANDOFFICE_TYPE_NAME_P2,
                        P1.PSN_TYPE_NAME AS PSN_TYPE_NAME_P1, P2.PSN_TYPE_NAME AS PSN_TYPE_NAME_P2
                        FROM P1
                        FULL OUTER JOIN P2
                        ON P1.PSN_SEQ = P2.PSN_SEQ
                        WHERE P1.PSN_SEQ IS NULL OR P2.PSN_SEQ IS NULL"
            . $adm_id_sql
            . $adm_firstname_sql
            . $adm_lastname_sql;

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($landoffice_type != '') oci_bind_by_name($stid, ':landoffice_type', $landoffice_type);
        if ($adm_id != '') oci_bind_by_name($stid, ':adm_id', $adm_id);
        if ($adm_firstname != '') oci_bind_by_name($stid, ':adm_firstname', $adm_firstname);
        if ($adm_lastname != '') oci_bind_by_name($stid, ':adm_lastname', $adm_lastname);
        oci_execute($stid);
        break;

    case '16':
        $adm_id_sql = $adm_id == '' ? '' : " WHERE NVL(P2.PSN_PID, P1.PSN_PID) = :adm_id ";
        $adm_firstname_sql = $adm_firstname == '' ? '' : " WHERE NVL(P2.PSN_FNAME, P1.PSN_FNAME) LIKE '%' || :adm_firstname || '%' ";
        $adm_lastname_sql = $adm_lastname == '' ? '' : " WHERE NVL(P2.PSN_LNAME, P1.PSN_LNAME) LIKE '%' || :adm_lastname || '%' ";
        $year_sql = $year == '' ? '' : " WHERE NVL(P2.FISCAL_YEAR,P1.FISCAL_YEAR) = :year";


        $select = "WITH COUNTROW_P1 AS (
                        SELECT RECORD.LEAVE_RECORD_SEQ, PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME,RECORD.FISCAL_YEAR, LEAVE_TYPE.LEAVE_TYPE_SEQ, LEAVE_TYPE.LEAVE_TYPE_NAME, RECORD.LEAVE_FROM_DATE, RECORD.LEAVE_TO_DATE, RECORD.TOTAL_DAY
                        FROM MGT1.TB_HRM_PNR_PERSONNEL PERSONNEL
                        LEFT OUTER JOIN MGT1.TB_HRM_MAS_POSITION POSITION
                        ON PERSONNEL.PSN_SEQ = POSITION.PSN_SEQ
                        LEFT OUTER JOIN MGT1.TB_HRM_FUR_LEAVE_RECORD RECORD
                        ON PERSONNEL.PSN_SEQ = RECORD.PSN_SEQ
                        LEFT OUTER JOIN MGT1.TB_HRM_MAS_LEAVE_TYPE LEAVE_TYPE
                        ON RECORD.LEAVE_TYPE_SEQ = LEAVE_TYPE.LEAVE_TYPE_SEQ
                        WHERE POSITION.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )
                        AND PERSONNEL.PSN_PID = :adm_id
                        ORDER BY PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME,RECORD.FISCAL_YEAR,RECORD.LEAVE_FROM_DATE
                    ),
                    COUNTROW_P2 AS (
                        SELECT RECORD.LEAVE_RECORD_SEQ, PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME,RECORD.FISCAL_YEAR, LEAVE_TYPE.LEAVE_TYPE_SEQ, LEAVE_TYPE.LEAVE_TYPE_NAME, RECORD.LEAVE_FROM_DATE, RECORD.LEAVE_TO_DATE, RECORD.TOTAL_DAY
                        FROM HRM.TB_HRM_PNR_PERSONNEL PERSONNEL
                        LEFT OUTER JOIN HRM.TB_HRM_MAS_POSITION POSITION
                        ON PERSONNEL.PSN_SEQ = POSITION.PSN_SEQ
                        LEFT OUTER JOIN HRM.TB_HRM_FUR_LEAVE_RECORD RECORD
                        ON PERSONNEL.PSN_SEQ = RECORD.PSN_SEQ
                        LEFT OUTER JOIN HRM.TB_HRM_MAS_LEAVE_TYPE LEAVE_TYPE
                        ON RECORD.LEAVE_TYPE_SEQ = LEAVE_TYPE.LEAVE_TYPE_SEQ
                        WHERE POSITION.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )
                        AND PERSONNEL.PSN_PID = :adm_id
                        ORDER BY PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME,RECORD.FISCAL_YEAR,RECORD.LEAVE_FROM_DATE
                    )
                    SELECT P1.LEAVE_RECORD_SEQ AS LEAVE_RECORD_SEQ_P1, P2.LEAVE_RECORD_SEQ AS LEAVE_RECORD_SEQ_P2, 
                    P1.PSN_SEQ AS PSN_SEQ_P1, P2.PSN_SEQ AS PSN_SEQ_P2, 
                    P1.PSN_PID AS PSN_PID_P1, P2.PSN_PID AS PSN_PID_P2, 
                    P1.PSN_FNAME AS PSN_FNAME_P1, P2.PSN_FNAME AS PSN_FNAME_P2, 
                    P1.PSN_LNAME AS PSN_LNAME_P1, P2.PSN_LNAME AS PSN_LNAME_P2, 
                    P1.FISCAL_YEAR AS FISCAL_YEAR_P1, P2.FISCAL_YEAR AS FISCAL_YEAR_P2, 
                    P1.LEAVE_TYPE_SEQ AS LEAVE_TYPE_SEQ_P1, P2.LEAVE_TYPE_SEQ AS LEAVE_TYPE_SEQ_P2, 
                    P1.LEAVE_TYPE_NAME AS LEAVE_TYPE_NAME_P1, P2.LEAVE_TYPE_NAME AS LEAVE_TYPE_NAME_P2, 
                    CASE WHEN SUBSTR(P1.LEAVE_FROM_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P1.LEAVE_FROM_DATE) ELSE TO_CHAR(P1.LEAVE_FROM_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LEAVE_FROM_DATE_P1,
                    CASE WHEN SUBSTR(P2.LEAVE_FROM_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P2.LEAVE_FROM_DATE) ELSE TO_CHAR(P2.LEAVE_FROM_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LEAVE_FROM_DATE_P2,
                    CASE WHEN SUBSTR(P1.LEAVE_TO_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P1.LEAVE_TO_DATE) ELSE TO_CHAR(P1.LEAVE_TO_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LEAVE_TO_DATE_P1,
                    CASE WHEN SUBSTR(P2.LEAVE_TO_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P2.LEAVE_TO_DATE) ELSE TO_CHAR(P2.LEAVE_TO_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LEAVE_TO_DATE_P2,
                    P1.TOTAL_DAY AS TOTAL_DAY_P1, P2.TOTAL_DAY AS TOTAL_DAY_P2
                    FROM COUNTROW_P1 P1
                    FULL OUTER JOIN COUNTROW_P2 P2
                    ON  P1.PSN_SEQ = P2.PSN_SEQ
                    AND P1.LEAVE_RECORD_SEQ = P2.LEAVE_RECORD_SEQ
                    AND (P1.PSN_SEQ IS NULL OR P2.PSN_SEQ IS NULL)"
            . $year_sql
            . $adm_id_sql
            . $adm_firstname_sql
            . $adm_lastname_sql;
        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($year != '') oci_bind_by_name($stid, ':year', $year);
        if ($adm_id != '') oci_bind_by_name($stid, ':adm_id', $adm_id);
        if ($adm_firstname != '') oci_bind_by_name($stid, ':adm_firstname', $adm_firstname);
        if ($adm_lastname != '') oci_bind_by_name($stid, ':adm_lastname', $adm_lastname);

        oci_execute($stid);
        break;
    case '17':
        $select = "SELECT PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, TITLE.TITLE_NAME, PERSONNEL.PSN_FNAME,PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR, RECORD.LEAVE_RECORD_SEQ
                    FROM MGT1.TB_HRM_PNR_PERSONNEL PERSONNEL
                    LEFT OUTER JOIN MGT1.TB_MAS_TITLE TITLE
                    ON PERSONNEL.PSN_TITLE_SEQ = TITLE.TITLE_SEQ
                    LEFT OUTER JOIN MGT1.TB_HRM_MAS_POSITION POSITION
                    ON PERSONNEL.PSN_SEQ = POSITION.PSN_SEQ
                    LEFT OUTER JOIN MGT1.TB_HRM_FUR_LEAVE_RECORD RECORD
                    ON PERSONNEL.PSN_SEQ = RECORD.PSN_SEQ
                    WHERE POSITION.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )
                    AND PERSONNEL.PSN_SEQ = :psn_seq AND RECORD.FISCAL_YEAR = :year
                    ORDER BY PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME, RECORD.FISCAL_YEAR";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($psn_seq != '') oci_bind_by_name($stid, ':psn_seq', $psn_seq);
        if ($year != '') oci_bind_by_name($stid, ':year', $year);

        oci_execute($stid);
        break;
    case '18': 
        $select = "WITH COUNTROW_P1 AS (
                        SELECT RECORD.LEAVE_RECORD_SEQ, PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, TITLE.TITLE_NAME, PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME,RECORD.FISCAL_YEAR, LEAVE_TYPE.LEAVE_TYPE_SEQ, LEAVE_TYPE.LEAVE_TYPE_NAME, RECORD.LEAVE_FROM_DATE, RECORD.LEAVE_TO_DATE, RECORD.TOTAL_DAY
                        FROM MGT1.TB_HRM_PNR_PERSONNEL PERSONNEL
                        LEFT OUTER JOIN MGT1.TB_MAS_TITLE TITLE
                         ON PERSONNEL.PSN_TITLE_SEQ = TITLE.TITLE_SEQ
                        LEFT OUTER JOIN MGT1.TB_HRM_MAS_POSITION POSITION
                        ON PERSONNEL.PSN_SEQ = POSITION.PSN_SEQ
                        LEFT OUTER JOIN MGT1.TB_HRM_FUR_LEAVE_RECORD RECORD
                        ON PERSONNEL.PSN_SEQ = RECORD.PSN_SEQ
                        LEFT OUTER JOIN MGT1.TB_HRM_MAS_LEAVE_TYPE LEAVE_TYPE
                        ON RECORD.LEAVE_TYPE_SEQ = LEAVE_TYPE.LEAVE_TYPE_SEQ
                        WHERE POSITION.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )
                        AND PERSONNEL.PSN_SEQ = :psn_seq AND RECORD.LEAVE_RECORD_SEQ = :record_seq
                        ORDER BY PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME,RECORD.FISCAL_YEAR,RECORD.LEAVE_FROM_DATE
                        ),
                        COUNTROW_P2 AS (
                        SELECT RECORD.LEAVE_RECORD_SEQ, PERSONNEL.PSN_SEQ, PERSONNEL.PSN_PID, TITLE.TITLE_NAME, PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME,RECORD.FISCAL_YEAR, LEAVE_TYPE.LEAVE_TYPE_SEQ, LEAVE_TYPE.LEAVE_TYPE_NAME, RECORD.LEAVE_FROM_DATE, RECORD.LEAVE_TO_DATE, RECORD.TOTAL_DAY
                        FROM HRM.TB_HRM_PNR_PERSONNEL PERSONNEL
                        LEFT OUTER JOIN MGT1.TB_MAS_TITLE TITLE
                        ON PERSONNEL.PSN_TITLE_SEQ = TITLE.TITLE_SEQ
                        LEFT OUTER JOIN HRM.TB_HRM_MAS_POSITION POSITION
                        ON PERSONNEL.PSN_SEQ = POSITION.PSN_SEQ
                        LEFT OUTER JOIN HRM.TB_HRM_FUR_LEAVE_RECORD RECORD
                        ON PERSONNEL.PSN_SEQ = RECORD.PSN_SEQ
                        LEFT OUTER JOIN HRM.TB_HRM_MAS_LEAVE_TYPE LEAVE_TYPE
                        ON RECORD.LEAVE_TYPE_SEQ = LEAVE_TYPE.LEAVE_TYPE_SEQ
                        WHERE POSITION.LANDOFFICE_SEQ IN( SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE WHERE (LANDOFFICE_SEQ= :landoffice OR (REF_LANDOFFICE_SEQ= :landoffice  and LANDOFFICE_TYPE_SEQ NOT IN (6,7,8,9)))AND RECORD_STATUS = 'N' )
                        AND PERSONNEL.PSN_SEQ = :psn_seq AND RECORD.LEAVE_RECORD_SEQ = :record_seq
                        ORDER BY PERSONNEL.PSN_FNAME, PERSONNEL.PSN_LNAME,RECORD.FISCAL_YEAR,RECORD.LEAVE_FROM_DATE
                        )
                        SELECT P1.LEAVE_RECORD_SEQ AS LEAVE_RECORD_SEQ_P1, P2.LEAVE_RECORD_SEQ AS LEAVE_RECORD_SEQ_P2, 
                        P1.PSN_SEQ AS PSN_SEQ_P1, P2.PSN_SEQ AS PSN_SEQ_P2, 
                        P1.PSN_PID AS PSN_PID_P1, P2.PSN_PID AS PSN_PID_P2, 
                        P1.TITLE_NAME AS TITLE_NAME_P1, P2.TITLE_NAME AS TITLE_NAME_P2, 
                        P1.PSN_FNAME AS PSN_FNAME_P1, P2.PSN_FNAME AS PSN_FNAME_P2, 
                        P1.PSN_LNAME AS PSN_LNAME_P1, P2.PSN_LNAME AS PSN_LNAME_P2, 
                        P1.FISCAL_YEAR AS FISCAL_YEAR_P1, P2.FISCAL_YEAR AS FISCAL_YEAR_P2, 
                        P1.LEAVE_TYPE_SEQ AS LEAVE_TYPE_SEQ_P1, P2.LEAVE_TYPE_SEQ AS LEAVE_TYPE_SEQ_P2, 
                        P1.LEAVE_TYPE_NAME AS LEAVE_TYPE_NAME_P1, P2.LEAVE_TYPE_NAME AS LEAVE_TYPE_NAME_P2, 
                        CASE WHEN SUBSTR(P1.LEAVE_FROM_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LEAVE_FROM_DATE) ELSE TO_CHAR(P1.LEAVE_FROM_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LEAVE_FROM_DATE_P1,
                        CASE WHEN SUBSTR(P2.LEAVE_FROM_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LEAVE_FROM_DATE) ELSE TO_CHAR(P2.LEAVE_FROM_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LEAVE_FROM_DATE_P2,
                        CASE WHEN SUBSTR(P1.LEAVE_TO_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LEAVE_TO_DATE) ELSE TO_CHAR(P1.LEAVE_TO_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LEAVE_TO_DATE_P1,
                        CASE WHEN SUBSTR(P2.LEAVE_TO_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LEAVE_TO_DATE) ELSE TO_CHAR(P2.LEAVE_TO_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LEAVE_TO_DATE_P2,
                        P1.TOTAL_DAY AS TOTAL_DAY_P1, P2.TOTAL_DAY AS TOTAL_DAY_P2
                        FROM COUNTROW_P1 P1
                        FULL OUTER JOIN COUNTROW_P2 P2
                        ON  P1.PSN_SEQ = P2.PSN_SEQ
                        AND P1.LEAVE_RECORD_SEQ = P2.LEAVE_RECORD_SEQ
                        AND (P1.PSN_SEQ IS NULL OR P2.PSN_SEQ IS NULL)
                        ORDER BY P1.FISCAL_YEAR, P2.FISCAL_YEAR";

        $stid = oci_parse($conn, $select);

    

        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($psn_seq != '') oci_bind_by_name($stid, ':psn_seq', $psn_seq);
        if ($record_seq != '') oci_bind_by_name($stid, ':record_seq', $record_seq);


        oci_execute($stid);
        break;
}
