<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ข้อมูลงานการเงินและบัญชี</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" />
    
    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="./style/style.css">
    <link rel="stylesheet" href="./dist/css/dash.css">
    <link rel="stylesheet" href="./plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="./plugins/bootstrap-datepicker-thai/css/datepicker.css">
    <link rel="stylesheet" href="./plugins/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./dist/DataTables/datatables.min.css" />
    <link rel="stylesheet" href="./dist/DataTables/jquery.datatables.min.css" />

    <script src="./plugins/jquery/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./style/login.css">


    <link rel="shortcut icon" href="#" />

    <style>
        select {
            border: 1px solid #ccc;
            vertical-align: top;
            min-height: 20px;
        }
    </style>

</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div>
        <img id="main-bg" src="./img/dol.png" style="position: absolute;left: 31%;opacity: 0.15;width: 55vw;z-index: -1;">
    </div>
    <div class="page-wrapper chiller-theme toggled">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper navbar-expand" style="overflow: scroll;">
            <div class="sidebar-content">
                <div class="sidebar-brand">
                    <a href="#" id="landoffice_name"></a>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">Admin</span>
                        <span class="user-role">Administrator</span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-header  -->
                <nav class="mt-auto sidebar-menu">
                    <ul class="nav-pills-main nav nav-pills nav-sidebar flex-column" style="overflow-x: hidden;" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item " role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#tab-fin-sum" class="nav-link">
                                <i class="nav-icon fa fa-book"></i>
                                <p>
                                    ภาพรวมการถ่ายโอนข้อมูล
                                    <!-- <i class="fas fa-lg fa-angle-left right"></i> -->
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview " data-widget="treeview" role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#tab-fin-1" class="nav-link">
                                <i class="nav-icon fa fa-book"></i>
                                <p>
                                    ข้อมูลสำนักงานที่ดิน
                                    <!-- <i class="fas fa-lg fa-angle-left right"></i> -->
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview " data-widget="treeview" role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#tab-fin-2" class="nav-link">
                                <i class="nav-icon fa fa-book"></i>
                                <p>
                                    ข้อมูลเชื่อมโยงศูนย์ต้นทุนเจ้าของรายได้
                                    <!-- <i class="fas fa-lg fa-angle-left right"></i> -->
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview " data-widget="treeview" role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#tab-fin-3" class="nav-link">
                                <i class="nav-icon fa fa-book"></i>
                                <p>
                                    ข้อมูลเลขบัญชีธนาคาร
                                    <!-- <i class="fas fa-lg fa-angle-left right"></i> -->
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview " data-widget="treeview" role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#tab-fin-4" class="nav-link">
                                <i class="nav-icon fa fa-book"></i>
                                <p>
                                    ข้อมูลเงินมัดจำรังวัด
                                    <!-- <i class="fas fa-lg fa-angle-left right"></i> -->
                                </p>
                            </a>
                        </li>
                        <!-- <li class="nav-item has-treeview " data-widget="treeview" role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#tab-fin-5" class="nav-link">
                                <i class="nav-icon fa fa-book"></i>
                                <p>
                                    ข้อมูลตั้งต้นเงินที่รับ
                                </p>
                            </a>
                        </li> -->
                        <li class="nav-item has-treeview " data-widget="treeview" role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#tab-fin-6" class="nav-link">
                                <i class="nav-icon fa fa-book"></i>
                                <p>
                                    ข้อมูล บ.ท.ด.59
                                    <!-- <i class="fas fa-lg fa-angle-left right"></i> -->
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview " data-widget="treeview" role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#tab-fin-7" class="nav-link">
                                <i class="nav-icon fa fa-book"></i>
                                <p>
                                    ข้อมูลใบเสร็จ
                                    <!-- <i class="fas fa-lg fa-angle-left right"></i> -->
                                </p>
                            </a>
                        </li>
                    </ul>

                </nav>
                <div class="sidebar-footer">
                    <button class="btn btn-block" id="sign-out-btn" onclick="location.href='./portal.php';">กลับสู่หน้า Portal</button>
                </div>

        </nav>
        <!-- sidebar-wrapper  -->
        <main class="page-content">
            <div class="tab-pane fade" id="tab-fin-sum">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="card" style="margin-top: 10px;">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-fin-sum">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th style="vertical-align: middle;">ลำดับ</th>
                                        <th style="vertical-align: middle;text-align: center;">ประเภทงาน</th>
                                        <th style="vertical-align: middle;">รับมอบ</th>
                                        <th style="vertical-align: middle;">ถ่ายโอนสำเร็จ</th>
                                        <th style="vertical-align: middle;">ถ่ายโอนไม่สำเร็จ</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark" id="summary">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tab-fin-1">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="card">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-fin-1">
                                    <thead>
                                    <tr style="text-align: center;">
                                            <th colspan="5" class="col-6 th-p1" >ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            <th colspan="5" class="col-6 th-p2" >ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto"></th>
                                            <th class="w-auto">ชื่อสำนักงาน</th>
                                            <th class="w-auto">รหัสหน่วยงานผู้เบิก(10หลัก)</th>
                                            <th class="w-auto">รหัสศูนย์ต้นทุน</th>   
                                            <th class="w-auto">รหัสพื้นที่(4หลัก)</th>   
                                            <th class="w-auto">ชื่อสำนักงาน</th>
                                            <th class="w-auto">รหัสหน่วยงานผู้เบิก(10หลัก)</th>
                                            <th class="w-auto">รหัสศูนย์ต้นทุน</th>   
                                            <th class="w-auto">รหัสพื้นที่(4หลัก)</th>   
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="overlay1">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-fin-2">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="card">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-fin-2">
                                    <thead>
                                    <tr style="text-align: center;">
                                            <th colspan="4" class="col-6 th-p1" >ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            <th colspan="4" class="col-6 th-p2" >ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto"></th>
                                            <th class="w-auto">รหัสศูนย์ต้นทุนเจ้าของรายได้</th>
                                            <th class="w-auto">ชื่อต้นทุนเจ้าของรายได้</th>
                                            <th class="w-auto">ประเภทรายได้</th>   
                                            <th class="w-auto">รหัสศูนย์ต้นทุนเจ้าของรายได้</th>
                                            <th class="w-auto">ชื่อต้นทุนเจ้าของรายได้</th>
                                            <th class="w-auto">ประเภทรายได้</th>    
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="overlay2">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-fin-3">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="card">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-fin-3">
                                    <thead>
                                    <tr style="text-align: center;">
                                            <th colspan="5" class="col-6 th-p1" >ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            <th colspan="5" class="col-6 th-p2" >ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto"></th>
                                            <th class="w-auto">เลขบัญชีธนาคาร</th>
                                            <th class="w-auto">ธนาคาร/สาขา</th>
                                            <th class="w-auto">ประเภทเงินฝาก</th>   
                                            <th class="w-auto">รหัสธนาคาร</th>
                                            <th class="w-auto">เลขบัญชีธนาคาร</th>
                                            <th class="w-auto">ธนาคาร/สาขา</th>
                                            <th class="w-auto">ประเภทเงินฝาก</th>   
                                            <th class="w-auto">รหัสธนาคาร</th>  
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="overlay3">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-fin-4">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="card">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-fin-4">
                                    <thead>
                                    <tr style="text-align: center;">
                                            <th colspan="5" class="col-6 th-p1" >ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            <th colspan="5" class="col-6 th-p2" >ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto"></th>
                                            <th class="w-auto">เงินมัดจำรังวัดฝากคลัง/ส่วนราชการผู้เบิก</th>
                                            <th class="w-auto">เงินมัดจำรังวัดเงินสดในสำนักงานที่ดิน</th>
                                            <th class="w-auto">เงินมัดจำรังวัดเงินสดระหว่างดำเนินการในสำนักงาน</th>   
                                            <th class="w-auto">เงินมัดจำรังวัดฝากธนาคาร</th>
                                            <th class="w-auto">เงินมัดจำรังวัดฝากคลัง/ส่วนราชการผู้เบิก</th>
                                            <th class="w-auto">เงินมัดจำรังวัดเงินสดในสำนักงานที่ดิน</th>
                                            <th class="w-auto">เงินมัดจำรังวัดเงินสดระหว่างดำเนินการในสำนักงาน</th>   
                                            <th class="w-auto">เงินมัดจำรังวัดฝากธนาคาร</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="overlay4">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-fin-5">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="card">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-fin-5">
                                    <thead>
                                    <tr style="text-align: center;">
                                            <th colspan="4" class="col-6 th-p1" >ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            <th colspan="4" class="col-6 th-p2" >ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto"></th>
                                            <th class="w-auto">ประเภท</th>
                                            <th class="w-auto">วันที่ปิดงบ</th>
                                            <th class="w-auto">จำนวนเงิน</th>   
                                            <th class="w-auto">ประเภท</th>
                                            <th class="w-auto">วันที่ปิดงบ</th>
                                            <th class="w-auto">จำนวนเงิน</th>   
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="overlay5">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-fin-6">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label for="">ปีงบประมาณ:</label>
                                        <input type="text" name="fin-btd59-year" id="fin-btd59-year" class="form-control">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label for="">เลขที่บ.ท.ด.59:</label>
                                        <input type="text" name="fin-btd59-no" id="fin-btd59-no" class="form-control"></select>
                                    </div>
                                </div>
                                <!-- <div class="col-2">
                                     <div class="form-group">
                                        <label for="">เงินคงเหลือประเภท:</label>
                                        <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="select-recate"></select>
                                    </div>
                                </div> -->
                                <div class="col-2">
                                    <div class="form-group">
                                        <label for="">วันที่ ตั้งแต่:</label>
                                        <input type="date" name="date-fin-start" id="date-fin-start" class="form-control">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label for="">ถึง:</label>
                                        <input type="date" name="date-fin-end" id="date-fin-end" class="form-control">
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-group">
                                        <label for=""></label>
                                        <button class="btn btn-default searchBtn" type="submit" id="search-fin-6">ค้นหา</button>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-group">
                                        <label for=""></label>
                                        <button class="btn btn-default clearBtn" type="submit" id="clear-fin-6" style="margin-top: 15px">ล้างข้อมูล</button>
                                    </div>
                                </div>

                                <div class="form-inline d-flex justify-content-end">
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-success" type="submit" id="export-success-fin-6" style="margin-top: 25px; margin-bottom: 5px;"><i class="fas fa-file-excel"></i> Export Success</button>
                                    </div>
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-danger" type="submit" id="export-error-fin-6" style="margin-top: 25px; margin-bottom: 5px;"><i class="fas fa-file-excel"></i>Export Error</button>
                                    </div>
                                </div>
                                <!-- <div class="col-auto">
                                    <div class="form-group">
                                        <label for=""></label>
                                        <button class="btn btn-default searchBtn" type="submit" id="export-success-fin-6" > <i class="fas fa-file-excel"></i> Export Success</button>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-group">
                                        <label for=""></label>
                                        <button class="btn btn-default searchBtn" type="submit" id="export-error-fin-6" > <i class="fas fa-file-excel"></i> Export Error</button>
                                    </div>
                                </div> -->


                            </div>
                            <div class="card">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-fin-6">
                                    <thead>
                                    <tr style="text-align: center;">
                                            <th colspan="11" class="col-6 th-p1" >ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto"></th>
                                            <th class="w-auto">เลขที่บ.ท.ด.59</th>
                                            <th class="w-auto">วันที่วาง บ.ท.ด.</th>
                                            <th class="w-auto">ผู้วางเงินมัดจำ</th>   
                                            <th class="w-auto">ประเภทคำขอ</th>
                                            <th class="w-auto">เลขที่เอกสารสิทธิ</th>
                                            <th class="w-auto">หน้าสำรวจ</th> 
                                            <th class="w-auto">ตำบล</th>
                                            <th class="w-auto">อำเภอ</th>   
                                            <th class="w-auto">เงินที่วาง</th>
                                            <th class="w-auto">เงินคงเหลือ</th>   
 
                                        </tr>
                                    </thead>
                                </table>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-fin-62">
                                    <thead>
                                    <tr style="text-align: center;">
                                            <th colspan="11" class="col-6 th-p2" >ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto"></th>
                                            <th class="w-auto">เลขที่บ.ท.ด.59</th>
                                            <th class="w-auto">วันที่วาง บ.ท.ด.</th>
                                            <th class="w-auto">ผู้วางเงินมัดจำ</th>   
                                            <th class="w-auto">ประเภทคำขอ</th>
                                            <th class="w-auto">เลขที่เอกสารสิทธิ</th>
                                            <th class="w-auto">หน้าสำรวจ</th> 
                                            <th class="w-auto">ตำบล</th>
                                            <th class="w-auto">อำเภอ</th> 
                                            <th class="w-auto">เงินที่วาง</th>
                                            <th class="w-auto">เงินคงเหลือ</th>   
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="overlay6">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-fin-7">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="form-inline d-flex justify-content-end">
                                <div class="col-auto form-group">
                                    <button class="btn btn-outline-success" type="submit" id="export-success-fin-7" style="margin-top: 0px; margin-bottom: 5px;"><i class="fas fa-file-excel"></i> Export Success</button>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-outline-danger" type="submit" id="export-error-fin-7" style="margin-top: 0px; margin-bottom: 5px;"><i class="fas fa-file-excel"></i>Export Error</button>
                                </div>
                            </div>
                            <!-- <div class="form-row">
                                <div class="form-inline d-flex justify-content-end">
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-success" type="submit" id="export-success-fin-7" style="margin-top: 0px; margin-bottom: 5px;"><i class="fas fa-file-excel"></i> Export Success</button>
                                    </div>
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-danger" type="submit" id="export-error-fin-7" style="margin-top: 0px; margin-bottom: 5px;"><i class="fas fa-file-excel"></i>Export Error</button>
                                    </div>
                                </div>
                            </div> -->
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label for="">วันที่ใบเสร็จ ตั้งแต่:</label>
                                        <input type="date" name="datercpt-fin-start" id="datercpt-fin-start" class="form-control">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label for="">ถึง:</label>
                                        <input type="date" name="datercpt-fin-end" id="datercpt-fin-end" class="form-control">
                                    </div>
                                </div>
                                <div class="col-1">
                                    <div class="form-group">
                                        <label for="">เลขที่ใบเสร็จ:</label>
                                        <input type="text" name="fin-rcpt-start" id="fin-rcpt-start" class="form-control">
                                    </div>
                                </div>
                                <div class="col-1">
                                    <div class="form-group">
                                        <label for="">ถึง:</label>
                                        <input type="text" name="fin-rcpt-end" id="fin-rcpt-end" class="form-control">
                                    </div>
                                </div>
                                <div class="col-2">
                                     <div class="form-group">
                                        <label for="">ประเภทใบเสร็จ:</label>
                                        <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="select-rcpt" >
                                            <option value="">ทั้งหมด</option>
                                            <option value="R">เงินรายได้แผ่นดิน</option>
                                            <option value="S">เงินมัดจำรังวัด</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-2">
                                     <div class="form-group">
                                        <label for="">สถานะ:</label>
                                        <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="select-sts">
                                            <option value="">ทั้งหมด</option>
                                            <option value="C">ชำระเงินแล้ว</option>
                                            <option value="D">ยกเลิกใบเสร็จ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-group">
                                        <label for=""></label>
                                        <button class="btn btn-default searchBtn" type="submit" id="search-fin-7">ค้นหา</button>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-group">
                                        <label for=""></label>
                                        <button class="btn btn-default clearBtn" type="submit" id="clear-fin-7" style="margin-top: 15px">ล้างข้อมูล</button>
                                    </div>
                                </div>

                                <!-- <div class="form-inline d-flex justify-content-end">
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-success" type="submit" id="export-success-fin-7" style="margin-top: 0px; margin-bottom: 5px;"><i class="fas fa-file-excel"></i> Export Success</button>
                                    </div>
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-danger" type="submit" id="export-error-fin-7" style="margin-top: 0px; margin-bottom: 5px;"><i class="fas fa-file-excel"></i>Export Error</button>
                                    </div>
                                </div> -->
<!-- 
                                <div class="col-auto">
                                    <div class="form-group">
                                        <label for=""></label>
                                        <button class="btn btn-default searchBtn" type="submit" id="export-success-fin-7" > <i class="fas fa-file-excel"></i> Export Success</button>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-group">
                                        <label for=""></label>
                                        <button class="btn btn-default searchBtn" type="submit" id="export-error-fin-7" > <i class="fas fa-file-excel"></i> Export Error</button>
                                    </div>
                                </div> -->


                            </div>
                            <div class="card">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-fin-71">
                                    <thead>
                                    <tr style="text-align: center;">
                                            <th colspan="11" class="col-6 th-p1" >ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto"></th>
                                            <th class="w-auto">เลขที่คิว</th>
                                            <th class="w-auto">เลขที่ใบสั่ง</th>
                                            <th class="w-auto">ประเภทคำขอ</th>   
                                            <th class="w-auto">ได้รับเงินจาก</th>
                                            <th class="w-auto">คู่สัญญา</th>
                                            <th class="w-auto">จำนวนเงิน</th> 
                                            <th class="w-auto">เลขที่ใบเสร็จ</th>
                                            <th class="w-auto">วันที่ใบเสร็จ</th>   
                                            <th class="w-auto">สถานะ</th> 
                                            <th class="w-auto"></th>
 
                                        </tr>
                                    </thead>
                                </table>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-fin-72">
                                    <thead>
                                    <tr style="text-align: center;">
                                            <th colspan="11" class="col-6 th-p2" >ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto"></th>
                                            <th class="w-auto">เลขที่คิว</th>
                                            <th class="w-auto">เลขที่ใบสั่ง</th>
                                            <th class="w-auto">ประเภทคำขอ</th>   
                                            <th class="w-auto">ได้รับเงินจาก</th>
                                            <th class="w-auto">คู่สัญญา</th>
                                            <th class="w-auto">จำนวนเงิน</th> 
                                            <th class="w-auto">เลขที่ใบเสร็จ</th>
                                            <th class="w-auto">วันที่ใบเสร็จ</th>   
                                            <th class="w-auto">สถานะ</th>  
                                            <th class="w-auto"></th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="overlay7">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>

                            <div class="tabPanel card" id="tabPanel-fin-7">
                                <ul class="nav nav-tabs nav-pills detail-pills tabPanel-fin-7">
                                    <li class="active">
                                        <a data-toggle="tab" class="nav-link active" href="#detailTab-fin-7" id="detailTabPanel-fin-7">รายละเอียดทั่วไป</a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" class="nav-link" href="#detailincomeTab-fin-7">รายได้</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div id="detailTab-fin-7" class="tab-pane fade">
                                        <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" style="width:100%;border:none;" id="table-detailTab-fin-7">
                                            <thead>
                                                <tr style="text-align: center;">
                                                    <th class="w-auto">รายการข้อมูล</th>
                                                    <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div id="detailincomeTab-fin-7" class="tab-pane fade">
                                        <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" style="width:100%;border:none;" id="table-detailincomeTab-fin-7">
                                            <thead>
                                                <tr style="text-align: center;background-color:#e8e8e8;">
                                                    <th colspan="3" style="background-color: rgb(18 160 210);color:white;" >ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    <th colspan="3" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                </tr>

                                                <tr style="text-align: center;">
                                                    <th class="w-auto">รายการข้อมูล</th>
                                                    <th class="w-auto">จำนวนเงิน</th>
                                                    <th class="w-auto">สถานะ</th>
                                                    <th class="w-auto">รายการข้อมูล</th>
                                                    <th class="w-auto">จำนวนเงิน</th>
                                                    <th class="w-auto">สถานะ</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <div class="overlay overlay-main dark" id="detail7">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            

        </main>
        <!-- page-content" -->
    </div>

    <!-- jQuery -->
    <script src="./plugins/jquery/jquery.min.js"></script>
    <!-- <script src="plugins/vendor/jquery/jquery-3.2.1.min.js"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="./dist/js/adminlte.js"></script>
    <script src="./plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="./plugins/datatables/jquery.dataTables.min.js"></script>
    
    <script src="./plugins/bootstrap-datepicker-thai/js/bootstrap-datepicker.js"></script>
    <script src="./plugins/bootstrap-datepicker-thai/js/bootstrap-datepicker-thai.js"></script>
    <script src="./plugins/bootstrap-datepicker-thai/js/locales/bootstrap-datepicker.th.js"></script>
    
    <script src="./js/select.js"></script>
    <script src="./js/fin.js"></script>
    <script src="./js/global.js"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- <script>
        $(function(){
            $("#date-fin-start").datepicker({
                language:'th-th',
                format:'dd/mm/yyyy',
                autoclose: true
            });
        });
    </script> -->

    <!-- <script>
        $(document).ready(function() {


            $(".form-row").keypress(function(event) {
                if (event.keyCode == 13) {
                    $("#search-" + type).click();
                }
            })
            $('.detail-pills a').on('click', function(e) {

                e.preventDefault()
                $('.tabPanel > .tab-content .tab-pane').css("display", "none")
                console.log(this);

                $($(this).attr("href")).show();


            })
            $('.process-pills a').on('click', function(e) {

                e.preventDefault()
                $('.tab6Panel > .tab-content .tab-pane').css("display", "none")
                console.log(this);

                $($(this).attr("href")).show();


            })
        });
    </script> -->
</body>

</html>