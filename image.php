<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ข้อมูลภาพลักษณ์เอกสารสิทธิ์/สารบบ</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="./style/style.css">
    <link rel="stylesheet" href="./dist/css/dash.css">
    <link rel="stylesheet" href="./plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="./plugins/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./dist/DataTables/datatables.min.css" />
    <link rel="stylesheet" href="./dist/DataTables/jquery.datatables.min.css" />

    <script src="./plugins/jquery/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./style/login.css">


    <link rel="shortcut icon" href="#" />

    <style>
        select {
            border: 1px solid #ccc;
            vertical-align: top;
            min-height: 20px;
        }
    </style>

</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div>
        <img id="main-bg" src="./img/dol.png" style="position: absolute;left: 31%;opacity: 0.15;width: 55vw;z-index: -1;">
    </div>
    <div class="page-wrapper chiller-theme toggled">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand">
                    <a href="#" id="landoffice_name"></a>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">Admin</span>
                        <span class="user-role">Administrator</span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-header  -->
                <div class="sidebar-menu">
                    <ul class="nav-pills-main">
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-summary" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-image"></i>
                                <span style="font-size: 17px;">ภาพรวมการถ่ายโอนข้อมูล</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-image" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-image"></i>
                                <span style="font-size: 17px;">ข้อมูลภาพลักษณ์เอกสารสิทธิ/สารบบ</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="sidebar-footer">
                <button class="btn btn-block" id="sign-out-btn" onclick="location.href='./portal.php';">กลับสู่หน้า Portal</button>
            </div>
        </nav>
        <!-- sidebar-wrapper  -->
        <main class="page-content">
            <div style="width: 100%;">
                <!-- image -->
                <div class="tab-pane fade" id="tab-image">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="form-inline">
                                <div class="col-auto">
                                    <b>ประเภทเอกสารสิทธิ:</b>
                                    <select class="form-control" id="select-printplateType-image"></select>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default searchBtn" type="submit" id="search-image" style="margin-top: 0px">ค้นหา</button>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default clearBtn" type="submit" id="clear-image" style="margin-top: 0px">ล้างข้อมูล</button>
                                </div>
                            </div>
                            <div class="form-inline" id="form-image-parcel">
                                <div class="col-auto" id='input-image-no'>
                                    <b>เลขที่เอกสารสิทธิ:</b>
                                    <input type="text" class="form-control" maxlength="8"></input>
                                </div>
                                <div class="col-auto" id='input-image-survey'>
                                    <b>หน้าสำรวจ:</b>
                                    <input type="text" class="form-control" maxlength="5"></input>
                                </div>
                                <div class="col-auto" id='input-image-hid'>
                                    <b>รหัสประจำบ้าน:</b>
                                    <input type="text" class="form-control" maxlength="20"></input>
                                </div>
                                <div class="col-auto" id='input-image-hno'>
                                    <b>บ้่านเลขที่:</b>
                                    <input type="text" class="form-control" maxlength="20"></input>
                                </div>
                                <div class="col-auto" id='input-image-year'>
                                    <b>ปีที่ออก:</b>
                                    <input type="text" class="form-control" maxlength="4" size="2"></input>
                                </div>
                                <div class="col-auto" id='input-image-moo'>
                                    <b>หมู่ที่:</b>
                                    <input type="text" class="form-control" maxlength="3" size="1"></input>
                                </div>
                                <div class="col-auto" id='input-image-condoReg'>
                                    <b>เลขทะเบียนอาคารชุด:</b>
                                    <input type="text" class="form-control" maxlength="20" size='6'></input>
                                </div>
                                <div class="col-auto" id='input-image-condoName'>
                                    <b>ชื่ออาคารชุด:</b>
                                    <input type="text" class="form-control" maxlength="200" size='40'></input>
                                </div>
                                <div class="col-auto" id='input-image-condoroom'>
                                    <b>เลขที่ห้อง:</b>
                                    <input type="text" class="form-control" maxlength="20" size='4'></input>
                                </div>
                                <div class="col-auto" id='input-image-nsl'>
                                    <b>เลขที่ น.ส.ล.:</b>
                                    <input type="text" class="form-control" maxlength="20"></input>
                                </div>
                                <div class="col-auto" id='input-image-park'>
                                    <b>ชื่อที่สาธาณประโยชน์:</b>
                                    <input type="text" class="form-control" maxlength="20"></input>
                                </div>
                            <!-- </div>

                            <div class="form-inline d-flex justify-content-around"> -->
                                <div class="col-auto">
                                    <b>อำเภอ/เขต:</b>
                                    <select class="form-control" id="select-amphur-image"></select>
                                <!-- </div>
                                <div class="col-auto"> -->
                                    <b style="margin-left: 20px;">ตำบล/แขวง:</b>
                                    <select class="form-control" id="select-tambol-image"></select>
                                </div>
                            </div>
                            <div class="card" style="margin-top: 10px;">
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-main-image">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2"></th>
                                            <th class="w-auto th-p2">อำเภอ</th>
                                            <th class="w-auto th-p2">ตำบล</th>
                                            <th class="w-auto th-p2">หมู่</th>
                                            <th class="w-auto th-p2">เอกสารสิทธิ</th>
                                            <th class="w-auto th-p2">รหัสประจำบ้าน</th>
                                            <th class="w-auto th-p2">หน้าสำรวจ</th>
                                            <th class="w-auto th-p2">เลขที่ น.ส.ล.</th>
                                            <th class="w-auto th-p2">ปีที่ออก</th>
                                            <th class="w-auto th-p2">ชื่อเอกสารสิทธิ</th>
                                            <th class="w-auto th-p2">เลขทะเบียนอาคารชุด</th>
                                            <th class="w-auto th-p2">ชื่ออาคารชุด</th>
                                            <th class="w-auto th-p2">ประเภทอาคารโรงเรือน</th>
                                            <th class="w-auto th-p2">รายละเอียด</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="main">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                            <div class="card" style="margin-top: 10px;" id="card-detail">
                            
                            <!-- <div class="text-danger text-center" style="font-size: 18px">- ภาพลักษณ์เอกสารสิทธิ/สารบบ ของ โครงการพัฒนาระบบสารสนเทศที่ดิน (ระยะที่ 1) ยังไม่สามารถเปิดดูได้ -</div> -->
                                <div class="overlay overlay-main dark" id="detail">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-detail-image">
                                            <thead>
                                                <tr style="text-align: center;">
                                                    <th class="w-auto"></th>
                                                    <th class="w-auto">ภาพลักษณ์เอกสารสิทธิ</th>
                                                    <th class="w-auto th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    <th class="w-auto th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-detail2-image">
                                            <thead>
                                                <tr style="text-align: center;">
                                                    <th class="w-auto"></th>
                                                    <th class="w-auto">วันที่</th>
                                                    <th class="w-auto">ลำดับ</th>
                                                    <th class="w-auto">ประเภทการจดทะเบียน</th>
                                                    <th class="w-auto">ภาพลักษณ์สารบบ</th>
                                                    <th class="w-auto th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    <th class="w-auto th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="col text-center">
                                        <div class="spinner-border text-success" id="loading-image"></div>
                                        <div>
                                            <img id="image-image" class="img-fluid" src="./img/image-dol.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-summary">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="card" style="margin-top: 10px;">
                                <div class="form-inline d-flex justify-content-end">
                                    <div class="col-auto">
                                        <b>ประเภทเอกสารสิทธิ:</b>
                                        <select class="form-control" id="select-printplateType-summary"></select>
                                    </div>
                                </div>
                                <div class="form-inline d-flex justify-content-end">
                                    <b>รายงานภาพลักษณ์เอกสารสิทธิ :</b>
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-success" type="submit" id="report-s-image" style="margin-top: 5px"><i class="fas fa-file-excel"></i>  Export Success</button>
                                    </div>
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-danger" type="submit" id="report-e-image" style="margin-top: 5px"><i class="fas fa-file-excel"></i>  Export Error</button>
                                    </div>
                                </div>
                                <div class="form-inline d-flex justify-content-end">
                                    <b>รายงานภาพลักษณ์สารบบ :</b>
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-success" type="submit" id="report-s-index" style="margin-top: 5px"><i class="fas fa-file-excel"></i>  Export Success</button>
                                    </div>
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-danger" type="submit" id="report-e-index" style="margin-top: 5px"><i class="fas fa-file-excel"></i>  Export Error</button>
                                    </div>
                                </div>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-summary">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th rowspan="2" class="w-auto th-p2"></th>
                                            <th rowspan="2" class="w-auto th-p2">ประเภทเอกสารสิทธิ</th>
                                            <th colspan="3" class="w-auto th-p2">การถ่ายโอนข้อมูลภาพลักษณ์เอกสารสิทธิ</th>
                                            <th colspan="3" class="w-auto th-p2">การถ่ายโอนข้อมูลภาพลักษณ์สารบบ</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2">รับมอบ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนสำเร็จ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนไม่สำเร็จ</th>
                                            <th class="w-auto th-p2">รับมอบ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนสำเร็จ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนไม่สำเร็จ</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="summary">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="text" style="display:none;" id="temp" value=""></input>
        </main>
    <!-- page-content" -->
    </div>

    <!-- jQuery -->
    <script src="./plugins/jquery/jquery.min.js"></script>
    <!-- <script src="plugins/vendor/jquery/jquery-3.2.1.min.js"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="./dist/js/adminlte.js"></script>
    <script src="./plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="./plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="./js/mas.js"></script>
    <script src="./js/image.js"></script>
    <!-- <script src="./js/select.js"></script> -->
    <script src="./js/global.js"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

</script>

    <script>
       
    </script>
</body>

</html>