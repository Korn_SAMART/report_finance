$(document).ready(function () {

    $('.nav-pills-main a').on('show.bs.tab', function () {
        $("#search-" + type).off();
        $(current_href).hide();
        var href = $(this).attr('href');
        current_href = href;
        $(current_href).show();
        type= href.split('tab-')[1];

        if(type=='backlog-exp'){
            var tableexp1 = $("#table-1-" + type).DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable" : false,
                        "targets" : 0
                    }
                    ,{"className": "dt-center", "targets": [0, 1, 2, 4, 5]}
                    ,{"className": "dt-left", "targets": [ 3]}
                ],
                "pagingType" : "simple_numbers",
                "pageLength" : 5,
                "processing" : true,
                "lengthChange": false,
                "ordering" : false,
                "searching" : false,
                "info" : true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });
    
            var tableexp2 = $("#table-2-" + type).DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable" : false,
                        "targets" : 0
                    }
                    ,{"className": "dt-center", "targets": [0, 1, 2, 4, 5]}
                    ,{"className": "dt-left", "targets": [ 3]}

                ],
                "pagingType" : "simple_numbers",
                "pageLength" : 5,
                "processing" : true,
                "lengthChange": false,
                "ordering" : false,
                "searching" : false,
                "info" : true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });

            // $('#search-' + type).on("click", function(){
            //     $(this).toggleClass("active");
            //     searchFunctionEXP();
            // }); 
            // $('#clear-' + type).on("click", function(){
            //     $(this).toggleClass("active");
            //     // clearFunction();
            // });   
        }

        

    $('#table-1-backlog-exp').on( 'page.dt', function () {
        table = $('#table-1-backlog-exp').DataTable();
        var info = table.page.info();
        // alert(info.page+1)
        table2 = $('#table-2-backlog-exp').DataTable();
        table2.page(info.page).draw('page')
    } );

    $('#table-2-backlog-exp').on( 'page.dt', function () {
        table = $('#table-2-backlog-exp').DataTable();
        var info = table.page.info();
        // alert(info.page+1)
        table2 = $('#table-1-backlog-exp').DataTable();
        table2.page(info.page).draw('page')
    } );


        

    });
});

function searchFunctionEXP(){
    data = {
        "landoffice" : landoffice,
        "start" : $('#date-start-' + type).val(),
        "end" : $('#date-end-' + type).val()
    }
    getData(data);
}

function clearFunctionEXP(){
    $('#date-start-backlog-exp').val('');
    $('#date-end-backlog-exp').val('');
}


function getData(d){
    $('#exp.overlay-main').show();
    let tableexp1 = $('#table-1-' + type).DataTable();
    let tableexp2 = $('#table-2-' + type).DataTable();
    num = 1;
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "./queries/getBacklogExp.php",
        data : d,
        success: function(data){
            tableexp1.search("").draw();
            tableexp1.clear();
            tableexp2.search("").draw();
            tableexp2.clear();
            if(!jQuery.isEmptyObject(data)){
                console.log(data);
                count = data[0].length;
                console.log(count);
                for (let i = 0; i < count; i++){
                    temp1 = [];
                    temp2 = [];
                    // console.log(data[0][i]);

                    temp1[0] = num;
                    temp1[1] = data[0][i]['DOC_RECEIVE_DATE_P1']===undefined? "-" : convertDtm(data[0][i]['DOC_RECEIVE_DATE_P1']);
                    temp1[2] = data[0][i]['DOC_RECEIVE_NO_P1']===undefined? "-" : data[0][i]['DOC_RECEIVE_NO_P1'];
                    temp1[3] = data[0][i]['JOB_NAME_P1']===undefined? "-" : data[0][i]['JOB_NAME_P1'];
                    temp1[4] = data[0][i]['JOB_STATUS_P1']===undefined? "-" : data[0][i]['JOB_STATUS_P1'];
                    temp1[5] = data[0][i]['END_JOB_DATE_P1']===undefined? "-" : convertDtm(data[0][i]['END_JOB_DATE_P1']);

                    temp2[0] = num;
                    temp2[1] = data[0][i]['DOC_RECEIVE_DATE_P2']===undefined? "-" : convertDtm(data[0][i]['DOC_RECEIVE_DATE_P2']);
                    temp2[2] = data[0][i]['DOC_RECEIVE_NO_P2']===undefined? "-" : data[0][i]['DOC_RECEIVE_NO_P2'];
                    temp2[3] = data[0][i]['JOB_NAME_P2']===undefined? "-" : data[0][i]['JOB_NAME_P2'];
                    temp2[4] = data[0][i]['JOB_STATUS_P2']===undefined? "-" : data[0][i]['JOB_STATUS_P2'];
                    temp2[5] = data[0][i]['END_JOB_DATE_P2']===undefined? "-" : convertDtm(data[0][i]['END_JOB_DATE_P2']);

                    num++;
                    tableexp1.row.add(temp1)
                    tableexp2.row.add(temp2)
   
                }
            } else {
                tableexp1.search("").draw();
                tableexp1.clear();
                tableexp2.search("").draw();
                tableexp2.clear();
            }
        tableexp1.draw();
        tableexp2.draw();
        $('#exp.overlay-main').hide();
        }
    })
}