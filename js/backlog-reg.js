$(document).ready(function () {

    $('.nav-pills-main a').on('show.bs.tab', function () {
        if(type=='backlog-reg'){
            // console.log(type);
            var table1 = $("#table-1-" + type).DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable" : false,
                        "targets" : 0
                    },
                    {"className": "dt-center", "targets": "_all"}
                ],
                "pagingType" : "simple_numbers",
                "pageLength" : 5,
                "processing" : true,
                "lengthChange": false,
                "ordering" : false,
                "searching" : false,
                "info" : true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });
    
            var table2 = $("#table-2-" + type).DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable" : false,
                        "targets" : 0
                    },
                    {"className": "dt-center", "targets": "_all"}
                ],
                "pagingType" : "simple_numbers",
                "pageLength" : 5,
                "processing" : true,
                "lengthChange": false,
                "ordering" : false,
                "searching" : false,
                "info" : true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });
        }
        $('#table-1-backlog-reg').on( 'page.dt', function () {
            table = $('#table-1-backlog-reg').DataTable();
            var info = table.page.info();
            // alert(info.page+1)
            table2 = $('#table-2-backlog-reg').DataTable();
            table2.page(info.page).draw('page')
        } );
    
        $('#table-2-backlog-reg').on( 'page.dt', function () {
            table = $('#table-2-backlog-reg').DataTable();
            var info = table.page.info();
            // alert(info.page+1)
            table2 = $('#table-1-backlog-reg').DataTable();
            table2.page(info.page).draw('page')
        } );
    });
});

function searchFunctionReg(){
    data = {
        "landoffice" : landoffice,
        "start" : $('#date-start-' + type).val(),
        "end" : $('#date-end-' + type).val(),
        "n" : 1,
        "type" : $('#select-booktype-' + type).val()
    }
    getDataReg(data);
}

function clearFunctionReg(){

}

function getDataReg(d){
    $('#reg.overlay-main').show();
    let main, par, sor;
    let p = false;
    let s = false;
    let table = $('#table-1-' + type).DataTable();
    let table2 = $('#table-2-' + type).DataTable();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getBacklogReg.php',
        data: d,
        success: function(data){
            main = data;
            // console.log(data)
            if(!jQuery.isEmptyObject(data)){
                d['n'] = 2;
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: './queries/getBacklogReg.php',
                    data: d,
                    success: function(data){
                        par = data;
                        // console.log(par)
                        p = true;
                        if(p&&s){
                            draw(main,par,sor)
                        }
                    }
                })
                d['n'] = 3;
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: './queries/getBacklogReg.php',
                    data: d,
                    success: function(data){
                        sor = data;
                        // console.log(sor)
                        s = true;
                        if(p&&s){
                            draw(main,par,sor)
                        }
                    }
                })
            } else {
                // console.log('re')
                table.search("").draw();
                table.clear();
                table2.search("").draw();
                table2.clear();
                $('#reg.overlay-main').hide();
            }
        }
    })
}

function draw(main,par,sor){
    // console.log(main);
    // console.log(par);
    // console.log(sor);
    let table = $('#table-1-' + type).DataTable();
    let table2 = $('#table-2-' + type).DataTable();
    num = 1;
    if(!jQuery.isEmptyObject(data)){
        table.search("").draw();
        table.clear();
        table2.search("").draw();
        table2.clear();
        main.forEach(dm => {
            temp = [];
            temp2 = [];
            temp[0] = num;
            temp2[0] = num;

            temp[1] = dm['MANAGE_QUEUE_DTM']===undefined? "" : convertDtm(dm['MANAGE_QUEUE_DTM']);
            temp[2] = dm['MANAGE_QUEUE_NO']===undefined? "" : dm['MANAGE_QUEUE_NO'];
            temp[3] = dm['PROCESS_REGIST_NAME']===undefined? "" : dm['PROCESS_REGIST_NAME'];
            
            temp2[1] = dm['MANAGE_QUEUE_DTM_1']===undefined? "" : convertDtm(dm['MANAGE_QUEUE_DTM_1']);
            temp2[2] = dm['MANAGE_QUEUE_NO_1']===undefined? "" : dm['MANAGE_QUEUE_NO_1'];
            temp2[3] = dm['PROCESS_REGIST_NAME_1']===undefined? "" : dm['PROCESS_REGIST_NAME_1'];

            parcel = '';
            parcel2 = '';
            par.forEach(dp => {
                if(dp['PROCESS_SEQ']==dm['PROCESS_SEQ']){
                    if(dp['PRINTPLATE_TYPE_SEQ']!=undefined){
                        printplateType.forEach(pt=>{
                            if(pt['seq']==dp['PRINTPLATE_TYPE_SEQ']) parcel += pt['name']
                        })
                        parcel += '-';
                        parcel += dp['PARCEL_NO']==undefined? "" : dp['PARCEL_NO'];
                        parcel += dp['PARCEL_LAND_NO']==undefined? "" : dp['PARCEL_LAND_NO'];
                        parcel += dp['CONDOROOM_RNO']==undefined? "" : dp['CONDOROOM_RNO'];
                        parcel += dp['CONSTRUCT_ADDR_HNO']===undefined? "" : dp['CONSTRUCT_ADDR_HNO'];
                        parcel += '<br>';                        
                    }
                    if(dp['PRINTPLATE_TYPE_SEQ_1']!=undefined){
                        printplateType.forEach(pt=>{
                            if(pt['seq']==dp['PRINTPLATE_TYPE_SEQ_1']) parcel2 += pt['name']
                        })
                        parcel2 += '-';
                        parcel2 += dp['PARCEL_NO_1']==undefined? "" : dp['PARCEL_NO_1'];
                        parcel2 += dp['PARCEL_LAND_NO_1']==undefined? "" : dp['PARCEL_LAND_NO_1'];
                        parcel2 += dp['CONDOROOM_RNO_1']==undefined? "" : dp['CONDOROOM_RNO_1'];
                        parcel2 += dp['CONSTRUCT_ADDR_HNO_1']===undefined? "" : dp['CONSTRUCT_ADDR_HNO_1'];
                        parcel2 += '<br>';                        
                    }
                }
            })
            temp[4] = parcel;
            temp2[4] = parcel2;

            promisor = '';
            promisor2 = '';
            sor.forEach(ds => {
                if(ds['PROCESS_SEQ']==dm['PROCESS_SEQ']){
                    promisor += fullname(ds['TITLE_NAME'],ds['PROCESS_PROMISOR_TEMP_FNAME'],ds['PROCESS_PROMISOR_TEMP_MNAME'],ds['PROCESS_PROMISOR_TEMP_LNAME']);
                    promisor += '<br>';
                    promisor2 += fullname(ds['TITLE_NAME_1'],ds['PROCESS_PROMISOR_TEMP_FNAME_1'],ds['PROCESS_PROMISOR_TEMP_MNAME_1'],ds['PROCESS_PROMISOR_TEMP_LNAME_1']);
                    promisor2 += '<br>';
                }
            })
            temp[5] = promisor;
            temp2[5] = promisor2;

            temp[6] = dm['BOOK_ACC_INFORM_DTM']==undefined? "": convertDtm(dm['BOOK_ACC_INFORM_DTM']);
            temp[7] = dm['BOOK_STS_SEQ']==undefined? "" : dm['BOOK_STS_SEQ']==1? "ระหว่างประกาศ" : dm['BOOK_STS_SEQ']==15? "กลับจากฝ่ายรังวัด" : "";
            temp[8] = dm['REQUEST_FIXED']===undefined? "" :dm['REQUEST_FIXED']==0? "งานปกติ" : "ซ่อมงาน";
            temp2[6] = dm['BOOK_ACC_INFORM_DTM_1']==undefined? "": convertDtm(dm['BOOK_ACC_INFORM_DTM_1']);
            temp2[7] = dm['BOOK_STS_SEQ_1']==undefined? "" : dm['BOOK_STS_SEQ_1']==1? "ระหว่างประกาศ" : dm['BOOK_STS_SEQ_1']==15? "กลับจากฝ่ายรังวัด" : "";
            temp2[8] = dm['REQUEST_FIXED_1']===undefined? "" :dm['REQUEST_FIXED_1']==0? "งานปกติ" : "ซ่อมงาน";;

            num++;
            table.row.add(temp)
            table2.row.add(temp2)
        });
    } else {
        table.search("").draw();
        table.clear();
        table2.search("").draw();
        table2.clear();
    }
    table.draw();
    table2.draw();
    $('#reg.overlay-main').hide();
}