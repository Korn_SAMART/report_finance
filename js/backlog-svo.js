$(document).ready(function () {

    $('.nav-pills-main a').on('show.bs.tab', function () {
        $("#search-" + type).off();
        $(current_href).hide();
        var href = $(this).attr('href');
        current_href = href;
        $(current_href).show();
        type = href.split('tab-')[1];


        if (type == 'backlog-svo') {
            $("#table-1-" + type).DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs": [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    },
                    { "className": "dt-center", "targets": "_all" }
                ],
                "pagingType": "simple_numbers",
                "pageLength": 5,
                "processing": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "info": true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });

            $("#table-1-" + type + '-surveyor').DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs": [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    },
                    { "className": "dt-center", "targets": "_all" }
                ],
                "pagingType": "simple_numbers",
                "pageLength": 5,
                "processing": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "info": true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });

            $("#table-1-" + type + '-private').DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs": [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    },
                    { "className": "dt-center", "targets": "_all" }
                ],
                "pagingType": "simple_numbers",
                "pageLength": 5,
                "processing": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "info": true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });

            $("#table-2-" + type).DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs": [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    },
                    { "className": "dt-center", "targets": "_all" }
                ],
                "pagingType": "simple_numbers",
                "pageLength": 5,
                "processing": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "info": true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });

            $("#table-2-" + type + '-surveyor').DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs": [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    },
                    { "className": "dt-center", "targets": "_all" }
                ],
                "pagingType": "simple_numbers",
                "pageLength": 5,
                "processing": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "info": true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });

            $("#table-2-" + type + '-private').DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs": [
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    },
                    { "className": "dt-center", "targets": "_all" }
                ],
                "pagingType": "simple_numbers",
                "pageLength": 5,
                "processing": true,
                "lengthChange": false,
                "ordering": false,
                "searching": false,
                "info": true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });
        }


        $('#table-1-backlog-svo').on('page.dt', function () {
            table = $('#table-1-backlog-svo').DataTable();
            var info = table.page.info();
            // alert(info.page+1)
            table2 = $('#table-2-backlog-svo').DataTable();
            table2.page(info.page).draw('page')
        });

        $('#table-2-backlog-svo').on('page.dt', function () {
            table = $('#table-2-backlog-svo').DataTable();
            var info = table.page.info();
            // alert(info.page+1)
            table2 = $('#table-1-backlog-svo').DataTable();
            table2.page(info.page).draw('page')
        });

        $('#table-1-backlog-svo-surveyor').on('page.dt', function () {
            table = $('#table-1-backlog-svo-surveyor').DataTable();
            var info = table.page.info();
            // alert(info.page+1)
            table2 = $('#table-2-backlog-svo-surveyor').DataTable();
            table2.page(info.page).draw('page')
        });

        $('#table-2-backlog-svo-surveyor').on('page.dt', function () {
            table = $('#table-2-backlog-svo-surveyor').DataTable();
            var info = table.page.info();
            // alert(info.page+1)
            table2 = $('#table-1-backlog-svo-surveyor').DataTable();
            table2.page(info.page).draw('page')
        });

        $('#table-1-backlog-svo-private').on('page.dt', function () {
            table = $('#table-1-backlog-svo-private').DataTable();
            var info = table.page.info();
            // alert(info.page+1)
            table2 = $('#table-2-backlog-svo-private').DataTable();
            table2.page(info.page).draw('page')
        });

        $('#table-2-backlog-svo-private').on('page.dt', function () {
            table = $('#table-2-backlog-svo-private').DataTable();
            var info = table.page.info();
            // alert(info.page+1)
            table2 = $('#table-1-backlog-svo-private').DataTable();
            table2.page(info.page).draw('page')
        });

    });
});

function searchFunctionSvo() {
    data = {
        "landoffice": landoffice,
        "n": 1
    }
    getDataSvo(data);
}

function clearFunctionSvo() {

}

function getDataSvo(d) {
    $('#svo.overlay-main').show();
    let table = $('#table-1-' + type).DataTable();
    let table2 = $('#table-2-' + type).DataTable();

    let surveyor_table = $('#table-1-' + type + '-surveyor').DataTable();
    let surveyor_table2 = $('#table-2-' + type + '-surveyor').DataTable();

    let private_table = $('#table-1-' + type + '-private').DataTable();
    let private_table2 = $('#table-2-' + type + '-private').DataTable();

    num = 1;
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getBacklogSvo.php',
        data: d,
        success: function (data) {
            if (!jQuery.isEmptyObject(data)) {
                console.log(data);
                count = data[0].length;
                console.log(count);
                for (let i = 0; i < count; i++) {
                    temp1 = [];
                    temp2 = [];
                    // console.log(data[0][i]);

                    temp1[0] = num;
                    temp1[1] = data[0][i]['REQ_QUEUE_NO_P1'] === undefined ? "-" : data[0][i]['REQ_QUEUE_NO_P1'];
                    temp1[2] = data[0][i]['REQ_QUEUE_DATE_P1'] === undefined ? "-" : data[0][i]['REQ_QUEUE_DATE_P1'];
                    temp1[3] = data[0][i]['SURVEYJOB_NO_P1'] === undefined ? "-" : data[0][i]['SURVEYJOB_NO_P1'];
                    temp1[4] = data[0][i]['OWNER_REQUEST_P1'] === undefined ? "-" : data[0][i]['OWNER_REQUEST_P1'];
                    temp1[5] = data[0][i]['TYPEOFSURVEY_NAME_P1'] === undefined ? "-" : data[0][i]['TYPEOFSURVEY_NAME_P1'];
                    temp1[6] = data[0][i]['HALT_DETAIL_P1'] === undefined ? "-" : data[0][i]['HALT_DETAIL_P1'];

                    temp2[0] = num;
                    temp2[1] = data[0][i]['REQ_QUEUE_NO_P2'] === undefined ? "-" : data[0][i]['REQ_QUEUE_NO_P2'];
                    temp2[2] = data[0][i]['REQ_QUEUE_DATE_P2'] === undefined ? "-" : data[0][i]['REQ_QUEUE_DATE_P2'];
                    temp2[3] = data[0][i]['SURVEYJOB_NO_P2'] === undefined ? "-" : data[0][i]['SURVEYJOB_NO_P2'];
                    temp2[4] = data[0][i]['OWNER_REQUEST_P2'] === undefined ? "-" : data[0][i]['OWNER_REQUEST_P2'];
                    temp2[5] = data[0][i]['TYPEOFSURVEY_NAME_P2'] === undefined ? "-" : data[0][i]['TYPEOFSURVEY_NAME_P2'];
                    temp2[6] = data[0][i]['HALT_DETAIL_P2'] === undefined ? "-" : data[0][i]['HALT_DETAIL_P2'];

                    num++;
                    table.row.add(temp1)
                    table2.row.add(temp2)

                }
            } else {
                table.search("").draw();
                table.clear();
                table2.search("").draw();
                table2.clear();
            }
            d['n'] = 2;
            num = 1;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: './queries/getBacklogSvo.php',
                data: d,
                success: function (data) {
                    if (!jQuery.isEmptyObject(data)) {
                        console.log(data);
                        count = data[0].length;
                        console.log(count);
                        for (let i = 0; i < count; i++) {
                            temp1 = [];
                            temp2 = [];
                            // console.log(data[0][i]);

                            temp1[0] = num;
                            temp1[1] = data[0][i]['SURVEYJOB_NO_P1'] === undefined ? "-" : data[0][i]['SURVEYJOB_NO_P1'];
                            temp1[2] = data[0][i]['OWNER_NAME_P1'] === undefined ? "-" : data[0][i]['OWNER_NAME_P1'];
                            temp1[3] = data[0][i]['SURVEYOR_NAME_P1'] === undefined ? "-" : data[0][i]['SURVEYOR_NAME_P1'];
                            temp1[4] = data[0][i]['TYPEOFSURVEY_NAME_P1'] === undefined ? "-" : data[0][i]['TYPEOFSURVEY_NAME_P1'];
                            temp1[5] = data[0][i]['APPOINTMENT_DTM_P1'] === undefined ? "-" : data[0][i]['APPOINTMENT_DTM_P1'];
                            temp1[6] = data[0][i]['JOBSTATUS_NAME_P1'] === undefined ? "-" : data[0][i]['JOBSTATUS_NAME_P1'];

                            temp2[0] = num;
                            temp2[1] = data[0][i]['SURVEYJOB_NO_P2'] === undefined ? "-" : data[0][i]['SURVEYJOB_NO_P2'];
                            temp2[2] = data[0][i]['OWNER_NAME_P2'] === undefined ? "-" : data[0][i]['OWNER_NAME_P2'];
                            temp2[3] = data[0][i]['SURVEYOR_NAME_P2'] === undefined ? "-" : data[0][i]['SURVEYOR_NAME_P2'];
                            temp2[4] = data[0][i]['TYPEOFSURVEY_NAME_P2'] === undefined ? "-" : data[0][i]['TYPEOFSURVEY_NAME_P2'];
                            temp2[5] = data[0][i]['APPOINTMENT_DTM_P2'] === undefined ? "-" : data[0][i]['APPOINTMENT_DTM_P2'];
                            temp2[6] = data[0][i]['JOBSTATUS_NAME_P2'] === undefined ? "-" : data[0][i]['JOBSTATUS_NAME_P2'];

                            num++;
                            surveyor_table.row.add(temp1)
                            surveyor_table2.row.add(temp2)

                        }
                    } else {
                        surveyor_table.search("").draw();
                        surveyor_table.clear();
                        surveyor_table2.search("").draw();
                        surveyor_table2.clear();
                    }
                    d['n'] = 3;
                    num = 1;
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: './queries/getBacklogSvo.php',
                        data: d,
                        success: function (data) {
                            if (!jQuery.isEmptyObject(data)) {
                                console.log(data);
                                count = data[0].length;
                                console.log(count);
                                for (let i = 0; i < count; i++) {
                                    temp1 = [];
                                    temp2 = [];
                                    // console.log(data[0][i]);

                                    temp1[0] = num;
                                    temp1[1] = data[0][i]['SURVEYJOB_NO_P1'] === undefined ? "-" : data[0][i]['SURVEYJOB_NO_P1'];
                                    temp1[2] = data[0][i]['OWNER_NAME_P1'] === undefined ? "-" : data[0][i]['OWNER_NAME_P1'];
                                    temp1[3] = data[0][i]['PRIVATE_SVY_NAME_P1'] === undefined ? "-" : data[0][i]['PRIVATE_SVY_NAME_P1'];
                                    temp1[4] = data[0][i]['TYPEOFSURVEY_NAME_P1'] === undefined ? "-" : data[0][i]['TYPEOFSURVEY_NAME_P1'];
                                    temp1[5] = data[0][i]['APPOINTMENT_DTM_P1'] === undefined ? "-" : data[0][i]['APPOINTMENT_DTM_P1'];
                                    temp1[6] = data[0][i]['JOBSTATUS_NAME_P1'] === undefined ? "-" : data[0][i]['JOBSTATUS_NAME_P1'];

                                    temp2[0] = num;
                                    temp2[1] = data[0][i]['SURVEYJOB_NO_P2'] === undefined ? "-" : data[0][i]['SURVEYJOB_NO_P2'];
                                    temp2[2] = data[0][i]['OWNER_NAME_P2'] === undefined ? "-" : data[0][i]['OWNER_NAME_P2'];
                                    temp2[3] = data[0][i]['PRIVATE_SVY_NAME_P2'] === undefined ? "-" : data[0][i]['PRIVATE_SVY_NAME_P2'];
                                    temp2[4] = data[0][i]['TYPEOFSURVEY_NAME_P2'] === undefined ? "-" : data[0][i]['TYPEOFSURVEY_NAME_P2'];
                                    temp2[5] = data[0][i]['APPOINTMENT_DTM_P2'] === undefined ? "-" : data[0][i]['APPOINTMENT_DTM_P2'];
                                    temp2[6] = data[0][i]['JOBSTATUS_NAME_P2'] === undefined ? "-" : data[0][i]['JOBSTATUS_NAME_P2'];

                                    num++;
                                    private_table.row.add(temp1)
                                    private_table2.row.add(temp2)

                                }
                            } else {
                                private_table.search("").draw();
                                private_table.clear();
                                private_table2.search("").draw();
                                private_table2.clear();
                            }
                            table.draw();
                            table2.draw();
                            surveyor_table.draw();
                            surveyor_table2.draw();
                            private_table.draw();
                            private_table2.draw();
                            $('#svo.overlay-main').hide();

                        }

                    })



              

                }

            })


        }
    })
}

