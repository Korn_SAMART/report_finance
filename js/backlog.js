$(document).ready(function () {
    //redirect to login page when bypass by URL
    if (getCookie("landoffice") == "") {
        location.href = "./index.html";
    }

    $('.sidebar-menu').click(function () {
        $('ul a.nav-link').removeClass('active');
        $(this).parent().children('a').addClass('active');
    });

    $('.tab-pane').hide();
    $('.overlay-main').hide();
    $('.spinner-border').hide();

    $.fn.dataTable.ext.errMode = 'none';

    // branchName = 'สำนักงานที่ดินกรุงเทพมหานคร';
    // landoffice = 241;
    branchName = getCookie('branchName');
    landoffice = getCookie('landoffice');

    document.getElementById("landoffice_name").innerHTML = '<i class="fa fa-info-circle"></i>   ระบบตรวจสอบข้อมูลงานค้าง<br>[' + branchName + ']';

    $('select[id^="select-booktype-"').empty();
    optionPt = ""
    optionPt += '<option value="">ทุกประเภทบัญชีคุม</option>';
    bookAccType.forEach(type => {
        optionPt += '<option value="' + type.seq + '">' + type.name + '</option>';
    })
    $('select[id^="select-booktype-"').append(optionPt);


    $("#table-summary").DataTable({
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs" : [
            {
                "searchable" : false,
                "orderable" : false,
                "targets" : 0
            },
            {"className": "dt-center", "targets": "_all"}
        ],
        "processing" : true,
        "lengthChange": false,
        "ordering" : false,
        "searching" : false,
        "paging" : false,
        "info" : false,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    });

    getSummary(landoffice);

    $('.nav-pills-main a').on('show.bs.tab', function () {
        $("#search-" + type).off();
        $(current_href).hide();
        var href = $(this).attr('href');
        current_href = href;
        $(current_href).show();
        type= href.split('tab-')[1];    
        // console.log(type);
    });

});



function getSummary(landoffice){
    
}