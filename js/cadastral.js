$(document).ready(function () {
    //redirect to login page when bypass by URL
    if (getCookie("landoffice") == "") {
        location.href = "./index.html";
    }

    $('.sidebar-menu').click(function () {
        $('ul a.nav-link').removeClass('active');
        $(this).parent().children('a').addClass('active');
    });

    $('.tab-pane').hide();
    $('.overlay-main').hide();
    $('.spinner-border').hide();

    $.fn.dataTable.ext.errMode = 'none';

    // branchName = 'สำนักงานที่ดินกรุงเทพมหานคร';
    // landoffice = 241;
    branchName = getCookie('branchName');
    landoffice = getCookie('landoffice');

    document.getElementById("landoffice_name").innerHTML = '<i class="far fa-file-image"></i>   ระบบตรวจสอบข้อมูลภาพลักษณ์ต้นร่าง<br>[' + branchName + ']';

    $('select[id^="select-printplateType-"').empty();
    optionPt = ""
    printplateType.forEach(type => {
        if(type.seq!=10 && type.seq!=11 && type.seq!=13)
            optionPt += '<option value="' + type.seq + '">' + type.name + '</option>';
    })
    optionPt += '<option value="9999">ไม่สามารถแยกประเภทเอกสารได้</option>';
    $('select[id^="select-printplateType-"').append(optionPt);

    getAmphur(landoffice);
    $('select[id^="select-tambol-"').empty()
    optionTb = '<option value="">เลือกตำบล</option>';
    $('select[id^="select-tambol-"').append(optionTb);
    $('select[id^="select-tambol-"').prop('disabled', 'disabled');
    // console.log(ap);

    $("#table-summary").DataTable({
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs" : [
            {
                "searchable" : false,
                "orderable" : false,
                "targets" : 0
            },
            {"className": "dt-center", "targets": "_all"}
        ],
        "processing" : true,
        "lengthChange": false,
        "ordering" : false,
        "searching" : false,
        "paging" : false,
        "info" : false,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    });

    getSummary(landoffice);

    $('.nav-pills-main a').on('show.bs.tab', function () {
        $("#search-" + type).off();
        $(current_href).hide();
        var href = $(this).attr('href');
        current_href = href;
        $(current_href).show();
        type= href.split('tab-')[1];

        var table = $("#table-main-" + type).DataTable({
            "language": {
                "emptyTable": "ไม่มีข้อมูล"
            },
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable" : false,
                    "targets" : 0
                },
                {"className": "dt-center", "targets": "_all"}
            ],
            "pagingType" : "simple_numbers",
            "pageLength" : 10,
            "processing" : true,
            "lengthChange": false,
            "ordering" : false,
            "searching" : false,
            "info" : true,
            "drawCallback": function () {
                currentPageCount = this.api().rows({
                    page: 'current'
                }).data().length;
            }
        });

        var tableDet = $("#table-detail-" + type).DataTable({
            "language": {
                "emptyTable": "ไม่มีข้อมูล"
            },
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable" : false,
                    "targets" : 0
                },
                {"className": "dt-center", "targets": "_all"}
            ],
            "pagingType" : "simple_numbers",
            "pageLength" : 10,
            "processing" : true,
            "lengthChange": false,
            "ordering" : false,
            "searching" : false,
            "info" : true,
            "drawCallback": function () {
                currentPageCount = this.api().rows({
                    page: 'current'
                }).data().length;
            }
        });

        $('#select-printplateType-' + type).on('change', function(){
            p = this.value;
            clearFunction ()
            
        })

        $('#select-amphur-' + type).on('change', function(){
            amphur = this.value;
            if(amphur!=''){
                // $('#select-tambol-' + type).empty()
                getTambol(amphur);
                $('#select-tambol-' + type).prop('disabled', false);
            } else {
                $('#select-tambol-' + type).empty()
                optionTb = '<option value="">เลือกตำบล</option>';
                $('#select-tambol-' + type).append(optionTb);
                $('#select-tambol-' + type).prop('disabled', 'disabled');
                tambon = '';
            }
        })

        $('#search-' + type).on("click", function(){
            $(this).toggleClass("active");
            searchFunction();
        }); 
        $('#clear-' + type).on("click", function(){
            $(this).toggleClass("active");
            clearFunction();
        });   
        
        $('#table-detail-' + type + ' tbody').on('click', 'tr', function () {
            let table = $('#table-detail-' + type).DataTable();
            table.$('tr.row-click').removeClass('row-click');
            $(this).addClass('row-click');
        } );

        $('#table-main-' + type + ' tbody').on('click', 'tr', function () {
            let table = $('#table-main-' + type).DataTable();
            table.$('tr.row-click').removeClass('row-click');
            $(this).addClass('row-click');
        } );

    });
    
    $('#report-s-cadastral').on("click", function(){
        let url = '';
        url += '&type=s'
        url += '&landoffice=' + landoffice;
        url += '&branchName=' + branchName;
        url += '&printplateType=' + $('#select-printplateType-'+type+" :selected").val();
        window.open('./queries/exportCadastral2.php?' + url, true);
    })
    $('#report-e-cadastral').on("click", function(){
        let url = '';
        url += '&type=err'
        url += '&landoffice=' + landoffice;
        url += '&branchName=' + branchName;
        url += '&printplateType=' + $('#select-printplateType-'+type+" :selected").val();
        window.open('./queries/exportCadastral2.php?' + url, true);
    })
});

function searchFunction(){
    p = $('#select-printplateType-' + type).val();
    tambon = $('#select-tambol-' + type).val();

    data = {
        "landoffice" : landoffice,
        "printplateType" : p,
        "amphur" : amphur,
        "tambol" : tambon,
        "sheet" : $('#input-cadastral-sheet').val(),
        "box" : $('#input-cadastral-box').val(),
        "no" : $('#input-cadastral-no').val(),
        "num" : $('#input-cadastral-num').val()
    }
    
    // console.log(data);
    getDataImage(data);

}

function getAmphur(landoffice){
    var amphurArr = [];
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getAmphur2.php',
        data: "landoffice=" + landoffice,
        success: function (data) {
            if(!jQuery.isEmptyObject(data)){
                num = 0;
                $.each(data, function (key, val){
                    if(num > 0 && amphurArr[num-1][1]==val['AMPHUR_NAME']){
                        num--;
                        amphurArr[num][0] += ','+val['AMPHUR_SEQ'];
                    } else {
                        amphurArr.push([val['AMPHUR_SEQ'],val['AMPHUR_NAME']]);
                    }
                    num++;
                })
                $('select[id^="select-amphur-"').empty();
                optionAp = "";
                optionAp = '<option value="">เลือกอำเภอทั้งหมด</option>';
                amphurArr.forEach(element => {
                    optionAp += '<option value="' + element[0] + '">' + element[1] + '</option>';
                })
                $('select[id^="select-amphur-"').append(optionAp);
                // console.log(amphurArr)
            } else {
                $('select[id^="select-amphur-"').empty();
                optionAp = '<option value="">ไม่สามรถเลือกอำเภอได้</option>';
                $('select[id^="select-amphur-"').append(optionAp);
                $('select[id^="select-amphur-"').prop('disabled', 'disabled');
            }
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getTambol(amphur){
    var tambolArr = []
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getTambol2.php',
        data: 'amphur=' + amphur,
        success: function(data){
            // console.log(data);
            if(!jQuery.isEmptyObject(data)){
                num = 0;
                $.each(data, function (key, val){
                    if(num > 0 && tambolArr[num-1][1]==val['TAMBOL_NAME']){
                        num--;
                        tambolArr[num][0] += ','+val['TAMBOL_SEQ'];
                    } else {
                        tambolArr.push([val['TAMBOL_SEQ'],val['TAMBOL_NAME']]);
                    }
                    num++;
                })
                $('#select-tambol-' + type).empty();
                optionTb = "";
                optionTb = '<option value="">เลือกตำบลทั้งหมด</option>';
                tambolArr.forEach(element => {
                    optionTb += '<option value="' + element[0] + '">' + element[1] + '</option>';
                })
                $('#select-tambol-' + type).append(optionTb);
            } else {
                $('#select-tambol-' + type).empty();
                optionTb = '<option value="">เลือกตำบล</option>';
                $('#select-tambol-' + type).append(optionTb);
                $('#select-tambol-' + type).prop('disabled', 'disabled');
            }
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function clearFunction (){
    $('#select-amphur-' + type).val('').change();
    $('#select-tambol-' + type).val('').change();
    $('#input-cadastral-sheet').val('');
    $('#input-cadastral-box').val('');
    $('#input-cadastral-no').val('');
    $('#input-cadastral-num').val('');
}

function getDataImage(d){
    $('#main.overlay-main').show();
    $('#detail.overlay-main').show();
    $("#table-detail-" + type).DataTable().clear().draw();
    $('#image-cadastral').attr('src', "./img/image-dol.jpg");
    $('#detail.overlay-main').hide();
    var table = $("#table-main-" + type).DataTable();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "./queries/getCadastral.php",
        data: d,
        success: function(data){
            // console.log(data)
            datatemp = data
            if(!jQuery.isEmptyObject(data)){
                table.search("").draw();
                table.clear();
                num = 1
                datatemp.forEach(d => {
                    temp = [];
                    temp[0] = num;
                    temp[1] = d['SHEET']===undefined? "" : d['SHEET']+'-'+d['BOX'];
                    temp[2] = d['NO']===undefined? "" : d['NO'];
                    temp[3] = d['NUM']===undefined? "" : d['NUM'];
                    temp[4] = d['AMPHUR_NAME']===undefined? "" : d['AMPHUR_NAME'];
                    temp[5] = d['TAMBOL_NAME']===undefined? "" : d['TAMBOL_NAME'];
                    temp[6] = d['TYPEOFSURVEY_NAME']===undefined? "" : d['TYPEOFSURVEY_NAME'];
                    temp[7] = d['DTM']===undefined? "" : convertDtm(d['DTM']);
                    seq1 = d['SEQ1']===undefined? "0" : d['SEQ1'];
                    seq2 = d['SEQ2']===undefined? "0" : d['SEQ2'];
                    pt = d['PT']==9999? "" : d['PT']
                    temp[8] = '<a onClick="getDetail('+seq1+','+seq2+','+pt+')"><i  class="fa fa-info-circle fa-lg" style="color:#2ca7d6;"></i></a>';
                    table.row.add(temp);
                    num++;
                })
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#main.overlay-main').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getSummary(landoffice){
    var table = $("#table-summary").DataTable();
    $('#summary.overlay-main').show();
    $.ajax({
        type : "POST",
        dataType : "json",
        url : "./queries/getCadastralSum.php",
        data : "landoffice="+landoffice,
        success: function(data){
            datatemp = data;
            if(!jQuery.isEmptyObject(data)){
                table.search("").draw();
                table.clear();
                num = 1;
                datatemp.forEach(d => {
                    // console.log(d)
                    temp = [];
                    temp[0] = num;
                    temp[1] = d['PRINTPLATE_TYPE_NAME']===undefined? "" : d['PRINTPLATE_TYPE_NAME'];
                    temp[2] = d['RECEIVE']===undefined? "" : Number(d['RECEIVE']).toLocaleString();
                    temp[3] = d['MIGRATE_SUCCESS']===undefined? "" : Number(d['MIGRATE_SUCCESS']).toLocaleString();
                    temp[4] = d['MIGRATE_ERROR']===undefined? "" : Number(d['MIGRATE_ERROR']).toLocaleString();
                    table.row.add(temp);
                    num++;
                })
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#summary.overlay-main').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getDetail(seq1,seq2,pt){
    $('#detail.overlay-main').show();
    $('#image-cadastral').attr('src', "./img/image-dol.jpg");
    pt = pt===undefined? "" : pt;
    // console.log(seq1,seq2,pt);
    dt = {
        "seq1" : seq1,
        "seq2" : seq2,
        "printplateType" : pt
    }
    var table = $("#table-detail-" + type).DataTable();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getCadastralDetail.php',
        data: dt,
        success: function(data){
            if(!jQuery.isEmptyObject(data)){
                datatemp = data
                table.search("").draw();
                table.clear();
                num = 1;
                datatemp.forEach(d => {
                    temp = [];
                    temp[0] = num;
                    temp[1] = d['SURVEYDOCTYPE_NAME']===undefined? "" : d['SURVEYDOCTYPE_NAME']
                    
                    temp[4] = d['IMG1']===undefined? "" : d['IMG1'];
                    temp[5] = d['IMG2']===undefined? "" : d['IMG2'];
                    temp[6] = pt

                    temp[2] = d['IMG1']===undefined? '<i class="fas fa-times fa-lg" style="color:#ff0000;"></i>' : '<a onClick="getImg1($(this))"><i class="far fa-file-image fa-lg" style="color:#28a745;"></i></a>';
                    temp[3] = d['IMG2']===undefined? '<i class="fas fa-times fa-lg" style="color:#ff0000;"></i>' : '<a onClick="getImg2($(this))"><i class="far fa-file-image fa-lg" style="color:#28a745;"></i></a>';
                    table.row.add(temp);
                    num++;
                })
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#detail.overlay-main').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    
}

function getImg1(e){
    var tr = e.parent().closest('tr');
    var row = $("#table-detail-" + type).DataTable().row(tr);
    $('#loading-image.spinner-border').show();
    // console.log(row.data());
    data = {
        "plateType" : row.data()[6],
        "readOnly" : 2,
        "indexSeq" : "",
        "action" : "svo",
        "systemType" : 2,
        "landofficeSeq" : landoffice,
        "image" : row.data()['4']
    }
    $.ajax({
        url: "http://ilands.dol.go.th/evdservice/servlet/EvdImageUnCompressServlet",
        data: data,
        type: 'GET',
        xhrFields: {
            responseType: 'blob'
        },
        success (data) {
            const url = window.URL || window.webkitURL;
            const src = url.createObjectURL(data);
            $('#image-cadastral').attr('src', src);
            $('#loading-image.spinner-border').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getImg2(e){
    var tr = e.parent().closest('tr');
    var row = $("#table-detail-" + type).DataTable().row(tr);
    $('#loading-image.spinner-border').show();
    // console.log(row.data());
    data = {
        "plateType" : row.data()[6],
        "readOnly" : 2,
        "indexSeq" : "",
        "action" : "svo",
        "systemType" : 2,
        "landofficeSeq" : landoffice,
        "image" : row.data()['5']
    }
    $.ajax({
        url: "http://ilands.dol.go.th/evdservice/servlet/EvdImageUnCompressServlet",
        data: data,
        type: 'GET',
        xhrFields: {
            responseType: 'blob'
        },
        success (data) {
            const url = window.URL || window.webkitURL;
            const src = url.createObjectURL(data);
            $('#image-cadastral').attr('src', src);
            $('#loading-image.spinner-border').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}