function showDetail(e) {
    var tr = e.parent().closest('tr');
    var row = $('#table-' + type).DataTable().row(tr);

    $('.detailTab-' + type + ' li:first-child a ').click();

    console.log('#tabPanel-' + type);
    $('#tabPanel-' + type).css("visibility", "visible");
    $('#tab6Panel-' + type).css("visibility", "hidden");
    $('#tab6Panel-' + type).css("display", "none");


    /*
        row.data()[5] = พัฒฯ 1
        row.data()[6] = พัฒฯ 2
    */

    var dataUrl = 'landoffice=' + landoffice;
    if (typeSeq == 9) {
        if (row.data()[7] != "") dataUrl += '&parcelSeqP1=' + row.data()[7];
        if (row.data()[8] != "") dataUrl += '&parcelSeqP2=' + row.data()[8];
    }
    else if (typeSeq == 8) { //nsl
        if (row.data()[7] != "") dataUrl += '&parcelSeqP1=' + row.data()[7];
        if (row.data()[8] != "") dataUrl += '&parcelSeqP2=' + row.data()[8];

    }
    else if (typeSeq == 13 || typeSeq == 5 || typeSeq == 23 || typeSeq == 8) { //condo || ns3 || subNsl
        if (row.data()[6] != "") dataUrl += '&parcelSeqP1=' + row.data()[6];
        if (row.data()[7] != "") dataUrl += '&parcelSeqP2=' + row.data()[7];

    }
    else if (typeSeq == 'c') {
        if (row.data()[8] != "") dataUrl += '&parcelSeqP1=' + row.data()[8];
        if (row.data()[9] != "") dataUrl += '&parcelSeqP2=' + row.data()[9];

    }
    else {
        if (row.data()[5] != "") dataUrl += '&parcelSeqP1=' + row.data()[5];
        if (row.data()[6] != "") dataUrl += '&parcelSeqP2=' + row.data()[6];
    }

    dataUrl += '&check=' + typeSeq;



    console.log(row.data());

    $('.overlayP2').show();

    $('#tabPanel-' + type + ' > ul > li:nth-child(4) > a').removeClass('disabled')
    $('#tabPanel-' + type + ' > ul > li:nth-child(4)').css("cursor", "");

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getProcessData.php',
        data: dataUrl,
        success: function (data) {

            var dataSetP1 = [], dataSetP2 = [], keys = [];
            var numP1 = 0, numP2 = 0;
            var count = Object.keys(data[0]).length;
            console.log("ProcessData = ", data);
            if (count > 0) { //ไม่มีข้อมูลโฉนด
                for (let i = 0; i < count; i++) {
                    var key = Object.keys(data[0][i])[i];
                    temp = []

                    var DATE = data[0][i]['MANAGE_QUEUE_DTM'].split(" ");
                    var YEAR = DATE[2] != "" ? DATE[2] : DATE[3];
                    if (parseInt(YEAR) < 2500) {
                        temp[0] = data[0][i]['MANAGE_QUEUE_DTM'].replace(YEAR, parseInt(YEAR) + 543)
                    }
                    else {
                        temp[0] = data[0][i]['MANAGE_QUEUE_DTM'];
                    }



                    temp[1] = data[0][i]['MANAGE_QUEUE_NO'] ? data[0][i]['MANAGE_QUEUE_NO'] : "";
                    temp[2] = data[0][i]['PROCESS_ORDER'] ? data[0][i]['PROCESS_ORDER'] : "";
                    temp[3] = data[0][i]['PROCESS_REGIST_NAME'] ? data[0][i]['PROCESS_REGIST_NAME'] : "";
                    temp[4] = '<a id="' + "detailIcon" + i + '" onClick="showProcessDetail($(this))"><i  class="fa fa-info-circle fa-lg" style="color:#2ca7d6;"></i></a>';
                    temp[5] = data[0][i]['KEY'] ? data[0][i]['KEY'] : "";
                    temp[6] = data[0][i]['PROCESS_SEQ'] ? data[0][i]['PROCESS_SEQ'] : "";
                    temp[7] = data[0][i]['PROCESS_SEQ'] ? data[0].filter((data) => data.PROCESS_SEQ.trim() == temp[6]) : "";
                    keys.push(data[0][i]['KEY']);

                    if (data[0][i]['KEY'] == 1) {
                        dataSetP1[numP1] = temp;
                        numP1++;
                        hasKeyP1 = true
                    }
                    else if (data[0][i]['KEY'] == 2) {
                        dataSetP2[numP2] = temp;
                        numP2++;
                        hasKeyP2 = true;
                    }
                }

            } else {
                $('#tabPanel-' + type + ' > ul > li:nth-child(4) > a').addClass('disabled')
                $('#tabPanel-' + type + ' > ul > li:nth-child(4)').css("cursor", "not-allowed");
            }
            console.log("Part1Data = ", dataSetP1);
            console.log("Part2Data = ", dataSetP2);

            console.log("Key =", keys);

            // if (dataSetP1.length <= 0 && dataSetP2.length <= 0) {
            //     $('#tabPanel-' + type + ' > ul > li:nth-child(4) > a').hover(function(){
            //         // $('#tabPanel-' + type + ' > ul > li:first-child > a').addClass('disabled');
            //         $(this).addClass('disabled');
            //         $('#tabPanel-' + type + ' > ul > li:nth-child(4').css("cursor", "not-allowed");
            //     });
            // }

            var table_p1 = $('#table-p1-' + type).DataTable({
                "pageLength": 5,
                "pagingType": "simple_numbers",
                "searching": false,
                "ordering": false,
                'info': true,
                orderCellsTop: true,
                "language": {
                    "emptyTable": (((typeSeq == 'c' && row.data()[8]) || (typeSeq == 9 && row.data()[7]) || ((typeSeq == 13 || typeSeq == 5 || typeSeq == 23) && row.data()[6]) || (typeSeq <= 5 && row.data()[5]))) ? "ไม่มีข้อมูลการจดทะเบียน" : dataSetP1.length <= 0 || count < 1 ? "ไม่มีข้อมูลเอกสารสิทธินี้ในโครงการพัฒฯ 1" : "ไม่มีข้อมูล"
                },
                "columnDefs": [{
                    "className": "dt-center",
                    "targets": [4],

                }, {
                    "className": "dt-right",
                    "targets": [0, 1, 2],

                }],
                "lengthChange": false,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });
            table_p1.clear();
            table_p1.draw();
            console.log('#table-p1-' + type);

            var table_p2 = $('#table-p2-' + type).DataTable({
                "pageLength": 5,
                "pagingType": "simple_numbers",
                "searching": false,
                "ordering": false,
                orderCellsTop: true,
                "language": {
                    "emptyTable": (((typeSeq == 'c' && row.data()[9]) || (typeSeq == 9 && row.data()[8]) || ((typeSeq == 13 || typeSeq == 5 || typeSeq == 23) && row.data()[7]) || (typeSeq <= 5 && row.data()[6]))) ? "ไม่มีข้อมูลการจดทะเบียน" : dataSetP2.length <= 0 || count < 1 ? "ไม่มีข้อมูลเอกสารสิทธินี้ในโครงการพัฒฯ 2" : "ไม่มีข้อมูล"
                },
                "columnDefs": [{
                    "className": "dt-center",
                    "targets": [4],

                }, {
                    "className": "dt-right",
                    "targets": [0, 1, 2],

                }],
                'info': true,
                "lengthChange": false,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });

            table_p2.clear();
            table_p2.draw();

            table_p1.search('').draw();
            table_p1.clear();
            table_p1.rows.add(dataSetP1);
            table_p1.draw();

            table_p2.search('').draw();
            table_p2.clear();
            table_p2.rows.add(dataSetP2);
            table_p2.draw();
            $('th.dt-right').removeClass('dt-right');







        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    //=================================================== Sequester Detail ===================================================
    if (typeSeq != 13) {
        $('#tabPanel-' + type + ' > ul > li:last-child > a').removeClass('disabled');
        $('#tabPanel-' + type + ' > ul > li:last-child').css("cursor", "");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: './queries/sequesterEachParcel.php',
            data: dataUrl,
            success: function (data) {
                var num = 0, dataSet = [];
                var count = Object.keys(data[0]).length;
                if (count > 0) {
                    // console.log('SequestCount: '+data.length)
                    // var loop = dataUrl.includes("parcelSeqP1") && dataUrl.includes("parcelSeqP2") ? 2 : 1;
                    for (let j = 0; j < data[0].length; j++) {
                        var count = Object.keys(columnSequesterDetailMain).length;
                        for (let i = 0; i < count; i++) {
                            var key = Object.keys(columnSequesterDetailMain)[i];
                            temp = []
                            temp[0] = columnSequesterDetailMain[key]
                            if (key.includes('SEQUESTER_REQ_NAME')) {
                                let title, title2;
                                if (data[0][j]['SEQUESTER_REQ_TYPE'] == 1) {
                                    sequesterDept.forEach(sd => {
                                        if (data[0][j]['REQ_TITLE_SEQ'] == sd.seq) title = sd.name;
                                        if (data[0][j]['REQ_TITLE_SEQ_1'] == sd.seq) title2 = sd.name;
                                    })
                                } else {
                                    title = data[0][j]['TITLE_NAME'];
                                    title2 = data[0][j]['TITLE_NAME_1'];
                                }
                                temp[1] = fullname(title, data[0][j]['SEQUESTER_REQ_FNAME'], data[0][j]['SEQUESTER_REQ_MNAME'], data[0][j]['SEQUESTER_REQ_LNAME']);
                                temp[2] = fullname(title2, data[0][j]['SEQUESTER_REQ_FNAME_1'], data[0][j]['SEQUESTER_REQ_MNAME_1'], data[0][j]['SEQUESTER_REQ_LNAME_1']);
                            } else if (key.includes('SEQUESTER_REQ_CONDITION')) {
                                temp[1] = data[0][j][key] ? data[0][j][key] == 1 ? 'อายัดทั้งแปลง' : data[0][j][key] == 2 ? 'อายัดเฉพาะส่วน' : 'อายัดบางส่วน' : '-';
                                temp[2] = data[0][j][key + '_1'] ? data[0][j][key + '_1'] == 1 ? 'อายัดทั้งแปลง' : data[0][j][key + '_1'] == 2 ? 'อายัดเฉพาะส่วน' : 'อายัดบางส่วน' : '-';
                            } else if (key.includes('OWNER_NAME')) {
                                temp[1] = fullname(data[0][j]['TITLE_NAME_OWN'], data[0][j]['OWNER_FNAME'], data[0][j]['OWNER_MNAME'], data[0][j]['OWNER_LNAME']);
                                temp[2] = fullname(data[0][j]['TITLE_NAME_OWN_1'], data[0][j]['OWNER_FNAME_1'], data[0][j]['OWNER_MNAME_1'], data[0][j]['OWNER_LNAME_1']);
                            } else if (key.includes('_DTM')) {
                                temp[1] = data[0][j][key] ? convertDtm(data[0][j][key]) : "-";
                                temp[2] = data[0][j][key] ? convertDtm(data[0][j][key + '_1']) : "-";
                            } else if (key.includes('SEQUESTER_EXPIRE_STS')) {
                                temp[1] = data[0][j][key] ? data[0][j][key] == 1 ? 'มีกำหนด' : 'ไม่มีกำหนด' : "-";
                                temp[2] = data[0][j][key + '_1'] ? data[0][j][key + '_1'] == 1 ? 'มีกำหนด' : 'ไม่มีกำหนด' : "-";
                            } else {
                                temp[1] = data[0][j][key] ? data[0][j][key] : "-";
                                temp[2] = data[0][j][key + '_1'] ? data[0][j][key + '_1'] : "-";
                            }
                            dataSet[num] = temp;
                            num++;
                        }
                    }
                } else {
                    $('#tabPanel-' + type + ' > ul > li:last-child > a').addClass('disabled');
                    $('#tabPanel-' + type + ' > ul > li:last-child').css("cursor", "not-allowed");
                }

                // if (dataSet.length <= 0) {
                //     $('#tabPanel-' + type + ' > ul > li:last-child > a').hover(function(){
                //         // $('#tabPanel-' + type + ' > ul > li:first-child > a').addClass('disabled');
                //         $(this).addClass('disabled');
                //         $('#tabPanel-' + type + ' > ul > li:last-child').css("cursor", "not-allowed");
                //     });
                // }

                var sequester_detailTab = $('#sequester-detailTab-' + type + '-table').DataTable({
                    "searching": false,
                    "ordering": false,
                    orderCellsTop: true,
                    "language": {
                        "emptyTable": "ไม่มีข้อมูลอายัด"
                    },
                    "bPaginate": false,
                    'info': false,
                    "columnDefs": [{
                        "className": "dt-center",
                        "targets": [1, 2],

                    }, {
                        "className": "dt-left",
                        "targets": [0],

                    }],
                    "drawCallback": function () {
                        currentPageCount = this.api().rows({
                            page: 'current'
                        }).data().length;
                    }
                });

                sequester_detailTab.search('').draw();
                sequester_detailTab.clear();
                sequester_detailTab.rows.add(dataSet);
                sequester_detailTab.draw();

                $('.overlayP2').hide();
            }
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    }


    //=================================================== Parcel Detail ===================================================
    dataUrl = 'landoffice=' + landoffice;
    if (typeSeq == 9) {
        if (row.data()[7] != "") dataUrl += '&dataSeqP1=' + row.data()[7];
        if (row.data()[8] != "") dataUrl += '&dataSeqP2=' + row.data()[8];
    }
    else if (typeSeq == 8) {  //nsl
        if (row.data()[7] != "") dataUrl += '&dataSeqP1=' + row.data()[7];
        if (row.data()[8] != "") dataUrl += '&dataSeqP2=' + row.data()[8];

    }
    else if (typeSeq == 13 || typeSeq == 5 || typeSeq == 23) { //condo || ns3 || subNsl
        if (row.data()[6] != "") dataUrl += '&dataSeqP1=' + row.data()[6];
        if (row.data()[7] != "") dataUrl += '&dataSeqP2=' + row.data()[7];

    }
    else if (typeSeq == 'c') {
        if (row.data()[8] != "") dataUrl += '&dataSeqP1=' + row.data()[8];
        if (row.data()[9] != "") dataUrl += '&dataSeqP2=' + row.data()[9];
    }
    else {
        if (row.data()[5] != "") dataUrl += '&dataSeqP1=' + row.data()[5];
        if (row.data()[6] != "") dataUrl += '&dataSeqP2=' + row.data()[6];
    }

    dataUrl += '&check=' + typeSeq;


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDetailData.php',
        data: dataUrl,
        success: function (data) {
            var num = 0, dataSet = [];
            console.log(data);
            var count = data[0].length > 0 ? Object.keys(data[0][0]).length : 0;
            var loop = dataUrl.includes("dataSeqP1") && dataUrl.includes("dataSeqP2") ? 2 : 1;
            console.log("detailData = ", data);

            if (typeSeq == 13) {
                let condoColumn = {
                    'CONDO_ID_P1': 'เลขที่อาคารชุด',
                    'CONDO_NAME_TH_P1': 'ชื่ออาคารชุด',
                    'AMPHUR_NAME_P1': 'อำเภอ',
                    'TAMBOL_NAME_P1': 'ตำบล',
                    'CONDO_ROOM_NUM_P1': 'จำนวนห้องชุด',
                    'CONDO_RECV_DATE_P1': 'วันที่ได้รับอนุญาต',
                    'CONDO_REGISTER_DATE_P1': 'วันที่จดทะเบียนอาคารชุด',
                    'CORP_ID_P1': 'เลขที่นิติบุคคลอาคารชุด',
                    'CONDO_CORP_NAME_P1': 'ชื่อนิติบุคคลอาคารชุด',
                    'CONDO_CORP_REGDTM_P1': 'วันที่จดทะเบียนนิติบุคคลอาคารชุด',
                    'ADDR_P1': 'ที่ตั้งนิติบุคคลอาคารชุด',
                }

                var count = data[0].length > 0 ? Object.keys(data[0][0]).length : 0;
                var loop = dataUrl.includes("dataSeqP1") && dataUrl.includes("dataSeqP2") ? 2 : 1;
                console.log("detailData = ", data);

                for (let i = 0; i < data[0].length; i += loop) {
                    for (let j = 0; j < (Object.keys(condoColumn).length); j++) {
                        var key = Object.keys(condoColumn)[j];

                        temp = []
                        temp[0] = condoColumn[key]
                        if (key.includes("OBLIGATION_NUM")) {
                            continue;
                        }


                        if (key.includes("UTMSCALE_SEQ")) {
                            var UTMP1, UTMP2;
                            utmScale.forEach(utm => {
                                if (data[0][0]['UTMSCALE_SEQ_P1'] == utm.seq) UTMP1 = utm.value
                                if (data[0][0]['UTMSCALE_SEQ_P2'] == utm.seq) UTMP2 = utm.value
                            })

                            temp[1] = UTMP1;
                            temp[2] = UTMP2;
                        }
                        else if (key.includes("ORIGINSCALE_SEQ")) {
                            var UTMP1, UTMP2;
                            utmScale.forEach(utm => {
                                if (data[0][0]['ORIGINSCALE_SEQ_P1'] == utm.seq) UTMP1 = utm.value
                                if (data[0][0]['ORIGINSCALE_SEQ_P2'] == utm.seq) UTMP2 = utm.value
                            })

                            temp[1] = UTMP1;
                            temp[2] = UTMP2;
                        }

                        else if (key.includes("PARCEL_OPT_FLAG") || key.includes("PARCEL_LAND_OPT_FLAG") || key.includes("CONDOROOM_OPT_FLG") || key.includes("CONDOROOM_OPT_FLAG")) {
                            var PARCEL_LAND_OPT_FLAG_P1, PARCEL_LAND_OPT_FLAG_P2;
                            switch (data[0][0][key.replace("P2", "P1")]) {
                                case '0':
                                    PARCEL_LAND_OPT_FLAG_P1 = "นอกเขตเทศบาล";
                                    break;
                                case '1':
                                    PARCEL_LAND_OPT_FLAG_P1 = "ในเขตเทศบาล";
                                    break;


                            }
                            switch (data[0][0][key.replace("P1", "P2")]) {
                                case '0':
                                    PARCEL_LAND_OPT_FLAG_P2 = "นอกเขตเทศบาล";
                                    break;
                                case '1':
                                    PARCEL_LAND_OPT_FLAG_P2 = "ในเขตเทศบาล";
                                    break;


                            }
                            temp[1] = PARCEL_LAND_OPT_FLAG_P1;
                            temp[2] = PARCEL_LAND_OPT_FLAG_P2;
                        }
                        else if (key.includes("PARCEL_SEQUEST_STS") || key.includes("PARCEL_LAND_SEQUEST_STS") || key.includes("CONDOROOM_SEQUEST_STS") || key.includes("CONSTRUCT_SEQUEST_STS")) {
                            var PARCEL_SEQUEST_STS_P1, PARCEL_SEQUEST_STS_P2;
                            switch (data[0][0][key.replace("P2", "P1")]) {
                                case '1':
                                    PARCEL_SEQUEST_STS_P1 = "ไม่มี";
                                    break;
                                case '2':
                                    PARCEL_SEQUEST_STS_P1 = "ติดอายัด";
                                    break;
                                case '3':
                                    PARCEL_SEQUEST_STS_P1 = "ขออายัด";
                                    break;


                            }
                            switch (data[0][0][key.replace("P1", "P2")]) {
                                case '1':
                                    PARCEL_SEQUEST_STS_P2 = "ไม่มี";
                                    break;
                                case '2':
                                    PARCEL_SEQUEST_STS_P2 = "ติดอายัด";
                                    break;
                                case '3':
                                    PARCEL_SEQUEST_STS_P2 = "ขออายัด";
                                    break;

                            }
                            temp[1] = PARCEL_SEQUEST_STS_P1;
                            temp[2] = PARCEL_SEQUEST_STS_P2;
                        }
                        else {
                            temp[1] = data[0][0][key.replace("P2", "P1")] ? data[0][0][key.replace("P2", "P1")] : "-";
                            temp[2] = data[0][0][key.replace("P1", "P2")] ? data[0][0][key.replace("P1", "P2")] : "-";
                        }

                        if (!dataUrl.includes("dataSeqP1") && dataUrl.includes("dataSeqP2")) {
                            temp[1] = "-";
                        }

                        if (dataUrl.includes("dataSeqP1") && !dataUrl.includes("dataSeqP2")) {
                            temp[2] = "-";
                        }

                        dataSet[num] = temp;
                        num++;


                    }



                }
            }
            else {

                for (let i = 0; i < count; i += loop) {
                    var key = Object.keys(data[0][0])[i];
                    if (key.includes("OBLIGATION_NUM")) {
                        continue;
                    }

                    temp = []
                    temp[0] = columnsParcelDetailMain[key]


                    if (key.includes("UTMSCALE_SEQ")) {
                        var UTMP1, UTMP2;
                        utmScale.forEach(utm => {
                            if (data[0][0]['UTMSCALE_SEQ_P1'] == utm.seq) UTMP1 = utm.value
                            if (data[0][0]['UTMSCALE_SEQ_P2'] == utm.seq) UTMP2 = utm.value
                        })

                        temp[1] = UTMP1;
                        temp[2] = UTMP2;
                    }
                    else if (key.includes("ORIGINSCALE_SEQ")) {
                        var UTMP1, UTMP2;
                        utmScale.forEach(utm => {
                            if (data[0][0]['ORIGINSCALE_SEQ_P1'] == utm.seq) UTMP1 = utm.value
                            if (data[0][0]['ORIGINSCALE_SEQ_P2'] == utm.seq) UTMP2 = utm.value
                        })

                        temp[1] = UTMP1;
                        temp[2] = UTMP2;
                    }

                    else if (key.includes("PARCEL_OPT_FLAG") || key.includes("PARCEL_LAND_OPT_FLAG") || key.includes("CONDOROOM_OPT_FLG") || key.includes("CONDOROOM_OPT_FLAG")) {
                        var PARCEL_LAND_OPT_FLAG_P1, PARCEL_LAND_OPT_FLAG_P2;
                        switch (data[0][0][key.replace("P2", "P1")]) {
                            case '0':
                                PARCEL_LAND_OPT_FLAG_P1 = "นอกเขตเทศบาล";
                                break;
                            case '1':
                                PARCEL_LAND_OPT_FLAG_P1 = "ในเขตเทศบาล";
                                break;


                        }
                        switch (data[0][0][key.replace("P1", "P2")]) {
                            case '0':
                                PARCEL_LAND_OPT_FLAG_P2 = "นอกเขตเทศบาล";
                                break;
                            case '1':
                                PARCEL_LAND_OPT_FLAG_P2 = "ในเขตเทศบาล";
                                break;


                        }
                        temp[1] = PARCEL_LAND_OPT_FLAG_P1;
                        temp[2] = PARCEL_LAND_OPT_FLAG_P2;
                    }
                    else if (key.includes("PARCEL_SEQUEST_STS") || key.includes("PARCEL_LAND_SEQUEST_STS") || key.includes("CONDOROOM_SEQUEST_STS") || key.includes("CONSTRUCT_SEQUEST_STS")) {
                        var PARCEL_SEQUEST_STS_P1, PARCEL_SEQUEST_STS_P2;
                        switch (data[0][0][key.replace("P2", "P1")]) {
                            case '1':
                                PARCEL_SEQUEST_STS_P1 = "ไม่มี";
                                break;
                            case '2':
                                PARCEL_SEQUEST_STS_P1 = "ติดอายัด";
                                break;
                            case '3':
                                PARCEL_SEQUEST_STS_P1 = "ขออายัด";
                                break;


                        }
                        switch (data[0][0][key.replace("P1", "P2")]) {
                            case '1':
                                PARCEL_SEQUEST_STS_P2 = "ไม่มี";
                                break;
                            case '2':
                                PARCEL_SEQUEST_STS_P2 = "ติดอายัด";
                                break;
                            case '3':
                                PARCEL_SEQUEST_STS_P2 = "ขออายัด";
                                break;

                        }
                        temp[1] = PARCEL_SEQUEST_STS_P1;
                        temp[2] = PARCEL_SEQUEST_STS_P2;
                    }
                    else {
                        console.log(key, data[0][0][key.replace("P2", "P1")], data[0][0][key.replace("P1", "P2")]);
                        temp[1] = data[0][0][key.replace("P2", "P1")] ? data[0][0][key.replace("P2", "P1")] : "-";
                        temp[2] = data[0][0][key.replace("P1", "P2")] ? data[0][0][key.replace("P1", "P2")] : "-";
                    }

                    if (!dataUrl.includes("dataSeqP1") && dataUrl.includes("dataSeqP2")) {
                        temp[1] = "-";
                    }

                    if (dataUrl.includes("dataSeqP1") && !dataUrl.includes("dataSeqP2")) {
                        temp[2] = "-";
                    }

                    dataSet[num] = temp;
                    num++;

                }
            }


            var parcel_detailTab = $('#parcel-detailTab-' + type + '-table').DataTable({
                "searching": false,
                "ordering": false,
                orderCellsTop: true,
                "language": {
                    "emptyTable": "ไม่มีข้อมูลทะเบียน"
                },
                "bPaginate": false,
                'info': false,
                "columnDefs": [{
                    "className": "dt-center",
                    "targets": [1, 2],

                }, {
                    "className": "dt-left",
                    "targets": [0],

                }],
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });

            parcel_detailTab.search('').draw();
            parcel_detailTab.clear();
            parcel_detailTab.rows.add(dataSet);
            parcel_detailTab.draw();

            $('.overlayP2').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');


    //=================================================== OWNER Detail ===================================================
    if (typeSeq != 13) {
        $('#tabPanel-' + type + ' > ul > li:nth-child(2) > a').removeClass('disabled');
        $('#tabPanel-' + type + ' > ul > li:nth-child(2)').css("cursor", "");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: './queries/getOwnDetail.php',
            data: dataUrl,
            success: function (data) {
                var num = 0, dataSet = [];
                console.log(data);

                console.log("ownerData = ", data);
                var count = Object.keys(data[0]).length;
                if (count > 0) {
                    for (let j = 0; j < data[0].length; j++) {
                        var count = data[0].length > 0 ? Object.keys(data[0][j]).length : 0;
                        for (let i = 0; i < (Object.keys(columnOwnerDetailMain).length / 2); i++) {
                            var key = Object.keys(columnOwnerDetailMain)[i];

                            temp = []
                            temp[0] = columnOwnerDetailMain[key]

                            if (key.includes("OWNER_GENDER")) {
                                var OWNER_GENDER_P1, OWNER_GENDER_P2;
                                switch (data[0][j][key.replace("P2", "P1")]) {
                                    case '1':
                                        OWNER_GENDER_P1 = 'ชาย';
                                        break;
                                    case '2':
                                        OWNER_GENDER_P1 = 'หญิง';
                                        break;
                                }
                                switch (data[0][j][key.replace("P1", "P2")]) {
                                    case '1':
                                        OWNER_GENDER_P2 = 'ชาย';
                                        break;
                                    case '2':
                                        OWNER_GENDER_P2 = 'หญิง';
                                        break;
                                }
                                temp[1] = OWNER_GENDER_P1;
                                temp[2] = OWNER_GENDER_P2;
                            }
                            else if (key.includes("OWNER_PN_STS")) {
                                var OWNER_PN_STS_P1, OWNER_PN_STS_P2;
                                switch (data[0][j][key.replace("P2", "P1")]) {
                                    case '0':
                                        OWNER_PN_STS_P1 = 'โสด';
                                        break;
                                    case '1':
                                        OWNER_PN_STS_P1 = 'สมรส';
                                        break;
                                    case '2':
                                        OWNER_PN_STS_P1 = 'หย่า';
                                        break;
                                    case '3':
                                        OWNER_PN_STS_P1 = 'หม้าย';
                                        break;
                                }
                                switch (data[0][j][key.replace("P1", "P2")]) {
                                    case '0':
                                        OWNER_PN_STS_P2 = 'โสด';
                                        break;
                                    case '1':
                                        OWNER_PN_STS_P2 = 'สมรส';
                                        break;
                                    case '2':
                                        OWNER_PN_STS_P2 = 'หย่า';
                                        break;
                                    case '3':
                                        OWNER_PN_STS_P2 = 'หม้าย';
                                        break;

                                }
                                temp[1] = OWNER_PN_STS_P1;
                                temp[2] = OWNER_PN_STS_P2;
                            }
                            else {
                                temp[1] = data[0][j][key.replace("P2", "P1")] ? data[0][j][key.replace("P2", "P1")] : "-";
                                temp[2] = data[0][j][key.replace("P1", "P2")] ? data[0][j][key.replace("P1", "P2")] : "-";
                            }

                            if (!dataUrl.includes("dataSeqP1") && dataUrl.includes("dataSeqP2")) {
                                temp[1] = "-";
                            }

                            if (dataUrl.includes("dataSeqP1") && !dataUrl.includes("dataSeqP2")) {
                                temp[2] = "-";
                            }
                            dataSet[num] = temp;
                            num++;

                        }
                    }
                } else {
                    $('#tabPanel-' + type + ' > ul > li:nth-child(2) > a').addClass('disabled');
                    $('#tabPanel-' + type + ' > ul > li:nth-child(2)').css("cursor", "not-allowed");
                }
                // if (dataSet.length <= 0) {
                //     $('#tabPanel-' + type + ' > ul > li:nth-child(2) > a').hover(function(){
                //         $(this).addClass('disabled');
                //         $('#tabPanel-' + type + ' > ul > li:nth-child(2)').css("cursor", "not-allowed");
                //     });
                // }


                var owner_detailTab = $('#owner-detailTab-' + type + '-table').DataTable({
                    "searching": false,
                    "ordering": false,
                    orderCellsTop: true,
                    "bPaginate": false,
                    "language": {
                        "emptyTable": "ไม่มีข้อมูลผู้ถือกรรมสิทธิ์"
                    },
                    'info': false,
                    "createdRow": function (row, data, index) {
                        if (data[0].includes("ลำดับผู้ถือกรรมสิทธิ์")) {
                            $('td', row).css('background-color', 'rgb(236,236,236)');
                        }
                    },
                    "columnDefs": [{
                        "className": "dt-center",
                        "targets": [1, 2],

                    }, {
                        "className": "dt-left",
                        "targets": [0],

                    }],
                    "drawCallback": function () {
                        currentPageCount = this.api().rows({
                            page: 'current'
                        }).data().length;

                    }
                });

                owner_detailTab.search('').draw();
                owner_detailTab.clear();
                owner_detailTab.rows.add(dataSet);
                owner_detailTab.draw();


                $('.overlayP2').hide();

            }
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    }



    //=================================================== OLGT Detail ===================================================
    if (typeSeq != 13) {
        $('#tabPanel-' + type + ' > ul > li:nth-child(3) > a').removeClass('disabled');
        $('#tabPanel-' + type + ' > ul > li:nth-child(3)').css("cursor", "");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: './queries/getOlgtDetail.php',
            data: dataUrl,
            success: function (data) {
                var num = 0, dataSet = [];
                console.log("olgtData = ", data[0]);
                var count = Object.keys(data[0]).length;
                if (count > 0) {
                    var loop = dataUrl.includes("dataSeqP1") && dataUrl.includes("dataSeqP2") ? 2 : 1;
                    for (let j = 0; j < data[0].length; j++) {
                        var count = data[0].length > 0 ? Object.keys(data[0][j]).length : 0;
                        for (let i = 0; i < count; i += loop) {
                            var key = Object.keys(data[0][j])[i];
                            if (key.includes("PARTY_TYPE") || key.includes("HD_SEQ")) {
                                continue;
                            }
                            temp = []
                            temp[0] = columnOlgtDetailMain[key]
                            if (j > 0 /* && j < data[0][j - 1]["OLGT_HD_SEQ_P1"] == data[0][j]["OLGT_HD_SEQ_P1"] && data[0][j - 1]["OLGT_HD_SEQ_P2"] == data[0][j]["OLGT_HD_SEQ_P2"] && data[0][j - 1]["PARTY_TYPE_P1"] == data[0][j]["PARTY_TYPE_P1"] && data[0][j - 1]["PARTY_TYPE_P2"] == data[0][j]["PARTY_TYPE_P2"]*/) {
                                // console.log( data[0][j - 1]["OLGT_HD_SEQ_P1"],  data[0][j]["OLGT_HD_SEQ_P1"]);
                                console.log(data[0][j - 1]["OLGT_HD_SEQ_P1"], data[0][j]["OLGT_HD_SEQ_P1"], data[0][j - 1]["OLGT_HD_SEQ_P1"] == data[0][j]["OLGT_HD_SEQ_P1"]);
                                switch (data[0][j]['PARTY_TYPE_P1']) {
                                    case '1':
                                    case '4':
                                        temp[0] = "ผู้ให้สัญญา";
                                        break;
                                    case '2':
                                    case '3':
                                        temp[0] = "ผู้รับสัญญา";
                                        break;
                                }
                                temp[1] = data[0][j]["OWN_P1"] ? data[0][j]["OWN_P1"] : "-";
                                temp[2] = data[0][j]["OWN_P1".replace("P1", "P2")] ? data[0][j]["OWN_P1".replace("P1", "P2")] : "-";
                                dataSet[num] = temp;
                                num++;
                                break;
                            }

                            if (key.includes("PERIOD_FLG")) {
                                var PERIOD_FLG_P1, PERIOD_FLG_P2;
                                switch (data[0][j][key.replace("P2", "P1")]) {
                                    case '1':
                                        PERIOD_FLG_P1 = 'มีกำหนด ... ปี';
                                        break;
                                    case '2':
                                        PERIOD_FLG_P1 = 'ตลอดชีวิตของผู้ทรงสิทธิ';
                                        break;
                                    case '3':
                                        PERIOD_FLG_P1 = 'ตลอดชีวิตของผู้ถือกรรมสิทธิ์';
                                        break;
                                    case '4':
                                        PERIOD_FLG_P1 = 'ไม่มีกำหนดระยะเวลา';
                                        break;
                                }
                                switch (data[0][j][key.replace("P1", "P2")]) {
                                    case '1':
                                        PERIOD_FLG_P2 = 'มีกำหนด ... ปี';
                                        break;
                                    case '2':
                                        PERIOD_FLG_P2 = 'ตลอดชีวิตของผู้ทรงสิทธิ';
                                        break;
                                    case '3':
                                        PERIOD_FLG_P2 = 'ตลอดชีวิตของผู้ถือกรรมสิทธิ์';
                                        break;
                                    case '4':
                                        PERIOD_FLG_P2 = 'ไม่มีกำหนดระยะเวลา';
                                        break;
                                }
                                temp[1] = PERIOD_FLG_P1;
                                temp[2] = PERIOD_FLG_P2;
                            }
                            else if (key.includes("OWN") && data[0][j]["PARTY_TYPE_P1"] == data[0][j]["PARTY_TYPE_P2"]) {
                                switch (data[0][j]["PARTY_TYPE_P1"]) {
                                    case '1':
                                    case '4':
                                        temp[0] = "ผู้ให้สัญญา";
                                        break;
                                    case '2':
                                    case '3':
                                        temp[0] = "ผู้รับสัญญา";
                                        break;
                                }
                                temp[1] = data[0][j][key.replace("P2", "P1")] ? data[0][j][key.replace("P2", "P1")] : "-";
                                temp[2] = data[0][j][key.replace("P1", "P2")] ? data[0][j][key.replace("P1", "P2")] : "-";

                            }
                            else if (key.includes("MNY")) {
                                temp[1] = data[0][j][key] ? parseFloat(data[0][j][key]).toLocaleString(undefined, {
                                    minimumFractionDigits: 2,
                                    maximumFractionDigits: 2
                                }) : "-";
                                temp[2] = data[0][j][key.replace("P1", "P2")] ? parseFloat(data[0][j][key.replace("P1", "P2")]).toLocaleString(undefined, {
                                    minimumFractionDigits: 2,
                                    maximumFractionDigits: 2
                                }) : "-";
                            }
                            else if (key.includes("DURATION")) {
                                if (data[0][j][key].includes("0ปี 0เดือน 0วัน")) {
                                    temp[1] = "-";
                                }
                                if (data[0][j][key].includes("0ปี 0เดือน 0วัน")) {
                                    temp[2] = "-";
                                }
                            }
                            else {
                                temp[1] = data[0][j][key.replace("P2", "P1")] ? data[0][j][key.replace("P2", "P1")] : "-";
                                temp[2] = data[0][j][key.replace("P1", "P2")] ? data[0][j][key.replace("P1", "P2")] : "-";
                            }
                            if (!dataUrl.includes("dataSeqP1") && dataUrl.includes("dataSeqP2")) {
                                temp[1] = "-";
                            }

                            if (dataUrl.includes("dataSeqP1") && !dataUrl.includes("dataSeqP2")) {
                                temp[2] = "-";
                            }
                            dataSet[num] = temp;
                            num++;




                        }
                    }
                } else {
                    $('#tabPanel-' + type + ' > ul > li:nth-child(3) > a').addClass('disabled');
                    $('#tabPanel-' + type + ' > ul > li:nth-child(3)').css("cursor", "not-allowed");
                }
                // if (dataSet.length <= 0) {
                //     $('#tabPanel-' + type + ' > ul > li:nth-child(3) > a').hover(function(){
                //         $(this).addClass('disabled');
                //         $('#tabPanel-' + type + ' > ul > li:nth-child(3)').css("cursor", "not-allowed");
                //     });
                // }

                var olgt_detailTab = $('#olgt-detailTab-' + type + '-table').DataTable({
                    "searching": false,
                    "ordering": false,
                    orderCellsTop: true,
                    "language": {
                        "emptyTable": "ไม่มีข้อมูลภาระผูกพัน"
                    },
                    "bPaginate": false,
                    'info': false,
                    "columnDefs": [{
                        "className": "dt-center",
                        "targets": [1, 2],

                    }, {
                        "className": "dt-left",
                        "targets": [0],

                    }],
                    "drawCallback": function () {
                        currentPageCount = this.api().rows({
                            page: 'current'
                        }).data().length;
                    }
                });

                olgt_detailTab.search('').draw();
                olgt_detailTab.clear();
                olgt_detailTab.rows.add(dataSet);
                olgt_detailTab.draw();

                $('.overlayP2').hide();
            }
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    }





}

function showProcessDetail(e) {


    var tr = e.parent().closest('tr');
    var table = tr.parent().parent().attr('id');
    var row = $('#' + table).DataTable().row(tr);
    // $('.processTab-' + type + ' a ').click();
    $('#tab6Panel-' + type).css("visibility", "visible");
    $('#tab6Panel-' + type).css("display", "block");

    $('.overlayP1').show();

    console.log("row ", row.data());

    //======================================== ราคาประเมิน ========================================
    var dataSet = [], dataSetP1 = [], dataSetP2 = [];

    var dataUrl = 'landoffice=' + landoffice;


    dataUrl += '&processSeqP1=' + row.data()[7][0]["PROCESS_SEQ"];

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getAPSData.php',
        data: dataUrl,
        async: true,
        success: function (data) {
            dataSetP1 = data;
            dataUrl = 'landoffice=' + landoffice;

            dataUrl += '&processSeqP2=' + row.data()[7][1]["PROCESS_SEQ"];

        }
    }).done(function () {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: './queries/getAPSData.php',
            data: dataUrl,
            async: true,
            success: function (data) {
                dataSetP2 = data;
                console.log("APS1 ", dataSetP1);
                console.log("APS2 ", dataSetP2);
                var num = 0, maxLoop = dataSetP1[0].length >= dataSetP2[0].length ? dataSetP1[0].length : dataSetP2[0].length;


                for (let j = 0; j < maxLoop; j++) {
                    for (let i = 0; i < Object.keys(columnAPSDetailMain).length; i++) {
                        var key = Object.keys(columnAPSDetailMain)[i];
                        if (key.includes("TEMP_SEQ")) {
                            continue;
                        }


                        temp = []
                        console.log(j, " ", dataSetP1[0][j]);
                        temp[0] = columnAPSDetailMain[key];

                        if (key.includes("TEMP_NUM") || key.includes("TEMP_TOT")) {
                            temp[1] = dataSetP1[0][j] && dataSetP1[0][j][key] ? parseInt(dataSetP1[0][j][key]).toLocaleString() : "-";
                            temp[2] = dataSetP2[0][j] && dataSetP2[0][j][key] ? parseInt(dataSetP2[0][j][key]).toLocaleString() : "-";
                        }
                        else if (key.includes("DTM") || key.includes("DATE")) {
                            var DATE_P1, DATE_P2, YEAR_P1, YEAR_P2;
                            if (dataSetP1[0][j][key]) {
                                DATE_P1 = data[0][j][key].split(" ");
                                YEAR_P1 = DATE_P1[2] != "" ? DATE_P1[2] : DATE_P1[3];
                                if (parseInt(YEAR_P1) < 2500) {
                                    temp[1] = dataSetP1[0][j][key].replace(YEAR_P1, parseInt(YEAR_P1) + 543)
                                }
                                else {
                                    temp[1] = dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                }
                            }
                            else {
                                temp[1] = dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                            }
                            if (dataSetP2[0][j][key]) {
                                DATE_P2 = dataSetP2[0][j][key].split(" ");
                                YEAR_P2 = DATE_P2[2] != "" ? DATE_P2[2] : DATE_P2[3];
                                if (parseInt(YEAR_P2) < 2500) {
                                    temp[2] = dataSetP2[0][j][key].replace(YEAR_P2, parseInt(YEAR_P2) + 543)
                                }
                                else {
                                    temp[2] = dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                                }
                            }
                            else {
                                temp[2] = dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                            }
                        }
                        else {
                            temp[1] = dataSetP1[0][j] && dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                            temp[2] = dataSetP2[0][j] && dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                        }




                        dataSet[num] = temp;
                        num++;

                    }
                }

                if (dataSet.length <= 0) {
                    $('#tab6Panel-' + type + ' > ul > li:first-child > a').hover(function () {
                        $(this).addClass('disabled');
                        $('#tab6Panel-' + type + ' > ul > li:first-child').css("cursor", "not-allowed");
                    });
                }

                var aps_processTab = $('#aps-processTab-' + type + '-table').DataTable({
                    "searching": false,
                    "ordering": false,
                    orderCellsTop: true,
                    "language": {
                        "emptyTable": "ไม่มีข้อมูลราคาประเมิน"
                    },
                    "bPaginate": true,
                    "pageLength": Object.keys(columnAPSDetailMain).length,
                    "pagingType": "simple_numbers",
                    'info': false,
                    "lengthChange": false,
                    "columnDefs": [{
                        "className": "dt-center",
                        "targets": [1, 2],

                    }, {
                        "className": "dt-left",
                        "targets": [0],

                    }],
                    "drawCallback": function () {
                        currentPageCount = this.api().rows({
                            page: 'current'
                        }).data().length;
                    }
                });

                aps_processTab.search('').draw();
                aps_processTab.clear();
                aps_processTab.rows.add(dataSet);
                aps_processTab.draw();




            }

        }).done(function () {
            //======================================== คู่สัญญา ========================================
            dataSet = [], dataSetP1 = [], dataSetP2 = [];
            dataUrl = 'landoffice=' + landoffice;

            dataUrl += '&processSeqP1=' + row.data()[7][0]["PROCESS_SEQ"];
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: './queries/getPromiseData.php',
                data: dataUrl,
                async: true,
                success: function (data) {
                    dataSetP1 = data;
                    dataUrl = 'landoffice=' + landoffice;

                    dataUrl += '&processSeqP2=' + row.data()[7][1]["PROCESS_SEQ"];


                }
            }).done(function () {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: './queries/getPromiseData.php',
                    data: dataUrl,
                    success: function (data) {
                        dataSetP2 = data;
                        console.log("PromiseP1 = ", dataSetP1);
                        console.log("PromiseP2 = ", dataSetP2);
                        dataSetP2 = data;
                        var num = 0, maxLoop = dataSetP1[0].length >= dataSetP2[0].length ? dataSetP1[0].length : dataSetP2[0].length;
                        let promisor_count = 1, promisee_count = 1;

                        for (let j = 0; j < maxLoop; j++) {
                            for (let i = 0; i < Object.keys(columnPromiseDetailMain).length; i++) {
                                var key = Object.keys(columnPromiseDetailMain)[i];
                                if (!key || key.includes("TYPE") || key.includes("ORD")) {
                                    continue;
                                }

                                temp = []

                                if (key.includes("NAME_PROM")) {
                                    if (dataSetP1[0][j]["TYPE"] == 1) {
                                        temp[0] = 'ผู้ให้สัญญา (ลำดับที่ ' + promisor_count + ")";
                                        promisor_count++
                                    }
                                    else if (dataSetP1[0][j]["TYPE"] == 2) {
                                        temp[0] = 'ผู้รับสัญญา (ลำดับที่ ' + promisee_count + ")";
                                        promisee_count++
                                    }
                                }

                                else {
                                    temp[0] = columnPromiseDetailMain[key];
                                }

                                if (key.includes("DTM") || key.includes("DATE")) {
                                    var DATE_P1, DATE_P2, YEAR_P1, YEAR_P2;
                                    if (dataSetP1[0][j][key]) {
                                        DATE_P1 = data[0][j][key].split(" ");
                                        YEAR_P1 = DATE_P1[2] != "" ? DATE_P1[2] : DATE_P1[3];
                                        if (parseInt(YEAR_P1) < 2500) {
                                            temp[1] = dataSetP1[0][j][key].replace(YEAR_P1, parseInt(YEAR_P1) + 543)
                                        }
                                        else {
                                            temp[1] = dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                        }
                                    }
                                    else {
                                        temp[1] = dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                    }
                                    if (dataSetP2[0][j][key]) {
                                        DATE_P2 = dataSetP2[0][j][key].split(" ");
                                        YEAR_P2 = DATE_P2[2] != "" ? DATE_P2[2] : DATE_P2[3];
                                        if (parseInt(YEAR_P2) < 2500) {
                                            temp[2] = dataSetP2[0][j][key].replace(YEAR_P2, parseInt(YEAR_P2) + 543)
                                        }
                                        else {
                                            temp[2] = dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                                        }
                                    }
                                    else {
                                        temp[2] = dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                                    }
                                }
                                else {
                                    temp[1] = dataSetP1[0][j] && dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                    temp[2] = dataSetP2[0][j] && dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                                }


                                dataSet[num] = temp;
                                num++;

                            }
                        }

                        if (dataSet.length <= 0) {
                            $('#tab6Panel-' + type + ' > ul > li:nth-child(2) > a').hover(function () {
                                $(this).addClass('disabled');
                                $('#tab6Panel-' + type + ' > ul > li:nth-child(2)').css("cursor", "not-allowed");
                            });
                        }

                        var promise_processTab = $('#promise-processTab-' + type + '-table').DataTable({
                            "searching": false,
                            "ordering": false,
                            orderCellsTop: true,
                            "language": {
                                "emptyTable": "ไม่มีข้อมูลคู่สัญญา"
                            },
                            "bPaginate": false,
                            'info': false,
                            "createdRow": function (row, data, index) {
                                if (data[0].includes("เลขประจำตัวประชาชน")) {
                                    $('td', row).css('background-color', 'rgb(236,236,236)');
                                }
                            },
                            "columnDefs": [{
                                "className": "dt-center",
                                "targets": [1, 2],

                            }, {
                                "className": "dt-left",
                                "targets": [0],

                            }],
                            "drawCallback": function () {
                                currentPageCount = this.api().rows({
                                    page: 'current'
                                }).data().length;
                            }
                        });

                        promise_processTab.search('').draw();
                        promise_processTab.clear();
                        promise_processTab.rows.add(dataSet);
                        promise_processTab.draw();


                    }

                }).done(function () {
                    //======================================== การได้มา ========================================
                    dataSet = [], dataSetP1 = [], dataSetP2 = [];
                    dataUrl = 'landoffice=' + landoffice;

                    dataUrl += '&processSeqP1=' + row.data()[7][0]["PROCESS_SEQ"];

                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: './queries/getObtainData.php',
                        data: dataUrl,
                        async: true,
                        success: function (data) {
                            dataSetP1 = data;
                            dataUrl = 'landoffice=' + landoffice;

                            dataUrl += '&processSeqP2=' + row.data()[7][1]["PROCESS_SEQ"];

                        }
                    }).done(function () {
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: './queries/getObtainData.php',
                            data: dataUrl,
                            async: true,
                            success: function (data) {
                                dataSetP2 = data;
                                console.log("Obtain1 ", dataSetP1);
                                console.log("Obtain2 ", dataSetP2);
                                var num = 0, maxLoop = dataSetP1[0].length >= dataSetP2[0].length ? dataSetP1[0].length : dataSetP2[0].length;

                                for (let j = 0; j < maxLoop; j++) {
                                    for (let i = 0; i < Object.keys(columnObtainDetailMain).length; i++) {
                                        var key = Object.keys(columnObtainDetailMain)[i];
                                        if (key.includes("ORDER") || key.includes("TEMP_SEQ")) {
                                            continue;
                                        }

                                        temp = []

                                        temp[0] = columnObtainDetailMain[key];
                                        if (key.includes("DTM") || key.includes("DATE")) {
                                            var DATE_P1, DATE_P2, YEAR_P1, YEAR_P2;
                                            if (dataSetP1[0][j][key]) {
                                                DATE_P1 = data[0][j][key].split(" ");
                                                YEAR_P1 = DATE_P1[2] != "" ? DATE_P1[2] : DATE_P1[3];
                                                if (parseInt(YEAR_P1) < 2500) {
                                                    temp[1] = dataSetP1[0][j][key].replace(YEAR_P1, parseInt(YEAR_P1) + 543)
                                                }
                                                else {
                                                    temp[1] = dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                                }
                                            }
                                            else {
                                                temp[1] = dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                            }
                                            if (dataSetP2[0][j][key]) {
                                                DATE_P2 = dataSetP2[0][j][key].split(" ");
                                                YEAR_P2 = DATE_P2[2] != "" ? DATE_P2[2] : DATE_P2[3];
                                                if (parseInt(YEAR_P2) < 2500) {
                                                    temp[2] = dataSetP2[0][j][key].replace(YEAR_P2, parseInt(YEAR_P2) + 543)
                                                }
                                                else {
                                                    temp[2] = dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                                                }
                                            }
                                            else {
                                                temp[2] = dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                                            }
                                        }
                                        else {
                                            temp[1] = dataSetP1[0][j] && dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                            temp[2] = dataSetP2[0][j] && dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                                        }




                                        dataSet[num] = temp;
                                        num++;

                                    }
                                }
                                console.log(dataSet);

                                if (dataSet.length <= 0) {

                                    $('#tab6Panel-' + type + ' > ul > li:nth-child(3) > a').hover(function () {
                                        $(this).addClass('disabled');
                                        $('#tab6Panel-' + type + ' > ul > li:nth-child(3)').css("cursor", "not-allowed");
                                    });
                                }

                                var obtain_processTab = $('#obtain-processTab-' + type + '-table').DataTable({
                                    "searching": false,
                                    "ordering": false,
                                    orderCellsTop: true,
                                    "language": {
                                        "emptyTable": "ไม่มีข้อมูลการได้มา"
                                    },
                                    "bPaginate": true,
                                    "pageLength": Object.keys(columnObtainDetailMain).length,
                                    "pagingType": "simple_numbers",
                                    'info': false,
                                    "lengthChange": false,
                                    "columnDefs": [{
                                        "className": "dt-center",
                                        "targets": [1, 2],

                                    }, {
                                        "className": "dt-left",
                                        "targets": [0],

                                    }],
                                    "drawCallback": function () {
                                        currentPageCount = this.api().rows({
                                            page: 'current'
                                        }).data().length;
                                    }
                                });

                                obtain_processTab.search('').draw();
                                obtain_processTab.clear();
                                obtain_processTab.rows.add(dataSet);
                                obtain_processTab.draw();



                            }

                        }).done(function () {
                            //======================================== สอบสวน ========================================
                            dataSet = [], dataSetP1 = [], dataSetP2 = [];
                            dataUrl = 'landoffice=' + landoffice;

                            dataUrl += '&processSeqP1=' + row.data()[7][0]["PROCESS_SEQ"];
                            $.ajax({
                                type: 'POST',
                                dataType: 'json',
                                url: './queries/getProcProcess.php',
                                data: dataUrl,
                                async: true,
                                success: function (data) {
                                    dataSetP1 = data;
                                    dataUrl = 'landoffice=' + landoffice;

                                    dataUrl += '&processSeqP2=' + row.data()[7][1]["PROCESS_SEQ"];


                                }
                            }).done(function () {
                                $.ajax({
                                    type: 'POST',
                                    dataType: 'json',
                                    url: './queries/getProcProcess.php',
                                    data: dataUrl,
                                    success: function (data) {
                                        dataSetP2 = data;
                                        console.log("Proc1 ", dataSetP1);
                                        console.log("Proc2 ", dataSetP2);
                                        var num = 0, maxLoop = dataSetP1[0].length >= dataSetP2[0].length ? dataSetP1[0].length : dataSetP2[0].length;

                                        for (let j = 0; j < maxLoop; j++) {
                                            for (let i = 0; i < Object.keys(columnProcessDetailMain).length; i++) {
                                                var key = Object.keys(columnProcessDetailMain)[i];
                                                if (key.includes("SEQ") || key.includes("COURT_NAME")) {
                                                    continue;
                                                }

                                                temp = []

                                                temp[0] = columnProcessDetailMain[key];
                                                if (key.includes("MNY")) {
                                                    temp[1] = dataSetP1[0][j] && dataSetP1[0][j][key] ? parseFloat(dataSetP1[0][j][key]).toLocaleString(undefined, {
                                                        minimumFractionDigits: 2,
                                                        maximumFractionDigits: 2
                                                    }) : "-";
                                                    temp[2] = dataSetP2[0][j] && dataSetP2[0][j][key] ? parseFloat(dataSetP2[0][j][key]).toLocaleString(undefined, {
                                                        minimumFractionDigits: 2,
                                                        maximumFractionDigits: 2
                                                    }) : "-";

                                                }
                                                else if (key.includes("CONS")) {
                                                    switch (dataSetP1[0][j]['PROCESS_PARCEL_TEMP_CONS']) {
                                                        case '1':
                                                            temp[1] = "ไม่มีสิ่งปลูกสร้าง"
                                                            break;
                                                        case '2':
                                                            temp[1] = "พร้อมสิ่งปลูกสร้าง";
                                                            break;
                                                        case '3':
                                                            temp[1] = "เฉพาะที่ดิน";
                                                            break;
                                                        default:
                                                            temp[1] = "-";
                                                            break;
                                                    }
                                                    switch (dataSetP2[0][j]['PROCESS_PARCEL_TEMP_CONS']) {
                                                        case '1':
                                                            temp[2] = "ไม่มีสิ่งปลูกสร้าง"
                                                            break;
                                                        case '2':
                                                            temp[2] = "พร้อมสิ่งปลูกสร้าง";
                                                            break;
                                                        case '3':
                                                            temp[2] = "เฉพาะที่ดิน";
                                                            break;
                                                        default:
                                                            temp[2] = "-";
                                                            break;
                                                    }

                                                }
                                                else if (key.includes("COURT")) {
                                                    switch (dataSetP1[0][j]['PROCESS_TEMP_COURT_STS']) {
                                                        case '0':
                                                            temp[1] = "ไม่มีคำสั่งศาล"
                                                            break;
                                                        case '1':
                                                            temp[1] = dataSetP1[0][j]['PROCESS_TEMP_COURT_NAME'];
                                                            break;
                                                        default:
                                                            temp[1] = "-";
                                                            break;
                                                    }
                                                    switch (dataSetP2[0][j]['PROCESS_TEMP_COURT_STS']) {
                                                        case '0':
                                                            temp[2] = "ไม่มีคำสั่งศาล"
                                                            break;
                                                        case '1':
                                                            temp[2] = dataSetP1[0][j]['PROCESS_TEMP_COURT_NAME'];
                                                            break;
                                                        default:
                                                            temp[2] = "-";
                                                            break;
                                                    }


                                                }
                                                else if (key.includes("DURATION")) {
                                                    if (dataSetP1[0][j][key].includes("0ปี 0เดือน 0วัน")) {
                                                        temp[1] = "-";
                                                    }
                                                    if (dataSetP2[0][j][key].includes("0ปี 0เดือน 0วัน")) {
                                                        temp[2] = "-";
                                                    }
                                                }
                                                else if (key.includes("DTM") || key.includes("DATE")) {
                                                    var DATE_P1, DATE_P2, YEAR_P1, YEAR_P2;
                                                    if (dataSetP1[0][j][key]) {
                                                        DATE_P1 = data[0][j][key].split(" ");
                                                        YEAR_P1 = DATE_P1[2] != "" ? DATE_P1[2] : DATE_P1[3];
                                                        if (parseInt(YEAR_P1) < 2500) {
                                                            temp[1] = dataSetP1[0][j][key].replace(YEAR_P1, parseInt(YEAR_P1) + 543)
                                                        }
                                                        else {
                                                            temp[1] = dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                                        }
                                                    }
                                                    else {
                                                        temp[1] = dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                                    }
                                                    if (dataSetP2[0][j][key]) {
                                                        DATE_P2 = dataSetP2[0][j][key].split(" ");
                                                        YEAR_P2 = DATE_P2[2] != "" ? DATE_P2[2] : DATE_P2[3];
                                                        if (parseInt(YEAR_P2) < 2500) {
                                                            temp[2] = dataSetP2[0][j][key].replace(YEAR_P2, parseInt(YEAR_P2) + 543)
                                                        }
                                                        else {
                                                            temp[2] = dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                                                        }
                                                    }
                                                    else {
                                                        temp[2] = dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                                                    }
                                                }
                                                else {
                                                    temp[1] = dataSetP1[0][j] && dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                                    temp[2] = dataSetP2[0][j] && dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";
                                                }




                                                dataSet[num] = temp;
                                                num++;

                                            }
                                        }
                                        console.log(dataSet);

                                        if (dataSet.length <= 0) {
                                            $('#tab6Panel-' + type + ' > ul > li:nth-child(4) > a').hover(function () {
                                                $(this).addClass('disabled');
                                                $('#tab6Panel-' + type + ' > ul > li:nth-child(4)').css("cursor", "not-allowed");
                                            });
                                        }
                                        var proc_processTab = $('#process-processTab-' + type + '-table').DataTable({
                                            "searching": false,
                                            "ordering": false,
                                            orderCellsTop: true,
                                            "language": {
                                                "emptyTable": "ไม่มีข้อมูลสอบสวน"
                                            },
                                            "bPaginate": true,
                                            "pageLength": Object.keys(columnProcessDetailMain).length,
                                            "pagingType": "simple_numbers",
                                            'info': false,
                                            "lengthChange": false,
                                            "columnDefs": [{
                                                "className": "dt-center",
                                                "targets": [1, 2],

                                            }, {
                                                "className": "dt-left",
                                                "targets": [0],

                                            }],
                                            "drawCallback": function () {
                                                currentPageCount = this.api().rows({
                                                    page: 'current'
                                                }).data().length;
                                            }
                                        });

                                        proc_processTab.search('').draw();
                                        proc_processTab.clear();
                                        proc_processTab.rows.add(dataSet);
                                        proc_processTab.draw();


                                    }

                                }).done(function () {
                                    //======================================== ค่าใช้จ้าย ========================================
                                    dataSet = [], dataSetP1 = [], dataSetP2 = [];

                                    dataUrl = 'landoffice=' + landoffice;
                                    dataUrl += '&processSeqP1=' + row.data()[7][0]["PROCESS_SEQ"];

                                    $.ajax({
                                        type: 'POST',
                                        dataType: 'json',
                                        url: './queries/getOrderReciept.php',
                                        data: dataUrl,
                                        async: true,
                                        success: function (data) {
                                            dataSetP1 = data;
                                            dataUrl = 'landoffice=' + landoffice;

                                            dataUrl += '&processSeqP2=' + row.data()[7][1]["PROCESS_SEQ"];

                                        }
                                    }).done(function () {
                                        $.ajax({
                                            type: 'POST',
                                            dataType: 'json',
                                            url: './queries/getOrderReciept.php',
                                            data: dataUrl,
                                            success: function (data) {
                                                dataSetP2 = data;
                                                var num = 0, totalReg = 0, totalFin = 0, temp = [];
                                                console.log("ExpenseP1", dataSetP1);
                                                console.log("ExpenseP2", dataSetP2);

                                                for (let i = 0; i < dataSetP1[0].length; i++) {
                                                    temp = []
                                                    temp[0] = dataSetP1[0][i]['REG_EXPENSES'] ? dataSetP1[0][i]['REG_EXPENSES'] : "";
                                                    temp[1] = dataSetP1[0][i]['REG_MNY'] ? parseFloat(dataSetP1[0][i]['REG_MNY']).toLocaleString(undefined, {
                                                        minimumFractionDigits: 2,
                                                        maximumFractionDigits: 2
                                                    }) : "";
                                                    // temp[2] = data[0][j]['ORDER_NO'] ? data[0][j]['ORDER_NO'] : "";
                                                    // temp[3] = data[0][j]['RECEIPT_NO'] ? data[0][j]['RECEIPT_NO'] : "";
                                                    temp[2] = dataSetP1[0][i]['FIN_EXPENSES'] ? dataSetP1[0][i]['FIN_EXPENSES'] : "";
                                                    temp[3] = dataSetP1[0][i]['FIN_MNY'] ? parseFloat(dataSetP1[0][i]['FIN_MNY']).toLocaleString(undefined, {
                                                        minimumFractionDigits: 2,
                                                        maximumFractionDigits: 2
                                                    }) : "";

                                                    totalReg += dataSetP1[0][i]['REG_MNY'] ? parseFloat(dataSetP1[0][i]['REG_MNY']) : 0;
                                                    totalFin += dataSetP1[0][i]['FIN_MNY'] ? parseFloat(dataSetP1[0][i]['FIN_MNY']) : 0;

                                                    dataSet[num] = temp;
                                                    num++;


                                                }
                                                temp = []
                                                temp[0] = "รวม";
                                                temp[1] = totalReg.toLocaleString(undefined, {
                                                    minimumFractionDigits: 2,
                                                    maximumFractionDigits: 2
                                                })
                                                temp[2] = "รวม";
                                                temp[3] = totalFin.toLocaleString(undefined, {
                                                    minimumFractionDigits: 2,
                                                    maximumFractionDigits: 2
                                                })


                                                dataSet[num] = temp;
                                                num++;

                                                var expensep1_processTab = $('#expense-processTab-' + type + '-table-p1').DataTable({
                                                    "searching": false,
                                                    "ordering": false,
                                                    orderCellsTop: true,
                                                    "language": {
                                                        "emptyTable": "ไม่มีข้อมูลค่าใช้จ่าย"
                                                    },
                                                    "bPaginate": false,
                                                    'info': false,
                                                    "columnDefs": [{
                                                        "className": "dt-right",
                                                        "targets": [1, 3],

                                                    }, {
                                                        "className": "dt-left",
                                                        "targets": [0, 2],

                                                    }],
                                                    "drawCallback": function () {
                                                        currentPageCount = this.api().rows({
                                                            page: 'current'
                                                        }).data().length;
                                                    }
                                                });

                                                expensep1_processTab.search('').draw();
                                                expensep1_processTab.clear();
                                                expensep1_processTab.rows.add(dataSet);
                                                expensep1_processTab.draw();

                                                num = 0, totalReg = 0, totalFin = 0;
                                                dataSet = [];

                                                for (let i = 0; i < dataSetP2[0].length; i++) {
                                                    temp = []
                                                    temp[0] = dataSetP2[0][i]['REG_EXPENSES'] ? dataSetP2[0][i]['REG_EXPENSES'] : "";
                                                    temp[1] = dataSetP2[0][i]['REG_MNY'] ? parseFloat(dataSetP2[0][i]['REG_MNY']).toLocaleString(undefined, {
                                                        minimumFractionDigits: 2,
                                                        maximumFractionDigits: 2
                                                    }) : "";
                                                    // temp[2] = data[0][j]['ORDER_NO'] ? data[0][j]['ORDER_NO'] : "";
                                                    // temp[3] = data[0][j]['RECEIPT_NO'] ? data[0][j]['RECEIPT_NO'] : "";
                                                    temp[2] = dataSetP2[0][i]['FIN_EXPENSES'] ? dataSetP2[0][i]['FIN_EXPENSES'] : "";
                                                    temp[3] = dataSetP2[0][i]['FIN_MNY'] ? parseFloat(dataSetP2[0][i]['FIN_MNY']).toLocaleString(undefined, {
                                                        minimumFractionDigits: 2,
                                                        maximumFractionDigits: 2
                                                    }) : "";

                                                    totalReg += dataSetP2[0][i]['REG_MNY'] ? parseFloat(dataSetP2[0][i]['REG_MNY']) : 0;
                                                    totalFin += dataSetP2[0][i]['FIN_MNY'] ? parseFloat(dataSetP2[0][i]['FIN_MNY']) : 0;

                                                    dataSet[num] = temp;
                                                    num++;


                                                }
                                                temp = []
                                                temp[0] = "รวม";
                                                temp[1] = totalReg.toLocaleString(undefined, {
                                                    minimumFractionDigits: 2,
                                                    maximumFractionDigits: 2
                                                })
                                                temp[2] = "รวม";
                                                temp[3] = totalFin.toLocaleString(undefined, {
                                                    minimumFractionDigits: 2,
                                                    maximumFractionDigits: 2
                                                })


                                                dataSet[num] = temp;
                                                num++;

                                                var expensep2_processTab = $('#expense-processTab-' + type + '-table-p2').DataTable({
                                                    "searching": false,
                                                    "ordering": false,
                                                    orderCellsTop: true,
                                                    "language": {
                                                        "emptyTable": "ไม่มีข้อมูลค่าใช้จ่าย"
                                                    },
                                                    "bPaginate": false,
                                                    'info': false,
                                                    "columnDefs": [{
                                                        "className": "dt-right",
                                                        "targets": [1, 3],

                                                    }, {
                                                        "className": "dt-left",
                                                        "targets": [0, 2],

                                                    }],
                                                    "drawCallback": function () {
                                                        currentPageCount = this.api().rows({
                                                            page: 'current'
                                                        }).data().length;
                                                    }
                                                });

                                                expensep2_processTab.search('').draw();
                                                expensep2_processTab.clear();
                                                expensep2_processTab.rows.add(dataSet);
                                                expensep2_processTab.draw();


                                            }
                                        }).done(function () {
                                            //======================================== แบบพิมพ์ ========================================
                                            dataSet = [], dataSetP1 = [], dataSetP2 = [];
                                            dataUrl = 'landoffice=' + landoffice;

                                            dataUrl += '&processSeqP1=' + row.data()[7][0]["PROCESS_SEQ"];

                                            $.ajax({
                                                type: 'POST',
                                                dataType: 'json',
                                                url: './queries/getDocumentData.php',
                                                data: dataUrl,
                                                async: true,
                                                success: function (data) {
                                                    dataSetP1 = data;

                                                    dataUrl = 'landoffice=' + landoffice;
                                                    dataUrl += '&processSeqP2=' + row.data()[7][1]["PROCESS_SEQ"];
                                                }
                                            }).done(function () {
                                                $.ajax({
                                                    type: 'POST',
                                                    dataType: 'json',
                                                    url: './queries/getDocumentData.php',
                                                    data: dataUrl,
                                                    async: true,
                                                    success: function (data) {
                                                        dataSetP2 = data;
                                                        console.log("P1 ", dataSetP1);
                                                        console.log("P2 ", dataSetP2);
                                                        var num = 0, maxLoop = dataSetP1[0].length >= dataSetP2[0].length ? dataSetP1[0].length : dataSetP2[0].length;

                                                        for (let j = 0; j < maxLoop; j++) {
                                                            for (let i = 0; i < Object.keys(columnDocumentDetailMain).length; i++) {
                                                                var key = Object.keys(columnDocumentDetailMain)[i];
                                                                // if (!key) {
                                                                //     continue;
                                                                // }
                                                                temp = []
                                                                temp[0] = columnDocumentDetailMain[key];
                                                                temp[1] = dataSetP1[0][j] && dataSetP1[0][j][key] ? dataSetP1[0][j][key] : "-";
                                                                temp[2] = dataSetP2[0][j] && dataSetP2[0][j][key] ? dataSetP2[0][j][key] : "-";

                                                                dataSet[num] = temp;
                                                                num++;
                                                            }
                                                        }
                                                        if (dataSet.length <= 0) {
                                                            $('#tab6Panel-' + type + ' > ul > li:last-child > a').hover(function () {
                                                                $(this).addClass('disabled');
                                                                $('#tab6Panel-' + type + ' > ul > li:last-child').css("cursor", "not-allowed");
                                                            });
                                                        }

                                                        var document_processTab = $('#document-processTab-' + type + '-table').DataTable({
                                                            "searching": false,
                                                            "ordering": false,
                                                            orderCellsTop: true,
                                                            "language": {
                                                                "emptyTable": "ไม่มีข้อมูลแบบพิมพ์"
                                                            },
                                                            "bPaginate": false,
                                                            'info': false,
                                                            "createdRow": function (row, data, index) {
                                                                if (data[0].includes("ชื่อย่อแบบพิมพ์")) {
                                                                    $('td', row).css('background-color', 'rgb(236,236,236)');
                                                                }
                                                            },
                                                            "columnDefs": [{
                                                                "className": "dt-center",
                                                                "targets": [1, 2],

                                                            }, {
                                                                "className": "dt-left",
                                                                "targets": [0],

                                                            }],
                                                            "drawCallback": function () {
                                                                currentPageCount = this.api().rows({
                                                                    page: 'current'
                                                                }).data().length;
                                                            }
                                                        });
                                                        document_processTab.search('').draw();
                                                        document_processTab.clear();
                                                        document_processTab.rows.add(dataSet);
                                                        document_processTab.draw();


                                                        $('.overlayP1').hide();
                                                    }
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        })


                    })
                })
            })
        })
    })
}

function formatReceipt(d) {
    /*
        d[][0] = รายการค่าใช้จ่ายฝั่งทะเบียน
        d[][1] = ราคาฝั่งทะเบียน
        d[][2] = เลขที่ใบสั่ง
        d[][3] = เลขที่ใบเสร็จ
        d[][4] = รายการค่าใช้จ่ายฝั่งการเงิน
        d[][5] = ราคาฝั่งการเงิน
    */
    let order_no, receipt_no;
    for (let i = 0; i < d.length; i++) {
        for (let j = 0; j < 6; j++) {
            if (d[i][2]) {
                order_no = d[i][2];
            }
            if (d[i][3]) {
                receipt_no = d[i][3];
            }
        }

    }

    if (!order_no) {
        order_no = "ไม่พบข้อมูล"
    }
    if (!receipt_no) {
        receipt_no = "ไม่พบข้อมูล"
    }

    console.log(d);

    let total_reg = 0, total_fin = 0;

    var add = '<div class="row">\
        <div class="col">\
            <h5 style="text-align:center;">เลขที่ใบสั่ง ' + order_no + '</h5>\
        </div>\
        <div class="col">\
            <h5 style="text-align:center;">เลขที่ใบเสร็จ ' + receipt_no + '</h5>\
        </div>\
    </div>';

    add += '<table class="table"><thead>\
                <tr style="text-align: center;">\
                    <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>\
                    <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>\
                </tr>\
                <tr style="text-align: center;background-color:#e8e8e8;" >\
                    <th class="w-auto">รายการค่าใช้จ่าย</th>\
                    <th class="w-auto">ราคา (บาท)</th>\
                    <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>\
                    <th class="w-auto">ราคา (บาท)</th>\
                </tr>\
            </thead>';
    for (i = 0; i < d.length; i++) {
        add += '<tr style="background-color:#e8e8e8;"><td class="w-auto">' + d[i][0] + '</td>';
        add += '<td class="w-auto" style="text-align:right;">' + d[i][1] + '</td>';
        add += '<td class="w-auto" style="border-left:1px solid black;">' + d[i][4] + '</td>';
        add += '<td class="w-auto" style="text-align:right;">' + d[i][5] + '</td></tr>';

        total_reg += d[i][1] ? parseInt(d[i][1]) : 0;
        total_fin += d[i][5] ? parseInt(d[i][5]) : 0;
    }
    add += '<tr style="background-color:#e8e8e8;font-weight:bold;"><td class="w-auto">รวม</td>';
    add += '<td class="w-auto" style="text-align:right;">' + total_reg.toLocaleString() + '</td>';
    add += '<td class="w-auto" style="border-left:1px solid black;">รวม</td>';
    add += '<td class="w-auto" style="text-align:right;">' + total_fin.toLocaleString() + '</td></tr>';
    add += '</table>';

    return add;
}

function getParcelData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;
    parcelNo = document.getElementById('parcel_no').value;

    if (amphur != '' && amphur != 'all') dataUrl += '&amphur=' + document.getElementById('amphur-select-' + type).value;
    if (tambon != '' && tambon != 'all') dataUrl += '&tambon=' + document.getElementById('tambon-select-' + type).value;
    if (parcelNo != '') dataUrl += '&parcelNo=' + parcelNo;
    // dataUrl += '&offset=0'
    dataUrl += '&check=' + typeSeq;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getAllData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['PARCEL_NO'] ? data[0][i]['PARCEL_NO'] : "";
                temp[2] = data[0][i]['AMPHUR_NAME'] ? data[0][i]['AMPHUR_NAME'] : "";
                temp[3] = data[0][i]['TAMBOL_NAME'] ? data[0][i]['TAMBOL_NAME'] : "";
                temp[4] = '<a onClick="showDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>'
                temp[5] = data[0][i]['PARCEL_SEQ_P1'] ? data[0][i]['PARCEL_SEQ_P1'] : "";
                temp[6] = data[0][i]['PARCEL_SEQ_P2'] ? data[0][i]['PARCEL_SEQ_P2'] : "";

                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}

function getParcelLandData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;
    parcelNo = document.getElementById('parcelLandNo-' + type).value;
    if (typeSeq == 5 || typeSeq == 23) {
        moo = document.getElementById('parcelLandMoo-' + type).value ? document.getElementById('parcelLandMoo-' + type).value : '';
    }
    if (typeSeq == 8) {
        parcelYear = document.getElementById('parcelYear').value ? document.getElementById('parcelYear').value : '';
    }

    if (amphur != '' && amphur != 'all') dataUrl += '&amphur=' + document.getElementById('amphur-select-' + type).value;
    if (tambon != '' && tambon != 'all') dataUrl += '&tambon=' + document.getElementById('tambon-select-' + type).value;
    if (parcelNo != '') dataUrl += '&parcelNo=' + parcelNo;
    if (moo != '') dataUrl += '&parcelMoo=' + moo;
    if (parcelYear != '') dataUrl += '&parcelYear=' + parcelYear;
    // dataUrl += '&offset=0'
    dataUrl += '&check=' + typeSeq;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getAllData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {

                temp = []
                if (typeSeq == 8) { //nsl
                    temp[0] = i + 1;
                    temp[1] = data[0][i]['PARCEL_LAND_NO'] ? data[0][i]['PARCEL_LAND_NO'] : "";
                    temp[2] = data[0][i]['PARCEL_LAND_NAME'] ? data[0][i]['PARCEL_LAND_NAME'] : "";
                    temp[3] = data[0][i]['YEAR'] ? data[0][i]['YEAR'] : "";
                    temp[4] = data[0][i]['AMPHUR_NAME'] ? data[0][i]['AMPHUR_NAME'] : "";
                    temp[5] = data[0][i]['TAMBOL_NAME'] ? data[0][i]['TAMBOL_NAME'] : "";
                    temp[6] = '<a onClick="showDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>'
                    temp[7] = data[0][i]['PARCEL_LAND_SEQ_P1'] ? data[0][i]['PARCEL_LAND_SEQ_P1'] : "";
                    temp[8] = data[0][i]['PARCEL_LAND_SEQ_P2'] ? data[0][i]['PARCEL_LAND_SEQ_P2'] : "";
                    temp[9] = data[0][i]['STS'] ? data[0][i]['STS'] : "";
                }
                else if (typeSeq == 5 || typeSeq == 23 || typeSeq == 8) { //ns3 or subNsl
                    temp[0] = i + 1;
                    temp[1] = data[0][i]['PARCEL_LAND_NO'] ? data[0][i]['PARCEL_LAND_NO'] : "";
                    temp[2] = data[0][i]['MOO'] ? data[0][i]['MOO'] : ""
                    temp[3] = data[0][i]['AMPHUR_NAME'] ? data[0][i]['AMPHUR_NAME'] : "";
                    temp[4] = data[0][i]['TAMBOL_NAME'] ? data[0][i]['TAMBOL_NAME'] : "";
                    temp[5] = '<a onClick="showDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>'
                    temp[6] = data[0][i]['PARCEL_LAND_SEQ_P1'] ? data[0][i]['PARCEL_LAND_SEQ_P1'] : "";
                    temp[7] = data[0][i]['PARCEL_LAND_SEQ_P2'] ? data[0][i]['PARCEL_LAND_SEQ_P2'] : "";
                    temp[8] = data[0][i]['STS'] ? data[0][i]['STS'] : "";
                }
                else {
                    temp[0] = i + 1;
                    temp[1] = data[0][i]['PARCEL_LAND_NO'] ? data[0][i]['PARCEL_LAND_NO'] : "";
                    temp[2] = data[0][i]['AMPHUR_NAME'] ? data[0][i]['AMPHUR_NAME'] : "";
                    temp[3] = data[0][i]['TAMBOL_NAME'] ? data[0][i]['TAMBOL_NAME'] : "";
                    temp[4] = '<a onClick="showDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>'
                    temp[5] = data[0][i]['PARCEL_LAND_SEQ_P1'] ? data[0][i]['PARCEL_LAND_SEQ_P1'] : "";
                    temp[6] = data[0][i]['PARCEL_LAND_SEQ_P2'] ? data[0][i]['PARCEL_LAND_SEQ_P2'] : "";
                    temp[7] = data[0][i]['STS'] ? data[0][i]['STS'] : "";
                }
                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();
}
function getCondoData(table, e) {

    var dataUrl = 'landoffice=' + landoffice;
    parcelNo = document.getElementById('condo_reg').value

    if (amphur != '' && amphur != 'all') dataUrl += '&amphur=' + document.getElementById('amphur-select-' + type).value;
    if (tambon != '' && tambon != 'all') dataUrl += '&tambon=' + document.getElementById('tambon-select-' + type).value;
    if (parcelNo != '') dataUrl += '&parcelNo=' + parcelNo;
    if (document.getElementById('condo_name').value) dataUrl += '&condoName=' + document.getElementById('condo_name').value;

    // dataUrl += '&offset=0'
    dataUrl += '&check=' + typeSeq;


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getAllData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {

                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['AMPHUR_NAME'] ? data[0][i]['AMPHUR_NAME'] : "";
                temp[2] = data[0][i]['TAMBOL_NAME'] ? data[0][i]['TAMBOL_NAME'] : "";
                temp[3] = data[0][i]['CONDO_REG_YEAR'] ? data[0][i]['CONDO_REG_YEAR'] : "";
                temp[4] = data[0][i]['CONDO_NAME_TH'] ? data[0][i]['CONDO_NAME_TH'] : "";
                temp[5] = '<a onClick="showDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>'
                temp[6] = data[0][i]['CONDO_SEQ_P1'] ? data[0][i]['CONDO_SEQ_P1'] : "";
                temp[7] = data[0][i]['CONDO_SEQ_P2'] ? data[0][i]['CONDO_SEQ_P2'] : "";
                temp[8] = data[0][i]['STS'] ? data[0][i]['STS'] : "";

                dataSet[num] = temp;
                num++;

            }

            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();
}
function getCondoroomData(table, e) {

    var dataUrl = 'landoffice=' + landoffice;
    parcelNo = document.getElementById('condoroom_reg').value

    if (amphur != '' && amphur != 'all') dataUrl += '&amphur=' + document.getElementById('amphur-select-' + type).value;
    if (tambon != '' && tambon != 'all') dataUrl += '&tambon=' + document.getElementById('tambon-select-' + type).value;
    if (parcelNo != '') dataUrl += '&parcelNo=' + parcelNo;
    if (document.getElementById('condoroom_name').value) dataUrl += '&condoName=' + document.getElementById('condoroom_name').value
    if (document.getElementById('condoroom_no').value) dataUrl += '&condoroomNo=' + document.getElementById('condoroom_no').value

    // dataUrl += '&offset=0'
    dataUrl += '&check=' + typeSeq;


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getAllData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {

                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['AMPHUR_NAME'] ? data[0][i]['AMPHUR_NAME'] : "";
                temp[2] = data[0][i]['TAMBOL_NAME'] ? data[0][i]['TAMBOL_NAME'] : "";
                temp[3] = data[0][i]['CONDOROOM_RNO'] ? data[0][i]['CONDOROOM_RNO'] : "";
                temp[4] = data[0][i]['CONDO_REG_YEAR'] ? data[0][i]['CONDO_REG_YEAR'] : "";
                temp[5] = data[0][i]['CONDO_NAME_TH'] ? data[0][i]['CONDO_NAME_TH'] : "";
                temp[6] = '<a onClick="showDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>'
                temp[7] = data[0][i]['CONDOROOM_SEQ_P1'] ? data[0][i]['CONDOROOM_SEQ_P1'] : "";
                temp[8] = data[0][i]['CONDOROOM_SEQ_P2'] ? data[0][i]['CONDOROOM_SEQ_P2'] : "";
                temp[9] = data[0][i]['STS'] ? data[0][i]['STS'] : "";

                dataSet[num] = temp;
                num++;


            }


            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();
}

function getConstructData(table, e) {

    var dataUrl = 'landoffice=' + landoffice;

    if (amphur != '' && amphur != 'all') dataUrl += '&amphur=' + document.getElementById('amphur-select-' + type).value;
    if (tambon != '' && tambon != 'all') dataUrl += '&tambon=' + document.getElementById('tambon-select-' + type).value;

    if (document.getElementById('construct_hid').value) dataUrl += '&constrHid=' + document.getElementById('construct_hid').value
    if (document.getElementById('construct_hno').value) dataUrl += '&constrHno=' + document.getElementById('construct_hno').value
    if (document.getElementById('construct_name').value) dataUrl += '&constrName=' + document.getElementById('construct_name').value
    if (document.getElementById('construct_moo').value) dataUrl += '&constrMoo=' + document.getElementById('construct_moo').value

    // dataUrl += '&offset=0'
    dataUrl += '&check=c';


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getAllData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {

                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['AMPHUR_NAME'] ? data[0][i]['AMPHUR_NAME'] : "";
                temp[2] = data[0][i]['TAMBOL_NAME'] ? data[0][i]['TAMBOL_NAME'] : "";
                temp[3] = data[0][i]['CONSTRUCT_ADDR_HID'] ? data[0][i]['CONSTRUCT_ADDR_HID'] : "";
                temp[4] = data[0][i]['CONSTRUCT_ADDR_HNO'] ? data[0][i]['CONSTRUCT_ADDR_HNO'] : "";
                temp[5] = data[0][i]['CONSTRUCT_ADDR_MOO'] ? data[0][i]['CONSTRUCT_ADDR_MOO'] : "";
                temp[6] = data[0][i]['CONSTRUCTION_NAME'] ? data[0][i]['CONSTRUCTION_NAME'] : "";
                temp[7] = '<a onClick="showDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>'
                temp[8] = data[0][i]['CONSTRUCT_SEQ_P1'] ? data[0][i]['CONSTRUCT_SEQ_P1'] : "";
                temp[9] = data[0][i]['CONSTRUCT_SEQ_P2'] ? data[0][i]['CONSTRUCT_SEQ_P2'] : "";
                temp[10] = data[0][i]['STS'] ? data[0][i]['STS'] : "";

                dataSet[num] = temp;
                num++;


            }


            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();
}

// ================================= Overall ================================= 
function getOverAll() {
    var dataUrl = 'landoffice=' + landoffice;
    $('.overlay-count').show();

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/queryDataEntry.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [], num = 0, j = 0;
            console.log(data);
            for (i = 0; i < data.length; i++) {
                n = num + 1
                temp = [];

                temp[0] = n;
                temp[1] = data[i]['PRINTPLATE_TYPE_NAME'];
                temp[2] = Number(data[i]['RECEIVE']).toLocaleString();
                temp[3] = Number(data[i]['MIGRATE_SUCCESS']).toLocaleString();
                temp[4] = Number(data[i]['MIGRATE_ERROR']).toLocaleString();


                dataSet[num] = temp;
                num++;
            }
            var table = $('#table-overall').DataTable({
                "searching": false,
                "ordering": false,
                orderCellsTop: true,
                "bPaginate": false,
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs": [{
                    "className": "dt-center",
                    "targets": [0, 2, 3, 4],

                }, {
                    "className": "dt-left",
                    "targets": [1],

                }],
                'info': false,
                "lengthChange": false,
            });

            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();

            $('.overlay-count').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');



}
function exportReg(type, sts, landoffice) {

    window.open('./queries/excelRegData.php?' + "check=" + typeSeq + "&sts=" + sts + "&landoffice=" + landoffice + "&branchName=" + branchName, true);


}
function exportErrorReg(type, sts, landoffice) {

    window.open('./queries/excelRegErrorData.php?' + "check=" + typeSeq + "&sts=" + sts + "&landoffice=" + landoffice + "&branchName=" + branchName, true);


}





var searchFunction = function (e) {
    console.log(typeSeq);

    $('.overlay-count').hide();

    $('.overlay-main').show();
    $('#detail-heading').css("visibility", "hidden");
    $('.detail-card').css("visibility", "hidden");

    var table = $('#table-' + type).DataTable({
        "pageLength": 10,
        "pagingType": "simple_numbers",
        "searching": false,
        "ordering": false,
        orderCellsTop: true,
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs": [{
            "className": "text-center",
            "targets": "_all",

        }],
        'info': true,
        "drawCallback": function (settings) {


        }
    }).on('page.dt', function () {
        var info = table.page.info();

    });
    table.clear();
    table.draw();

    if (typeSeq == 1) {
        getParcelData(table, e);
    }
    else if (typeSeq == 9) {
        getCondoroomData(table, e);
    }
    else if (typeSeq == 13) {
        getCondoData(table, e);
    }
    else if (typeSeq == 'c') {
        getConstructData(table, e);
    }
    else {
        getParcelLandData(table, e);
    }



    $('#table-' + type + ' tbody').on('click', 'tr', function () {
        let table = $('#table-' + type).DataTable();
        table.$('tr.row-click').removeClass('row-click');
        $(this).addClass('row-click');
    });
    $('#table-p1-' + type).on('click', 'tr', function () {
        let table = $('#table-p1-' + type).DataTable();
        table.$('tr.row-click').removeClass('row-click');
        $(this).addClass('row-click');

        let table2 = $('#table-p2-' + type).DataTable();
        table2.$('tr.row-click').removeClass('row-click');
        index = table.row(this).index();
        table2.rows(index).nodes().to$().addClass('row-click')
    });
    $('#table-p2-' + type).on('click', 'tr', function () {
        let table = $('#table-p2-' + type).DataTable();
        table.$('tr.row-click').removeClass('row-click');
        $(this).addClass('row-click');

        let table2 = $('#table-p1-' + type).DataTable();
        table2.$('tr.row-click').removeClass('row-click');
        index = table.row(this).index();
        table2.rows(index).nodes().to$().addClass('row-click')
    });


}

$(document).ready(function () {
    $('.sidebar-menu').click(function () {
        $('ul a.nav-link').removeClass('active');
        $(this).parent().children('a').addClass('active');
    });
    //redirect to login page when bypass by URL
    if (getCookie("landoffice") == "") {
        location.href = "./index.html";
    }


    $('.overlay-main').hide();

    $('.overlayP2').hide();
    $('.tab-pane').hide();

    $.fn.dataTable.ext.errMode = 'none';

    branchName = getCookie('branchName');
    landoffice = getCookie('landoffice');

    document.getElementById("landoffice_name").innerHTML = '<i class="fa-lg fa fa-file"></i> ระบบตรวจสอบข้อมูล Transaction<br>[' + branchName + ']';

    $('.nav-pills-main a').on('show.bs.tab', function () {

        $("#search-" + type).off();
        $(current_href).hide();
        var href = $(this).attr('href');
        current_href = href;
        $(current_href).show();
        type = href.split('-')[1];
        console.log(current_href);



        switch (type) {
            case 'overall':
                getOverAll();
                break;
            case 'chanode':
                typeSeq = 1;
                break;
            case 'chanodetrajong':
                typeSeq = 2;
                break;
            case 'trajong':
                typeSeq = 3;
                break;
            case 'ns3a':
                typeSeq = 4;
                break;
            case 'ns3':
                typeSeq = 5;
                break;
            case 'nsl':
                typeSeq = 8;
                break;
            case 'condoroom':
                typeSeq = 9;
                break;
            case 'condo':
                typeSeq = 13;
                break;
            case 'subNsl':
                typeSeq = 23;
                break;
            case 'construct':
                typeSeq = 'c';
                break;
            default:
                break;
        }
        $("#search-" + type).on("click", searchFunction);

        $('#export-success-' + type).on("click", function () {
            exportReg(type, 'e', landoffice);
        });
        $('#export-diff-' + type).on("click", function () {
            exportReg(type, 'e', landoffice);
        });
        $('#export-error-' + type).on("click", function () {
            exportErrorReg(type, 's', landoffice);
        });

        getAmphur("#amphur-select-" + type, landoffice);

        $("#amphur-select-" + type).on('change', function () {
            amphur = this.value;

            if (amphur != '') {
                getTambon("#tambon-select-" + type, landoffice, amphur);
            } else {
                $(tambonId).empty();
                tambon = '';
            }
        });

        $("#tambon-select-" + type).on('change', function () {

            tambon = this.value;
        });



    });

});