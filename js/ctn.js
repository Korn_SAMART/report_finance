var searchFunction = function (e) {

    if (ctnType != 16) $('.overlay-main').show();


    var table = $('#table-ctn-' + type).DataTable({
        "pageLength": 10,
        "pagingType": "simple_numbers",
        "searching": false,
        "ordering": false,
        orderCellsTop: true,
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs": [{
            "className": "diff-p", "targets": ctnType == 15 || ctnType == 16 ? [8] : ctnType == 1 || ctnType == 2 ? [7]
                : ctnType == 3 || ctnType == 4 || ctnType == 14 ? [6]
                    : ctnType == 7 || ctnType == 10 || ctnType == 11 || ctnType == 12 ? [5]
                        : [4]


        }, {
            "className": "text-center",
            "targets": "_all",

        }, {
            "className": "text-center",
            "targets": "_all",

        }],
        'info': true,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    })
    table.clear();
    table.draw();

    switch (ctnType) {
        case 1:
        case 2:
            getReceiveDocData(table, e);
            break;
        case 3:
        case 4:
            getSentDocData(table, e);
            break;
        case 5:
        case 6:

            break;
        case 7:
            getLandofficeNoData(table, e);
            break;
        case 8:
            getConstructionData(table, e);
            break;
        case 9:

            break;
        case 10:
        case 11:
        case 12:
            getDocumentData(table, e);
            break;
        case 13:
            break;
        case 14:
            getDistrictData(table, e);
            break;
        case 15:
            getADMData(table, e);
            break;
        case 16:
            getAbsenceCount();
        default:
            break;
    }

    $('#table-ctn-' + type + ' tbody').on('click', 'tr', function () {
        let table = $('#table-ctn-' + type).DataTable();
        table.$('tr.row-click').removeClass('row-click');
        $(this).addClass('row-click');
    });





}
var absenceDetailIndex = 0, absenceNum = 0;
var dataSetAbsence = [];
async function getAbsenceDetail(psn_seq, record_seq, index) {
    if (absenceDetailIndex >= record_seq.length) {

        table.search('').draw();
        table.clear();
        table.rows.add(dataSetAbsence);

        table.draw();
        $('.overlay-main').hide();
        return;
    }

    dataUrl = 'landoffice=' + landoffice

    //psn_seq
    dataUrl += '&psn_seq=' + psn_seq;

    //record_seq
    dataUrl += '&record_seq=' + record_seq[absenceDetailIndex];
    dataUrl += '&check=18';


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnData.php',
        data: dataUrl,
        success: function (data) {
            temp = []
            temp[0] = absenceDetailIndex + 1;
            temp[1] = data[0][0]['PSN_PID_P1'] ? data[0][0]['PSN_PID_P1'] : "";
            temp[2] = data[0][0]['TITLE_NAME_P1'] ? data[0][0]['TITLE_NAME_P1'] + " " : "";
            temp[3] = data[0][0]['PSN_FNAME_P1'] ? data[0][0]['PSN_FNAME_P1'] + " " : "";
            temp[3] += data[0][0]['PSN_LNAME_P1'] ? data[0][0]['PSN_LNAME_P1'] : "";
            temp[4] = data[0][0]['FISCAL_YEAR_P1'] ? data[0][0]['FISCAL_YEAR_P1'] : "";
            temp[5] = data[0][0]['LEAVE_TYPE_NAME_P1'] ? data[0][0]['LEAVE_TYPE_NAME_P1'] : "";
            temp[6] = data[0][0]['LEAVE_FROM_DATE_P1'] ? data[0][0]['LEAVE_FROM_DATE_P1'] : "";
            temp[7] = data[0][0]['LEAVE_TO_DATE_P1'] ? data[0][0]['LEAVE_TO_DATE_P1'] : "";
            temp[8] = data[0][0]['TOTAL_DAY_P1'] ? data[0][0]['TOTAL_DAY_P1'] : "";

            temp[9] = data[0][0]['PSN_PID_P2'] ? data[0][0]['PSN_PID_P2'] : "";
            temp[10] = data[0][0]['TITLE_NAME_P2'] ? data[0][0]['TITLE_NAME_P2'] + " " : "";
            temp[11] = data[0][0]['PSN_FNAME_P2'] ? data[0][0]['PSN_FNAME_P2'] + " " : "";
            temp[11] += data[0][0]['PSN_LNAME_P2'] ? data[0][0]['PSN_LNAME_P2'] : "";
            temp[12] = data[0][0]['FISCAL_YEAR_P2'] ? data[0][0]['FISCAL_YEAR_P2'] : "";
            temp[13] = data[0][0]['LEAVE_TYPE_NAME_P2'] ? data[0][0]['LEAVE_TYPE_NAME_P2'] : "";
            temp[14] = data[0][0]['LEAVE_FROM_DATE_P2'] ? data[0][0]['LEAVE_FROM_DATE_P2'] : "";
            temp[15] = data[0][0]['LEAVE_TO_DATE_P2'] ? data[0][0]['LEAVE_TO_DATE_P2'] : "";
            temp[16] = data[0][0]['TOTAL_DAY_P2'] ? data[0][0]['TOTAL_DAY_P2'] : "";

            dataSetAbsence[absenceNum] = temp;
            absenceNum++;
            absenceDetailIndex++;
            getAbsenceDetail(psn_seq, record_seq, index);

        }
    })






}
function showCTNDetail(e) {
    var tr = e.parent().closest('tr');
    var row = $('#table-ctn-' + type).DataTable().row(tr);
    console.log(row.data());

    $('#tabPanel-' + type).css("visibility", "visible");
    $('.detailTab-' + type + ' a ').click();


    $('.overlay-detail').show();


    var dataUrl = 'landoffice=' + landoffice;
    if (ctnType >= 10 && ctnType <= 12) {
        if (row.data()[11] != "") dataUrl += '&revhdSeqP1=' + row.data()[11];
        if (row.data()[12] != "") dataUrl += '&revhdSeqP2=' + row.data()[12];
    }



    //=================================================== ISS Detail ===================================================
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnIssData.php',
        data: dataUrl,
        success: function (data) {
            var num = 0, dataSet = [];
            var count = data[0].length > 0 ? Object.keys(data[0][0]).length : 0;
            var loop = dataUrl.includes("revhdSeqP1") && dataUrl.includes("revhdSeqP2") ? 2 : 1;
            console.log("detailData = ", data);

            for (let i = 0; i < count; i += loop) {
                var key = Object.keys(data[0][0])[i];

                temp = []
                temp[0] = columnCtnFinDetailMain[key]
                temp[1] = data[0][0][key] ? data[0][0][key] : "-";
                temp[2] = data[0][0][key.replace("P1", "P2")] ? data[0][0][key.replace("P1", "P2")] : "-";

                dataSet[num] = temp;
                num++;

            }
            var iss_detailTab = $('#iss-detailTab-' + type + '-table').DataTable({
                "searching": false,
                "ordering": false,
                orderCellsTop: true,
                "language": {
                    "emptyTable": "ไม่มีข้อมูลการเบิกจ่ายพัสดุ"
                },
                "bPaginate": false,
                'info': false,
                "columnDefs": [{
                    "className": "dt-center",
                    "targets": [1, 2],

                }, {
                    "className": "dt-left",
                    "targets": [0],

                }],
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });

            iss_detailTab.search('').draw();
            iss_detailTab.clear();
            iss_detailTab.rows.add(dataSet);
            iss_detailTab.draw();
            $('.overlay-detail').hide();



        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    //=================================================== RTN Detail ===================================================
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnRtnData.php',
        data: dataUrl,
        success: function (data) {
            var num = 0, dataSet = [];
            var count = data[0].length > 0 ? Object.keys(data[0][0]).length : 0;
            var loop = dataUrl.includes("revhdSeqP1") && dataUrl.includes("revhdSeqP2") ? 2 : 1;
            console.log("detailData = ", data);

            for (let i = 0; i < count; i += loop) {
                var key = Object.keys(data[0][0])[i];

                temp = []
                temp[0] = columnCtnFinRtnDetailMain[key]
                temp[1] = data[0][0][key] ? data[0][0][key] : "-";
                temp[2] = data[0][0][key.replace("P1", "P2")] ? data[0][0][key.replace("P1", "P2")] : "-";

                dataSet[num] = temp;
                num++;

            }
            var rtn_detailTab = $('#rtn-detailTab-' + type + '-table').DataTable({
                "searching": false,
                "ordering": false,
                orderCellsTop: true,
                "language": {
                    "emptyTable": "ไม่มีข้อมูลการส่งคืนพัสดุ"
                },
                "bPaginate": false,
                'info': false,
                "columnDefs": [{
                    "className": "dt-center",
                    "targets": [1, 2],

                }, {
                    "className": "dt-left",
                    "targets": [0],

                }],
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });

            rtn_detailTab.search('').draw();
            rtn_detailTab.clear();
            rtn_detailTab.rows.add(dataSet);
            rtn_detailTab.draw();
            $('.overlay-detail').hide();



        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    // //=================================================== FEESURVEY Detail ===================================================
    // $.ajax({
    //     type: 'POST',
    //     dataType: 'json',
    //     url: './sva/getSvaExpenseData.php',
    //     data: dataUrl,
    //     success: function (data) {
    //         var dataSetP1 = [], dataSetP2 = []
    //         var num = 0;

    //         var count = Object.keys(data[0]).length;
    //         if (count > 0) { //ไม่มีข้อมูลโฉนด
    //             for (let i = 0; i < count; i++) {
    //                 temp = []

    //                 temp[0] = data[0][i]['SVA_FEESURVEY_NAME_P1'] ? data[0][i]['SVA_FEESURVEY_NAME_P1'] : "";
    //                 temp[1] = data[0][i]['SVA_FEESURVEY_AMOUNT_P1'] ? Number(data[0][i]['SVA_FEESURVEY_AMOUNT_P1']).toLocaleString(undefined, {
    //                     minimumFractionDigits: 2,
    //                     maximumFractionDigits: 2
    //                 }) : "";

    //                 dataSetP1[num] = temp;

    //                 temp = []
    //                 temp[0] = data[0][i]['SVA_FEESURVEY_NAME_P2'] ? data[0][i]['SVA_FEESURVEY_NAME_P2'] : "";
    //                 temp[1] = data[0][i]['SVA_FEESURVEY_AMOUNT_P2'] ? Number(data[0][i]['SVA_FEESURVEY_AMOUNT_P2']).toLocaleString(undefined, {
    //                     minimumFractionDigits: 2,
    //                     maximumFractionDigits: 2
    //                 }) : "";

    //                 dataSetP2[num] = temp;
    //                 num++;

    //             }

    //         }


    //         $('#tabPanel-' + type + ' > ul > li:last-child > a').css("display", "block");

    //         var table_p1 = $('#expense-detailTab-' + type + '-table-p1').DataTable({
    //             "searching": false,
    //             "ordering": false,
    //             'info': false,
    //             orderCellsTop: true,
    //             "language": {
    //                 "emptyTable": "ไม่มีข้อมูล"
    //             },
    //             "columnDefs": [{
    //                 "className": "dt-left",
    //                 "targets": [0, 2],

    //             }, {
    //                 "className": "dt-right",
    //                 "targets": [1, 3],

    //             }],
    //             "lengthChange": false,
    //             "drawCallback": function () {
    //                 currentPageCount = this.api().rows({
    //                     page: 'current'
    //                 }).data().length;
    //             }
    //         });
    //         table_p1.clear();
    //         table_p1.draw();

    //         var table_p2 = $('#expense-detailTab-' + type + '-table-p2').DataTable({
    //             "searching": false,
    //             "ordering": false,
    //             orderCellsTop: true,
    //             "language": {
    //                 "emptyTable": "ไม่มีข้อมูล"
    //             },
    //             "columnDefs": [{
    //                 "className": "dt-left",
    //                 "targets": [0, 2],

    //             }, {
    //                 "className": "dt-right",
    //                 "targets": [1, 3],

    //             }],
    //             'info': false,
    //             "lengthChange": false,
    //             "drawCallback": function () {
    //                 currentPageCount = this.api().rows({
    //                     page: 'current'
    //                 }).data().length;
    //             }
    //         });

    //         table_p2.clear();
    //         table_p2.draw();

    //         table_p1.search('').draw();
    //         table_p1.clear();
    //         table_p1.rows.add(dataSetP1);
    //         table_p1.draw();

    //         table_p2.search('').draw();
    //         table_p2.clear();
    //         table_p2.rows.add(dataSetP2);
    //         table_p2.draw();

    //         $('.overlayP2').hide();


    //     }
    // }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

}


function showDetail(e, index) {
    var tr = e.parent().closest('tr');
    var row = $('#table-ctn-absence-count-p' + index).DataTable().row(tr);

    var dataUrl = 'landoffice=' + landoffice
    $('.overlay-main').show();
    console.log(row.data());

    //psn_seq
    if (row.data()[17] != "") dataUrl += '&psn_seq=' + row.data()[17];

    //year
    if (row.data()[3] != "") dataUrl += '&year=' + row.data()[3];


    dataUrl += '&check=17';

    var record_seq = [];

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnData.php',
        data: dataUrl,
        success: function (data) {
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                record_seq.push(data[0][i]['LEAVE_RECORD_SEQ']);
            }

        }
    }).done(async function () {
        dataSetAbsenceP1 = [], dataSetAbsenceP2 = [];
        absenceDetailIndex = 0, absenceNum = 0;
        table = $('#table-ctn-absence').DataTable({
            "pageLength": 5,
            "pagingType": "simple_numbers",
            "searching": false,
            "ordering": false,
            orderCellsTop: true,
            "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
            "language": {
                "emptyTable": "ไม่มีข้อมูล"
            },
            "columnDefs": [
                { "className": "diff-p", "targets": [8] }, {
                    "className": "text-center",
                    "targets": "_all",

                },],
            'info': true,
            "drawCallback": function () {
                currentPageCount = this.api().rows({
                    page: 'current'
                }).data().length;
            }
        })
        table.clear();
        table.draw();
        await getAbsenceDetail(row.data()[17], record_seq, index);

    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');


}

function getReceiveDocData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;

    console.log(ctnType);

    if (ctnType == 1) {
        console.log(document.getElementById('date-ctn-recv_nosecret').value);
        if (document.getElementById('recv_reg-ctn-recv_nosecret').value) dataUrl += '&register_no=' + document.getElementById('recv_reg-ctn-recv_nosecret').value
        if (document.getElementById('recv_no-ctn-recv_nosecret').value) dataUrl += '&doc_no=' + document.getElementById('recv_no-ctn-recv_nosecret').value
        if (document.getElementById('date-ctn-recv_nosecret').value) dataUrl += '&doc_date=' + document.getElementById('date-ctn-recv_nosecret').value
    }
    else {
        if (document.getElementById('recv_reg-ctn-recv_secret').value) dataUrl += '&register_no=' + document.getElementById('recv_reg-ctn-recv_secret').value
        if (document.getElementById('recv_no-ctn-recv_secret').value) dataUrl += '&doc_no=' + document.getElementById('recv_no-ctn-recv_secret').value
        if (document.getElementById('type-ctn-recv_secret').value) dataUrl += '&secret_type=' + document.getElementById('type-ctn-recv_secret').value
        if (document.getElementById('date-ctn-recv_secret').value) dataUrl += '&doc_date=' + document.getElementById('date-ctn-recv_secret').value
    }

    dataUrl += '&check=' + ctnType;


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;



                temp[1] = data[0][i]['REGISTER_NO_P1'] ? data[0][i]['REGISTER_NO_P1'] : "";
                temp[2] = data[0][i]['DOC_NUMBER_P1'] ? data[0][i]['DOC_NUMBER_P1'] : "";
                temp[3] = data[0][i]['DOC_DATE_P1'] ? data[0][i]['DOC_DATE_P1'] : "";
                temp[4] = data[0][i]['RECEIVE_DATE_P1'] ? data[0][i]['RECEIVE_DATE_P1'] : "";
                temp[5] = data[0][i]['DOC_FROM_TEXT_P1'] ? data[0][i]['DOC_FROM_TEXT_P1'] : "";
                temp[6] = data[0][i]['DOC_TO_P1'] ? data[0][i]['DOC_TO_P1'] : "";
                temp[7] = data[0][i]['SUBJECT_P1'] ? data[0][i]['SUBJECT_P1'] : "";

                temp[8] = data[0][i]['REGISTER_NO_P2'] ? data[0][i]['REGISTER_NO_P2'] : "";
                temp[9] = data[0][i]['DOC_NUMBER_P2'] ? data[0][i]['DOC_NUMBER_P2'] : "";
                temp[10] = data[0][i]['DOC_DATE_P2'] ? data[0][i]['DOC_DATE_P2'] : "";
                temp[11] = data[0][i]['RECEIVE_DATE_P2'] ? data[0][i]['RECEIVE_DATE_P2'] : "";
                temp[12] = data[0][i]['DOC_FROM_TEXT_P2'] ? data[0][i]['DOC_FROM_TEXT_P2'] : "";
                temp[13] = data[0][i]['DOC_TO_P2'] ? data[0][i]['DOC_TO_P2'] : "";
                temp[14] = data[0][i]['SUBJECT_P2'] ? data[0][i]['SUBJECT_P2'] : "";


                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}

function getSentDocData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;

    console.log(ctnType);

    if (ctnType == 3) {
        if (document.getElementById('recv_reg-ctn-sent_nosecret').value) dataUrl += '&register_no=' + document.getElementById('recv_reg-ctn-sent_nosecret').value
        if (document.getElementById('recv_no-ctn-sent_nosecret').value) dataUrl += '&doc_no=' + document.getElementById('recv_no-ctn-sent_nosecret').value
        // if (document.getElementById('recv_sign_date-ctn-sent_nosecret').value) dataUrl += '&doc_date=' + document.getElementById('recv_sign_date-ctn-sent_nosecret').value
        // if (document.getElementById('recv_date-ctn-sent_nosecret').value) dataUrl += '&recv_date=' + document.getElementById('recv_date-ctn-sent_nosecret').value
    }
    else {
        if (document.getElementById('recv_reg-ctn-sent_secret').value) dataUrl += '&register_no=' + document.getElementById('recv_reg-ctn-sent_secret').value
        if (document.getElementById('recv_no-ctn-sent_secret').value) dataUrl += '&doc_no=' + document.getElementById('recv_no-ctn-sent_secret').value
        if (document.getElementById('type-ctn-sent_secret').value) dataUrl += '&secret_type=' + document.getElementById('type-ctn-sent_secret').value
        // if (document.getElementById('recv_date-ctn-sent_secret').value) dataUrl += '&recv_date=' + document.getElementById('recv_date-ctn-sent_secret').value
    }

    dataUrl += '&check=' + ctnType;


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['REGISTER_NO_P1'] ? data[0][i]['REGISTER_NO_P1'] : "";
                temp[2] = data[0][i]['DOC_NUMBER_P1'] ? data[0][i]['DOC_NUMBER_P1'] : "";
                temp[3] = data[0][i]['DOC_DATE_P1'] ? data[0][i]['DOC_DATE_P1'] : "";
                temp[4] = data[0][i]['SEND_DATE_P1'] ? data[0][i]['SEND_DATE_P1'] : "";
                temp[5] = data[0][i]['DOC_FROM_TEXT_P1'] ? data[0][i]['DOC_FROM_TEXT_P1'] : "";
                temp[6] = data[0][i]['SUBJECT_P1'] ? data[0][i]['SUBJECT_P1'] : "";

                temp[7] = data[0][i]['REGISTER_NO_P2'] ? data[0][i]['REGISTER_NO_P2'] : "";
                temp[8] = data[0][i]['DOC_NUMBER_P2'] ? data[0][i]['DOC_NUMBER_P2'] : "";
                temp[9] = data[0][i]['DOC_DATE_P2'] ? data[0][i]['DOC_DATE_P2'] : "";
                temp[10] = data[0][i]['SEND_DATE_P2'] ? data[0][i]['SEND_DATE_P2'] : "";
                temp[11] = data[0][i]['DOC_FROM_TEXT_P2'] ? data[0][i]['DOC_FROM_TEXT_P2'] : "";
                temp[12] = data[0][i]['SUBJECT_P2'] ? data[0][i]['SUBJECT_P2'] : "";



                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}

function getLandofficeNoData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;

    if (document.getElementById('order_no-ctn-document').value) dataUrl += '&order_no=' + document.getElementById('order_no-ctn-document').value
    if (document.getElementById('recv_sign_date-ctn-document').value) dataUrl += '&doc_date=' + document.getElementById('recv_sign_date-ctn-document').value



    dataUrl += '&check=' + ctnType;


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['ORDER_NO_P1'] ? data[0][i]['ORDER_NO_P1'] : "-";
                temp[2] = data[0][i]['DOC_DATE_P1'] ? data[0][i]['DOC_DATE_P1'] : "-";
                temp[3] = data[0][i]['DOC_FROM_P1'] ? data[0][i]['DOC_FROM_P1'] : "-";
                temp[4] = data[0][i]['DOC_TO_P1'] ? data[0][i]['DOC_TO_P1'] : "-";
                temp[5] = data[0][i]['SUBJECT_P1'] ? data[0][i]['SUBJECT_P1'] : "-";

                temp[6] = data[0][i]['ORDER_NO_P2'] ? data[0][i]['ORDER_NO_P2'] : "-";
                temp[7] = data[0][i]['DOC_DATE_P2'] ? data[0][i]['DOC_DATE_P2'] : "-";
                temp[8] = data[0][i]['DOC_FROM_P2'] ? data[0][i]['DOC_FROM_P2'] : "-";
                temp[9] = data[0][i]['DOC_TO_P2'] ? data[0][i]['DOC_TO_P2'] : "-";
                temp[10] = data[0][i]['SUBJECT_P2'] ? data[0][i]['SUBJECT_P2'] : "-";


                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}

function getConstructionData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;


    if (document.getElementById('type-ctn-construct').value) dataUrl += '&con_type=' + document.getElementById('type-ctn-construct').value
    if (document.getElementById('id-ctn-construct').value) dataUrl += '&con_id=' + document.getElementById('id-ctn-construct').value
    if (document.getElementById('name-ctn-construct').value) dataUrl += '&con_name=' + document.getElementById('name-ctn-construct').value

    console.log(document.getElementById('type-ctn-construct').value);

    dataUrl += '&check=' + ctnType;


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['BLD_NAME_P1'] ? data[0][i]['BLD_NAME_P1'] : "";
                temp[2] = data[0][i]['REV_BLD_NO_P1'] ? data[0][i]['REV_BLD_NO_P1'] : "";
                temp[3] = data[0][i]['REV_BLD_NAME_P1'] ? data[0][i]['REV_BLD_NAME_P1'] : "";
                temp[4] = data[0][i]['REV_BLD_DATE_P1'] ? data[0][i]['REV_BLD_DATE_P1'] : "";

                temp[5] = data[0][i]['BLD_NAME_P2'] ? data[0][i]['BLD_NAME_P2'] : "";
                temp[6] = data[0][i]['REV_BLD_NO_P2'] ? data[0][i]['REV_BLD_NO_P2'] : "";
                temp[7] = data[0][i]['REV_BLD_NAME_P2'] ? data[0][i]['REV_BLD_NAME_P2'] : "";
                temp[8] = data[0][i]['REV_BLD_DATE_P2'] ? data[0][i]['REV_BLD_DATE_P2'] : "";




                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}

function getDocumentData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;
    $('.overlay-main').show();
    switch (ctnType) {
        case 10:
            if (document.getElementById('type-ctn-fin').value) dataUrl += '&docfin_type=' + document.getElementById('type-ctn-fin').value
            if (document.getElementById('no-ctn-fin').value) dataUrl += '&docfin_no=' + document.getElementById('no-ctn-fin').value

            break;
        case 11:
            if (document.getElementById('type-ctn-doc').value) dataUrl += '&docfin_type=' + document.getElementById('type-ctn-doc').value
            if (document.getElementById('no-ctn-doc').value) dataUrl += '&docfin_no=' + document.getElementById('no-ctn-doc').value
            break;
        case 12:
            if (document.getElementById('type-ctn-parcel').value) dataUrl += '&docfin_type=' + document.getElementById('type-ctn-parcel').value
            if (document.getElementById('no-ctn-parcel').value) dataUrl += '&docfin_no=' + document.getElementById('no-ctn-parcel').value
            break;

        default:
            break;
    }




    dataUrl += '&check=' + ctnType;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['PLATE_NAME_P1'] ? data[0][i]['PLATE_NAME_P1'] : "";
                temp[2] = data[0][i]['REV_HD_DATE_P1'] ? data[0][i]['REV_HD_DATE_P1'] : "";
                temp[3] = data[0][i]['UNIT_NAME_P1'] ? data[0][i]['UNIT_NAME_P1'] : "";
                temp[4] = data[0][i]['REV_PLATE_QTY_P1'] ? data[0][i]['REV_PLATE_QTY_P1'] : "";
                temp[5] = '<a onClick="showCTNDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';

                temp[6] = data[0][i]['PLATE_NAME_P2'] ? data[0][i]['PLATE_NAME_P2'] : "";
                temp[7] = data[0][i]['REV_HD_DATE_P2'] ? data[0][i]['REV_HD_DATE_P2'] : "";
                temp[8] = data[0][i]['UNIT_NAME_P2'] ? data[0][i]['UNIT_NAME_P2'] : "";
                temp[9] = data[0][i]['REV_PLATE_QTY_P2'] ? data[0][i]['REV_PLATE_QTY_P2'] : "";
                temp[10] = '<a onClick="showCTNDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';

                temp[11] = data[0][i]['REV_HD_SEQ_P1'] ? data[0][i]['REV_HD_SEQ_P1'] : "";
                temp[12] = data[0][i]['REV_HD_SEQ_P2'] ? data[0][i]['REV_HD_SEQ_P2'] : "";


                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}

function getDistrictData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;


    if (document.getElementById('type-ctn-district').value) dataUrl += '&dis_type=' + document.getElementById('type-ctn-district').value
    if (document.getElementById('year-ctn-district').value) dataUrl += '&year=' + document.getElementById('year-ctn-district').value


    dataUrl += '&check=' + ctnType;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['PIECE_NAME_P1'] ? data[0][i]['PIECE_NAME_P1'] : "";
                temp[2] = data[0][i]['REV_HD_YEAR_P1'] ? data[0][i]['REV_HD_YEAR_P1'] : "";
                temp[3] = data[0][i]['UNIT_NAME_P1'] ? data[0][i]['UNIT_NAME_P1'] : "";
                temp[4] = data[0][i]['REV_HD_DATE_P1'] ? data[0][i]['REV_HD_DATE_P1'] : "";
                temp[5] = data[0][i]['REV_PIECE_QTY_P1'] ? data[0][i]['REV_PIECE_QTY_P1'] : "";
                temp[6] = '<a onClick="showCTNDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';

                temp[7] = data[0][i]['PIECE_NAME_P2'] ? data[0][i]['PIECE_NAME_P2'] : "";
                temp[8] = data[0][i]['REV_HD_YEAR_P2'] ? data[0][i]['REV_HD_YEAR_P2'] : "";
                temp[9] = data[0][i]['REV_HD_DATE_P1'] ? data[0][i]['REV_HD_DATE_P1'] : "";
                temp[10] = data[0][i]['UNIT_NAME_P2'] ? data[0][i]['UNIT_NAME_P2'] : "";
                temp[11] = data[0][i]['REV_PIECE_QTY_P2'] ? data[0][i]['REV_PIECE_QTY_P2'] : "";
                temp[12] = '<a onClick="showCTNDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';
                temp[13] = data[0][i]['REV_HD_SEQ_P1'] ? data[0][i]['REV_HD_SEQ_P1'] : "";
                temp[14] = data[0][i]['REV_HD_SEQ_P2'] ? data[0][i]['REV_HD_SEQ_P2'] : "";


                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}

function getADMData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;

    if (document.getElementById('id-ctn-adm').value) dataUrl += '&adm_id=' + document.getElementById('id-ctn-adm').value
    if (document.getElementById('firstname-ctn-adm').value) dataUrl += '&adm_firstname=' + document.getElementById('firstname-ctn-adm').value
    if (document.getElementById('lastname-ctn-adm').value) dataUrl += '&adm_lastname=' + document.getElementById('lastname-ctn-adm').value
    if (document.getElementById('type-ctn-adm').value) dataUrl += '&landoffice_type=' + document.getElementById('type-ctn-adm').value

    dataUrl += '&check=' + ctnType;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['PSN_PID_P1'] ? data[0][i]['PSN_PID_P1'] : "";
                temp[2] = data[0][i]['TITLE_NAME_P1'] ? data[0][i]['TITLE_NAME_P1'] : "";
                temp[3] = data[0][i]['NAME_P1'] ? data[0][i]['NAME_P1'] : "";
                temp[4] = data[0][i]['POSITION_NO_P1'] ? data[0][i]['POSITION_NO_P1'] : "";
                temp[5] = data[0][i]['DIV_POSITION_NAME_P1'] ? data[0][i]['DIV_POSITION_NAME_P1'] : "";
                temp[6] = data[0][i]['POSITION_LEVEL_NAME_P1'] ? data[0][i]['POSITION_LEVEL_NAME_P1'] : "";
                temp[7] = data[0][i]['LANDOFFICE_TYPE_NAME_P1'] ? data[0][i]['LANDOFFICE_TYPE_NAME_P1'] : "";
                temp[8] = data[0][i]['PSN_TYPE_NAME_P1'] ? data[0][i]['PSN_TYPE_NAME_P1'] : "";

                temp[9] = data[0][i]['PSN_PID_P2'] ? data[0][i]['PSN_PID_P2'] : "";
                temp[10] = data[0][i]['TITLE_NAME_P2'] ? data[0][i]['TITLE_NAME_P2'] : "";
                temp[11] = data[0][i]['NAME_P2'] ? data[0][i]['NAME_P2'] : "";
                temp[12] = data[0][i]['POSITION_NO_P2'] ? data[0][i]['POSITION_NO_P2'] : "";
                temp[13] = data[0][i]['DIV_POSITION_NAME_P2'] ? data[0][i]['DIV_POSITION_NAME_P2'] : "";
                temp[14] = data[0][i]['POSITION_LEVEL_NAME_P2'] ? data[0][i]['POSITION_LEVEL_NAME_P2'] : "";
                temp[15] = data[0][i]['LANDOFFICE_TYPE_NAME_P2'] ? data[0][i]['LANDOFFICE_TYPE_NAME_P2'] : "";
                temp[16] = data[0][i]['PSN_TYPE_NAME_P2'] ? data[0][i]['PSN_TYPE_NAME_P2'] : "";


                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();
            $('#table-ctn-' + type).DataTable({
                "columnDefs": [{
                    className: "diff-p", "targets": [8]
                }]
            })

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}



function getCTNOverall() {
    var dataUrl = 'landoffice=' + landoffice + '&check=overall';

    table = $('#table-ctn-overall').DataTable({
        "searching": false,
        "ordering": false,
        orderCellsTop: true,
        "bPaginate": false,
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs": [{
            "className": "dt-center",
            "targets": [0, 2, 3, 4],

        }, {
            "className": "dt-left",
            "targets": [1],

        }],
        "createdRow": function (row, data, dataIndex) {
            if (dataIndex == 0 || dataIndex == 5 || dataIndex == 10) {
                $(row).addClass('redClass');
            }
        },
        "info": false,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    })

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnCount.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            let i = 0;
            for (let j = 0; j < Object.keys(ctnColumn).length; j++) {
                temp = []
                temp[0] = j + 1;

                if (Object.keys(ctnColumn)[j] != data[0][i]['NUM']) {
                    temp[1] = ctnColumn[Object.keys(ctnColumn)[j]];
                    temp[2] = "0";
                    temp[3] = "0";
                    temp[4] = "0";
                }
                else {
                    temp[1] = data[0][i]['NUM'] ? ctnColumn[data[0][i]['NUM']] : "";
                    temp[2] = data[0][i]['TOTAL'] ? parseInt(data[0][i]['TOTAL']).toLocaleString() : "0";
                    temp[3] = data[0][i]['Y_SEQ'] ? parseInt(data[0][i]['Y_SEQ']).toLocaleString() : "0";
                    temp[4] = data[0][i]['N_SEQ'] ? parseInt(data[0][i]['N_SEQ']).toLocaleString() : "0";
                    i++;
                }



                dataSet[num] = temp;
                num++;


            }

            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-count').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');


}

function getCTNCount() {
    var dataUrl = 'landoffice=' + landoffice;

    table = $('#table-ctn-' + type + '-count').DataTable({
        "pageLength": 10,
        "pagingType": "simple_numbers",
        "searching": false,
        "ordering": false,
        orderCellsTop: true,
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs": [{
            "className": "text-center",
            "targets": "_all",

        }],
        'info': true,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    })

    dataUrl += '&check=' + ctnType;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnCount.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['NAME'] ? data[0][i]['NAME'] : "";
                temp[2] = data[0][i]['COUNT_P1'] ? parseInt(data[0][i]['COUNT_P1']).toLocaleString() : "";
                temp[3] = data[0][i]['COUNT_P2'] ? parseInt(data[0][i]['COUNT_P2']).toLocaleString() : "";


                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-count').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');


}

function getAbsenceCount() {
    var dataUrl = 'landoffice=' + landoffice;

    if (document.getElementById('id-ctn-absence').value) dataUrl += '&adm_id=' + document.getElementById('id-ctn-absence').value
    if (document.getElementById('firstname-ctn-absence').value) dataUrl += '&adm_firstname=' + document.getElementById('firstname-ctn-absence').value
    if (document.getElementById('lastname-ctn-absence').value) dataUrl += '&adm_lastname=' + document.getElementById('lastname-ctn-absence').value
    if (document.getElementById('year-ctn-absence').value) dataUrl += '&year=' + document.getElementById('year-ctn-absence').value


    var tablep1 = $('#table-ctn-absence-count-p1').DataTable({
        "pageLength": 5,
        "pagingType": "simple_numbers",
        "searching": false,
        "ordering": false,
        orderCellsTop: true,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs": [{
            "className": "text-center",
            "targets": "_all",

        }],
        'info': true,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    })

    dataUrl += '&check=16';

    $('.overlay-count').show();

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnCount.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['PSN_PID'] ? data[0][i]['PSN_PID'] : "-";
                temp[2] = data[0][i]['TITLE_NAME'] ? data[0][i]['TITLE_NAME'] : "-";
                temp[2] += data[0][i]['PSN_FNAME'] ? data[0][i]['PSN_FNAME'] : "-";
                temp[2] += data[0][i]['PSN_LNAME'] ? data[0][i]['PSN_LNAME'] : "-";
                temp[3] = data[0][i]['FISCAL_YEAR'] ? data[0][i]['FISCAL_YEAR'] : "-";
                temp[4] = data[0][i]['SICK_COUNT'] ? data[0][i]['SICK_COUNT'] : "0";
                temp[5] = data[0][i]['DELIVER_COUNT'] ? data[0][i]['DELIVER_COUNT'] : "0";
                temp[6] = data[0][i]['BUSINESS_COUNT'] ? data[0][i]['BUSINESS_COUNT'] : "0";
                temp[7] = data[0][i]['REST_COUNT'] ? data[0][i]['REST_COUNT'] : "0";
                temp[8] = data[0][i]['ORDAIN_COUNT'] ? data[0][i]['ORDAIN_COUNT'] : "0";
                temp[9] = data[0][i]['HUJJ_COUNT'] ? data[0][i]['HUJJ_COUNT'] : "0";
                temp[10] = data[0][i]['MILITARY_COUNT'] ? data[0][i]['MILITARY_COUNT'] : "0";
                temp[11] = data[0][i]['STUDY_COUNT'] ? data[0][i]['STUDY_COUNT'] : "0";
                temp[12] = data[0][i]['INTER_COUNT'] ? data[0][i]['INTER_COUNT'] : "0";
                temp[13] = data[0][i]['FOLLOW_COUNT'] ? data[0][i]['FOLLOW_COUNT'] : "0";
                temp[14] = data[0][i]['HELP_WIFE_COUNT'] ? data[0][i]['HELP_WIFE_COUNT'] : "0";
                temp[15] = data[0][i]['IMPROVE_COUNT'] ? data[0][i]['IMPROVE_COUNT'] : "0";
                temp[16] = '<a onClick="showDetail($(this), 1)"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';
                temp[17] = data[0][i]['PSN_SEQ'] ? data[0][i]['PSN_SEQ'] : "";

                dataSet[num] = temp;
                num++;
            }
            tablep1.search('').draw();
            tablep1.clear();
            tablep1.rows.add(dataSet);
            tablep1.draw();




        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    var tablep2 = $('#table-ctn-absence-count-p2').DataTable({
        "pageLength": 5,
        "pagingType": "simple_numbers",
        "searching": false,
        "ordering": false,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        orderCellsTop: true,
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs": [{
            "className": "text-center",
            "targets": "_all",

        }],
        'info': true,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    })

    dataUrl = 'landoffice=' + landoffice;

    if (document.getElementById('id-ctn-absence').value) dataUrl += '&adm_id=' + document.getElementById('id-ctn-absence').value
    if (document.getElementById('firstname-ctn-absence').value) dataUrl += '&adm_firstname=' + document.getElementById('firstname-ctn-absence').value
    if (document.getElementById('lastname-ctn-absence').value) dataUrl += '&adm_lastname=' + document.getElementById('lastname-ctn-absence').value
    if (document.getElementById('year-ctn-absence').value) dataUrl += '&year=' + document.getElementById('year-ctn-absence').value


    dataUrl += '&check=16';

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './ctn/getCtnCount.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['PSN_PID_P2'] ? data[0][i]['PSN_PID_P2'] : "-";
                temp[2] = data[0][i]['TITLE_NAME_P2'] ? data[0][i]['TITLE_NAME_P2'] : "-";
                temp[2] += data[0][i]['PSN_FNAME_P2'] ? data[0][i]['PSN_FNAME_P2'] : "-";
                temp[2] += data[0][i]['PSN_LNAME_P2'] ? data[0][i]['PSN_LNAME_P2'] : "-";
                temp[3] = data[0][i]['FISCAL_YEAR_P2'] ? data[0][i]['FISCAL_YEAR_P2'] : "-";
                temp[4] = data[0][i]['SICK_COUNT_P2'] ? data[0][i]['SICK_COUNT_P2'] : "0";
                temp[5] = data[0][i]['DELIVER_COUNT_P2'] ? data[0][i]['DELIVER_COUNT_P2'] : "0";
                temp[6] = data[0][i]['BUSINESS_COUNT_P2'] ? data[0][i]['BUSINESS_COUNT_P2'] : "0";
                temp[7] = data[0][i]['REST_COUNT_P2'] ? data[0][i]['REST_COUNT_P2'] : "0";
                temp[8] = data[0][i]['ORDAIN_COUNT_P2'] ? data[0][i]['ORDAIN_COUNT_P2'] : "0";
                temp[9] = data[0][i]['HUJJ_COUNT_P2'] ? data[0][i]['HUJJ_COUNT_P2'] : "0";
                temp[10] = data[0][i]['MILITARY_COUNT_P2'] ? data[0][i]['MILITARY_COUNT_P2'] : "0";
                temp[11] = data[0][i]['STUDY_COUNT_P2'] ? data[0][i]['STUDY_COUNT_P2'] : "0";
                temp[12] = data[0][i]['INTER_COUNT_P2'] ? data[0][i]['INTER_COUNT_P2'] : "0";
                temp[13] = data[0][i]['FOLLOW_COUNT_P2'] ? data[0][i]['FOLLOW_COUNT_P2'] : "0";
                temp[14] = data[0][i]['HELP_WIFE_COUNT_P2'] ? data[0][i]['HELP_WIFE_COUNT_P2'] : "0";
                temp[15] = data[0][i]['IMPROVE_COUNT_P2'] ? data[0][i]['IMPROVE_COUNT_P2'] : "0";
                temp[16] = '<a onClick="showDetail($(this), 2)"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';
                temp[17] = data[0][i]['PSN_SEQ_P2'] ? data[0][i]['PSN_SEQ_P2'] : "";


                dataSet[num] = temp;
                num++;
            }

            tablep2.search('').draw();
            tablep2.clear();
            tablep2.rows.add(dataSet);
            tablep2.draw();



            $('.overlay-count').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');


}

function exportCtn(check, sts, landoffice) {


    window.open('./queries/excelCtnData.php?' + "check=" + check + "&sts=" + sts +  "&landoffice=" + landoffice + "&branchName=" + branchName, true);



}
function exportErrorCtn(check, sts, landoffice) {


    window.open('./queries/excelCtnErrorData.php?' + "check=" + check + "&sts=" + sts + "&landoffice=" + landoffice + "&branchName=" + branchName, true);





}



$(document).ready(function () {
    $('.sidebar-menu').click(function () {
        $('ul a.nav-link').removeClass('active');
        $(this).parent().children('a').addClass('active');
    });
    $('.tab-pane').hide();




    //redirect to login page when bypass by URL
    if (getCookie("landoffice") == "") {
        location.href = "./index.html";
    }


    $('.overlay-main').hide();


    $.fn.dataTable.ext.errMode = 'none';

    branchName = getCookie('branchName');
    landoffice = getCookie('landoffice');

    document.getElementById("landoffice_name").innerHTML = '<i class="fa-lg fa fa-sitemap"></i> ระบบตรวจสอบข้อมูลงานอำนวยการ<br>[' + branchName + ']';

    $('.nav-pills-main li ul li a').on('show.bs.tab', function () {

        $('.overlay-count').show();
        $(current_href).hide();
        $(current_href + '-table').hide();

        var href = $(this).attr('href');
        console.log(href);
        current_href = href;
        $(current_href).show();
        $(current_href + '-table').show();
        console.log(current_href + '-table');
        $("#search-ctn-" + type).off();
        type = href.split('-')[2];
        ctnType = 0;
        switch (type) {
            case 'overall':
                getCTNOverall();
                break;
            case 'recv_nosecret':
                ctnType = 1;
                getCTNCount();
                break;
            case 'recv_secret':
                ctnType = 2;
                getCTNCount();
                getSecretType("#type-ctn-recv_secret");
                break;
            case 'sent_nosecret':
                ctnType = 3;
                getCTNCount();
                break;
            case 'sent_secret':
                ctnType = 4;
                getCTNCount();
                getSecretType("#type-ctn-sent_secret");
                break;
            case 'province_nosecret':
                ctnType = 5;
                getCTNCount();
                break;
            case 'province_secret':
                ctnType = 6;
                getCTNCount();
                break;
            case 'document':
                ctnType = 7;
                getCTNCount();
                break;
            case 'construct':
                getBuildingType("#type-ctn-construct");
                ctnType = 8;
                getCTNCount();
                break;
            case 'inv':
                getINVType("#type-ctn-inv");
                ctnType = 9;
                getCTNCount();
                break;
            case 'fin':
                getDocumentFinanceType("#type-ctn-fin");
                ctnType = 10;
                getCTNCount();
                break;
            case 'doc':
                getDocumentGeneralType("#type-ctn-doc");
                ctnType = 11;
                getCTNCount();
                break;
            case 'parcel':
                getDocumentParcelType("#type-ctn-parcel");
                ctnType = 12;
                getCTNCount();
                break;
            case 'material':
                ctnType = 13;
                getCTNCount();
                break;
            case 'district':
                ctnType = 14;
                getCTNCount();
                getDistrictType("#type-ctn-district");
                break;
            case 'adm':
                ctnType = 15;
                getCTNCount();
                getLandofficeType("#type-ctn-adm");
                break;
            case 'absence':
                getAbsenceCount();
                ctnType = 16;
                break;
            default:
                break;
        }

        $('#table-ctn-absence-count-p1 tbody').on('click', 'tr', function () {
            let table = $('#table-ctn-absence-count-p1').DataTable();
            table.$('tr.row-click').removeClass('row-click');
            $(this).addClass('row-click');

            let table2 = $('#table-ctn-absence-count-p2').DataTable();
            table2.$('tr.row-click').removeClass('row-click');

            index = table.row(this).index();
            table2.rows(index).nodes().to$().addClass('row-click')

        });
        $('#table-ctn-absence-count-p2 tbody').on('click', 'tr', function () {
            let table = $('#table-ctn-absence-count-p2').DataTable();
            table.$('tr.row-click').removeClass('row-click');
            $(this).addClass('row-click');

            let table2 = $('#table-ctn-absence-count-p1').DataTable();
            table2.$('tr.row-click').removeClass('row-click');

            index = table.row(this).index();
            table2.rows(index).nodes().to$().addClass('row-click')

        });

        $('#table-ctn-absence-count-p1').on('page.dt', function () {
            table = $('#table-ctn-absence-count-p1').DataTable();
            var info = table.page.info();
            // alert(info.page+1)
            table2 = $('#table-ctn-absence-count-p2').DataTable();
            table2.page(info.page).draw('page')
        });

        $('#table-ctn-absence-count-p2').on('page.dt', function () {
            table = $('#table-ctn-absence-count-p2').DataTable();
            var info = table.page.info();
            // alert(info.page+1)
            table2 = $('#table-ctn-absence-count-p1').DataTable();
            table2.page(info.page).draw('page')
        });

        $("#search-ctn-" + type).on("click", searchFunction);



        $('#export-success-ctn-' + ctnType).on("click", function () {
            exportCtn(ctnType, 's', landoffice);
        });
        $('#export-diff-ctn-' + ctnType).on("click", function () {
            exportCtn(ctnType, 'e', landoffice);
        });
        $('#export-error-ctn-' + ctnType).on("click", function () {
            exportErrorCtn(ctnType, 's', landoffice);
        });





    });

});