$(document).ready(function () {

    $('.sidebar-menu').click(function () {
        $('ul a.nav-link').removeClass('active');
        $(this).parent().children('a').addClass('active');
    });

    $('.tab-pane').hide();
    $('.overlay-main').hide();

    if (getCookie("landoffice") == "") {
        location.href = "./index.html";
    }

    $.fn.dataTable.ext.errMode = 'none';

    branchName = getCookie('branchName');
    landoffice = getCookie('landoffice');

    document.getElementById("landoffice_name").innerHTML = '<i class="fa-lg fa fa-money"></i> ระบบตรวจสอบข้อมูลการเงินและบัญชี<br>[' + branchName + ']';

    getReCate("#select-recate", landoffice);
    


    
    $('.nav-pills-main li a').on('show.bs.tab', function () {
        $("#search-fin-"+type).off();
        // console.log('down')
        $(current_href).hide();

        var href = $(this).attr('href');
        current_href = href;

        

        $(current_href).show();
        type = href.split('-')[2]; 
        console.log('type=' + type);

        if(current_href === "#tab-fin-sum"){
            // console.log(current_href + ' === ' + '#tab-sum-'+type);
            getSummary(landoffice);
        }
        else if(type==1 || type==2 || type==3 || type==4 || type==5 ){
            getDataFin(landoffice,type);
        }
        // else if(type==7){
        //     $('#tabPanel-fin-7').css("visibility", "visible");
        // }


        $('#search-fin-'+type).on("click", function(){
            console.log('search');
            $('#tabPanel-fin-'+type).css("visibility", "hidden");
            getDataFin(landoffice,type);
            $('#detail7-fin-'+type).hide();
        }); 

        $('#clear-fin-' + type).on("click", function(){
            clearFunction(type);
        }); 

        $('#export-success-fin-' + type).on("click", function(){
            window.open('./queries/fin/exportExcel.php?type=' + type + "&sts=" + 's' + "&landoffice=" + landoffice + "&branchName=" + branchName, true);
        }); 

        
        $('#export-error-fin-' + type).on("click", function(){
            window.open('./queries/fin/exportExcel.php?type=' + type + "&sts=" + 'e' + "&landoffice=" + landoffice + "&branchName=" + branchName, true);
        }); 
        
    });

    $('.nav-tabs li a').on('click', function () {
        $(current_tab_href).hide();

        var href = $(this).attr('href');
        current_tab_href = href;

        $(current_tab_href).show();
        tab_type = href.split('-')[0]; 
        console.log(tab_type);


        console.log(tab_type+'-fin-'+type);
        if(tab_type+'-fin-'+type == '#detailTab-fin-7'){
            $('#detailincomeTab-fin-7').css("display", "none");
            $('#detailTab-fin-7').css("display", "block");
        }else{
            $('#detailTab-fin-7').css("display", "none");
            $('#detailincomeTab-fin-7').css("display", "block");
            
        }


    });

    $('#table-fin-71').on('click', 'tbody tr', function () {
        console.log('table-fin-71');
        let table = $('#table-fin-71').DataTable();
        table.$('tr.row-click').removeClass('row-click');
        $(this).addClass('row-click');

        let table2 = $('#table-fin-72').DataTable();
        table2.$('tr.row-click').removeClass('row-click');

        index = table.row(this).index();
        table2.rows(index).nodes().to$().addClass('row-click')
  } );

    $('#table-fin-72').on('click', 'tbody tr', function () {
        console.log('table-fin-72');
        let table = $('#table-fin-72').DataTable();
        table.$('tr.row-click').removeClass('row-click');
        $(this).addClass('row-click');

        let table2 = $('#table-fin-71').DataTable();
        table2.$('tr.row-click').removeClass('row-click');

        index = table.row(this).index();
        table2.rows(index).nodes().to$().addClass('row-click')
    } );

    $('#table-fin-6').on( 'page.dt', function () {
        table = $('#table-fin-6').DataTable();
        var info = table.page.info();
        // alert(info.page+1)
        table2 = $('#table-fin-62').DataTable();
        table2.page(info.page).draw('page')
    } );
    $('#table-fin-62').on( 'page.dt', function () {
        table = $('#table-fin-62').DataTable();
        var info = table.page.info();
        // alert(info.page+1)
        table2 = $('#table-fin-6').DataTable();
        table2.page(info.page).draw('page')
    } );

    $('#table-fin-71').on( 'page.dt', function () {
        table = $('#table-fin-71').DataTable();
        var info = table.page.info();
        // alert(info.page+1)
        table2 = $('#table-fin-72').DataTable();
        table2.page(info.page).draw('page')
    } );

    $('#table-fin-72').on( 'page.dt', function () {
        table = $('#table-fin-72').DataTable();
        var info = table.page.info();
        // alert(info.page+1)
        table2 = $('#table-fin-71').DataTable();
        table2.page(info.page).draw('page')
    } );


});


function clearFunction (type){
    // console.log(n)
    if(type=6) {
        $('#fin-btd59-year').val('');
        $('#fin-btd59-no').val('');
        $('#date-fin-start').val('');
        $('#date-fin-end').val('');
        // $('#select-recate').val('');
    }
    if(type=7) {
        $('#datercpt-fin-start').val('');
        $('#datercpt-fin-end').val('');
        $('#fin-rcpt-start').val('');
        $('#fin-rcpt-end').val('');
        $('#select-rcpt').val('');
        $('#select-sts').val('');
    }
}



function getReCate(ID,landoffice) {
    // console.log('ID= ' + ID + ' landoffice= ' + landoffice);
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/fin/getReCate.php',
        data: 'landoffice=' + landoffice,
        success: function (data) {
            // console.log(data);
            $(ID).empty();
            option = '';
            option += '<option value="" selected>เลือกประเภทคำขอ</option>';

            if(!jQuery.isEmptyObject(data)){
                for (let i = 0; i < data[0].length; i++){
                    option += '<option value="' + data[0][i]['RECEIPT_CATEGORY'] + '">' + data[0][i]['RECEIPT_CATEGORY'] + '</option>';
                }
            }
            $(ID).append(option);
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    
}

function getDetail1(e){
    $('#detail7.overlay-main').show();
    var tr = e.parent().closest('tr');
    var row = $("#table-fin-71").DataTable().row(tr);


    if(type == 7){
        $('#tabPanel-fin-7').css("visibility", "visible");
        $('#detailTabPanel-fin-7').click();
        var dataUrl = 'landoffice=' + landoffice;
        if (row.data()[2] != "") dataUrl += '&detailOrder=' + row.data()[2];
        if (row.data()[7] != "") dataUrl += '&detailRec=' + row.data()[7];

        var DATE = row.data()[8].split(" ");
        var YEAR = DATE[2] != "" ? DATE[2] : DATE[3];
        if (parseInt(YEAR) > 2400) {
            datetemp = row.data()[8].replace(YEAR, parseInt(YEAR) - 543)
        }
        else {
            datetemp = row.data()[8];
        }


        if (row.data()[8] != "") dataUrl += '&detailDate=' + datetemp;
        
        console.log('dataUrl1: '+dataUrl);
        getDetailGen(dataUrl);
        getDetailIncome(dataUrl);
    }
}

function getDetail2(e){
    $('#detail7.overlay-main').show();
    var tr = e.parent().closest('tr');
    var row = $("#table-fin-72").DataTable().row(tr);

    if(type == 7){
        $('#tabPanel-fin-7').css("visibility", "visible");
        $('#detailTabPanel-fin-7').click();
        var dataUrl = 'landoffice=' + landoffice;
        if (row.data()[2] != "") dataUrl += '&detailOrder=' + row.data()[2];
        if (row.data()[7] != "") dataUrl += '&detailRec=' + row.data()[7];

        var DATE = row.data()[8].split(" ");
        var YEAR = DATE[2] != "" ? DATE[2] : DATE[3];
        if (parseInt(YEAR) > 2400) {
            datetemp = row.data()[8].replace(YEAR, parseInt(YEAR) - 543)
        }
        else {
            datetemp = row.data()[8];
        }


        if (row.data()[8] != "") dataUrl += '&detailDate=' + datetemp;
        
        console.log('dataUrl1: '+dataUrl);
        getDetailGen(dataUrl);
        getDetailIncome(dataUrl);
    }
}

function getDetailGen(dataUrl){
    // var table = $("#table-detailTab-fin-7").DataTable();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "./queries/fin/getDetailRCPT.php",
        data : dataUrl,
        success: function(data){
            var table = $("#table-detailTab-fin-7").DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable" : false,
                        "targets" : 0
                    }
                ,{"className": "dt-center", "targets": [ 1,2 ]}
                ],
                "deferRender" : true,
                "processing" : true,
                "lengthChange": false,
                "ordering" : false,
                "searching" : false,
                "paging" : false,
                "info" : false,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
                    });
            if(!jQuery.isEmptyObject(data)){
                console.log(data);
                table.search("").draw();
                table.clear();
                var count = Object.keys(data[0][0]).length;
                for (let i = 0; i < (Object.keys(ColumnRCPTdetail).length); i++){
                    var key = Object.keys(ColumnRCPTdetail)[i];
                    temp = [];
                    temp[0] = ColumnRCPTdetail[key]
                    if(key.includes('_MNY')) {
                        temp[1] = data[0][0][key+'_P1'] ? Number(data[0][0][key+'_P1']).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2
                        }) : "0.00";

                        temp[2] = data[0][0][key+'_P2'] ? Number(data[0][0][key+'_P2']).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2
                        }) : "0.00";

                        // temp[1] = Number(data[0][0][key+'_P1']).toLocaleString()===undefined? "-" : Number(data[0][0][key+'_P1']).toLocaleString(undefined, {
                        //     minimumFractionDigits: 2,
                        //     maximumFractionDigits: 2
                        // });
                        // temp[2] = Number(data[0][0][key+'_P2']).toLocaleString()===undefined? "-" : Number(data[0][0][key+'_P2']).toLocaleString(undefined, {
                        //     minimumFractionDigits: 2,
                        //     maximumFractionDigits: 2
                        // });
                    }else{
                        temp[1] = data[0][0][key+'_P1']===undefined? "-" : data[0][0][key+'_P1'];
                        temp[2] = data[0][0][key+'_P2']===undefined? "-" : data[0][0][key+'_P2'];
                    }
                    table.row.add(temp);
                }

            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#detail7.overlay-main').hide();
        }
    })
}


function getDetailIncome(dataUrl){
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "./queries/fin/getDetailIncome.php",
        data : dataUrl,
        success: function(data){

            var table = $("#table-detailincomeTab-fin-7").DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable" : false,
                        "targets" : 0
                    }
                ,{"className": "dt-center", "targets": [ 2,5 ]}
                ,{"className": "dt-right", "targets": [ 1,4 ]}
                ],
                "deferRender" : true,
                "processing" : true,
                "lengthChange": false,
                "ordering" : false,
                "searching" : false,
                "paging" : false,
                "info" : false,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
                    });

            if(!jQuery.isEmptyObject(data)){
                console.log(data);
                table.search("").draw();
                table.clear();
                count = data[0].length;
                total1 = 0;
                total2 = 0;
                for (let i = 0; i <= count; i++){
                    temp = [];
                    if(i == count){
                        temp[0] = "รวมรายการทั้งสิ้น(ยกเว้นค่าพยาน ค่าอากรแสตมป์(ดวง))(บาท)";
                        temp[1] = total1 ? Number(total1).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2
                        }) : "-";
                        // temp[1] = Number(total1).toLocaleString()===undefined? "-" : Number(total1).toLocaleString(undefined, {
                        //     minimumFractionDigits: 2,
                        //     maximumFractionDigits: 2
                        // });
                        // temp[2] = '-';
                        temp[3] = "รวมรายการทั้งสิ้น(ยกเว้นค่าพยาน ค่าอากรแสตมป์(ดวง))(บาท)";
                        temp[4] = total2 ? Number(total2).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2
                        }) : "-";
                        // temp[4] = Number(total2).toLocaleString()===undefined? "-" : Number(total2).toLocaleString(undefined, {
                        //     minimumFractionDigits: 2,
                        //     maximumFractionDigits: 2
                        // });
                        // temp[5] = '-';

                    }else{
                        temp[0] = data[0][i]['INCOME_NAME_P1']===undefined? "-" : data[0][i]['INCOME_NAME_P1'];
                        temp[1] = Number(data[0][i]['INCOME_MNY_P1']).toLocaleString()===undefined? "-" : Number(data[0][i]['INCOME_MNY_P1']).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2
                        });
                        temp[2] = data[0][i]['EXC_FLAG_P1']===undefined? "-" : data[0][i]['EXC_FLAG_P1'];
                        temp[3] = data[0][i]['INCOME_NAME_P2']===undefined? "-" : data[0][i]['INCOME_NAME_P2'];
                        temp[4] = Number(data[0][i]['INCOME_MNY_P2']).toLocaleString()===undefined? "-" : Number(data[0][i]['INCOME_MNY_P2']).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2
                        });
                        temp[5] = data[0][i]['EXC_FLAG_P2']===undefined? "-" : data[0][i]['EXC_FLAG_P2'];

                        console.log('รวม1 '+ i + ' ' + total1);
                        console.log('รวม2 '+ i + ' ' + total2);

                        if(data[0][i]['INCOME_NAME_P1'] != 'ค่าพยาน' && data[0][i]['INCOME_NAME_P1'] != 'ค่าอากรแสตมป์(ดวง)' && data[0][i]['EXC_FLAG_P1'] != 'ยกเว้น' &&
                           data[0][i]['INCOME_NAME_P2'] != 'ค่าพยาน' && data[0][i]['INCOME_NAME_P2'] != 'ค่าอากรแสตมป์(ดวง)' && data[0][i]['EXC_FLAG_P2'] != 'ยกเว้น'){
                            total1 += Number(data[0][i]['INCOME_MNY_P1']);
                            total2 += Number(data[0][i]['INCOME_MNY_P2']);
                        }
                    }



                    table.row.add(temp);
                    // num++;
                }
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#detail7.overlay-main').hide();
        }
    })
}



function getDataFin(landoffice,type){
    var dataUrl = 'landoffice=' + landoffice + '&type=' + type ;
    switch (type) {
        case '1': $('#overlay1.overlay-main').show();
        break;
        case '2': $('#overlay2.overlay-main').show();
        break;
        case '3': $('#overlay3.overlay-main').show();  
        break;
        case '4': $('#overlay4.overlay-main').show();
        break;
        case '5': $('#overlay5.overlay-main').show();
        break;
        case '6': $('#overlay6.overlay-main').show();
            if (document.getElementById('fin-btd59-year').value) dataUrl += '&btd59year=' + document.getElementById('fin-btd59-year').value
            if (document.getElementById('fin-btd59-no').value) dataUrl += '&btd59no=' + document.getElementById('fin-btd59-no').value
            if (document.getElementById('date-fin-start').value) dataUrl += '&datestart=' + document.getElementById('date-fin-start').value
            if (document.getElementById('date-fin-end').value) dataUrl += '&dateend=' + document.getElementById('date-fin-end').value
            // if (document.getElementById('select-recate').value) dataUrl += '&receipcate=' + document.getElementById('select-recate').value
        break;
        case '7':
            $('#overlay7.overlay-main').show();
            if (document.getElementById('datercpt-fin-start').value) dataUrl += '&datestart=' + document.getElementById('datercpt-fin-start').value
            if (document.getElementById('datercpt-fin-end').value) dataUrl += '&dateend=' + document.getElementById('datercpt-fin-end').value
            if (document.getElementById('fin-rcpt-start').value) dataUrl += '&rcptstart=' + document.getElementById('fin-rcpt-start').value
            if (document.getElementById('fin-rcpt-end').value) dataUrl += '&rcptend=' + document.getElementById('fin-rcpt-end').value
            if (document.getElementById('select-rcpt').value) dataUrl += '&selectrcpt=' + document.getElementById('select-rcpt').value
            if (document.getElementById('select-sts').value) dataUrl += '&selectsts=' + document.getElementById('select-sts').value

        break;
    }

    // $('.overlay-main').show();
    $.ajax({
        type : "POST",
        dataType : "json",
        url : "./queries/fin/getFinData.php",
        data : dataUrl,
        success: function(data){
            console.log(data);
            // console.log('type=' + type);
            // console.log('#table-fin-=' +type);
            datatemp = data;
            if(type==1 || type==2 || type==3 || type==4 || type==5){
            var table = $("#table-fin-"+type).DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable" : false,
                        "targets" : 0
                    }
                ],
                "deferRender" : true,
                "pagingType" : "simple_numbers",
                "pageLength" : 10,
                "processing" : true,
                "lengthChange": false,
                "ordering" : false,
                "searching" : false,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });
            }
            if(!jQuery.isEmptyObject(data)){
                if(type != 6 && type != 7 ){
                    table.search("").draw();
                    table.clear();
                }
                count = data[0].length;
                num = 0;
                switch (type){
                    case '1':
                        console.log(type);
                        for (let i = 0; i < count; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LANDOFFICE_NAME_TH_P1']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P1'];
                            temp[2] = data[0][i]['GFMIS_CAPITAL_PAY_ID_P1']===undefined? "-" : data[0][i]['GFMIS_CAPITAL_PAY_ID_P1'];
                            temp[3] = data[0][i]['GFMIS_CAPITAL_ID_P1']===undefined? "-" : data[0][i]['GFMIS_CAPITAL_ID_P1'];
                            temp[4] = data[0][i]['AREA_CODE_P1']===undefined? "-" : data[0][i]['AREA_CODE_P1'];

                            temp[5] = data[0][i]['LANDOFFICE_NAME_TH_P2']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P2'];
                            temp[6] = data[0][i]['GFMIS_CAPITAL_PAY_ID_P2']===undefined? "-" : data[0][i]['GFMIS_CAPITAL_PAY_ID_P2'];
                            temp[7] = data[0][i]['GFMIS_CAPITAL_ID_P2']===undefined? "-" : data[0][i]['GFMIS_CAPITAL_ID_P2'];
                            temp[8] = data[0][i]['AREA_CODE_P2']===undefined? "-" : data[0][i]['AREA_CODE_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case '2':
                        console.log(type);
                        for (let i = 0; i < count; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['OWNER_CODE_P1']===undefined? "-" : data[0][i]['OWNER_CODE_P1'];
                            temp[2] = data[0][i]['OWNER_NAME_P1']===undefined? "-" : data[0][i]['OWNER_NAME_P1'];
                            temp[3] = data[0][i]['GROUP_NAME_P1']===undefined? "-" : data[0][i]['GROUP_NAME_P1'] ;

                            temp[4] = data[0][i]['OWNER_CODE_P2']===undefined? "-" : data[0][i]['OWNER_CODE_P2'];
                            temp[5] = data[0][i]['OWNER_NAME_P2']===undefined? "-" : data[0][i]['OWNER_NAME_P2'];
                            temp[6] = data[0][i]['GROUP_NAME_P2']===undefined? "-" : data[0][i]['GROUP_NAME_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case '3':
                        console.log(type);
                        for (let i = 0; i < count; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['ACCOUNT_NO_P1']===undefined? "-" : data[0][i]['ACCOUNT_NO_P1'];
                            temp[2] = data[0][i]['BANK_P1']===undefined? "-" : data[0][i]['BANK_P1'];
                            temp[3] = data[0][i]['ACCOUNT_TYPE__P1']===undefined? "-" : data[0][i]['ACCOUNT_TYPE__P1'];
                            temp[4] = data[0][i]['ZBANK_P1']===undefined? "-" : data[0][i]['ZBANK_P1'];

                            temp[5] = data[0][i]['ACCOUNT_NO_P2']===undefined? "-" : data[0][i]['ACCOUNT_NO_P2'];
                            temp[6] = data[0][i]['BANK_P2']===undefined? "-" : data[0][i]['BANK_P2'];
                            temp[7] = data[0][i]['ACCOUNT_TYPE__P2']===undefined? "-" : data[0][i]['ACCOUNT_TYPE__P2'];
                            temp[8] = data[0][i]['ZBANK_P2']===undefined? "-" : data[0][i]['ZBANK_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case '4':
                        console.log(type);
                        for (let i = 0; i < count; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = Number(data[0][i]['REV_930_TREASURY_MNY_P1']).toLocaleString()===undefined? "-" : Number(data[0][i]['REV_930_TREASURY_MNY_P1']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            temp[2] = Number(data[0][i]['REV_930_OFFICE_MNY_P1']).toLocaleString()===undefined? "-" : Number(data[0][i]['REV_930_OFFICE_MNY_P1']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            temp[3] = Number(data[0][i]['REV_930_WIP_MNY_P1']).toLocaleString()===undefined? "-" : Number(data[0][i]['REV_930_WIP_MNY_P1']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            temp[4] = Number(data[0][i]['REV_930_BANK_MNY_P1']).toLocaleString()===undefined? "-" : Number(data[0][i]['REV_930_BANK_MNY_P1']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });

                            temp[5] = Number(data[0][i]['REV_930_TREASURY_MNY_P2']).toLocaleString()===undefined? "-" : Number(data[0][i]['REV_930_TREASURY_MNY_P2']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            temp[6] = Number(data[0][i]['REV_930_OFFICE_MNY_P2']).toLocaleString()===undefined? "-" : Number(data[0][i]['REV_930_OFFICE_MNY_P2']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            temp[7] = Number(data[0][i]['REV_930_WIP_MNY_P2']).toLocaleString()===undefined? "-" : Number(data[0][i]['REV_930_WIP_MNY_P2']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            temp[8] = Number(data[0][i]['REV_930_BANK_MNY_P2']).toLocaleString()===undefined? "-" : Number(data[0][i]['REV_930_BANK_MNY_P2']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case '5':
                        console.log(type);
                        for (let i = 0; i < count; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['INCOME_GROUP_NAME_P1']===undefined? "-" : data[0][i]['INCOME_GROUP_NAME_P1'];
                            temp[2] = data[0][i]['CLOSE_DTM_P1']===undefined? "-" : data[0][i]['CLOSE_DTM_P1'];
                            temp[3] = Number(data[0][i]['INCOME_MNY_P1']).toLocaleString()===undefined? "-" : Number(data[0][i]['INCOME_MNY_P1']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });

                            temp[4] = data[0][i]['INCOME_GROUP_NAME_P2']===undefined? "-" : data[0][i]['INCOME_GROUP_NAME_P2'];
                            temp[5] = data[0][i]['CLOSE_DTM_P2']===undefined? "-" : data[0][i]['CLOSE_DTM_P2'];
                            temp[6] = Number(data[0][i]['INCOME_MNY_P2']).toLocaleString()===undefined? "-" : Number(data[0][i]['INCOME_MNY_P2']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case '6':
                        console.log(type);
                        var table1 = $("#table-fin-6").DataTable({
                            "language": {
                                "emptyTable": "ไม่มีข้อมูล"
                            },
                            "columnDefs" : [
                                {
                                    "searchable" : false,
                                    "orderable" : false,
                                    "targets" : 0
                                }
                            ,{"className": "dt-right", "targets": [ 9,10]}
                            ],
                            "deferRender" : true,
                            "pagingType" : "simple_numbers",
                            "pageLength" : 5,
                            "processing" : true,
                            "lengthChange": true,
                            "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
                            "ordering" : false,
                            "searching" : false,
                            "drawCallback": function () {
                                currentPageCount = this.api().rows({
                                    page: 'current'
                                }).data().length;
                            }
                        });
                        table1.search("").draw();
                        table1.clear();
                        for (let i = 0; i < count; i++){
                            temp1 = [];
                            temp1[0] = num+1;
                            temp1[1] = data[0][i]['BOOK59_NO_P1']===undefined? "-" : data[0][i]['BOOK59_NO_P1'];
                            temp1[2] = data[0][i]['BOOK59_DATE_P1']===undefined? "-" : data[0][i]['BOOK59_DATE_P1'];
                            temp1[3] = data[0][i]['PAYER_NAME_P1']===undefined? "-" : data[0][i]['PAYER_NAME_P1'];
                            temp1[4] = data[0][i]['RECEIPT_CATEGORY_P1']===undefined? "-" : data[0][i]['RECEIPT_CATEGORY_P1'];
                            temp1[5] = data[0][i]['PARCEL_NO_P1']===undefined? "-" : data[0][i]['PARCEL_NO_P1'];
                            temp1[6] = data[0][i]['SURVEY_NO_P1']===undefined? "-" : data[0][i]['SURVEY_NO_P1'];
                            temp1[7] = data[0][i]['ADDR_TAMBOL_P1']===undefined? "-" : data[0][i]['ADDR_TAMBOL_P1'];
                            temp1[8] = data[0][i]['ADDR_AMPHUR_P1']===undefined? "-" : data[0][i]['ADDR_AMPHUR_P1'];
                            temp1[9] = Number(data[0][i]['STR_MNY_P1']).toLocaleString()===undefined? "-" : Number(data[0][i]['STR_MNY_P1']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            temp1[10] = Number(data[0][i]['REMAIN_MNY_P1']).toLocaleString()===undefined? "-" : Number(data[0][i]['REMAIN_MNY_P1']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            table1.row.add(temp1);
                            num++;
                        }
                        table1.draw();

                        var table2 = $("#table-fin-62").DataTable({
                            "language": {
                                "emptyTable": "ไม่มีข้อมูล"
                            },
                            "columnDefs" : [
                                {
                                    "searchable" : false,
                                    "orderable" : false,
                                    "targets" : 0
                                }
                            ,{"className": "dt-right", "targets": [ 9,10]}
                            ],
                            "deferRender" : true,
                            "pagingType" : "simple_numbers",
                            "pageLength" : 5,
                            "processing" : true,
                            "lengthChange": true,
                            "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
                            "ordering" : false,
                            "searching" : false,
                            "drawCallback": function () {
                                currentPageCount = this.api().rows({
                                    page: 'current'
                                }).data().length;
                            }
                        });
                        table2.search("").draw();
                        table2.clear();
                        num=0;
                        for (let i = 0; i < count; i++){
                            temp2 = [];
                            temp2[0] = num+1;
                            temp2[1] = data[0][i]['BOOK59_NO_P2']===undefined? "-" : data[0][i]['BOOK59_NO_P2'];
                            temp2[2] = data[0][i]['BOOK59_DATE_P2']===undefined? "-" : data[0][i]['BOOK59_DATE_P2'];
                            temp2[3] = data[0][i]['PAYER_NAME_P2']===undefined? "-" : data[0][i]['PAYER_NAME_P2'];
                            temp2[4] = data[0][i]['RECEIPT_CATEGORY_P2']===undefined? "-" : data[0][i]['RECEIPT_CATEGORY_P2'];
                            temp2[5] = data[0][i]['PARCEL_NO_P2']===undefined? "-" : data[0][i]['PARCEL_NO_P2'];
                            temp2[6] = data[0][i]['SURVEY_NO_P2']===undefined? "-" : data[0][i]['SURVEY_NO_P2'];
                            temp2[7] = data[0][i]['ADDR_TAMBOL_P2']===undefined? "-" : data[0][i]['ADDR_TAMBOL_P2'];
                            temp2[8] = data[0][i]['ADDR_AMPHUR_P2']===undefined? "-" : data[0][i]['ADDR_AMPHUR_P2'];
                            temp2[9] = Number(data[0][i]['STR_MNY_P2']).toLocaleString()===undefined? "-" : Number(data[0][i]['STR_MNY_P2']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            temp2[10] = Number(data[0][i]['REMAIN_MNY_P2']).toLocaleString()===undefined? "-" : Number(data[0][i]['REMAIN_MNY_P2']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            table2.row.add(temp2);
                            num++;
                        }
                        table2.draw();
                    break;
                    case '7':
                        console.log(type);
                        var table1 = $("#table-fin-71").DataTable({
                            "language": {
                                "emptyTable": "ไม่มีข้อมูล"
                            },
                            "columnDefs" : [
                                {
                                    "searchable" : false,
                                    "orderable" : false,
                                    "targets" : 0
                                }
                                ,{"className": "dt-center", "targets": [ 10]}
                                ,{"className": "dt-right", "targets": [ 6]}
                            ],
                            "deferRender" : true,
                            "pagingType" : "simple_numbers",
                            "pageLength" : 5,
                            "processing" : true,
                            "lengthChange": true,
                            "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
                            "ordering" : false,
                            "searching" : false,
                            "drawCallback": function () {
                                currentPageCount = this.api().rows({
                                    page: 'current'
                                }).data().length;
                            }
                        });
                        table1.search("").draw();
                        table1.clear();
                        for (let i = 0; i < count; i++){
                            temp1 = [];
                            temp1[0] = num+1;
                            temp1[1] = data[0][i]['QUEUE_NO_P1']===undefined? "-" : data[0][i]['QUEUE_NO_P1'];
                            temp1[2] = data[0][i]['ORDER_NO_P1']===undefined? "-" : data[0][i]['ORDER_NO_P1'];
                            temp1[3] = data[0][i]['RCPT_CAT_P1']===undefined? "-" : data[0][i]['RCPT_CAT_P1'];
                            temp1[4] = data[0][i]['PAYER_NAME_P1']===undefined? "-" : data[0][i]['PAYER_NAME_P1'];
                            temp1[5] = data[0][i]['PARTY_NAME_P1']===undefined? "-" : data[0][i]['PARTY_NAME_P1'];
                            temp1[6] = Number(data[0][i]['RCPT_TOTAL_MNY_P1']).toLocaleString()===undefined? "-" : Number(data[0][i]['RCPT_TOTAL_MNY_P1']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            temp1[7] = data[0][i]['RECEIPT_NO_P1']===undefined? "-" : data[0][i]['RECEIPT_NO_P1'];
                            temp1[8] = data[0][i]['RECEIPT_DTM_P1']===undefined? "-" : convertDtm(data[0][i]['RECEIPT_DTM_P1']);
                            temp1[9] = data[0][i]['STATUS_P1']===undefined? "-" : data[0][i]['STATUS_P1'];

                            // detailOrder = data[0][i]['ORDER_NO_P2']===undefined? "-" : data[0][i]['ORDER_NO_P2'];
                            // detailDate = data[0][i]['RECEIPT_DTM_P2']===undefined? "-" : data[0][i]['RECEIPT_DTM_P2'];
                            // detailRECEIPT = data[0][i]['RECEIPT_NO_P2']===undefined? "-" : data[0][i]['RECEIPT_NO_P2'];
                            temp1[10] ='<a onClick="getDetail1($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';
                            table1.row.add(temp1);
                            num++;
                        }
                        table1.draw();

                        var table2 = $("#table-fin-72").DataTable({
                            "language": {
                                "emptyTable": "ไม่มีข้อมูล"
                            },
                            "columnDefs" : [
                                {
                                    "searchable" : false,
                                    "orderable" : false,
                                    "targets" : 0
                                }
                                ,{"className": "dt-center", "targets": [ 10]}
                                ,{"className": "dt-right", "targets": [ 6]}
                            ],
                            "deferRender" : true,
                            "pagingType" : "simple_numbers",
                            "pageLength" : 5,
                            "processing" : true,
                            "lengthChange": true,
                            "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
                            "ordering" : false,
                            "searching" : false,
                            "drawCallback": function () {
                                currentPageCount = this.api().rows({
                                    page: 'current'
                                }).data().length;
                            }
                        });
                        table2.search("").draw();
                        table2.clear();
                        num=0;
                        for (let i = 0; i < count; i++){
                            temp2 = [];
                            // Number(data[0][i]['SVA_FEESURVEY_AMOUNT_P1']).toLocaleString(undefined, {
                            //     minimumFractionDigits: 2,
                            //     maximumFractionDigits: 2
                            // })
                            temp2[0] = num+1;
                            temp2[1] = data[0][i]['QUEUE_NO_P2']===undefined? "-" : data[0][i]['QUEUE_NO_P2'];
                            temp2[2] = data[0][i]['ORDER_NO_P2']===undefined? "-" : data[0][i]['ORDER_NO_P2'];
                            temp2[3] = data[0][i]['RCPT_CAT_P2']===undefined? "-" : data[0][i]['RCPT_CAT_P2'];
                            temp2[4] = data[0][i]['PAYER_NAME_P2']===undefined? "-" : data[0][i]['PAYER_NAME_P2'];
                            temp2[5] = data[0][i]['PARTY_NAME_P2']===undefined? "-" : data[0][i]['PARTY_NAME_P2'];
                            temp2[6] = Number(data[0][i]['RCPT_TOTAL_MNY_P2']).toLocaleString()===undefined? "-" : Number(data[0][i]['RCPT_TOTAL_MNY_P2']).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            });
                            temp2[7] = data[0][i]['RECEIPT_NO_P2']===undefined? "-" : data[0][i]['RECEIPT_NO_P2'];
                            temp2[8] = data[0][i]['RECEIPT_DTM_P2']===undefined? "-" : convertDtm(data[0][i]['RECEIPT_DTM_P2']);
                            temp2[9] = data[0][i]['STATUS_P2']===undefined? "-" : data[0][i]['STATUS_P2'];

                            // detailOrder = data[0][i]['ORDER_NO_P2']===undefined? "-" : data[0][i]['ORDER_NO_P2'];
                            // detailDate = data[0][i]['RECEIPT_DTM_P2']===undefined? "-" : data[0][i]['RECEIPT_DTM_P2'];
                            // detailRec = data[0][i]['RECEIPT_NO_P2']===undefined? "-" : data[0][i]['RECEIPT_NO_P2'];

                            temp2[10] ='<a onClick="getDetail2($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';
                            table2.row.add(temp2);
                            num++;
                        }
                        table2.draw();
                        $('#overlay7.overlay-main').hide();
                    break;
                }


            } else {
                table.search("").draw();
                table.clear();

            }
            if(type != 6 && type != 7){
                table.draw();
            }
            $('#overlay'+type+'.overlay-main').hide();

        }
    });
}




function getSummary(landoffice){
    // $('.overlay-main').show();
    $('#summary.overlay-main').show();
    $.ajax({
        type : "POST",
        dataType : "json",
        url : "./queries/fin/getFinSum.php",
        data : 'landoffice=' + landoffice,
        success: function(data){
            console.log(data);
            datatemp = data;    
            var table = $("#table-fin-sum").DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable" : false,
                        "targets" : 0
                    }
                    ,{"className": "dt-center", "targets": [ 2, 3, 4]}
                ],
                "deferRender" : true,
                "processing" : true,
                "lengthChange": false,
                "ordering" : false,
                "searching" : false,
                "paging" : false,
                "info" : false,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
                    });

            if(!jQuery.isEmptyObject(data)){
                table.search("").draw();
                table.clear();
                num = 0;
                for (let i = 0; i < data[0].length; i++){
                    temp = [];
                    temp[0] = num + 1;
                    temp[1] = data[0][i]['TYPE']===undefined? "-" : data[0][i]['TYPE'];
                    temp[2] = Number(data[0][i]['RECEIVE']).toLocaleString()===undefined? "-" : Number(data[0][i]['RECEIVE']).toLocaleString();
                    temp[3] = Number(data[0][i]['MIGRATE_SUCCESS']).toLocaleString()===undefined? "-" : Number(data[0][i]['MIGRATE_SUCCESS']).toLocaleString();
                    temp[4] = Number(data[0][i]['MIGRATE_ERROR']).toLocaleString()===undefined? "-" : Number(data[0][i]['MIGRATE_ERROR']).toLocaleString();
                    table.row.add(temp);
                    num++;
                }
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#summary.overlay-main').hide();
        }
    })
}
