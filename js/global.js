var landoffice = '';
var type = '';
var typ = '';
var typeSeq = '';
var amphur = '';
var datatemp = '';
var category = '';
var parcelNo = '';
var parcelLandNo = '';
var survey = '';
var tambon = '';
var moo = '';
var currentPageCount = 0;
var checkboxAll = '';
var recv_date = '';
var version = '';
var pdfDataEntry = [];
var current_href = '';
var parcelYear = '';
var ctnType = '';
var current_tab_href = '';
var tab_type = '';


var columnsParcelDetailMain = {
  'PARCEL_NO_P1': 'เลขที่เอกสารสิทธิ',
  'PARCEL_LAND_NO_P1': 'เลขที่เอกสารสิทธิ',
  'PRINTPLATE_TYPE_NAME_P1': 'ชื่อประเภทเอกสารสิทธิ',
  'TAMBOL_NAME_P1': 'ตำบล/แขวง',
  'AMPHUR_NAME_P1': 'อำเภอ/เขต',
  'PARCEL_SURVEY_NO_P1': 'หน้าสำรวจ',
  'PARCEL_BOOK_P1': 'เลขหนังสือ',
  'PARCEL_PAGE_P1': 'เลขหน้า',
  'AREA_P1': 'ขนาดพื้นที่',
  'UTMSCALE_SEQ_P1': 'มาตราส่วนระวาง UTM',
  'UTM_P1': 'ระวาง UTM',
  'PARCEL_UTM_LAND_NO_P1': 'เลขที่ดินระวาง UTM',
  'ORIGINSCALE_SEQ_P1': 'มาตราส่วนระวางศูนย์กำเนิด',
  'ORIGIN_P1': 'ระวางศูนย์กำเนิด',
  'PARCEL_ORIGIN_LAND_NO_P1': 'เลขที่ดินระวางศูนย์กำเนิด',
  'OWN_P1': 'ชื่อผู้ถือกรรมสิทธิ์',
  'PARCEL_OWNER_NUM_P1': 'จำนวนผู้ถือกรรมสิทธิ์',
  'PARCEL_PROCESS_DTM_P1': 'วันที่การจดทะเบียนครั้งสุดท้าย',
  'PARCEL_REGIST_NAME_P1': 'ประเภทการจดทะเบียนครั้งสุดท้าย',
  'PARCEL_OPT_FLAG_P1': 'สถานะ อปท.',
  'OPT_NAME_P1': 'ชื่อ อปท.',
  'PARCEL_SEQUEST_STS_P1': 'สถานะการอายัด',
  'PARCEL_OBLIGATION_NUM_P1': 'จำนวนภาระผูกพัน',
  'PARCEL_LAND_MOO_P1': 'หมู่',
  'PARCEL_LAND_SURVEY_NO_P1': 'เลขสำรวจ',
  'PARCEL_LAND_OBTAIN_DTM_P1': 'ปีที่ออก',
  'PARCEL_LAND_BOOK_P1': 'เลขหนังสือ',
  'PARCEL_LAND_PAGE_P1': 'เลขหนัา',
  'PARCEL_LAND_UTM_LAND_NO_P1': 'เลขที่ดินระวาง UTM',
  'PARCEL_LAND_ORIGIN_LAND_NO_P1': 'เลขที่ดินระวางศูนย์กำเนิด',
  'PARCEL_LAND_OWNER_NUM_P1': 'จำนวนผู้ถือกรรมสิทธิ์',
  'PARCEL_LAND_PROCESS_DTM_P1': 'วันที่การจดทะเบียนครั้งสุดท้าย',
  'PARCEL_LAND_REGIST_NAME_P1': 'ประเภทการจดทะเบียนครั้งสุดท้าย',
  'PARCEL_LAND_OPT_FLAG_P1': 'สถานะ อปท.',
  'PARCEL_LAND_SEQUEST_STS_P1': 'สถานะการอายัด',
  'PARCEL_LAND_OBLIGATION_NUM_P1': 'จำนวนภาระผูกพัน',
  'PARCEL_LAND_LANDUSED_DESC_P1': 'การใช้ประโยชน์',
  'PARCEL_LANDUSED_DESC_P1': 'การใช้ประโยชน์',
  'CONDO_ID_P1': 'เลขที่อาคารชุด',
  'CONDO_NAME_TH_P1': 'ชื่ออาคารชุด',
  'CONDO_BLD_P1': 'จำนวนอาคาร',
  'CONDO_ROOM_NUM_P1': 'จำนวนห้องชุด',
  'CONDO_RECV_DATE_P1': 'วันที่ได้รับอนุญาต',
  'CONDO_REGISTER_DATE_P1': 'วันที่จดทะเบียนอาคารชุด',
  'CORP_ID_P1': 'เลขที่นิติบุคคลอาคารชุด',
  'CONDO_CORP_NAME_P1': 'ชื่อนิติบุคคลอาคารชุด',
  'CONDO_CORP_REGDTM_P1': 'วันที่จดทะเบียนนิติบุคคลอาคารชุด',
  'ADDR_P1': 'ที่ตั้งนิติบุคคลอาคารชุด',
  'CONDOROOM_RNO_P1': 'เลขที่ห้องชุด',
  'BLD_NAME_TH_P1': 'ชื่ออาคาร',
  'CONDOROOM_AREA_NUM_P1': 'ขนาดพื้นที่',
  'CONDOROOM_OWNER_NUM_P1': 'จำนวนผู้ถือกรรมสิทธิ์',
  'CONDOROOM_PROCESS_DTM_P1': 'วันที่การจดทะเบียนครั้งสุดท้าย',
  'CONDOROOM_REGIST_NAME_P1': 'ประเภทการจดทะเบียนครั้งสุดท้าย',
  'CONDOROOM_OPT_FLG_P1': 'สถานะ อปท.',
  'CONDOROOM_OPT_FLAG_P1': 'สถานะ อปท.',
  'CONDOROOM_SEQUEST_STS_P1': 'สถานะการอายัด',
  'CONDOROOM_OBLIGATION_NUM_P1': 'จำนวนภาระผูกพัน',
  'CONSTRUCT_ADDR_HID_P1': 'รหัสประจำบ้าน',
  'CONSTRUCT_AREA_NUM_P1': 'เนื้อที่',
  'CONSTRUCT_PROCESS_DTM_P1': 'วันที่จดทะเบียนล่าสุด',
  'CONSTRUCT_PROCESS_DATE_P1': 'วันที่จดทะเบียนล่าสุด',
  'CONSTRUCT_REGIST_NAME_P1': 'ประเภทจดทะเบียนล่าสุด',
  'CONSTRUCT_SEQUEST_STS_P1': 'สถานะอายัด',
  'PARCEL_NO_P2': 'เลขที่เอกสารสิทธิ',
  'PARCEL_LAND_NO_P2': 'เลขที่เอกสารสิทธิ',
  'PRINTPLATE_TYPE_NAME_P2': 'ชื่อประเภทเอกสารสิทธิ',
  'TAMBOL_NAME_P2': 'ตำบล/แขวง',
  'AMPHUR_NAME_P2': 'อำเภอ/เขต',
  'PARCEL_SURVEY_NO_P2': 'หน้าสำรวจ',
  'PARCEL_BOOK_P2': 'เลขหนังสือ',
  'PARCEL_PAGE_P2': 'เลขหน้า',
  'AREA_P2': 'ขนาดพื้นที่',
  'UTMSCALE_SEQ_P2': 'มาตราส่วนระวาง UTM',
  'UTM_P2': 'ระวาง UTM',
  'PARCEL_UTM_LAND_NO_P2': 'เลขที่ดินระวาง UTM',
  'ORIGINSCALE_SEQ_P2': 'มาตราส่วนระวางศูนย์กำเนิด',
  'ORIGIN_P2': 'ระวางศูนย์กำเนิด',
  'PARCEL_ORIGIN_LAND_NO_P2': 'เลขที่ดินระวางศูนย์กำเนิด',
  'OWN_P2': 'ชื่อผู้ถือกรรมสิทธิ์',
  'PARCEL_OWNER_NUM_P2': 'จำนวนผู้ถือกรรมสิทธิ์',
  'PARCEL_PROCESS_DTM_P2': 'วันที่การจดทะเบียนครั้งสุดท้าย',
  'PARCEL_REGIST_NAME_P2': 'ประเภทการจดทะเบียนครั้งสุดท้าย',
  'PARCEL_OPT_FLAG_P2': 'สถานะ อปท.',
  'OPT_NAME_P2': 'ชื่อ อปท.',
  'PARCEL_SEQUEST_STS_P2': 'สถานะการอายัด',
  'PARCEL_OBLIGATION_NUM_P2': 'จำนวนภาระผูกพัน',
  'PARCEL_LAND_MOO_P2': 'หมู่',
  'PARCEL_LAND_SURVEY_NO_P2': 'เลขสำรวจ',
  'PARCEL_LAND_OBTAIN_DTM_P2': 'ปีที่ออก',
  'PARCEL_LAND_BOOK_P2': 'เลขหนังสือ',
  'PARCEL_LAND_PAGE_P2': 'เลขหนัา',
  'PARCEL_LAND_UTM_LAND_NO_P2': 'เลขที่ดินระวาง UTM',
  'PARCEL_LAND_ORIGIN_LAND_NO_P2': 'เลขที่ดินระวางศูนย์กำเนิด',
  'PARCEL_LAND_OWNER_NUM_P2': 'จำนวนผู้ถือกรรมสิทธิ์',
  'PARCEL_LAND_PROCESS_DTM_P2': 'วันที่การจดทะเบียนครั้งสุดท้าย',
  'PARCEL_LAND_REGIST_NAME_P2': 'ประเภทการจดทะเบียนครั้งสุดท้าย',
  'PARCEL_LAND_OPT_FLG_P2': 'สถานะ อปท.',
  'PARCEL_LAND_OPT_FLAG_P2': 'สถานะ อปท.',
  'PARCEL_LAND_SEQUEST_STS_P2': 'สถานะการอายัด',
  'PARCEL_LAND_OBLIGATION_NUM_P2': 'จำนวนภาระผูกพัน',
  'PARCEL_LANDUSED_DESC_P2': 'การใช้ประโยชน์',
  'CONDO_ID_P2': 'เลขที่อาคารชุด',
  'CONDO_NAME_TH_P2': 'ชื่ออาคารชุด',
  'CONDO_BLD_P2': 'จำนวนอาคาร',
  'CONDO_ROOM_NUM_P2': 'จำนวนห้องชุด',
  'CONDO_RECV_DATE_P2': 'วันที่ได้รับอนุญาต',
  'CONDO_REGISTER_DATE_P2': 'วันที่จดทะเบียนอาคารชุด',
  'CORP_ID_P2': 'เลขที่นิติบุคคลอาคารชุด',
  'CONDO_CORP_NAME_P2': 'ชื่อนิติบุคคลอาคารชุด',
  'CONDO_CORP_REGDTM_P2': 'วันที่จดทะเบียนนิติบุคคลอาคารชุด',
  'ADDR_P2': 'ที่ตั้งนิติบุคคลอาคารชุด',
  'CONDOROOM_RNO_P2': 'เลขที่ห้องชุด',
  'BLD_NAME_TH_P2': 'ชื่ออาคาร',
  'CONDOROOM_AREA_NUM_P2': 'ขนาดพื้นที่',
  'CONDOROOM_OWNER_NUM_P2': 'จำนวนผู้ถือกรรมสิทธิ์',
  'CONDOROOM_PROCESS_DTM_P2': 'วันที่การจดทะเบียนครั้งสุดท้าย',
  'CONDOROOM_REGIST_NAME_P2': 'ประเภทการจดทะเบียนครั้งสุดท้าย',
  'CONDOROOM_OPT_FLG_P2': 'สถานะ อปท.',
  'CONDOROOM_SEQUEST_STS_P2': 'สถานะการอายัด',
  'CONDOROOM_OBLIGATION_NUM_P2': 'จำนวนภาระผูกพัน',
  'CONSTRUCT_ADDR_HID_P2': 'รหัสประจำบ้าน',
  'CONSTRUCT_AREA_NUM_P2': 'เนื้อที่',
  'CONSTRUCT_PROCESS_DTM_P2': 'วันที่จดทะเบียนล่าสุด',
  'CONSTRUCT_PROCESS_DATE_P2': 'วันที่จดทะเบียนล่าสุด',
  'CONSTRUCT_REGIST_NAME_P2': 'ประเภทจดทะเบียนล่าสุด',
  'CONSTRUCT_SEQUEST_STS_P2': 'สถานะอายัด',
  'PARCEL_LAND_LANDUSED_DESC_P2': 'การใช้ประโยชน์',
};

var columnOwnerDetailMain = {
  'OWNER_ORDER_P1': 'ลำดับผู้ถือกรรมสิทธิ์',
  'OWNER_PID_P1': 'เลขบัตรประจำตัวประชาชน',
  'OWN_P1': 'ชื่อผู้ถือกรรมสิทธิ์',
  'OWNER_GENDER_P1': 'เพศผู้ถือกรรมสิทธิ์',
  'OWNER_BDATE_P1': 'วันเกิดผู้ถือกรรมสิทธิ์',
  'OWNER_PN_STS_P1': 'สถานะสมรสผู้ถือกรรมสิทธิ์',
  'NATIONALITY_NAME_P1': 'สัญชาติผู้ถือกรรมสิทธิ์',
  'RACE_NAME_P1': 'เชื้อชาติผู้ถือกรรมสิทธิ์',
  'RELIGION_NAME_P1': 'ศาสนาผู้ถือกรรมสิทธิ์',
  'FAT_P1': 'ชื่อบิดาผู้ถือกรรมสิทธิ์',
  'MOT_P1': 'ชื่อมารดาผู้ถือกรรมสิทธิ์',
  'REG_ADDR_P1': 'ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์',
  'ADDRESS_P1': 'ที่อยู่',
  'RECV_DATE_P1': 'วันที่ได้มา',
  'OBTAIN_NAME_P1': 'การได้มา',
  'NUM_P1': 'สัดส่วนการได้มา',
  'OWNER_ORDER_P2': 'ลำดับผู้ถือกรรมสิทธิ์',
  'OWNER_PID_P2': 'เลขบัตรประจำตัวประชาชน',
  'OWN_P2': 'ชื่อผู้ถือกรรมสิทธิ์',
  'OWNER_GENDER_P2': 'เพศผู้ถือกรรมสิทธิ์',
  'OWNER_BDATE_P2': 'วันเกิดผู้ถือกรรมสิทธิ์',
  'OWNER_PN_STS_P2': 'สถานะสมรสผู้ถือกรรมสิทธิ์',
  'NATIONALITY_NAME_P2': 'สัญชาติผู้ถือกรรมสิทธิ์',
  'RACE_NAME_P2': 'เชื้อชาติผู้ถือกรรมสิทธิ์',
  'RELIGION_NAME_P2': 'ศาสนาผู้ถือกรรมสิทธิ์',
  'FAT_P2': 'ชื่อบิดาผู้ถือกรรมสิทธิ์',
  'MOT_P2': 'ชื่อมารดาผู้ถือกรรมสิทธิ์',
  'REG_ADDR_P2': 'ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์',
  'ADDRESS_P2': 'ที่อยู่',
  'RECV_DATE_P2': 'วันที่ได้มา',
  'OBTAIN_NAME_P2': 'การได้มา',
  'NUM_P2': 'สัดส่วนการได้มา',
}

var columnOlgtDetailMain = {
  'OLGT_HD_SEQ_P1': 'เลข Seq ภาระผูกพัน',
  'OLGT_HD_CON_NAME_P1': 'ชื่อหนังสือสัญญา',
  'OLGT_HD_CON_NO_P1': 'เลขที่หนังสือสัญญา',
  'OLGT_HD_CON_DATE_P1': 'วันที่หนังสือสัญญา',
  'OLGT_HD_CON_STDTM_P1': 'วันที่เริ่มสัญญา',
  'OLGT_HD_CON_ENDTM_P1': 'วันที่สิ้นสุดสัญญา',
  'OLGT_HD_MNY_P1': 'ราคาทุนทรัพย์',
  'OLGT_CON_HD_PERIOD_FLG_P1': 'ประเภทระยะเวลา',
  'OLGT_HD_CON_PERIOD_FLG_P1': 'ประเภทระยะเวลา',
  'DURATION_P1': 'ระยะเวลา',
  'PARTY_TYPE_P1': 'ประเภทคู่สัญญา',
  'OLGT_PMS_PID_P1': 'เลขบัตรประจำตัวประชาชนคู่สัญญา',
  'OWN_P1': 'ชื่อคู่สัญญา',
  'OLGT_HD_SEQ_P2': 'เลข Seq ภาระผูกพัน',
  'OLGT_HD_CON_NAME_P2': 'ชื่อหนังสือสัญญา',
  'OLGT_HD_CON_NO_P2': 'เลขที่หนังสือสัญญา',
  'OLGT_HD_CON_DATE_P2': 'วันที่หนังสือสัญญา',
  'OLGT_HD_CON_STDTM_P2': 'วันที่เริ่มสัญญา',
  'OLGT_HD_CON_ENDTM_P2': 'วันที่สิ้นสุดสัญญา',
  'OLGT_HD_MNY_P2': 'ราคาทุนทรัพย์',
  'OLGT_CON_HD_PERIOD_FLG_P2': 'ประเภทระยะเวลา',
  'OLGT_HD_CON_PERIOD_FLG_P2': 'ประเภทระยะเวลา',
  'DURATION_P2': 'ระยะเวลา',
  'PARTY_TYPE_P2': 'ประเภทคู่สัญญา',
  'OLGT_PMS_PID_P2': 'เลขบัตรประจำตัวประชาชนคู่สัญญา',
  'OWN_P2': 'ชื่อคู่สัญญา',
}

var columnPromiseDetailMain = {
  'PID_PROM': 'เลขประจำตัวประชาชน',
  'NAME_PROM': 'ผู้ให้สัญญา:ผู้รับสัญญา',
  'NAME_AGT': 'ผู้ดำเนินการแทน',
  'NAME_ASN': 'ผู้รับมอบอำนาจ',
  'NAME_TES': 'ผู้จัดการมรดก',
  'ORD': 'ลำดับ',
  'TYPE': 'ประเภท'
}

var columnObtainDetailMain = {
  'PID_PROM': 'เลขประจำตัวประชาชน',
  'PROM_NAME': 'ชื่อผู้ถือกรรมสิทธิ',
  'PRINTPLATE_TYPE': 'ประเภทเอกสารสิทธิ',
  'NUM_RECV': 'สัดส่วนการได้มา',
  'PROCESS_PPO_OWN_TEMP_REC_DATE': 'วันที่ได้มา',
  'ORD': 'ลำดับ',
  'AMPHUR_NAME': 'อำเภอ',
  'NO': 'เลขที่เอกสารสิทธิ'

}

var columnProcessDetailMain = {
  'PROCESS_TEMP_MNY': 'ราคาทุนทรัพย์การจดทะเบียน',
  'PROCESS_PPMS_TEMP_PROPERTY_NO': 'เลขที่เอกสารสิทธิ/เลขที่สิ่งปลูกสร้าง',
  'PROCESS_PARCEL_TEMP_USE_DETAIL': 'การใช้ประโยชน์',
  'NAME_PROMISOR': 'ชื่อผู้ให้สัญญา',
  'NUM_PROMISOR': 'สัดส่วนผู้ให้สัญญา',
  'NAME_PROMISEE': 'ชื่อผู้รับสัญญา',
  'NUM_PROMISEE': 'สัดส่วนผู้รับสัญญา',
  'DURATION': 'ระยะเวลา',
  'DURATION_ADD': 'ขยายระยะเวลา',
  'DURATION_OLGT': 'ระยะเวลาภาระผูกพัน',
  'PROCESS_PARCEL_TEMP_CONS': 'สิ่งปลูกสร้าง',
  'PROCESS_TEMP_COURT_STS': 'คำสั่งศาล'
}

var columnDocumentDetailMain = {
  'DOCUMENT_ABBR': 'ชื่อย่อแบบพิมพ์',
  'DOCUMENT_NAME': 'ชื่อแบบพิมพ์',
  'PROCESS_DOCUMENT_TEMP_TITLE': 'ชื่อหัวเอกสาร',
  'TITLE_EXT': 'ชื่อเอกสารจดทะเบียน',

}

var columnCtnFinDetailMain = {
  'REV_HD_SEQ_P1': 'ลำดับที่รับพัสดุ',
  'ISS_HD_NO_P1': 'เลขที่ใบขอเบิก',
  'ISS_HD_DATE_P1': 'วันที่หนังสือใบขอเบิก',
  'ISS_HD_PAY_DATE_P1': 'วันที่จ่าย',
  'ISS_HD_SEND_FLAG_P1': 'วิธีการจัดส่ง',
  'ISS_HD_TYPE_FLAG_P1': 'ประเภทการขอเบิก',
  'ISS_HD_TOPIC_FLAG_P1': 'ประเภทเรื่อง',
  'ISS_HD_STS_P1': 'สถานะเบิกจ่าย',
  'PSN_NAME_P1': 'ชื่อผู้ขอเบิก',
  'PSN_G_NAME_P1': 'ชื่อผู้ให้เบิก',
  'REV_HD_SEQ_P2': 'ลำดับที่รับพัสดุ',
  'ISS_HD_NO_P2': 'เลขที่ใบขอเบิก',
  'ISS_HD_DATE_P2': 'วันที่หนังสือใบขอเบิก',
  'ISS_HD_PAY_DATE_P2': 'วันที่จ่าย',
  'ISS_HD_SEND_FLAG_P2': 'วิธีการจัดส่ง',
  'ISS_HD_TYPE_FLAG_P2': 'ประเภทการขอเบิก',
  'ISS_HD_TOPIC_FLAG_P2': 'ประเภทเรื่อง',
  'ISS_HD_STS_P2': 'สถานะเบิกจ่าย',
  'PSN_NAME_P2': 'ชื่อผู้ขอเบิก',
  'PSN_G_NAME_P2': 'ชื่อผู้ให้เบิก',
}

var columnCtnFinRtnDetailMain = {
  'REV_HD_SEQ_P1': 'ลำดับที่รับพัสดุ',
  'RTN_HD_NO_P1': 'เลขที่ใบส่งคืน',
  'RTN_HD_DATE_P1': 'วันที่หนังสือใบส่งคืน',
  'RTN_HD_PAY_DATE_P1': 'วันที่จ่าย',
  'RTN_HD_SEND_FLAG_P1': 'วิธีการจัดส่ง',
  'RTN_HD_TYPE_FLAG_P1': 'ประเภทการส่งคืน',
  'RTN_HD_TOPIC_FLAG_P1': 'ประเภทเรื่อง',
  'RTN_HD_STS_P1': 'สถานะส่งคืน',
  'PSN_NAME_P1': 'ชื่อผู้ส่งคืน',
  'PSN_G_NAME_P1': 'ชื่อผู้ส่งคืนไปยัง',
  'REV_HD_SEQ_P2': 'ลำดับที่รับพัสดุ',
  'RTN_HD_NO_P2': 'เลขที่ใบส่งคืน',
  'RTN_HD_DATE_P2': 'วันที่หนังสือใบส่งคืน',
  'RTN_HD_PAY_DATE_P2': 'วันที่จ่าย',
  'RTN_HD_SEND_FLAG_P2': 'วิธีการจัดส่ง',
  'RTN_HD_TYPE_FLAG_P2': 'ประเภทการส่งคืน',
  'RTN_HD_TOPIC_FLAG_P2': 'ประเภทเรื่อง',
  'RTN_HD_STS_P2': 'สถานะส่งคืน',
  'PSN_NAME_P2': 'ชื่อผู้ส่งคืน',
  'PSN_G_NAME_P2': 'ชื่อผู้ส่งคืนไปยัง',
}
var columnSVC_parcelDescDetailMain = {
  'PCM_ORDERNO_P1': 'ลำดับหมุดหลักเขตในแปลงที่ดิน',
  'PCM_BNDNAME_P1': 'ชื่อหมุดหลักเขต',
  'PAR_AREA_COOR_P1': 'เนื้อที่พิกัดฉาก',
  'PAR_PARCEL_NO_P1': 'เลขที่เอกสารสิทธิ',
  'PAR_LAND_NO_P1': 'เลขที่ดิน',
  'PAR_SURVEY_NO_P1': 'หน้าสำรวจ',
  'PAR_TYPE_P1': 'ประเภทรูปแปลงที่ดิน',
  'PAR_SUMPAIR_N_P1': 'ผลรวมของผลบวก คูณ พิกัดเหนือ',
  'PAR_SUMPAIR_S_P1': 'ผลรวมของผลบวก คูณ พิกัดใต้',
  'SCALE_SEQ_P1': 'มาตราส่วนระวาง UTM',
  'PROVINCE_NAME_P1': 'จังหวัด',
  'AMPHUR_NAME_P1': 'อำเภอ',
  'TAMBOL_NAME_P1': 'ตำบล',
  'OLD_AREA_P1': 'เนื้อที่เดิม',
  'CAL_AREA_P1': 'เนื้อที่คำนวณ',
  'USE_AREA_P1': 'เนื้อที่ใช้',
  'REAL_AREA_P1': 'เนื้อที่จริง',
  'PAR_UTM_P1': 'ระวาง UTM',
  'PCM_ORDERNO_P2': 'ลำดับหมุดหลักเขตในแปลงที่ดิน',
  'PCM_BNDNAME_P2': 'ชื่อหมุดหลักเขต',
  'PAR_AREA_COOR_P2': 'เนื้อที่พิกัดฉาก',
  'PAR_PARCEL_NO_P2': 'เลขที่เอกสารสิทธิ',
  'PAR_LAND_NO_P2': 'เลขที่ดิน',
  'PAR_SURVEY_NO_P2': 'หน้าสำรวจ',
  'PAR_TYPE_P2': 'ประเภทรูปแปลงที่ดิน',
  'PAR_SUMPAIR_N_P2': 'ผลรวมของผลบวก คูณ พิกัดเหนือ',
  'PAR_SUMPAIR_S_P2': 'ผลรวมของผลบวก คูณ พิกัดใต้',
  'SCALE_SEQ_P2': 'มาตราส่วนระวาง UTM',
  'PROVINCE_NAME_P2': 'จังหวัด',
  'AMPHUR_NAME_P2': 'อำเภอ',
  'TAMBOL_NAME_P2': 'ตำบล',
  'OLD_AREA_P2': 'เนื้อที่เดิม',
  'CAL_AREA_P2': 'เนื้อที่คำนวณ',
  'USE_AREA_P2': 'เนื้อที่ใช้',
  'REAL_AREA_P2': 'เนื้อที่จริง',
  'PAR_UTM_P2': 'ระวาง UTM',
}
// ชื่อผู้ขอรังวัด
// วันที่ทำการรังวัด
// วันที่สิ้นสุดรังวัด
// ชนิดหลักเขตที่ใชัรังวัด
// จำนวนหลักเขตที่ใช้ปัก
// ชื่อผู้ตรวจ
// ผู้ตรวจมุม,ระยะ
var columnSvcSurveyDescDetail = {
  'APPROVE_ANGDIST_P1': "ผู้ตรวจมุม,ระยะ",
  'APPROVE_NAME_P1': "ชื่อผู้ตรวจ",
  'BOX_NO_P1': "เลขแฟ้มหรือกล่อง",
  'OWNER_NAME_P1': "ชื่อผู้ขอรังวัด",
  'SURVEY_BND_NO_P1': "จำนวนหลักเขตที่ใช้ปัก",
  'SURVEY_BND_TYPE_P1': "ชนิดหลักเขตที่ใชัรังวัด",
  'SURVEY_ENDDATE_P1': "วันที่สิ้นสุดรังวัด",
  'SURVEY_STARTDATE_P1': "วันที่ทำการรังวัด",
  'APPROVE_ANGDIST_P1': "ผู้ตรวจมุม,ระยะ",
  'APPROVE_NAME_P2': "ชื่อผู้ตรวจ",
  'BOX_NO_P2': "เลขแฟ้มหรือกล่อง",
  'OWNER_NAME_P2': "ชื่อผู้ขอรังวัด",
  'SURVEY_BND_NO_P2': "ชนิดหลักเขตที่ใชัรังวัด",
  'SURVEY_BND_TYPE_P2': "ชนิดหลักเขตที่ใชัรังวัด",
  'SURVEY_ENDDATE_P2': "วันที่สิ้นสุดรังวัด",
  'SURVEY_STARTDATE_P2': "วันที่ทำการรังวัด",
}

var columnSVO_projectDetailMain = {
  'PRINTPLATE_TYPE_NAME_P1': 'ประเภทเอกสารสิทธิ',
  'METHODOFSURVEY_NAME_P1': 'วิธีการรังวัด',
  'SURVEYJOB_NO_P1': 'เลขที่คำขอรังวัด (ร.ว.12)',
  'SURVEYJOB_DATE_P1': 'วันที่รับคำขอ',
  'TYPEOFSURVEY_NAME_P1': 'ประเภทการรังวัด',
  'PROJECT_NAME_P1': 'ชื่อโครงการ',
  'PROJECT_DOCNO_P1': 'เลขที่หนังสือ',
  'PROJECT_DOCDATE_P1': 'ลงวันที่',
  'PROJECT_OFFICEOWNER_P1': 'หน่วยงาน',
  'PROJECTTYPE_NAME_P1': 'ประเภทโครงการ', 
  'MAINTYPEOFSURVEY_NAME_P1': 'ประเภทการรังวัดหลัก', 
  'PRINTPLATE_TYPE_NAME_P2': 'ประเภทเอกสารสิทธิ',
  'METHODOFSURVEY_NAME_P2': 'วิธีการรังวัด',
  'SURVEYJOB_NO_P2': 'เลขที่คำขอรังวัด (ร.ว.12)',
  'SURVEYJOB_DATE_P2': 'วันที่รับคำขอ',
  'TYPEOFSURVEY_NAME_P2': 'ประเภทการรังวัด',
  'PROJECT_NAME_P2': 'ชื่อโครงการ',
  'PROJECT_DOCNO_P2': 'เลขที่หนังสือ',
  'PROJECT_DOCDATE_P2': 'ลงวันที่',
  'PROJECT_OFFICEOWNER_P2': 'หน่วยงาน',
  'PROJECTTYPE_NAME_P2': 'ประเภทโครงการ', 
  'MAINTYPEOFSURVEY_NAME_P2': 'ประเภทการรังวัดหลัก', 
}

var columnSequesterDetailMain = {
  'SEQUESTER_ORDER_NO': 'อายัดลำดับที่',
  'SEQUESTER_DOC_NO': 'เลขที่คำสั่ง/หนังสือ',
  'SEQUESTER_DOC_DTM': 'วันที่คำสั่ง/หนังสือ',
  'SEQUESTER_REQ_DEPT': 'หน่วยงานที่ขออายัด',
  'SEQUESTER_REQ_PID': 'เลขที่บัตรประชาชนของผู้ขออายัด',
  'SEQUESTER_REQ_NAME': 'ชื่อ-สกุลผู้ขออายัด',
  'SEQUESTER_EXPIRE_STS': 'สถานะกำหนดอายัด',
  'SEQUESTER_REQ_DTM': 'วันที่ขออายัด',
  'SEQUESTER_REC_DTM': 'วันที่รับอายัด',
  'SEQUESTER_EXPIRE_NUM': 'จำนวนวันที่รับอายัด',
  'SEQUESTER_EXPIRE_DTM': 'วันที่สิ้นสุดอายัด',
  'SEQUESTER_REM': 'เหตุผลที่ขออายัด',
  'SEQUESTER_REQ_CONDITION': 'เงื่อนไขการอายัด',
  'OWNER_NAME': 'ชื่อผู้ถูกอายัด'
}

var columnAPSDetailMain = {
  'AMPHUR_NAME': 'อำเภอ',
  'AREA': 'ขนาดพื้นที่',
  'NO': 'เลขที่เอกสารสิทธิ',
  'NUM_OPT': 'สัดส่วน อปท.',
  'OPT_NAME': 'ผู้จัดการมรดก',
  'PRINTPLATE_TYPE': 'ประเภทเอกสารสิทธิ',
  'PROCESS_PARCEL_APS_TEMP_NUM': 'ราคาประเมินต่อหน่วย',
  'PROCESS_PARCEL_APS_TEMP_TOT': 'ราคาประเมินรวม',

}
var ctnColumn = {
  '1.1': 'ข้อมูลหนังสือรับ (ไม่มีชั้นความลับ)',
  '1.2': 'ข้อมูลหนังสือรับ (มีชั้นความลับ)',
  '1.3': 'ข้อมูลหนังสือส่ง (ไม่มีชั้นความลับ)',
  '1.4': 'ข้อมูลหนังสือส่ง (มีชั้นความลับ)',
  '1.7': 'ข้อมูลเลขที่คำสั่งสำนักงานที่ดิน',
  '2.1': 'ทะเบียนคุมอาคาร',
  '2.3': 'แบบพิมพ์การเงิน',
  '2.4': 'แบบพิมพ์ทั่วไป',
  '2.5': 'แบบพิมพ์แสดงสิทธิในที่ดิน',
  '2.7': 'ทะเบียนคุมหลักเขต',
  '3.1': 'บุคลากร',
  '3.2': 'ข้อมูลการลา',

}

var ColumnRCPTdetail = {
  'RECEIPT_NO': 'เลขที่ใบเสร็จรับเงิน',
  'RECEIPT_DTM': 'วันที่ใบเสร็จ',
  'LANDOFFICE_NAME_RECEIPT': 'สำนักงานที่ดิน',
  'RECEIPT_TYPE': 'ประเภทใบเสร็จ',
  'RCPT_GROUP': 'ประเภทรายการ',
  'RCPT_CAT': 'ประเภทคำขอ',
  'OPT_SHOW': 'อปท',
  'ORDER_NO': 'เลขที่ใบสั่ง',
  'PAYER_NAME': 'ได้รับเงินจาก', 
  'PARTY_NAME': 'คู่สัญญา',
  'QUEUE_NO': 'เลขที่คิว',
  'ASSET_MNY': 'ราคาทุนทรัพย์(บาท)',
  'EVALUATE_MNY' : 'ราคาประเมิน(บาท)',
  'PLATE_ABBR_NAME' : 'หนังสือสำหรับ',
  'PARCEL_NO' : 'เลขที่',
  'SHEET_NO' : 'ระวาง',
  'LAND_NO' : 'เลขที่ดิน',
  'SURVEY_NO' : 'หน้าสำรวจ',
  'MOO' : 'หมู่ที่',
  'ADDR_TAMBOL' : 'ตำบล/แแขวง',
  'ADDR_AMPHUR' : 'อำเภอ/เขต',
  'ADDR_PROVINCE' : 'จังหวัด',
  'CANCEL_CAUSE_REM' : 'หมายเหตุ'
  
}

var months = [
  { "ABBR_MONTH": "01", "FULL_MONTH": "มกราคม" },
  { "ABBR_MONTH": "02", "FULL_MONTH": "กุมภาพันธ์" },
  { "ABBR_MONTH": "03", "FULL_MONTH": "มีนาคม" },
  { "ABBR_MONTH": "04", "FULL_MONTH": "เมษายน" },
  { "ABBR_MONTH": "05", "FULL_MONTH": "พฤษภาคม" },
  { "ABBR_MONTH": "06", "FULL_MONTH": "มิถุนายน" },
  { "ABBR_MONTH": "07", "FULL_MONTH": "กรกฎาคม" },
  { "ABBR_MONTH": "08", "FULL_MONTH": "สิงหาคม" },
  { "ABBR_MONTH": "09", "FULL_MONTH": "กันยายน" },
  { "ABBR_MONTH": "10", "FULL_MONTH": "ตุลาคม" },
  { "ABBR_MONTH": "11", "FULL_MONTH": "พฤศจิกายน" },
  { "ABBR_MONTH": "12", "FULL_MONTH": "ธันวาคม" },
]
var columnAllTypeName = ["โฉนดที่ดิน", "โฉนดตราจอง", "ตราจอง", "น.ส.3", "น.ส.3 ก.", "น.ส.ล.", "หนังสืออนุญาต", "อาคารชุด", "ห้องชุด"]
var columnLAND2012TypeName = ["โฉนดที่ดิน", "โฉนดตราจอง", "ตราจองที่ได้ทำประโยชน์แล้ว", "น.ส.3", "น.ส.3 ก.", "น.ส.ล.", "หนังสืออนุญาต", "อาคารชุด", "ห้องชุด"]

function changeToFullMonth(month) {
  return months.filter(monthData => monthData.ABBR_MONTH == month)[0].FULL_MONTH
}

function fullname(title,fname,mname,lname){
  title = title===undefined? '': title;
  fname = fname===undefined? '': fname;
  mname = mname===undefined? '': ' '+mname;
  lname = lname===undefined? '': ' '+lname;
  fullame = ""
  if(title.includes('...')){
    fullame = title.replace('...',fname) + mname + lname;
  } else {
    fullame = title + fname + mname + lname
  }
  return fullame;
}

function convertDtm(dtm){
  str = dtm.split(' ');
  day = str[0];
  mon = str[1];
  year = str[str.length-1]<2400? parseInt(str[str.length-1])+543 : str(str[str.length-1])

  return day + ' ' + mon + ' ' + year;
}

function changeMM_DD_YYYY_to_DD_MM_YYYY(date) {
  var split_date = date.split("/");
  if (parseInt(split_date[1]) > 12) {
    var temp = split_date[0];
    split_date[0] = split_date[1];
    split_date[1] = temp;
  }
  return split_date;
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie() {
  var c = getCookie("landoffice");
  if (c != "") {
    location.href = './portal.php'
  }

}

function clearCookie() {
  document.cookie = "landoffice=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  document.cookie = "branchName=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";

  location.reload()
  location.href = './index.html'
}