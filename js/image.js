$(document).ready(function () {
    //redirect to login page when bypass by URL
    if (getCookie("landoffice") == "") {
        location.href = "./index.html";
    }

    $('.sidebar-menu').click(function () {
        $('ul a.nav-link').removeClass('active');
        $(this).parent().children('a').addClass('active');
    });

    $('.tab-pane').hide();
    $('.overlay-main').hide();
    $('.spinner-border').hide();
    $('div[id^="input-image-"').hide();
    $('#input-image-no').show();
    $('#input-image-survey').show();

    $.fn.dataTable.ext.errMode = 'none';

    // branchName = 'สำนักงานที่ดินกรุงเทพมหานคร';
    // landoffice = 241;
    branchName = getCookie('branchName');
    landoffice = getCookie('landoffice');

    document.getElementById("landoffice_name").innerHTML = '<i class="far fa-file-image"></i>   ระบบตรวจสอบข้อมูลภาพลักษณ์เอกสารสิทธิ/สารบบ<br>[' + branchName + ']';

    $('select[id^="select-printplateType-"').empty();
    optionPt = ""
    printplateType.forEach(type => {
        optionPt += '<option value="' + type.seq + '">' + type.name + '</option>';
    })
    $('select[id^="select-printplateType-"').append(optionPt);

    getAmphur(landoffice);
    $('select[id^="select-tambol-"').empty()
    optionTb = '<option value="">เลือกตำบล</option>';
    $('select[id^="select-tambol-"').append(optionTb);
    $('select[id^="select-tambol-"').prop('disabled', 'disabled');
    // console.log(ap);

    $("#table-summary").DataTable({
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs" : [
            {
                "searchable" : false,
                "orderable" : false,
                "targets" : 0
            },
            {"className": "dt-center", "targets": "_all"}
        ],
        "processing" : true,
        "lengthChange": false,
        "ordering" : false,
        "searching" : false,
        "paging" : false,
        "info" : false,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    });

    getSummary(landoffice);

    $('.nav-pills-main a').on('show.bs.tab', function () {
        $("#search-" + type).off();
        $(current_href).hide();
        var href = $(this).attr('href');
        current_href = href;
        $(current_href).show();
        type= href.split('tab-')[1];

        var table = $("#table-main-" + type).DataTable({
            "language": {
                "emptyTable": "ไม่มีข้อมูล"
            },
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable" : false,
                    "targets" : 0
                },
                {"className": "dt-center", "targets": "_all"}
            ],
            "pagingType" : "simple_numbers",
            "pageLength" : 10,
            "processing" : true,
            "lengthChange": false,
            "ordering" : false,
            "searching" : false,
            "info" : true,
            "drawCallback": function () {
                currentPageCount = this.api().rows({
                    page: 'current'
                }).data().length;
            }
        });

        table.columns([3,5,7,8,9,10,11,12]).visible(false);

        var tableDet = $("#table-detail-" + type).DataTable({
            "language": {
                "emptyTable": "ไม่มีข้อมูล"
            },
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable" : false,
                    "targets" : 0
                },
                {"className": "dt-center", "targets": "_all"}
            ],
            "pagingType" : "simple_numbers",
            "pageLength" : 10,
            "processing" : true,
            "lengthChange": false,
            "ordering" : false,
            "searching" : false,
            "info" : true,
            "drawCallback": function () {
                currentPageCount = this.api().rows({
                    page: 'current'
                }).data().length;
            }
        });

        var tableDet = $("#table-detail2-" + type).DataTable({
            "language": {
                "emptyTable": "ไม่มีข้อมูล"
            },
            "columnDefs" : [
                {
                    "searchable" : false,
                    "orderable" : false,
                    "targets" : 0
                },
                {"className": "dt-center", "targets": "_all"}
            ],
            "pagingType" : "simple_numbers",
            "pageLength" : 10,
            "processing" : true,
            "lengthChange": false,
            "ordering" : false,
            "searching" : false,
            "info" : true,
            "drawCallback": function () {
                currentPageCount = this.api().rows({
                    page: 'current'
                }).data().length;
            }
        });

        $('#select-printplateType-' + type).on('change', function(){
            p = this.value;
            clearFunction ()
            if(p==10 || p==13) { //condoroom - condo
                $('div[id^="input-image-"').hide();
                $('#input-image-condoReg').show();
                $('#input-image-condoName').show();
                if(p==10) $('#input-image-condoroom').show();
            } else if(p==11) { //constr
                $('div[id^="input-image-"').hide();
                $('#input-image-hno').show();
                $('#input-image-hid').show();
                $('#input-image-moo').show();
            } else {
                $('div[id^="input-image-"').hide();
                $('#input-image-no').show();
                if(p>4 && p!=8 && p!=17) $('#input-image-moo').show();
                if(p==23) $('#input-image-nsl').show(); //sub_nsl
                if(p==9) $('#input-image-park').show(); //park
                if(p==8) $('#input-image-year').show();
                if(p<4 || p==17) $('#input-image-survey').show();
            }
        })

        $('#select-amphur-' + type).on('change', function(){
            amphur = this.value;
            if(amphur!=''){
                // $('#select-tambol-' + type).empty()
                getTambol(amphur);
                $('#select-tambol-' + type).prop('disabled', false);
            } else {
                $('#select-tambol-' + type).empty()
                optionTb = '<option value="">เลือกตำบล</option>';
                $('#select-tambol-' + type).append(optionTb);
                $('#select-tambol-' + type).prop('disabled', 'disabled');
                tambon = '';
            }
        })

        $('#search-' + type).on("click", function(){
            $(this).toggleClass("active");
            searchFunction();
        }); 
        $('#clear-' + type).on("click", function(){
            $(this).toggleClass("active");
            clearFunction();
        });   
        
        $('#table-detail-' + type + ' tbody').on('click', 'tr', function () {
            let table = $('#table-detail-' + type).DataTable();
            table.$('tr.row-click').removeClass('row-click');
            $(this).addClass('row-click');

            let table2 = $('#table-detail2-' + type).DataTable();
            table2.$('tr.row-click').removeClass('row-click');
        } );

        $('#table-detail2-' + type + ' tbody').on('click', 'tr', function () {
            let table = $('#table-detail2-' + type).DataTable();
            table.$('tr.row-click').removeClass('row-click');
            $(this).addClass('row-click');

            let table2 = $('#table-detail-' + type).DataTable();
            table2.$('tr.row-click').removeClass('row-click');
        } );

        $('#table-main-' + type + ' tbody').on('click', 'tr', function () {
            let table = $('#table-main-' + type).DataTable();
            table.$('tr.row-click').removeClass('row-click');
            $(this).addClass('row-click');
        } );

    });
    
    $('#report-s-image').on("click", function(){
        let url = '';
        url += '&type=s'
        url += '&landoffice=' + landoffice;
        url += '&branchName=' + branchName;
        url += '&printplateType=' + $('#select-printplateType-'+type+" :selected").val();
        url += '&round=0&max=-1';
        window.open('./queries/exportImage2.php?' + url, true);
    })
    $('#report-e-image').on("click", function(){
        let url = '';
        url += '&type=err'
        url += '&landoffice=' + landoffice;
        url += '&branchName=' + branchName;
        url += '&printplateType=' + $('#select-printplateType-'+type+" :selected").val();
        url += '&round=0&max=-1';
        window.open('./queries/exportImage2.php?' + url, true);
    })
    $('#report-s-index').on("click", function(){
        let url = '';
        url += '&type=s'
        url += '&landoffice=' + landoffice;
        url += '&branchName=' + branchName;
        url += '&printplateType=' + $('#select-printplateType-'+type+" :selected").val();
        url += '&round=0&max=-1';
        window.open('./queries/exportIndex2.php?' + url, true);
    })
    $('#report-e-index').on("click", function(){
        let url = '';
        url += '&type=err'
        url += '&landoffice=' + landoffice;
        url += '&branchName=' + branchName;
        url += '&printplateType=' + $('#select-printplateType-'+type+" :selected").val();
        url += '&round=0&max=-1';
        window.open('./queries/exportIndex2.php?' + url, true);
    })

});

function searchFunction(){
    p = $('#select-printplateType-' + type).val();
    tambon = $('#select-tambol-' + type).val();
    // console.log(pt);
    data = {
        "landoffice" : landoffice,
        "printplateType" : p,
        "amphur" : amphur,
        "tambol" : tambon
    }
    if(p<4 || p==17){ //parcel
        p==1? data['parcelNo'] = $('#input-image-no > input').val() : data['parcelLandNo'] = $('#input-image-no > input').val()
        data['survey'] = $('#input-image-survey > input').val()
    }  else if(p==10 || p==13) { //condoroom - condo
        data['condoReg'] = $('#input-image-condoReg > input').val()
        data['condoName'] = $('#input-image-condoName > input').val()
        if(p==10) {
            data['condoroom'] = $('#input-image-condoroom > input').val()
        }
    } else if(p==11) { //constr
        data['constrHno'] = $('#input-image-hno > input').val()
        data['constrHid'] = $('#input-image-hid > input').val()
        data['constrMoo'] = $('#input-image-moo > input').val()
    } else {
        data['parcelLandNo'] = $('#input-image-no > input').val()
        if(p!=4 && p!=8) {
           data['parcelLandMoo'] = $('#input-image-moo > input').val() 
        }
        if(p==23) {
            data['nslNo'] = $('#input-image-nsl > input').val() //sub_nsl
        } else if(p==9) {
            data['parcelLandName'] = $('#input-image-park > input').val()
        } else if(p==8){
            data['obtainYear'] = $('#input-image-year > input').val()
        } 
    }
    
    // console.log(data);
    getDataImage(data);

}

function getAmphur(landoffice){
    var amphurArr = [];
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getAmphur2.php',
        data: "landoffice=" + landoffice,
        success: function (data) {
            if(!jQuery.isEmptyObject(data)){
                num = 0;
                $.each(data, function (key, val){
                    if(num > 0 && amphurArr[num-1][1]==val['AMPHUR_NAME']){
                        num--;
                        amphurArr[num][0] += ','+val['AMPHUR_SEQ'];
                    } else {
                        amphurArr.push([val['AMPHUR_SEQ'],val['AMPHUR_NAME']]);
                    }
                    num++;
                })
                $('select[id^="select-amphur-"').empty();
                optionAp = "";
                optionAp = '<option value="">เลือกอำเภอทั้งหมด</option>';
                amphurArr.forEach(element => {
                    optionAp += '<option value="' + element[0] + '">' + element[1] + '</option>';
                })
                $('select[id^="select-amphur-"').append(optionAp);
                // console.log(amphurArr)
            } else {
                $('select[id^="select-amphur-"').empty();
                optionAp = '<option value="">ไม่สามารถเลือกอำเภอได้</option>';
                $('select[id^="select-amphur-"').append(optionAp);
                $('select[id^="select-amphur-"').prop('disabled', 'disabled');
            }
        }
    })
}

function getTambol(amphur){
    var tambolArr = []
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getTambol2.php',
        data: 'amphur=' + amphur,
        success: function(data){
            // console.log(data);
            if(!jQuery.isEmptyObject(data)){
                num = 0;
                $.each(data, function (key, val){
                    if(num > 0 && tambolArr[num-1][1]==val['TAMBOL_NAME']){
                        num--;
                        tambolArr[num][0] += ','+val['TAMBOL_SEQ'];
                    } else {
                        tambolArr.push([val['TAMBOL_SEQ'],val['TAMBOL_NAME']]);
                    }
                    num++;
                })
                $('#select-tambol-' + type).empty();
                optionTb = "";
                optionTb = '<option value="">เลือกตำบลทั้งหมด</option>';
                tambolArr.forEach(element => {
                    optionTb += '<option value="' + element[0] + '">' + element[1] + '</option>';
                })
                $('#select-tambol-' + type).append(optionTb);
            } else {
                $('#select-tambol-' + type).empty();
                optionTb = '<option value="">เลือกตำบล</option>';
                $('#select-tambol-' + type).append(optionTb);
                $('#select-tambol-' + type).prop('disabled', 'disabled');
            }
        }
    })
}

function clearFunction (){
    $('#input-image-no > input').val('');
    $('#input-image-survey > input').val('');
    $('#input-image-hid > input').val('');
    $('#input-image-hno > input').val('');
    $('#input-image-year > input').val('');
    $('#input-image-moo > input').val('');
    $('#input-image-condoReg > input').val('');
    $('#input-image-condoName > input').val('');
    $('#input-image-condoroom > input').val('');
    $('#input-image-nsl > input').val('');
    $('#input-image-park > input').val('');
    $('#select-amphur-' + type).val('').change();
    $('#select-tambol-' + type).val('').change();
}

function getDataImage(d){
    $('#main.overlay-main').show();
    $('#detail.overlay-main').show();
    $("#table-detail-" + type).DataTable().clear().draw();
    $("#table-detail2-" + type).DataTable().clear().draw();
    $('#image-image').attr('src', "./img/image-dol.jpg");
    $('#detail.overlay-main').hide();
    var table = $("#table-main-" + type).DataTable();
    table.columns([0,1,2,3,4,5,6,7,8,9,10,11,12,13]).visible(true);
    $.ajax({
        type : "POST",
        dataType : "json",
        url : "./queries/getImage.php",
        data : d,
        success: function(data){
            // console.log(d);
            pt = d.printplateType;
            datatemp = data;
            if(!jQuery.isEmptyObject(data)){
                table.search("").draw();
                table.clear();
                num = 1;
                // console.log(pt)
                if(pt==1){  
                    table.columns([3,5,7,8,9,10,11,12]).visible(false);
                    datatemp.forEach(d => {
                        // console.log(d)
                        temp = [];
                        temp[0] = num;
                        temp[1] = d['AMPHUR_NAME']===undefined? "" : d['AMPHUR_NAME'];
                        temp[2] = d['TAMBOL_NAME']===undefined? "" : d['TAMBOL_NAME'];
                        temp[4] = d['NO']===undefined? "" : d['NO'];
                        temp[6] = d['SURVEY']===undefined? "" : d['SURVEY'];
                        seq1 = d['PARCEL_SEQ']===undefined? "0" : d['PARCEL_SEQ'];
                        seq2 = d['PARCEL_SEQ_1']===undefined? "0" : d['PARCEL_SEQ_1'];
                        temp[13] = '<a onClick="getDetail('+seq1+','+seq2+','+pt+')"><i  class="fa fa-info-circle fa-lg" style="color:#2ca7d6;"></i></a>';
                            // console.log("44");
                        // temp = [0,1,2,3,4,5,6,7,8,9,10,11,12]
                        table.row.add(temp);
                        num++;
                    })
                }  else if(pt==10){
                    table.columns([3,5,6,7,8,9,12]).visible(false);
                    datatemp.forEach(d => {
                        // console.log(d)
                        temp = [];
                        temp[0] = num;
                        temp[1] = d['AMPHUR_NAME']===undefined? "" : d['AMPHUR_NAME'];
                        temp[2] = d['TAMBOL_NAME']===undefined? "" : d['TAMBOL_NAME'];
                        temp[4] = d['NO']===undefined? "" : d['NO'];
                        temp[10] = d['CONDO_REG']===undefined? "" : d['CONDO_REG'];
                        temp[11] = d['CONDO_NAME']===undefined? "" : d['CONDO_NAME'];
                        seq1 = d['CONDOROOM_SEQ']===undefined? "0" : d['CONDOROOM_SEQ'];
                        seq2 = d['CONDOROOM_SEQ_1']===undefined? "0" : d['CONDOROOM_SEQ_1'];
                        temp[13] = '<a onClick="getDetail('+seq1+','+seq2+','+pt+')"><i  class="fa fa-info-circle fa-lg" style="color:#2ca7d6;"></i></a>';
                            // console.log("44");
                        // temp = [0,1,2,3,4,5,6,7,8,9,10,11]
                        table.row.add(temp);
                        num++;
                    })
                } else if(pt==11) {
                    table.columns([6,7,8,9,10,11]).visible(false);
                } else if(pt==13) {
                    table.columns([3,5,6,7,8,9,10,12]).visible(false);
                    datatemp.forEach(d => {
                        temp = [];
                        temp[1] = d['AMPHUR_NAME']===undefined? "" : d['AMPHUR_NAME'];
                        temp[2] = d['TAMBOL_NAME']===undefined? "" : d['TAMBOL_NAME'];
                        temp[4] = d['CONDO_REG']===undefined? "" : d['CONDO_REG'];
                        temp[11] = d['CONDO_NAME']===undefined? "" : d['CONDO_NAME'];
                        seq1 = d['CONDO_SEQ']===undefined? "0" : d['CONDO_SEQ'];
                        seq2 = d['CONDO_SEQ_1']===undefined? "0" : d['CONDO_SEQ_1'];
                        temp[13] = '<a onClick="getDetail('+seq1+','+seq2+','+pt+')"><i  class="fa fa-info-circle fa-lg" style="color:#2ca7d6;"></i></a>';
                        table.row.add(temp);
                        num++;
                    })
                } else {
                    table.columns([3,5,6,7,8,9,10,11,12]).visible(false);
                    if(p>4 && p!=8 && p!=17) table.columns([3]).visible(true);
                    if(p==23) table.columns([7]).visible(true);
                    if(p==8) table.columns([8]).visible(true);
                    if(p==8) table.columns([9]).visible(true);
                    datatemp.forEach(d => {
                        // console.log(d)
                        temp = [];
                        temp[0] = num;
                        temp[1] = d['AMPHUR_NAME']===undefined? "" : d['AMPHUR_NAME'];
                        temp[2] = d['TAMBOL_NAME']===undefined? "" : d['TAMBOL_NAME'];
                        temp[3] = d['MOO']===undefined? "" : d['MOO'];
                        temp[4] = d['NO']===undefined? "" : d['NO'];
                        temp[6] = d['SURVEY']===undefined? "" : d['SURVEY'];
                        temp[7] = d['NSL_NO']===undefined? "" : d['NSL_NO']
                        temp[8] = d['YEAR']===undefined? "" : d['YEAR'];
                        temp[9] = d['NAME']===undefined? "" : d['NAME'];
                        seq1 = d['PARCEL_LAND_SEQ']===undefined? "0" : d['PARCEL_LAND_SEQ'];
                        seq2 = d['PARCEL_LAND_SEQ_1']===undefined? "0" : d['PARCEL_LAND_SEQ_1'];
                        temp[13] = '<a onClick="getDetail('+seq1+','+seq2+','+pt+')"><i  class="fa fa-info-circle fa-lg" style="color:#2ca7d6;"></i></a>';
                            // console.log("44");
                        // temp = [0,1,2,3,4,5,6,7,8,9,10,11]
                        table.row.add(temp);
                        num++;
                    })
                }
            } else {
                if(pt==1){  
                    table.columns([3,5,7,8,9,10,11,12]).visible(false);
                }  else if(pt==10){
                    table.columns([3,5,6,7,8,9,12]).visible(false);
                } else if(pt==11) {
                    table.columns([6,7,8,9,10,11]).visible(false);
                } else if(pt==13) {
                    table.columns([3,5,6,7,8,9,10,12]).visible(false);
                } else {
                    table.columns([3,5,6,7,8,9,10,11,12]).visible(false);
                    if(p>4 && p!=8 && p!=17) table.columns([3]).visible(true);
                    if(p==23) table.columns([7]).visible(true);
                    if(p==8) table.columns([8]).visible(true);
                }
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#main.overlay-main').hide();
        }
    });
}


function getDetail(seq1,seq2,pt){
    $('#detail.overlay-main').show();
    $('#image-image').attr('src', "./img/image-dol.jpg");
    d = {
        "seq1" : seq1,
        "seq2" : seq2,
        "printplateType" : pt
    }
    getDetailImage(d)
    getDetailIndex(d)
}

function getSummary(landoffice){
    var table = $("#table-summary").DataTable();
    $('#summary.overlay-main').show();
    $.ajax({
        type : "POST",
        dataType : "json",
        url : "./queries/getImageSum.php",
        data : "landoffice="+landoffice,
        success: function(data){
            datatemp = data;
            if(!jQuery.isEmptyObject(data)){
                table.search("").draw();
                table.clear();
                num = 1;
                datatemp.forEach(d => {
                    // console.log(d)
                    temp = [];
                    temp[0] = num;
                    temp[1] = d['PRINTPLATE_TYPE_NAME']===undefined? "" : d['PRINTPLATE_TYPE_NAME'];
                    temp[2] = d['RECEIVE']===undefined? "" : Number(d['RECEIVE']).toLocaleString();
                    temp[3] = d['MIGRATE_SUCCESS']===undefined? "" : Number(d['MIGRATE_SUCCESS']).toLocaleString();
                    temp[4] = d['MIGRATE_ERROR']===undefined? "" : Number(d['MIGRATE_ERROR']).toLocaleString();
                    temp[5] = d['RECEIVE']===undefined? "" : Number(d['RECEIVE2']).toLocaleString();
                    temp[6] = d['MIGRATE_SUCCESS']===undefined? "" : Number(d['MIGRATE_SUCCESS2']).toLocaleString();
                    temp[7] = d['MIGRATE_ERROR']===undefined? "" : Number(d['MIGRATE_ERROR2']).toLocaleString();
                    table.row.add(temp);
                    num++;
                })
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#summary.overlay-main').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getImg1(e){
    var tr = e.parent().closest('tr');
    var row = $("#table-detail-" + type).DataTable().row(tr);
    $('#loading-image.spinner-border').show();
    // console.log(row.data());
    data = {
        "plateType" : row.data()[8],
        "readOnly" : 2,
        "indexSeq" : "",
        "action" : "evd",
        "systemType" : 1,
        "landofficeSeq" : landoffice,
        "image" : row.data()['4']+'/'+row.data()['5']
    }
    $.ajax({
        url: "http://ilands.dol.go.th/evdservice/servlet/EvdImageUnCompressServlet",
        data: data,
        type: 'GET',
        xhrFields: {
            responseType: 'blob'
        },
        success (data) {
            const url = window.URL || window.webkitURL;
            const src = url.createObjectURL(data);
            $('#image-image').attr('src', src);
            $('#loading-image.spinner-border').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getImg2(e){
    var tr = e.parent().closest('tr');
    var row = $("#table-detail-" + type).DataTable().row(tr);
    $('#loading-image.spinner-border').show();
    // console.log(row.data());
    data = {
        "plateType" : row.data()[8],
        "readOnly" : 2,
        "indexSeq" : "",
        "action" : "evd",
        "systemType" : 1,
        "landofficeSeq" : landoffice,
        "image" : row.data()['6']+'/'+row.data()['7']
    }
    $.ajax({
        url: "http://ilands.dol.go.th/evdservice/servlet/EvdImageUnCompressServlet",
        data: data,
        type: 'GET',
        xhrFields: {
            responseType: 'blob'
        },
        success (data) {
            const url = window.URL || window.webkitURL;
            const src = url.createObjectURL(data);
            $('#image-image').attr('src', src);
            $('#loading-image.spinner-border').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    // console.log(data)
}

function getDetailImage(d){
    var table = $("#table-detail-" + type).DataTable();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "./queries/getImageDetail.php",
        data : d,
        success: function(data){
            if(!jQuery.isEmptyObject(data)){
                // console.log(pt)
                datatemp = data
                table.search("").draw();
                table.clear();
                num = 1;
                datatemp.forEach(d => {
                    // console.log(d)
                    temp = [];
                    temp[0] = num;
                    temp[1] = d['NAME']===undefined? "" : d['NAME'];

                    temp[4] = d['URL']===undefined? "" : d['URL'];
                    temp[5] = d['FILENAME']===undefined? "" : d['FILENAME'];
                    temp[6] = d['URL_1']===undefined? "" : d['URL_1'];
                    temp[7] = d['FILENAME_1']===undefined? "" : d['FILENAME_1'];
                    temp[8] = pt;

                    temp[2] = d['FILENAME']===undefined? '<i class="fas fa-times fa-lg" style="color:#ff0000;"></i>' : '<a onClick="getImg1($(this))"><i class="far fa-file-image fa-lg" style="color:#28a745;"></i></a>';
                    temp[3] = d['FILENAME_1']===undefined? '<i class="fas fa-times fa-lg" style="color:#ff0000;"></i>' : '<a onClick="getImg2($(this))"><i class="far fa-file-image fa-lg" style="color:#28a745;"></i></a>';
                    table.row.add(temp);
                    num++;
                })
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#detail.overlay-main').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getDetailIndex(d){
    var table = $("#table-detail2-" + type).DataTable();
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "./queries/getIndexDetail.php",
        data : d,
        success: function(data){
            if(!jQuery.isEmptyObject(data)){
                // console.log(pt)
                datatemp = data
                datatemp2 = data
                table.search("").draw();
                table.clear();
                num = 0;
                // datatemp.slice().reverse().filter((v,i,a)=>a.findIndex(t=>(t['FILENAME'] === v['FILENAME'] && t['URL']===v['URL']))===i).reverse()
                // console.log(datatemp)
                datatemp.forEach(d => {
                    // console.log(d)
                    let t = datatemp.filter(dt => 
                        dt['URL'] == d['URL'] && dt['FILENAME'] == d['FILENAME'] && dt['DOC_NAME'] == d['DOC_NAME'] && dt['REGIST'] == d['REGIST'] && dt['DTM']==d['DTM']
                        );
                    // console.log(t.length);
                    if(t.length > 1){
                        datatemp2.splice(num,1)
                        num--;
                    }
                    num++
                })
                // console.log(datatemp2);
                num = 1;
                datatemp2.forEach(d => {
                    temp = [];
                    temp[0] = num;
                    temp[1] = d['DTM']===undefined? "" : convertDtm(d['DTM']);
                    temp[2] = d['ORD_INX']===undefined? "" : d['ORD_INX'];
                    temp[3] = d['REGIST']===undefined? "" : d['REGIST'];
                    temp[4] = d['DOC_NAME']===undefined? "" : d['DOC_NAME'];
                    let t = datatemp.filter(dt => dt['DTM'] == d['DTM'] && dt['REGIST'] == d['REGIST'] && dt['ORD_INX'] == d['ORD_INX'] && dt['DOC_NAME'] == d['DOC_NAME']);
                    temp[4] += " (" + d['CPAGE'] + "/" + t.length + ")";

                    temp[7] = d['URL']===undefined? "" : d['URL'];
                    temp[8] = d['FILENAME']===undefined? "" : d['FILENAME'];
                    temp[9] = d['URL_1']===undefined? "" : d['URL_1'];
                    temp[10] = d['FILENAME_1']===undefined? "" : d['FILENAME_1'];
                    temp[11] = pt;

                    temp[5] = d['FILENAME']===undefined? '<span><i class="fas fa-times fa-lg" style="color:#ff0000;"></i></span>' : '<a onClick="getInx1($(this))"><i class="far fa-file-image fa-lg" style="color:#28a745;"></i></a>';
                    temp[6] = d['FILENAME_1']===undefined? '<span><i class="fas fa-times fa-lg" style="color:#ff0000;"></i></span>' : '<a onClick="getInx2($(this))"><i class="far fa-file-image fa-lg" style="color:#28a745;"></i></a>';
                    table.row.add(temp);
                    num++;
                })
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#detail.overlay-main').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getInx1(e){
    var tr = e.parent().closest('tr');
    var row = $("#table-detail2-" + type).DataTable().row(tr);
    $('#loading-image.spinner-border').show();
    // console.log(row.data());
    data = {
        "plateType" : row.data()[11],
        "readOnly" : 2,
        "indexSeq" : "",
        "action" : "evd",
        "systemType" : 1,
        "landofficeSeq" : landoffice,
        "image" : row.data()['7']+'/'+row.data()['8']
    }
    $.ajax({
        url: "http://ilands.dol.go.th/evdservice/servlet/EvdImageUnCompressServlet",
        data: data,
        type: 'GET',
        xhrFields: {
            responseType: 'blob'
        },
        success (data) {
            const url = window.URL || window.webkitURL;
            const src = url.createObjectURL(data);
            $('#image-image').attr('src', src);
            $('#loading-image.spinner-border').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    // console.log(data)
}

function getInx2(e){
    var tr = e.parent().closest('tr');
    var row = $("#table-detail2-" + type).DataTable().row(tr);
    $('#loading-image.spinner-border').show();
    // console.log(row.data());
    data = {
        "plateType" : row.data()[11],
        "readOnly" : 2,
        "indexSeq" : "",
        "action" : "evd",
        "systemType" : 1,
        "landofficeSeq" : landoffice,
        "image" : row.data()['9']+'/'+row.data()['10']
    }
    $.ajax({
        url: "http://ilands.dol.go.th/evdservice/servlet/EvdImageUnCompressServlet",
        data: data,
        type: 'GET',
        xhrFields: {
            responseType: 'blob'
        },
        success (data) {
            const url = window.URL || window.webkitURL;
            const src = url.createObjectURL(data);
            $('#image-image').attr('src', src);
            $('#loading-image.spinner-border').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    // console.log(data)
}