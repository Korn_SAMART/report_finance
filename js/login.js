const signInButton = document.getElementById('signIn');
const EnterButton = document.getElementById('Enter');
const container = document.getElementById('container');

$(document).ready(function () {
    checkCookie();
    $("#signIn").on("click", function (e) {
        user = $('#userName').val();
        pass = $('#password').val();
        e.preventDefault();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: './queries/checkLogin.php',
            data: 'user=' + user + '&pass=' + pass,
            success: function (data) {
                if (data[0].STATUS == 'TRUE') {
                    console.log(data[0].USER_LOGIN);
                    if (data[0].USER_LOGIN == 'Admin') {
                        container.classList.add("right-panel-active");
                        getProvince();
                    }
                    else {
                        // container.classList.remove("right-panel-active");
                        document.cookie = "user=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                        setCookie('user', data[0].USER_LOGIN);
                        document.cookie = "landoffice=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                        setCookie('landoffice', data[0].USER_ORG);
                        document.cookie = "branchName=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                        setCookie('branchName', data[0].USER_ORG_NAME)
                        location.href = './portal.php';
                    }
                } else {
                    $('.login-div > label').css("display", "block");
                }
            }
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    })
})

function enter() {
    landoffice = $('#select-branch').val().split(',')[0];
    console.log(landoffice);
    document.cookie = "landoffice=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    setCookie('landoffice', landoffice);
    document.cookie = "branchName=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    setCookie('branchName', $('#select-branch option:selected').text());

    if (landoffice != '') {
        location.href = './portal.php';
    }
    else {
        $('#require_modal').modal();
    }

}
