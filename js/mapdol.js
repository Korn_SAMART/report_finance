var clicked = false;
$(document).ready(function () {
    //redirect to login page when bypass by URL
    if (getCookie("landoffice") == "") {
        location.href = "./index.html";
    }

    $('.sidebar-menu').click(function () {
        $('ul a.nav-link').removeClass('active');
        $(this).parent().children('a').addClass('active');
    });

    $('.tab-pane').hide();
    $('div[id^=rv-ns3k]').hide();
    $('.overlay-main').hide();
    $('div[id^="div-parcelType"').hide();

    $('select[id^="utmscale-"]').empty();
    optionScale = ""
    optionScale = '<option value=""></option>'
    utmScale.forEach(scale=>{
        optionScale += '<option value="' + scale.value + '">' + scale.value + '</option>'
    })
    $('select[id^="utmscale-"]').append(optionScale);

    $('select[id^="select-parcelType-mapdol"]').empty();
    optionParcelType = ""
    parcelType.forEach(type=>{
        optionParcelType += '<option value="' + type.parcelType + '">' + type.parcelDesc + '</option>'
    })
    $('select[id^="select-parcelType-mapdol"]').append(optionParcelType);

    $('select[id^="select-printplateType-"').empty();
    optionPt = ""
    printplateType.forEach(type => {
        if(type.seq!=10 && type.seq!=11 && type.seq!=13)
            optionPt += '<option value="' + type.seq + '">' + type.name + '</option>';
    })
    optionPt += '<option value="-1">ไม่สามารถแยกประเภทเอกสารได้</option>';
    $('select[id^="select-printplateType-"').append(optionPt);


    $.fn.dataTable.ext.errMode = 'none';

    // branchName = 'สำนักงานที่ดินกรุงเทพมหานคร';
    // landoffice = 241;
    branchName = getCookie('branchName');
    landoffice = getCookie('landoffice');

    document.getElementById("landoffice_name").innerHTML = '<i class="far fa-file-image"></i>   ระบบตรวจสอบข้อมูลรูปแปลงที่ดิน<br>[' + branchName + ']';

    // $("#table-summary").DataTable({
    //     "language": {
    //         "emptyTable": "ไม่มีข้อมูล"
    //     },
    //     "columnDefs" : [
    //         {
    //             "searchable" : false,
    //             "orderable" : false,
    //             "targets" : 0
    //         },
    //         {"className": "dt-center", "targets": "_all"}
    //     ],
    //     "processing" : true,
    //     "lengthChange": false,
    //     "ordering" : false,
    //     "searching" : false,
    //     "paging" : false,
    //     "info" : false,
    //     "drawCallback": function () {
    //         currentPageCount = this.api().rows({
    //             page: 'current'
    //         }).data().length;
    //     }
    // });
    $("#table-summary2").DataTable({
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs" : [
            {
                "searchable" : false,
                "orderable" : false,
                "targets" : 0
            },
            {"className": "dt-center", "targets": "_all"}
        ],
        "processing" : true,
        "lengthChange": false,
        "ordering" : false,
        "searching" : false,
        "paging" : false,
        "info" : false,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    });

    $('#table-summary').hide();
    //getSummary(landoffice);
    getSummary2(landoffice);

    $("#table-mapdol").DataTable({
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs" : [
            {
                "searchable" : false,
                "orderable" : false,
                "targets" : 0
            },
            {"className": "diff-p", "targets": [8]}
        ],
        "pagingType" : "simple_numbers",
        "pageLength" : 25,
        "processing" : true,
        "lengthChange": false,
        "ordering" : false,
        "searching" : false,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    });
    $("#table-mapdol-temp").DataTable({
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs" : [
            {
                "searchable" : false,
                "orderable" : false,
                "targets" : 0
            },
            {"className": "diff-p", "targets": [8]}
        ],
        "pagingType" : "simple_numbers",
        "pageLength" : 25,
        "processing" : true,
        "lengthChange": false,
        "ordering" : false,
        "dom" : 'tip',
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    });

    $('.nav-pills-main a').on('show.bs.tab', function () {
        $("#search-" + type).off();
        $("#notpublic-" + type).off();
        $(current_href).hide();
        var href = $(this).attr('href');
        current_href = href;
        $(current_href).show();
        type= href.split('tab-')[1];

        $('#select-printplateType-' + type).on('change', function(){
            // $('#rvType-' + type).val('1').change();
            clearFunction(0);
        })
        $('#select-parcelType-' + type).on('change', function(){
            // $('#rvType-' + type).val('1').change();
            clearFunction(0);
        })

        $('#rvType-' + type).on('change', function(){
            if(this.value == 1){
                $('#rv-utm-' + type).show();
                $('#rv-ns3k-' + type).hide();
                clearFunction(1);
            } else if (this.value == 2){
                $('#rv-utm-' + type).hide();
                $('#rv-ns3k-' + type).show();
                clearFunction(2);
            } else {
                $('#rv-utm-' + type).hide();
                $('#rv-ns3k-' + type).hide();
                clearFunction(0);
            }
        })

        
        // table.columns([4]).visible(false);

        $('#search-' + type).on("click", function(){
            $(this).toggleClass("active");
            searchFunction();
        }); 
        $('#clear-' + type).on("click", function(){
            $(this).toggleClass("active");
            clearFunction(0);
        });

    });

    $('#report-s-mapdol').on("click", function(){
        let url = '';
        url += 'temp=0'
        url += '&type=s'
        url += '&landoffice=' + landoffice;
        url += '&branchName=' + branchName;
        url += '&printplateType=' + $('#select-printplateType-'+type+" :selected").val();
        window.open('./queries/exportMapdol2.php?' + url, true);
    })
    $('#report-e-mapdol').on("click", function(){
        let url = '';
        url += 'temp=0'
        url += '&type=err'
        url += '&landoffice=' + landoffice;
        url += '&branchName=' + branchName;
        url += '&printplateType=' + $('#select-printplateType-'+type+" :selected").val();
        window.open('./queries/exportMapdol2.php?' + url, true);
    })
    $('#report-s-mapdol-temp').on("click", function(){
        let url = '';
        url += 'temp=1'
        url += '&type=s'
        url += '&landoffice=' + landoffice;
        url += '&branchName=' + branchName;
        url += '&printplateType=' + $('#select-printplateType-'+type+" :selected").val();
        window.open('./queries/exportMapdol2.php?' + url, true);
    })
    $('#report-e-mapdol-temp').on("click", function(){
        let url = '';
        url += 'temp=1'
        url += '&type=err'
        url += '&landoffice=' + landoffice;
        url += '&branchName=' + branchName;
        url += '&printplateType=' + $('#select-printplateType-'+type+" :selected").val();
        window.open('./queries/exportMapdol2.php?' + url, true);
    })
});

function searchFunction(){
    // console.log('กด')
    if($('#rvType-' + type).val() == 1){
        data = {
            "landoffice" : landoffice,
            "parcelType" : $("#select-parcelType-"+type+" :selected").val(),
            "printplateType" : $('#select-printplateType-'+type+" :selected").val(),
            "utmScale" : $("#utmscale-"+type+" :selected").val(),
            "utmmap1" : $("#utmmap1-"+type).val(),
            "utmmap2" : $("#utmmap2-"+type+" :selected").val(),
            "utmmap3" : $("#utmmap3-"+type).val(),
            "utmmap4" : $("#utmmap4-"+type).val(),
            "landno" : $("#landno-"+type).val(),
            "rvType" : $('#rvType-' + type).val(),
            "type" : type,
            "zone" : $('#zone-'+type+" :selected").val()
        }
    } else {
        data = {
            "landoffice" : landoffice,
            "parcelType" : $("#select-parcelType-"+type+" :selected").val(),
            "printplateType" : $('#select-printplateType-'+type+" :selected").val(),
            "utmScale" : $("#utmscale-ns3k-"+type+" :selected").val(),
            "utmmap1" : $("#utmmap1-ns3k-"+type).val(),
            "utmmap2" : $("#utmmap2-ns3k-"+type+" :selected").val(),
            "utmmap3" : "",
            "utmmap4" : $("#utmmap4-ns3k-"+type).val(),
            "landno" : $("#landno-ns3k-"+type).val(),
            "rvType" : $('#rvType-' + type).val(),
            "type" : type,
            "zone" : $('#zone-'+type+" :selected").val()
        }
    }
    // console.log(data);
    getDataMapdol(data);

}

function clearFunction (n){
    // console.log(n)
    if(n==0) {
        $('#utmscale-' + type).val('').change();
        $('#utmmap1-' + type).val('');
        $('#utmmap2-' + type).val('').change();
        $('#utmmap3-' + type).val('');
        $('#utmmap4-' + type).val('');
        $('#landno-' + type).val('');
        $('#utmscale-ns3k-' + type).val('').change();
        $('#utmmap1-ns3k-' + type).val('');
        $("#utmmap2-ns3k-" + type).val('').change();
        $('#utmmap4-ns3k-' + type).val('');
        $('#landno-ns3k-' + type).val('');
    } else if (n==1){
        $('#utmscale-ns3k-' + type).val('').change();
        $('#utmmap1-ns3k-' + type).val('');
        $("#utmmap2-ns3k-" + type).val('').change();
        $('#utmmap4-ns3k-' + type).val('');
        $('#landno-ns3k-' + type).val('');
    } else if (n==2) {
        $('#utmscale-' + type).val('').change();
        $('#utmmap1-' + type).val('');
        $('#utmmap2-' + type).val('').change();
        $('#utmmap3-' + type).val('');
        $('#utmmap4-' + type).val('');
        $('#landno-' + type).val('');
    }
}

function getDataMapdol(d){
    var link = "";
    let ov = ""
    if(d.type == 'mapdol') {
        link = "./queries/getMapdol2.php";
    }
    else {
        link = "./queries/getMapdolTemp.php";
        ov = "-temp"
    } 
    // console.log(link);
    var table = $("#table-" + d.type).DataTable();
    $('#main'+ov+'.overlay-main').show();
    table.columns([4,12]).visible(false);
    table.$('tr.rv.dup').removeClass('rv-dup');
    $.ajax({
        type : "POST",
        dataType : "json",
        url : link,
        data : d,
        success: function(data){
            datatemp = data;
            index = [];
            // console.log(data)
            if(!jQuery.isEmptyObject(data)){
                table.search("").draw();
                table.clear();
                num = 1;
                datatemp.forEach(d => {
                    // console.log(d['PT'])
                    if(d['PT']>4) table.columns([4,12]).visible(true);
                    temp = [];
                    temp[0] = num;
                    temp[1] = d['NO']===undefined? "-" : d['NO'];
                    temp[2] = d['AMPHUR_NAME']===undefined? "-" : d['AMPHUR_NAME'];
                    temp[3] = d['TAMBOL_NAME']===undefined? "-" : d['TAMBOL_NAME'];
                    temp[4] = d['MOO']===undefined? "-" : d['MOO'];
                    temp[5] = d['UTM']===undefined? "" : d['UTM'];
                    temp[6] = d['UTMMAP4']===undefined? "" : d['UTMMAP4'];
                    temp[7] = d['UTMSCALE']===undefined? "" : d['UTMSCALE'] ;
                    temp[8] = d['LAND_NO']===undefined? "" : d['LAND_NO'];
                    temp[9] = (d['NO']===undefined || d['UTM_1']==="  ") ?  "-" : d['NO'];
                    temp[10] = (d['AMPHUR_NAME']===undefined || d['UTM_1']==="  ")? "-" : d['AMPHUR_NAME'];
                    temp[11] = (d['TAMBOL_NAME']===undefined || d['UTM_1']==="  ")? "-" : d['TAMBOL_NAME'];
                    temp[12] = (d['MOO']===undefined? "-" : d['MOO'] || d['UTM_1']==="  ");
                    temp[13] = d['UTM_1']==="  "? "" : d['UTM_1'];
                    temp[14] = d['UTMMAP4_1']===undefined? "" : d['UTMMAP4_1'];
                    temp[15] = d['UTMSCALE_1']===undefined? "" : d['UTMSCALE_1'] ;
                    temp[16] = d['LAND_NO_1']===undefined? "" : d['LAND_NO_1'];

                    sts = '<span style="font-size: 0px">-</span>';
                    if(d['STS']==1) sts = '<a onClick="show($(this))"><i class="fas fa-check-circle fa-lg" style="color:#28a745;"></i></a>';
                    temp[17] = d['STS']===undefined? ' ' : sts;
                    // console.log(sts);
                    if(num > 1){
                        if(d['UTM']==datatemp[num-2]['UTM']
                            && d['UTMMAP4']==datatemp[num-2]['UTMMAP4']
                            && d['LAND_NO']==datatemp[num-2]['LAND_NO']
                            && d['UTMSCALE']==datatemp[num-2]['UTMSCALE']
                            && d['NO']!=datatemp[num-2]['NO']){
                                // console.log(num-2);
                                index.push(num-2);
                                index.push(num-1);
                                // table.rows(num-2).nodes().to$().addClass('rv-dup');
                                // table.rows(num-1).nodes().to$().addClass('rv-dup');

                        }
                    }
                    
                    table.row.add(temp);
                    num++;
                })
            } else {
                table.search("").draw();
                table.clear();
            }
            if(clicked && d.type == "mapdol-temp"){
                $('#notpublic-mapdol-temp').toggleClass('active');
                table.column(17).search("").draw();
                clicked = false
            }
            table.draw();
            for(i=0; i<index.length; i++){
                table.rows(index[i]).nodes().to$().addClass('rv-dup')
                // console.log(index[i]);
            }
            // console.log(table.row(27).data());
            // table.rows(527).nodes().to$().addClass('rv-dup')
            $('#main'+ov+'.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getSummary(landoffice){
    var table = $("#table-summary").DataTable();
    $('#summary.overlay-main').show();
    $.ajax({
        type : "POST",
        dataType : "json",
        url : "./queries/getMapdolSum.php",
        data : "landoffice="+landoffice,
        success: function(data){
            datatemp = data;
            if(!jQuery.isEmptyObject(data)){
                table.search("").draw();
                table.clear();
                num = 1;
                datatemp.forEach(d => {
                    // console.log(d)
                    temp = [];
                    temp[0] = num;
                    temp[1] = d['PARCELTYPE_DESC']===undefined? "" : d['PARCELTYPE_DESC'];
                    temp[2] = d['RECEIVE']===undefined? "" : Number(d['RECEIVE']).toLocaleString();
                    temp[3] = d['MIGRATE_SUCCESS']===undefined? "" : Number(d['MIGRATE_SUCCESS']).toLocaleString();
                    temp[4] = d['MIGRATE_ERROR']===undefined? "" : Number(d['MIGRATE_ERROR']).toLocaleString();
                    temp[5] = d['RECEIVE_TEMP']===undefined? "" : Number(d['RECEIVE_TEMP']).toLocaleString();
                    temp[6] = d['MIGRATE_SUCCESS_TEMP']===undefined? "" : Number(d['MIGRATE_SUCCESS_TEMP']).toLocaleString();
                    temp[7] = d['MIGRATE_ERROR_TEMP']===undefined? "" : Number(d['MIGRATE_ERROR_TEMP']).toLocaleString();
                    table.row.add(temp);
                    num++;
                })
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#summary.overlay-main').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getSummary2(landoffice){
    var table = $("#table-summary2").DataTable();
    $('#summary.overlay-main').show();
    $.ajax({
        type : "POST",
        dataType : "json",
        url : "./queries/getMapdolSum2.php",
        data : "landoffice="+landoffice,
        success: function(data){
            datatemp = data;
            if(!jQuery.isEmptyObject(data)){
                table.search("").draw();
                table.clear();
                num = 1;
                datatemp.forEach(d => {
                    // console.log(d)
                    temp = [];
                    temp[0] = num;
                    temp[1] = d['PARCELTYPE_DESC']===undefined? "" : d['PARCELTYPE_DESC'];
                    temp[2] = d['RECEIVE']===undefined? "" : Number(d['RECEIVE']).toLocaleString();
                    temp[3] = d['MIGRATE_SUCCESS']===undefined? "" : Number(d['MIGRATE_SUCCESS']).toLocaleString();
                    temp[4] = d['MIGRATE_ERROR']===undefined? "" : Number(d['MIGRATE_ERROR']).toLocaleString();
                    temp[5] = d['RECEIVE_TEMP']===undefined? "" : Number(d['RECEIVE_TEMP']).toLocaleString();
                    temp[6] = d['MIGRATE_SUCCESS_TEMP']===undefined? "" : Number(d['MIGRATE_SUCCESS_TEMP']).toLocaleString();
                    temp[7] = d['MIGRATE_ERROR_TEMP']===undefined? "" : Number(d['MIGRATE_ERROR_TEMP']).toLocaleString();
                    table.row.add(temp);
                    num++;
                })
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('#summary.overlay-main').hide();
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function showNotPublic(){
    var table = $('#table-mapdol-temp').DataTable();
    if(clicked){
        $('#notpublic-mapdol-temp').toggleClass('active');
        table.column(17).search("").draw();
        clicked = false
    } else {
        $('#notpublic-mapdol-temp').toggleClass('active');
        table.column(17).search('-').draw();
        clicked = true
    }
}

function show(e){

}