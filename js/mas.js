var utmScale = [
    {"value": 250,
    "seq" : 12},
    {"value": 500,
    "seq" : 1},
    {"value": 1000,
    "seq" : 2},
    {"value": 1250,
    "seq" : 8},
    {"value": 2000,
    "seq" : 3},
    {"value": 2500,
    "seq" : 4},
    {"value": 4000,
    "seq" : 5},
    {"value": 5000,
    "seq" : 7},
    {"value": 6000,
    "seq" : 9},
    {"value": 8000,
    "seq" : 10},
    {"value": 16000,
    "seq" : 11},
    {"value": 50000,
    "seq" : 6}
]

var parcelType = [
    {"parcelType": 1,
    "parcelDesc": "โฉนดที่ดิน"},
    {"parcelType": 2,
    "parcelDesc": "น.ส.3 / น.ส.3 ข"},
    {"parcelType": 3,
    "parcelDesc": "น.ส.3 ก"},
    {"parcelType": 4,
    "parcelDesc": "โฉนดตราจอง"},
    {"parcelType": 5,
    "parcelDesc": "ตราจองที่ตราว่า 'ได้ทำประโยชน์แล้ว'"},
    {"parcelType": 6,
    "parcelDesc": "ที่สาธารณประโยชน์"},
    {"parcelType": 7,
    "parcelDesc": "หนังสือสำคัญสำหรับที่หลวง (น.ส.ล.)"},
    {"parcelType": 8,
    "parcelDesc": "ใบจอง (น.ส.2) / น.ส.2 ข"},
    {"parcelType": 9,
    "parcelDesc": "ส.ค.1"},
    {"parcelType": 10,
    "parcelDesc": "ที่ว่างเปล่า"},
    {"parcelType": 11,
    "parcelDesc": "ป่า"},
    {"parcelType": 12,
    "parcelDesc": "ที่มีการครอบครอง (ท.ค.)"},
    {"parcelType": 13,
    "parcelDesc": "ทางสาธารณประโยชน์(ทางบก)"},
    {"parcelType": 14,
    "parcelDesc": "ทางสาธารณประโยชน์(ทางน้ำ)"},
    {"parcelType": 15,
    "parcelDesc": "ที่วัด"},
    {"parcelType": 16,
    "parcelDesc": "ที่ราชพัสดุ"},
    {"parcelType": 17,
    "parcelDesc": "บ่อน้ำ, สระน้ำ"},
    {"parcelType": 18,
    "parcelDesc": "หนังสือแสดงการทำประโยชน์ (กสน.5)"},
    {"parcelType": 19,
    "parcelDesc": "หนังสืออนุญาตให้ใช้ประโยชน์ในที่ดินของรัฐ"},
    {"parcelType": 99,
    "parcelDesc": "อื่นๆ (ไม่ปรากฎเลขที่ดินในระวางแผนที่)"}    
]

var printplateType = [
    {"seq" : 1,
    "name" : "โฉนดที่ดิน"},
    {"seq" : 2,
    "name" : "โฉนดตราจอง"},
    {"seq" : 17,
    "name" : "ตราจอง"},
    {"seq" : 3,
    "name" : "ตราจองที่ตราว่าได้ทำประโยชน์แล้ว"},
    {"seq" : 5,
    "name" : "หนังสือรับรองการทำประโยชน์ (น.ส.3)"},
    {"seq" : 4,
    "name" : "หนังสือรับรองการทำประโยชน์ (น.ส.3 ก.)"},
    {"seq" : 8,
    "name" : "หนังสือสำคัญสำหรับที่หลวง (น.ส.ล.)"},
    {"seq" : 23,
    "name" : "หนังสืออนุญาต"},
    {"seq" : 13,
    "name" : "อาคารชุด"},
    {"seq" : 10,
    "name" : "หนังสือกรรมสิทธิ์ห้องชุด (อ.ช.2)"},
    {"seq" : 9,
    "name" : "ที่สาธารณประโยชน์"},
    {"seq" : 7,
    "name" : "หลักฐานการแจ้งการครอบครองที่ดิน (ส.ค.1)"},
    {"seq" : 6,
    "name" : "ใบจอง (น.ส.2)"},
    {"seq" : 11,
    "name" : "อาคารโรงเรือน"},
    {"seq" : 15,
    "name" : "ใบไต่สวน (น.ส.5)"},
    {"seq" : 16,
    "name" : "แบบหมายเลข3"},
    {"seq" : 18,
    "name" : "โฉนดแผนที่ (จส7)"},
    {"seq" : 19,
    "name" : "ก.ส.น.5"},
    {"seq" : 20,
    "name" : "น.ค.3"},
    {"seq" : 21,
    "name" : "ส.ป.ก."},
    {"seq" : 22,
    "name" : "ที่ราชพัสดุ"},
    {"seq" : 28,
    "name" : "ทางสาธารณประโยชน์"},
    {"seq" : 12,
    "name" : "อื่นๆ"},
    {"seq" : 14,
    "name" : "ไม่มีเอกสาร/หลักฐาน"}
]

var sequesterDept = [
    {"seq" : 2,
    "name" : "ศาล"},
    {"seq" : 3,
    "name" : "กรมสรรพากร"},
    {"seq" : 4,
    "name" : "สำนักงานบังคับคดี"},
    {"seq" : 5,
    "name" : "",
    "note" : "อื่น ๆ"},
    {"seq" : 6,
    "name" : "ปปส."},
    {"seq" : 7,
    "name" : "ปปง."}
]

var bookAccType = [
    {"seq" : 2,
    "name" : "บัญชีคุมเรื่องและรับทำการ (ท.อ.14)"},
    {"seq" : 3,
    "name" : "มรดก (ม.81)"},
    {"seq" : 4,
    "name" : "ใบแทน (ม.63)"},
    {"seq" : 5,
    "name" : "งานรังวัด"},
    {"seq" : 6,
    "name" : "จัดสร้างโฉนดใหม่ (ม.64)"},
    {"seq" : 7,
    "name" : "ได้มาที่ดินเพื่อการศาสนา (ม.84)"},
    {"seq" : 8,
    "name" : "ได้มาที่ดินของคนต่างด้าว (ม.93)"},
    {"seq" : 9,
    "name" : "ได้มาที่ดินของคนต่างด้าว (ม.96 ทวิ)"},
    {"seq" : 10,
    "name" : "จัดสรรที่ดิน"},
    {"seq" : 11,
    "name" : "อาคารชุด"},
    {"seq" : 12,
    "name" : "ขออนุญาตดูดทราย (ม.9)"},
    {"seq" : 13,
    "name" : "งานค้าง"},
    {"seq" : 14,
    "name" : "เช่าตามพ.ร.บ.การเช่าอสังหาริมทรัพย์เพื่อพาณิชย์ฯ"},
    {"seq" : 15,
    "name" : "คำขอล่วงหน้า"},
    {"seq" : 16,
    "name" : "ค้าที่ดิน"}
]