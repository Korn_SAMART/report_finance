$(document).ready(function () {

    $('.sidebar-menu').click(function () {
        $('ul a.nav-link').removeClass('active');
        $(this).parent().children('a').addClass('active');
    });

    $('.tab-pane').hide();
    $('.overlay-main').hide();
    
    $.fn.dataTable.ext.errMode = 'none';

    $('.nav-pills-main li ul li a').on('show.bs.tab', function () {
        $(current_href).hide();
        var href = $(this).attr('href');
        // console.log(href);
        current_href = href;
        $(current_href).show();
        sysType = href.split('-')[1];
        type = href.split('-')[2];     

        getDataMaster(sysType,type);

        console.log('#export-success-'+sysType+'-'+type);
        $('#export-success-'+sysType+'-'+type).on("click", function(){
            exportexcel(sysType,type,'s');
        }); 
        $('#export-error-'+sysType+'-'+type).on("click", function(){
            exportexcel(sysType,type,'e');
        }); 

    });


    
    $('.nav-pills-main li a').on('show.bs.tab', function () {
        console.log('down')
        $(current_href).hide();
        var href = $(this).attr('href');
        current_href = href;
        $(current_href).show();
        type = href.split('-')[2]; 
        console.log('type=' + type);

        if(current_href === "#tab-sum-"+type){
            // console.log(current_href + ' === ' + '#tab-sum-'+type);
            getSummary(type);
        }

    });

});



function exportexcel(sysType,type,sts){
    switch (sysType) {
        case 'mas':
            window.open('./queries/mas/exportMas.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'reg':
            window.open('./queries/mas/exportReg.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'evd':
            window.open('./queries/mas/exportEvd.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'sva':
            window.open('./queries/mas/exportSva.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'svc':
            window.open('./queries/mas/exportSvc.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'fin':
            window.open('./queries/mas/exportFin.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'adm':
            window.open('./queries/mas/exportAdm.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'aps':
            window.open('./queries/mas/exportAps.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'ctn':
            window.open('./queries/mas/exportCtn.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'exp':
            window.open('./queries/mas/exportExp.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'gis':
            window.open('./queries/mas/exportGis.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'hrm':
            window.open('./queries/mas/exportHrm.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'inv':
            window.open('./queries/mas/exportInv.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'usp':
            window.open('./queries/mas/exportUsp.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'esp':
            window.open('./queries/mas/exportEsp.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        case 'ech':
            window.open('./queries/mas/exportEch.php?sysType=' + sysType + "&type=" + type + "&sts=" + sts , true);
            break;
        default:
            break;
    }

}






function getDataMaster(sysType,type){
    $('.overlay-main').show();
    $.ajax({
        type : "POST",
        dataType : "json",
        url : "./queries/mas/getMasterData.php",
        data : 'sysType=' + sysType + '&type=' + type,
        success: function(data){
            console.log(data);
            datatemp = data;
            var table = $("#table-" + sysType + "-" + type).DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable" : false,
                        "targets" : 0
                    }
                ],
                "deferRender" : true,
                "pagingType" : "simple_numbers",
                "pageLength" : 20,
                "lengthMenu": [ 5, 10, 20, 50, 75, 100 ],
                "processing" : true,
                "lengthChange": true,
                "ordering" : false,
                "searching" : true,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
            });
            
            if(!jQuery.isEmptyObject(data)){
                table.search("").draw();
                table.clear();
                count = data[0].length;
                num = 0;
                switch (sysType+"-"+type){
                    case 'adm-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < count; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PERMISSION_SEQ_P1']===undefined? "-" : data[0][i]['PERMISSION_SEQ_P1'];
                            temp[2] = data[0][i]['PERMISSION_ID_P1']===undefined? "-" : data[0][i]['PERMISSION_ID_P1'];
                            temp[3] = data[0][i]['PERMISSION_NAME_P1']===undefined? "-" : data[0][i]['PERMISSION_NAME_P1'] ;
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['PERMISSION_SEQ_P2']===undefined? "-" : data[0][i]['PERMISSION_SEQ_P2'];
                            temp[7] = data[0][i]['PERMISSION_ID_P2']===undefined? "-" : data[0][i]['PERMISSION_ID_P2'];
                            temp[8] = data[0][i]['PERMISSION_NAME_P2']===undefined? "-" : data[0][i]['PERMISSION_NAME_P2'] ;
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'adm-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['SYSTEM_SEQ_P1']===undefined? "-" : data[0][i]['SYSTEM_SEQ_P1'];
                            temp[2] = data[0][i]['SYSTEM_ABBR_P1']===undefined? "-" : data[0][i]['SYSTEM_ABBR_P1'];
                            temp[3] = data[0][i]['SYSTEM_NAME_TH_P1']===undefined? "-" : data[0][i]['SYSTEM_NAME_TH_P1'] ;
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['SYSTEM_SEQ_P2']===undefined? "-" : data[0][i]['SYSTEM_SEQ_P2'];
                            temp[7] = data[0][i]['SYSTEM_ABBR_P2']===undefined? "-" : data[0][i]['SYSTEM_ABBR_P2'];
                            temp[8] = data[0][i]['SYSTEM_NAME_TH_P2']===undefined? "-" : data[0][i]['SYSTEM_NAME_TH_P2'] ;
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'adm-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['SCREEN_SEQ_P1']===undefined? "-" : data[0][i]['SCREEN_SEQ_P1'];
                            temp[2] = data[0][i]['SCREEN_ID_P1']===undefined? "-" : data[0][i]['SCREEN_ID_P1'];
                            temp[3] = data[0][i]['SCREEN_NAME_TH_P1']===undefined? "-" : data[0][i]['SCREEN_NAME_TH_P1'] ;
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['SCREEN_SEQ_P2']===undefined? "-" : data[0][i]['SCREEN_SEQ_P2'];
                            temp[7] = data[0][i]['SCREEN_ID_P2']===undefined? "-" : data[0][i]['SCREEN_ID_P2'];
                            temp[8] = data[0][i]['SCREEN_NAME_TH_P2']===undefined? "-" : data[0][i]['SCREEN_NAME_TH_P2'] ;
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'adm-4':
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[2] = data[0][i]['USER_GROUP_SEQ_P1']===undefined? "-" : data[0][i]['USER_GROUP_SEQ_P1'];
                            temp[1] = data[0][i]['USER_GROUP_ID_P1']===undefined? "-" : data[0][i]['USER_GROUP_ID_P1'];
                            temp[3] = data[0][i]['USER_GROUP_NAME_P1']===undefined? "-" : data[0][i]['USER_GROUP_NAME_P1'] ;
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['USER_GROUP_SEQ_P2']===undefined? "-" : data[0][i]['USER_GROUP_SEQ_P2'];
                            temp[6] = data[0][i]['USER_GROUP_ID_P2']===undefined? "-" : data[0][i]['USER_GROUP_ID_P2'];
                            temp[8] = data[0][i]['USER_GROUP_NAME_P2']===undefined? "-" : data[0][i]['USER_GROUP_NAME_P2'] ;
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'adm-5':
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[2] = data[0][i]['MENU_GROUP_SEQ_P1']===undefined? "-" : data[0][i]['MENU_GROUP_SEQ_P1'];
                            temp[1] = data[0][i]['MENU_GROUP_NAME_P1']===undefined? "-" : data[0][i]['MENU_GROUP_NAME_P1'];
                            temp[3] = data[0][i]['MENU_GROUP_DESC_P1']===undefined? "-" : data[0][i]['MENU_GROUP_DESC_P1'] ;
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['MENU_GROUP_SEQ_P2']===undefined? "-" : data[0][i]['MENU_GROUP_SEQ_P2'];
                            temp[6] = data[0][i]['MENU_GROUP_NAME_P2']===undefined? "-" : data[0][i]['MENU_GROUP_NAME_P2'];
                            temp[8] = data[0][i]['MENU_GROUP_DESC_P2']===undefined? "-" : data[0][i]['MENU_GROUP_DESC_P2'] ;
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'adm-6':
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[2] = data[0][i]['POSITION_GROUP_SEQ_P1']===undefined? "-" : data[0][i]['POSITION_GROUP_SEQ_P1'];
                            temp[1] = data[0][i]['POSITION_GROUP_ID_P1']===undefined? "-" : data[0][i]['POSITION_GROUP_ID_P1'];
                            temp[3] = data[0][i]['POSITION_GROUP_NAME_P1']===undefined? "-" : data[0][i]['POSITION_GROUP_NAME_P1'] ;
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['POSITION_GROUP_SEQ_P2']===undefined? "-" : data[0][i]['POSITION_GROUP_SEQ_P2'];
                            temp[6] = data[0][i]['POSITION_GROUP_ID_P2']===undefined? "-" : data[0][i]['POSITION_GROUP_ID_P2'];
                            temp[8] = data[0][i]['POSITION_GROUP_NAME_P2']===undefined? "-" : data[0][i]['POSITION_GROUP_NAME_P2'] ;
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'aps-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['ROOM_USAGE_SEQ_P1']===undefined? "-" : data[0][i]['ROOM_USAGE_SEQ_P1'];
                            temp[2] = data[0][i]['ROOM_USAGE_ID_P1']===undefined? "-" : data[0][i]['ROOM_USAGE_ID_P1'];
                            temp[3] = data[0][i]['ROOM_USAGE_NAME_P1']===undefined? "-" : data[0][i]['ROOM_USAGE_NAME_P1'] ;
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['ROOM_USAGE_SEQ_P2']===undefined? "-" : data[0][i]['ROOM_USAGE_SEQ_P2'];
                            temp[7] = data[0][i]['ROOM_USAGE_ID_P2']===undefined? "-" : data[0][i]['ROOM_USAGE_ID_P2'];
                            temp[8] = data[0][i]['ROOM_USAGE_NAME_P2']===undefined? "-" : data[0][i]['ROOM_USAGE_NAME_P2'] ;
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'aps-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['POSSESSORY_SEQ_P1']===undefined? "-" : data[0][i]['POSSESSORY_SEQ_P1'];
                            temp[2] = data[0][i]['POSSESSORY_ID_P1']===undefined? "-" : data[0][i]['POSSESSORY_ID_P1'];
                            temp[3] = data[0][i]['POSSESSORY_NAME_P1']===undefined? "-" : data[0][i]['POSSESSORY_NAME_P1'] ;
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['POSSESSORY_SEQ_P2']===undefined? "-" : data[0][i]['POSSESSORY_SEQ_P2'];
                            temp[7] = data[0][i]['POSSESSORY_ID_P2']===undefined? "-" : data[0][i]['POSSESSORY_ID_P2'];
                            temp[8] = data[0][i]['POSSESSORY_NAME_P2']===undefined? "-" : data[0][i]['POSSESSORY_NAME_P2'] ;
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'aps-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['FLOOR_SEQ_P1']===undefined? "-" : data[0][i]['FLOOR_SEQ_P1'];
                            temp[2] = data[0][i]['FLOOR_NAME_P1']===undefined? "-" : data[0][i]['FLOOR_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['FLOOR_SEQ_P2']===undefined? "-" : data[0][i]['FLOOR_SEQ_P2'];
                            temp[6] = data[0][i]['FLOOR_NAME_P2']===undefined? "-" : data[0][i]['FLOOR_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'ctn-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['DOC_CLASS_SEQ_P1']===undefined? "-" : data[0][i]['DOC_CLASS_SEQ_P1'];
                            temp[2] = data[0][i]['DOC_CLASS_NAME_P1']===undefined? "-" : data[0][i]['DOC_CLASS_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['DOC_CLASS_SEQ_P2']===undefined? "-" : data[0][i]['DOC_CLASS_SEQ_P2'];
                            temp[6] = data[0][i]['DOC_CLASS_NAME_P2']===undefined? "-" : data[0][i]['DOC_CLASS_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'ctn-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['DOC_SECRET_SEQ_P1']===undefined? "-" : data[0][i]['DOC_SECRET_SEQ_P1'];
                            temp[2] = data[0][i]['DOC_SECRET_NAME_P1']===undefined? "-" : data[0][i]['DOC_SECRET_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['DOC_SECRET_SEQ_P2']===undefined? "-" : data[0][i]['DOC_SECRET_SEQ_P2'];
                            temp[6] = data[0][i]['DOC_SECRET_NAME_P2']===undefined? "-" : data[0][i]['DOC_SECRET_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'ctn-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['DOC_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['DOC_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['DOC_TYPE_NAME_P1']===undefined? "-" : data[0][i]['DOC_TYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['DOC_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['DOC_TYPE_SEQ_P2'];
                            temp[6] = data[0][i]['DOC_TYPE_NAME_P2']===undefined? "-" : data[0][i]['DOC_TYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'ctn-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LANDOFFICE_SEQ_P1']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P1'];
                            temp[2] = data[0][i]['LANDOFFICE_NAME_TH_P1']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P1'];
                            temp[3] = data[0][i]['LANDOFFICE_LETTER_NO_P1']===undefined? "-" : data[0][i]['LANDOFFICE_LETTER_NO_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['LANDOFFICE_SEQ_P2']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P2'];
                            temp[7] = data[0][i]['LANDOFFICE_NAME_TH_P2']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P2'];
                            temp[8] = data[0][i]['LANDOFFICE_LETTER_NO_P2']===undefined? "-" : data[0][i]['LANDOFFICE_LETTER_NO_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'exp-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['ILLEGALITY_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['ILLEGALITY_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['ILLEGALITY_TYPE_NAME_P1']===undefined? "-" : data[0][i]['ILLEGALITY_TYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['ILLEGALITY_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['ILLEGALITY_TYPE_SEQ_P2'];
                            temp[6] = data[0][i]['ILLEGALITY_TYPE_NAME_P2']===undefined? "-" : data[0][i]['ILLEGALITY_TYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'exp-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['JOB_GROUP_SEQ_P1']===undefined? "-" : data[0][i]['JOB_GROUP_SEQ_P1'];
                            temp[2] = data[0][i]['JOB_GROUP_NAME_P1']===undefined? "-" : data[0][i]['JOB_GROUP_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['JOB_GROUP_SEQ_P2']===undefined? "-" : data[0][i]['JOB_GROUP_SEQ_P2'];
                            temp[6] = data[0][i]['JOB_GROUP_NAME_P2']===undefined? "-" : data[0][i]['JOB_GROUP_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'exp-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PROC_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['PROC_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['PROC_TYPE_NAME_P1']===undefined? "-" : data[0][i]['PROC_TYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['PROC_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['PROC_TYPE_SEQ_P2'];
                            temp[6] = data[0][i]['PROC_TYPE_NAME_P2']===undefined? "-" : data[0][i]['PROC_TYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'exp-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['REQUEST_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['REQUEST_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['REQUEST_NAME_P1']===undefined? "-" : data[0][i]['REQUEST_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['REQUEST_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['REQUEST_TYPE_SEQ_P2'];
                            temp[6] = data[0][i]['REQUEST_NAME_P2']===undefined? "-" : data[0][i]['REQUEST_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'exp-5':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['SUB_JOB_SEQ_P1']===undefined? "-" : data[0][i]['SUB_JOB_SEQ_P1'];
                            temp[2] = data[0][i]['SUB_JOB_NAME_P1']===undefined? "-" : data[0][i]['SUB_JOB_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['SUB_JOB_SEQ_P2']===undefined? "-" : data[0][i]['SUB_JOB_SEQ_P2'];
                            temp[6] = data[0][i]['SUB_JOB_NAME_P2']===undefined? "-" : data[0][i]['SUB_JOB_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'exp-6':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['USE_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['USE_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['USE_TYPE_NAME_P1']===undefined? "-" : data[0][i]['USE_TYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['USE_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['USE_TYPE_SEQ_P2'];
                            temp[6] = data[0][i]['USE_TYPE_NAME_P2']===undefined? "-" : data[0][i]['USE_TYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'exp-7':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['WORK_STS_SEQ_P1']===undefined? "-" : data[0][i]['WORK_STS_SEQ_P1'];
                            temp[2] = data[0][i]['WORK_STS_NAME_P1']===undefined? "-" : data[0][i]['WORK_STS_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['WORK_STS_SEQ_P2']===undefined? "-" : data[0][i]['WORK_STS_SEQ_P2'];
                            temp[6] = data[0][i]['WORK_STS_NAME_P2']===undefined? "-" : data[0][i]['WORK_STS_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'fin-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['CONFIG_NO_P1']===undefined? "-" : data[0][i]['CONFIG_NO_P1'];
                            temp[2] = data[0][i]['CONFIG_NAME_P1']===undefined? "-" : data[0][i]['CONFIG_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['CONFIG_NO_P2']===undefined? "-" : data[0][i]['CONFIG_NO_P2'];
                            temp[6] = data[0][i]['CONFIG_NAME_P2']===undefined? "-" : data[0][i]['CONFIG_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'fin-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['FUND_TYPE_ID_P1']===undefined? "-" : data[0][i]['FUND_TYPE_ID_P1'];
                            temp[2] = data[0][i]['FUND_NAME_P1']===undefined? "-" : data[0][i]['FUND_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['FUND_TYPE_ID_P2']===undefined? "-" : data[0][i]['FUND_TYPE_ID_P2'];
                            temp[6] = data[0][i]['FUND_NAME_P2']===undefined? "-" : data[0][i]['FUND_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'fin-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['INCOME_ID_P1']===undefined? "-" : data[0][i]['INCOME_ID_P1'];
                            temp[2] = data[0][i]['INCOME_CODE_P1']===undefined? "-" : data[0][i]['INCOME_CODE_P1'];
                            temp[3] = data[0][i]['INCOME_NAME_P1']===undefined? "-" : data[0][i]['INCOME_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['INCOME_ID_P2']===undefined? "-" : data[0][i]['INCOME_ID_P2'];
                            temp[7] = data[0][i]['INCOME_CODE_P2']===undefined? "-" : data[0][i]['INCOME_CODE_P2'];
                            temp[8] = data[0][i]['INCOME_NAME_P2']===undefined? "-" : data[0][i]['INCOME_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'fin-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['REVENUE_ID_P1']===undefined? "-" : data[0][i]['REVENUE_ID_P1'];
                            temp[2] = data[0][i]['REVENUE_CODE_P1']===undefined? "-" : data[0][i]['REVENUE_CODE_P1'];
                            temp[3] = data[0][i]['REVENUE_NAME_P1']===undefined? "-" : data[0][i]['REVENUE_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['REVENUE_ID_P2']===undefined? "-" : data[0][i]['REVENUE_ID_P2'];
                            temp[7] = data[0][i]['REVENUE_CODE_P2']===undefined? "-" : data[0][i]['REVENUE_CODE_P2'];
                            temp[8] = data[0][i]['REVENUE_NAME_P2']===undefined? "-" : data[0][i]['REVENUE_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'fin-5':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['STORE_ID_P1']===undefined? "-" : data[0][i]['STORE_ID_P1'];
                            temp[2] = data[0][i]['STORE_ABBR_NAME_P1']===undefined? "-" : data[0][i]['STORE_ABBR_NAME_P1'];
                            temp[3] = data[0][i]['STORE_NAME_P2']===undefined? "-" : data[0][i]['STORE_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['STORE_ID_P2']===undefined? "-" : data[0][i]['STORE_ID_P2'];
                            temp[7] = data[0][i]['STORE_ABBR_NAME_P2']===undefined? "-" : data[0][i]['STORE_ABBR_NAME_P2'];
                            temp[8] = data[0][i]['STORE_NAME_P2']===undefined? "-" : data[0][i]['STORE_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'gis-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PROVINCE_ID_P1']===undefined? "-" : data[0][i]['PROVINCE_ID_P1'];
                            temp[2] = data[0][i]['NAME_T_P1']===undefined? "-" : data[0][i]['NAME_T_P1'];
                            temp[3] = data[0][i]['LONGTITUDE_P1']===undefined? "-" : data[0][i]['LONGTITUDE_P1'];
                            temp[4] = data[0][i]['LATTITUDE_P1']===undefined? "-" : data[0][i]['LATTITUDE_P1'];
                            temp[5] = data[0][i]['PROVINCE_ID_P2']===undefined? "-" : data[0][i]['PROVINCE_ID_P2'];
                            temp[6] = data[0][i]['NAME_T_P2']===undefined? "-" : data[0][i]['NAME_T_P2'];
                            temp[7] = data[0][i]['LONGTITUDE_P2']===undefined? "-" : data[0][i]['LONGTITUDE_P2'];
                            temp[8] = data[0][i]['LATTITUDE_P2']===undefined? "-" : data[0][i]['LATTITUDE_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'gis-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['NAME_T_P_P1']===undefined? "-" : data[0][i]['NAME_T_P_P1'];
                            temp[2] = data[0][i]['AMPHOE_ID_P1']===undefined? "-" : data[0][i]['AMPHOE_ID_P1'];
                            temp[3] = data[0][i]['NAME_T_P1']===undefined? "-" : data[0][i]['NAME_T_P1'];
                            temp[4] = data[0][i]['LONGTITUDE_P1']===undefined? "-" : data[0][i]['LONGTITUDE_P1'];
                            temp[5] = data[0][i]['LATTITUDE_P1']===undefined? "-" : data[0][i]['LATTITUDE_P1'];
                            temp[6] = data[0][i]['NAME_T_P_P2']===undefined? "-" : data[0][i]['NAME_T_P_P2'];
                            temp[7] = data[0][i]['AMPHOE_ID_P2']===undefined? "-" : data[0][i]['AMPHOE_ID_P2'];
                            temp[8] = data[0][i]['NAME_T_P2']===undefined? "-" : data[0][i]['NAME_T_P2'];
                            temp[9] = data[0][i]['LONGTITUDE_P2']===undefined? "-" : data[0][i]['LONGTITUDE_P2'];
                            temp[10] = data[0][i]['LATTITUDE_P2']===undefined? "-" : data[0][i]['LATTITUDE_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'gis-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['NAME_T_P_P1']===undefined? "-" : data[0][i]['NAME_T_P_P1'];
                            temp[2] = data[0][i]['NAME_T_A_P1']===undefined? "-" : data[0][i]['NAME_T_A_P1'];
                            temp[3] = data[0][i]['TAMBON_ID_P1']===undefined? "-" : data[0][i]['TAMBON_ID_P1'];
                            temp[4] = data[0][i]['NAME_T_P1']===undefined? "-" : data[0][i]['NAME_T_P1'];
                            temp[5] = data[0][i]['LONGTITUDE_P1']===undefined? "-" : data[0][i]['LONGTITUDE_P1'];
                            temp[6] = data[0][i]['LATTITUDE_P1']===undefined? "-" : data[0][i]['LATTITUDE_P1'];
                            temp[7] = data[0][i]['NAME_T_P_P2']===undefined? "-" : data[0][i]['NAME_T_P_P2'];
                            temp[8] = data[0][i]['NAME_T_A_P2']===undefined? "-" : data[0][i]['NAME_T_A_P2'];
                            temp[9] = data[0][i]['TAMBON_ID_P2']===undefined? "-" : data[0][i]['TAMBON_ID_P2'];
                            temp[10] = data[0][i]['NAME_T_P2']===undefined? "-" : data[0][i]['NAME_T_P2'];
                            temp[11] = data[0][i]['LONGTITUDE_P2']===undefined? "-" : data[0][i]['LONGTITUDE_P2'];
                            temp[12] = data[0][i]['LATTITUDE_P2']===undefined? "-" : data[0][i]['LATTITUDE_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'gis-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['ORG_CODE_P1']===undefined? "-" : data[0][i]['ORG_CODE_P1'];
                            temp[2] = data[0][i]['ORG_NAME_P1']===undefined? "-" : data[0][i]['ORG_NAME_P1'];
                            temp[3] = data[0][i]['ORG_LONG_P1']===undefined? "-" : data[0][i]['ORG_LONG_P1'];
                            temp[4] = data[0][i]['ORG_LAT_P1']===undefined? "-" : data[0][i]['ORG_LAT_P1'];
                            temp[5] = data[0][i]['ORG_CODE_P2']===undefined? "-" : data[0][i]['ORG_CODE_P2'];
                            temp[6] = data[0][i]['ORG_NAME_P2']===undefined? "-" : data[0][i]['ORG_NAME_P2'];
                            temp[7] = data[0][i]['ORG_LONG_P2']===undefined? "-" : data[0][i]['ORG_LONG_P2'];
                            temp[8] = data[0][i]['ORG_LAT_P2']===undefined? "-" : data[0][i]['ORG_LAT_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'gis-5':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['CRS_CODE_P1']===undefined? "-" : data[0][i]['CRS_CODE_P1'];
                            temp[2] = data[0][i]['CRS_NAME_P1']===undefined? "-" : data[0][i]['CRS_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['CRS_CODE_P2']===undefined? "-" : data[0][i]['CRS_CODE_P2'];
                            temp[6] = data[0][i]['CRS_NAME_P2']===undefined? "-" : data[0][i]['CRS_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'gis-6':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['DEFINIT_SEQ_P1']===undefined? "-" : data[0][i]['DEFINIT_SEQ_P1'];
                            temp[2] = data[0][i]['DEFINIT_CODE_P1']===undefined? "-" : data[0][i]['DEFINIT_CODE_P1'];
                            temp[3] = data[0][i]['DEFENIT_NAME_P1']===undefined? "-" : data[0][i]['DEFENIT_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['DEFINIT_SEQ_P2']===undefined? "-" : data[0][i]['DEFINIT_SEQ_P2'];
                            temp[7] = data[0][i]['DEFINIT_CODE_P2']===undefined? "-" : data[0][i]['DEFINIT_CODE_P2'];
                            temp[8] = data[0][i]['DEFENIT_NAME_P2']===undefined? "-" : data[0][i]['DEFENIT_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'gis-7':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PRODUCT_GROUP_SEQ_P1']===undefined? "-" : data[0][i]['PRODUCT_GROUP_SEQ_P1'];
                            temp[2] = data[0][i]['GROUP_NAME_P1']===undefined? "-" : data[0][i]['GROUP_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['PRODUCT_GROUP_SEQ_P2']===undefined? "-" : data[0][i]['PRODUCT_GROUP_SEQ_P2'];
                            temp[6] = data[0][i]['GROUP_NAME_P2']===undefined? "-" : data[0][i]['GROUP_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'hrm-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['DIV_POSITION_SEQ_P1']===undefined? "-" : data[0][i]['DIV_POSITION_SEQ_P1'];
                            temp[2] = data[0][i]['DIV_POSITION_NAME_P1']===undefined? "-" : data[0][i]['DIV_POSITION_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['DIV_POSITION_SEQ_P2']===undefined? "-" : data[0][i]['DIV_POSITION_SEQ_P2'];
                            temp[6] = data[0][i]['DIV_POSITION_NAME_P2']===undefined? "-" : data[0][i]['DIV_POSITION_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'hrm-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PSN_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['PSN_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['PSN_TYPE_NAME_P1']===undefined? "-" : data[0][i]['PSN_TYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['PSN_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['PSN_TYPE_SEQ_P2'];
                            temp[6] = data[0][i]['PSN_TYPE_NAME_P2']===undefined? "-" : data[0][i]['PSN_TYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'hrm-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LEAVE_RIGHT_SEQ_P1']===undefined? "-" : data[0][i]['LEAVE_RIGHT_SEQ_P1'];
                            temp[2] = data[0][i]['LEAVE_TYPE_NAME_P1']===undefined? "-" : data[0][i]['LEAVE_TYPE_NAME_P1'];
                            temp[3] = data[0][i]['PSN_TYPE_NAME_P1']===undefined? "-" : data[0][i]['PSN_TYPE_NAME_P1'];
                            temp[4] = data[0][i]['LEAVE_AMOUNT_P1']===undefined? "-" : data[0][i]['LEAVE_AMOUNT_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['LEAVE_RIGHT_SEQ_P2']===undefined? "-" : data[0][i]['LEAVE_RIGHT_SEQ_P2'];
                            temp[8] = data[0][i]['LEAVE_TYPE_NAME_P2']===undefined? "-" : data[0][i]['LEAVE_TYPE_NAME_P2'];
                            temp[9] = data[0][i]['PSN_TYPE_NAME_P2']===undefined? "-" : data[0][i]['PSN_TYPE_NAME_P2'];
                            temp[10] = data[0][i]['LEAVE_AMOUNT_P2']===undefined? "-" : data[0][i]['LEAVE_AMOUNT_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'inv-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['TYPE_ASSET_SEQ_P1']===undefined? "-" : data[0][i]['TYPE_ASSET_SEQ_P1'];
                            temp[2] = data[0][i]['TYPE_ASSET_ID_P1']===undefined? "-" : data[0][i]['TYPE_ASSET_ID_P1'];
                            temp[3] = data[0][i]['TYPE_ASSET_NAME_P1']===undefined? "-" : data[0][i]['TYPE_ASSET_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['TYPE_ASSET_SEQ_P2']===undefined? "-" : data[0][i]['TYPE_ASSET_SEQ_P2'];
                            temp[7] = data[0][i]['TYPE_ASSET_ID_P2']===undefined? "-" : data[0][i]['TYPE_ASSET_ID_P2'];
                            temp[8] = data[0][i]['TYPE_ASSET_NAME_P2']===undefined? "-" : data[0][i]['TYPE_ASSET_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'inv-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['MAT_SEQ_P1']===undefined? "-" : data[0][i]['MAT_SEQ_P1'];
                            temp[2] = data[0][i]['MAT_NAME_P1']===undefined? "-" : data[0][i]['MAT_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['MAT_SEQ_P2']===undefined? "-" : data[0][i]['MAT_SEQ_P2'];
                            temp[6] = data[0][i]['MAT_NAME_P2']===undefined? "-" : data[0][i]['MAT_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'inv-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PLATE_SEQ_P1']===undefined? "-" : data[0][i]['PLATE_SEQ_P1'];
                            temp[2] = data[0][i]['PLATE_ABBR_NAME_P1']===undefined? "-" : data[0][i]['PLATE_ABBR_NAME_P1'];
                            temp[3] = data[0][i]['PLATE_NAME_P1']===undefined? "-" : data[0][i]['PLATE_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['PLATE_SEQ_P2']===undefined? "-" : data[0][i]['PLATE_SEQ_P2'];
                            temp[7] = data[0][i]['PLATE_ABBR_NAME_P2']===undefined? "-" : data[0][i]['PLATE_ABBR_NAME_P2'];
                            temp[8] = data[0][i]['PLATE_NAME_P2']===undefined? "-" : data[0][i]['PLATE_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'inv-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PIECE_SEQ_P1']===undefined? "-" : data[0][i]['PIECE_SEQ_P1'];
                            temp[2] = data[0][i]['PIECE_NAME_P1']===undefined? "-" : data[0][i]['PIECE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['PIECE_SEQ_P2']===undefined? "-" : data[0][i]['PIECE_SEQ_P2'];
                            temp[6] = data[0][i]['PIECE_NAME_P2']===undefined? "-" : data[0][i]['PIECE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'inv-5':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['BLD_SEQ_P1']===undefined? "-" : data[0][i]['BLD_SEQ_P1'];
                            temp[2] = data[0][i]['BLD_NAME_P1']===undefined? "-" : data[0][i]['BLD_NAME_P1'];
                            temp[3] = data[0][i]['BLD_YEAR_P1']===undefined? "-" : data[0][i]['BLD_YEAR_P1'];
                            temp[4] = data[0][i]['BLD_RATE_P1']===undefined? "-" : data[0][i]['BLD_RATE_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['BLD_SEQ_P2']===undefined? "-" : data[0][i]['BLD_SEQ_P2'];
                            temp[8] = data[0][i]['BLD_NAME_P2']===undefined? "-" : data[0][i]['BLD_NAME_P2'];
                            temp[9] = data[0][i]['BLD_YEAR_P2']===undefined? "-" : data[0][i]['BLD_YEAR_P2'];
                            temp[10] = data[0][i]['BLD_RATE_P2']===undefined? "-" : data[0][i]['BLD_RATE_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'mas-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['COUNTRY_SEQ_P1']===undefined? "-" : data[0][i]['COUNTRY_SEQ_P1'];
                            temp[2] = data[0][i]['COUNTRY_ID_P1']===undefined? "-" : data[0][i]['COUNTRY_ID_P1'];
                            temp[3] = data[0][i]['COUNTRY_NAME_P1']===undefined? "-" : data[0][i]['COUNTRY_NAME_P1'];
                            temp[4] = data[0][i]['COUNTRY_NAME_EN_P1']===undefined? "-" : data[0][i]['COUNTRY_NAME_EN_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['COUNTRY_SEQ_P2']===undefined? "-" : data[0][i]['COUNTRY_SEQ_P2'];
                            temp[8] = data[0][i]['COUNTRY_ID_P2']===undefined? "-" : data[0][i]['COUNTRY_ID_P2'];
                            temp[9] = data[0][i]['COUNTRY_NAME_P2']===undefined? "-" : data[0][i]['COUNTRY_NAME_P2'];
                            temp[10] = data[0][i]['COUNTRY_NAME_EN_P2']===undefined? "-" : data[0][i]['COUNTRY_NAME_EN_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PROVINCE_SEQ_P1']===undefined? "-" : data[0][i]['PROVINCE_SEQ_P1'];
                            temp[2] = data[0][i]['PROVINCE_ID_P1']===undefined? "-" : data[0][i]['PROVINCE_ID_P1'];
                            temp[3] = data[0][i]['PROVINCE_NAME_P1']===undefined? "-" : data[0][i]['PROVINCE_NAME_P1'];
                            temp[4] = data[0][i]['PROVINCE_ABBR_P1']===undefined? "-" : data[0][i]['PROVINCE_ABBR_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['PROVINCE_SEQ_P2']===undefined? "-" : data[0][i]['PROVINCE_SEQ_P2'];
                            temp[8] = data[0][i]['PROVINCE_ID_P2']===undefined? "-" : data[0][i]['PROVINCE_ID_P2'];
                            temp[9] = data[0][i]['PROVINCE_NAME_P2']===undefined? "-" : data[0][i]['PROVINCE_NAME_P2'];
                            temp[10] = data[0][i]['PROVINCE_ABBR_P2']===undefined? "-" : data[0][i]['PROVINCE_ABBR_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['AMPHUR_SEQ_P1']===undefined? "-" : data[0][i]['AMPHUR_SEQ_P1'];
                            temp[2] = data[0][i]['AMPHUR_ID_P1']===undefined? "-" : data[0][i]['AMPHUR_ID_P1'];
                            temp[3] = data[0][i]['PROVINCE_NAME_P1']===undefined? "-" : data[0][i]['PROVINCE_NAME_P1'];
                            temp[4] = data[0][i]['AMPHUR_NAME_P1']===undefined? "-" : data[0][i]['AMPHUR_NAME_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['AMPHUR_SEQ_P2']===undefined? "-" : data[0][i]['AMPHUR_SEQ_P2'];
                            temp[8] = data[0][i]['AMPHUR_ID_P2']===undefined? "-" : data[0][i]['AMPHUR_ID_P2'];
                            temp[9] = data[0][i]['PROVINCE_NAME_P2']===undefined? "-" : data[0][i]['PROVINCE_NAME_P2'];
                            temp[10] = data[0][i]['AMPHUR_NAME_P2']===undefined? "-" : data[0][i]['AMPHUR_NAME_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];

                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['TAMBOL_SEQ_P1']===undefined? "-" : data[0][i]['TAMBOL_SEQ_P1'];
                            temp[2] = data[0][i]['TAMBOL_ID_P1']===undefined? "-" : data[0][i]['TAMBOL_ID_P1'];
                            temp[3] = data[0][i]['PROVINCE_NAME_P1']===undefined? "-" : data[0][i]['PROVINCE_NAME_P1'];
                            temp[4] = data[0][i]['AMPHUR_NAME_P1']===undefined? "-" : data[0][i]['AMPHUR_NAME_P1'];
                            temp[5] = data[0][i]['TAMBOL_NAME_P1']===undefined? "-" : data[0][i]['TAMBOL_NAME_P1'];
                            temp[6] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[7] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[8] = data[0][i]['TAMBOL_SEQ_P2']===undefined? "-" : data[0][i]['TAMBOL_SEQ_P2'];
                            temp[9] = data[0][i]['TAMBOL_ID_P2']===undefined? "-" : data[0][i]['TAMBOL_ID_P2'];
                            temp[10] = data[0][i]['PROVINCE_NAME_P2']===undefined? "-" : data[0][i]['PROVINCE_NAME_P2'];
                            temp[11] = data[0][i]['AMPHUR_NAME_P2']===undefined? "-" : data[0][i]['AMPHUR_NAME_P2'];
                            temp[12] = data[0][i]['TAMBOL_NAME_P2']===undefined? "-" : data[0][i]['TAMBOL_NAME_P2'];
                            temp[13] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[14] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-5':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LANDOFFICE_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['LANDOFFICE_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['LANDOFFICE_TYPE_ID_P1']===undefined? "-" : data[0][i]['LANDOFFICE_TYPE_ID_P1'];
                            temp[3] = data[0][i]['LANDOFFICE_TYPE_NAME_P1']===undefined? "-" : data[0][i]['LANDOFFICE_TYPE_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['LANDOFFICE_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['LANDOFFICE_TYPE_SEQ_P2'];
                            temp[7] = data[0][i]['LANDOFFICE_TYPE_ID_P2']===undefined? "-" : data[0][i]['LANDOFFICE_TYPE_ID_P2'];
                            temp[8] = data[0][i]['LANDOFFICE_TYPE_NAME_P2']===undefined? "-" : data[0][i]['LANDOFFICE_TYPE_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-52':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LANDOFFICE_SEQ_P1']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P1'];
                            temp[2] = data[0][i]['LANDOFFICE_NAME_TH_P1']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['LANDOFFICE_SEQ_P2']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P2'];
                            temp[6] = data[0][i]['LANDOFFICE_NAME_TH_P2']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-6':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['FINANCE_SEQ_P1']===undefined? "-" : data[0][i]['FINANCE_SEQ_P1'];
                            temp[2] = data[0][i]['FINANCE_ID_P1']===undefined? "-" : data[0][i]['FINANCE_ID_P1'];
                            temp[3] = data[0][i]['FINANCE_THAI_NAME_P1']===undefined? "-" : data[0][i]['FINANCE_THAI_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['FINANCE_SEQ_P2']===undefined? "-" : data[0][i]['FINANCE_SEQ_P2'];
                            temp[7] = data[0][i]['FINANCE_ID_P2']===undefined? "-" : data[0][i]['FINANCE_ID_P2'];
                            temp[8] = data[0][i]['FINANCE_THAI_NAME_P2']===undefined? "-" : data[0][i]['FINANCE_THAI_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-7':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LANDUSED_SEQ_P1']===undefined? "-" : data[0][i]['LANDUSED_SEQ_P1'];
                            temp[2] = data[0][i]['LANDUSED_NAME_P1']===undefined? "-" : data[0][i]['LANDUSED_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['LANDUSED_SEQ_P2']===undefined? "-" : data[0][i]['LANDUSED_SEQ_P2'];
                            temp[6] = data[0][i]['LANDUSED_NAME_P2']===undefined? "-" : data[0][i]['LANDUSED_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-8':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['OPT_SEQ_P1']===undefined? "-" : data[0][i]['OPT_SEQ_P1'];
                            temp[2] = data[0][i]['OPT_NAME_P1']===undefined? "-" : data[0][i]['OPT_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['OPT_SEQ_P2']===undefined? "-" : data[0][i]['OPT_SEQ_P2'];
                            temp[6] = data[0][i]['OPT_NAME_P2']===undefined? "-" : data[0][i]['OPT_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-9':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PRINTPLATE_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['PRINTPLATE_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['PRINTPLATE_TYPE_ID_P1']===undefined? "-" : data[0][i]['PRINTPLATE_TYPE_ID_P1'];
                            temp[3] = data[0][i]['PRINTPLATE_TYPE_ABBR_P1']===undefined? "-" : data[0][i]['PRINTPLATE_TYPE_ABBR_P1'];
                            temp[4] = data[0][i]['PRINTPLATE_TYPE_NAME_P1']===undefined? "-" : data[0][i]['PRINTPLATE_TYPE_NAME_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['PRINTPLATE_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['PRINTPLATE_TYPE_SEQ_P2'];
                            temp[8] = data[0][i]['PRINTPLATE_TYPE_ID_P2']===undefined? "-" : data[0][i]['PRINTPLATE_TYPE_ID_P2'];
                            temp[9] = data[0][i]['PRINTPLATE_TYPE_ABBR_P2']===undefined? "-" : data[0][i]['PRINTPLATE_TYPE_ABBR_P2'];
                            temp[10] = data[0][i]['PRINTPLATE_TYPE_NAME_P2']===undefined? "-" : data[0][i]['PRINTPLATE_TYPE_NAME_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-10':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['TITLE_SEQ_P1']===undefined? "-" : data[0][i]['TITLE_SEQ_P1'];
                            temp[2] = data[0][i]['TITLE_ABBR_P1']===undefined? "-" : data[0][i]['TITLE_ABBR_P1'];
                            temp[3] = data[0][i]['TITLE_NAME_P1']===undefined? "-" : data[0][i]['TITLE_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['TITLE_SEQ_P2']===undefined? "-" : data[0][i]['TITLE_SEQ_P2'];
                            temp[7] = data[0][i]['TITLE_ABBR_P2']===undefined? "-" : data[0][i]['TITLE_ABBR_P2'];
                            temp[8] = data[0][i]['TITLE_NAME_P2']===undefined? "-" : data[0][i]['TITLE_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-11':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['COURT_SEQ_P1']===undefined? "-" : data[0][i]['COURT_SEQ_P1'];
                            temp[2] = data[0][i]['COURT_NAME_P1']===undefined? "-" : data[0][i]['COURT_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['COURT_SEQ_P2']===undefined? "-" : data[0][i]['COURT_SEQ_P2'];
                            temp[6] = data[0][i]['COURT_NAME_P2']===undefined? "-" : data[0][i]['COURT_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-12':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[3] = data[0][i]['LANDOFFICE_AREA_SEQ_P1']===undefined? "-" : data[0][i]['LANDOFFICE_AREA_SEQ_P1'];
                            temp[1] = data[0][i]['LANDOFFICE_NAME_TH_P1']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P1'];
                            temp[2] = data[0][i]['AMPHUR_NAME_P1']===undefined? "-" : data[0][i]['AMPHUR_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];
                            
                            temp[8] = data[0][i]['LANDOFFICE_AREA_SEQ_P2']===undefined? "-" : data[0][i]['LANDOFFICE_AREA_SEQ_P2'];
                            temp[6] = data[0][i]['LANDOFFICE_NAME_TH_P2']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P2'];
                            temp[7] = data[0][i]['AMPHUR_NAME_P2']===undefined? "-" : data[0][i]['AMPHUR_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-13':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['OPT_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['OPT_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['OPT_TYPE_NAME_P1']===undefined? "-" : data[0][i]['OPT_TYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];
                            temp[5] = data[0][i]['OPT_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['OPT_TYPE_SEQ_P2'];
                            temp[6] = data[0][i]['OPT_TYPE_NAME_P2']===undefined? "-" : data[0][i]['OPT_TYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-132':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LANDOFFICE_NAME_TH_P1']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P1'];
                            temp[2] = data[0][i]['OPT_NAME_P1']===undefined? "-" : data[0][i]['OPT_NAME_P1'];
                            temp[3] = data[0][i]['OPT_AREA_SEQ_P1']===undefined? "-" : data[0][i]['OPT_AREA_SEQ_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];
                            
                            temp[6] = data[0][i]['LANDOFFICE_NAME_TH_P2']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P2'];
                            temp[7] = data[0][i]['OPT_NAME_P2']===undefined? "-" : data[0][i]['OPT_NAME_P2'];
                            temp[8] = data[0][i]['OPT_AREA_SEQ_P2']===undefined? "-" : data[0][i]['OPT_AREA_SEQ_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-14':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['FINANCE_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['FINANCE_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['FINANCE_TYPE_NAME_P1']===undefined? "-" : data[0][i]['FINANCE_TYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['FINANCE_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['FINANCE_TYPE_SEQ_P2'];
                            temp[6] = data[0][i]['FINANCE_TYPE_NAME_P2']===undefined? "-" : data[0][i]['FINANCE_TYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-15':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['COURT_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['COURT_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['COURT_TYPE_ID_P1']===undefined? "-" : data[0][i]['COURT_TYPE_ID_P1'];
                            temp[3] = data[0][i]['COURT_TYPE_NAME_P1']===undefined? "-" : data[0][i]['COURT_TYPE_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['COURT_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['COURT_TYPE_SEQ_P2'];
                            temp[7] = data[0][i]['COURT_TYPE_ID_P2']===undefined? "-" : data[0][i]['COURT_TYPE_ID_P2'];
                            temp[8] = data[0][i]['COURT_TYPE_NAME_P2']===undefined? "-" : data[0][i]['COURT_TYPE_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-16':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['NATIONALITY_SEQ_P1']===undefined? "-" : data[0][i]['NATIONALITY_SEQ_P1'];
                            temp[2] = data[0][i]['NATIONALITY_ID_P1']===undefined? "-" : data[0][i]['NATIONALITY_ID_P1'];
                            temp[3] = data[0][i]['NATIONALITY_NAME_P1']===undefined? "-" : data[0][i]['NATIONALITY_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['NATIONALITY_SEQ_P2']===undefined? "-" : data[0][i]['NATIONALITY_SEQ_P2'];
                            temp[7] = data[0][i]['NATIONALITY_ID_P2']===undefined? "-" : data[0][i]['NATIONALITY_ID_P2'];
                            temp[8] = data[0][i]['NATIONALITY_NAME_P2']===undefined? "-" : data[0][i]['NATIONALITY_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-17':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['RACE_SEQ_P1']===undefined? "-" : data[0][i]['RACE_SEQ_P1'];
                            temp[2] = data[0][i]['RACE_ID_P1']===undefined? "-" : data[0][i]['RACE_ID_P1'];
                            temp[3] = data[0][i]['RACE_NAME_P1']===undefined? "-" : data[0][i]['RACE_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['RACE_SEQ_P2']===undefined? "-" : data[0][i]['RACE_SEQ_P2'];
                            temp[7] = data[0][i]['RACE_ID_P2']===undefined? "-" : data[0][i]['RACE_ID_P2'];
                            temp[8] = data[0][i]['RACE_NAME_P2']===undefined? "-" : data[0][i]['RACE_NAME_P1'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-18':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['RELIGION_SEQ_P1']===undefined? "-" : data[0][i]['RELIGION_SEQ_P1'];
                            temp[2] = data[0][i]['RELIGION_ID_P1']===undefined? "-" : data[0][i]['RELIGION_ID_P1'];
                            temp[3] = data[0][i]['RELIGION_NAME_P1']===undefined? "-" : data[0][i]['RELIGION_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['RELIGION_SEQ_P2']===undefined? "-" : data[0][i]['RELIGION_SEQ_P2'];
                            temp[7] = data[0][i]['RELIGION_ID_P2']===undefined? "-" : data[0][i]['RELIGION_ID_P2'];
                            temp[8] = data[0][i]['RELIGION_NAME_P2']===undefined? "-" : data[0][i]['RELIGION_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-19':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['UNIT_SEQ_P1']===undefined? "-" : data[0][i]['UNIT_SEQ_P1'];
                            temp[2] = data[0][i]['UNIT_NAME_P1']===undefined? "-" : data[0][i]['UNIT_NAME_P1'];
                            temp[3] = data[0][i]['UNIT_ABBR_P1']===undefined? "-" : data[0][i]['UNIT_ABBR_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['UNIT_SEQ_P2']===undefined? "-" : data[0][i]['UNIT_SEQ_P2'];
                            temp[7] = data[0][i]['UNIT_NAME_P2']===undefined? "-" : data[0][i]['UNIT_NAME_P2'];
                            temp[8] = data[0][i]['UNIT_ABBR_P2']===undefined? "-" : data[0][i]['UNIT_ABBR_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-20':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['COUNTRY_GROUP_SEQ_P1']===undefined? "-" : data[0][i]['COUNTRY_GROUP_SEQ_P1'];
                            temp[2] = data[0][i]['COUNTRY_GROUP_ID_P1']===undefined? "-" : data[0][i]['COUNTRY_GROUP_ID_P1'];
                            temp[3] = data[0][i]['COUNTRY_GROUP_NAME_P1']===undefined? "-" : data[0][i]['COUNTRY_GROUP_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['COUNTRY_GROUP_SEQ_P2']===undefined? "-" : data[0][i]['COUNTRY_GROUP_SEQ_P2'];
                            temp[7] = data[0][i]['COUNTRY_GROUP_ID_P2']===undefined? "-" : data[0][i]['COUNTRY_GROUP_ID_P2'];
                            temp[8] = data[0][i]['COUNTRY_GROUP_NAME_P2']===undefined? "-" : data[0][i]['COUNTRY_GROUP_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-21':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['REGION_SEQ_P1']===undefined? "-" : data[0][i]['REGION_SEQ_P1'];
                            temp[2] = data[0][i]['REGION_ID_P1']===undefined? "-" : data[0][i]['REGION_ID_P1'];
                            temp[3] = data[0][i]['REGION_NAME_P1']===undefined? "-" : data[0][i]['REGION_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['REGION_SEQ_P2']===undefined? "-" : data[0][i]['REGION_SEQ_P2'];
                            temp[7] = data[0][i]['REGION_ID_P2']===undefined? "-" : data[0][i]['REGION_ID_P2'];
                            temp[8] = data[0][i]['REGION_NAME_P2']===undefined? "-" : data[0][i]['REGION_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-22':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['OFFICE_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['OFFICE_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['OFFICE_TYPE_ID_P1']===undefined? "-" : data[0][i]['OFFICE_TYPE_ID_P1'];
                            temp[3] = data[0][i]['OFFICE_TYPE_NAME_P1']===undefined? "-" : data[0][i]['OFFICE_TYPE_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['OFFICE_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['OFFICE_TYPE_SEQ_P2'];
                            temp[7] = data[0][i]['OFFICE_TYPE_ID_P2']===undefined? "-" : data[0][i]['OFFICE_TYPE_ID_P2'];
                            temp[8] = data[0][i]['OFFICE_TYPE_NAME_P2']===undefined? "-" : data[0][i]['OFFICE_TYPE_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-23':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['OFFICE_LEVEL_SEQ_P1']===undefined? "-" : data[0][i]['OFFICE_LEVEL_SEQ_P1'];
                            temp[2] = data[0][i]['OFFICE_LEVEL_ID_P1']===undefined? "-" : data[0][i]['OFFICE_LEVEL_ID_P1'];
                            temp[3] = data[0][i]['OFFICE_LEVEL_NAME_P1']===undefined? "-" : data[0][i]['OFFICE_LEVEL_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['OFFICE_LEVEL_SEQ_P2']===undefined? "-" : data[0][i]['OFFICE_LEVEL_SEQ_P2'];
                            temp[7] = data[0][i]['OFFICE_LEVEL_ID_P2']===undefined? "-" : data[0][i]['OFFICE_LEVEL_ID_P2'];
                            temp[8] = data[0][i]['OFFICE_LEVEL_NAME_P2']===undefined? "-" : data[0][i]['OFFICE_LEVEL_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-24':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LANDOFFICE_SEQ_P1']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P1'];
                            temp[2] = data[0][i]['LANDOFFICE_ID_P1']===undefined? "-" : data[0][i]['LANDOFFICE_ID_P1'];
                            temp[3] = data[0][i]['LANDOFFICE_NAME_TH_P1']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['LANDOFFICE_SEQ_P2']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P2'];
                            temp[7] = data[0][i]['LANDOFFICE_ID_P2']===undefined? "-" : data[0][i]['LANDOFFICE_ID_P2'];
                            temp[8] = data[0][i]['LANDOFFICE_NAME_TH_P2']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-25':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LANDOFFICE_SEQ_P1']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P1'];
                            temp[2] = data[0][i]['LANDOFFICE_ID_P1']===undefined? "-" : data[0][i]['LANDOFFICE_ID_P1'];
                            temp[3] = data[0][i]['LANDOFFICE_NAME_TH_P1']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['LANDOFFICE_SEQ_P2']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P2'];
                            temp[7] = data[0][i]['LANDOFFICE_ID_P2']===undefined? "-" : data[0][i]['LANDOFFICE_ID_P2'];
                            temp[8] = data[0][i]['LANDOFFICE_NAME_TH_P2']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-26':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LANDOFFICE_SEQ_P1']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P1'];
                            temp[2] = data[0][i]['LANDOFFICE_TYPE_NAME_P1']===undefined? "-" : data[0][i]['LANDOFFICE_TYPE_NAME_P1'];
                            temp[3] = data[0][i]['LANDOFFICE_NAME_TH_P1']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['LANDOFFICE_SEQ_P2']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P2'];
                            temp[7] = data[0][i]['LANDOFFICE_TYPE_NAME_P2']===undefined? "-" : data[0][i]['LANDOFFICE_TYPE_NAME_P2'];
                            temp[8] = data[0][i]['LANDOFFICE_NAME_TH_P2']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'mas-27':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['HOLIDAY_SEQ_P1']===undefined? "-" : data[0][i]['HOLIDAY_SEQ_P1'];
                            temp[2] = data[0][i]['HOLIDAY_DATE_P1']===undefined? "-" : data[0][i]['HOLIDAY_DATE_P1'];
                            temp[3] = data[0][i]['HOLIDAY_DESCRIPTION_P1']===undefined? "-" : data[0][i]['HOLIDAY_DESCRIPTION_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['HOLIDAY_SEQ_P2']===undefined? "-" : data[0][i]['HOLIDAY_SEQ_P2'];
                            temp[7] = data[0][i]['HOLIDAY_DATE_P2']===undefined? "-" : data[0][i]['HOLIDAY_DATE_P2'];
                            temp[8] = data[0][i]['HOLIDAY_DESCRIPTION_P2']===undefined? "-" : data[0][i]['HOLIDAY_DESCRIPTION_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;


                    case 'reg-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['OBTAIN_SEQ_P1']===undefined? "-" : data[0][i]['OBTAIN_SEQ_P1'];
                            temp[2] = data[0][i]['OBTAIN_NAME_P1']===undefined? "-" : data[0][i]['OBTAIN_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];
                            temp[5] = data[0][i]['OBTAIN_SEQ_P2']===undefined? "-" : data[0][i]['OBTAIN_SEQ_P2'];
                            temp[6] = data[0][i]['OBTAIN_NAME_P2']===undefined? "-" : data[0][i]['OBTAIN_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'reg-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['REGISTER_SEQ_P1']===undefined? "-" : data[0][i]['REGISTER_SEQ_P1'];
                            temp[2] = data[0][i]['REGISTER_ABBR_P1']===undefined? "-" : data[0][i]['REGISTER_ABBR_P1'];
                            temp[3] = data[0][i]['REGISTER_NAME_P1']===undefined? "-" : data[0][i]['REGISTER_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['REGISTER_SEQ_P2']===undefined? "-" : data[0][i]['REGISTER_SEQ_P2'];
                            temp[7] = data[0][i]['REGISTER_ABBR_P2']===undefined? "-" : data[0][i]['REGISTER_ABBR_P2'];
                            temp[8] = data[0][i]['REGISTER_NAME_P2']===undefined? "-" : data[0][i]['REGISTER_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'reg-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['BOOK_TYPE_SEQ_P1']===undefined? "-" : data[0][i]['BOOK_TYPE_SEQ_P1'];
                            temp[2] = data[0][i]['BOOK_TYPE_NAME_P1']===undefined? "-" : data[0][i]['BOOK_TYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['BOOK_TYPE_SEQ_P2']===undefined? "-" : data[0][i]['BOOK_TYPE_SEQ_P2'];
                            temp[6] = data[0][i]['BOOK_TYPE_NAME_P2']===undefined? "-" : data[0][i]['BOOK_TYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'reg-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['DOCUMENT_SEQ_P1']===undefined? "-" : data[0][i]['DOCUMENT_SEQ_P1'];
                            temp[2] = data[0][i]['DOCUMENT_ID_P1']===undefined? "-" : data[0][i]['DOCUMENT_ID_P1'];
                            temp[3] = data[0][i]['DOCUMENT_ABBR_P1']===undefined? "-" : data[0][i]['DOCUMENT_ABBR_P1'];
                            temp[4] = data[0][i]['DOCUMENT_NAME_P1']===undefined? "-" : data[0][i]['DOCUMENT_NAME_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            
                            temp[7] = data[0][i]['DOCUMENT_SEQ_P2']===undefined? "-" : data[0][i]['DOCUMENT_SEQ_P2'];
                            temp[8] = data[0][i]['DOCUMENT_ID_P2']===undefined? "-" : data[0][i]['DOCUMENT_ID_P2'];
                            temp[9] = data[0][i]['DOCUMENT_ABBR_P2']===undefined? "-" : data[0][i]['DOCUMENT_ABBR_P2'];
                            temp[10] = data[0][i]['DOCUMENT_NAME_P2']===undefined? "-" : data[0][i]['DOCUMENT_NAME_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'evd-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PROVINCE_ABBR_SEQ_P1']===undefined? "-" : data[0][i]['PROVINCE_ABBR_SEQ_P1'];
                            temp[2] = data[0][i]['PROVINCE_NAME_P1']===undefined? "-" : data[0][i]['PROVINCE_NAME_P1'];
                            temp[3] = data[0][i]['PROVINCE_ABBR_2TH_P1']===undefined? "-" : data[0][i]['PROVINCE_ABBR_2TH_P1'];
                            temp[4] = data[0][i]['PROVINCE_ABBR_2EN_P1']===undefined? "-" : data[0][i]['PROVINCE_ABBR_2EN_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['PROVINCE_ABBR_SEQ_P2']===undefined? "-" : data[0][i]['PROVINCE_ABBR_SEQ_P2'];
                            temp[8] = data[0][i]['PROVINCE_NAME_P2']===undefined? "-" : data[0][i]['PROVINCE_NAME_P2'];
                            temp[9] = data[0][i]['PROVINCE_ABBR_2TH_P2']===undefined? "-" : data[0][i]['PROVINCE_ABBR_2TH_P2'];
                            temp[10] = data[0][i]['PROVINCE_ABBR_2EN_P2']===undefined? "-" : data[0][i]['PROVINCE_ABBR_2EN_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'evd-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['IMAGE_STS_SEQ_P1']===undefined? "-" : data[0][i]['IMAGE_STS_SEQ_P1'];
                            temp[2] = data[0][i]['IMAGE_STS_ID_P1']===undefined? "-" : data[0][i]['IMAGE_STS_ID_P1'];
                            temp[3] = data[0][i]['IMAGE_STS_NAME_P1']===undefined? "-" : data[0][i]['IMAGE_STS_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['IMAGE_STS_SEQ_P2']===undefined? "-" : data[0][i]['IMAGE_STS_SEQ_P2'];
                            temp[7] = data[0][i]['IMAGE_STS_ID_P2']===undefined? "-" : data[0][i]['IMAGE_STS_ID_P2'];
                            temp[8] = data[0][i]['IMAGE_STS_NAME_P2']===undefined? "-" : data[0][i]['IMAGE_STS_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'evd-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['DOCTYPE_MAPPING_SEQ_P1']===undefined? "-" : data[0][i]['DOCTYPE_MAPPING_SEQ_P1'];
                            temp[2] = data[0][i]['DOCTYPE_MAPPING_ID_P1']===undefined? "-" : data[0][i]['DOCTYPE_MAPPING_ID_P1'];
                            temp[3] = data[0][i]['DOCTYPE_MAPPING_NAME_P1']===undefined? "-" : data[0][i]['DOCTYPE_MAPPING_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['DOCTYPE_MAPPING_SEQ_P2']===undefined? "-" : data[0][i]['DOCTYPE_MAPPING_SEQ_P2'];
                            temp[7] = data[0][i]['DOCTYPE_MAPPING_ID_P2']===undefined? "-" : data[0][i]['DOCTYPE_MAPPING_ID_P2'];
                            temp[8] = data[0][i]['DOCTYPE_MAPPING_NAME_P2']===undefined? "-" : data[0][i]['DOCTYPE_MAPPING_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'sva-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['TYPEOFSURVEY_SEQ_P1']===undefined? "-" : data[0][i]['TYPEOFSURVEY_SEQ_P1'];
                            temp[2] = data[0][i]['MAINTYPEOFSURVEY_NAME_P1']===undefined? "-" : data[0][i]['MAINTYPEOFSURVEY_NAME_P1'];
                            temp[3] = data[0][i]['TYPEOFSURVEY_NAME_P1']===undefined? "-" : data[0][i]['TYPEOFSURVEY_NAME_P1'];
                            temp[4] = data[0][i]['TYPEOFSURVEY_SHORTNAME_P1']===undefined? "-" : data[0][i]['TYPEOFSURVEY_SHORTNAME_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];
                            
                            temp[7] = data[0][i]['TYPEOFSURVEY_SEQ_P2']===undefined? "-" : data[0][i]['TYPEOFSURVEY_SEQ_P2'];
                            temp[8] = data[0][i]['MAINTYPEOFSURVEY_NAME_P2']===undefined? "-" : data[0][i]['MAINTYPEOFSURVEY_NAME_P2'];
                            temp[9] = data[0][i]['TYPEOFSURVEY_NAME_P2']===undefined? "-" : data[0][i]['TYPEOFSURVEY_NAME_P2'];
                            temp[10] = data[0][i]['TYPEOFSURVEY_SHORTNAME_P2']===undefined? "-" : data[0][i]['TYPEOFSURVEY_SHORTNAME_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'sva-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['SURVEYDOCTYPE_SEQ_P1']===undefined? "-" : data[0][i]['SURVEYDOCTYPE_SEQ_P1'];
                            temp[2] = data[0][i]['SURVEYDOCTYPE_NAME_P1']===undefined? "-" : data[0][i]['SURVEYDOCTYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['SURVEYDOCTYPE_SEQ_P2']===undefined? "-" : data[0][i]['SURVEYDOCTYPE_SEQ_P2'];
                            temp[6] = data[0][i]['SURVEYDOCTYPE_NAME_P2']===undefined? "-" : data[0][i]['SURVEYDOCTYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'sva-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['EQUIPMENTTYPE_SEQ_P1']===undefined? "-" : data[0][i]['EQUIPMENTTYPE_SEQ_P1'];
                            temp[2] = data[0][i]['EQUIPMENTTYPE_NAME_P1']===undefined? "-" : data[0][i]['EQUIPMENTTYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['EQUIPMENTTYPE_SEQ_P2']===undefined? "-" : data[0][i]['EQUIPMENTTYPE_SEQ_P2'];
                            temp[6] = data[0][i]['EQUIPMENTTYPE_NAME_P2']===undefined? "-" : data[0][i]['EQUIPMENTTYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'sva-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PRIVATE_OFFICE_SEQ_P1']===undefined? "-" : data[0][i]['PRIVATE_OFFICE_SEQ_P1'];
                            temp[2] = data[0][i]['PRIVATE_OFFICE_NAME_P1']===undefined? "-" : data[0][i]['PRIVATE_OFFICE_NAME_P1'];
                            temp[3] = data[0][i]['PRIVATE_OFFICE_CERTIFICATE_P1']===undefined? "-" : data[0][i]['PRIVATE_OFFICE_CERTIFICATE_P1'];
                            temp[4] = data[0][i]['PRIVATE_OFFICE_REGIST_DATE_P1']===undefined? "-" : data[0][i]['PRIVATE_OFFICE_REGIST_DATE_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['PRIVATE_OFFICE_SEQ_P2']===undefined? "-" : data[0][i]['PRIVATE_OFFICE_SEQ_P2'];
                            temp[8] = data[0][i]['PRIVATE_OFFICE_NAME_P2']===undefined? "-" : data[0][i]['PRIVATE_OFFICE_NAME_P2'];
                            temp[9] = data[0][i]['PRIVATE_OFFICE_CERTIFICATE_P2']===undefined? "-" : data[0][i]['PRIVATE_OFFICE_CERTIFICATE_P2'];
                            temp[10] = data[0][i]['PRIVATE_OFFICE_REGIST_DATE_P2']===undefined? "-" : data[0][i]['PRIVATE_OFFICE_REGIST_DATE_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'sva-5':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['SHEET_SEQ_P1']===undefined? "-" : data[0][i]['SHEET_SEQ_P1'];
                            temp[2] = data[0][i]['SHEETTYPE_NAME_P1']===undefined? "-" : data[0][i]['SHEETTYPE_NAME_P1'];
                            temp[3] = data[0][i]['UTM_P1']===undefined? "-" : data[0][i]['UTM_P1'];
                            temp[4] = data[0][i]['SHEET_UTMMAP4_P1']===undefined? "-" : data[0][i]['SHEET_UTMMAP4_P1'];
                            temp[5] = data[0][i]['SCALE_NAME_P1']===undefined? "-" : data[0][i]['SCALE_NAME_P1'];
                            temp[6] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[7] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[8] = data[0][i]['SHEET_SEQ_P2']===undefined? "-" : data[0][i]['SHEET_SEQ_P2'];
                            temp[9] = data[0][i]['SHEETTYPE_NAME_P2']===undefined? "-" : data[0][i]['SHEETTYPE_NAME_P2'];
                            temp[10] = data[0][i]['UTM_P2']===undefined? "-" : data[0][i]['UTM_P2'];
                            temp[11] = data[0][i]['SHEET_UTMMAP4_P2']===undefined? "-" : data[0][i]['SHEET_UTMMAP4_P2'];
                            temp[12] = data[0][i]['SCALE_NAME_P2']===undefined? "-" : data[0][i]['SCALE_NAME_P2'];
                            temp[13] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[14] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'sva-6':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['JOBSTATUS_SEQ_P1']===undefined? "-" : data[0][i]['JOBSTATUS_SEQ_P1'];
                            temp[2] = data[0][i]['JOBSTATUS_NAME_P1']===undefined? "-" : data[0][i]['JOBSTATUS_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['JOBSTATUS_SEQ_P2']===undefined? "-" : data[0][i]['JOBSTATUS_SEQ_P2'];
                            temp[6] = data[0][i]['JOBSTATUS_NAME_P2']===undefined? "-" : data[0][i]['JOBSTATUS_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'sva-7':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LETTERTYPE_SEQ_P1']===undefined? "-" : data[0][i]['LETTERTYPE_SEQ_P1'];
                            temp[2] = data[0][i]['LETTERTYPE_NAME_P1']===undefined? "-" : data[0][i]['LETTERTYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['LETTERTYPE_SEQ_P2']===undefined? "-" : data[0][i]['LETTERTYPE_SEQ_P2'];
                            temp[6] = data[0][i]['LETTERTYPE_NAME_P2']===undefined? "-" : data[0][i]['LETTERTYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'sva-8':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['JOBGROUP_SEQ_P1']===undefined? "-" : data[0][i]['JOBGROUP_SEQ_P1'];
                            temp[2] = data[0][i]['JOBGROUPNAME_P1']===undefined? "-" : data[0][i]['JOBGROUPNAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['JOBGROUP_SEQ_P2']===undefined? "-" : data[0][i]['JOBGROUP_SEQ_P2'];
                            temp[6] = data[0][i]['JOBGROUPNAME_P2']===undefined? "-" : data[0][i]['JOBGROUPNAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'sva-9':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['GROUPTYPE_SEQ_P1']===undefined? "-" : data[0][i]['GROUPTYPE_SEQ_P1'];
                            temp[2] = data[0][i]['GROUPTYPE_NAME_P1']===undefined? "-" : data[0][i]['GROUPTYPE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['GROUPTYPE_SEQ_P2']===undefined? "-" : data[0][i]['GROUPTYPE_SEQ_P2'];
                            temp[6] = data[0][i]['GROUPTYPE_NAME_P2']===undefined? "-" : data[0][i]['GROUPTYPE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'svc-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['TV_ZONE_P1']===undefined? "-" : data[0][i]['TV_ZONE_P1'];
                            temp[2] = data[0][i]['TV_PROV_P1']===undefined? "-" : data[0][i]['TV_PROV_P1'];
                            temp[3] = data[0][i]['TV_NAME_P1']===undefined? "-" : data[0][i]['TV_NAME_P1'];
                            temp[4] = data[0][i]['TVC_CTRLNAME_P1']===undefined? "-" : data[0][i]['TVC_CTRLNAME_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : convertDtm(data[0][i]['CREATE_DTM_P1']);
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : convertDtm(data[0][i]['LAST_UPD_DTM_P1']);

                            temp[7] = data[0][i]['TV_ZONE_P2']===undefined? "-" : data[0][i]['TV_ZONE_P2'];
                            temp[8] = data[0][i]['TV_PROV_P2']===undefined? "-" : data[0][i]['TV_PROV_P2'];
                            temp[9] = data[0][i]['TV_NAME_P2']===undefined? "-" : data[0][i]['TV_NAME_P2'];
                            temp[10] = data[0][i]['TVC_CTRLNAME_P2']===undefined? "-" : data[0][i]['TVC_CTRLNAME_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : convertDtm(data[0][i]['CREATE_DTM_P2']);
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : convertDtm(data[0][i]['LAST_UPD_DTM_P2']);
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'svc-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['GPSMARK_SEQ_P1']===undefined? "-" : data[0][i]['GPSMARK_SEQ_P1'];
                            temp[2] = data[0][i]['GM_ZONE_P1']===undefined? "-" : data[0][i]['GM_ZONE_P1'];
                            temp[3] = data[0][i]['GM_NAME_P1']===undefined? "-" : data[0][i]['GM_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : convertDtm(data[0][i]['CREATE_DTM_P1']);
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : convertDtm(data[0][i]['LAST_UPD_DTM_P1']);

                            temp[6] = data[0][i]['GPSMARK_SEQ_P2']===undefined? "-" : data[0][i]['GPSMARK_SEQ_P2'];
                            temp[7] = data[0][i]['GM_ZONE_P2']===undefined? "-" : data[0][i]['GM_ZONE_P2'];
                            temp[8] = data[0][i]['GM_NAME_P2']===undefined? "-" : data[0][i]['GM_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : convertDtm(data[0][i]['CREATE_DTM_P2']);
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : convertDtm(data[0][i]['LAST_UPD_DTM_P2']);
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'svc-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['INDEX_SEQ_P1']===undefined? "-" : data[0][i]['INDEX_SEQ_P1'];
                            temp[2] = data[0][i]['INDEX_NAME_P1']===undefined? "-" : data[0][i]['INDEX_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['INDEX_SEQ_P2']===undefined? "-" : data[0][i]['INDEX_SEQ_P2'];
                            temp[6] = data[0][i]['INDEX_NAME_P2']===undefined? "-" : data[0][i]['INDEX_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'svc-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['INDEX_SEQ_P1']===undefined? "-" : data[0][i]['INDEX_SEQ_P1'];
                            temp[2] = data[0][i]['INDEX_NAME_P1']===undefined? "-" : data[0][i]['INDEX_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['INDEX_SEQ_P2']===undefined? "-" : data[0][i]['INDEX_SEQ_P2'];
                            temp[6] = data[0][i]['INDEX_NAME_P2']===undefined? "-" : data[0][i]['INDEX_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'svc-5':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['TV_ZONE_P1']===undefined? "-" : data[0][i]['TV_ZONE_P1'];
                            temp[2] = data[0][i]['TV_PROV_P1']===undefined? "-" : data[0][i]['TV_PROV_P1'];
                            temp[3] = data[0][i]['TV_NAME_P1']===undefined? "-" : data[0][i]['TV_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['TV_ZONE_P2']===undefined? "-" : data[0][i]['TV_ZONE_P2'];
                            temp[7] = data[0][i]['TV_PROV_P2']===undefined? "-" : data[0][i]['TV_PROV_P2'];
                            temp[8] = data[0][i]['TV_NAME_P2']===undefined? "-" : data[0][i]['TV_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'usp-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['TYPE_ASSET_SEQ_P1']===undefined? "-" : data[0][i]['TYPE_ASSET_SEQ_P1'];
                            temp[2] = data[0][i]['TYPE_ASSET_ID_P1']===undefined? "-" : data[0][i]['TYPE_ASSET_ID_P1'];
                            temp[3] = data[0][i]['TYPE_ASSET_NAME_P1']===undefined? "-" : data[0][i]['TYPE_ASSET_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['TYPE_ASSET_SEQ_P2']===undefined? "-" : data[0][i]['TYPE_ASSET_SEQ_P2'];
                            temp[7] = data[0][i]['TYPE_ASSET_ID_P2']===undefined? "-" : data[0][i]['TYPE_ASSET_ID_P2'];
                            temp[8] = data[0][i]['TYPE_ASSET_NAME_P2']===undefined? "-" : data[0][i]['TYPE_ASSET_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'usp-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['MAT_SEQ_P1']===undefined? "-" : data[0][i]['MAT_SEQ_P1'];
                            temp[2] = data[0][i]['MAT_NAME_P1']===undefined? "-" : data[0][i]['MAT_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['MAT_SEQ_P2']===undefined? "-" : data[0][i]['MAT_SEQ_P2'];
                            temp[6] = data[0][i]['MAT_NAME_P2']===undefined? "-" : data[0][i]['MAT_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'usp-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PLATE_SEQ_P1']===undefined? "-" : data[0][i]['PLATE_SEQ_P1'];
                            temp[2] = data[0][i]['PLATE_ABBR_NAME_P1']===undefined? "-" : data[0][i]['PLATE_ABBR_NAME_P1'];
                            temp[3] = data[0][i]['PLATE_NAME_P1']===undefined? "-" : data[0][i]['PLATE_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['PLATE_SEQ_P2']===undefined? "-" : data[0][i]['PLATE_SEQ_P2'];
                            temp[7] = data[0][i]['PLATE_ABBR_NAME_P2']===undefined? "-" : data[0][i]['PLATE_ABBR_NAME_P2'];
                            temp[8] = data[0][i]['PLATE_NAME_P2']===undefined? "-" : data[0][i]['PLATE_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'usp-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['PIECE_SEQ_P1']===undefined? "-" : data[0][i]['PIECE_SEQ_P1'];
                            temp[2] = data[0][i]['PIECE_NAME_P1']===undefined? "-" : data[0][i]['PIECE_NAME_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['PIECE_SEQ_P2']===undefined? "-" : data[0][i]['PIECE_SEQ_P2'];
                            temp[6] = data[0][i]['PIECE_NAME_P2']===undefined? "-" : data[0][i]['PIECE_NAME_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'usp-5':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['BLD_SEQ_P1']===undefined? "-" : data[0][i]['BLD_SEQ_P1'];
                            temp[2] = data[0][i]['BLD_NAME_P1']===undefined? "-" : data[0][i]['BLD_NAME_P1'];
                            temp[3] = data[0][i]['BLD_YEAR_P1']===undefined? "-" : data[0][i]['BLD_YEAR_P1'];
                            temp[4] = data[0][i]['BLD_RATE_P1']===undefined? "-" : data[0][i]['BLD_RATE_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['BLD_SEQ_P2']===undefined? "-" : data[0][i]['BLD_SEQ_P2'];
                            temp[8] = data[0][i]['BLD_NAME_P2']===undefined? "-" : data[0][i]['BLD_NAME_P2'];
                            temp[9] = data[0][i]['BLD_YEAR_P2']===undefined? "-" : data[0][i]['BLD_YEAR_P2'];
                            temp[10] = data[0][i]['BLD_RATE_P2']===undefined? "-" : data[0][i]['BLD_RATE_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'esp-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['DAY_SEQ_P1']===undefined? "-" : data[0][i]['DAY_SEQ_P1'];
                            temp[2] = data[0][i]['DAY_ID_P1']===undefined? "-" : data[0][i]['DAY_ID_P1'];
                            temp[3] = data[0][i]['DAY_FLAG_P1']===undefined? "-" : data[0][i]['DAY_FLAG_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['DAY_SEQ_P2']===undefined? "-" : data[0][i]['DAY_SEQ_P2'];
                            temp[7] = data[0][i]['DAY_ID_P2']===undefined? "-" : data[0][i]['DAY_ID_P2'];
                            temp[8] = data[0][i]['DAY_FLAG_P2']===undefined? "-" : data[0][i]['DAY_FLAG_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'esp-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['MOI_STATUS_SEQ_P1']===undefined? "-" : data[0][i]['MOI_STATUS_SEQ_P1'];
                            temp[2] = data[0][i]['MOI_STATUS_CODE_P1']===undefined? "-" : data[0][i]['MOI_STATUS_CODE_P1'];
                            temp[3] = data[0][i]['MOI_STATUS_DESC_P1']===undefined? "-" : data[0][i]['MOI_STATUS_DESC_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['MOI_STATUS_SEQ_P2']===undefined? "-" : data[0][i]['MOI_STATUS_SEQ_P2'];
                            temp[7] = data[0][i]['MOI_STATUS_CODE_P2']===undefined? "-" : data[0][i]['MOI_STATUS_CODE_P2'];
                            temp[8] = data[0][i]['MOI_STATUS_DESC_P2']===undefined? "-" : data[0][i]['MOI_STATUS_DESC_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'esp-3':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['MONTH_SEQ_P1']===undefined? "-" : data[0][i]['MONTH_SEQ_P1'];
                            temp[2] = data[0][i]['MONTH_ID_P1']===undefined? "-" : data[0][i]['MONTH_ID_P1'];
                            temp[3] = data[0][i]['MONTH_NAME_TH_P1']===undefined? "-" : data[0][i]['MONTH_NAME_TH_P1'];
                            temp[4] = data[0][i]['MONTH_NAME_EN_P1']===undefined? "-" : data[0][i]['MONTH_NAME_EN_P1'];
                            temp[5] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[6] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[7] = data[0][i]['MONTH_SEQ_P2']===undefined? "-" : data[0][i]['MONTH_SEQ_P2'];
                            temp[8] = data[0][i]['MONTH_ID_P2']===undefined? "-" : data[0][i]['MONTH_ID_P2'];
                            temp[9] = data[0][i]['MONTH_NAME_TH_P2']===undefined? "-" : data[0][i]['MONTH_NAME_TH_P2'];
                            temp[10] = data[0][i]['MONTH_NAME_EN_P2']===undefined? "-" : data[0][i]['MONTH_NAME_EN_P2'];
                            temp[11] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[12] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'esp-4':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['TIME_RANG_SEQ_P1']===undefined? "-" : data[0][i]['TIME_RANG_SEQ_P1'];
                            temp[2] = data[0][i]['TIME_RANG_ID_P1']===undefined? "-" : data[0][i]['TIME_RANG_ID_P1'];
                            temp[3] = data[0][i]['TIME_RANG_NAME_P1']===undefined? "-" : data[0][i]['TIME_RANG_NAME_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['TIME_RANG_SEQ_P2']===undefined? "-" : data[0][i]['TIME_RANG_SEQ_P2'];
                            temp[7] = data[0][i]['TIME_RANG_ID_P2']===undefined? "-" : data[0][i]['TIME_RANG_ID_P2'];
                            temp[8] = data[0][i]['TIME_RANG_NAME_P2']===undefined? "-" : data[0][i]['TIME_RANG_NAME_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;

                    case 'ech-1':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['MOU_SEQ_P1']===undefined? "-" : data[0][i]['MOU_SEQ_P1'];
                            temp[2] = data[0][i]['MOU_NO_P1']===undefined? "-" : data[0][i]['MOU_NO_P1'];
                            temp[3] = data[0][i]['MOU_SUBJECT_P1']===undefined? "-" : data[0][i]['MOU_SUBJECT_P1'];
                            temp[4] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[5] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[6] = data[0][i]['MOU_SEQ_P2']===undefined? "-" : data[0][i]['MOU_SEQ_P2'];
                            temp[7] = data[0][i]['MOU_NO_P2']===undefined? "-" : data[0][i]['MOU_NO_P2'];
                            temp[8] = data[0][i]['MOU_SUBJECT_P2']===undefined? "-" : data[0][i]['MOU_SUBJECT_P2'];
                            temp[9] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[10] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                    case 'ech-2':
                        console.log(sysType+"-"+type);
                        for (let i = 0; i < data[0].length; i++){
                            temp = [];
                            temp[0] = num+1;
                            temp[1] = data[0][i]['LANDOFFICE_SEQ_P1']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P1'];
                            temp[2] = data[0][i]['LANDOFFICE_NAME_TH_P1']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P1'];
                            temp[3] = data[0][i]['CREATE_DTM_P1']===undefined? "-" : data[0][i]['CREATE_DTM_P1'];
                            temp[4] = data[0][i]['LAST_UPD_DTM_P1']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P1'];

                            temp[5] = data[0][i]['LANDOFFICE_SEQ_P2']===undefined? "-" : data[0][i]['LANDOFFICE_SEQ_P2'];
                            temp[6] = data[0][i]['LANDOFFICE_NAME_TH_P2']===undefined? "-" : data[0][i]['LANDOFFICE_NAME_TH_P2'];
                            temp[7] = data[0][i]['CREATE_DTM_P2']===undefined? "-" : data[0][i]['CREATE_DTM_P2'];
                            temp[8] = data[0][i]['LAST_UPD_DTM_P2']===undefined? "-" : data[0][i]['LAST_UPD_DTM_P2'];
                            table.row.add(temp);
                            num++;
                        }
                    break;
                }


            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('.overlay-main').hide();

        }
    });
}

function getSummary(type){
    $('.overlay-main').show();
    $.ajax({
        type : "POST",
        dataType : "json",
        url : "./queries/mas/getMasterSum.php",
        data : '&type=' + type,
        success: function(data){
            console.log(data);
            datatemp = data;    
            var table = $("#table-sum-"+type).DataTable({
                "language": {
                    "emptyTable": "ไม่มีข้อมูล"
                },
                "columnDefs" : [
                    {
                        "searchable" : false,
                        "orderable" : false,
                        "targets" : 0
                    }
                    ,{"className": "dt-center", "targets": [ 2, 3, 4]}
                    // ,{"className": "dt-left", "targets": [ 4]}
                ],
                "deferRender" : true,
                "processing" : true,
                "lengthChange": false,
                "ordering" : false,
                "searching" : false,
                "paging" : false,
                "info" : false,
                "drawCallback": function () {
                    currentPageCount = this.api().rows({
                        page: 'current'
                    }).data().length;
                }
                    });

            if(!jQuery.isEmptyObject(data)){
                table.search("").draw();
                table.clear();
                num = 0;
                for (let i = 0; i < data[0].length; i++){
                    temp = [];
                    temp[0] = data[0][i]['NUM']===undefined? "-" : data[0][i]['NUM'];
                    temp[1] = data[0][i]['TYPE']===undefined? "-" : data[0][i]['TYPE'];
                    temp[2] = Number(data[0][i]['P1']).toLocaleString()===undefined? "-" : Number(data[0][i]['P1']).toLocaleString();
                    temp[3] = Number(data[0][i]['P2']).toLocaleString()===undefined? "-" : Number(data[0][i]['P2']).toLocaleString();
                    if(data[0][i]['AP2RECEIVE'] !=0 && data[0][i]['DP2RECEIVE'] !=0){
                        temp[4] = '-มีข้อมูลในพัฒน์ฯ 1 ไม่มีข้อมูลในพัฒน์ฯ 2: ' + Number(data[0][i]['DP2RECEIVE']).toLocaleString() + '<br>' +
                                  '-มีข้อมูลในพัฒน์ฯ 2 ไม่มีข้อมูลในพัฒน์ฯ 1: ' + Number(data[0][i]['AP2RECEIVE']).toLocaleString();

                    }
                    else if(data[0][i]['AP2RECEIVE'] !='0') temp[4] = '-มีข้อมูลในพัฒน์ฯ 2 ไม่มีข้อมูลในพัฒน์ฯ 1: ' + Number(data[0][i]['AP2RECEIVE']).toLocaleString();
                    else if(data[0][i]['DP2RECEIVE'] !='0') temp[4] = '-มีข้อมูลในพัฒน์ฯ 1 ไม่มีข้อมูลในพัฒน์ฯ 2: ' + Number(data[0][i]['DP2RECEIVE']).toLocaleString();
                    else  temp[4] = '-';

                     
                    // temp[4] = data[0][i]['REMARK']===undefined? "-" : data[0][i]['REMARK'];
                    // temp[4] = 'เพิ่ม: ' + data[0][i]['A']===undefined? "-" : data[0][i]['A'] + 'ลบ: ' + data[0][i]['D']===undefined? "-" : data[0][i]['D'] ;
                    // temp[4] = data[0][i]['DIFF_P1_P2']===undefined? "-" : data[0][i]['DIFF_P1_P2'];
                    // temp[5] = data[0][i]['DIFF_P2_P1']===undefined? "-" : data[0][i]['DIFF_P2_P1'];
                    table.row.add(temp);
                    // num++;
                }
            } else {
                table.search("").draw();
                table.clear();
            }
            table.draw();
            $('.overlay-main').hide();
        }
    })
}
