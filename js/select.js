$(document).ready(function(){
    getProvince();

    $('#select-province').on('change', function(){
        provinceSeq = this.value;
        if(provinceSeq!=''){
            getLandoffice(provinceSeq);
        }
    })
})

function getProvince(){
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: './queries/getProvince.php',
        success: function(data){
            $('#select-province').empty();
            option = '';
            option = '<option value="">เลือกจังหวัด</option>'
            if(!jQuery.isEmptyObject(data)){
                $.each(data, function(key, val){
                    option += '<option value="' + val['PROVINCE_SEQ'] + '">' + val['PROVINCE_NAME'] + '</option>';
                });
            }
            $('#select-province').append(option);
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getLandoffice(n){
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: './queries/getLandoffice.php',
        data: 'province=' + n,
        success: function(data){
            $('#select-branch').empty();
            option = '';
            option = '<option value="">เลือกสำนักงาน</option>'
            if(!jQuery.isEmptyObject(data)){
                $.each(data, function(key, val){
                    option += '<option value="' + val['LANDOFFICE_SEQ'] + ',' + val['RECV_DATE'] + '">' + val['LANDOFFICE_NAME_TH'] + '</option>';
                });
            }
            $('#select-branch').append(option);
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
}

function getAmphur(amphurId, landoffice) {
    console.log(amphurId);
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getAmphur.php',
        data: 'landoffice=' + landoffice,
        success: function (data) {
            $(amphurId).empty();
            option = '';
            option += '<option value="" selected>อำเภอทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['AMPHUR_SEQ'] + '">' + val['AMPHUR_NAME'] + " (" + val['AMPHUR_SEQ'] + ")" + '</option>';
                });
            }
            $(amphurId).append(option);
        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    
}

function getTambon(tambonId, landoffice, amphur) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getTambon.php',
        data: 'landoffice=' + landoffice + '&amphur=' + amphur,
        success: function (data) {
            $(tambonId).empty();
            option = '';
            option += '<option value="" selected>ตำบลทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['TAMBOL_SEQ'] + '">' + val['TAMBOL_NAME'] + '</option>';
                });
            }
            $(tambonId).append(option);
        }
    })
}

function getSecretType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=secret',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['DOC_SECRET_SEQ'] + '">' + val['DOC_SECRET_NAME'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}

function getBuildingType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=bld',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['BLD_SEQ'] + '">' + val['BLD_NAME'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}

function getINVType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=inv',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['TOPIC_ASSET_SEQ'] + '">' + val['TOPIC_ASSET_NAME'] + '</option>';
                });
                option += '<option value="null">' + 'ไม่มีชื่อหมวดครุภัณฑ์' + '</option>';
            }
            $(id).append(option);
        }
    })
}

function getDocumentFinanceType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=doc_fin',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['PLATE_SEQ'] + '">' + val['PLATE_NAME'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}

function getDocumentGeneralType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=doc',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['PLATE_SEQ'] + '">' + val['PLATE_NAME'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}

function getDocumentParcelType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=doc_parcel',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['PLATE_SEQ'] + '">' + val['PLATE_NAME'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}

function getMaterialType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=mat',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['MAT_SEQ'] + '">' + val['MAT_NAME'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}

function getDistrictType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=district',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['PIECE_SEQ'] + '">' + val['PIECE_NAME'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}

function getLandofficeType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=landoffice_type',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['PSN_TYPE_SEQ'] + '">' + val['PSN_TYPE_NAME'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}

function getJobStatus(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=jobstatus',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['JOBSTATUS_SEQ'] + '">' + val['JOBSTATUS_NAME'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}

function getScaleType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=scale',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['SCALE_SEQ'] + '">' + val['SCALE_NAME'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}

function getParcelType(id) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './queries/getDropdownType.php',
        data: 'type=parceltype',
        success: function (data) {
            $(id).empty();
            option = '';
            option += '<option value="" selected>เลือกทั้งหมด</option>';
            if (!jQuery.isEmptyObject(data)) {
                $.each(data, function (key, val) {
                    option += '<option value="' + val['PARCELTYPE_SEQ'] + '">' + val['PARCELTYPE_DESC'] + '</option>';
                });
            }
            $(id).append(option);
        }
    })
}