var searchFunction = function (e) {
    $('.overlay-main').show();

    var table = $('#table-sva-' + type).DataTable({
        "pageLength": 10,
        "pagingType": "simple_numbers",
        "searching": false,
        "ordering": false,
        orderCellsTop: true,
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs": [{
            "className": "text-center",
            "targets": "_all",

        }, {
            "className": "text-center",
            "targets": "_all",

        }],
        'info': true,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    })
    table.clear();
    table.draw();

    if (type.includes("local")) {
        getLandofficeLocalData(table, e);
    }

    else if (type.includes("calculate")) {
        getCalculateData(table, e);
    }
    else {
        getDigitalMapData(table, e);
    }
    $('#table-sva-' + type + ' tbody').on('click', 'tr', function () {
        let table = $('#table-sva-' + type).DataTable();
        table.$('tr.row-click').removeClass('row-click');
        $(this).addClass('row-click');
    });


}
function showDetail(e) {
    var tr = e.parent().closest('tr');
    var row = $('#table-sva-' + type).DataTable().row(tr);


    console.log('#tabPanel-' + type);
    $('#tabPanel-' + type).css("visibility", "visible");

    $('.detailTab-' + type + ' li:first-child a ').click();

    $('.overlayP2').show();


    var dataUrl = 'landoffice=' + landoffice;
    if (svaType > 0) {
        if (row.data()[6] != "") dataUrl += '&surveyjobSeqP1=' + row.data()[6];
        if (row.data()[7] != "") dataUrl += '&surveyjobSeqP2=' + row.data()[7];
        if (row.data()[8] != "") dataUrl += '&processSeqP1=' + row.data()[8];
        if (row.data()[9] != "") dataUrl += '&processSeqP2=' + row.data()[9];
    }
    else { //udm
        if (row.data()[7] != "") dataUrl += '&udmparcelSeqP1=' + row.data()[7];
        if (row.data()[8] != "") dataUrl += '&udmparcelSeqP2=' + row.data()[8];
    }


    if (type.includes("local")) {
        dataUrl += '&check=local';
    }

    else if (type.includes("calculate")) {
        dataUrl += '&check=calculate';
    }
    else {
        dataUrl += '&check=digital_map';
    }



    console.log(row.data());

    if (svaType > 0) {
        //=================================================== Parcel Detail ===================================================
        if (type.includes("local")) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: './sva/getSvaDetailData.php',
                data: dataUrl,
                success: function (data) {
                    var num = 0, dataSet = [];
                    console.log(data);
                    var count = data[0].length > 0 ? Object.keys(data[0][0]).length : 0;
                    var loop = dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP2") ? 2 : 1;
                    console.log("detailData = ", data);
                    let column = {
                        'SURVEYJOB_NO': 'เลขลำดับ',
                        'TYPEOFSURVEY_NAME': 'ประเภทการรังวัด',
                        'REQ_QUEUE_NO': 'คำขอหรือหนังสือศาล ฉบับที่',
                        'SURVEYJOB_DATE': 'ลงวันที่',
                        'PROVINCE_NAME': 'จังหวัด',
                        'TAMBOL_NAME': 'ตำบล',
                        'AMPHUR_NAME': 'อำเภอ',
                        'SURVEYOR_NAME': 'ชื่อช่างรังวัด',
                        'APPOINTMENT_DTM': 'วันที่นัดรังวัด',
                        'PRINTPLATE_TYPE_NAME': 'ประเภทเอกสารสิทธิ',
                        'LANDSURVEY_PARCEL_NO': 'เลขที่เอกสารสิทธิ',
                        'UTM': 'ระวาง',
                        'LANDSURVEY_LAND_NO': 'เลขที่ดิน',
                        'LANDSURVEY_SURVEY_NO': 'หน้าสำรวจ',
                        'LANDSURVEY_MOO': 'หมู่',
                        'AREA': 'เนื้อที่',
                    };
                    for (let j = 0; j < data[0].length; j++) {
                        for (let i = 0; i < (Object.keys(column).length); i++) {
                            var key = Object.keys(column)[i];
                            temp = []
                            temp[0] = column[key]
                            if (key.includes("SCALE_SEQ")) {
                                var UTMP1, UTMP2;
                                utmScale.forEach(utm => {
                                    if (data[0][j][key + "_P1"] == utm.seq) UTMP1 = utm.value
                                    if (data[0][j][key + "_P2"] == utm.seq) UTMP2 = utm.value
                                })

                                temp[1] = UTMP1 ? UTMP1 : '-';
                                temp[2] = UTMP2 ? UTMP2 : '-';
                            }
                            else {
                                temp[1] = data[0][j][key + "_P1"] ? data[0][j][key + "_P1"] : "-";
                                temp[2] = data[0][j][key + "_P2"] ? data[0][j][key + "_P2"] : "-";
                            }

                            if (!dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP2")) {
                                temp[1] = "-";
                            }

                            if (dataUrl.includes("surveyjobSeqP1") && !dataUrl.includes("surveyjobSeqP2")) {
                                temp[2] = "-";
                            }
                            dataSet[num] = temp;
                            num++;

                        }
                    }

                    var parcel_detailTab = $('#parcel-detailTab-' + type + '-table').DataTable({
                        "searching": false,
                        "ordering": false,
                        orderCellsTop: true,
                        "language": {
                            "emptyTable": "ไม่มีข้อมูลรายละเอียดแปลงที่ดิน"
                        },
                        "createdRow": function (row, data, index) {
                            if (data[0].includes("ลำดับหมุดหลักเขตในแปลงที่ดิน") || data[0].includes("เลขลำดับ")) {
                                $('td', row).css('background-color', 'rgb(236,236,236)');
                            }
                        },
                        "bPaginate": false,
                        'info': false,
                        "columnDefs": [{
                            "className": "dt-center",
                            "targets": [1, 2],

                        }, {
                            "className": "dt-left",
                            "targets": [0],

                        }],
                        "drawCallback": function () {
                            currentPageCount = this.api().rows({
                                page: 'current'
                            }).data().length;
                        }
                    });

                    parcel_detailTab.search('').draw();
                    parcel_detailTab.clear();
                    parcel_detailTab.rows.add(dataSet);
                    parcel_detailTab.draw();



                }
            }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }
        else {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: './sva/getSvaDetailData.php',
                data: dataUrl,
                success: function (data) {
                    var num = 0, dataSet = [];
                    console.log(data);
                    var count = data[0].length > 0 ? Object.keys(data[0][0]).length : 0;
                    var loop = dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP2") ? 2 : 1;
                    console.log("detailData = ", data);
                    for (let j = 0; j < data[0].length; j++) {
                        for (let i = 0; i < (Object.keys(columnSVC_parcelDescDetailMain).length / 2); i++) {
                            var key = Object.keys(columnSVC_parcelDescDetailMain)[i];

                            temp = []
                            temp[0] = columnSVC_parcelDescDetailMain[key]
                            if (key.includes("SCALE_SEQ")) {
                                var UTMP1, UTMP2;
                                utmScale.forEach(utm => {
                                    if (data[0][j]['SCALE_SEQ_P1'] == utm.seq) UTMP1 = utm.value
                                    if (data[0][j]['SCALE_SEQ_P2'] == utm.seq) UTMP2 = utm.value
                                })

                                temp[1] = UTMP1;
                                temp[2] = UTMP2;
                            }
                            else {
                                temp[1] = data[0][j][key] ? data[0][j][key] : "-";
                                temp[2] = data[0][j][key.replace("P1", "P2")] ? data[0][j][key.replace("P1", "P2")] : "-";
                            }

                            if (!dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP2")) {
                                temp[1] = "-";
                            }

                            if (dataUrl.includes("surveyjobSeqP1") && !dataUrl.includes("surveyjobSeqP2")) {
                                temp[2] = "-";
                            }
                            dataSet[num] = temp;
                            num++;

                        }
                    }

                    var parcel_detailTab = $('#parcel-detailTab-' + type + '-table').DataTable({
                        "searching": false,
                        "ordering": false,
                        orderCellsTop: true,
                        "language": {
                            "emptyTable": "ไม่มีข้อมูลรายละเอียดแปลงที่ดิน"
                        },
                        "createdRow": function (row, data, index) {
                            if (data[0].includes("ลำดับหมุดหลักเขตในแปลงที่ดิน") || data[0].includes("เลขลำดับ")) {
                                $('td', row).css('background-color', 'rgb(236,236,236)');
                            }
                        },
                        "bPaginate": false,
                        'info': false,
                        "columnDefs": [{
                            "className": "dt-center",
                            "targets": [1, 2],

                        }, {
                            "className": "dt-left",
                            "targets": [0],

                        }],
                        "drawCallback": function () {
                            currentPageCount = this.api().rows({
                                page: 'current'
                            }).data().length;
                        }
                    });

                    parcel_detailTab.search('').draw();
                    parcel_detailTab.clear();
                    parcel_detailTab.rows.add(dataSet);
                    parcel_detailTab.draw();



                }
            }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }


        // ======================================== PROJECT =========================================
        if (type.includes("project")) {
            let tempUrl = '';

            tempUrl = dataUrl.includes("local") ? dataUrl.replace("&check=local", "&check=project") : dataUrl.replace("&check=calculate", "&check=project")
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: './sva/getSvaDetailData.php',
                data: tempUrl,
                success: function (data) {
                    var num = 0, dataSet = [];
                    console.log(data);
                    console.log("detailData = ", data);
                    for (let j = 0; j < data[0].length; j++) {
                        for (let i = 0; i < (Object.keys(columnSVO_projectDetailMain).length / 2); i++) {
                            var key = Object.keys(columnSVO_projectDetailMain)[i];

                            temp = []
                            temp[0] = columnSVO_projectDetailMain[key]
                            temp[1] = data[0][j][key] ? data[0][j][key] : "-";
                            temp[2] = data[0][j][key.replace("P1", "P2")] ? data[0][j][key.replace("P1", "P2")] : "-";


                            if (!dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP2")) {
                                temp[1] = "-";
                            }

                            if (dataUrl.includes("surveyjobSeqP1") && !dataUrl.includes("surveyjobSeqP2")) {
                                temp[2] = "-";
                            }
                            dataSet[num] = temp;
                            num++;

                        }
                    }

                    var project_detailTab = $('#project-detailTab-' + type + '-table').DataTable({
                        "searching": false,
                        "ordering": false,
                        orderCellsTop: true,
                        "language": {
                            "emptyTable": "ไม่มีข้อมูลโครงการ"
                        },
                        "bPaginate": false,
                        'info': false,
                        "columnDefs": [{
                            "className": "dt-center",
                            "targets": [1, 2],

                        }, {
                            "className": "dt-left",
                            "targets": [0],

                        }],

                        "drawCallback": function () {
                            currentPageCount = this.api().rows({
                                page: 'current'
                            }).data().length;
                        }
                    });

                    project_detailTab.search('').draw();
                    project_detailTab.clear();
                    project_detailTab.rows.add(dataSet);
                    project_detailTab.draw();



                }
            }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }


        //=================================================== OWNER Detail ===================================================

        if (type.includes("local")) {

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: './sva/getSvaLandsurveyData.php',
                data: dataUrl,
                success: function (data) {
                    var num = 0, dataSet = [];
                    console.log(data);
                    let column = {
                        'OWNER_FIRST_FLAG': 'ลำดับผู้ถือกรรมสิทธิ์',
                        'OWNER_PID': 'เลขบัตรประจำตัวประชาชน',
                        'OWNER': 'ชื่อ-สกุลผู้ถือกรรมสิทธิ์',
                        'ADDRESS': 'ที่อยู่',
                        'AGENT_NAME': 'ชื่อ-สกุลผู้จัดการแทน',
                        'MANDATE_NAME': 'ชื่อ-สกุลผู้รับมอบอำนาจ',
                    };
                    for (let j = 0; j < data[0].length; j++) {
                        for (let i = 0; i < Object.keys(column).length; i++) {
                            var key = Object.keys(column)[i];

                            temp = []
                            temp[0] = column[key]



                            temp[1] = data[0][j][key + "_P1"] ? data[0][j][key + "_P1"] : "-";
                            temp[2] = data[0][j][key + "_P2"] ? data[0][j][key + "_P2"] : "-";


                            if (!dataUrl.includes("dataSeqP1") && dataUrl.includes("dataSeqP2")) {
                                temp[1] = "-";
                            }

                            if (dataUrl.includes("dataSeqP1") && !dataUrl.includes("dataSeqP2")) {
                                temp[2] = "-";
                            }
                            dataSet[num] = temp;
                            num++;

                        }
                    }
                    var owner_detailTab = $('#owner-detailTab-' + type + '-table').DataTable({
                        "searching": false,
                        "ordering": false,
                        orderCellsTop: true,
                        "language": {
                            "emptyTable": "ไม่มีข้อมูลผู้มีสิทธิ์ในที่ดิน"
                        },
                        "bPaginate": false,
                        'info': false,
                        "columnDefs": [{
                            "className": "dt-center",
                            "targets": [1, 2],

                        }, {
                            "className": "dt-left",
                            "targets": [0],

                        }],
                        "createdRow": function (row, data, index) {
                            if (data[0].includes("ลำดับผู้ถือกรรมสิทธิ์")) {
                                $('td', row).css('background-color', 'rgb(236,236,236)');
                            }
                        },
                        "drawCallback": function () {
                            currentPageCount = this.api().rows({
                                page: 'current'
                            }).data().length;
                        }
                    });

                    owner_detailTab.search('').draw();
                    owner_detailTab.clear();
                    owner_detailTab.rows.add(dataSet);
                    owner_detailTab.draw();



                }
            }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }
        else if (type.includes("calculate")) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: './sva/getSvcSurveyData.php',
                data: dataUrl,
                success: function (data) {
                    var num = 0, dataSet = [];
                    console.log(data);
                    var count = data[0].length > 0 ? Object.keys(data[0][0]).length : 0;
                    var loop = dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP2") ? 2 : 1;
                    console.log("detailData = ", data);
                    for (let i = 0; i < count; i += loop) {
                        var key = Object.keys(data[0][0])[i];

                        temp = []
                        temp[0] = columnSvcSurveyDescDetail[key]

                        temp[1] = data[0][0][key] ? data[0][0][key] : "-";
                        temp[2] = data[0][0][key.replace("P1", "P2")] ? data[0][0][key.replace("P1", "P2")] : "-";




                        if (!dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP1")) {
                            temp[1] = "-";
                        }

                        if (dataUrl.includes("surveyjobSeqP2") && !dataUrl.includes("surveyjobSeqP2")) {
                            temp[2] = "-";
                        }

                        dataSet[num] = temp;
                        num++;

                    }
                    var owner_detailTab = $('#owner-detailTab-' + type + '-table').DataTable({
                        "searching": false,
                        "ordering": false,
                        orderCellsTop: true,
                        "language": {
                            "emptyTable": "ไม่มีข้อมูลรายละเอียดงานรังวัด"
                        },
                        "bPaginate": false,
                        'info': false,
                        "columnDefs": [{
                            "className": "dt-center",
                            "targets": [1, 2],

                        }, {
                            "className": "dt-left",
                            "targets": [0],

                        }],
                        "drawCallback": function () {
                            currentPageCount = this.api().rows({
                                page: 'current'
                            }).data().length;
                        }
                    });

                    owner_detailTab.search('').draw();
                    owner_detailTab.clear();
                    owner_detailTab.rows.add(dataSet);
                    owner_detailTab.draw();



                }
            }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }




        //=================================================== FEESURVEY Detail ===================================================
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: './sva/getSvaExpenseData.php',
            data: dataUrl,
            success: function (data) {
                var dataSetP1 = [], dataSetP2 = [], withdrawDataset = [];
                var numP1 = 0, numP2 = 0, numWithdraw = 0;
                console.log("SVA Expenses ", data);

                //============================BTD59 detail
                let column = {
                    'BTD59_NO': 'เลขที่ บ.ท.ด.59',
                    'WITHDRAW_DATE': 'วันที่ทำการถอนจ่าย',
                    'WITHDRAW_AMOUNT': 'จำนวนเงินที่ถอนจ่าย',
                    'WITHDRAW_ADJUST_TYPE': 'เรียกเพิ่ม/คืนเงิน',
                    'WITHDRAWSTATUS_NAME': 'สถานะ',
                };

                for (let i = 0; i < Object.keys(column).length; i++) {
                    var key = Object.keys(column)[i];

                    temp = []
                    temp[0] = column[key]

                    if (key.includes("AMOUNT")) {
                        temp[1] = data[0][data[0].length - 1][key + "_P1"] ? Number(data[0][data[0].length - 1][key + "_P1"]).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2
                        }) : "-";
                        temp[2] = data[0][data[0].length - 1][key + "_P2"] ? Number(data[0][data[0].length - 1][key + "_P2"]).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2
                        }) : "-";
                    }
                    else {
                        temp[1] = data[0][data[0].length - 1][key + "_P1"] ? data[0][data[0].length - 1][key + "_P1"] : "-";
                        temp[2] = data[0][data[0].length - 1][key + "_P2"] ? data[0][data[0].length - 1][key + "_P2"] : "-";
                    }



                    if (!dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP2")) {
                        temp[1] = "-";
                    }

                    if (dataUrl.includes("surveyjobSeqP1") && !dataUrl.includes("surveyjobSeqP2")) {
                        temp[2] = "-";
                    }
                    withdrawDataset[numWithdraw] = temp;
                    numWithdraw++;

                }

                var count = Object.keys(data[0]).length;
                if (count > 0) { //ไม่มีข้อมูลโฉนด
                    for (let i = 0; i < count - 1; i++) {
                        temp = []

                        temp[0] = data[0][i]['FEESURVEY_NAME'] ? data[0][i]['FEESURVEY_NAME'] : "";
                        temp[1] = data[0][i]['FEESURVEY_QTY'] ? data[0][i]['FEESURVEY_QTY'] : "";
                        temp[2] = data[0][i]['FEESURVEY_UNITTEXT'] ? data[0][i]['FEESURVEY_UNITTEXT'] : "";
                        temp[3] = data[0][i]['FEESURVEY_AMOUNT'] ? Number(data[0][i]['FEESURVEY_AMOUNT']).toLocaleString(undefined, {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2
                        }) : "";

                        if (data[0][i]['KEY'] == 1) {
                            dataSetP1[numP1] = temp;
                            numP1++;
                        }
                        else if (data[0][i]['KEY'] == 2) {
                            dataSetP2[numP2] = temp;
                            numP2++;
                        }

                    }
                    //ราคารวม
                    temp = []
                    temp[0] = "รวม";
                    temp[1] = ""
                    temp[2] = "";
                    temp[3] = data[0][Object.keys(data[0]).length - 1]['WITHDRAW_EXPENSE_P1'] ? Number(data[0][Object.keys(data[0]).length - 1]['WITHDRAW_EXPENSE_P1']).toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2
                    }) : '-';
                    dataSetP1[numP1] = temp;
                    numP1++;

                    temp = []
                    temp[0] = "รวม";
                    temp[1] = ""
                    temp[2] = "";
                    temp[3] = data[0][Object.keys(data[0]).length - 1]['WITHDRAW_EXPENSE_P2'] ? Number(data[0][Object.keys(data[0]).length - 1]['WITHDRAW_EXPENSE_P2']).toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2
                    }) : '-';
                    dataSetP2[numP2] = temp;
                    numP2++;

                    //จำนวนเงินที่เรียกเพิ่มจากผู้ขอ
                    temp = []
                    temp[0] = "จำนวนเงินที่เรียกเพิ่มจากผู้ขอ";
                    temp[1] = ""
                    temp[2] = "";
                    temp[3] = data[0][Object.keys(data[0]).length - 1]['WITHDRAW_AMOUNT_P1'] ? Number(data[0][Object.keys(data[0]).length - 1]['WITHDRAW_AMOUNT_P1']).toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2
                    }) : '-';
                    dataSetP1[numP1] = temp;
                    numP1++;

                    temp = []
                    temp[0] = "จำนวนเงินที่เรียกเพิ่มจากผู้ขอ";
                    temp[1] = ""
                    temp[2] = "";
                    temp[3] = data[0][Object.keys(data[0]).length - 1]['WITHDRAW_AMOUNT_P2'] ? Number(data[0][Object.keys(data[0]).length - 1]['WITHDRAW_AMOUNT_P2']).toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2
                    }) : '-';
                    dataSetP2[numP2] = temp;
                    numP2++;

                    //จำนวนเงินที่คืนผู้ขอ
                    temp = []
                    temp[0] = "จำนวนเงินที่คืนผู้ขอ";
                    temp[1] = ""
                    temp[2] = "";
                    temp[3] = data[0][Object.keys(data[0]).length - 1]['WITHDRAW_ADJUST_AMOUNT_P1'] ? Number(data[0][Object.keys(data[0]).length - 1]['WITHDRAW_ADJUST_AMOUNT_P1']).toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2
                    }) : '-'
                    dataSetP1[numP1] = temp;
                    numP1++;

                    temp = []
                    temp[0] = "จำนวนเงินที่คืนผู้ขอ";
                    temp[1] = ""
                    temp[2] = "";
                    temp[3] = data[0][Object.keys(data[0]).length - 1]['WITHDRAW_ADJUST_AMOUNT_P2'] ? Number(data[0][Object.keys(data[0]).length - 1]['WITHDRAW_ADJUST_AMOUNT_P2']).toLocaleString(undefined, {
                        minimumFractionDigits: 2,
                        maximumFractionDigits: 2
                    }) : '-';
                    dataSetP2[numP2] = temp;
                    numP2++;




                }
                console.log("withdraw", withdrawDataset);



                var table_p1 = $('#expense-detailTab-' + type + '-table-p1').DataTable({
                    "searching": false,
                    "ordering": false,
                    "bPaginate": false,
                    'info': false,
                    "createdRow": function (row, data, index) {
                        if (data[0].includes("รวม") || data[0].includes("จำนวนเงินที่เรียกเพิ่มจากผู้ขอ") || data[0].includes("จำนวนเงินที่คืนผู้ขอ")) {
                            $('td', row).css('background-color', 'rgb(236,236,236)');
                            $('td', row).css('font-weight', '600');
                        }
                    },
                    orderCellsTop: true,
                    "language": {
                        "emptyTable": "ไม่มีข้อมูล"
                    },
                    "columnDefs": [{
                        "className": "dt-left",
                        "targets": [0],

                    }, {
                        "className": "dt-right",
                        "targets": [3],

                    }, {
                        "className": "dt-center",
                        "targets": [1, 2],

                    }],
                    "lengthChange": false,
                    "drawCallback": function () {
                        currentPageCount = this.api().rows({
                            page: 'current'
                        }).data().length;
                    }
                });
                table_p1.clear();
                table_p1.draw();

                var table_p2 = $('#expense-detailTab-' + type + '-table-p2').DataTable({
                    "searching": false,
                    "ordering": false,
                    orderCellsTop: true,
                    "bPaginate": false,
                    "createdRow": function (row, data, index) {
                        if (data[0].includes("รวม") || data[0].includes("จำนวนเงินที่เรียกเพิ่มจากผู้ขอ") || data[0].includes("จำนวนเงินที่คืนผู้ขอ")) {
                            $('td', row).css('background-color', 'rgb(236,236,236)');
                            $('td', row).css('font-weight', '600');
                        }
                    },
                    "language": {
                        "emptyTable": "ไม่มีข้อมูล"
                    },
                    "columnDefs": [{
                        "className": "dt-left",
                        "targets": [0],

                    }, {
                        "className": "dt-right",
                        "targets": [3],

                    }, {
                        "className": "dt-center",
                        "targets": [1, 2],

                    }],
                    'info': false,
                    "lengthChange": false,
                    "drawCallback": function () {
                        currentPageCount = this.api().rows({
                            page: 'current'
                        }).data().length;
                    }
                });

                var withdraw_table = $('#withdraw-detailTab-' + type + '-table').DataTable({
                    "searching": false,
                    "ordering": false,
                    orderCellsTop: true,
                    "bPaginate": false,
                    "language": {
                        "emptyTable": "ไม่มีข้อมูล"
                    },
                    "columnDefs": [{
                        "className": "dt-left",
                        "targets": [0],

                    }, {
                        "className": "dt-center",
                        "targets": [1, 2],

                    }],
                    'info': false,
                    "lengthChange": false,
                    "drawCallback": function () {
                        currentPageCount = this.api().rows({
                            page: 'current'
                        }).data().length;
                    }
                });

                table_p2.clear();
                table_p2.draw();

                table_p1.search('').draw();
                table_p1.clear();
                table_p1.rows.add(dataSetP1);
                table_p1.draw();

                table_p2.search('').draw();
                table_p2.clear();
                table_p2.rows.add(dataSetP2);
                table_p2.draw();

                withdraw_table.search('').draw();
                withdraw_table.clear();
                withdraw_table.rows.add(withdrawDataset);
                withdraw_table.draw();
                $('.overlayP2').hide();



            }
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

        //================================= JOBSTATUS ===============================
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: './sva/getSvaJobStatusData.php',
            data: dataUrl,
            success: function (data) {
                var num = 0, dataSet = [];
                console.log(data);
                console.log("detailData = ", data);
                for (let j = 0; j < data[0].length; j++) {

                    temp = []
                    temp[0] = data[0][j]["JOBSTATUS_NAME"]

                    temp[1] = data[0][j]["MOVEMENT_DTM_P1"] ? data[0][j]["MOVEMENT_DTM_P1"] : "-";
                    temp[2] = data[0][j]["MOVEMENT_DTM_P2"] ? data[0][j]["MOVEMENT_DTM_P2"] : "-";


                    if (!dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP2")) {
                        temp[1] = "-";
                    }

                    if (dataUrl.includes("surveyjobSeqP1") && !dataUrl.includes("surveyjobSeqP2")) {
                        temp[2] = "-";
                    }
                    dataSet[num] = temp;
                    num++;

                }


                var status_detailTab = $('#status-detailTab-' + type + '-table').DataTable({
                    "searching": false,
                    "ordering": false,
                    orderCellsTop: true,
                    "language": {
                        "emptyTable": "ไม่มีข้อมูลรายละเอียดสถานะการรังวัด"
                    },
                    "bPaginate": false,
                    'info': false,
                    "columnDefs": [{
                        "className": "dt-center",
                        "targets": [1, 2],

                    }, {
                        "className": "dt-left",
                        "targets": [0],

                    }],
                    "drawCallback": function () {
                        currentPageCount = this.api().rows({
                            page: 'current'
                        }).data().length;
                    }
                });

                status_detailTab.search('').draw();
                status_detailTab.clear();
                status_detailTab.rows.add(dataSet);
                status_detailTab.draw();



            }
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');


        //=================================================== Appointment Detail ===================================================
        if (type.includes("private")) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: './sva/getSvaAppointmentData.php',
                data: dataUrl + "&private=1",
                success: function (data) {
                    var num = 0, dataSet = [];
                    console.log(data);
                    console.log("detailData = ", data);
                    let column = {
                        'PRIVATE_OFFICE_NAME': 'สำนักงานรังวัดเอกชน',
                        'PV_SURVEYOR_NAME': 'ช่างรังวัดเอกชน',
                        'PRIVATE_SVY_MOBILE': 'โทรศัพท์มือถือช่างรังวัดเอกชน',
                        'APPOINTMENT_DTM': 'วันที่นัดรังวัด',
                        'APPOINTMENT_TIME': 'เวลานัดรังวัด',
                        'APPOINTMENT_DAY_QTY': 'จำนวนวันที่รังวัด',
                        'SURVEYOR_NAME': 'นายช่างรังวัดสำนักงานที่ดินรับผิดชอบ',
                        'SURVEYOR_MOBILE': 'โทรศัพท์มือถือนายช่างรังวัดสำนักงานที่ดินรับผิดชอบ',
                        'APPOINTMENT_REM': 'หมายเหตุ',
                        'APPOINTMENT_DTM': 'วันที่นัดรังวัด',
                        'APPOINTMENT_WRITER': 'ชื่อผู้บันทึกนัดรังวัด',
                    };
                    for (let j = 0; j < data[0].length; j++) {
                        for (let i = 0; i < (Object.keys(column).length); i++) {
                            var key = Object.keys(column)[i];
                            temp = []
                            temp[0] = column[key]

                            temp[1] = data[0][j][key + "_P1"] ? data[0][j][key + "_P1"] : "-";
                            temp[2] = data[0][j][key + "_P2"] ? data[0][j][key + "_P2"] : "-";


                            if (!dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP2")) {
                                temp[1] = "-";
                            }

                            if (dataUrl.includes("surveyjobSeqP1") && !dataUrl.includes("surveyjobSeqP2")) {
                                temp[2] = "-";
                            }
                            dataSet[num] = temp;
                            num++;

                        }
                    }

                    var appointment_detailTab = $('#appointment-detailTab-' + type + '-table').DataTable({
                        "searching": false,
                        "ordering": false,
                        orderCellsTop: true,
                        "language": {
                            "emptyTable": "ไม่มีข้อมูลรายละเอียดการนัดรังวัด"
                        },
                        "bPaginate": false,
                        'info': false,
                        "createdRow": function (row, data, index) {
                            if (data[0].includes("สำนักงานรังวัดเอกชน")) {
                                $('td', row).css('background-color', 'rgb(236,236,236)');
                            }
                        },
                        "columnDefs": [{
                            "className": "dt-center",
                            "targets": [1, 2],

                        }, {
                            "className": "dt-left",
                            "targets": [0],

                        }],
                        "drawCallback": function () {
                            currentPageCount = this.api().rows({
                                page: 'current'
                            }).data().length;
                        }
                    });

                    appointment_detailTab.search('').draw();
                    appointment_detailTab.clear();
                    appointment_detailTab.rows.add(dataSet);
                    appointment_detailTab.draw();



                }
            }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }
        else {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: './sva/getSvaAppointmentData.php',
                data: dataUrl,
                success: function (data) {
                    var num = 0, dataSet = [];
                    console.log(data);
                    console.log("detailData = ", data);
                    let column = {
                        'APPOINTMENT_DTM': 'วันที่นัดรังวัด',
                        'APPOINTMENT_TIME': 'เวลานัดรังวัด',
                        'APPOINTMENT_DAY_QTY': 'จำนวนวันที่รังวัด',
                        'SURVEYOR_NAME': 'ชื่อช่างรังวัด',
                        'SURVEYOR_MOBILE': 'โทรศัพท์มือถือ',
                        'APPOINTMENT_DETAIL': 'รายละเอียดการนัด',
                        'APPOINTMENT_REM': 'หมายเหตุ',
                        'APPOINTMENT_WRITER': 'ชื่อผู้บันทึกนัดรังวัด',
                    };
                    for (let j = 0; j < data[0].length; j++) {
                        for (let i = 0; i < (Object.keys(column).length); i++) {
                            var key = Object.keys(column)[i];
                            temp = []
                            temp[0] = column[key]

                            temp[1] = data[0][j][key + "_P1"] ? data[0][j][key + "_P1"] : "-";
                            temp[2] = data[0][j][key + "_P2"] ? data[0][j][key + "_P2"] : "-";


                            if (!dataUrl.includes("surveyjobSeqP1") && dataUrl.includes("surveyjobSeqP2")) {
                                temp[1] = "-";
                            }

                            if (dataUrl.includes("surveyjobSeqP1") && !dataUrl.includes("surveyjobSeqP2")) {
                                temp[2] = "-";
                            }
                            dataSet[num] = temp;
                            num++;

                        }
                    }

                    var appointment_detailTab = $('#appointment-detailTab-' + type + '-table').DataTable({
                        "searching": false,
                        "ordering": false,
                        orderCellsTop: true,
                        "language": {
                            "emptyTable": "ไม่มีข้อมูลรายละเอียดการนัดรังวัด"
                        },
                        "createdRow": function (row, data, index) {
                            if (data[0].includes("วันที่นัดรังวัด")) {
                                $('td', row).css('background-color', 'rgb(236,236,236)');
                            }
                        },
                        "bPaginate": false,
                        'info': false,
                        "columnDefs": [{
                            "className": "dt-center",
                            "targets": [1, 2],

                        }, {
                            "className": "dt-left",
                            "targets": [0],

                        }],
                        "drawCallback": function () {
                            currentPageCount = this.api().rows({
                                page: 'current'
                            }).data().length;
                        }
                    });

                    appointment_detailTab.search('').draw();
                    appointment_detailTab.clear();
                    appointment_detailTab.rows.add(dataSet);
                    appointment_detailTab.draw();



                }
            }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        }


    }
    else {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: './sva/getSvaDetailData.php',
            data: dataUrl,
            success: function (data) {
                var num = 0, dataSet = [];
                console.log(data);
                var count = data[0].length > 0 ? Object.keys(data[0][0]).length : 0;
                var loop = dataUrl.includes("udmparcelSeqP1") && dataUrl.includes("udmparcelSeqP2") ? 2 : 1;
                let columnDigitalMapDetail = {
                    'UTM': 'ระวาง UTM',
                    'AREA': 'เนื้อที่',
                    'SCALE_SEQ': 'มาตราส่วนระวาง UTM',
                    'PERIMETER': 'เส้นรอบรูปแปลงที่ดิน',
                };
                console.log("detailData = ", data);
                for (let j = 0; j < data[0].length; j++) {
                    for (let i = 0; i < (Object.keys(columnDigitalMapDetail).length); i++) {
                        var key = Object.keys(columnDigitalMapDetail)[i];

                        temp = []
                        temp[0] = columnDigitalMapDetail[key]

                        if (key.includes("SCALE_SEQ")) {
                            var UTMP1, UTMP2, UTMP3, UTMP4;
                            utmScale.forEach(utm => {
                                if (data[0][j]['OLDSCALE_SEQ_P1'] == utm.seq) UTMP1 = utm.value
                                if (data[0][j]['OLDSCALE_SEQ_P2'] == utm.seq) UTMP2 = utm.value
                                if (data[0][j]['NEWSCALE_SEQ_P1'] == utm.seq) UTMP3 = utm.value
                                if (data[0][j]['NEWSCALE_SEQ_P2'] == utm.seq) UTMP4 = utm.value
                            })

                            temp[1] = UTMP1;
                            temp[2] = UTMP2;
                            temp[3] = UTMP3;
                            temp[4] = UTMP4;
                        }
                        else if (key.includes("AREA") || key.includes("PERIMETER")) {
                            temp[1] = data[0][j]["OLD" + key + "_P1"] ? Number(data[0][j]["OLD" + key + "_P1"]).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            }) : "-";
                            temp[2] = data[0][j]["OLD" + key + "_P2"] ? Number(data[0][j]["OLD" + key + "_P2"]).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            }) : "-";
                            temp[3] = data[0][j]["NEW" + key + "_P1"] ? Number(data[0][j]["NEW" + key + "_P1"]).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            }) : "-";
                            temp[4] = data[0][j]["NEW" + key + "_P2"] ? Number(data[0][j]["NEW" + key + "_P2"]).toLocaleString(undefined, {
                                minimumFractionDigits: 2,
                                maximumFractionDigits: 2
                            }) : "-";
                        }
                        else {
                            temp[1] = data[0][j]["OLD" + key + "_P1"] ? data[0][j]["OLD" + key + "_P1"] : "-";
                            temp[2] = data[0][j]["OLD" + key + "_P2"] ? data[0][j]["OLD" + key + "_P2"] : "-";
                            temp[3] = data[0][j]["NEW" + key + "_P1"] ? data[0][j]["NEW" + key + "_P1"] : "-";
                            temp[4] = data[0][j]["NEW" + key + "_P2"] ? data[0][j]["NEW" + key + "_P2"] : "-";
                        }


                        if (!dataUrl.includes("udmparcelSeqP1") && dataUrl.includes("udmparcelSeqP1")) {
                            temp[1] = "-";
                            temp[2] = "-";
                        }

                        if (dataUrl.includes("udmparcelSeqP2") && !dataUrl.includes("udmparcelSeqP2")) {
                            temp[2] = "-";
                            temp[4] = "-";
                        }
                        dataSet[num] = temp;
                        num++;

                    }
                }

                var udm_detailTab = $('#parcel-detailTab-' + type + '-table').DataTable({
                    "searching": false,
                    "ordering": false,
                    orderCellsTop: true,
                    "language": {
                        "emptyTable": "ไม่มีข้อมูลการปรับปรุงรูปแปลง"
                    },
                    "bPaginate": false,
                    'info': false,
                    "columnDefs": [{
                        "className": "dt-center",
                        "targets": [1, 2, 3, 4],

                    }, {
                        "className": "dt-left",
                        "targets": [0],

                    }],
                    "drawCallback": function () {
                        currentPageCount = this.api().rows({
                            page: 'current'
                        }).data().length;
                    }
                });

                udm_detailTab.search('').draw();
                udm_detailTab.clear();
                udm_detailTab.rows.add(dataSet);
                udm_detailTab.draw();

                $('.overlayP2').hide();



            }
        }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    }







}

function getLandofficeLocalData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;

    switch (svaType) {
        case 1:
            if (document.getElementById('no-sva-landoffice_local').value) dataUrl += '&no=' + document.getElementById('no-sva-landoffice_local').value
            if (document.getElementById('date-sva-landoffice_local').value) dataUrl += '&date=' + document.getElementById('date-sva-landoffice_local').value
            if (document.getElementById('status-sva-landoffice_local').value) dataUrl += '&status=' + document.getElementById('status-sva-landoffice_local').value
            break;
        case 2:
            if (document.getElementById('no-sva-private_local').value) dataUrl += '&no=' + document.getElementById('no-sva-private_local').value
            if (document.getElementById('date-sva-private_local').value) dataUrl += '&date=' + document.getElementById('date-sva-private_local').value
            if (document.getElementById('status-sva-private_local').value) dataUrl += '&status=' + document.getElementById('status-sva-private_local').value

            break;
        case 3:
            if (document.getElementById('no-sva-project_local').value) dataUrl += '&no=' + document.getElementById('no-sva-project_local').value
            if (document.getElementById('date-sva-project_local').value) dataUrl += '&date=' + document.getElementById('date-sva-project_local').value
            if (document.getElementById('status-sva-project_local').value) dataUrl += '&status=' + document.getElementById('status-sva-project_local').value

            break;
        case 4:
            if (document.getElementById('no-sva-survey_local').value) dataUrl += '&no=' + document.getElementById('no-sva-survey_local').value
            if (document.getElementById('date-sva-survey_local').value) dataUrl += '&date=' + document.getElementById('date-sva-survey_local').value
            if (document.getElementById('status-sva-survey_local').value) dataUrl += '&status=' + document.getElementById('status-sva-survey_local').value

            break;
        default:
            break;
    }

    dataUrl += '&jobgroup_seq=' + svaType;
    dataUrl += '&check=' + 'local';


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './sva/getSvaData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['SURVEYJOB_NO_P2'] ? data[0][i]['SURVEYJOB_NO_P2'] : "-";
                temp[2] = data[0][i]['SURVEYJOB_DATE_P2'] ? data[0][i]['SURVEYJOB_DATE_P2'] : "-";
                temp[3] = data[0][i]['TYPEOFSURVEY_NAME_P2'] ? data[0][i]['TYPEOFSURVEY_NAME_P2'] : "-";
                temp[4] = data[0][i]['JOBSTATUS_NAME_P2'] ? data[0][i]['JOBSTATUS_NAME_P2'] : "-";
                temp[5] = '<a onClick="showDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';
                temp[6] = data[0][i]['SURVEYJOB_SEQ_P1'] ? data[0][i]['SURVEYJOB_SEQ_P1'] : "";
                temp[7] = data[0][i]['SURVEYJOB_SEQ_P2'] ? data[0][i]['SURVEYJOB_SEQ_P2'] : "";
                temp[8] = data[0][i]['PROCESS_SEQ_P1'] ? data[0][i]['PROCESS_SEQ_P1'] : "";
                temp[9] = data[0][i]['PROCESS_SEQ_P2'] ? data[0][i]['PROCESS_SEQ_P2'] : "";
                temp[10] = data[0][i]['PRINTPLATE_TYPE_SEQ_P1'] ? data[0][i]['PRINTPLATE_TYPE_SEQ_P1'] : "";
                temp[11] = data[0][i]['PRINTPLATE_TYPE_SEQ_P2'] ? data[0][i]['PRINTPLATE_TYPE_SEQ_P2'] : "";
                // temp[5] = data[0][i]['SURVEYJOB_NO_P2'] ? data[0][i]['SURVEYJOB_NO_P2'] : "-";
                // temp[6] = data[0][i]['SURVEYJOB_DATE_P2'] ? data[0][i]['SURVEYJOB_DATE_P2'] : "-";
                // temp[7] = data[0][i]['TYPEOFSURVEY_NAME_P2'] ? data[0][i]['TYPEOFSURVEY_NAME_P2'] : "-";
                // temp[8] = data[0][i]['JOBSTATUS_NAME_P2'] ? data[0][i]['JOBSTATUS_NAME_P2'] : "-";


                dataSet[num] = temp;
                num++;
            }
            console.log(dataSet);
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}

function getCalculateData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;

    switch (svaType) {
        case 1:
            if (document.getElementById('no-sva-landoffice_calculate').value) dataUrl += '&no=' + document.getElementById('no-sva-landoffice_calculate').value
            if (document.getElementById('date-sva-landoffice_calculate').value) dataUrl += '&date=' + document.getElementById('date-sva-landoffice_calculate').value
            if (document.getElementById('status-sva-landoffice_calculate').value) dataUrl += '&status=' + document.getElementById('status-sva-landoffice_calculate').value
            break;
        case 2:
            if (document.getElementById('no-sva-private_calculate').value) dataUrl += '&no=' + document.getElementById('no-sva-private_calculate').value
            if (document.getElementById('date-sva-private_calculate').value) dataUrl += '&date=' + document.getElementById('date-sva-private_calculate').value
            if (document.getElementById('status-sva-private_calculate').value) dataUrl += '&status=' + document.getElementById('status-sva-private_calculate').value

            break;
        case 3:
            if (document.getElementById('no-sva-project_calculate').value) dataUrl += '&no=' + document.getElementById('no-sva-project_calculate').value
            if (document.getElementById('date-sva-project_calculate').value) dataUrl += '&date=' + document.getElementById('date-sva-project_calculate').value
            if (document.getElementById('status-sva-project_calculate').value) dataUrl += '&status=' + document.getElementById('status-sva-project_calculate').value

            break;
        case 4:
            if (document.getElementById('no-sva-survey_calculate').value) dataUrl += '&no=' + document.getElementById('no-sva-survey_calculate').value
            if (document.getElementById('date-sva-survey_calculate').value) dataUrl += '&date=' + document.getElementById('date-sva-survey_calculate').value
            if (document.getElementById('status-sva-survey_calculate').value) dataUrl += '&status=' + document.getElementById('status-sva-survey_calculate').value

            break;
        default:
            break;
    }



    dataUrl += '&jobgroup_seq=' + svaType;
    dataUrl += '&check=' + 'calculate';


    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './sva/getSvaData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['SURVEYJOB_NO_P2'] ? data[0][i]['SURVEYJOB_NO_P2'] : "-";
                temp[2] = data[0][i]['SURVEYJOB_DATE_P2'] ? data[0][i]['SURVEYJOB_DATE_P2'] : "-";
                temp[3] = data[0][i]['TYPEOFSURVEY_NAME_P2'] ? data[0][i]['TYPEOFSURVEY_NAME_P2'] : "-";
                temp[4] = data[0][i]['JOBSTATUS_NAME_P2'] ? data[0][i]['JOBSTATUS_NAME_P2'] : "-";
                temp[5] = '<a onClick="showDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';
                temp[6] = data[0][i]['SURVEYJOB_SEQ_P1'] ? data[0][i]['SURVEYJOB_SEQ_P1'] : "";
                temp[7] = data[0][i]['SURVEYJOB_SEQ_P2'] ? data[0][i]['SURVEYJOB_SEQ_P2'] : "";
                temp[8] = data[0][i]['PROCESS_SEQ_P1'] ? data[0][i]['PROCESS_SEQ_P1'] : "";
                temp[9] = data[0][i]['PROCESS_SEQ_P2'] ? data[0][i]['PROCESS_SEQ_P2'] : "";
                temp[10] = data[0][i]['PRINTPLATE_TYPE_SEQ_P1'] ? data[0][i]['PRINTPLATE_TYPE_SEQ_P1'] : "";
                temp[11] = data[0][i]['PRINTPLATE_TYPE_SEQ_P2'] ? data[0][i]['PRINTPLATE_TYPE_SEQ_P2'] : "";


                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}

function getDigitalMapData(table, e) {
    var dataUrl = 'landoffice=' + landoffice;


    if (document.getElementById('utmmap1-sva-digital_map').value) dataUrl += '&utmmap1=' + document.getElementById('utmmap1-sva-digital_map').value
    if (document.getElementById('utmmap2-sva-digital_map').value) dataUrl += '&utmmap2=' + document.getElementById('utmmap2-sva-digital_map').value
    if (document.getElementById('utmmap3-sva-digital_map').value) dataUrl += '&utmmap3=' + document.getElementById('utmmap3-sva-digital_map').value
    if (document.getElementById('utmmap4-sva-digital_map').value) dataUrl += '&utmmap4=' + document.getElementById('utmmap4-sva-digital_map').value

    if (document.getElementById('landno-sva-digital_map').value) dataUrl += '&landno=' + document.getElementById('landno-sva-digital_map').value
    if (document.getElementById('scale-sva-digital_map').value) dataUrl += '&scale=' + document.getElementById('scale-sva-digital_map').value
    if (document.getElementById('parceltype-sva-digital_map').value) dataUrl += '&parceltype=' + document.getElementById('parceltype-sva-digital_map').value

    dataUrl += '&check=' + 'digital_map';

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './sva/getSvaData.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['UTM_P2'] ? data[0][i]['UTM_P2'] : "-";
                temp[2] = data[0][i]['UTMMAP4_P2'] ? data[0][i]['UTMMAP4_P2'] : "-";
                temp[3] = data[0][i]['SCALE_NAME_P2'] ? data[0][i]['SCALE_NAME_P2'] : "-";
                temp[4] = data[0][i]['LAND_NO_P2'] ? data[0][i]['LAND_NO_P2'] : "-";
                temp[5] = data[0][i]['PARCELTYPE_DESC_P2'] ? data[0][i]['PARCELTYPE_DESC_P2'] : "-";
                temp[6] = '<a onClick="showDetail($(this))"><i class="fa fa-info-circle fa-lg" style="color:#2ca7d6;" aria-hidden="true"></i></a>';
                temp[7] = data[0][i]['UDM_PARCEL_SEQ_P1'] ? data[0][i]['UDM_PARCEL_SEQ_P1'] : "";
                temp[8] = data[0][i]['UDM_PARCEL_SEQ_P2'] ? data[0][i]['UDM_PARCEL_SEQ_P2'] : "";


                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-main').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');

    e.preventDefault();

}
function getSVACount(check) {
    var dataUrl = 'landoffice=' + landoffice;

    table = $('#table-sva-' + type + '-count').DataTable({
        "pageLength": 10,
        "pagingType": "simple_numbers",
        "searching": false,
        "ordering": false,
        orderCellsTop: true,
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs": [{
            "className": "text-center",
            "targets": "_all",

        }],
        "info": false,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    })

    dataUrl += '&jobgroup_seq=' + svaType;
    dataUrl += '&check=' + check;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './sva/getSvaCount.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []
                temp[0] = i + 1;
                temp[1] = data[0][i]['NAME'] ? data[0][i]['NAME'] : "";
                temp[2] = data[0][i]['COUNT_P1'] ? parseInt(data[0][i]['COUNT_P1']).toLocaleString() : "";
                temp[3] = data[0][i]['COUNT_P2'] ? parseInt(data[0][i]['COUNT_P2']).toLocaleString() : "";


                dataSet[num] = temp;
                num++;
            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-count').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');


}

function getSVAOverall() {
    var dataUrl = 'landoffice=' + landoffice + '&check=overall';

    table = $('#table-sva-overall').DataTable({
        "searching": false,
        "ordering": false,
        orderCellsTop: true,
        "bPaginate": false,
        "language": {
            "emptyTable": "ไม่มีข้อมูล"
        },
        "columnDefs": [{
            "className": "dt-center",
            "targets": [0, 2, 3, 4],

        }, {
            "className": "dt-left",
            "targets": [1],

        }],
        "createdRow": function (row, data, dataIndex) {
            if (dataIndex == 0 || dataIndex == 5 || dataIndex == 10) {
                $(row).addClass('redClass');
            }
        },
        "info": false,
        "drawCallback": function () {
            currentPageCount = this.api().rows({
                page: 'current'
            }).data().length;
        }
    })

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: './sva/getSvaCount.php',
        data: dataUrl,
        success: function (data) {
            var dataSet = [];
            var num = 0;
            var count = Object.keys(data[0]).length;
            console.log("Success", data);
            for (let i = 0; i < count; i++) {
                temp = []

                if (i == 0) {
                    temp[1] = "ระบบบริหารงานรังวัดในสำนักงาน";
                    temp[2] = "";
                    temp[3] = "";
                    temp[4] = "";
                    dataSet[num] = temp;
                    num++;
                    temp = []
                    temp[0] = i + 1;
                    temp[1] = data[0][i]['TYPE'] ? data[0][i]['TYPE'] : "";
                    temp[2] = data[0][i]['RECV'] ? parseInt(data[0][i]['RECV']).toLocaleString() : "";
                    temp[3] = data[0][i]['OK'] ? parseInt(data[0][i]['OK']).toLocaleString() : "";
                    temp[4] = data[0][i]['ERROR'] ? parseInt(data[0][i]['ERROR']).toLocaleString() : "";
                    dataSet[num] = temp;
                    num++;
                }
                else if (i == 4) {
                    temp[1] = "ระบบคำนวณรังวัดในสำนักงานที่ดิน";
                    temp[2] = "";
                    temp[3] = "";
                    temp[4] = "";
                    dataSet[num] = temp;
                    num++;
                    temp = []
                    temp[0] = i + 1;
                    temp[1] = data[0][i]['TYPE'] ? data[0][i]['TYPE'] : "";
                    temp[2] = data[0][i]['RECV'] ? parseInt(data[0][i]['RECV']).toLocaleString() : "";
                    temp[3] = data[0][i]['OK'] ? parseInt(data[0][i]['OK']).toLocaleString() : "";
                    temp[4] = data[0][i]['ERROR'] ? parseInt(data[0][i]['ERROR']).toLocaleString() : "";
                    dataSet[num] = temp;
                    num++;
                }
                else {
                    temp[0] = i + 1;
                    temp[1] = data[0][i]['TYPE'] ? data[0][i]['TYPE'] : "";
                    temp[2] = data[0][i]['RECV'] ? parseInt(data[0][i]['RECV']).toLocaleString() : "";
                    temp[3] = data[0][i]['OK'] ? parseInt(data[0][i]['OK']).toLocaleString() : "";
                    temp[4] = data[0][i]['ERROR'] ? parseInt(data[0][i]['ERROR']).toLocaleString() : "";

                    dataSet[num] = temp;
                    num++;
                }

            }
            table.search('').draw();
            table.clear();
            table.rows.add(dataSet);
            table.draw();
            $('.overlay-count').hide();

        }
    }).setRequestHeader('X-Requested-With', 'XMLHttpRequest');


}
function exportSvo(check, sts, jobseq, landoffice) {

    if (jobseq == 2) {
        window.open('./queries/excelSvaData.php?' + "check=" + check + "&sts=" + sts + "&jobgroup_seq=" + jobseq + "&landoffice=" + landoffice + "&branchName=" + branchName + "&private=1", true);

    }
    else {
        window.open('./queries/excelSvaData.php?' + "check=" + check + "&sts=" + sts + "&jobgroup_seq=" + jobseq + "&landoffice=" + landoffice + "&branchName=" + branchName, true);

    }

}
function exportErrorSvo(check, sts, jobseq, landoffice) {

    if (jobseq == 2) {
        window.open('./queries/excelSvaErrorData.php?' + "check=" + check + "&sts=" + sts + "&jobgroup_seq=" + jobseq + "&landoffice=" + landoffice + "&branchName=" + branchName + "&private=1", true);

    }
    else {
        window.open('./queries/excelSvaErrorData.php?' + "check=" + check + "&sts=" + sts + "&jobgroup_seq=" + jobseq + "&landoffice=" + landoffice + "&branchName=" + branchName, true);

    }



}


$(document).ready(function () {
    $('.sidebar-menu').click(function () {
        $('ul a.nav-link').removeClass('active');
        $(this).parent().children('a').addClass('active');
    });
    $('.tab-pane').hide();




    //redirect to login page when bypass by URL
    if (getCookie("landoffice") == "") {
        location.href = "./index.html";
    }


    $('.overlay-main').hide();


    $.fn.dataTable.ext.errMode = 'none';

    branchName = getCookie('branchName');
    landoffice = getCookie('landoffice');

    document.getElementById("landoffice_name").innerHTML = '<i class="fa-lg fa fa-map"></i> ข้อมูลงานรังวัดและแผนที่<br>[' + branchName + ']';

    $('.nav-pills-main li ul li a').on('show.bs.tab', function () {

        $('.overlay-count').show();
        $(current_href).hide();
        $(current_href + '-table').hide();

        var href = $(this).attr('href');
        console.log("href = ", href);
        current_href = href;
        $(current_href).show();
        $(current_href + '-table').show();
        $("#search-sva-" + type).off();
        type = href.split('-')[2];
        jobgroup = href.split('-')[3];
        svaType = 0;
        switch (type) {
            case 'overall':
                getSVAOverall();
                break;
            case 'landoffice_local':
                svaType = 1;
                getJobStatus("#status-sva-landoffice_local");
                getSVACount('local');
                break;
            case 'private_local':
                svaType = 2;
                getJobStatus("#status-sva-private_local");
                getSVACount('local');
                break;
            case 'project_local':
                svaType = 3;
                getJobStatus("#status-sva-project_local");
                getSVACount('local');
                break;
            case 'survey_local':
                svaType = 4;
                getJobStatus("#status-sva-survey_local");
                getSVACount('local');
                break;
            case 'landoffice_calculate':
                svaType = 1;
                getJobStatus("#status-sva-landoffice_calculate");
                getSVACount('calculate');
                break;
            case 'private_calculate':
                svaType = 2;
                getJobStatus("#status-sva-private_calculate");
                getSVACount('calculate');
                break;
            case 'project_calculate':
                svaType = 3;
                getJobStatus("#status-sva-project_calculate");
                getSVACount('calculate');
                break;
            case 'survey_calculate':
                svaType = 4;
                getJobStatus("#status-sva-survey_calculate");
                getSVACount('calculate');
                break;
            case 'survey_calculate':
                svaType = 4;
                getJobStatus("#status-sva-survey_calculate");
                getSVACount('calculate');
                break;
            case 'digital_map':
                svaType = 0;
                getScaleType("#scale-sva-digital_map");
                getParcelType("#parceltype-sva-digital_map");
                getSVACount('digital_map');
                break;
            default:
                break;

        }



        $("#search-sva-" + type).on("click", searchFunction);


        $('#export-success-sva-' + svaType).on("click", function () {
            exportSvo('local', 's', svaType, landoffice);
        });
        $('#export-diff-sva-' + svaType).on("click", function () {
            exportSvo('local', 'e', svaType, landoffice);
        });
        $('#export-error-sva-' + svaType).on("click", function () {
            exportErrorSvo('local', 's', svaType, landoffice);
        });


        $('#export-success-svc-' + svaType).on("click", function () {
            exportSvo('calculate', 's', svaType, landoffice);
        });
        $('#export-diff-svc-' + svaType).on("click", function () {
            exportSvo('calculate', 'e', svaType, landoffice);
        });
        $('#export-error-svc-' + svaType).on("click", function () {
            exportErrorSvo('calculate', 's', svaType, landoffice);
        });

        $('#export-success-udm').on("click", function () {
            exportSvo('digital_map', 's', svaType, landoffice);

        });
        $('#export-diff-udm').on("click", function () {
            exportSvo('digital_map', 'e', svaType, landoffice);
        });
        $('#export-error-udm').on("click", function () {
            exportErrorSvo('digital_map', 's', svaType, landoffice);
        });



    });

});