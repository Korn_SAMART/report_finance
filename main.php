﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบตรวจสอบ Transaction</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">


    <!-- Theme style -->
    <link rel="stylesheet" href="./plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="./style/style.css">
    <link rel="stylesheet" href="./dist/css/dash.css">
    <link rel="stylesheet" href="./plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="./plugins/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./dist/DataTables/datatables.min.css" />
    <link rel="stylesheet" href="./dist/DataTables/jquery.datatables.min.css" />

    <script src="./plugins/jquery/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./style/login.css">


    <link rel="shortcut icon" href="#" />

    <style>
        select {
            border: 1px solid #ccc;
            vertical-align: top;
            min-height: 20px;
        }
    </style>

</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div>
        <img id="main-bg" src="./img/dol.png" style="position: absolute;left: 31%;opacity: 0.15;width: 55vw;z-index: -1;">
    </div>
    <div class="page-wrapper chiller-theme toggled">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand">
                    <a href="#" id="landoffice_name">ระบบตรวจสอบข้อมูล Transaction</a>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">Admin</span>
                        <span class="user-role">Administrator</span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-header  -->
                <div class="sidebar-menu">
                    <ul class="nav-pills-main">
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-overall" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">ภาพรวมการถ่ายโอนข้อมูล</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-chanode" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">โฉนดที่ดิน</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-chanodetrajong" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">โฉนดตราจอง</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-trajong" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">ตราจองที่ได้ทำประโยชน์แล้ว</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-ns3" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">น.ส.3</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-ns3a" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">น.ส.3 ก</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-nsl" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">น.ส.ล.</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-subNsl" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">หนังสืออนุญาต</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-condo" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">อาคารชุด</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-condoroom" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">ห้องชุด</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-construct" class="nav-link">
                                <i style="font-size: 17px;" class="fa fa-file-alt fa-2x"></i>
                                <span style="font-size: 17px;">อาคารโรงเรือน</span>
                            </a>
                        </li>


                    </ul>
                </div>
                <!-- sidebar-menu  -->
            </div>
            <!-- sidebar-content  -->
            <div class="sidebar-footer">
                <button class="btn btn-block" id="sign-out-btn" onclick="location.href='./portal.php';">กลับสู่หน้า Portal</button>
            </div>
        </nav>
        <!-- sidebar-wrapper  -->
        <main class="page-content">
            <!-- OverAll Summary -->
            <div class="tab-pane fade" id="tab-overall">
                <!-- <div class="row justify-content-around">
                    <div class="col">
                        <div class="card" id="tab-overall-table">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-overall">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                         
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">ประเภทเอกสารสิทธิ</th>
                                        <th class="w-auto">จำนวนเอกสารสิทธิ</th>
                                        <th class="w-auto">จำนวนรายการที่ติดอายัด</th>
                                        <th class="w-auto">จำนวนรายการที่ถอนอายัด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="card" style="margin-top: 10px;">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-overall">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th style="vertical-align: middle;">ลำดับ</th>
                                        <th style="vertical-align: middle;text-align: center;">ประเภทเอกสารสิทธิ</th>
                                        <th style="vertical-align: middle;">รับมอบ</th>
                                        <th style="vertical-align: middle;">ถ่ายโอนสำเร็จ</th>
                                        <th style="vertical-align: middle;">ถ่ายโอนไม่สำเร็จ</th>
                                    </tr>
                                    <!-- <tr style="text-align: center;">
                                        <th style="vertical-align: middle;" rowspan="2">ทะเบียนที่ดิน</th>
                                        <th style="vertical-align: middle;" rowspan="2">อายัด/ห้ามโอน</th>
                                        <th style="vertical-align: middle;" rowspan="2">ภาระผูกพัน</th>
                                        <th style="vertical-align: middle;" rowspan="2">อาคารโรงเรือน</th>
                                        <th style="vertical-align: middle;" rowspan="2">ทะเบียนที่ดิน</th>
                                        <th style="vertical-align: middle;" colspan="2">อายัด/ห้ามโอน</th>
                                        <th style="vertical-align: middle;" rowspan="2">ภาระผูกพัน</th>
                                        <th style="vertical-align: middle;" rowspan="2">อาคารโรงเรือน</th>
                                        <th style="vertical-align: middle;" rowspan="2">ทะเบียนที่ดิน</th>
                                        <th style="vertical-align: middle;" rowspan="2">อายัด/ห้ามโอน</th>
                                        <th style="vertical-align: middle;" rowspan="2">ภาระผูกพัน</th>
                                        <th style="vertical-align: middle;" rowspan="2">อาคารโรงเรือน</th>
                                    </tr>    
                                    <tr style="text-align: center;">
                                        <th style="vertical-align: middle;">ติดอายัด</th>
                                        <th style="vertical-align: middle;">ถอนอายัด</th>
                                    </tr>                   -->
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- chanode -->
            <div class="tab-pane fade" id="tab-chanode">
                <div class="row justify-content-around">
                    <div class="col">

                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-chanode" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
  
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-chanode" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-2">
                                <div class="form-group">
                                    <label for="">เลขที่เอกสารสิทธิ</label>
                                    <input type="text" name="parcel_no" id="parcel_no" class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">อำเภอ/เขต</label>
                                    <select class="form-control select2 select2-danger amphur-select" style="margin-top: 8px;" id="amphur-select-chanode"></select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">ตำบล/แขวง</label>
                                    <select class="form-control select2 select2-danger tambon-select" style="margin-top: 8px;" id="tambon-select-chanode"></select>
                                </div>
                            </div>
                            <div class="col-2">
                                <!-- <label for="">อายัด / ห้ามโอน</label>
                                <div class="form-group" style="margin: 0;">
                                    <input class="form-check-input" type="checkbox" value="sequester" id="chanode-sequester">
                                    <label for="flexCheckDefault">
                                        อายัด
                                    </label>
                                </div>
                                <div class="form-group" style="margin: 0;">
                                    <input class="form-check-input" type="checkbox" value="hold" id="chanode-hold">
                                    <label for="flexCheckDefault">
                                        ห้ามโอน
                                    </label>
                                </div> -->
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-chanode">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-chanode">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขเอกสารสิทธิ</th>
                                        <th class="w-auto">อำเภอ</th>
                                        <th class="w-auto">ตำบล</th>
                                        <th class="w-auto status_header">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="tabPanel card" id="tabPanel-chanode">

                    <ul class="nav nav-tabs nav-pills detail-pills detailTab-chanode">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-chanode">ข้อมูลเอกสารสิทธิ</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-chanode">ผู้ถือกรรมสิทธิ์</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#olgt-detailTab-chanode">ภาระผูกพัน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-chanode">รายการจดทะเบียน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#sequester-detailTab-chanode">ข้อมูลอายัด</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="parcel-detailTab-chanode" class="tab-pane fade">
                            <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="parcel-detailTab-chanode-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลโฉนดที่ดิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="owner-detailTab-chanode" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-chanode-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลผู้ถือกรรมสิทธิ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="olgt-detailTab-chanode" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="olgt-detailTab-chanode-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลภาระผูกพัน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="transaction-detailTab-chanode" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-p1-chanode">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-p2-chanode">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sequester-detailTab-chanode" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="sequester-detailTab-chanode-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลอายัด</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP2 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
                <!-- 6 tabs of process -->
                <div class="tab6Panel card" id="tab6Panel-chanode">
                    <ul class="nav nav-tabs nav-pills process-pills processTab-chanode">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#aps-processTab-chanode">ราคาประเมิน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#promise-processTab-chanode">คู่สัญญา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#obtain-processTab-chanode">การได้มา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#process-processTab-chanode">สอบสวน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#expense-processTab-chanode">ค่าใช้จ่าย</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#document-processTab-chanode">แบบพิมพ์</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="aps-processTab-chanode" class="tab-pane fade">
                            <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="aps-processTab-chanode-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลราคาประเมิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="promise-processTab-chanode" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="promise-processTab-chanode-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลคู่สัญญา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="obtain-processTab-chanode" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="obtain-processTab-chanode-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลการได้มา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="process-processTab-chanode" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="process-processTab-chanode-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลสอบสวน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="expense-processTab-chanode" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <h5 style="text-align:center;" id="receipt_no"></h5>
                                </div>
                                <div class="col">
                                    <h5 style="text-align:center;" id="fin_no"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-chanode-table-p1">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="expense-processTab-chanode-table-p2">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="document-processTab-chanode" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="document-processTab-chanode-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลแบบพิมพ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP1 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>

            </div>

            <!-- chanodetrajong -->

            <div class="tab-pane fade" id="tab-chanodetrajong">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-chanodetrajong" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                          
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-chanodetrajong" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่เอกสารสิทธิ</label>
                                    <input type="text" name="parcelLandNo-chanodetrajong" id="parcelLandNo-chanodetrajong" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">อำเภอ/เขต</label>
                                    <select class="form-control select2 select2-danger amphur-select" style="margin-top: 8px;" id="amphur-select-chanodetrajong"></select>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">ตำบล/แขวง</label>
                                    <select class="form-control select2 select2-danger tambon-select" style="margin-top: 8px;" id="tambon-select-chanodetrajong"></select>
                                </div>
                            </div>
                            <div class="col-1">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-chanodetrajong">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-chanodetrajong">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขเอกสารสิทธิ</th>
                                        <th class="w-auto">อำเภอ</th>
                                        <th class="w-auto">ตำบล</th>
                                        <th class="w-auto status_header">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabPanel card" id="tabPanel-chanodetrajong">

                    <ul class="nav nav-tabs nav-pills detail-pills detailTab-chanodetrajong">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-chanodetrajong">ข้อมูลเอกสารสิทธิ</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-chanodetrajong">ผู้ถือกรรมสิทธิ์</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#olgt-detailTab-chanodetrajong">ภาระผูกพัน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-chanodetrajong">รายการจดทะเบียน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#sequester-detailTab-chanodetrajong">ข้อมูลอายัด</a></li>

                    </ul>

                    <div class="tab-content">
                        <div id="parcel-detailTab-chanodetrajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-chanodetrajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลโฉนดที่ดิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="owner-detailTab-chanodetrajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-chanodetrajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลผู้ถือกรรมสิทธิ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="olgt-detailTab-chanodetrajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="olgt-detailTab-chanodetrajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลภาระผูกพัน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>


                            </table>
                        </div>
                        <div id="transaction-detailTab-chanodetrajong" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p1-chanodetrajong">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p2-chanodetrajong">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sequester-detailTab-chanodetrajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="sequester-detailTab-chanodetrajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลอายัด</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div class="overlay overlayP2 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
                <!-- 6 tabs of process -->
                <div class="tab6Panel card" id="tab6Panel-chanodetrajong">
                    <ul class="nav nav-tabs nav-pills process-pills processTab-chanodetrajong">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#aps-processTab-chanodetrajong">ราคาประเมิน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#promise-processTab-chanodetrajong">คู่สัญญา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#obtain-processTab-chanodetrajong">การได้มา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#process-processTab-chanodetrajong">สอบสวน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#expense-processTab-chanodetrajong">ค่าใช้จ่าย</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#document-processTab-chanodetrajong">แบบพิมพ์</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="aps-processTab-chanodetrajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="aps-processTab-chanodetrajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลราคาประเมิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="promise-processTab-chanodetrajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="promise-processTab-chanodetrajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลคู่สัญญา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="obtain-processTab-chanodetrajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="obtain-processTab-chanodetrajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลการได้มา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="process-processTab-chanodetrajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="process-processTab-chanodetrajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลสอบสวน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="expense-processTab-chanodetrajong" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <h5 style="text-align:center;" id="receipt_no"></h5>
                                </div>
                                <div class="col">
                                    <h5 style="text-align:center;" id="fin_no"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-chanodetrajong-table-p1">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-chanodetrajong-table-p2">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="document-processTab-chanodetrajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="document-processTab-chanodetrajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลแบบพิมพ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP1 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
            </div>
            <!-- trajong -->
            <div class="tab-pane fade" id="tab-trajong">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-trajong" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                        
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-trajong" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่เอกสารสิทธิ</label>
                                    <input type="text" name="parcelLandNo-trajong" id="parcelLandNo-trajong" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">อำเภอ/เขต</label>
                                    <select class="form-control select2 select2-danger amphur-select" style="margin-top: 8px;" id="amphur-select-trajong"></select>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">ตำบล/แขวง</label>
                                    <select class="form-control select2 select2-danger tambon-select" style="margin-top: 8px;" id="tambon-select-trajong"></select>
                                </div>
                            </div>
                            <div class="col-1">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-trajong">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-trajong">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขเอกสารสิทธิ</th>
                                        <th class="w-auto">อำเภอ</th>
                                        <th class="w-auto">ตำบล</th>
                                        <th class="w-auto status_header">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabPanel card" id="tabPanel-trajong">

                    <ul class="nav nav-tabs nav-pills detail-pills detailTab-trajong">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-trajong">ข้อมูลเอกสารสิทธิ</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-trajong">ผู้ถือกรรมสิทธิ์</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#olgt-detailTab-trajong">ภาระผูกพัน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-trajong">รายการจดทะเบียน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#sequester-detailTab-trajong">ข้อมูลอายัด</a></li>

                    </ul>

                    <div class="tab-content">
                        <div id="parcel-detailTab-trajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-trajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลโฉนดที่ดิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="owner-detailTab-trajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-trajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลผู้ถือกรรมสิทธิ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="olgt-detailTab-trajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="olgt-detailTab-trajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลภาระผูกพัน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <div id="transaction-detailTab-trajong" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p1-trajong">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p2-trajong">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sequester-detailTab-trajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="sequester-detailTab-trajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลอายัด</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div class="overlay overlayP2 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
                <!-- 6 tabs of process -->
                <div class="tab6Panel card" id="tab6Panel-trajong">
                    <ul class="nav nav-tabs nav-pills process-pills processTab-trajong">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#aps-processTab-trajong">ราคาประเมิน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#promise-processTab-trajong">คู่สัญญา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#obtain-processTab-trajong">การได้มา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#process-processTab-trajong">สอบสวน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#expense-processTab-trajong">ค่าใช้จ่าย</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#document-processTab-trajong">แบบพิมพ์</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="aps-processTab-trajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="aps-processTab-trajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลราคาประเมิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="promise-processTab-trajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="promise-processTab-trajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลคู่สัญญา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="obtain-processTab-trajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="obtain-processTab-trajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลการได้มา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="process-processTab-trajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="process-processTab-trajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลสอบสวน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="expense-processTab-trajong" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <h5 style="text-align:center;" id="receipt_no"></h5>
                                </div>
                                <div class="col">
                                    <h5 style="text-align:center;" id="fin_no"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-trajong-table-p1">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-trajong-table-p2">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="document-processTab-trajong" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="document-processTab-trajong-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลแบบพิมพ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP1 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
            </div>
            <!-- ns3 -->
            <div class="tab-pane fade" id="tab-ns3">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-ns3" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                         
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-ns3" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">เลขที่เอกสารสิทธิ</label>
                                    <input type="text" name="parcelLandNo-ns3" id="parcelLandNo-ns3" class="form-control">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for="">หมู่</label>
                                    <input type="text" name="parcelLandMoo-ns3" id="parcelLandMoo-ns3" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">อำเภอ/เขต</label>
                                    <select class="form-control select2 select2-danger amphur-select" style="margin-top: 8px;" id="amphur-select-ns3"></select>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">ตำบล/แขวง</label>
                                    <select class="form-control select2 select2-danger tambon-select" style="margin-top: 8px;" id="tambon-select-ns3"></select>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-ns3">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-ns3">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขเอกสารสิทธิ</th>
                                        <th class="w-auto">หมู่</th>
                                        <th class="w-auto">อำเภอ</th>
                                        <th class="w-auto">ตำบล</th>
                                        <th class="w-auto status_header">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabPanel card" id="tabPanel-ns3">
                    <ul class="nav nav-tabs nav-pills detail-pills detailTab-ns3">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-ns3">ข้อมูลเอกสารสิทธิ</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-ns3">ผู้ถือกรรมสิทธิ์</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#olgt-detailTab-ns3">ภาระผูกพัน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-ns3">รายการจดทะเบียน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#sequester-detailTab-ns3">ข้อมูลอายัด</a></li>

                    </ul>

                    <div class="tab-content">
                        <div id="parcel-detailTab-ns3" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-ns3-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลโฉนดที่ดิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="owner-detailTab-ns3" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-ns3-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลผู้ถือกรรมสิทธิ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="olgt-detailTab-ns3" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="olgt-detailTab-ns3-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลภาระผูกพัน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="transaction-detailTab-ns3" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p1-ns3">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p2-ns3">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sequester-detailTab-ns3" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="sequester-detailTab-ns3-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลอายัด</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div class="overlay overlayP2 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
                <!-- 6 tabs of process -->
                <div class="tab6Panel card" id="tab6Panel-ns3">
                    <ul class="nav nav-tabs nav-pills process-pills processTab-ns3">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#aps-processTab-ns3">ราคาประเมิน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#promise-processTab-ns3">คู่สัญญา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#obtain-processTab-ns3">การได้มา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#process-processTab-ns3">สอบสวน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#expense-processTab-ns3">ค่าใช้จ่าย</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#document-processTab-ns3">แบบพิมพ์</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="aps-processTab-ns3" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="aps-processTab-ns3-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลราคาประเมิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="promise-processTab-ns3" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="promise-processTab-ns3-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลคู่สัญญา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="obtain-processTab-ns3" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="obtain-processTab-ns3-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลการได้มา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="process-processTab-ns3" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="process-processTab-ns3-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลสอบสวน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="expense-processTab-ns3" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <h5 style="text-align:center;" id="receipt_no"></h5>
                                </div>
                                <div class="col">
                                    <h5 style="text-align:center;" id="fin_no"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-ns3-table-p1">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-ns3-table-p2">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="document-processTab-ns3" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="document-processTab-ns3-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลแบบพิมพ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP1 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
            </div>
            <!-- ns3a -->
            <div class="tab-pane fade" id="tab-ns3a">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-ns3a" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                        
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-ns3a" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่เอกสารสิทธิ</label>
                                    <input type="text" name="parcelLandNo-ns3a" id="parcelLandNo-ns3a" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">อำเภอ/เขต</label>
                                    <select class="form-control select2 select2-danger amphur-select" style="margin-top: 8px;" id="amphur-select-ns3a"></select>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">ตำบล/แขวง</label>
                                    <select class="form-control select2 select2-danger tambon-select" style="margin-top: 8px;" id="tambon-select-ns3a"></select>
                                </div>
                            </div>
                            <div class="col-1">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-ns3a">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-ns3a">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขเอกสารสิทธิ</th>
                                        <th class="w-auto">อำเภอ</th>
                                        <th class="w-auto">ตำบล</th>
                                        <th class="w-auto status_header">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabPanel card" id="tabPanel-ns3a">
                    <ul class="nav nav-tabs nav-pills detail-pills detailTab-ns3a">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-ns3a">ข้อมูลเอกสารสิทธิ</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-ns3a">ผู้ถือกรรมสิทธิ์</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#olgt-detailTab-ns3a">ภาระผูกพัน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-ns3a">รายการจดทะเบียน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#sequester-detailTab-ns3a">ข้อมูลอายัด</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="parcel-detailTab-ns3a" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-ns3a-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลโฉนดที่ดิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="owner-detailTab-ns3a" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-ns3a-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลผู้ถือกรรมสิทธิ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="olgt-detailTab-ns3a" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="olgt-detailTab-ns3a-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="8">เปรียบเทียบข้อมูลภาระผูกพัน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="transaction-detailTab-ns3a" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p1-ns3a">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p2-ns3a">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sequester-detailTab-ns3a" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="sequester-detailTab-ns3a-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลอายัด</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div class="overlay overlayP2 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
                <!-- 6 tabs of process -->
                <div class="tab6Panel card" id="tab6Panel-ns3a">
                    <ul class="nav nav-tabs nav-pills process-pills processTab-ns3a">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#aps-processTab-ns3a">ราคาประเมิน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#promise-processTab-ns3a">คู่สัญญา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#obtain-processTab-ns3a">การได้มา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#process-processTab-ns3a">สอบสวน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#expense-processTab-ns3a">ค่าใช้จ่าย</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#document-processTab-ns3a">แบบพิมพ์</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="aps-processTab-ns3a" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="aps-processTab-ns3a-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลราคาประเมิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="promise-processTab-ns3a" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="promise-processTab-ns3a-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลคู่สัญญา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="obtain-processTab-ns3a" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="obtain-processTab-ns3a-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลการได้มา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="process-processTab-ns3a" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="process-processTab-ns3a-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลสอบสวน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="expense-processTab-ns3a" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <h5 style="text-align:center;" id="receipt_no"></h5>
                                </div>
                                <div class="col">
                                    <h5 style="text-align:center;" id="fin_no"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-ns3a-table-p1">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-ns3a-table-p2">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="document-processTab-ns3a" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="document-processTab-ns3a-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลแบบพิมพ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP1 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
            </div>
            <!-- nsl -->
            <div class="tab-pane fade" id="tab-nsl">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-nsl" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                          
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-nsl" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">เลขที่เอกสารสิทธิ</label>
                                    <input type="text" name="parcelLandNo-nsl" id="parcelLandNo-nsl" class="form-control">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for="">ปีที่ออก</label>
                                    <input type="text" name="parcelYear" id="parcelYear" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">อำเภอ/เขต</label>
                                    <select class="form-control select2 select2-danger amphur-select" style="margin-top: 8px;" id="amphur-select-nsl"></select>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">ตำบล/แขวง</label>
                                    <select class="form-control select2 select2-danger tambon-select" style="margin-top: 8px;" id="tambon-select-nsl"></select>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-nsl">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-nsl">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขเอกสารสิทธิ</th>
                                        <th class="w-auto">ชื่อ น.ส.ล.</th>
                                        <th class="w-auto">ปีที่ออก</th>
                                        <th class="w-auto">อำเภอ</th>
                                        <th class="w-auto">ตำบล</th>
                                        <th class="w-auto status_header">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabPanel card" id="tabPanel-nsl">
                    <ul class="nav nav-tabs nav-pills detail-pills detailTab-nsl">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-nsl">ข้อมูลเอกสารสิทธิ</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-nsl">ผู้ถือกรรมสิทธิ์</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#olgt-detailTab-nsl">ภาระผูกพัน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-nsl">รายการจดทะเบียน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#sequester-detailTab-nsl">ข้อมูลอายัด</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="parcel-detailTab-nsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-nsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลโฉนดที่ดิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="owner-detailTab-nsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-nsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลผู้ถือกรรมสิทธิ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="olgt-detailTab-nsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="olgt-detailTab-nsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="8">เปรียบเทียบข้อมูลภาระผูกพัน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="transaction-detailTab-nsl" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p1-nsl">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p2-nsl">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sequester-detailTab-nsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="sequester-detailTab-nsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลอายัด</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div class="overlay overlayP2 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
                <!-- 6 tabs of process -->
                <div class="tab6Panel card" id="tab6Panel-nsl">
                    <ul class="nav nav-tabs nav-pills process-pills processTab-nsl">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#aps-processTab-nsl">ราคาประเมิน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#promise-processTab-nsl">คู่สัญญา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#obtain-processTab-nsl">การได้มา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#process-processTab-nsl">สอบสวน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#expense-processTab-nsl">ค่าใช้จ่าย</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#document-processTab-nsl">แบบพิมพ์</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="aps-processTab-nsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="aps-processTab-nsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลราคาประเมิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="promise-processTab-nsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="promise-processTab-nsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลคู่สัญญา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="obtain-processTab-nsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="obtain-processTab-nsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลการได้มา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="process-processTab-nsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="process-processTab-nsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลสอบสวน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="expense-processTab-nsl" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <h5 style="text-align:center;" id="receipt_no"></h5>
                                </div>
                                <div class="col">
                                    <h5 style="text-align:center;" id="fin_no"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-nsl-table-p1">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-nsl-table-p2">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="document-processTab-nsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="document-processTab-nsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลแบบพิมพ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP1 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
            </div>
            <!-- subNsl -->
            <div class="tab-pane fade" id="tab-subNsl">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-subNsl" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                         
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-subNsl" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">เลขที่เอกสารสิทธิ</label>
                                    <input type="text" name="parcelLandNo-ns3" id="parcelLandNo-subNsl" class="form-control">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for="">หมู่</label>
                                    <input type="text" name="parcelLandMoo-subNsl" id="parcelLandMoo-subNsl" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">อำเภอ/เขต</label>
                                    <select class="form-control select2 select2-danger amphur-select" style="margin-top: 8px;" id="amphur-select-subNsl"></select>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">ตำบล/แขวง</label>
                                    <select class="form-control select2 select2-danger tambon-select" style="margin-top: 8px;" id="tambon-select-subNsl"></select>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-subNsl">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-subNsl">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขเอกสารสิทธิ</th>
                                        <th class="w-auto">หมู่</th>
                                        <th class="w-auto">อำเภอ</th>
                                        <th class="w-auto">ตำบล</th>
                                        <th class="w-auto status_header">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabPanel card" id="tabPanel-subNsl">
                    <ul class="nav nav-tabs nav-pills detail-pills detailTab-subNsl">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-subNsl">ข้อมูลเอกสารสิทธิ</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-subNsl">ผู้ถือกรรมสิทธิ์</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#olgt-detailTab-subNsl">ภาระผูกพัน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-subNsl">รายการจดทะเบียน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#sequester-detailTab-subNsl">ข้อมูลอายัด</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="parcel-detailTab-subNsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-subNsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลโฉนดที่ดิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="owner-detailTab-subNsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-subNsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลผู้ถือกรรมสิทธิ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="olgt-detailTab-subNsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="olgt-detailTab-subNsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="8">เปรียบเทียบข้อมูลภาระผูกพัน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="transaction-detailTab-subNsl" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p1-subNsl">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p2-subNsl">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sequester-detailTab-subNsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="sequester-detailTab-subNsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลอายัด</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div class="overlay overlayP2 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
                <!-- 6 tabs of process -->
                <div class="tab6Panel card" id="tab6Panel-subNsl">
                    <ul class="nav nav-tabs nav-pills process-pills processTab-subNsl">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#aps-processTab-subNsl">ราคาประเมิน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#promise-processTab-subNsl">คู่สัญญา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#obtain-processTab-subNsl">การได้มา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#process-processTab-subNsl">สอบสวน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#expense-processTab-subNsl">ค่าใช้จ่าย</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#document-processTab-subNsl">แบบพิมพ์</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="aps-processTab-subNsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="aps-processTab-subNsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลราคาประเมิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="promise-processTab-subNsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="promise-processTab-subNsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลคู่สัญญา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="obtain-processTab-subNsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="obtain-processTab-subNsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลการได้มา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="process-processTab-subNsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="process-processTab-subNsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลสอบสวน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="expense-processTab-subNsl" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <h5 style="text-align:center;" id="receipt_no"></h5>
                                </div>
                                <div class="col">
                                    <h5 style="text-align:center;" id="fin_no"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-subNsl-table-p1">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-subNsl-table-p2">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="document-processTab-subNsl" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="document-processTab-subNsl-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลแบบพิมพ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP1 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
            </div>
            <!-- condo -->
            <div class="tab-pane fade" id="tab-condo">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-condo" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                      
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-condo" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">เลขที่อาคารชุด</label>
                                    <input type="text" name="condo_reg" id="condo_reg" class="form-control">
                                </div>
                            </div>
                            <div class="col-7">
                                <div class="form-group">
                                    <label for="">ชื่ออาคารชุด</label>
                                    <input type="text" name="condo_name" id="condo_name" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">อำเภอ/เขต</label>
                                    <select class="form-control select2 select2-danger amphur-select" style="margin-top: 8px;" id="amphur-select-condo"></select>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">ตำบล/แขวง</label>
                                    <select class="form-control select2 select2-danger tambon-select" style="margin-top: 8px;" id="tambon-select-condo"></select>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-condo">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-condo">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">อำเภอ</th>
                                        <th class="w-auto">ตำบล</th>
                                        <th class="w-auto">เลขที่อาคารชุด</th>
                                        <th class="w-auto">ชื่ออาคารชุด</th>
                                        <th class="w-auto status_header">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabPanel card" id="tabPanel-condo">
                    <ul class="nav nav-tabs nav-pills detail-pills detailTab-condo">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-condo">ข้อมูลเอกสารสิทธิ</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-condo">รายการจดทะเบียน</a></li>
                        <!-- <li><a data-toggle="tab" class="nav-link" href="#sequester-detailTab-condo">ข้อมูลอายัด</a></li> -->
                    </ul>

                    <div class="tab-content">
                        <div id="parcel-detailTab-condo" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-condo-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลโฉนดที่ดิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="transaction-detailTab-condo" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p1-condo">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p2-condo">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sequester-detailTab-condo" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="sequester-detailTab-condo-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลอายัด</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div class="overlay overlayP2 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
                <!-- 6 tabs of process -->
                <div class="tab6Panel card" id="tab6Panel-condo">
                    <ul class="nav nav-tabs nav-pills process-pills processTab-condo">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#aps-processTab-condo">ราคาประเมิน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#promise-processTab-condo">คู่สัญญา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#obtain-processTab-condo">การได้มา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#process-processTab-condo">สอบสวน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#expense-processTab-condo">ค่าใช้จ่าย</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#document-processTab-condo">แบบพิมพ์</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="aps-processTab-condo" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="aps-processTab-condo-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลราคาประเมิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="promise-processTab-condo" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="promise-processTab-condo-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลคู่สัญญา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="obtain-processTab-condo" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="obtain-processTab-condo-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลการได้มา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="process-processTab-condo" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="process-processTab-condo-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลสอบสวน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="expense-processTab-condo" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <h5 style="text-align:center;" id="receipt_no"></h5>
                                </div>
                                <div class="col">
                                    <h5 style="text-align:center;" id="fin_no"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-condo-table-p1">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-condo-table-p2">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="document-processTab-condo" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="document-processTab-condo-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลแบบพิมพ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP1 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
            </div>
            <!-- condoroom -->
            <div class="tab-pane fade" id="tab-condoroom">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-condoroom" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                        
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-condoroom" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-2">
                                <div class="form-group">
                                    <label for="">เลขที่ห้อง</label>
                                    <input type="text" name="condoroom_no" id="condoroom_no" class="form-control">
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">เลขที่อาคารชุด</label>
                                    <input type="text" name="condoroom_reg" id="condoroom_reg" class="form-control">
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">ชื่ออาคารชุด</label>
                                    <input type="text" name="condoroom_name" id="condoroom_name" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">อำเภอ/เขต</label>
                                    <select class="form-control select2 select2-danger amphur-select" id="amphur-select-condoroom"></select>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">ตำบล/แขวง</label>
                                    <select class="form-control select2 select2-danger tambon-select" id="tambon-select-condoroom"></select>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" style="margin-top: 8px;" id="search-condoroom">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-condoroom">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">อำเภอ</th>
                                        <th class="w-auto">ตำบล</th>
                                        <th class="w-auto">เลขที่ห้อง</th>
                                        <th class="w-auto">เลขที่อาคารชุด</th>
                                        <th class="w-auto">ชื่ออาคารชุด</th>
                                        <th class="w-auto status_header">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabPanel card" id="tabPanel-condoroom">
                    <ul class="nav nav-tabs nav-pills detail-pills detailTab-condoroom">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-condoroom">ข้อมูลเอกสารสิทธิ</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-condoroom">ผู้ถือกรรมสิทธิ์</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#olgt-detailTab-condoroom">ภาระผูกพัน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-condoroom">รายการจดทะเบียน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#sequester-detailTab-condoroom">ข้อมูลอายัด</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="parcel-detailTab-condoroom" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-condoroom-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลโฉนดที่ดิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="owner-detailTab-condoroom" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-condoroom-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลผู้ถือกรรมสิทธิ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="olgt-detailTab-condoroom" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="olgt-detailTab-condoroom-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="8">เปรียบเทียบข้อมูลภาระผูกพัน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="transaction-detailTab-condoroom" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p1-condoroom">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p2-condoroom">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sequester-detailTab-condoroom" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="sequester-detailTab-condoroom-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลอายัด</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div class="overlay overlayP2 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
                <!-- 6 tabs of process -->
                <div class="tab6Panel card" id="tab6Panel-condoroom">
                    <ul class="nav nav-tabs nav-pills process-pills processTab-condoroom">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#aps-processTab-condoroom">ราคาประเมิน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#promise-processTab-condoroom">คู่สัญญา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#obtain-processTab-condoroom">การได้มา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#process-processTab-condoroom">สอบสวน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#expense-processTab-condoroom">ค่าใช้จ่าย</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#document-processTab-condoroom">แบบพิมพ์</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="aps-processTab-condoroom" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="aps-processTab-condoroom-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลราคาประเมิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="promise-processTab-condoroom" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="promise-processTab-condoroom-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลคู่สัญญา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="obtain-processTab-condoroom" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="obtain-processTab-condoroom-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลการได้มา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="process-processTab-condoroom" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="process-processTab-condoroom-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลสอบสวน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="expense-processTab-condoroom" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <h5 style="text-align:center;" id="receipt_no"></h5>
                                </div>
                                <div class="col">
                                    <h5 style="text-align:center;" id="fin_no"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-condoroom-table-p1">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-condoroom-table-p2">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="document-processTab-condoroom" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="document-processTab-condoroom-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลแบบพิมพ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP1 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
            </div>
            <!-- construct -->
            <div class="tab-pane fade" id="tab-construct">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-construct" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>

                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-construct" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">รหัสประจำบ้าน</label>
                                    <input type="text" name="construct_hid" id="construct_hid" class="form-control">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for="">เลขที่บ้าน</label>
                                    <input type="text" name="construct_hno" id="construct_hno" class="form-control">
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">ชื่อสิ่งปลูกสร้าง</label>
                                    <input type="text" name="construct_name" id="construct_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for="">หมู่</label>
                                    <input type="text" name="construct_moo" id="construct_moo" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">อำเภอ/เขต</label>
                                    <select class="form-control select2 select2-danger amphur-select" id="amphur-select-construct"></select>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group">
                                    <label for="">ตำบล/แขวง</label>
                                    <select class="form-control select2 select2-danger tambon-select" id="tambon-select-construct"></select>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" style="margin-top: 8px;" id="search-construct">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-construct">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">อำเภอ</th>
                                        <th class="w-auto">ตำบล</th>
                                        <th class="w-auto">รหัสประจำบ้าน</th>
                                        <th class="w-auto">เลขที่บ้าน</th>
                                        <th class="w-auto">หมู่</th>
                                        <th class="w-auto">ชื่อสิ่งปลูกสร้าง</th>
                                        <th class="w-auto status_header">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabPanel card" id="tabPanel-construct">
                    <ul class="nav nav-tabs nav-pills detail-pills detailTab-construct">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-construct">ข้อมูลเอกสารสิทธิ</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-construct">ผู้ถือกรรมสิทธิ์</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#olgt-detailTab-construct">ภาระผูกพัน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-construct">รายการจดทะเบียน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#sequester-detailTab-construct">ข้อมูลอายัด</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="parcel-detailTab-construct" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-construct-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลโฉนดที่ดิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="owner-detailTab-construct" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-construct-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลผู้ถือกรรมสิทธิ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="olgt-detailTab-construct" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="olgt-detailTab-construct-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="8">เปรียบเทียบข้อมูลภาระผูกพัน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="transaction-detailTab-construct" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p1-construct">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-p2-construct">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="5">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">วันที่</th>
                                                <th class="w-auto">เลขคิว</th>
                                                <th class="w-auto">ลำดับ</th>
                                                <th class="w-auto">รายการจดทะเบียน</th>
                                                <th class="w-auto status_header">รายละเอียด</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="sequester-detailTab-construct" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="sequester-detailTab-construct-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลอายัด</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                    <div class="overlay overlayP2 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
                <!-- 6 tabs of process -->
                <div class="tab6Panel card" id="tab6Panel-construct">
                    <ul class="nav nav-tabs nav-pills process-pills processTab-construct">
                        <li class="active"><a data-toggle="tab" class="nav-link active" href="#aps-processTab-construct">ราคาประเมิน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#promise-processTab-construct">คู่สัญญา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#obtain-processTab-construct">การได้มา</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#process-processTab-construct">สอบสวน</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#expense-processTab-construct">ค่าใช้จ่าย</a></li>
                        <li><a data-toggle="tab" class="nav-link" href="#document-processTab-construct">แบบพิมพ์</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="aps-processTab-construct" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="aps-processTab-construct-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลราคาประเมิน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="promise-processTab-construct" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="promise-processTab-construct-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลคู่สัญญา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="obtain-processTab-construct" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="obtain-processTab-construct-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลการได้มา</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="process-processTab-construct" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="process-processTab-construct-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลสอบสวน</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div id="expense-processTab-construct" class="tab-pane fade">
                            <div class="row">
                                <div class="col">
                                    <h5 style="text-align:center;" id="receipt_no"></h5>
                                </div>
                                <div class="col">
                                    <h5 style="text-align:center;" id="fin_no"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-construct-table-p1">
                                        <thead style="background-color: rgb(18 160 210);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col">
                                    <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-processTab-construct-table-p2">
                                        <thead style="background-color: rgb(46, 132, 39);color:white;">
                                            <tr style="text-align: center;">
                                                <th colspan="8">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th colspan="2">ค่าใช้จ่ายระบบทะเบียน</th>
                                                <th colspan="2" style="border-left:1px solid black;">ค่าใช้จ่ายระบบการเงิน</th>
                                            </tr>
                                            <tr style="text-align: center">
                                                <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                                <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                <th class="w-auto">ราคา (บาท)</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="document-processTab-construct" class="tab-pane fade">
                            <table class="table table-hover cell-border" style="width:100%;border:none;" id="document-processTab-construct-table">
                                <thead>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th colspan="3">เปรียบเทียบข้อมูลแบบพิมพ์</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                        <th colspan="2" class="w-auto">ค่าข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="overlay overlayP1 dark">
                        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                </div>
            </div>
    </div>
    </main>
    <!-- page-content" -->
    </div>

    <!-- jQuery -->
    <script src="./plugins/jquery/jquery.min.js"></script>
    <!-- <script src="plugins/vendor/jquery/jquery-3.2.1.min.js"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="./plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="./plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="./js/content.js"></script>
    <script src="./js/select.js"></script>
    <script src="./js/global.js"></script>
    <script src="./js/mas.js"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script>
        $(document).ready(function() {
            $(".form-row").keypress(function(event) {
                if (event.keyCode == 13) {
                    $("#search-" + type).click();
                }
            })
            $('.detail-pills a').on('click', function(e) {

                e.preventDefault()
                $('.tabPanel > .tab-content .tab-pane').css("display", "none")
                console.log(this);

                $($(this).attr("href")).show();


            })
            $('.process-pills a').on('click', function(e) {

                e.preventDefault()
                $('.tab6Panel > .tab-content .tab-pane').css("display", "none")
                console.log(this);

                $($(this).attr("href")).show();


            })
        });
    </script>
</body>

</html>