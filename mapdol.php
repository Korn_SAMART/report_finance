<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ข้อมูลรูปแปลงที่ดิน</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="./plugins/fontawesome-free/css/all.min.css">
    <script src="./plugins/fontawesome-free/js/all.min.js"></script>

    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="./style/style.css">
    <link rel="stylesheet" href="./dist/css/dash.css">
    <link rel="stylesheet" href="./plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="./plugins/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./dist/DataTables/datatables.min.css" />
    <link rel="stylesheet" href="./dist/DataTables/jquery.datatables.min.css" />

    <script src="./plugins/jquery/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./style/login.css">


    <link rel="shortcut icon" href="#" />

    <style>
        select {
            border: 1px solid #ccc;
            vertical-align: top;
            min-height: 20px;
        }
        .border-right {
            border-width: 0px thick 0px 0px;
        }
    </style>

</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div>
        <img id="main-bg" src="./img/dol.png" style="position: absolute;left: 31%;opacity: 0.15;width: 55vw;z-index: -1;">
    </div>
    <div class="page-wrapper chiller-theme toggled">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand">
                    <a href="#" id="landoffice_name"></a>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">Admin</span>
                        <span class="user-role">Administrator</span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-header  -->
                <div class="sidebar-menu">
                    <ul class="nav-pills-main">
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-summary" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-image"></i>
                                <span style="font-size: 17px;">ภาพรวมการถ่ายโอนข้อมูล</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-mapdol" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-image"></i>
                                <span style="font-size: 17px;">รูปแปลงที่ดินชั้นเผยแพร่</span>
                            </a>
                        </li>
                        <li class="sidebar">
                            <a data-toggle="pill" href="#tab-mapdol-temp" class="nav-link">
                                <i style="font-size: 17px;" class="fa-nav fas fa-image"></i>
                                <span style="font-size: 17px;">รูปแปลงที่ดินชั้นรอจดทะเบียน</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="sidebar-footer">
                <button class="btn btn-block" id="sign-out-btn" onclick="location.href='./portal.php';">กลับสู่หน้า Portal</button>
            </div>
        </nav>
        <!-- sidebar-wrapper  -->
        <main class="page-content">
            <div style="width: 100%;">
                <!-- mapdol -->
                <div class="tab-pane fade" id="tab-mapdol">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="form-inline">
                                <div class="col-auto" id="div-parcelType">
                                   <b> ประเภทรูปแปลงที่ดิน:</b>
                                    <select class="form-control" id="select-parcelType-mapdol"></select>
                                </div>
                                <div class="col-auto" id="div-printplateType">
                                    <b>ประเภทเอกสารสิทธิ:</b>
                                    <select class="form-control" id="select-printplateType-mapdol"></select>
                                </div>
                                <div class="col-auto">
                                    <b>ประเภทระวาง:</b>
                                    <select class="form-control" id="rvType-mapdol">
                                        <option value="1">ระวาง UTM</option>
                                        <option value="2">ระวางภาพถ่าย น.ส.3ก</option>
                                    </select>
                                </div>
                                <div class="col-auto">
                                    <b>โซน:</b>
                                    <select class="form-control" id="zone-mapdol">
                                        <option value="47">47</option>
                                        <option value="48">48</option>
                                    </select>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default searchBtn" type="submit" id="search-mapdol" style="margin-top: 0px">ค้นหา</button>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default clearBtn" type="submit" id="clear-mapdol" style="margin-top: 0px">ล้างข้อมูล</button>
                                </div>
                            </div>
                            <div class="form-inline" id="rv-utm-mapdol">
                                <div class="col-auto">
                                    <b>มาตราส่วนUTM:</b>
                                    <select class="form-control" id="utmscale-mapdol"></select>
                                </div>
                                <div class="col-auto">
                                    <b>ระวาง UTM: </b>
                                    <input type="text" class="form-control" id='utmmap1-mapdol' maxlength="4" size="4"></input>
                                    <select class="form-control" id="utmmap2-mapdol">
                                        <option value=""></option>
                                        <option value="1">I</option>
                                        <option value="2">II</option>
                                        <option value="3">III</option>
                                        <option value="4">IV</option>
                                    </select>
                                    <input type="text" class="form-control" id='utmmap3-mapdol' maxlength="4" size="4"></input>
                                    <b>-</b>
                                    <input type="text" class="form-control" id='utmmap4-mapdol' maxlength="3" size="3"></input>
                                </div>
                                <div class="col-auto">
                                    <b>เลขที่ดิน:</b>
                                    <input type="text" class="form-control" id='landno-mapdol' maxlength="20"></input>
                                </div>
                            </div>

                            <div class="form-inline" id="rv-ns3k-mapdol" >
                                <div class="col-auto">
                                    <b>มาตราส่วน น.ส.3ก:</b>
                                    <select class="form-control" id="utmscale-ns3k-mapdol"></select>
                                </div>
                                <div class="col-auto">
                                    <b>ระวางภาพถ่าย น.ส.3ก: </b>
                                    <input type="text" class="form-control" id='utmmap1-ns3k-mapdol' maxlength="4" size="4"></input>
                                    <select class="form-control" id="utmmap2-ns3k-mapdol">
                                        <option value=""></option>
                                        <option value="1">I</option>
                                        <option value="2">II</option>
                                        <option value="3">III</option>
                                        <option value="4">IV</option>
                                    </select>
                                    <b>-</b>
                                    <input type="text" class="form-control" id='utmmap4-ns3k-mapdol' maxlength="3" size="3"></input>
                                </div>
                                <div class="col-auto">
                                    <b>เลขที่ดิน:</b>
                                    <input type="text" class="form-control" id='landno-ns3k-mapdol' maxlength="20"></input>
                                </div>
                            </div>
                            <div class="card">
                                <table class="table table-hover table-bordered table-striped table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-mapdol">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th rowspan="2" class="w-auto"></th>
                                            <th colspan="8" class="w-auto th-p1 diff-p" >ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            <th colspan="8" class="w-auto th-p2" >ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto">เลขเอกสารสิทธิ</th>
                                            <th class="w-auto">อำเภอ</th>
                                            <th class="w-auto">ตำบล</th>
                                            <th class="w-auto">หมู่</th>
                                            <th class="w-auto">หมายเลขระวาง</th>
                                            <th class="w-auto">แผ่นที่</th>
                                            <th class="w-auto">มาตราส่วน</th>
                                            <th class="w-auto diff-p">เลขที่ดิน</th>
                                            <th class="w-auto">เลขเอกสารสิทธิ</th>
                                            <th class="w-auto">อำเภอ</th>
                                            <th class="w-auto">ตำบล</th>
                                            <th class="w-auto">หมู่</th>
                                            <th class="w-auto">หมายเลขระวาง</th>
                                            <th class="w-auto">แผ่นที่</th>
                                            <th class="w-auto">มาตราส่วน</th>
                                            <th class="w-auto">เลขที่ดิน</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="text-danger text-center" style="font-size: 18px">- จำนวนของข้อมูลที่แสดงอาจคลาดเคลื่อนจากตารางภาพรวม เนื่องจากเอกสารสิทธิบางสำนักงานมีข้อมูลเลขระวาง และเลขที่ดินซ้ำกัน -</div>
                                <div class="overlay overlay-main dark" id="main">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-mapdol-temp">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="form-inline">
                                <div class="col-auto" id="div-parcelType">
                                    <b>ประเภทรูปแปลงที่ดิน:</b>
                                    <select class="form-control" id="select-parcelType-mapdol-temp"></select>
                                </div>
                                <div class="col-auto" id="div-printplateType">
                                    <b>ประเภทเอกสารสิทธิ:</b>
                                    <select class="form-control" id="select-printplateType-mapdol-temp"></select>
                                </div>
                                <div class="col-auto">
                                    <b>ประเภทระวาง:</b>
                                    <select class="form-control" id="rvType-mapdol-temp">
                                        <option value="1">ระวาง UTM</option>
                                        <option value="2">ระวางภาพถ่าย น.ส.3ก</option>
                                    </select>
                                </div>
                                <div class="col-auto">
                                    <b>โซน:</b>
                                    <select class="form-control" id="zone-mapdol-temp">
                                        <option value="47">47</option>
                                        <option value="48">48</option>
                                    </select>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default searchBtn" type="submit" id="search-mapdol-temp" style="margin-top: 0px">ค้นหา</button>
                                </div>
                                <div class="col-auto form-group">
                                    <button class="btn btn-default clearBtn" type="submit" id="clear-mapdol-temp" style="margin-top: 0px">ล้างข้อมูล</button>
                                </div>
                            </div>
                            <div class="form-inline" id="rv-utm-mapdol-temp">
                                <div class="col-auto">
                                    <b>มาตราส่วนUTM:</b>
                                    <select class="form-control" id="utmscale-mapdol-temp"></select>
                                </div>
                                <div class="col-auto">
                                    <b>ระวาง UTM: </b>
                                    <input type="text" class="form-control" id='utmmap1-mapdol-temp' maxlength="4" size="4"></input>
                                    <select class="form-control" id="utmmap2-mapdol-temp">
                                        <option value=""></option>
                                        <option value="1">I</option>
                                        <option value="2">II</option>
                                        <option value="3">III</option>
                                        <option value="4">IV</option>
                                    </select>
                                    <input type="text" class="form-control" id='utmmap3-mapdol-temp' maxlength="4" size="4"></input>
                                    <b>-</b>
                                    <input type="text" class="form-control" id='utmmap4-mapdol-temp' maxlength="3" size="3"></input>
                                </div>
                                <div class="col-auto">
                                    <b>เลขที่ดิน:</b>
                                    <input type="text" class="form-control" id='landno-mapdol-temp' maxlength="20"></input>
                                </div>
                            </div>
                            <div class="form-inline" id="rv-ns3k-mapdol-temp" >
                                <div class="col-auto">
                                    <b>มาตราส่วน น.ส.3ก:</b>
                                    <select class="form-control" id="utmscale-ns3k-mapdol-temp"></select>
                                </div>
                                <div class="col-auto">
                                    <b>ระวางภาพถ่าย น.ส.3ก: </b>
                                    <input type="text" class="form-control" id='utmmap1-ns3k-mapdol-temp' maxlength="4" size="4"></input>
                                    <select class="form-control" id="utmmap2-ns3k-mapdol-temp">
                                        <option value=""></option>
                                        <option value="1">I</option>
                                        <option value="2">II</option>
                                        <option value="3">III</option>
                                        <option value="4">IV</option>
                                    </select>
                                    <b>-</b>
                                    <input type="text" class="form-control" id='utmmap4-ns3k-mapdol-temp' maxlength="3" size="3"></input>
                                </div>
                                <div class="col-auto">
                                    <b>เลขที่ดิน:</b>
                                    <input type="text" class="form-control" id='landno-ns3k-mapdol-temp' maxlength="20"></input>
                                </div>
                            </div>
                            <div class="card">
                                <div class="col-auto form-group d-flex justify-content-end">
                                    <button class="btn btn-outline-info" type="submit" id="notpublic-mapdol-temp" onclick="showNotPublic()">รูปแปลงที่ยังไม่เผยแพร่</button>
                                </div>
                                <table class="table table-hover table-bordered table-striped table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-mapdol-temp">
                                    <thead >
                                        <tr style="text-align: center;">
                                            <th rowspan="2" class="w-auto"></th>
                                            <th colspan="8" class="w-auto th-p1 diff-p" >ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                            <th colspan="8" class="w-auto th-p2" >ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            <th rowspan="2" class="w-auto"></th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto">เลขเอกสารสิทธิ</th>
                                            <th class="w-auto">อำเภอ</th>
                                            <th class="w-auto">ตำบล</th>
                                            <th class="w-auto">หมู่</th>
                                            <th class="w-auto">หมายเลขระวาง</th>
                                            <th class="w-auto">แผ่นที่</th>
                                            <th class="w-auto">มาตราส่วน</th>
                                            <th class="w-auto diff-p">เลขที่ดิน</th>
                                            <th class="w-auto">เลขเอกสารสิทธิ</th>
                                            <th class="w-auto">อำเภอ</th>
                                            <th class="w-auto">ตำบล</th>
                                            <th class="w-auto">หมู่</th>
                                            <th class="w-auto">หมายเลขระวาง</th>
                                            <th class="w-auto">แผ่นที่</th>
                                            <th class="w-auto">มาตราส่วน</th>
                                            <th class="w-auto">เลขที่ดิน</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="text-danger text-center" style="font-size: 18px">- จำนวนของข้อมูลที่แสดงอาจคลาดเคลื่อนจากตารางภาพรวม เนื่องจากเอกสารสิทธิบางสำนักงานมีข้อมูลเลขระวาง และเลขที่ดินซ้ำกัน -</div>
                                <div class="overlay overlay-main dark" id="main-temp">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-summary">
                    <div class="row justify-content-around">
                        <div class="col">
                            <div class="card" style="margin-top: 10px;">
                                <div class="form-inline d-flex justify-content-end">
                                    <div class="col-auto" id="div-printplateType">
                                        <b>ประเภทเอกสารสิทธิ:</b>
                                        <select class="form-control" id="select-printplateType-summary"></select>
                                    </div>
                                </div>
                                <div class="form-inline d-flex justify-content-end">
                                    รูปแปลงชั้นเผยแพร่:
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-success" type="submit" id="report-s-mapdol" style="margin-top: 5px"><i class="fas fa-file-excel"></i>  Export Success</button>
                                    </div>
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-danger" type="submit" id="report-e-mapdol" style="margin-top: 5px"><i class="fas fa-file-excel"></i>  Export Error</button>
                                    </div>
                                </div>
                                <div class="form-inline d-flex justify-content-end">
                                    รูปแปลงชั้นรอจดทะเบียน:
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-success" type="submit" id="report-s-mapdol-temp" style="margin-top: 5px; margin-bottom: 5px;"><i class="fas fa-file-excel"></i>  Export Success</button>
                                    </div>
                                    <div class="col-auto form-group">
                                        <button class="btn btn-outline-danger" type="submit" id="report-e-mapdol-temp" style="margin-top: 5px; margin-bottom: 5px;"><i class="fas fa-file-excel"></i>  Export Error</button>
                                    </div>
                                </div>
                                <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-summary2">
                                    <thead>
                                        <tr style="text-align: center;">
                                            <th rowspan="2" class="w-auto th-p2"></th>
                                            <th rowspan="2" class="w-auto th-p2">ประเภทเอกสารสิทธิ</th>
                                            <th colspan="3" class="w-auto th-p2">ชั้นเผยแพร่</th>
                                            <th colspan="3" class="w-auto th-p2">ชั้นรอจดทะเบียน</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th class="w-auto th-p2">รับมอบ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนสำเร็จ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนไม่สำเร็จ</th>
                                            <th class="w-auto th-p2">รับมอบ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนสำเร็จ</th>
                                            <th class="w-auto th-p2">ถ่ายโอนไม่สำเร็จ</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="overlay overlay-main dark" id="summary">
                                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="text" style="display:none;" id="temp" value=""></input>
        </main>
    <!-- page-content" -->
    </div>

    <!-- jQuery -->
    <script src="./plugins/jquery/jquery.min.js"></script>
    <!-- <script src="plugins/vendor/jquery/jquery-3.2.1.min.js"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="./dist/js/adminlte.js"></script>
    <script src="./plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="./plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="./js/mas.js"></script>
    <script src="./js/mapdol.js"></script>
    <script src="./js/select.js"></script>
    <script src="./js/global.js"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script>
       
    </script>
</body>

</html>