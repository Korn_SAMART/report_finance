<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Portal</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" />
    


    <!-- Theme style -->
    <link rel="stylesheet" href="./plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="./style/style.css">
    <link rel="stylesheet" href="./dist/css/dash.css">
    <link rel="stylesheet" href="./plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="./plugins/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./dist/DataTables/datatables.min.css" />
    <link rel="stylesheet" href="./dist/DataTables/jquery.datatables.min.css" />
    <link rel="stylesheet" href="./style/portal.css" />

    <script src="./plugins/jquery/jquery.min.js"></script>


    <link rel="shortcut icon" href="#" />
</head>

<body>
    <!-- <div class="row">
        <div class=" col-auto d-flex justify-content-end">
            <span id="landoffice">...</span>
        </div>
    </div> -->
    <div class="row">
        <div class="col-6 col-sm-3 col-md-9">

        </div>

        <div class="col-4 col-md-2">
            <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#changeLandoffice"><i class="fa fa-info" aria-hidden="true"></i> เปลี่ยนสำนักงานที่ดิน</button>
        </div>
        <div class="col-3 col-md-1">
            <button class="btn btn-block btn-danger" onclick="clearCookie()"><i class="fa fa-sign-out" aria-hidden="true"></i> ออกจากระบบ</button>

        </div>
    </div>
    <div class="page-wrapper chiller-theme toggled main-portal">
        <div class="row justify-content-center gy-8">
            <div class="col-4">
                <div class="portal-transaction-btn portal-btn" onclick="location.href='./main.php';">
                    <i class="fa-3x fa fa-file-text"></i>
                    <p>ข้อมูลทะเบียนที่ดินและรายการจดทะเบียน (Transaction)</p>
                </div>
            </div>
            <div class="col-4">
                <div class="portal-transaction-btn portal-btn" onclick="location.href='./svo.php';">
                    <i class="fa-3x fa fa-map"></i>
                    <p>ข้อมูลงานรังวัดและแผนที่</p>
                </div>
            </div>
            <div class="col-4">
                <div class="portal-transaction-btn portal-btn" onclick="location.href='./fin.php';">
                    <i class="fa-3x fa fa-money"></i>
                    <p>ข้อมูลงานการเงินและบัญชี</p>
                </div>
            </div>



        </div>
        <div class="row justify-content-center gy-5">
            <div class="col-4">
                <div class="portal-transaction-btn portal-btn" onclick="location.href='./image.php';">
                    <i class="fa-3x fa fa-photo"></i>
                    <p>ข้อมูลภาพลักษณ์เอกสารสิทธิ/สารบบ</p>
                </div>
            </div>
            <div class="col-4">
                <div class="portal-transaction-btn portal-btn" onclick="location.href='./cadastral.php';">
                    <i class="fa-3x fa fa-calculator"></i>
                    <p>ข้อมูลภาพลักษณ์ต้นร่างและแบบคำนวณเนื้อที่</p>
                </div>
            </div>
            <div class="col-4">
                <div class="portal-transaction-btn portal-btn" onclick="location.href='./ctn.php';">
                    <i class="fa-3x fa fa-sitemap"></i>
                    <p>ข้อมูลงานอำนวยการ</p>
                </div>
            </div>




        </div>
        <div class="row justify-content-center gy-5">
            <div class="col-4">
                <div class="portal-transaction-btn portal-btn" onclick="location.href='./mapdol.php';">
                    <i class="fa-3x fa fa-file-image-o"></i>
                    <p>ข้อมูลรูปแปลงที่ดิน</p>
                </div>
            </div>
            <div class="col-4">
                <div class="portal-transaction-btn portal-btn" onclick="location.href='./master.php';">
                    <i class="fa-3x fa fa-table"></i>
                    <p>ข้อมูลพื้นฐาน (Master Table)</p>
                </div>
            </div>
            <div class="col-4">
                <div class="portal-transaction-btn portal-btn" onclick="location.href='./backlog.php';">
                    <i class="fa-3x fa fa-info-circle"></i>
                    <p>ข้อมูลงานค้าง</p>
                </div>
            </div>
        </div>




        <!-- Modal -->
        <div class="modal fade" id="changeLandoffice" tabindex="-1" role="dialog" aria-labelledby="changeLandoffice" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">เปลี่ยนสำนักงานที่ดิน</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container" id="container">
                            <div class="form-container sign-up-container">
                                <div class="branch-div">
                                    <select class="form-control select2 select2-danger select-amphur" data-dropdown-css-class="select2-danger" style="width: 100%;" id="select-province" required></select><br>
                                    <select class="form-control select2 select2-danger select-amphur" data-dropdown-css-class="select2-danger" style="width: 100%;" id="select-branch" required></select><br>
                                    <center><button class="btn btn-success btn-block" onClick="changeLandoffice()">ยืนยัน</button></center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <script src="./plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="./plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="./plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="./js/select.js"></script>
    <script src="./js/global.js"></script>


    <script>
        var c = getCookie("landoffice");
        if (c == "") {
            location.href = './index.html'
        }

        function changeLandoffice() {
            landoffice = $('#select-branch').val().split(',')[0];
            console.log(landoffice);
            document.cookie = "landoffice=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            setCookie('landoffice', landoffice);
            document.cookie = "branchName=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            setCookie('branchName', $('#select-branch option:selected').text());

            if (landoffice != '') {
                location.href = './portal.php';
            } else {
                $('#require_modal').modal();
            }


        }

    </script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>