<?php
    // session_start();
    // if(!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')){
    //     include '403.php';
    //     exit(0);
    // }

    include '../database/conn.php';

    $user = $_POST['user'];
    $pass = $_POST['pass'];

    $check = 'SELECT FU.* ,VL.*
            FROM DATAM.TB_FIXS_USER FU
            LEFT JOIN DATAM.VW_TB_MAS_LANDOFFICE VL
                ON FU.USER_ORG = VL.LANDOFFICE_SEQ
            WHERE FU.USER_LOGIN = :userName';
            
    $stid = oci_parse($conn, $check);
    oci_bind_by_name($stid, ':userName', $user);
    oci_execute($stid);
    $Result = [];
    $rowCount = 0;
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        if(password_verify($pass, $row['USER_PASSWORD'])){
            $rowCount = 1;
            if($user == 'Admin'){
            $Result[] = array("STATUS"=>"TRUE",
                            "USER_LOGIN"=>$row['USER_LOGIN'],
                            "USER_PASSWORD"=>$row['USER_PASSWORD']);
            }else {
            $Result[] = array("STATUS"=>"TRUE",
                            "USER_LOGIN"=>$row['USER_LOGIN'],
                            "USER_PASSWORD"=>$row['USER_PASSWORD'],
                            "USER_ORG"=>$row['USER_ORG'],
                            "USER_ORG_NAME"=>$row['LANDOFFICE_NAME_TH']);

            }
            if($user == 'Admin'){
                $_SESSION['User'] = [
                            'Login' => $row['USER_LOGIN'],
                            'Role' => $row['USER_ROLE'],
                            'Name' => $row['USER_NAME']
                ];
            }else{
                $_SESSION['User'] = [
                            'Login' => $row['USER_LOGIN'],
                            'Role' => $row['USER_ROLE'],
                            'Name' => $row['USER_NAME'],
                            'Org' => $row['USER_ORG'],
                ];
            }
        }
    }

    
    if($rowCount==0){
        $Result[] = array("STATUS"=>"FALSE");
    }

    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_close($conn);

?>