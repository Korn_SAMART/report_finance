<?php

function countImage($printplateType, $type, $landoffice){
    include '../database/conn.php';

    if($type=='err'){
        if($printplateType == 1){
            $sql = "SELECT COUNT(*) AS COUNT
                    FROM (
                        SELECT PI.PARCEL_IMAGE_SEQ
                            FROM MGT1.TB_REG_PARCEL P 
                            INNER JOIN MGT1.TB_REG_PARCEL_IMAGE PI 
                                ON P.PARCEL_SEQ  = PI.PARCEL_SEQ 
                                AND PI.RECORD_STATUS IN ('N', 'W', 'E') 
                            WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND P.RECORD_STATUS = 'N'
                        MINUS
                            SELECT PI.PARCEL_IMAGE_SEQ
                            FROM REG.TB_REG_PARCEL P 
                            INNER JOIN REG.TB_REG_PARCEL_IMAGE PI 
                                ON P.PARCEL_SEQ  = PI.PARCEL_SEQ 
                                AND PI.RECORD_STATUS IN ('N', 'W', 'E') 
                            WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND P.RECORD_STATUS = 'N'
                    ) M
                    INNER JOIN MGT1.TB_REG_PARCEL_IMAGE PI
                        ON PI.PARCEL_IMAGE_SEQ = M.PARCEL_IMAGE_SEQ
                        AND PI.RECORD_STATUS IN ('N', 'W', 'E') 
                    INNER JOIN MGT1.TB_REG_PARCEL P
                        ON P.PARCEL_SEQ = PI.PARCEL_SEQ
                        AND P.RECORD_STATUS = 'N'
                    ";
        } else if ($printplateType==10){
            $sql = "SELECT COUNT(*) AS COUNT
                    FROM (
                        SELECT CONDOROOM_IMAGE_SEQ
                        FROM MGT1.TB_REG_CONDOROOM C
                        LEFT JOIN MGT1.TB_REG_CONDO_BLD B
                            ON B.BLD_SEQ = C.BLD_SEQ
                            AND B.RECORD_STATUS = 'N'
                        LEFT JOIN MGT1.TB_REG_CONDO CD
                            ON CD.CONDO_SEQ = B.CONDO_SEQ
                            AND CD.RECORD_STATUS = 'N'
                        INNER JOIN MGT1.TB_REG_CONDOROOM_IMAGE CI
                            ON CI.CONDOROOM_SEQ = C.CONDOROOM_SEQ
                            AND CI.RECORD_STATUS IN ('W', 'N', 'E')
                        WHERE C.LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND C.RECORD_STATUS = 'N'
                        MINUS
                        SELECT CONDOROOM_IMAGE_SEQ
                        FROM REG.TB_REG_CONDOROOM C
                        LEFT JOIN REG.TB_REG_CONDO_BLD B
                            ON B.BLD_SEQ = C.BLD_SEQ
                            AND B.RECORD_STATUS = 'N'
                        LEFT JOIN REG.TB_REG_CONDO CD
                            ON CD.CONDO_SEQ = B.CONDO_SEQ
                            AND CD.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_CONDOROOM_IMAGE CI
                            ON CI.CONDOROOM_SEQ = C.CONDOROOM_SEQ
                            AND CI.RECORD_STATUS IN ('W', 'N', 'E')
                        WHERE C.LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND C.RECORD_STATUS = 'N'
                    ) M
                    INNER JOIN MGT1.TB_REG_CONDOROOM_IMAGE CI
                        ON M.CONDOROOM_IMAGE_SEQ = CI.CONDOROOM_IMAGE_SEQ
                        AND CI.RECORD_STATUS IN ('W', 'N', 'E')
                    INNER JOIN MGT1.TB_REG_CONDOROOM C
                        ON CI.CONDOROOM_SEQ = C.CONDOROOM_SEQ
                        AND C.RECORD_STATUS = 'N' ";
        } else if ($printplateType==11){
    
        } else if ($printplateType==13){
    
        } else {
            $sql = "SELECT COUNT(*) AS COUNT
                    FROM (
                        SELECT PI.PARCEL_LAND_IMAGE_SEQ
                            FROM MGT1.TB_REG_PARCEL_LAND P 
                            INNER JOIN MGT1.TB_REG_PARCEL_LAND_IMAGE PI 
                                ON P.PARCEL_LAND_SEQ  = PI.PARCEL_LAND_SEQ 
                                AND PI.RECORD_STATUS IN ('N', 'W', 'E') 
                            WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND P.RECORD_STATUS = 'N'
                        MINUS
                            SELECT PI.PARCEL_LAND_IMAGE_SEQ
                            FROM REG.TB_REG_PARCEL_LAND P 
                            INNER JOIN REG.TB_REG_PARCEL_LAND_IMAGE PI 
                                ON P.PARCEL_LAND_SEQ  = PI.PARCEL_LAND_SEQ 
                                AND PI.RECORD_STATUS IN ('N', 'W', 'E') 
                            WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND P.RECORD_STATUS = 'N'
                    ) M
                    INNER JOIN MGT1.TB_REG_PARCEL_LAND_IMAGE PI
                        ON PI.PARCEL_LAND_IMAGE_SEQ = M.PARCEL_LAND_IMAGE_SEQ
                        AND PI.RECORD_STATUS IN ('N', 'W', 'E') 
                    INNER JOIN MGT1.TB_REG_PARCEL_LAND P
                        ON P.PARCEL_LAND_SEQ = PI.PARCEL_LAND_SEQ
                        AND P.RECORD_STATUS = 'N'
                    ";
        }

    } else {
        // $landoffice = $_REQUEST['landoffice'];
        // $printplateType = $_REQUEST['printplateType'];
    
        if($printplateType == 1){
            $sql = "WITH P1 AS (
                        SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_NO, PARCEL_SURVEY_NO, P.PARCEL_SEQ, P.PRINTPLATE_TYPE_SEQ
                            ,PI.PARCEL_IMAGE_SEQ, PI.PARCEL_IMAGE_ORDER, PI.PARCEL_IMAGE_PNAME, PI.PARCEL_IMAGE_FILENAME_, PI.PARCEL_IMAGE_URL_
                        FROM MGT1.TB_REG_PARCEL P 
                        INNER JOIN MGT1.TB_REG_PARCEL_IMAGE PI 
                            ON P.PARCEL_SEQ  = PI.PARCEL_SEQ 
                            AND PI.RECORD_STATUS IN ('N', 'W', 'E') 
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND P.RECORD_STATUS = 'N'
                    ), P2 AS (
                        SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_NO, PARCEL_SURVEY_NO, P.PARCEL_SEQ, P.PRINTPLATE_TYPE_SEQ
                            ,PI.PARCEL_IMAGE_SEQ, PI.PARCEL_IMAGE_ORDER, PI.PARCEL_IMAGE_PNAME, PI.PARCEL_IMAGE_FILENAME, PI.PARCEL_IMAGE_URL
                        FROM REG.TB_REG_PARCEL P 
                        INNER JOIN REG.TB_REG_PARCEL_IMAGE PI 
                            ON P.PARCEL_SEQ  = PI.PARCEL_SEQ 
                            AND PI.RECORD_STATUS IN ('N', 'W', 'E') 
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND P.RECORD_STATUS = 'N'
                    )
                    SELECT COUNT(*) AS COUNT
                    FROM (
                        SELECT * FROM P2
                        INTERSECT
                        SELECT * FROM P1
                        ) P3
                    ";
        } else if ($printplateType==10){
            $sql = "WITH P1 AS (  
                SELECT C.CONDOROOM_SEQ AS SEQ, C.CONDOROOM_RNO AS NO, CD.AMPHUR_SEQ, CD.TAMBOL_SEQ, CD.CONDO_ID, TO_NUMBER(CD.CONDO_REG_YEAR) AS YEAR, CD.CONDO_NAME_TH AS NAME, C.PRINTPLATE_TYPE_SEQ
                    ,CI.CONDOROOM_IMAGE_SEQ AS IMAGE_SEQ, CI.CONDOROOM_IMAGE_ORDER AS IMAGE_ORDER, CI.CONDOROOM_IMAGE_PNAME AS IMAGE_PNAME
                    ,CI.CONDOROOM_IMAGE_FILENAME_ AS FNAME
                    ,CI.CONDOROOM_IMAGE_URL_ AS FURL
                FROM MGT1.TB_REG_CONDOROOM C
                LEFT JOIN MGT1.TB_REG_CONDO_BLD B
                    ON B.BLD_SEQ = C.BLD_SEQ
                    AND B.RECORD_STATUS = 'N'
                LEFT JOIN MGT1.TB_REG_CONDO CD
                    ON CD.CONDO_SEQ = B.CONDO_SEQ
                    AND CD.RECORD_STATUS = 'N'
                INNER JOIN MGT1.TB_REG_CONDOROOM_IMAGE CI
                    ON CI.CONDOROOM_SEQ = C.CONDOROOM_SEQ
                    AND CI.RECORD_STATUS IN ('W', 'N', 'E')
                WHERE C.LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND C.RECORD_STATUS = 'N'
            ), P2 AS (
                SELECT C.CONDOROOM_SEQ AS SEQ, C.CONDOROOM_RNO AS NO, CD.AMPHUR_SEQ, CD.TAMBOL_SEQ, CD.CONDO_ID, TO_NUMBER(CD.CONDO_REG_YEAR) AS YEAR, CD.CONDO_NAME_TH AS NAME, C.PRINTPLATE_TYPE_SEQ
                    ,CI.CONDOROOM_IMAGE_SEQ AS IMAGE_SEQ, CI.CONDOROOM_IMAGE_ORDER AS IMAGE_ORDER, CI.CONDOROOM_IMAGE_PNAME AS IMAGE_PNAME
                    ,CI.CONDOROOM_IMAGE_FILENAME AS FNAME
                    ,CI.CONDOROOM_IMAGE_URL AS FURL
                FROM REG.TB_REG_CONDOROOM C
                LEFT JOIN REG.TB_REG_CONDO_BLD B
                    ON B.BLD_SEQ = C.BLD_SEQ
                    AND B.RECORD_STATUS = 'N'
                LEFT JOIN REG.TB_REG_CONDO CD
                    ON CD.CONDO_SEQ = B.CONDO_SEQ
                    AND CD.RECORD_STATUS = 'N'
                INNER JOIN REG.TB_REG_CONDOROOM_IMAGE CI
                    ON CI.CONDOROOM_SEQ = C.CONDOROOM_SEQ
                    AND CI.RECORD_STATUS IN ('W', 'N', 'E')
                WHERE C.LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND C.RECORD_STATUS = 'N'
            )
            SELECT COUNT(*) AS COUNT
            FROM (
                SELECT * FROM P2
                INTERSECT
                SELECT * FROM P1
                ) P3 ";
        } else if ($printplateType==11){
    
        } else if ($printplateType==13){
    
        } else {
            $sql = "WITH P1 AS (
                        SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_NO, PARCEL_LAND_SURVEY_NO, P.PARCEL_LAND_SEQ, P.PRINTPLATE_TYPE_SEQ
                            ,P.PARCEL_LAND_MOO, P.PARCEL_LAND_NAME, P.PARCEL_LAND_OBTAIN_DTM AS YEAR
                            ,PI.PARCEL_LAND_IMAGE_SEQ, PI.PARCEL_LAND_IMAGE_ORDER, PI.PARCEL_LAND_IMAGE_PNAME, PI.PARCEL_LAND_IMAGE_FILENAME_, PI.PARCEL_LAND_IMAGE_URL_
                        FROM MGT1.TB_REG_PARCEL_LAND P 
                        INNER JOIN MGT1.TB_REG_PARCEL_LAND_IMAGE PI 
                            ON P.PARCEL_LAND_SEQ  = PI.PARCEL_LAND_SEQ 
                            AND PI.RECORD_STATUS IN ('N', 'W', 'E') 
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND P.RECORD_STATUS = 'N'
                    ), P2 AS (
                        SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_NO, PARCEL_LAND_SURVEY_NO, P.PARCEL_LAND_SEQ, P.PRINTPLATE_TYPE_SEQ
                            ,P.PARCEL_LAND_MOO, P.PARCEL_LAND_NAME, P.PARCEL_LAND_OBTAIN_DTM AS YEAR
                            ,PI.PARCEL_LAND_IMAGE_SEQ, PI.PARCEL_LAND_IMAGE_ORDER, PI.PARCEL_LAND_IMAGE_PNAME, PI.PARCEL_LAND_IMAGE_FILENAME, PI.PARCEL_LAND_IMAGE_URL
                        FROM REG.TB_REG_PARCEL_LAND P 
                        INNER JOIN REG.TB_REG_PARCEL_LAND_IMAGE PI 
                            ON P.PARCEL_LAND_SEQ  = PI.PARCEL_LAND_SEQ 
                            AND PI.RECORD_STATUS IN ('N', 'W', 'E') 
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND P.RECORD_STATUS = 'N'
                    )
                    SELECT COUNT(*) AS COUNT
                    FROM (
                        SELECT * FROM P2
                        INTERSECT
                        SELECT * FROM P1
                        ) P3
                    ";
        }
    }

    $Result = array();

    // echo $sql."\n";
    if($printplateType!=11 && $printplateType != 13){
        $stid = oci_parse($conn, $sql);
        oci_bind_by_name($stid, ':landoffice', $landoffice);
        oci_bind_by_name($stid, ':printplateType', $printplateType);
        oci_execute($stid);
        while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
            $Result[] = $row;
        }
    }
    // echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);

    return $Result[0]['COUNT'];
}