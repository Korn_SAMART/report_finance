<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';

    $landoffice = !isset($_POST['landoffice'])? '' : $_POST['landoffice'];

    // ข้อมูลหนังสือรับ / ส่ง
    $register_no = !isset($_POST['register_no']) ? '' : $_POST['register_no'];
    $secret_type = !isset($_POST['secret_type'])? '' : $_POST['secret_type'];
    $doc_no = !isset($_POST['doc_no'])? '' : $_POST['doc_no'];
    $doc_date = !isset($_POST['doc_date'])? '' : $_POST['doc_date'];

    //เลขที่คำสั่ง
    $order_no = !isset($_POST['order_no'])? '' : $_POST['order_no'];
    $recv_date = !isset($_POST['recv_date'])? '' : $_POST['recv_date'];
    $year = !isset($_POST['year']) ? '' : $_POST['year'];

    //ทะเบียนคุมอาคาร
    $con_type = !isset($_POST['con_type'])? '' : $_POST['con_type'];
    $con_id = !isset($_POST['con_id'])? '' : $_POST['con_id'];
    $con_name = !isset($_POST['con_name'])? '' : $_POST['con_name'];

    //ทะเบียนคุมครุภัณฑ์
    $inv_type = !isset($_POST['inv_type'])? '' : $_POST['inv_type'];
    $inv_id = !isset($_POST['inv_id'])? '' : $_POST['inv_id'];
    $inv_name = !isset($_POST['inv_name'])? '' : $_POST['inv_name'];

    //แบบพิมพ์การเงิน + แบบพิมพ์ทั่วไป + แบบพิมพ์แสดงสิทธิในที่ดิน
    $docfin_type = !isset($_POST['docfin_type'])? '' : $_POST['docfin_type'];
    $docfin_no = !isset($_POST['docfin_no'])? '' : $_POST['docfin_no'];

    //รายการคุมวัสดุ
    $mat_type = !isset($_POST['mat_type'])? '' : $_POST['mat_type'];

    //รายการคุมหลักเขต
    $dis_type = !isset($_POST['dis_type'])? '' : $_POST['dis_type'];

    //บุคลากร + การลา
    $psn_seq = !isset($_POST['psn_seq'])? '' : $_POST['psn_seq'];
    $record_seq = !isset($_POST['record_seq'])? '' : $_POST['record_seq'];
    $adm_id = !isset($_POST['adm_id'])? '' : $_POST['adm_id'];
    $adm_firstname = !isset($_POST['adm_firstname'])? '' : $_POST['adm_firstname'];
    $adm_lastname = !isset($_POST['adm_lastname'])? '' : $_POST['adm_lastname'];
    $landoffice_type = !isset($_POST['landoffice_type'])? '' : $_POST['landoffice_type'];

    $check = $_POST['check'];
    $Result = array();

    include 'queryCtnData.php';
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
        
    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
