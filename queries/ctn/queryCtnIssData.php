<?php
header('Content-Type: application/json; charset=utf-8');
//require_once 'isXmlHttpRequest.php';
include '../../database/conn.php';

$select = "WITH P1 AS (
                SELECT HD.REV_HD_SEQ, ISS.ISS_HD_NO,ISS.ISS_HD_DATE,ISS.ISS_HD_PAY_DATE,CASE ISS.ISS_HD_SEND_FLAG WHEN '0' THEN 'รับด้วยตนเอง' WHEN '1' THEN 'จัดส่งจากหน่วยงานผู้ให้เบิก' ELSE '-' END AS ISS_HD_SEND_FLAG,
                CASE ISS.ISS_HD_TYPE_FLAG WHEN '0' THEN 'ขอเบิกภายในหน่วยงาน' WHEN '1' THEN 'ขอเบิกระหว่างสำนักงานที่ดินแบบตามลำดับ' WHEN '2' THEN 'ขอเบิกระหว่างสำนักงานที่ดินแบบขั้นตอนเดียว' 
                WHEN '3' THEN 'ขอเบิกระหว่างสำนักงานที่ดินกับกองพัสดุแบบตามลำดับ' WHEN '4' THEN 'ขอเบิกระหว่างสำนักงานที่ดินกับกองพัสดุแบบขั้นตอนเดียว' ELSE '-' END AS ISS_HD_TYPE_FLAG,
                CASE ISS.ISS_HD_TOPIC_FLAG WHEN '0' THEN 'เบิกแบบพิมพ์สำรองจ่าย' WHEN '1' THEN 'เบิกสิ่งของ' WHEN '2' THEN 'การยืมการโอน' ELSE '-' END AS ISS_HD_TOPIC_FLAG,
                CASE ISS.ISS_HD_STS WHEN '0' THEN 'ร่างใบขอเบิก' WHEN '1' THEN 'รอเบิกจ่าย' WHEN '2' THEN 'ยกเลิกใบเบิก' WHEN '3' THEN 'เสนออนุมัติ' WHEN '4' THEN 'อนุมัติ' WHEN '5' THEN 'ไม่อนุมัติ' 
                WHEN '6' THEN 'เสนอแก้ไข' WHEN '7' THEN 'รับของ' ELSE '-' END AS ISS_HD_STS,
                ISS.PSN_NAME,ISS.PSN_G_NAME
                FROM MGT1.TB_INV_REV_HD HD
                LEFT OUTER JOIN MGT1.TB_INV_ISS_HD ISS
                ON HD.ISS_HD_SEQ = ISS.ISS_HD_SEQ
                WHERE HD.REV_HD_SEQ = :revhdSeqP1
            ),
            P2 AS (
                SELECT HD.REV_HD_SEQ, ISS.ISS_HD_NO,ISS.ISS_HD_DATE,ISS.ISS_HD_PAY_DATE,CASE ISS.ISS_HD_SEND_FLAG WHEN '0' THEN 'รับด้วยตนเอง' WHEN '1' THEN 'จัดส่งจากหน่วยงานผู้ให้เบิก' ELSE '-' END AS ISS_HD_SEND_FLAG,
                CASE ISS.ISS_HD_TYPE_FLAG WHEN '0' THEN 'ขอเบิกภายในหน่วยงาน' WHEN '1' THEN 'ขอเบิกระหว่างสำนักงานที่ดินแบบตามลำดับ' WHEN '2' THEN 'ขอเบิกระหว่างสำนักงานที่ดินแบบขั้นตอนเดียว' 
                WHEN '3' THEN 'ขอเบิกระหว่างสำนักงานที่ดินกับกองพัสดุแบบตามลำดับ' WHEN '4' THEN 'ขอเบิกระหว่างสำนักงานที่ดินกับกองพัสดุแบบขั้นตอนเดียว' ELSE '-' END AS ISS_HD_TYPE_FLAG,
                CASE ISS.ISS_HD_TOPIC_FLAG WHEN '0' THEN 'เบิกแบบพิมพ์สำรองจ่าย' WHEN '1' THEN 'เบิกสิ่งของ' WHEN '2' THEN 'การยืมการโอน' ELSE '-' END AS ISS_HD_TOPIC_FLAG,
                CASE ISS.ISS_HD_STS WHEN '0' THEN 'ร่างใบขอเบิก' WHEN '1' THEN 'รอเบิกจ่าย' WHEN '2' THEN 'ยกเลิกใบเบิก' WHEN '3' THEN 'เสนออนุมัติ' WHEN '4' THEN 'อนุมัติ' WHEN '5' THEN 'ไม่อนุมัติ' 
                WHEN '6' THEN 'เสนอแก้ไข' WHEN '7' THEN 'รับของ' ELSE '-' END AS ISS_HD_STS,
                ISS.PSN_NAME,ISS.PSN_G_NAME
                FROM INV.TB_INV_REV_HD HD
                LEFT OUTER JOIN INV.TB_INV_ISS_HD ISS
                ON HD.ISS_HD_SEQ = ISS.ISS_HD_SEQ
                WHERE HD.REV_HD_SEQ = :revhdSeqP2
            )
            SELECT P1.REV_HD_SEQ AS REV_HD_SEQ_P1, P2.REV_HD_SEQ AS REV_HD_SEQ_P2,
            P1.ISS_HD_NO AS ISS_HD_NO_P1, P2.ISS_HD_NO AS ISS_HD_NO_P2,
            CASE WHEN SUBSTR(P1.ISS_HD_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P1.ISS_HD_DATE) ELSE TO_CHAR(P1.ISS_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS ISS_HD_DATE_P1,
            CASE WHEN SUBSTR(P2.ISS_HD_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P2.ISS_HD_DATE) ELSE TO_CHAR(P2.ISS_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS ISS_HD_DATE_P2,
            CASE WHEN SUBSTR(P1.ISS_HD_PAY_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P1.ISS_HD_PAY_DATE) ELSE TO_CHAR(P1.ISS_HD_PAY_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS ISS_HD_PAY_DATE_P1,
            CASE WHEN SUBSTR(P2.ISS_HD_PAY_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P2.ISS_HD_PAY_DATE) ELSE TO_CHAR(P2.ISS_HD_PAY_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS ISS_HD_PAY_DATE_P2,
            P1.ISS_HD_SEND_FLAG AS ISS_HD_SEND_FLAG_P1, P2.ISS_HD_SEND_FLAG AS ISS_HD_SEND_FLAG_P2,
            P1.ISS_HD_TYPE_FLAG AS ISS_HD_TYPE_FLAG_P1, P2.ISS_HD_TYPE_FLAG AS ISS_HD_TYPE_FLAG_P2,
            P1.ISS_HD_TOPIC_FLAG AS ISS_HD_TOPIC_FLAG_P1, P2.ISS_HD_TOPIC_FLAG AS ISS_HD_TOPIC_FLAG_P2,
            P1.ISS_HD_STS AS ISS_HD_STS_P1, P2.ISS_HD_STS AS ISS_HD_STS_P2,
            P1.PSN_NAME AS PSN_NAME_P1, P2.PSN_NAME AS PSN_NAME_P2,
            P1.PSN_G_NAME AS PSN_G_NAME_P1, P2.PSN_G_NAME AS PSN_G_NAME_P2
            FROM P1
            FULL OUTER JOIN P2
            ON P1.REV_HD_SEQ = P2.REV_HD_SEQ";

$stid = oci_parse($conn, $select);

if ($revhdSeqP1 != '') oci_bind_by_name($stid, ':revhdSeqP1', $revhdSeqP1);
if ($revhdSeqP2 != '') oci_bind_by_name($stid, ':revhdSeqP2', $revhdSeqP2);

oci_execute($stid);
