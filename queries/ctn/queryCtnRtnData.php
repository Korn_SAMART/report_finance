<?php
header('Content-Type: application/json; charset=utf-8');
//require_once 'isXmlHttpRequest.php';
include '../../database/conn.php';

$select = "WITH P1 AS (
                SELECT HD.REV_HD_SEQ, RTN.RTN_HD_NO,RTN.RTN_HD_DATE,RTN.RTN_HD_PAY_DATE,CASE RTN.RTN_HD_SEND_FLAG WHEN '0' THEN 'จัดส่งโดยผู้ส่งคืน' WHEN '1' THEN 'ผู้รับคืนมารับด้วยตนเอง' ELSE '-' END AS RTN_HD_SEND_FLAG,
            CASE RTN.RTN_HD_TYPE_FLAG WHEN '0' THEN 'ขอส่งคืนภายในหน่วยงาน' WHEN '1' THEN 'ขอส่งคืนระหว่างสำนักงานที่ดินแบบตามลำดับ' WHEN '2' THEN 'ขอส่งคืนระหว่างสำนักงานที่ดินแบบขั้นตอนเดียว' 
            WHEN '3' THEN 'ขอส่งคืนระหว่างสำนักงานที่ดินกับกองพัสดุแบบตามลำดับ' WHEN '4' THEN 'ขอส่งคืนระหว่างสำนักงานที่ดินกับกองพัสดุแบบขั้นตอนเดียว' ELSE '-' END AS RTN_HD_TYPE_FLAG,
            CASE RTN.RTN_HD_STS WHEN '0' THEN 'ร่างใบส่งคืน' WHEN '1' THEN 'รอส่งคืน' WHEN '2' THEN 'ยกเลิกใบส่งคืน' WHEN '3' THEN 'เสนออนุมัติ' WHEN '4' THEN 'อนุมัติ' WHEN '5' THEN 'ไม่อนุมัติ' 
            WHEN '6' THEN 'เสนอแก้ไข' WHEN '7' THEN 'รับของ' ELSE '-' END AS RTN_HD_STS,
            RTN.PSN_NAME,RTN.PSN_G_NAME
            FROM MGT1.TB_INV_REV_HD HD
            LEFT OUTER JOIN MGT1.TB_INV_RTN_HD RTN
            ON HD.RTN_HD_SEQ = RTN.RTN_HD_SEQ
            WHERE HD.REV_HD_SEQ = :revhdSeqP1
            ),
            P2 AS (
            SELECT HD.REV_HD_SEQ, RTN.RTN_HD_NO,RTN.RTN_HD_DATE,RTN.RTN_HD_PAY_DATE,CASE RTN.RTN_HD_SEND_FLAG WHEN '0' THEN 'จัดส่งโดยผู้ส่งคืน' WHEN '1' THEN 'ผู้รับคืนมารับด้วยตนเอง' ELSE '-' END AS RTN_HD_SEND_FLAG,
            CASE RTN.RTN_HD_TYPE_FLAG WHEN '0' THEN 'ขอส่งคืนภายในหน่วยงาน' WHEN '1' THEN 'ขอส่งคืนระหว่างสำนักงานที่ดินแบบตามลำดับ' WHEN '2' THEN 'ขอส่งคืนระหว่างสำนักงานที่ดินแบบขั้นตอนเดียว' 
            WHEN '3' THEN 'ขอส่งคืนระหว่างสำนักงานที่ดินกับกองพัสดุแบบตามลำดับ' WHEN '4' THEN 'ขอส่งคืนระหว่างสำนักงานที่ดินกับกองพัสดุแบบขั้นตอนเดียว' ELSE '-' END AS RTN_HD_TYPE_FLAG,
            CASE RTN.RTN_HD_STS WHEN '0' THEN 'ร่างใบส่งคืน' WHEN '1' THEN 'รอส่งคืน' WHEN '2' THEN 'ยกเลิกใบส่งคืน' WHEN '3' THEN 'เสนออนุมัติ' WHEN '4' THEN 'อนุมัติ' WHEN '5' THEN 'ไม่อนุมัติ' 
            WHEN '6' THEN 'เสนอแก้ไข' WHEN '7' THEN 'รับของ' ELSE '-' END AS RTN_HD_STS,
            RTN.PSN_NAME,RTN.PSN_G_NAME
            FROM INV.TB_INV_REV_HD HD
            LEFT OUTER JOIN INV.TB_INV_RTN_HD RTN
            ON HD.RTN_HD_SEQ = RTN.RTN_HD_SEQ
            WHERE HD.REV_HD_SEQ = :revhdSeqP2

            )
            SELECT P1.REV_HD_SEQ AS REV_HD_SEQ_P1, P2.REV_HD_SEQ AS REV_HD_SEQ_P2,
            P1.RTN_HD_NO AS RTN_HD_NO_P1, P2.RTN_HD_NO AS RTN_HD_NO_P2,
            CASE WHEN SUBSTR(P1.RTN_HD_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P1.RTN_HD_DATE) ELSE TO_CHAR(P1.RTN_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RTN_HD_DATE_P1,
            CASE WHEN SUBSTR(P2.RTN_HD_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P2.RTN_HD_DATE) ELSE TO_CHAR(P2.RTN_HD_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RTN_HD_DATE_P2,
            CASE WHEN SUBSTR(P1.RTN_HD_PAY_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P1.RTN_HD_PAY_DATE) ELSE TO_CHAR(P1.RTN_HD_PAY_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RTN_HD_PAY_DATE_P1,
            CASE WHEN SUBSTR(P2.RTN_HD_PAY_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P2.RTN_HD_PAY_DATE) ELSE TO_CHAR(P2.RTN_HD_PAY_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RTN_HD_PAY_DATE_P2,
            P1.RTN_HD_SEND_FLAG AS RTN_HD_SEND_FLAG_P1, P2.RTN_HD_SEND_FLAG AS RTN_HD_SEND_FLAG_P2,
            P1.RTN_HD_TYPE_FLAG AS RTN_HD_TYPE_FLAG_P1, P2.RTN_HD_TYPE_FLAG AS RTN_HD_TYPE_FLAG_P2,
            P1.RTN_HD_STS AS RTN_HD_STS_P1, P2.RTN_HD_STS AS RTN_HD_STS_P2,
            P1.PSN_NAME AS PSN_NAME_P1, P2.PSN_NAME AS PSN_NAME_P2,
            P1.PSN_G_NAME AS PSN_G_NAME_P1, P2.PSN_G_NAME AS PSN_G_NAME_P2
            FROM P1
            FULL OUTER JOIN P2
            ON P1.REV_HD_SEQ = P2.REV_HD_SEQ";

$stid = oci_parse($conn, $select);

if ($revhdSeqP1 != '') oci_bind_by_name($stid, ':revhdSeqP1', $revhdSeqP1);
if ($revhdSeqP2 != '') oci_bind_by_name($stid, ':revhdSeqP2', $revhdSeqP2);

oci_execute($stid);
