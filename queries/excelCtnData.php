<?php

require '../plugins/vendor/autoload.php';

include '../database/conn.php';

error_reporting(0);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$Result = array();



$check = !isset($_GET['check']) ? '' : $_GET['check'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];
$landoffice = !isset($_GET['landoffice']) ? '' : $_GET['landoffice'];
$branchName = !isset($_GET['branchName']) ? '' : $_GET['branchName'];



$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$spreadsheet->getActiveSheet()->setTitle('ข้อมูลหนังสือ');



switch ($check) {
    case "1":
    case "2":
    case "3":
    case "4":
    case "7":
        include './ctn/queryCtnData.php';
        while (($row = oci_fetch_array($stid, OCI_ASSOC))) {
            $Result[] = $row;
        }
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $bookCellIndex = 0;
        $errorBook = 0;

        //===================BOOK DETAIL===================
        for ($i = 0; $i < $last; $i++) {
            if ($sts == 'e') {
                $problemDesc = '';
                if ($check == '3' || $check == '4') {
                    if ($Result[$i]['REGISTER_NO_P1'] != $Result[$i]['REGISTER_NO_P2']) $problemDesc .= "-เลขทะเบียนส่งไม่ตรงกัน\n";
                    if ($Result[$i]['DOC_NUMBER_P1'] != $Result[$i]['DOC_NUMBER_P2']) $problemDesc .= "-เลขที่หนังสือไม่ตรงกัน\n";
                    if ($Result[$i]['DOC_DATE_P1'] != $Result[$i]['DOC_DATE_P2']) $problemDesc .= "-วันที่ลงหนังสือไม่ตรงกัน\n";
                    if ($Result[$i]['RECEIVE_DATE_P1'] != $Result[$i]['RECEIVE_DATE_P2']) $problemDesc .= "-วันที่รับหนังสือไม่ตรงกัน\n";
                    if ($Result[$i]['DOC_FROM_TEXT_P1'] != $Result[$i]['DOC_FROM_TEXT_P2']) $problemDesc .= "-ชื่อหนังสือจากไม่ตรงกัน\n";
                    if ($Result[$i]['SUBJECT_P1'] != $Result[$i]['SUBJECT_P2']) $problemDesc .= "-ชื่อเรื่องหนังสือไม่ตรงกัน\n";
                    if ($problemDesc == '') {
                        $bookCellIndex = 6 + $current;
                        continue;
                    } else $errorBook++;
                    $sheet->setCellValue('P' . (6 + $current), rtrim($problemDesc));
                    $problemDesc = '';
                } else if ($check == '1' || $check == '2') {

                    if ($Result[$i]['REGISTER_NO_P1'] != $Result[$i]['REGISTER_NO_P2']) $problemDesc .= "-เลขทะเบียนรับไม่ตรงกัน\n";
                    if ($Result[$i]['DOC_NUMBER_P1'] != $Result[$i]['DOC_NUMBER_P2']) $problemDesc .= "-เลขที่หนังสือไม่ตรงกัน\n";
                    if ($Result[$i]['DOC_DATE_P1'] != $Result[$i]['DOC_DATE_P2']) $problemDesc .= "-วันที่ลงหนังสือไม่ตรงกัน\n";
                    if ($Result[$i]['SEND_DATE_P1'] != $Result[$i]['SEND_DATE_P2']) $problemDesc .= "-วันที่รับหนังสือไม่ตรงกัน\n";
                    if ($Result[$i]['DOC_FROM_TEXT_P1'] != $Result[$i]['DOC_FROM_TEXT_P2']) $problemDesc .= "-ชื่อหนังสือจากไม่ตรงกัน\n";
                    if ($Result[$i]['DOC_TO_P1'] != $Result[$i]['DOC_TO_P2']) $problemDesc .= "-ชื่อหนังสือถึงไม่ตรงกัน\n";
                    if ($Result[$i]['SUBJECT_P1'] != $Result[$i]['SUBJECT_P2']) $problemDesc .= "-ชื่อเรื่องหนังสือไม่ตรงกัน\n";
                    if ($problemDesc == '') {
                        $bookCellIndex = 6 + $current;
                        continue;
                    } else $errorBook++;
                    $sheet->setCellValue('N' . (6 + $current), rtrim($problemDesc));
                    $problemDesc = '';
                } else {
                    if ($Result[$i]['ORDER_NO_P1'] != $Result[$i]['ORDER_NO_P2']) $problemDesc .= "-เลขที่คำสั่งไม่ตรงกัน\n";
                    if ($Result[$i]['DOC_DATE_P1'] != $Result[$i]['DOC_DATE_P2']) $problemDesc .= "-วันที่ลงหนังสือไม่ตรงกัน\n";
                    if ($Result[$i]['DOC_FROM_P1'] != $Result[$i]['DOC_FROM_P2']) $problemDesc .= "-ชื่อหนังสือจากไม่ตรงกัน\n";
                    if ($Result[$i]['DOC_TO_P1'] != $Result[$i]['DOC_TO_P2']) $problemDesc .= "-ชื่อหนังสือถึงไม่ตรงกัน\n";
                    if ($Result[$i]['SUBJECT_P1'] != $Result[$i]['SUBJECT_P2']) $problemDesc .= "-ชื่อเรื่องหนังสือไม่ตรงกัน\n";
                    if ($problemDesc == '') {
                        $bookCellIndex = 6 + $current;
                        continue;
                    } else $errorBook++;
                    $sheet->setCellValue('L' . (6 + $current), rtrim($problemDesc));
                    $problemDesc = '';
                }
            }
            if ($check == '3' || $check == '4') {
                $sheet->setCellValue('A' . (6 + $current), ($current + 1));
                if (empty($Result[$i]['REGISTER_NO_P1'])) {
                    $sheet->setCellValue('B' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (6 + $current), $Result[$i]['REGISTER_NO_P1']);
                }
                if (empty($Result[$i]['DOC_NUMBER_P1'])) {
                    $sheet->setCellValue('C' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (6 + $current), $Result[$i]['DOC_NUMBER_P1']);
                }
                if (empty($Result[$i]['DOC_DATE_P1'])) {
                    $sheet->setCellValue('D' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (6 + $current), $Result[$i]['DOC_DATE_P1']);
                }
                if (empty($Result[$i]['SEND_DATE_P1'])) {
                    $sheet->setCellValue('E' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (6 + $current), $Result[$i]['SEND_DATE_P1']);
                }
                if (empty($Result[$i]['DOC_FROM_TEXT_P1'])) {
                    $sheet->setCellValue('F' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (6 + $current), $Result[$i]['DOC_FROM_TEXT_P1']);
                }
                if (empty($Result[$i]['SUBJECT_P1'])) {
                    $sheet->setCellValue('G' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('G' . (6 + $current), $Result[$i]['SUBJECT_P1']);
                }
                if (empty($Result[$i]['REGISTER_NO_P2'])) {
                    $sheet->setCellValue('H' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('H' . (6 + $current), $Result[$i]['REGISTER_NO_P2']);
                }
                if (empty($Result[$i]['DOC_NUMBER_P2'])) {
                    $sheet->setCellValue('I' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('I' . (6 + $current), $Result[$i]['DOC_NUMBER_P2']);
                }
                if (empty($Result[$i]['DOC_DATE_P2'])) {
                    $sheet->setCellValue('J' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('J' . (6 + $current), $Result[$i]['DOC_DATE_P2']);
                }
                if (empty($Result[$i]['SEND_DATE_P2'])) {
                    $sheet->setCellValue('K' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('K' . (6 + $current), $Result[$i]['SEND_DATE_P2']);
                }
                if (empty($Result[$i]['DOC_FROM_TEXT_P2'])) {
                    $sheet->setCellValue('L' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (6 + $current), $Result[$i]['DOC_FROM_TEXT_P2']);
                }

                if (empty($Result[$i]['SUBJECT_P2'])) {
                    $sheet->setCellValue('M' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (6 + $current), $Result[$i]['SUBJECT_P2']);
                }
            } else if ($check == '1' || $check == '2') {
                $sheet->setCellValue('A' . (6 + $current), ($current + 1));
                if (empty($Result[$i]['REGISTER_NO_P1'])) {
                    $sheet->setCellValue('B' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (6 + $current), $Result[$i]['REGISTER_NO_P1']);
                }
                if (empty($Result[$i]['DOC_NUMBER_P1'])) {
                    $sheet->setCellValue('C' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (6 + $current), $Result[$i]['DOC_NUMBER_P1']);
                }
                if (empty($Result[$i]['DOC_DATE_P1'])) {
                    $sheet->setCellValue('D' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (6 + $current), $Result[$i]['DOC_DATE_P1']);
                }
                if (empty($Result[$i]['RECEIVE_DATE_P1'])) {
                    $sheet->setCellValue('E' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (6 + $current), $Result[$i]['RECEIVE_DATE_P1']);
                }
                if (empty($Result[$i]['DOC_FROM_TEXT_P1'])) {
                    $sheet->setCellValue('F' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (6 + $current), $Result[$i]['DOC_FROM_TEXT_P1']);
                }

                if (empty($Result[$i]['DOC_TO_P1'])) {
                    $sheet->setCellValue('G' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('G' . (6 + $current), $Result[$i]['DOC_TO_P1']);
                }
                if (empty($Result[$i]['SUBJECT_P1'])) {
                    $sheet->setCellValue('H' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('H' . (6 + $current), $Result[$i]['SUBJECT_P1']);
                }
                if (empty($Result[$i]['REGISTER_NO_P2'])) {
                    $sheet->setCellValue('I' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('I' . (6 + $current), $Result[$i]['REGISTER_NO_P2']);
                }
                if (empty($Result[$i]['DOC_NUMBER_P2'])) {
                    $sheet->setCellValue('J' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('J' . (6 + $current), $Result[$i]['DOC_NUMBER_P2']);
                }
                if (empty($Result[$i]['DOC_DATE_P2'])) {
                    $sheet->setCellValue('K' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('K' . (6 + $current), $Result[$i]['DOC_DATE_P2']);
                }
                if (empty($Result[$i]['RECEIVE_DATE_P2'])) {
                    $sheet->setCellValue('L' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (6 + $current), $Result[$i]['RECEIVE_DATE_P2']);
                }
                if (empty($Result[$i]['DOC_FROM_TEXT_P2'])) {
                    $sheet->setCellValue('M' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (6 + $current), $Result[$i]['DOC_FROM_TEXT_P2']);
                }

                if (empty($Result[$i]['DOC_TO_P2'])) {
                    $sheet->setCellValue('N' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (6 + $current), $Result[$i]['DOC_TO_P2']);
                }
                if (empty($Result[$i]['SUBJECT_P2'])) {
                    $sheet->setCellValue('O' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('O' . (6 + $current), $Result[$i]['SUBJECT_P2']);
                }
            } else {
                $sheet->setCellValue('A' . (6 + $current), ($current + 1));
                if (empty($Result[$i]['ORDER_NO_P1'])) {
                    $sheet->setCellValue('B' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (6 + $current), $Result[$i]['ORDER_NO_P1']);
                }
                if (empty($Result[$i]['DOC_DATE_P1'])) {
                    $sheet->setCellValue('C' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (6 + $current), $Result[$i]['DOC_DATE_P1']);
                }
                if (empty($Result[$i]['DOC_FROM_P1'])) {
                    $sheet->setCellValue('D' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (6 + $current), $Result[$i]['DOC_FROM_P1']);
                }
                if (empty($Result[$i]['DOC_TO_P1'])) {
                    $sheet->setCellValue('E' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (6 + $current), $Result[$i]['DOC_TO_P1']);
                }
                if (empty($Result[$i]['SUBJECT_P1'])) {
                    $sheet->setCellValue('F' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (6 + $current), $Result[$i]['SUBJECT_P1']);
                }
                if (empty($Result[$i]['ORDER_NO_P2'])) {
                    $sheet->setCellValue('G' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('G' . (6 + $current), $Result[$i]['ORDER_NO_P2']);
                }
                if (empty($Result[$i]['DOC_DATE_P2'])) {
                    $sheet->setCellValue('H' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('H' . (6 + $current), $Result[$i]['DOC_DATE_P2']);
                }
                if (empty($Result[$i]['DOC_FROM_P2'])) {
                    $sheet->setCellValue('I' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('I' . (6 + $current), $Result[$i]['DOC_FROM_P2']);
                }
                if (empty($Result[$i]['DOC_TO_P2'])) {
                    $sheet->setCellValue('J' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('J' . (6 + $current), $Result[$i]['DOC_TO_P2']);
                }
                if (empty($Result[$i]['SUBJECT_P2'])) {
                    $sheet->setCellValue('K' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('K' . (6 + $current), $Result[$i]['SUBJECT_P2']);
                }
            }



            $bookCellIndex = 6 + $current;
            $current++;
        }

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];

        $fileTypeName = '';
        switch ($check) {
            case '1':
                $fileTypeName = 'ข้อมูลหนังสือรับ (ไม่มีชั้นความลับ) ';
                break;
            case '2':
                $fileTypeName = 'ข้อมูลหนังสือรับ (มีชั้นความลับ) ';
                break;
            case '3':
                $fileTypeName = 'ข้อมูลหนังสือส่ง (ไม่มีชั้นความลับ) ';
                break;
            case '4':
                $fileTypeName = 'ข้อมูลหนังสือส่ง (มีชั้นความลับ) ';
                break;
            case '7':
                $fileTypeName = 'ข้อมูลเลขที่คำสั่งสำนักงานที่ดิน';
                break;
        }

        if ($check == 1 || $check == 2) {
            $sheet->mergeCells('A1:P1');
            $sheet->mergeCells('A2:P2');
            $sheet->mergeCells('A3:P3');
            $sheet->mergeCells('A4:H4');
            $sheet->mergeCells('I4:O4');
            $sheet->mergeCells('P4:P5');
            $sheet->getColumnDimension('P')->setWidth(50);



            if ($sts == 'e') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', ' ที่มีข้อมูลแตกต่างกัน');
                $sheet->setCellValue('A3', $branchName);
                $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
                $sheet->setCellValue('I4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
                $bn = $fileTypeName . 'ที่มีข้อมูลแตกต่างกัน';
                $fileName =  $branchName . '-' . $fileTypeName . 'ที่มีข้อมูลแตกต่างกัน';
            } else if ($sts == 's') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', $fileTypeName . 'ที่ถ่ายโอนสำเร็จ');
                $sheet->setCellValue('A3', $branchName);
                $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
                $sheet->setCellValue('I4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
                $bn = $fileTypeName . 'ที่ถ่ายโอนสำเร็จ';
                $fileName =  $branchName . '-' . $fileTypeName . 'ที่ถ่ายโอนสำเร็จ';
            }


            $sheet->setCellValue('A5', '')
                ->setCellValue('B5', 'เลขทะเบียนรับ')
                ->setCellValue('C5', 'เลขที่หนังสือ')
                ->setCellValue('D5', 'ลงวันที่')
                ->setCellValue('E5', 'วันที่รับหนังสือ')
                ->setCellValue('F5', 'จาก')
                ->setCellValue('G5', 'ถึง')
                ->setCellValue('H5', 'เรื่อง')
                ->setCellValue('I5', 'เลขทะเบียนรับ')
                ->setCellValue('J5', 'เลขที่หนังสือ')
                ->setCellValue('K5', 'ลงวันที่')
                ->setCellValue('L5', 'วันที่รับหนังสือ')
                ->setCellValue('M5', 'จาก')
                ->setCellValue('N5', 'ถึง')
                ->setCellValue('O5', 'เรื่อง')
                ->setCellValue('P4', 'หมายเหตุ')
                ->getStyle('A1:P5')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


            $sheet->getStyle('A1:P' . $bookCellIndex)->applyFromArray($styleArray);
            $sheet->getStyle('A1:P5')->getFont()->setBold(true);
            $sheet->getStyle('A6:P' . $bookCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
            $sheet->getStyle('P6:P' . $bookCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


            $sheet->getStyle('A6:P' . $bookCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $sheet->getStyle('P6:P' . $bookCellIndex)
                ->getAlignment()
                ->setWrapText(true);
        } else if ($check == '3' || $check == '4') {
            $sheet->mergeCells('A1:O1');
            $sheet->mergeCells('A2:O2');
            $sheet->mergeCells('A3:O3');
            $sheet->mergeCells('A4:G4');
            $sheet->mergeCells('H4:M4');
            $sheet->mergeCells('N4:N5');
            $sheet->getColumnDimension('N')->setWidth(50);



            if ($sts == 'e') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', ' ที่มีข้อมูลแตกต่างกัน');
                $sheet->setCellValue('A3', $branchName);
                $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
                $sheet->setCellValue('H4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
                $bn = $fileTypeName . 'ที่มีข้อมูลแตกต่างกัน';
                $fileName =  $branchName . '-' . $fileTypeName . 'ที่มีข้อมูลแตกต่างกัน';
            } else if ($sts == 's') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', $fileTypeName . 'ที่ถ่ายโอนสำเร็จ');
                $sheet->setCellValue('A3', $branchName);
                $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
                $sheet->setCellValue('H4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
                $bn = $fileTypeName . 'ที่ถ่ายโอนสำเร็จ';
                $fileName =  $branchName . '-' . $fileTypeName . 'ที่ถ่ายโอนสำเร็จ';
            }


            $sheet->setCellValue('A5', '')
                ->setCellValue('B5', 'เลขทะเบียนส่ง')
                ->setCellValue('C5', 'เลขที่หนังสือ')
                ->setCellValue('D5', 'ลงวันที่')
                ->setCellValue('E5', 'วันที่รับหนังสือ')
                ->setCellValue('F5', 'จาก')
                ->setCellValue('G5', 'เรื่อง')
                ->setCellValue('H5', 'เลขทะเบียนส่ง')
                ->setCellValue('I5', 'เลขที่หนังสือ')
                ->setCellValue('J5', 'ลงวันที่')
                ->setCellValue('K5', 'วันที่รับหนังสือ')
                ->setCellValue('L5', 'จาก')
                ->setCellValue('M5', 'เรื่อง')
                ->setCellValue('N4', 'หมายเหตุ')
                ->getStyle('A1:N5')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


            $sheet->getStyle('A1:N' . $bookCellIndex)->applyFromArray($styleArray);
            $sheet->getStyle('A1:N5')->getFont()->setBold(true);
            $sheet->getStyle('A6:N' . $bookCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
            $sheet->getStyle('N6:N' . $bookCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


            $sheet->getStyle('A6:N' . $bookCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $sheet->getStyle('N6:N' . $bookCellIndex)
                ->getAlignment()
                ->setWrapText(true);
        } else {
            $sheet->mergeCells('A1:L1');
            $sheet->mergeCells('A2:L2');
            $sheet->mergeCells('A3:L3');
            $sheet->mergeCells('A4:F4');
            $sheet->mergeCells('G4:K4');
            $sheet->mergeCells('L4:L5');
            $sheet->getColumnDimension('L')->setWidth(50);



            if ($sts == 'e') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', ' ที่มีข้อมูลแตกต่างกัน');
                $sheet->setCellValue('A3', $branchName);
                $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
                $sheet->setCellValue('G4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
                $bn = $fileTypeName . 'ที่มีข้อมูลแตกต่างกัน';
                $fileName =  $branchName . '-' . $fileTypeName . 'ที่มีข้อมูลแตกต่างกัน';
            } else if ($sts == 's') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', $fileTypeName . 'ที่ถ่ายโอนสำเร็จ');
                $sheet->setCellValue('A3', $branchName);
                $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
                $sheet->setCellValue('G4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
                $bn = $fileTypeName . 'ที่ถ่ายโอนสำเร็จ';
                $fileName =  $branchName . '-' . $fileTypeName . 'ที่ถ่ายโอนสำเร็จ';
            }


            $sheet->setCellValue('A5', '')
                ->setCellValue('B5', 'เลขที่คำสั่ง')
                ->setCellValue('C5', 'ลงวันที่')
                ->setCellValue('D5', 'จาก')
                ->setCellValue('E5', 'ถึง')
                ->setCellValue('F5', 'เรื่อง')
                ->setCellValue('G5', 'เลขที่คำสั่ง')
                ->setCellValue('H5', 'ลงวันที่')
                ->setCellValue('I5', 'จาก')
                ->setCellValue('J5', 'ถึง')
                ->setCellValue('K5', 'เรื่อง')
                ->setCellValue('L4', 'หมายเหตุ')
                ->getStyle('A1:L5')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


            $sheet->getStyle('A1:L' . $bookCellIndex)->applyFromArray($styleArray);
            $sheet->getStyle('A1:L5')->getFont()->setBold(true);
            $sheet->getStyle('A6:L' . $bookCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
            $sheet->getStyle('L6:L' . $bookCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


            $sheet->getStyle('A6:L' . $bookCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $sheet->getStyle('L6:L' . $bookCellIndex)
                ->getAlignment()
                ->setWrapText(true);
        }


        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save('php://output');
        break;
    case '8':
        include './ctn/queryCtnData.php';
        while (($row = oci_fetch_array($stid, OCI_ASSOC))) {
            $Result[] = $row;
        }
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $bookCellIndex = 0;
        $errorBook = 0;

        //===================BOOK DETAIL===================
        for ($i = 0; $i < $last; $i++) {
            if ($sts == 'e') {
                $problemDesc = '';

                if ($Result[$i]['BLD_NAME_P1'] != $Result[$i]['BLD_NAME_P2']) $problemDesc .= "-ประเภทอาคารไม่ตรงกัน\n";
                if ($Result[$i]['REV_BLD_NO_P1'] != $Result[$i]['REV_BLD_NO_P2']) $problemDesc .= "-เลขที่อาคารไม่ตรงกัน\n";
                if ($Result[$i]['REV_BLD_NAME_P1'] != $Result[$i]['REV_BLD_NAME_P2']) $problemDesc .= "-ชื่ออาคารจากไม่ตรงกัน\n";
                if ($Result[$i]['REV_BLD_DATE_P1'] != $Result[$i]['REV_BLD_DATE_P2']) $problemDesc .= "-วันที่ได้มาไม่ตรงกัน\n";
                if ($problemDesc == '') {
                    $bookCellIndex = 6 + $current;
                    continue;
                } else $errorBook++;
                $sheet->setCellValue('J' . (6 + $current), rtrim($problemDesc));
                $problemDesc = '';
            }

            $sheet->setCellValue('A' . (6 + $current), ($current + 1));
            if (empty($Result[$i]['BLD_NAME_P1'])) {
                $sheet->setCellValue('B' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('B' . (6 + $current), $Result[$i]['BLD_NAME_P1']);
            }
            if (empty($Result[$i]['REV_BLD_NO_P1'])) {
                $sheet->setCellValue('C' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('C' . (6 + $current), $Result[$i]['REV_BLD_NO_P1']);
            }
            if (empty($Result[$i]['REV_BLD_NAME_P1'])) {
                $sheet->setCellValue('D' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('D' . (6 + $current), $Result[$i]['REV_BLD_NAME_P1']);
            }
            if (empty($Result[$i]['REV_BLD_DATE_P1'])) {
                $sheet->setCellValue('E' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (6 + $current), $Result[$i]['REV_BLD_DATE_P1']);
            }
            if (empty($Result[$i]['BLD_NAME_P2'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $Result[$i]['BLD_NAME_P2']);
            }
            if (empty($Result[$i]['REV_BLD_NO_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $Result[$i]['REV_BLD_NO_P2']);
            }
            if (empty($Result[$i]['REV_BLD_NAME_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $Result[$i]['REV_BLD_NAME_P2']);
            }
            if (empty($Result[$i]['REV_BLD_DATE_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $Result[$i]['REV_BLD_DATE_P2']);
            }





            $bookCellIndex = 6 + $current;
            $current++;
        }

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];



        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        $sheet->mergeCells('A3:J3');
        $sheet->mergeCells('A4:E4');
        $sheet->mergeCells('F4:I4');
        $sheet->mergeCells('J4:J5');
        $sheet->getColumnDimension('J')->setWidth(50);



        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนคุมอาคารที่มีข้อมูลแตกต่างกัน');
            $sheet->setCellValue('A3', $branchName);
            $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
            $sheet->setCellValue('F4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
            $bn = $fileTypeName . 'ข้อมูลทะเบียนคุมอาคารที่มีข้อมูลแตกต่างกัน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนคุมอาคารที่มีข้อมูลแตกต่างกัน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนคุมอาคารที่ถ่ายโอนสำเร็จ');
            $sheet->setCellValue('A3', $branchName);
            $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
            $sheet->setCellValue('F4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
            $bn = $fileTypeName . 'ข้อมูลทะเบียนคุมอาคารที่ถ่ายโอนสำเร็จ';
            $fileName =  $branchName . '-ข้อมูลทะเบียนคุมอาคารที่ถ่ายโอนสำเร็จ';
        }


        $sheet->setCellValue('A5', '')
            ->setCellValue('B5', 'ประเภทอาคาร')
            ->setCellValue('C5', 'เลขที่อาคาร')
            ->setCellValue('D5', 'ชื่ออาคาร')
            ->setCellValue('E5', 'วันที่ได้มาของอาคาร')
            ->setCellValue('F5', 'ประเภทอาคาร')
            ->setCellValue('G5', 'เลขที่อาคาร')
            ->setCellValue('H5', 'ชื่ออาคาร')
            ->setCellValue('I5', 'วันที่ได้มาของอาคาร')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A1:J5')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->getStyle('A1:J' . $bookCellIndex)->applyFromArray($styleArray);
        $sheet->getStyle('A1:J5')->getFont()->setBold(true);
        $sheet->getStyle('A6:J' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $sheet->getStyle('J6:J' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


        $sheet->getStyle('A6:J' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('J6:J' . $bookCellIndex)
            ->getAlignment()
            ->setWrapText(true);


        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save('php://output');
        break;
    case '10':
    case '11':
    case '12':
        include './ctn/queryCtnData.php';
        while (($row = oci_fetch_array($stid, OCI_ASSOC))) {
            $Result[] = $row;
        }
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $bookCellIndex = 0;
        $errorBook = 0;

        //===================BOOK DETAIL===================
        for ($i = 0; $i < $last; $i++) {
            if ($sts == 'e') {
                $problemDesc = '';

                if ($Result[$i]['PLATE_NAME_P1'] != $Result[$i]['PLATE_NAME_P2']) $problemDesc .= "-ชนิดแบบพิมพ์ไม่ตรงกัน\n";
                if ($Result[$i]['REV_HD_DATE_P1'] != $Result[$i]['REV_HD_DATE_P2']) $problemDesc .= "-วันที่รับไม่ตรงกัน\n";
                if ($Result[$i]['UNIT_NAME_P1'] != $Result[$i]['UNIT_NAME_P2']) $problemDesc .= "-หน่วยนับไม่ตรงกัน\n";
                if ($Result[$i]['REV_PLATE_QTY_P1'] != $Result[$i]['REV_PLATE_QTY_P2']) $problemDesc .= "-จำนวนไม่ตรงกัน\n";
                if ($problemDesc == '') {
                    $bookCellIndex = 6 + $current;
                    continue;
                } else $errorBook++;
                $sheet->setCellValue('J' . (6 + $current), rtrim($problemDesc));
                $problemDesc = '';
            }

            $sheet->setCellValue('A' . (6 + $current), ($current + 1));
            if (empty($Result[$i]['PLATE_NAME_P1'])) {
                $sheet->setCellValue('B' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('B' . (6 + $current), $Result[$i]['PLATE_NAME_P1']);
            }
            if (empty($Result[$i]['REV_HD_DATE_P1'])) {
                $sheet->setCellValue('C' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('C' . (6 + $current), $Result[$i]['REV_HD_DATE_P1']);
            }
            if (empty($Result[$i]['UNIT_NAME_P1'])) {
                $sheet->setCellValue('D' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('D' . (6 + $current), $Result[$i]['UNIT_NAME_P1']);
            }
            if (empty($Result[$i]['REV_PLATE_QTY_P1'])) {
                $sheet->setCellValue('E' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (6 + $current), $Result[$i]['REV_PLATE_QTY_P1']);
            }
            if (empty($Result[$i]['PLATE_NAME_P2'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $Result[$i]['PLATE_NAME_P2']);
            }
            if (empty($Result[$i]['REV_HD_DATE_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $Result[$i]['REV_HD_DATE_P2']);
            }
            if (empty($Result[$i]['UNIT_NAME_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $Result[$i]['UNIT_NAME_P2']);
            }
            if (empty($Result[$i]['REV_PLATE_QTY_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $Result[$i]['REV_PLATE_QTY_P2']);
            }





            $bookCellIndex = 6 + $current;
            $current++;
        }

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];



        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        $sheet->mergeCells('A3:J3');
        $sheet->mergeCells('A4:E4');
        $sheet->mergeCells('F4:I4');
        $sheet->mergeCells('J4:J5');
        $sheet->getColumnDimension('J')->setWidth(50);

        $fileTypeName = '';
        switch ($check) {
            case '10':
                $fileTypeName = 'แบบพิมพ์การเงิน';
                break;
            case '11':
                $fileTypeName = 'แบบพิมพ์ทั่วไป';
                break;
            case '12':
                $fileTypeName = 'แบบพิมพ์แสดงสิทธิในที่ดิน';
                break;
        }


        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', $fileTypeName . 'ที่มีข้อมูลแตกต่างกัน');
            $sheet->setCellValue('A3', $branchName);
            $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
            $sheet->setCellValue('F4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
            $bn = $fileTypeName . 'ที่มีข้อมูลแตกต่างกัน';
            $fileName =  $branchName . '-' . $fileTypeName . 'ที่มีข้อมูลแตกต่างกัน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', $fileTypeName . 'ที่ถ่ายโอนสำเร็จ');
            $sheet->setCellValue('A3', $branchName);
            $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
            $sheet->setCellValue('F4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
            $bn = $fileTypeName . 'ที่ถ่ายโอนสำเร็จ';
            $fileName =  $branchName . '-' . $fileTypeName . 'ที่ถ่ายโอนสำเร็จ';
        }


        $sheet->setCellValue('A5', '')
            ->setCellValue('B5', 'ชนิดแบบพิมพ์')
            ->setCellValue('C5', 'วันที่รับ')
            ->setCellValue('D5', 'หน่วยนับ')
            ->setCellValue('E5', 'จำนวน')
            ->setCellValue('F5', 'ชนิดแบบพิมพ์')
            ->setCellValue('G5', 'วันที่รับ')
            ->setCellValue('H5', 'หน่วยนับ')
            ->setCellValue('I5', 'จำนวน')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A1:J5')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->getStyle('A1:J' . $bookCellIndex)->applyFromArray($styleArray);
        $sheet->getStyle('A1:J5')->getFont()->setBold(true);
        $sheet->getStyle('A6:J' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $sheet->getStyle('J6:J' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


        $sheet->getStyle('A6:J' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('J6:J' . $bookCellIndex)
            ->getAlignment()
            ->setWrapText(true);


        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save('php://output');
        break;
    case '14':
        include './ctn/queryCtnData.php';
        while (($row = oci_fetch_array($stid, OCI_ASSOC))) {
            $Result[] = $row;
        }
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $bookCellIndex = 0;
        $errorBook = 0;

        //===================BOOK DETAIL===================
        for ($i = 0; $i < $last; $i++) {
            if ($sts == 'e') {
                $problemDesc = '';

                if ($Result[$i]['PIECE_NAME_P1'] != $Result[$i]['PIECE_NAME_P2']) $problemDesc .= "-รายการหลักเขตไม่ตรงกัน\n";
                if ($Result[$i]['REV_HD_YEAR_P1'] != $Result[$i]['REV_HD_YEAR_P2']) $problemDesc .= "-ปีงบประมาณไม่ตรงกัน\n";
                if ($Result[$i]['REV_HD_DATE_P1'] != $Result[$i]['REV_HD_DATE_P2']) $problemDesc .= "-วันที่รับไม่ตรงกัน\n";
                if ($Result[$i]['UNIT_NAME_P1'] != $Result[$i]['UNIT_NAME_P2']) $problemDesc .= "-หน่วยนับไม่ตรงกัน\n";
                if ($Result[$i]['REV_PIECE_QTY_P1'] != $Result[$i]['REV_PIECE_QTY_P2']) $problemDesc .= "-จำนวนไม่ตรงกัน\n";

                if ($problemDesc == '') {
                    $bookCellIndex = 6 + $current;
                    continue;
                } else $errorBook++;
                $sheet->setCellValue('L' . (6 + $current), rtrim($problemDesc));
                $problemDesc = '';
            }

            $sheet->setCellValue('A' . (6 + $current), ($current + 1));
            if (empty($Result[$i]['PIECE_NAME_P1'])) {
                $sheet->setCellValue('B' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('B' . (6 + $current), $Result[$i]['PIECE_NAME_P1']);
            }
            if (empty($Result[$i]['REV_HD_YEAR_P1'])) {
                $sheet->setCellValue('C' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('C' . (6 + $current), $Result[$i]['REV_HD_YEAR_P1']);
            }
            if (empty($Result[$i]['REV_HD_DATE_P1'])) {
                $sheet->setCellValue('D' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('D' . (6 + $current), $Result[$i]['REV_HD_DATE_P1']);
            }
            if (empty($Result[$i]['UNIT_NAME_P1'])) {
                $sheet->setCellValue('E' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (6 + $current), $Result[$i]['UNIT_NAME_P1']);
            }
            if (empty($Result[$i]['REV_PIECE_QTY_P1'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $Result[$i]['REV_PIECE_QTY_P1']);
            }
            if (empty($Result[$i]['PIECE_NAME_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $Result[$i]['PIECE_NAME_P2']);
            }
            if (empty($Result[$i]['REV_HD_YEAR_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $Result[$i]['REV_HD_YEAR_P2']);
            }
            if (empty($Result[$i]['REV_HD_DATE_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $Result[$i]['REV_HD_DATE_P2']);
            }
            if (empty($Result[$i]['UNIT_NAME_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $Result[$i]['UNIT_NAME_P2']);
            }
            if (empty($Result[$i]['REV_PIECE_QTY_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $Result[$i]['REV_PIECE_QTY_P2']);
            }

            $bookCellIndex = 6 + $current;
            $current++;
        }

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];



        $sheet->mergeCells('A1:L1');
        $sheet->mergeCells('A2:L2');
        $sheet->mergeCells('A3:L3');
        $sheet->mergeCells('A4:G4');
        $sheet->mergeCells('H4:J4');
        $sheet->mergeCells('L4:L5');
        $sheet->getColumnDimension('L')->setWidth(50);



        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนคุมหลักเขตที่มีข้อมูลแตกต่างกัน');
            $sheet->setCellValue('A3', $branchName);
            $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
            $sheet->setCellValue('G4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
            $bn = $fileTypeName . 'ข้อมูลทะเบียนคุมหลักเขตที่มีข้อมูลแตกต่างกัน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนคุมหลักเขตที่มีข้อมูลแตกต่างกัน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนคุมหลักเขตที่ถ่ายโอนสำเร็จ');
            $sheet->setCellValue('A3', $branchName);
            $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
            $sheet->setCellValue('G4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
            $bn = $fileTypeName . 'ข้อมูลทะเบียนคุมหลักเขตที่ถ่ายโอนสำเร็จ';
            $fileName =  $branchName . '-ข้อมูลทะเบียนคุมหลักเขตที่ถ่ายโอนสำเร็จ';
        }


        $sheet->setCellValue('A5', '')
            ->setCellValue('B5', 'รายการหลักเขต')
            ->setCellValue('C5', 'ปีงบประมาณ')
            ->setCellValue('D5', 'วันที่รับ')
            ->setCellValue('E5', 'หน่วยนับ')
            ->setCellValue('F5', 'จำนวน')
            ->setCellValue('G5', 'รายการหลักเขต')
            ->setCellValue('H5', 'ปีงบประมาณ')
            ->setCellValue('I5', 'วันที่รับ')
            ->setCellValue('J5', 'หน่วยนับ')
            ->setCellValue('K5', 'จำนวน')
            ->setCellValue('L4', 'หมายเหตุ')
            ->getStyle('A1:L5')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->getStyle('A1:L' . $bookCellIndex)->applyFromArray($styleArray);
        $sheet->getStyle('A1:L5')->getFont()->setBold(true);
        $sheet->getStyle('A6:L' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $sheet->getStyle('L6:L' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


        $sheet->getStyle('A6:L' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('L6:L' . $bookCellIndex)
            ->getAlignment()
            ->setWrapText(true);


        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save('php://output');
        break;
    case '15':
        include './ctn/queryCtnData.php';
        while (($row = oci_fetch_array($stid, OCI_ASSOC))) {
            $Result[] = $row;
        }
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $bookCellIndex = 0;
        $errorBook = 0;

        //===================BOOK DETAIL===================
        for ($i = 0; $i < $last; $i++) {
            if ($sts == 'e') {
                $problemDesc = '';

                if ($Result[$i]['PSN_PID_P1'] != $Result[$i]['PSN_PID_P2']) $problemDesc .= "-เลขที่บัตรประชาชนไม่ตรงกัน\n";
                if ($Result[$i]['TITLE_NAME_P1'] != $Result[$i]['TITLE_NAME_P2']) $problemDesc .= "-คำนำหน้าชื่อไม่ตรงกัน\n";
                if ($Result[$i]['NAME_P1'] != $Result[$i]['NAME_P2']) $problemDesc .= "-ชื่อ-สกุลไม่ตรงกัน\n";
                if ($Result[$i]['POSITION_NO_P1'] != $Result[$i]['POSITION_NO_P2']) $problemDesc .= "-เลขที่ตำแหน่งไม่ตรงกัน\n";
                if ($Result[$i]['DIV_POSITION_NAME_P1'] != $Result[$i]['DIV_POSITION_NAME_P2']) $problemDesc .= "-ตำแหน่งทางสายงานไม่ตรงกัน\n";
                if ($Result[$i]['POSITION_LEVEL_NAME_P1'] != $Result[$i]['POSITION_LEVEL_NAME_P2']) $problemDesc .= "-ระดับไม่ตรงกัน\n";
                if ($Result[$i]['LANDOFFICE_TYPE_NAME_P1'] != $Result[$i]['LANDOFFICE_TYPE_NAME_P2']) $problemDesc .= "-ฝ่าย/กลุ่มไม่ตรงกัน\n";
                if ($Result[$i]['PSN_TYPE_NAME_P1'] != $Result[$i]['PSN_TYPE_NAME_P2']) $problemDesc .= "-ประเภทบุคลากรไม่ตรงกัน\n";


                if ($problemDesc == '') {
                    $bookCellIndex = 6 + $current;
                    continue;
                } else $errorBook++;
                $sheet->setCellValue('R' . (6 + $current), rtrim($problemDesc));
                $problemDesc = '';
            }

            $sheet->setCellValue('A' . (6 + $current), ($current + 1));
            if (empty($Result[$i]['PSN_PID_P1'])) {
                $sheet->setCellValue('B' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('B' . (6 + $current), $Result[$i]['PSN_PID_P1']);
            }
            if (empty($Result[$i]['TITLE_NAME_P1'])) {
                $sheet->setCellValue('C' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('C' . (6 + $current), $Result[$i]['TITLE_NAME_P1']);
            }
            if (empty($Result[$i]['NAME_P1'])) {
                $sheet->setCellValue('D' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('D' . (6 + $current), $Result[$i]['NAME_P1']);
            }
            if (empty($Result[$i]['POSITION_NO_P1'])) {
                $sheet->setCellValue('E' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (6 + $current), $Result[$i]['POSITION_NO_P1']);
            }
            if (empty($Result[$i]['DIV_POSITION_NAME_P1'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $Result[$i]['DIV_POSITION_NAME_P1']);
            }
            if (empty($Result[$i]['POSITION_LEVEL_NAME_P1'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $Result[$i]['POSITION_LEVEL_NAME_P1']);
            }
            if (empty($Result[$i]['LANDOFFICE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $Result[$i]['LANDOFFICE_TYPE_NAME_P1']);
            }
            if (empty($Result[$i]['PSN_TYPE_NAME_P1'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $Result[$i]['PSN_TYPE_NAME_P1']);
            }

            if (empty($Result[$i]['PSN_PID_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $Result[$i]['PSN_PID_P2']);
            }
            if (empty($Result[$i]['TITLE_NAME_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $Result[$i]['TITLE_NAME_P2']);
            }
            if (empty($Result[$i]['NAME_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $Result[$i]['NAME_P2']);
            }
            if (empty($Result[$i]['POSITION_NO_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $Result[$i]['POSITION_NO_P2']);
            }
            if (empty($Result[$i]['DIV_POSITION_NAME_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $Result[$i]['DIV_POSITION_NAME_P2']);
            }
            if (empty($Result[$i]['POSITION_LEVEL_NAME_P2'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $Result[$i]['POSITION_LEVEL_NAME_P2']);
            }
            if (empty($Result[$i]['LANDOFFICE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('P' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (6 + $current), $Result[$i]['LANDOFFICE_TYPE_NAME_P2']);
            }
            if (empty($Result[$i]['PSN_TYPE_NAME_P2'])) {
                $sheet->setCellValue('Q' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (6 + $current), $Result[$i]['PSN_TYPE_NAME_P2']);
            }





            $bookCellIndex = 6 + $current;
            $current++;
        }

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];



        $sheet->mergeCells('A1:R1');
        $sheet->mergeCells('A2:R2');
        $sheet->mergeCells('A3:R3');
        $sheet->mergeCells('A4:I4');
        $sheet->mergeCells('J4:Q4');
        $sheet->mergeCells('R4:R5');
        $sheet->getColumnDimension('R')->setWidth(50);



        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลบุคลากรที่มีข้อมูลแตกต่างกัน');
            $sheet->setCellValue('A3', $branchName);
            $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
            $sheet->setCellValue('J4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
            $bn = $fileTypeName . 'ข้อมูลบุคลากรที่มีข้อมูลแตกต่างกัน';
            $fileName =  $branchName . '-ข้อมูลบุคลากรที่มีข้อมูลแตกต่างกัน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลบุคลากรที่ถ่ายโอนสำเร็จ');
            $sheet->setCellValue('A3', $branchName);
            $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
            $sheet->setCellValue('J4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
            $bn = $fileTypeName . 'ข้อมูลบุคลากรที่ถ่ายโอนสำเร็จ';
            $fileName =  $branchName . '-ข้อมูลบุคลากรที่ถ่ายโอนสำเร็จ';
        }


        $sheet->getStyle('B5:B' . $last)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $sheet->getStyle('J5:J' . $last)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);


        $sheet->setCellValue('A5', '')
            ->setCellValue('B5', 'เลขที่บัตรประชาชน')
            ->setCellValue('C5', 'คำนำหน้าชื่อ')
            ->setCellValue('D5', 'ชื่อ-สกุล')
            ->setCellValue('E5', 'เลขที่ตำแหน่ง')
            ->setCellValue('F5', 'ตำแหน่งทางสายงาน')
            ->setCellValue('G5', 'ระดับ')
            ->setCellValue('H5', 'ฝ่าย/กลุ่ม')
            ->setCellValue('I5', 'ประเภทบุคลากร')
            ->setCellValue('J5', 'เลขที่บัตรประชาชน')
            ->setCellValue('K5', 'คำนำหน้าชื่อ')
            ->setCellValue('L5', 'ชื่อ-สกุล')
            ->setCellValue('M5', 'เลขที่ตำแหน่ง')
            ->setCellValue('N5', 'ตำแหน่งทางสายงาน')
            ->setCellValue('O5', 'ระดับ')
            ->setCellValue('P5', 'ฝ่าย/กลุ่ม')
            ->setCellValue('Q5', 'ประเภทบุคลากร')
            ->setCellValue('R4', 'หมายเหตุ')
            ->getStyle('A1:R5')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->getStyle('A1:R' . $bookCellIndex)->applyFromArray($styleArray);
        $sheet->getStyle('A1:R5')->getFont()->setBold(true);
        $sheet->getStyle('A6:R' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $sheet->getStyle('R6:R' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


        $sheet->getStyle('A6:R' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('R6:R' . $bookCellIndex)
            ->getAlignment()
            ->setWrapText(true);


        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save('php://output');
        break;
    case "16":
        $ResultP1 = array();

        include './ctn/queryCtnCount.php';
        while (($row = oci_fetch_array($stid, OCI_ASSOC))) {
            $ResultP1[] = $row;
        }

        $last = count($ResultP1);
        $current = 0;
        $problemDesc = "";
        $bookCellIndex = 0;
        $errorBook = 0;

        //===================HRM DETAIL===================
        for ($i = 0; $i < $last; $i++) {
            if ($sts == 'e') {
                $problemDesc = '';

                if ($ResultP1[$i]['PSN_PID'] != $ResultP1[$i]['PSN_PID_P2']) $problemDesc .= "-เลขบัตรประจำตัวประชาชนม่ตรงกัน\n";
                if (
                    $ResultP1[$i]['TITLE_NAME'] != $ResultP1[$i]['TITLE_NAME_P2'] ||
                    $ResultP1[$i]['PSN_FNAME'] != $ResultP1[$i]['PSN_FNAME_P2'] ||
                    $ResultP1[$i]['PSN_LNAME'] != $ResultP1[$i]['PSN_LNAME_P2']
                ) $problemDesc .= "-ชื่อ-นามสกุลไม่ตรงกัน\n";
                if ($ResultP1[$i]['FISCAL_YEAR'] != $ResultP1[$i]['FISCAL_YEAR_P2']) $problemDesc .= "-ปีงบประมาณไม่ตรงกัน\n";
                if ($ResultP1[$i]['SICK_COUNT'] != $ResultP1[$i]['SICK_COUNT_P2']) $problemDesc .= "-จำนวนวันลาป่วยไม่ตรงกัน\n";
                if ($ResultP1[$i]['DELIVER_COUNT'] != $ResultP1[$i]['DELIVER_COUNT_P2']) $problemDesc .= "-จำนวนวันลาคลอดบุตรไม่ตรงกัน\n";
                if ($ResultP1[$i]['BUSINESS_COUNT'] != $ResultP1[$i]['BUSINESS_COUNT_P2']) $problemDesc .= "-จำนวนวันลากิจส่วนตัวไม่ตรงกัน\n";
                if ($ResultP1[$i]['REST_COUNT'] != $ResultP1[$i]['REST_COUNT_P2']) $problemDesc .= "-จำนวนวันลาพักผ่อนไม่ตรงกัน\n";
                if ($ResultP1[$i]['ORDAIN_COUNT'] != $ResultP1[$i]['ORDAIN_COUNT_P2']) $problemDesc .= "-จำนวนวันลาอุปสมบทไม่ตรงกัน\n";
                if ($ResultP1[$i]['HUJJ_COUNT'] != $ResultP1[$i]['HUJJ_COUNT_P2']) $problemDesc .= "-จำนวนวันลาไปประกอบพิธีฮัจย์ไม่ตรงกัน\n";
                if ($ResultP1[$i]['MILITARY_COUNT'] != $ResultP1[$i]['MILITARY_COUNT_P2']) $problemDesc .= "-จำนวนวันลาเข้ารับการตรวจเลือก หรือเข้ารับการเตรียมพลไม่ตรงกัน\n";
                if ($ResultP1[$i]['STUDY_COUNT'] != $ResultP1[$i]['STUDY_COUNT_P2']) $problemDesc .= "-จำนวนวันลาไปศึกษา ฝึกอบรม ปฏิบัติการวิจัย หรือดูงานไม่ตรงกัน\n";
                if ($ResultP1[$i]['INTER_COUNT'] != $ResultP1[$i]['INTER_COUNT_P2']) $problemDesc .= "-จำนวนวันลาไปปฏิบัติงานในองค์การระหว่างประเทศไม่ตรงกัน\n";
                if ($ResultP1[$i]['FOLLOW_COUNT'] != $ResultP1[$i]['FOLLOW_COUNT_P2']) $problemDesc .= "-จำนวนวันลาติดตามคู่สมรสไม่ตรงกัน\n";
                if ($ResultP1[$i]['HELP_WIFE_COUNT'] != $ResultP1[$i]['HELP_WIFE_COUNT_P2']) $problemDesc .= "-จำนวนวันลาไปช่วยเหลือภริยาที่คลอดบุตรงไม่ตรงกัน\n";
                if ($ResultP1[$i]['IMPROVE_COUNT'] != $ResultP1[$i]['IMPROVE_COUNT_P2']) $problemDesc .= "-จำนวนวันลาไปฟื้นฟูสมรรถภาพด้านอาชีพไม่ตรงกัน\n";
                if ($problemDesc == '') {
                    $bookCellIndex = 6 + $current;
                    continue;
                } else $errorBook++;
                $sheet->setCellValue('AF' . (6 + $current), rtrim($problemDesc));

                $problemDesc = '';
            }


            $sheet->setCellValue('A' . (6 + $current), ($current + 1));
            if (empty($ResultP1[$i]['PSN_PID'])) {
                $sheet->setCellValue('B' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('B' . (6 + $current), $ResultP1[$i]['PSN_PID']);
            }
            if (empty($ResultP1[$i]['PSN_FNAME'])) {
                $sheet->setCellValue('C' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('C' . (6 + $current), $ResultP1[$i]['TITLE_NAME'] . $ResultP1[$i]['PSN_FNAME'] . ' ' . $ResultP1[$i]['PSN_LNAME']);
            }
            if (empty($ResultP1[$i]['FISCAL_YEAR'])) {
                $sheet->setCellValue('D' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('D' . (6 + $current), $ResultP1[$i]['FISCAL_YEAR']);
            }
            if (empty($ResultP1[$i]['SICK_COUNT'])) {
                $sheet->setCellValue('E' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (6 + $current), $ResultP1[$i]['SICK_COUNT']);
            }
            if (empty($ResultP1[$i]['DELIVER_COUNT'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $ResultP1[$i]['DELIVER_COUNT']);
            }
            if (empty($ResultP1[$i]['BUSINESS_COUNT'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultP1[$i]['BUSINESS_COUNT']);
            }
            if (empty($ResultP1[$i]['REST_COUNT'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultP1[$i]['REST_COUNT']);
            }
            if (empty($ResultP1[$i]['ORDAIN_COUNT'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultP1[$i]['ORDAIN_COUNT']);
            }
            if (empty($ResultP1[$i]['HUJJ_COUNT'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultP1[$i]['HUJJ_COUNT']);
            }
            if (empty($ResultP1[$i]['MILITARY_COUNT'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultP1[$i]['MILITARY_COUNT']);
            }
            if (empty($ResultP1[$i]['STUDY_COUNT'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultP1[$i]['STUDY_COUNT']);
            }
            if (empty($ResultP1[$i]['INTER_COUNT'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultP1[$i]['INTER_COUNT']);
            }
            if (empty($ResultP1[$i]['FOLLOW_COUNT'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultP1[$i]['FOLLOW_COUNT']);
            }
            if (empty($ResultP1[$i]['HELP_WIFE_COUNT'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $ResultP1[$i]['HELP_WIFE_COUNT']);
            }
            if (empty($ResultP1[$i]['IMPROVE_COUNT'])) {
                $sheet->setCellValue('P' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (6 + $current), $ResultP1[$i]['IMPROVE_COUNT']);
            }

            if (empty($ResultP1[$i]['PSN_PID_P2'])) {
                $sheet->setCellValue('Q' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (6 + $current), $ResultP1[$i]['PSN_PID_P2']);
            }
            if (empty($ResultP1[$i]['PSN_FNAME_P2'])) {
                $sheet->setCellValue('R' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (6 + $current), $ResultP1[$i]['TITLE_NAME_P2'] . $ResultP1[$i]['PSN_FNAME_P2'] . ' ' . $ResultP1[$i]['PSN_LNAME_P2']);
            }
            if (empty($ResultP1[$i]['FISCAL_YEAR_P2'])) {
                $sheet->setCellValue('S' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (6 + $current), $ResultP1[$i]['FISCAL_YEAR_P2']);
            }
            if (empty($ResultP1[$i]['SICK_COUNT_P2'])) {
                $sheet->setCellValue('T' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (6 + $current), $ResultP1[$i]['SICK_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['DELIVER_COUNT_P2'])) {
                $sheet->setCellValue('U' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (6 + $current), $ResultP1[$i]['DELIVER_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['BUSINESS_COUNT_P2'])) {
                $sheet->setCellValue('V' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (6 + $current), $ResultP1[$i]['BUSINESS_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['REST_COUNT_P2'])) {
                $sheet->setCellValue('W' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('W' . (6 + $current), $ResultP1[$i]['REST_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['ORDAIN_COUNT_P2'])) {
                $sheet->setCellValue('X' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('X' . (6 + $current), $ResultP1[$i]['ORDAIN_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['HUJJ_COUNT_P2'])) {
                $sheet->setCellValue('Y' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('Y' . (6 + $current), $ResultP1[$i]['HUJJ_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['MILITARY_COUNT_P2'])) {
                $sheet->setCellValue('Z' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('Z' . (6 + $current), $ResultP1[$i]['MILITARY_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['STUDY_COUNT_P2'])) {
                $sheet->setCellValue('AA' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('AA' . (6 + $current), $ResultP1[$i]['STUDY_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['INTER_COUNT_P2'])) {
                $sheet->setCellValue('AB' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('AB' . (6 + $current), $ResultP1[$i]['INTER_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['FOLLOW_COUNT_P2'])) {
                $sheet->setCellValue('AC' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('AC' . (6 + $current), $ResultP1[$i]['FOLLOW_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['HELP_WIFE_COUNT_P2'])) {
                $sheet->setCellValue('AD' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('AD' . (6 + $current), $ResultP1[$i]['HELP_WIFE_COUNT_P2']);
            }
            if (empty($ResultP1[$i]['IMPROVE_COUNT_P2'])) {
                $sheet->setCellValue('AE' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('AE' . (6 + $current), $ResultP1[$i]['IMPROVE_COUNT_P2']);
            }




            $bookCellIndex = 6 + $current;
            $current++;
        }

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];




        $sheet->mergeCells('A1:AF1');
        $sheet->mergeCells('A2:AF2');
        $sheet->mergeCells('A3:AF3');
        $sheet->mergeCells('A4:P4');
        $sheet->mergeCells('Q4:AE4');
        $sheet->mergeCells('AF4:AF5');
        $sheet->getColumnDimension('AF')->setWidth(50);



        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', ' ที่มีข้อมูลแตกต่างกัน');
            $sheet->setCellValue('A3', $branchName);
            $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
            $sheet->setCellValue('Q4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
            $bn = 'ข้อมูลการลาที่ข้อมูลแตกต่างกัน';
            $fileName =  $branchName . '-ข้อมูลการลาที่ข้อมูลแตกต่างกันที่มีข้อมูลแตกต่างกัน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลการลา' . 'ที่ถ่ายโอนสำเร็จ');
            $sheet->setCellValue('A3', $branchName);
            $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
            $sheet->setCellValue('Q4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
            $bn = 'ข้อมูลการลาที่ถ่ายโอนสำเร็จ';
            $fileName =  $branchName . '-ข้อมูลการลาที่ถ่ายโอนสำเร็จ';
        }


        $sheet->getStyle('B5:B' . $last)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $sheet->getStyle('Q5:Q' . $last)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);

        $sheet->setCellValue('A5', '')
            ->setCellValue('B5', 'เลขบัตรประจำตัวประชาชน')
            ->setCellValue('C5', 'ชื่อ-สกุล')
            ->setCellValue('D5', 'ปีงบ')
            ->setCellValue('E5', 'ลาป่วย')
            ->setCellValue('F5', 'ลาคลอตบุตร')
            ->setCellValue('G5', 'ลากิจส่วนตัว')
            ->setCellValue('H5', 'ลาพักผ่อน')
            ->setCellValue('I5', 'ลาอุปสมบท')
            ->setCellValue('J5', 'ลาไปประกอบพิธีฮัจย์')
            ->setCellValue('K5', 'ลาเข้ารับการตรวจเลือก หรือเข้ารับการเตรียมพล')
            ->setCellValue('L5', 'ลาไปศึกษา ฝึกอบรม ปฏิบัติการวิจัย หรือดูงาน')
            ->setCellValue('M5', 'ลาไปปฏิบัติงานในองค์การระหว่างประเทศ')
            ->setCellValue('N5', 'ลาติดตามคู่สมรส')
            ->setCellValue('O5', 'ลาไปช่วยเหลือภริยาที่คลอดบุตร')
            ->setCellValue('P5', 'ลาไปฟื้นฟูสมรรถภาพด้านอาชีพ')
            ->setCellValue('Q5', 'เลขบัตรประจำตัวประชาชน')
            ->setCellValue('R5', 'ชื่อ-สกุล')
            ->setCellValue('S5', 'ปีงบ')
            ->setCellValue('T5', 'ลาป่วย')
            ->setCellValue('U5', 'ลาคลอตบุตร')
            ->setCellValue('V5', 'ลากิจส่วนตัว')
            ->setCellValue('W5', 'ลาพักผ่อน')
            ->setCellValue('X5', 'ลาอุปสมบท')
            ->setCellValue('Y5', 'ลาไปประกอบพิธีฮัจย์')
            ->setCellValue('Z5', 'ลาเข้ารับการตรวจเลือก หรือเข้ารับการเตรียมพล')
            ->setCellValue('AA5', 'ลาไปศึกษา ฝึกอบรม ปฏิบัติการวิจัย หรือดูงาน')
            ->setCellValue('AB5', 'ลาไปปฏิบัติงานในองค์การระหว่างประเทศ')
            ->setCellValue('AC5', 'ลาติดตามคู่สมรส')
            ->setCellValue('AD5', 'ลาไปช่วยเหลือภริยาที่คลอดบุตร')
            ->setCellValue('AE5', 'ลาไปฟื้นฟูสมรรถภาพด้านอาชีพ')
            ->setCellValue('AF4', 'หมายเหตุ')
            ->getStyle('A1:AF5')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->getStyle('A1:AF' . $bookCellIndex)->applyFromArray($styleArray);
        $sheet->getStyle('A1:AF5')->getFont()->setBold(true);
        $sheet->getStyle('A6:AF' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $sheet->getStyle('AF6:AF' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


        $sheet->getStyle('AF6:AF' . $bookCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('AF6:AF' . $bookCellIndex)
            ->getAlignment()
            ->setWrapText(true);



        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save('php://output');
        break;

    default:
        # code...
        break;
}
