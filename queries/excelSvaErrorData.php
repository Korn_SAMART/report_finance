<?php

require '../plugins/vendor/autoload.php';

include '../database/conn.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$Result = array();


$check = !isset($_GET['check']) ? '' : $_GET['check'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];
$jobgroup_seq =!isset($_GET['jobgroup_seq']) ? '' : $_GET['jobgroup_seq'];
$landoffice = !isset($_GET['landoffice']) ? '' : $_GET['landoffice'];
$branchName = !isset($_GET['branchName']) ? '' : $_GET['branchName'];
$private = !isset($_GET['private']) ? false : $_GET['private'];
// $check = explode('=', $argv[1])[1];
// $sts = explode('=', $argv[2])[1];
// $jobgroup_seq = explode('=', $argv[3])[1];
// $landoffice = explode('=', $argv[4])[1];
// $branchName = explode('=', $argv[5])[1];
// $private = explode('=', $argv[6])[1] ? explode('=', $argv[6])[1] : false;
include './sva/querySvaErrorData.php';
while (($row = oci_fetch_array($stid, OCI_ASSOC))) {
    $Result[] = $row;
}


$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
if ($check == 'digital_map') {
    $spreadsheet->getActiveSheet()->setTitle('ข้อมูลปรับปรุงรูปแปลง');
} else {
    if ($case == 'local') {
        $spreadsheet->getActiveSheet()->setTitle('ติดตามสถานะเรื่องรังวัด');
        $spreadsheet->createSheet(1)->setTitle('ผู้ถือกรรมสิทธิ์');
    } else {
        $spreadsheet->getActiveSheet()->setTitle('รายละเอียดแปลงที่ดิน');
        $spreadsheet->createSheet(1)->setTitle('รายละเอียดงานรังวัด');
    }

    $spreadsheet->createSheet(2)->setTitle('สถานะงานรังวัด');
    $spreadsheet->createSheet(3)->setTitle('ค่าใช้จ่าย');
    $spreadsheet->createSheet(4)->setTitle('การนัดรังวัด');
    if ($jobgroup_seq == 3) {
        $spreadsheet->createSheet(5)->setTitle('งานโครงการ');
    }
}


switch ($check) {
    case "local":

        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $surveyCellIndex = 0;
        $ownerCellIndex = 0;
        $surveyStsCellIndex = 0;
        $expenseCellIndex = 0;
        $appointCellIndex = 0;
        $projectCellIndex = 0;

        $errorParcel = 0;
        $errorOwner = 0;
        $errorSurveySts = 0;
        $errorExpense = 0;
        $errorAppoint = 0;
        $errorProject = 0;

        $current = 0;
        //===================PARCEL DETAIL=====================================
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();

            $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
            $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];

            include './sva/querySvaDetailData.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultDetail[] = $row;
            }
            $haserror = true;


            if ($sts == 'e') {
                for ($i = 0; $i < count($ResultDetail); $i++) {
                    $problemDesc = '';
                    if (!$ResultDetail[$i]['SURVEYJOB_SEQ_P1']) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$ResultDetail[$i]['SURVEYJOB_SEQ_P2']) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                    } else {
                        if ($ResultDetail[$i]['SURVEYJOB_NO_P1'] != $ResultDetail[$i]['SURVEYJOB_NO_P2']) $problemDesc .= "เลขลำดับไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['TYPEOFSURVEY_NAME_P1'] != $ResultDetail[$i]['TYPEOFSURVEY_NAME_P2']) $problemDesc .= "ประเภทการรังวัดไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['REQ_QUEUE_NO_P1'] != $ResultDetail[$i]['REQ_QUEUE_NO_P2']) $problemDesc .= "คำขอหรือหนังสือศาลไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['SURVEYJOB_DATE_P1'] != $ResultDetail[$i]['SURVEYJOB_DATE_P2']) $problemDesc .= "วันที่ลงไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PROVINCE_NAME_P1'] != $ResultDetail[$i]['PROVINCE_NAME_P2']) $problemDesc .= "จังหวัดไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['AMPHUR_NAME_P1'] != $ResultDetail[$i]['AMPHUR_NAME_P2']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['TAMBOL_NAME_P1'] != $ResultDetail[$i]['TAMBOL_NAME_P2']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['SURVEYOR_NAME_P1'] != $ResultDetail[$i]['SURVEYOR_NAME_P2']) $problemDesc .= "ชื่อช่างรังวัดไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['APPOINTMENT_DTM_P1'] != $ResultDetail[$i]['APPOINTMENT_DTM_P2']) $problemDesc .= "วันที่นัดรังวัดไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P1'] != $ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P2']) $problemDesc .= "ชื่อประเภทเอกสารสิทธิไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['LANDSURVEY_PARCEL_NO_P1'] != $ResultDetail[$i]['LANDSURVEY_PARCEL_NO_P2']) $problemDesc .= "เลขที่เอกสารสิทธิไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['UTM_P1'] != $ResultDetail[$i]['UTM_P2']) $problemDesc .= "ระวาง UTM ไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['LANDSURVEY_LAND_NO_P1'] != $ResultDetail[$i]['LANDSURVEY_LAND_NO_P2']) $problemDesc .= "เลขที่ดินไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['LANDSURVEY_SURVEY_NO_P1'] != $ResultDetail[$i]['LANDSURVEY_SURVEY_NO_P2']) $problemDesc .= "หน้าสำรวจไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['LANDSURVEY_MOO_P1'] != $ResultDetail[$i]['LANDSURVEY_MOO_P2']) $problemDesc .= "หมู่ไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['AREA_P1'] != $ResultDetail[$i]['AREA_P2']) $problemDesc .= "เนื้อที่ไม่ตรงกัน\n";
                    }
                    if ($problemDesc == '') {
                        $haserror = false;
                        continue;
                    }
                    $sheet->setCellValue('W' . (5 + $current + $i), rtrim($problemDesc));
                    $problemDesc = '';
                }
                if (!$haserror) continue;
                else $errorParcel++;
            }


            if (count($ResultDetail) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + (count($ResultDetail)) - 1));
                $sheet->mergeCells('F' . (5 + $current + (count($ResultDetail))) . ':F' . (5 + $current + (count($ResultDetail) * 2) - 1));


                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }

                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (5 + $current + count($ResultDetail)), 'พัฒน์ฯ 2');

                for ($i = 0; $i < count($ResultDetail); $i++) {
                    if (empty($ResultDetail[$i]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + $i), $ResultDetail[$i]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($ResultDetail[$i]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultDetail[$i]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($ResultDetail[$i]['REQ_QUEUE_NO_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultDetail[$i]['REQ_QUEUE_NO_P1']);
                    }
                    if (empty($ResultDetail[$i]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultDetail[$i]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($ResultDetail[$i]['PROVINCE_NAME_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultDetail[$i]['PROVINCE_NAME_P1']);
                    }
                    if (empty($ResultDetail[$i]['AMPHUR_NAME_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultDetail[$i]['AMPHUR_NAME_P1']);
                    }
                    if (empty($ResultDetail[$i]['TAMBOL_NAME_P1'])) {
                        $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + $i), $ResultDetail[$i]['TAMBOL_NAME_P1']);
                    }
                    if (empty($ResultDetail[$i]['SURVEYOR_NAME_P1'])) {
                        $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + $i), $ResultDetail[$i]['SURVEYOR_NAME_P1']);
                    }
                    if (empty($ResultDetail[$i]['APPOINTMENT_DTM_P1'])) {
                        $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + $i), $ResultDetail[$i]['APPOINTMENT_DTM_P1']);
                    }
                    if (empty($ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P1'])) {
                        $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + $i), $ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P1']);
                    }
                    if (empty($ResultDetail[$i]['LANDSURVEY_PARCEL_NO_P1'])) {
                        $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + $i), $ResultDetail[$i]['LANDSURVEY_PARCEL_NO_P1']);
                    }
                    if (empty($ResultDetail[$i]['UTM_P1'])) {
                        $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + $i), $ResultDetail[$i]['UTM_P1']);
                    }
                    if (empty($ResultDetail[$i]['LANDSURVEY_LAND_NO_P1'])) {
                        $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + $i), $ResultDetail[$i]['LANDSURVEY_LAND_NO_P1']);
                    }
                    if (empty($ResultDetail[$i]['LANDSURVEY_SURVEY_NO_P1'])) {
                        $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + $i), $ResultDetail[$i]['LANDSURVEY_SURVEY_NO_P1']);
                    }
                    if (empty($ResultDetail[$i]['LANDSURVEY_MOO_P1'])) {
                        $sheet->setCellValue('U' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + $i), $ResultDetail[$i]['LANDSURVEY_MOO_P1']);
                    }
                    if (empty($ResultDetail[$i]['AREA_P1'])) {
                        $sheet->setCellValue('V' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('V' . (5 + $current + $i), $ResultDetail[$i]['AREA_P1']);
                    }

                    if (empty($ResultDetail[$i]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($ResultDetail[$i]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($ResultDetail[$i]['REQ_QUEUE_NO_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['REQ_QUEUE_NO_P2']);
                    }
                    if (empty($ResultDetail[$i]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($ResultDetail[$i]['PROVINCE_NAME_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROVINCE_NAME_P2']);
                    }
                    if (empty($ResultDetail[$i]['AMPHUR_NAME_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['AMPHUR_NAME_P2']);
                    }
                    if (empty($ResultDetail[$i]['TAMBOL_NAME_P2'])) {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['TAMBOL_NAME_P2']);
                    }
                    if (empty($ResultDetail[$i]['SURVEYOR_NAME_P2'])) {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['SURVEYOR_NAME_P2']);
                    }
                    if (empty($ResultDetail[$i]['APPOINTMENT_DTM_P2'])) {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['APPOINTMENT_DTM_P2']);
                    }
                    if (empty($ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P2'])) {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P2']);
                    }
                    if (empty($ResultDetail[$i]['LANDSURVEY_PARCEL_NO_P2'])) {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['LANDSURVEY_PARCEL_NO_P2']);
                    }
                    if (empty($ResultDetail[$i]['UTM_P2'])) {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['UTM_P2']);
                    }
                    if (empty($ResultDetail[$i]['LANDSURVEY_LAND_NO_P2'])) {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['LANDSURVEY_LAND_NO_P2']);
                    }
                    if (empty($ResultDetail[$i]['LANDSURVEY_SURVEY_NO_P2'])) {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['LANDSURVEY_SURVEY_NO_P2']);
                    }
                    if (empty($ResultDetail[$i]['LANDSURVEY_MOO_P2'])) {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['LANDSURVEY_MOO_P2']);
                    }
                    if (empty($ResultDetail[$i]['AREA_P2'])) {
                        $sheet->setCellValue('V' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('V' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['AREA_P2']);
                    }

                    $surveyCellIndex = (5 + $current + count($ResultDetail) + $i);
                }

                $current += count($ResultDetail) * 2;
            }
        }



        // =========== OWNER ====================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        for ($j = 0; $j < $last; $j++) {

            $ResultOwner = array();

            $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
            $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];

            include './sva/querySvaLandsurveyData.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOwner[] = $row;
            }
            $haserror = true;

            if ($sts == 'e') {
                for ($i = 0; $i < count($ResultOwner); $i++) {
                    $problemDesc = '';
                    if (!$surveyjobSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$surveyjobSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                    } else {
                        if ($ResultOwner[$i]['OWNER_FIRST_FLAG_P1'] != $ResultOwner[$i]['OWNER_FIRST_FLAG_P2']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultOwner[$i]['OWNER_PID_P1'] != $ResultOwner[$i]['OWNER_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                        if ($ResultOwner[$i]['OWNER_P1'] != $ResultOwner[$i]['OWNER_P2']) $problemDesc .= "ชื่อ-สกุลผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultOwner[$i]['ADDRESS_P1'] != $ResultOwner[$i]['ADDRESS_P2']) $problemDesc .= "ที่อยู่ไม่ตรงกัน\n";
                        if ($ResultOwner[$i]['AGENT_NAME_P1'] != $ResultOwner[$i]['AGENT_NAME_P2']) $problemDesc .= "ชื่อ-สกุลผู้จัดการแทนไม่ตรงกัน\n";
                        if ($ResultOwner[$i]['MANDATE_NAME_P1'] != $ResultOwner[$i]['MANDATE_NAME_P2'])  $problemDesc .= "ชื่อ-สกุลผู้รับมอบอำนาจไม่ตรงกัน\n";
                    }
                    if ($problemDesc == '') {
                        $haserror = false;
                        continue;
                    }
                    $sheet->setCellValue('M' . (5 + $current + $i), rtrim($problemDesc));
                    $problemDesc = '';
                }
                if (!$haserror) continue;
                else $errorOwner++;
            }


            if (count($ResultOwner) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultOwner) * 2) - 1));
                // $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + (count($ResultOwner)) - 1));
                $sheet->mergeCells('F' . (5 + $current + (count($ResultOwner))) . ':F' . (5 + $current + (count($ResultOwner) * 2) - 1));


                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }

                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (5 + $current + count($ResultOwner)), 'พัฒน์ฯ 2');

                for ($i = 0; $i < count($ResultOwner); $i++) {
                    if (empty($ResultOwner[$i]['OWNER_FIRST_FLAG_P1'])) {
                        $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + $i), $ResultOwner[$i]['OWNER_FIRST_FLAG_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P1'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PID_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultOwner[$i]['OWNER_P1']);
                    }
                    if (empty($ResultOwner[$i]['ADDRESS_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultOwner[$i]['ADDRESS_P1']);
                    }
                    if (empty($ResultOwner[$i]['AGENT_NAME_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultOwner[$i]['AGENT_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['MANDATE_NAME_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultOwner[$i]['MANDATE_NAME_P1']);
                    }


                    if (empty($ResultOwner[$i]['OWNER_FIRST_FLAG_P2'])) {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_FIRST_FLAG_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PID_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_P2']);
                    }
                    if (empty($ResultOwner[$i]['ADDRESS_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['ADDRESS_P2']);
                    }
                    if (empty($ResultOwner[$i]['AGENT_NAME_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['AGENT_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['MANDATE_NAME_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['MANDATE_NAME_P2']);
                    }





                    $ownerCellIndex = (5 + $current + count($ResultOwner) + $i);
                }

                $current += count($ResultOwner) * 2;
            }
        }


        // =========== JOBSTATUS ====================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        for ($j = 0; $j < $last; $j++) {

            $ResultStatus = array();

            $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
            $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];

            include './sva/querySvaJobStatusData.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultStatus[] = $row;
            }

            $haserror = true;


            if ($sts == 'e') {
                for ($i = 0; $i < count($ResultStatus); $i++) {
                    $problemDesc = '';
                    if (!$surveyjobSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$surveyjobSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                    } else {
                        if ($ResultStatus[$i]['MOVEMENT_DTM_P1'] != $ResultStatus[$i]['MOVEMENT_DTM_P2']) $problemDesc .= "วันที่บันทึกสถานะไม่ตรงกัน\n";
                    }
                    if ($problemDesc == '') {
                        $haserror = false;
                        continue;
                    }

                    $sheet->setCellValue('I' . (5 + $current + $i), rtrim($problemDesc));
                    $problemDesc = '';
                }

                if (!$haserror) continue;
                else $errorSurveySts++;
            }


            if (count($ResultStatus) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultStatus)) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultStatus)) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultStatus)) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultStatus)) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultStatus)) - 1));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }


                for ($i = 0; $i < count($ResultStatus); $i++) {
                    if (empty($ResultStatus[$i]['JOBSTATUS_NAME'])) {
                        $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current + $i), $ResultStatus[$i]['JOBSTATUS_NAME']);
                    }
                    if (empty($ResultStatus[$i]['MOVEMENT_DTM_P1'])) {
                        $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + $i), $ResultStatus[$i]['MOVEMENT_DTM_P1']);
                    }
                    if (empty($ResultStatus[$i]['MOVEMENT_DTM_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultStatus[$i]['MOVEMENT_DTM_P2']);
                    }

                    $surveyStsCellIndex = 5 + $current + $i;
                }

                $current += count($ResultStatus);
            }
        }


        // =========== expense ====================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        for ($j = 0; $j < $last; $j++) {

            $ResultExpense = array();

            $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
            $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];

            include './sva/querySvaExpenseData.php';
            $table = 'withdraw';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultExpense[] = $row;
            }
            $haserror = true;
            if ($sts == 'e') {
                if (!$surveyjobSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$surveyjobSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                } else {
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['BTD59_NO_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['BTD59_NO_P2']) $problemDesc .= "เลขที่ บ.ท.ด.59 ไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_DATE_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_DATE_P2']) $problemDesc .= "วันที่ทำการถอนจ่ายไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_AMOUNT_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_AMOUNT_P2']) $problemDesc .= "จำนวนเงินที่ถอนจ่ายไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_ADJUST_TYPE_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_ADJUST_TYPE_P2']) $problemDesc .= "จำนวนเงินที่เรียกเพิ่ม/คืนเงินไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAWSTATUS_NAME_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAWSTATUS_NAME_P2']) $problemDesc .= "สถานะไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_EXPENSE_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_EXPENSE_P2'])  $problemDesc .= "จำนวนเงินรวมไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_AMOUNT_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_AMOUNT_P2'])  $problemDesc .= "จำนวนเงินที่เรียกเพิ่มจากผู้ขอไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_ADJUST_AMOUNT_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_ADJUST_AMOUNT_P2'])  $problemDesc .= "จำนวนเงินที่คืนผู้ขอไม่ตรงกัน\n";
                }
                if ($problemDesc == '') {
                    $haserror = false;
                    continue;
                }
                $sheet->setCellValue('O' . (5 + $current), rtrim($problemDesc));
                $problemDesc = '';
            }
            if (!$haserror) continue;
            else $errorExpense++;



            if (count($ResultExpense) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));


                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }

                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (6 + $current), 'พัฒน์ฯ 2');


                if (empty($ResultExpense[count($ResultExpense) - 1]['BTD59_NO_P1'])) {
                    $sheet->setCellValue('G' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['BTD59_NO_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_DATE_P1'])) {
                    $sheet->setCellValue('H' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_DATE_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P1'])) {
                    $sheet->setCellValue('I' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_TYPE_P1'])) {
                    $sheet->setCellValue('J' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_TYPE_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAWSTATUS_NAME_P1'])) {
                    $sheet->setCellValue('K' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAWSTATUS_NAME_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_EXPENSE_P1'])) {
                    $sheet->setCellValue('L' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_EXPENSE_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P1'])) {
                    $sheet->setCellValue('M' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_AMOUNT_P1'])) {
                    $sheet->setCellValue('N' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_AMOUNT_P1']);
                }


                if (empty($ResultExpense[count($ResultExpense) - 1]['BTD59_NO_P2'])) {
                    $sheet->setCellValue('G' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('G' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['BTD69_NO_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_DATE_P2'])) {
                    $sheet->setCellValue('H' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('H' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_DATE_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P2'])) {
                    $sheet->setCellValue('I' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('I' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_TYPE_P2'])) {
                    $sheet->setCellValue('J' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('J' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_TYPE_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAWSTATUS_NAME_P2'])) {
                    $sheet->setCellValue('K' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('K' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAWSTATUS_NAME_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_EXPENSE_P2'])) {
                    $sheet->setCellValue('L' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_EXPENSE_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P2'])) {
                    $sheet->setCellValue('M' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_AMOUNT_P2'])) {
                    $sheet->setCellValue('N' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_AMOUNT_P2']);
                }
                $expenseCellIndex =  (6 + $current);


                $current += 2;
            }
        }


        // =========== APPOINTMENT ====================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        for ($j = 0; $j < $last; $j++) {

            $ResultAppointment = array();

            $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
            $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];

            include './sva/querySvaAppointmentData.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultAppointment[] = $row;
            }
            $haserror = true;

            if ($sts == 'e') {
                for ($i = 0; $i < count($ResultAppointment); $i++) {
                    $problemDesc = '';
                    if (!$surveyjobSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$surveyjobSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                    } else {
                        if ($ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P1'] != $ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P2']) $problemDesc .= "ชื่อสำนักงานรังวัดเอกชนไม่ตรงกัน\n";
                        if ($ResultAppointment[$i]['PV_SURVEYOR_NAME_P1'] != $ResultAppointment[$i]['PV_SURVEYOR_NAME_P2']) $problemDesc .= "ชื่อช่างรังวัดเอกชนไม่ตรงกัน\n";
                        if ($ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P1'] != $ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P2']) $problemDesc .= "หมายเลขโทรศัพท์มือถือช่างรังวัดเอกชนไม่ตรงกัน\n";
                        if ($ResultAppointment[$i]['APPOINTMENT_DTM_P1'] != $ResultAppointment[$i]['APPOINTMENT_DTM_P2']) $problemDesc .= "วันที่นัดรังวัดไม่ตรงกัน\n";
                        if ($ResultAppointment[$i]['APPOINTMENT_TIME_P1'] != $ResultAppointment[$i]['APPOINTMENT_TIME_P2']) $problemDesc .= "เวลานัดรังวัดไม่ตรงกัน\n";
                        if ($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1'] != $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2']) $problemDesc .= "จำนวนวันที่รังวัดไม่ตรงกัน\n";
                        if ($ResultAppointment[$i]['SURVEYOR_NAME_P1'] != $ResultAppointment[$i]['SURVEYOR_NAME_P2']) $problemDesc .= "ชื่อนายช่างรังวัดสำนักงานที่ดินรับผิดชอบไม่ตรงกัน\n";
                        if ($ResultAppointment[$i]['SURVEYOR_MOBILE_P1'] != $ResultAppointment[$i]['SURVEYOR_MOBILE_P2']) $problemDesc .= "หมายเลขโทรศัพท์มือถือนายช่างรังวัดสำนักงานที่ดินรับผิดชอบไม่ตรงกัน\n";
                        if ($ResultAppointment[$i]['APPOINTMENT_REM_P1'] != $ResultAppointment[$i]['APPOINTMENT_REM_P2'])  $problemDesc .= "หมายเหตุการรังวัดไม่ตรงกัน\n";
                        if ($ResultAppointment[$i]['APPOINTMENT_WRITER_P2'] != $ResultAppointment[$i]['APPOINTMENT_WRITER_P2'])  $problemDesc .= "ชื่อผู้บันทึกนัดรังวัดไม่ตรงกัน\n";
                    }
                    if ($problemDesc == '') {
                        $haserror = false;
                        continue;
                    }
                    if ($private) {
                        $sheet->setCellValue('Q' . (5 + $current + $i), rtrim($problemDesc));
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + $i), rtrim($problemDesc));
                    }
                    $problemDesc = '';
                }
                if (!$haserror) continue;
                else $errorAppoint++;
            }

            if (count($ResultAppointment) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));
                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultAppointment) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultAppointment) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultAppointment) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultAppointment) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultAppointment) * 2) - 1));
                // $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + (count($ResultAppointment)) - 1));
                $sheet->mergeCells('F' . (5 + $current + (count($ResultAppointment))) . ':F' . (5 + $current + (count($ResultAppointment) * 2) - 1));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }

                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (5 + $current + count($ResultAppointment)), 'พัฒน์ฯ 2');

                for ($i = 0; $i < count($ResultAppointment); $i++) {
                    if ($private) {
                        if (empty($ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P1'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['PV_SURVEYOR_NAME_P1'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultAppointment[$i]['PV_SURVEYOR_NAME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P1'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DTM_P1'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_DTM_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_TIME_P1'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_TIME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1'])) {
                            $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_NAME_P1'])) {
                            $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + $i), $ResultAppointment[$i]['SURVEYOR_NAME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_MOBILE_P1'])) {
                            $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('N' .  (5 + $current + $i), $ResultAppointment[$i]['SURVEYOR_MOBILE_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_REM_P1'])) {
                            $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('O' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_REM_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_WRITER_P1'])) {
                            $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('P' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_WRITER_P1']);
                        }


                        if (empty($ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P2'])) {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['PV_SURVEYOR_NAME_P2'])) {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['PV_SURVEYOR_NAME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P2'])) {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DTM_P2'])) {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['APPOINTMENT_DTM_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_TIME_P2'])) {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['APPOINTMENT_TIME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2'])) {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_NAME_P2'])) {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['SURVEYOR_NAME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_MOBILE_P2'])) {
                            $sheet->setCellValue('N' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('N' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['SURVEYOR_MOBILE_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_REM_P2'])) {
                            $sheet->setCellValue('O' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('O' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_REM_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_WRITER_P2'])) {
                            $sheet->setCellValue('P' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('P' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_WRITER_P2']);
                        }


                        $appointCellIndex = (5 + $current + count($ResultAppointment) + $i);
                    } else {
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DTM_P1'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_DTM_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_TIME_P1'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_TIME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_NAME_P1'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultAppointment[$i]['SURVEYOR_NAME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_MOBILE_P1'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultAppointment[$i]['SURVEYOR_MOBILE_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DETAIL_P1'])) {
                            $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_DETAIL_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_WRITER_P1'])) {
                            $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_WRITER_P1']);
                        }



                        if (empty($ResultAppointment[$i]['APPOINTMENT_DTM_P2'])) {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_DTM_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_TIME_P2'])) {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_TIME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2'])) {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_NAME_P2'])) {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['SURVEYOR_NAME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_MOBILE_P2'])) {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['SURVEYOR_MOBILE_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DETAIL_P2'])) {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_DETAIL_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_WRITER_P2'])) {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_WRITER_P2']);
                        }

                        $appointCellIndex = (5 + $current + count($ResultAppointment) + $i);
                    }
                }

                $current += count($ResultAppointment) * 2;
            }
        }

        //===================project DETAIL=====================================
        if ($jobgroup_seq == 3) {
            $current = 0;

            $sheet = $spreadsheet->setActiveSheetIndex(5);
            for ($j = 0; $j < $last; $j++) {

                $ResultDetail = array();

                $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
                $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];
                $check = 'project';

                include './sva/querySvaDetailData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultDetail[] = $row;
                }
                $haserror = true;
                if ($sts == 'e') {
                    for ($i = 0; $i < count($ResultDetail); $i++) {
                        $problemDesc = '';
                        if (!$surveyjobSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                        } else if (!$surveyjobSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                        } else {
                            if ($ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P1'] != $ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P2']) $problemDesc .= "ประเภทเอกสารสิทธิไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['METHODOFSURVEY_NAME_P1'] != $ResultDetail[$i]['METHODOFSURVEY_NAME_P2']) $problemDesc .= "วิธีการรังวัดไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['SURVEYJOB_NO_P1'] != $ResultDetail[$i]['SURVEYJOB_NO_P2']) $problemDesc .= "เลขที่คำขอรังวัด (ร.ว.12) ไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['SURVEYJOB_DATE_P1'] != $ResultDetail[$i]['SURVEYJOB_DATE_P2']) $problemDesc .= "วันที่รับคำขอไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['TYPEOFSURVEY_NAME_P1'] != $ResultDetail[$i]['TYPEOFSURVEY_NAME_P2']) $problemDesc .= "ประเภทการรังวัดไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['PROJECT_NAME_P1'] != $ResultDetail[$i]['PROJECT_NAME_P2']) $problemDesc .= "ชื่อโครงการไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['PROJECT_DOCNO_P1'] != $ResultDetail[$i]['PROJECT_DOCNO_P2']) $problemDesc .= "เลขที่หนังสือไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['PROJECT_DOCDATE_P1'] != $ResultDetail[$i]['PROJECT_DOCDATE_P2']) $problemDesc .= "ลงวันที่ไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['PROJECT_OFFICEOWNER_P1'] != $ResultDetail[$i]['PROJECT_OFFICEOWNER_P2']) $problemDesc .= "ชื่อหน่วยงานไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['PROJECTTYPE_NAME_P1'] != $ResultDetail[$i]['PROJECTTYPE_NAME_P2']) $problemDesc .= "ประเภทโครงการไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P1'] != $ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P2']) $problemDesc .= "ประเภทรังวัดหลักไม่ตรงกัน\n";
                        }
                        if ($problemDesc == '') {
                            $haserror = false;
                            continue;
                        }
                        $sheet->setCellValue('R' . (5 + $current + $i), rtrim($problemDesc));
                        $problemDesc = '';
                    }
                    if (!$haserror) continue;
                    else $errorProject++;
                }


                if (count($ResultDetail) <= 0) {
                    $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                    $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                    $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                    $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                    $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                    if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                        $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                        if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                            $sheet->setCellValue('B' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                        }
                        if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                            $sheet->setCellValue('C' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                        }
                        if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                            $sheet->setCellValue('D' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                        }
                        if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                            $sheet->setCellValue('E' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                        }
                    } else {

                        $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                        if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                            $sheet->setCellValue('B' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                        }
                        if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                            $sheet->setCellValue('C' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                        }
                        if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                            $sheet->setCellValue('D' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                        }
                        if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                            $sheet->setCellValue('E' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                        }
                    }
                    $current += 2;
                } else {
                    $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + (count($ResultDetail)) - 1));
                    $sheet->mergeCells('F' . (5 + $current + (count($ResultDetail))) . ':F' . (5 + $current + (count($ResultDetail) * 2) - 1));


                    if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                        $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                        if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                            $sheet->setCellValue('B' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                        }
                        if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                            $sheet->setCellValue('C' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                        }
                        if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                            $sheet->setCellValue('D' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                        }
                        if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                            $sheet->setCellValue('E' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                        }
                    } else {

                        $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                        if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                            $sheet->setCellValue('B' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                        }
                        if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                            $sheet->setCellValue('C' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                        }
                        if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                            $sheet->setCellValue('D' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                        }
                        if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                            $sheet->setCellValue('E' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                        }
                    }

                    $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                    $sheet->setCellValue('F' . (5 + $current + count($ResultDetail)), 'พัฒน์ฯ 2');

                    for ($i = 0; $i < count($ResultDetail); $i++) {
                        if (empty($ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P1'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P1']);
                        }
                        if (empty($ResultDetail[$i]['METHODOFSURVEY_NAME_P1'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultDetail[$i]['METHODOFSURVEY_NAME_P1']);
                        }
                        if (empty($ResultDetail[$i]['SURVEYJOB_NO_P1'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultDetail[$i]['SURVEYJOB_NO_P1']);
                        }
                        if (empty($ResultDetail[$i]['SURVEYJOB_DATE_P1'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultDetail[$i]['SURVEYJOB_DATE_P1']);
                        }
                        if (empty($ResultDetail[$i]['TYPEOFSURVEY_NAME_P1'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultDetail[$i]['TYPEOFSURVEY_NAME_P1']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_NAME_P1'])) {
                            $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + $i), $ResultDetail[$i]['PROJECT_NAME_P1']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_DOCNO_P1'])) {
                            $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + $i), $ResultDetail[$i]['PROJECT_DOCNO_P1']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_DOCDATE_P1'])) {
                            $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('N' . (5 + $current + $i), $ResultDetail[$i]['PROJECT_DOCDATE_P1']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_OFFICEOWNER_P1'])) {
                            $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('O' . (5 + $current + $i), $ResultDetail[$i]['PROJECT_OFFICEOWNER_P1']);
                        }
                        if (empty($ResultDetail[$i]['PROJECTTYPE_NAME_P1'])) {
                            $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('P' . (5 + $current + $i), $ResultDetail[$i]['PROJECTTYPE_NAME_P1']);
                        }
                        if (empty($ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P1'])) {
                            $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('Q' . (5 + $current + $i), $ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P1']);
                        }


                        if (empty($ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P2'])) {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P2']);
                        }
                        if (empty($ResultDetail[$i]['METHODOFSURVEY_NAME_P2'])) {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['METHODOFSURVEY_NAME_P2']);
                        }
                        if (empty($ResultDetail[$i]['SURVEYJOB_NO_P2'])) {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['SURVEYJOB_NO_P2']);
                        }
                        if (empty($ResultDetail[$i]['SURVEYJOB_DATE_P2'])) {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['SURVEYJOB_DATE_P2']);
                        }
                        if (empty($ResultDetail[$i]['TYPEOFSURVEY_NAME_P2'])) {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['TYPEOFSURVEY_NAME_P2']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_NAME_P2'])) {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROJECT_NAME_P2']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_DOCNO_P2'])) {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROJECT_DOCNO_P2']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_DOCDATE_P2'])) {
                            $sheet->setCellValue('N' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('N' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROJECT_DOCDATE_P2']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_OFFICEOWNER_P2'])) {
                            $sheet->setCellValue('O' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('O' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROJECT_OFFICEOWNER_P2']);
                        }
                        if (empty($ResultDetail[$i]['PROJECTTYPE_NAME_P2'])) {
                            $sheet->setCellValue('P' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('P' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROJECTTYPE_NAME_P2']);
                        }
                        if (empty($ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P2'])) {
                            $sheet->setCellValue('Q' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('Q' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P2']);
                        }





                        $projectCellIndex = (5 + $current + count($ResultDetail) + $i);
                    }

                    $current += count($ResultDetail) * 2;
                }
            }

        }


        $sheet = $spreadsheet->setActiveSheetIndex(0);


        $sheet->mergeCells('A1:W1');
        $sheet->mergeCells('A2:W2');
        $sheet->mergeCells('A3:W3');
        $sheet->getColumnDimension('W')->setWidth(50);

        $fileTypeName = '';
        switch ($jobgroup_seq) {
            case 1:
                $fileTypeName = 'งานสำนักงานที่ดิน';
                break;
            case 2:
                $fileTypeName = 'งานรังวัดเอกชน';
                break;
            case 3:
                $fileTypeName = 'งานโครงการ';
                break;
        }

        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            $fileName =  $branchName . '-SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            $fileName =  $branchName . '-SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขที่คำขอรังวัด')
            ->setCellValue('C4', 'วันที่รับคำขอ')
            ->setCellValue('D4', 'ประเภทการรังวัด')
            ->setCellValue('E4', 'สถานะเรื่องรังวัด')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'เลขลำดับ')
            ->setCellValue('H4', 'ประเภทการรังวัด')
            ->setCellValue('I4', 'คำขอหรือหนังสือศาล')
            ->setCellValue('J4', 'ลงวันที่')
            ->setCellValue('K4', 'จังหวัด')
            ->setCellValue('L4', 'อำเภอ')
            ->setCellValue('M4', 'ตำบล')
            ->setCellValue('N4', 'ชื่อช่างรังวัด')
            ->setCellValue('O4', 'วันที่นัดรังวัด')
            ->setCellValue('P4', 'ประเภทเอกสารสิทธิ')
            ->setCellValue('Q4', 'เลขที่เอกสารสิทธิ')
            ->setCellValue('R4', 'ระวาง UTM')
            ->setCellValue('S4', 'เลขที่ดิน')
            ->setCellValue('T4', 'หน้าสำรวจ')
            ->setCellValue('U4', 'หมู่')
            ->setCellValue('V4', 'เนื้อที่')
            ->setCellValue('W4', 'หมายเหตุ')
            ->getStyle('A1:W4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->getColumnDimension('M')->setWidth(50);
        $sheet->mergeCells('A1:M1');
        $sheet->mergeCells('A2:M2');
        $sheet->mergeCells('A3:M3');




        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            $fileName =  $branchName . '-SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            $fileName =  $branchName . '-SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขที่คำขอรังวัด')
            ->setCellValue('C4', 'วันที่รับคำขอ')
            ->setCellValue('D4', 'ประเภทการรังวัด')
            ->setCellValue('E4', 'สถานะเรื่องรังวัด')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'ลำดับผู้ถือกรรมสิทธิ์')
            ->setCellValue('H4', 'เลขบัตรประจำตัวประชาชน')
            ->setCellValue('I4', 'ชื่อ-สกุลผู้ถือกรรมสิทธิ์')
            ->setCellValue('J4', 'ที่อยู่')
            ->setCellValue('K4', 'ชื่อ-สกุลผู้จัดการแทน')
            ->setCellValue('L4', 'ชื่อ-สกุลผู้รับมอบอำนาจ')
            ->setCellValue('M4', 'หมายเหตุ')
            ->getStyle('A1:M4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);




        $sheet = $spreadsheet->setActiveSheetIndex(2);
        $sheet->getColumnDimension('I')->setWidth(50);
        $sheet->mergeCells('A1:I1');
        $sheet->mergeCells('A2:I2');
        $sheet->mergeCells('A3:I3');


        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            $fileName =  $branchName . '-SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            $fileName =  $branchName . '-SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขที่คำขอรังวัด')
            ->setCellValue('C4', 'วันที่รับคำขอ')
            ->setCellValue('D4', 'ประเภทการรังวัด')
            ->setCellValue('E4', 'สถานะเรื่องรังวัด')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'สถานะงานรังวัด')
            ->setCellValue('G4', 'วันที่บันทึกพัฒน์ฯ 1')
            ->setCellValue('H4', 'วันที่บันทึกพัฒน์ฯ 2')
            ->setCellValue('I4', 'หมายเหตุ')
            ->getStyle('A1:I4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $sheet->getColumnDimension('O')->setWidth(50);
        $sheet->mergeCells('A1:O1');
        $sheet->mergeCells('A2:O2');
        $sheet->mergeCells('A3:O3');


        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            $fileName =  $branchName . '-SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            $fileName =  $branchName . '-SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขที่คำขอรังวัด')
            ->setCellValue('C4', 'วันที่รับคำขอ')
            ->setCellValue('D4', 'ประเภทการรังวัด')
            ->setCellValue('E4', 'สถานะเรื่องรังวัด')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'เลขที่ บ.ท.ด.59')
            ->setCellValue('H4', 'วันที่ทำการถอนจ่าย')
            ->setCellValue('I4', 'จำนวนเงินที่ถอนจ่าย')
            ->setCellValue('J4', 'จำนวนเงินที่เรียกเพิ่ม/คืนเงิน')
            ->setCellValue('K4', 'สถานะ')
            ->setCellValue('L4', 'จำนวนเงินรวม')
            ->setCellValue('M4', 'จำนวนเงินที่เรียกเพิ่มจากผู้ขอ')
            ->setCellValue('N4', 'จำนวนเงินที่คืนผู้ขอ')
            ->setCellValue('O4', 'หมายเหตุ')
            ->getStyle('A1:O4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);



        $sheet = $spreadsheet->setActiveSheetIndex(4);
        if ($private) {
            $sheet->getColumnDimension('Q')->setWidth(50);
            $sheet->mergeCells('A1:Q1');
            $sheet->mergeCells('A2:Q2');
            $sheet->mergeCells('A3:Q3');


            if ($sts == 'e') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
                $fileName =  $branchName . '-SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            } else if ($sts == 's') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
                $fileName =  $branchName . '-SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            }


            $sheet->setCellValue('A4', '')
                ->setCellValue('B4', 'เลขที่คำขอรังวัด')
                ->setCellValue('C4', 'วันที่รับคำขอ')
                ->setCellValue('D4', 'ประเภทการรังวัด')
                ->setCellValue('E4', 'สถานะเรื่องรังวัด')
                ->getStyle('A1:E4')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
                ->setCellValue('G4', 'สำนักงานรังวัดเอกชน')
                ->setCellValue('H4', 'ช่างรังวัดเอกชน')
                ->setCellValue('I4', 'โทรศัพท์มือถือช่างรังวัดเอกชน')
                ->setCellValue('J4', 'วันที่นัดรังวัด')
                ->setCellValue('K4', 'เวลานัดรังวัด')
                ->setCellValue('L4', 'จำนวนวันที่รังวัด')
                ->setCellValue('M4', 'นายช่างรังวัดสำนักงานที่ดินรับผิดชอบ')
                ->setCellValue('N4', 'โทรศัพท์มือถือนายช่างรังวัดสำนักงานที่ดินรับผิดชอบ')
                ->setCellValue('O4', 'หมายเหตุการรังวัด')
                ->setCellValue('P4', 'ชื่อผู้บันทึกนัดรังวัด')
                ->setCellValue('Q4', 'หมายเหตุ')
                ->getStyle('A1:Q4')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        } else {
            $sheet->getColumnDimension('N')->setWidth(50);
            $sheet->mergeCells('A1:N1');
            $sheet->mergeCells('A2:N2');
            $sheet->mergeCells('A3:N3');


            if ($sts == 'e') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
                $fileName =  $branchName . '-SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            } else if ($sts == 's') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
                $fileName =  $branchName . '-SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            }
            $sheet->setCellValue('A4', '')
                ->setCellValue('B4', 'เลขที่คำขอรังวัด')
                ->setCellValue('C4', 'วันที่รับคำขอ')
                ->setCellValue('D4', 'ประเภทการรังวัด')
                ->setCellValue('E4', 'สถานะเรื่องรังวัด')
                ->getStyle('A1:E4')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            if ($private) {
                $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
                    ->setCellValue('G4', 'สำนักงานรังวัดเอกชน')
                    ->setCellValue('H4', 'ช่างรังวัดเอกชน')
                    ->setCellValue('I4', 'โทรศัพท์มือถือช่างรังวัดเอกชน')
                    ->setCellValue('J4', 'วันที่นัดรังวัด')
                    ->setCellValue('K4', 'เวลานัดรังวัด')
                    ->setCellValue('L4', 'จำนวนวันที่รังวัด')
                    ->setCellValue('M4', 'นายช่างรังวัดสำนักงานที่ดินรับผิดชอบ')
                    ->setCellValue('N4', 'โทรศัพท์มือถือนายช่างรังวัดสำนักงานที่ดินรับผิดชอบ')
                    ->setCellValue('O4', 'หมายเหตุการรังวัด')
                    ->setCellValue('P4', 'ชื่อผู้บันทึกนัดรังวัด')
                    ->setCellValue('Q4', 'หมายเหตุ')
                    ->getStyle('A1:Q4')
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else {
                $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
                    ->setCellValue('G4', 'วันที่นัดรังวัด')
                    ->setCellValue('H4', 'เวลานัดรังวัด')
                    ->setCellValue('I4', 'จำนวนวันที่รังวัด')
                    ->setCellValue('J4', 'ชื่อช่างรังวัด')
                    ->setCellValue('K4', 'หมายเลขโทรศัพท์มือถือ')
                    ->setCellValue('L4', 'รายละเอียดการนัด')
                    ->setCellValue('M4', 'ชื่อผู้บันทึกนัดรังวัด')
                    ->setCellValue('N4', 'หมายเหตุ')
                    ->getStyle('A1:N4')
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        }


        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];

        if ($jobgroup_seq == 3) {

            $sheet = $spreadsheet->setActiveSheetIndex(5);
            $sheet->getColumnDimension('R')->setWidth(50);
            $sheet->mergeCells('A1:R1');
            $sheet->mergeCells('A2:R2');
            $sheet->mergeCells('A3:R3');


            if ($sts == 'e') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
                $fileName =  $branchName . '-SVA ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            } else if ($sts == 's') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
                $fileName =  $branchName . '-SVA ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            }


            $sheet->setCellValue('A4', '')
                ->setCellValue('B4', 'เลขที่คำขอรังวัด')
                ->setCellValue('C4', 'วันที่รับคำขอ')
                ->setCellValue('D4', 'ประเภทการรังวัด')
                ->setCellValue('E4', 'สถานะเรื่องรังวัด')
                ->getStyle('A1:E4')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
                ->setCellValue('G4', 'ประเภทเอกสารสิทธิ')
                ->setCellValue('H4', 'วิธีการรังวัด')
                ->setCellValue('I4', 'เลขที่คำขอรังวัด (ร.ว.12)')
                ->setCellValue('J4', 'วันที่รับคำขอ')
                ->setCellValue('K4', 'ประเภทการรังวัด')
                ->setCellValue('L4', 'ชื่อโครงการ')
                ->setCellValue('M4', 'เลขที่หนังสือ')
                ->setCellValue('N4', 'ลงวันที่')
                ->setCellValue('O4', 'หน่วยงาน')
                ->setCellValue('P4', 'ประเภทโครงการ')
                ->setCellValue('Q4', 'ประเภทการรังวัดหลัก')
                ->setCellValue('R4', 'หมายเหตุ')
                ->getStyle('A1:R4')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


            $sheet->getStyle('A1:R' . $projectCellIndex)->applyFromArray($styleArray);
            $sheet->getStyle('A1:R4')->getFont()->setBold(true);
            $sheet->getStyle('A5:E' . $projectCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
            $sheet->getStyle('R5:R' . $projectCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


            $sheet->getStyle('A4:R' . $projectCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $sheet->getStyle('R4:R' . $projectCellIndex)
                ->getAlignment()
                ->setWrapText(true);
        }


        for ($j = 0; $j < 5; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);

            if ($j == 0) {
                $sheet->getStyle('A1:W' . $surveyCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:W4')->getFont()->setBold(true);
                $sheet->getStyle('A5:E' . $surveyCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('W5:W' . $surveyCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


                $sheet->getStyle('A4:W' . $surveyCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $sheet->getStyle('W4:W' . $surveyCellIndex)
                    ->getAlignment()
                    ->setWrapText(true);
            } else if ($j == 1) {

                $sheet->getStyle('A1:M' . $ownerCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:M4')->getFont()->setBold(true);
                $sheet->getStyle('A5:E' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('M5:M' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


                $sheet->getStyle('A4:M' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('M4:M' . $ownerCellIndex)
                    ->getAlignment()
                    ->setWrapText(true);
            } else if ($j == 2) {

                $sheet->getStyle('A1:I' . $surveyStsCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:I4')->getFont()->setBold(true);
                $sheet->getStyle('A5:E' . $surveyStsCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('I5:I' . $surveyCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $sheet->getStyle('A4:I' . $surveyStsCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $sheet->getStyle('I4:I' . $surveyStsCellIndex)
                    ->getAlignment()
                    ->setWrapText(true);
            } else if ($j == 3) {
                $sheet->getStyle('A1:O' . $expenseCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:O4')->getFont()->setBold(true);
                $sheet->getStyle('A5:E' . $expenseCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('O5:O' . $expenseCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $sheet->getStyle('A4:O' . $expenseCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $sheet->getStyle('O4:O' . $expenseCellIndex)
                    ->getAlignment()
                    ->setWrapText(true);
            } else {
                if ($private) {
                    $sheet->getStyle('A1:Q' . $appointCellIndex)->applyFromArray($styleArray);
                    $sheet->getStyle('A1:Q4')->getFont()->setBold(true);
                    $sheet->getStyle('A5:E' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                    $sheet->getStyle('Q5:Q' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->getStyle('A4:Q' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                    $sheet->getStyle('Q4:Q' . $appointCellIndex)
                        ->getAlignment()
                        ->setWrapText(true);
                } else {
                    $sheet->getStyle('A1:N' . $appointCellIndex)->applyFromArray($styleArray);
                    $sheet->getStyle('A1:N4')->getFont()->setBold(true);
                    $sheet->getStyle('A5:E' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                    $sheet->getStyle('N5:N' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->getStyle('A4:N' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                    $sheet->getStyle('N4:N' . $appointCellIndex)
                        ->getAlignment()
                        ->setWrapText(true);
                }
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //  $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        // if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
        //      $fileName = urlencode($fileName);
        // }
        // $fileName = urlencode($fileName);
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save('php://output');


        break;
    case "calculate":

        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $surveyCellIndex = 0;
        $ownerCellIndex = 0;
        $surveyStsCellIndex = 0;
        $expenseCellIndex = 0;
        $appointCellIndex = 0;
        $projectCellIndex = 0;

        $errorParcel = 0;
        $errorOwner = 0;
        $errorSurveySts = 0;
        $errorExpense = 0;
        $errorAppoint = 0;
        $errorProject = 0;



        $current = 0;
        //===================PARCEL DETAIL=====================================
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();

            $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
            $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];

            include './sva/querySvaDetailData.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultDetail[] = $row;
            }
            $haserror = true;
            if ($sts == 'e') {
                for ($i = 0; $i < count($ResultDetail); $i++) {
                    if (!$surveyjobSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$surveyjobSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                    } else {
                        if ($ResultDetail[$i]['PCM_ORDERNO_P1'] != $ResultDetail[$i]['PCM_ORDERNO_P2']) $problemDesc .= "ลำดับหมุดหลักเขตในแปลงที่ดินไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PCM_BNDNAME_P1'] != $ResultDetail[$i]['PCM_BNDNAME_P2']) $problemDesc .= "ชื่อหมุดหลักเขตไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PAR_AREA_COOR_P1'] != $ResultDetail[$i]['PAR_AREA_COOR_P2']) $problemDesc .= "เนื้อที่พิกัดฉากไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PAR_PARCEL_NO_P1'] != $ResultDetail[$i]['PAR_PARCEL_NO_P2']) $problemDesc .= "เลขที่เอกสารสิทธิไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PAR_LAND_NO_P1'] != $ResultDetail[$i]['PAR_LAND_NO_P2']) $problemDesc .= "เลขที่ดินไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PAR_SURVEY_NO_P1'] != $ResultDetail[$i]['PAR_SURVEY_NO_P2']) $problemDesc .= "หน้าสำรวจไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PAR_TYPE_P1'] != $ResultDetail[$i]['PAR_TYPE_P2']) $problemDesc .= "ประเภทรูปแปลงที่ดินไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PAR_SUMPAIR_N_P1'] != $ResultDetail[$i]['PAR_SUMPAIR_N_P2']) $problemDesc .= "ผลรวมของผลบวก คูณ พิกัดเหนือไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PAR_SUMPAIR_S_P1'] != $ResultDetail[$i]['PAR_SUMPAIR_S_P2']) $problemDesc .= "ผลรวมของผลบวก คูณ พิกัดใต้ไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PROVINCE_NAME_P1'] != $ResultDetail[$i]['PROVINCE_NAME_P2']) $problemDesc .= "จังหวัดไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['AMPHUR_NAME_P1'] != $ResultDetail[$i]['AMPHUR_NAME_P2']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['TAMBOL_NAME_P1'] != $ResultDetail[$i]['TAMBOL_NAME_P2']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['OLD_AREA_P1'] != $ResultDetail[$i]['OLD_AREA_P2']) $problemDesc .= "เนื้อที่เดิมไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['CAL_AREA_P1'] != $ResultDetail[$i]['CAL_AREA_P2']) $problemDesc .= "เนื้อที่คำนวณไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['USE_AREA_P1'] != $ResultDetail[$i]['USE_AREA_P2']) $problemDesc .= "เนื้อที่ใช้ไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['REAL_AREA_P1'] != $ResultDetail[$i]['REAL_AREA_P2']) $problemDesc .= "เนื้อที่จริงไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['PAR_UTM_P1'] != $ResultDetail[$i]['PAR_UTM_P2']) $problemDesc .= "ระวาง UTMไม่ตรงกัน\n";
                    }
                    if ($problemDesc == '') {
                        $haserror = false;
                        continue;
                    }
                    $sheet->setCellValue('X' . (5 + $current + $i), rtrim($problemDesc));
                    $problemDesc = '';
                }
                if (!$haserror) continue;
                else $errorParcel++;
            }


            if (count($ResultDetail) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + (count($ResultDetail)) - 1));
                $sheet->mergeCells('F' . (5 + $current + (count($ResultDetail))) . ':F' . (5 + $current + (count($ResultDetail) * 2) - 1));


                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }

                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (5 + $current + count($ResultDetail)), 'พัฒน์ฯ 2');

                for ($i = 0; $i < count($ResultDetail); $i++) {
                    if (empty($ResultDetail[$i]['PCM_ORDERNO_P1'])) {
                        $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + $i), $ResultDetail[$i]['PCM_ORDERNO_P1']);
                    }
                    if (empty($ResultDetail[$i]['PCM_BNDNAME_P1'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultDetail[$i]['PCM_BNDNAME_P1']);
                    }
                    if (empty($ResultDetail[$i]['PAR_AREA_COOR_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultDetail[$i]['PAR_AREA_COOR_P1']);
                    }
                    if (empty($ResultDetail[$i]['PAR_PARCEL_NO_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultDetail[$i]['PAR_PARCEL_NO_P1']);
                    }
                    if (empty($ResultDetail[$i]['PAR_LAND_NO_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultDetail[$i]['PAR_LAND_NO_P1']);
                    }
                    if (empty($ResultDetail[$i]['PAR_SURVEY_NO_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultDetail[$i]['PAR_SURVEY_NO_P1']);
                    }
                    if (empty($ResultDetail[$i]['PAR_TYPE_P1'])) {
                        $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + $i), $ResultDetail[$i]['PAR_TYPE_P1']);
                    }
                    if (empty($ResultDetail[$i]['PAR_SUMPAIR_N_P1'])) {
                        $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + $i), $ResultDetail[$i]['PAR_SUMPAIR_N_P1']);
                    }
                    if (empty($ResultDetail[$i]['PAR_SUMPAIR_S_P1'])) {
                        $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + $i), $ResultDetail[$i]['PAR_SUMPAIR_S_P1']);
                    }
                    if (empty($ResultDetail[$i]['PROVINCE_NAME_P1'])) {
                        $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + $i), $ResultDetail[$i]['PROVINCE_NAME_P1']);
                    }
                    if (empty($ResultDetail[$i]['AMPHUR_NAME_P1'])) {
                        $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + $i), $ResultDetail[$i]['AMPHUR_NAME_P1']);
                    }
                    if (empty($ResultDetail[$i]['TAMBOL_NAME_P1'])) {
                        $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + $i), $ResultDetail[$i]['TAMBOL_NAME_P1']);
                    }
                    if (empty($ResultDetail[$i]['OLD_AREA_P1'])) {
                        $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + $i), $ResultDetail[$i]['OLD_AREA_P1']);
                    }
                    if (empty($ResultDetail[$i]['CAL_AREA_P1'])) {
                        $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + $i), $ResultDetail[$i]['CAL_AREA_P1']);
                    }
                    if (empty($ResultDetail[$i]['USE_AREA_P1'])) {
                        $sheet->setCellValue('U' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + $i), $ResultDetail[$i]['USE_AREA_P1']);
                    }
                    if (empty($ResultDetail[$i]['REAL_AREA_P1'])) {
                        $sheet->setCellValue('V' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('V' . (5 + $current + $i), $ResultDetail[$i]['REAL_AREA_P1']);
                    }
                    if (empty($ResultDetail[$i]['PAR_UTM_P1'])) {
                        $sheet->setCellValue('W' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('W' . (5 + $current + $i), $ResultDetail[$i]['PAR_UTM_P1']);
                    }

                    if (empty($ResultDetail[$i]['PCM_ORDERNO_P2'])) {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PCM_ORDERNO_P2']);
                    }
                    if (empty($ResultDetail[$i]['PCM_BNDNAME_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PCM_BNDNAME_P2']);
                    }
                    if (empty($ResultDetail[$i]['PAR_AREA_COOR_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PAR_AREA_COOR_P2']);
                    }
                    if (empty($ResultDetail[$i]['PAR_PARCEL_NO_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PAR_PARCEL_NO_P2']);
                    }
                    if (empty($ResultDetail[$i]['PAR_LAND_NO_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PAR_LAND_NO_P2']);
                    }
                    if (empty($ResultDetail[$i]['PAR_SURVEY_NO_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PAR_SURVEY_NO_P2']);
                    }
                    if (empty($ResultDetail[$i]['PAR_TYPE_P2'])) {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PAR_TYPE_P2']);
                    }
                    if (empty($ResultDetail[$i]['PAR_SUMPAIR_N_P2'])) {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PAR_SUMPAIR_N_P2']);
                    }
                    if (empty($ResultDetail[$i]['PAR_SUMPAIR_S_P2'])) {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PAR_SUMPAIR_S_P2']);
                    }
                    if (empty($ResultDetail[$i]['PROVINCE_NAME_P2'])) {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROVINCE_NAME_P2']);
                    }
                    if (empty($ResultDetail[$i]['AMPHUR_NAME_P2'])) {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['AMPHUR_NAME_P2']);
                    }
                    if (empty($ResultDetail[$i]['TAMBOL_NAME_P2'])) {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['TAMBOL_NAME_P2']);
                    }
                    if (empty($ResultDetail[$i]['OLD_AREA_P2'])) {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['OLD_AREA_P2']);
                    }
                    if (empty($ResultDetail[$i]['CAL_AREA_P2'])) {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['CAL_AREA_P2']);
                    }
                    if (empty($ResultDetail[$i]['USE_AREA_P2'])) {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['USE_AREA_P2']);
                    }
                    if (empty($ResultDetail[$i]['REAL_AREA_P2'])) {
                        $sheet->setCellValue('V' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('V' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['REAL_AREA_P2']);
                    }
                    if (empty($ResultDetail[$i]['PAR_UTM_P2'])) {
                        $sheet->setCellValue('W' . (5 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('W' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PAR_UTM_P2']);
                    }




                    $surveyCellIndex = (5 + $current + count($ResultDetail) + $i);
                }

                $current += count($ResultDetail) * 2;
            }
        }



        // =========== SVC SURVEY ====================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        for ($j = 0; $j < $last; $j++) {

            $ResultSurvey = array();

            $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
            $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];

            include './sva/querySvcSurveyData.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultSurvey[] = $row;
            }
            $haserror = true;
            if ($sts == 'e') {
                for ($i = 0; $i < count($ResultSurvey); $i++) {
                    $problemDesc = '';
                    if (!$surveyjobSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$surveyjobSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                    } else {
                        if ($ResultSurvey[$i]['OWNER_NAME_P1'] != $ResultSurvey[$i]['OWNER_NAME_P2']) $problemDesc .= "ชื่อผู้ขอรังวัดไม่ตรงกัน\n";
                        if ($ResultSurvey[$i]['SURVEY_STARTDATE_P1'] != $ResultSurvey[$i]['SURVEY_STARTDATE_P2']) $problemDesc .= "วันที่ทำการรังวัดไม่ตรงกัน\n";
                        if ($ResultSurvey[$i]['SURVEY_ENDDATE_P1'] != $ResultSurvey[$i]['SURVEY_ENDDATE_P2']) $problemDesc .= "วันที่สิ้นสุดรังวัดม่ตรงกัน\n";
                        if ($ResultSurvey[$i]['SURVEY_BND_TYPE_P1'] != $ResultSurvey[$i]['SURVEY_BND_TYPE_P2']) $problemDesc .= "ชนิดหลักเขตที่ใช้รังวัดไม่ตรงกัน\n";
                        if ($ResultSurvey[$i]['SURVEY_BND_NO_P1'] != $ResultSurvey[$i]['SURVEY_BND_NO_P2'])  $problemDesc .= "จำนวนหลักเขตที่ใช้ปักไม่ตรงกัน\n";
                        if ($ResultSurvey[$i]['APPROVE_NAME_P1'] != $ResultSurvey[$i]['APPROVE_NAME_P2'])  $problemDesc .= "ชื่อผู้ตรวจไม่ตรงกัน\n";
                        if ($ResultSurvey[$i]['APPROVE_ANGDIST_P1'] != $ResultSurvey[$i]['APPROVE_ANGDIST_P2'])  $problemDesc .= "ผู้ตรวจมุม, ระยะไม่ตรงกัน\n";
                    }
                    if ($problemDesc == '') {
                        $haserror = false;
                        continue;
                    }
                    $sheet->setCellValue('N' . (5 + $current + $i), rtrim($problemDesc));
                    $problemDesc = '';
                }
                if (!$haserror) continue;
                $errorParcel++;
            }



            if (count($ResultSurvey) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultSurvey) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultSurvey) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultSurvey) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultSurvey) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultSurvey) * 2) - 1));
                // $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultSurvey) * 2) - 1));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + (count($ResultSurvey)) - 1));
                $sheet->mergeCells('F' . (5 + $current + (count($ResultSurvey))) . ':F' . (5 + $current + (count($ResultSurvey) * 2) - 1));


                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (5 + $current + count($ResultSurvey)), 'พัฒน์ฯ 2');

                for ($i = 0; $i < count($ResultSurvey); $i++) {
                    if (empty($ResultSurvey[$i]['OWNER_NAME_P1'])) {
                        $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + $i), $ResultSurvey[$i]['OWNER_NAME_P1']);
                    }
                    if (empty($ResultSurvey[$i]['SURVEY_STARTDATE_P1'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultSurvey[$i]['SURVEY_STARTDATE_P1']);
                    }
                    if (empty($ResultSurvey[$i]['SURVEY_ENDDATE_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultSurvey[$i]['SURVEY_ENDDATE_P1']);
                    }
                    if (empty($ResultSurvey[$i]['SURVEY_BND_TYPE_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultSurvey[$i]['SURVEY_BND_TYPE_P1']);
                    }
                    if (empty($ResultSurvey[$i]['SURVEY_BND_NO_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultSurvey[$i]['SURVEY_BND_NO_P1']);
                    }
                    if (empty($ResultSurvey[$i]['APPROVE_NAME_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultSurvey[$i]['APPROVE_NAME_P1']);
                    }
                    if (empty($ResultSurvey[$i]['APPROVE_ANGDIST_P1'])) {
                        $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + $i), $ResultSurvey[$i]['APPROVE_ANGDIST_P1']);
                    }




                    if (empty($ResultSurvey[$i]['OWNER_NAME_P2'])) {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultSurvey) + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultSurvey) + $i), $ResultSurvey[$i]['OWNER_NAME_P2']);
                    }
                    if (empty($ResultSurvey[$i]['SURVEY_STARTDATE_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultSurvey) + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultSurvey) + $i), $ResultSurvey[$i]['SURVEY_STARTDATE_P2']);
                    }
                    if (empty($ResultSurvey[$i]['SURVEY_ENDDATE_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultSurvey) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultSurvey) + $i), $ResultSurvey[$i]['SURVEY_ENDDATE_P2']);
                    }
                    if (empty($ResultSurvey[$i]['SURVEY_BND_TYPE_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultSurvey) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultSurvey) + $i), $ResultSurvey[$i]['SURVEY_BND_TYPE_P2']);
                    }
                    if (empty($ResultSurvey[$i]['SURVEY_BND_NO_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultSurvey) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultSurvey) + $i), $ResultSurvey[$i]['SURVEY_BND_NO_P2']);
                    }
                    if (empty($ResultSurvey[$i]['APPROVE_NAME_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultSurvey) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultSurvey) + $i), $ResultSurvey[$i]['APPROVE_NAME_P2']);
                    }
                    if (empty($ResultSurvey[$i]['APPROVE_ANGDIST_P2'])) {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultSurvey) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultSurvey) + $i), $ResultSurvey[$i]['APPROVE_ANGDIST_P2']);
                    }


                    $surveyCellIndex = (5 + $current + count($ResultSurvey) + $i);
                }

                $current += count($ResultSurvey) * 2;
            }
        }


        // =========== JOBSTATUS ====================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        for ($j = 0; $j < $last; $j++) {

            $ResultStatus = array();

            $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
            $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];

            include './sva/querySvaJobStatusData.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultStatus[] = $row;
            }
            $haserror = true;


            if ($sts == 'e') {
                for ($i = 0; $i < count($ResultStatus); $i++) {
                    $problemDesc = '';
                    if (!$surveyjobSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$surveyjobSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                    } else {
                        if ($ResultStatus[$i]['MOVEMENT_DTM_P1'] != $ResultStatus[$i]['MOVEMENT_DTM_P2']) $problemDesc .= "วันที่บันทึกสถานะไม่ตรงกัน\n";
                    }
                    if ($problemDesc == '') {
                        $haserror = false;
                        continue;
                    }

                    $sheet->setCellValue('I' . (5 + $current + $i), rtrim($problemDesc));
                    $problemDesc = '';
                }

                if (!$haserror) continue;
                else $errorSurveySts++;
            }


            if (count($ResultStatus) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultStatus)) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultStatus)) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultStatus)) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultStatus)) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultStatus)) - 1));
                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }


                for ($i = 0; $i < count($ResultStatus); $i++) {
                    if (empty($ResultStatus[$i]['JOBSTATUS_NAME'])) {
                        $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current + $i), $ResultStatus[$i]['JOBSTATUS_NAME']);
                    }
                    if (empty($ResultStatus[$i]['MOVEMENT_DTM_P1'])) {
                        $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + $i), $ResultStatus[$i]['MOVEMENT_DTM_P1']);
                    }
                    if (empty($ResultStatus[$i]['MOVEMENT_DTM_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultStatus[$i]['MOVEMENT_DTM_P2']);
                    }

                    $surveyStsCellIndex = 5 + $current + $i;
                }

                $current += count($ResultStatus);
            }
        }


        // =========== expense ====================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        for ($j = 0; $j < $last; $j++) {

            $ResultExpense = array();

            $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
            $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];

            include './sva/querySvaExpenseData.php';
            $table = 'withdraw';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultExpense[] = $row;
            }

            $haserror = true;
            if ($sts == 'e') {
                if (!$surveyjobSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$surveyjobSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                } else {
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['BTD59_NO_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['BTD59_NO_P2']) $problemDesc .= "เลขที่ บ.ท.ด.59 ไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_DATE_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_DATE_P2']) $problemDesc .= "วันที่ทำการถอนจ่ายไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_AMOUNT_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_AMOUNT_P2']) $problemDesc .= "จำนวนเงินที่ถอนจ่ายไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_ADJUST_TYPE_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_ADJUST_TYPE_P2']) $problemDesc .= "จำนวนเงินที่เรียกเพิ่ม/คืนเงินไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAWSTATUS_NAME_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAWSTATUS_NAME_P2']) $problemDesc .= "สถานะไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_EXPENSE_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_EXPENSE_P2'])  $problemDesc .= "จำนวนเงินรวมไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_AMOUNT_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_AMOUNT_P2'])  $problemDesc .= "จำนวนเงินที่เรียกเพิ่มจากผู้ขอไม่ตรงกัน\n";
                    if ($ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_ADJUST_AMOUNT_P1'] != $ResultExpense[$ResultExpense[count($ResultExpense) - 1]]['WITHDRAW_ADJUST_AMOUNT_P2'])  $problemDesc .= "จำนวนเงินที่คืนผู้ขอไม่ตรงกัน\n";
                }
                if ($problemDesc == '') {
                    $haserror = false;
                    continue;
                }
                $sheet->setCellValue('O' . (5 + $current), rtrim($problemDesc));
                $problemDesc = '';
            }
            if (!$haserror) continue;
            else $errorExpense++;



            if (count($ResultExpense) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));
                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }

                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (6 + $current), 'พัฒน์ฯ 2');


                if (empty($ResultExpense[count($ResultExpense) - 1]['BTD59_NO_P1'])) {
                    $sheet->setCellValue('G' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['BTD59_NO_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_DATE_P1'])) {
                    $sheet->setCellValue('H' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_DATE_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P1'])) {
                    $sheet->setCellValue('I' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_TYPE_P1'])) {
                    $sheet->setCellValue('J' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_TYPE_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAWSTATUS_NAME_P1'])) {
                    $sheet->setCellValue('K' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAWSTATUS_NAME_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_EXPENSE_P1'])) {
                    $sheet->setCellValue('L' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_EXPENSE_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P1'])) {
                    $sheet->setCellValue('M' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P1']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_AMOUNT_P1'])) {
                    $sheet->setCellValue('N' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_AMOUNT_P1']);
                }


                if (empty($ResultExpense[count($ResultExpense) - 1]['BTD59_NO_P2'])) {
                    $sheet->setCellValue('G' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('G' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['BTD69_NO_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_DATE_P2'])) {
                    $sheet->setCellValue('H' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('H' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_DATE_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P2'])) {
                    $sheet->setCellValue('I' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('I' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_TYPE_P2'])) {
                    $sheet->setCellValue('J' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('J' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_TYPE_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAWSTATUS_NAME_P2'])) {
                    $sheet->setCellValue('K' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('K' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAWSTATUS_NAME_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_EXPENSE_P2'])) {
                    $sheet->setCellValue('L' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_EXPENSE_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P2'])) {
                    $sheet->setCellValue('M' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_AMOUNT_P2']);
                }
                if (empty($ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_AMOUNT_P2'])) {
                    $sheet->setCellValue('N' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (6 + $current), $ResultExpense[count($ResultExpense) - 1]['WITHDRAW_ADJUST_AMOUNT_P2']);
                }
                $expenseCellIndex =  (6 + $current);



                $current += 2;
            }
        }


        // =========== APPOINTMENT ====================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        for ($j = 0; $j < $last; $j++) {

            $ResultAppointment = array();

            $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
            $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];

            include './sva/querySvaAppointmentData.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultAppointment[] = $row;
            }
            $haserror = true;

            if ($sts == 'e') {
                if ($private) {
                    for ($i = 0; $i < count($ResultAppointment); $i++) {
                        $problemDesc = '';
                        if (!$surveyjobSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                        } else if (!$surveyjobSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                        } else {
                            if ($ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P1'] != $ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P2']) $problemDesc .= "ชื่อสำนักงานรังวัดเอกชนไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['PV_SURVEYOR_NAME_P1'] != $ResultAppointment[$i]['PV_SURVEYOR_NAME_P2']) $problemDesc .= "ชื่อช่างรังวัดเอกชนไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P1'] != $ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P2']) $problemDesc .= "หมายเลขโทรศัพท์มือถือช่างรังวัดเอกชนไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['APPOINTMENT_DTM_P1'] != $ResultAppointment[$i]['APPOINTMENT_DTM_P2']) $problemDesc .= "วันที่นัดรังวัดไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['APPOINTMENT_TIME_P1'] != $ResultAppointment[$i]['APPOINTMENT_TIME_P2']) $problemDesc .= "เวลานัดรังวัดไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1'] != $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2']) $problemDesc .= "จำนวนวันที่รังวัดไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['SURVEYOR_NAME_P1'] != $ResultAppointment[$i]['SURVEYOR_NAME_P2']) $problemDesc .= "ชื่อนายช่างรังวัดสำนักงานที่ดินรับผิดชอบไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['SURVEYOR_MOBILE_P1'] != $ResultAppointment[$i]['SURVEYOR_MOBILE_P2']) $problemDesc .= "หมายเลขโทรศัพท์มือถือนายช่างรังวัดสำนักงานที่ดินรับผิดชอบไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['APPOINTMENT_REM_P1'] != $ResultAppointment[$i]['APPOINTMENT_REM_P2'])  $problemDesc .= "หมายเหตุการรังวัดไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['APPOINTMENT_WRITER_P2'] != $ResultAppointment[$i]['APPOINTMENT_WRITER_P2'])  $problemDesc .= "ชื่อผู้บันทึกนัดรังวัดไม่ตรงกัน\n";
                        }
                        if ($problemDesc == '') {
                            $haserror = false;
                            continue;
                        }
                        $sheet->setCellValue('Q' . (5 + $current + $i), rtrim($problemDesc));
                        $problemDesc = '';
                    }
                } else {
                    for ($i = 0; $i < count($ResultAppointment); $i++) {
                        $problemDesc = '';
                        if (!$surveyjobSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                        } else if (!$surveyjobSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                        } else {
                            if ($ResultAppointment[$i]['APPOINTMENT_DTM_P1'] != $ResultAppointment[$i]['APPOINTMENT_DTM_P2']) $problemDesc .= "วันที่นัดรังวัดไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['APPOINTMENT_TIME_P1'] != $ResultAppointment[$i]['APPOINTMENT_TIME_P2']) $problemDesc .= "เวลานัดรังวัดไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1'] != $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2']) $problemDesc .= "จำนวนวันที่รังวัดไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['SURVEYOR_NAME_P1'] != $ResultAppointment[$i]['SURVEYOR_NAME_P2']) $problemDesc .= "ชื่อช่างรังวัดไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['SURVEYOR_MOBILE_P1'] != $ResultAppointment[$i]['SURVEYOR_MOBILE_P2']) $problemDesc .= "หมายเลขโทรศัพท์มือถือช่างรังวัดไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['APPOINTMENT_DETAIL_P1'] != $ResultAppointment[$i]['APPOINTMENT_DETAIL_P2'])  $problemDesc .= "รายละเอียดการนัดไม่ตรงกัน\n";
                            if ($ResultAppointment[$i]['APPOINTMENT_WRITER_P2'] != $ResultAppointment[$i]['APPOINTMENT_WRITER_P2'])  $problemDesc .= "ชื่อผู้บันทึกนัดรังวัดไม่ตรงกัน\n";
                        }
                        if ($problemDesc == '') {
                            $haserror = false;
                            continue;
                        }
                        $sheet->setCellValue('X' . (5 + $current + $i), rtrim($problemDesc));
                        $problemDesc = '';
                    }
                }
            }
            if (!$haserror) continue;
            else $errorAppoint++;







            if (count($ResultAppointment) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultAppointment) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultAppointment) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultAppointment) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultAppointment) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultAppointment) * 2) - 1));
                // $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + (count($ResultAppointment)) - 1));
                $sheet->mergeCells('F' . (5 + $current + (count($ResultAppointment))) . ':F' . (5 + $current + (count($ResultAppointment) * 2) - 1));

                if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                    }
                } else {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                    }
                    if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                    }
                    if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                    }
                    if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                    }
                }

                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (5 + $current + count($ResultAppointment)), 'พัฒน์ฯ 2');

                for ($i = 0; $i < count($ResultAppointment); $i++) {
                    if ($private) {
                        if (empty($ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P1'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['PV_SURVEYOR_NAME_P1'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultAppointment[$i]['PV_SURVEYOR_NAME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P1'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DTM_P1'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_DTM_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_TIME_P1'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_TIME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1'])) {
                            $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_NAME_P1'])) {
                            $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + $i), $ResultAppointment[$i]['SURVEYOR_NAME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_MOBILE_P1'])) {
                            $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('N' .  (5 + $current + $i), $ResultAppointment[$i]['SURVEYOR_MOBILE_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_REM_P1'])) {
                            $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('O' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_REM_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_WRITER_P1'])) {
                            $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('P' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_WRITER_P1']);
                        }


                        if (empty($ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P2'])) {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['PRIVATE_OFFICE_NAME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['PV_SURVEYOR_NAME_P2'])) {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['PV_SURVEYOR_NAME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P2'])) {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['PRIVATE_SVY_MOBILE_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DTM_P2'])) {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['APPOINTMENT_DTM_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_TIME_P2'])) {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['APPOINTMENT_TIME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2'])) {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_NAME_P2'])) {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultAppointment)), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultAppointment)), $ResultAppointment[$i]['SURVEYOR_NAME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_MOBILE_P2'])) {
                            $sheet->setCellValue('N' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('N' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['SURVEYOR_MOBILE_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_REM_P2'])) {
                            $sheet->setCellValue('O' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('O' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_REM_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_WRITER_P2'])) {
                            $sheet->setCellValue('P' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('P' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_WRITER_P2']);
                        }


                        $appointCellIndex = (5 + $current + count($ResultAppointment) + $i);
                    } else {
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DTM_P1'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_DTM_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_TIME_P1'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_TIME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P1']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_NAME_P1'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultAppointment[$i]['SURVEYOR_NAME_P1']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_MOBILE_P1'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultAppointment[$i]['SURVEYOR_MOBILE_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DETAIL_P1'])) {
                            $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_DETAIL_P1']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_WRITER_P1'])) {
                            $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + $i), $ResultAppointment[$i]['APPOINTMENT_WRITER_P1']);
                        }



                        if (empty($ResultAppointment[$i]['APPOINTMENT_DTM_P2'])) {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_DTM_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_TIME_P2'])) {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_TIME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2'])) {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_DAY_QTY_P2']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_NAME_P2'])) {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['SURVEYOR_NAME_P2']);
                        }
                        if (empty($ResultAppointment[$i]['SURVEYOR_MOBILE_P2'])) {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['SURVEYOR_MOBILE_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_DETAIL_P2'])) {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_DETAIL_P2']);
                        }
                        if (empty($ResultAppointment[$i]['APPOINTMENT_WRITER_P2'])) {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultAppointment) + $i), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultAppointment) + $i), $ResultAppointment[$i]['APPOINTMENT_WRITER_P2']);
                        }

                        $appointCellIndex = (5 + $current + count($ResultAppointment) + $i);
                    }
                }

                $current += count($ResultAppointment) * 2;
            }
        }

        //===================project DETAIL=====================================
        if ($jobgroup_seq == 3) {
            $current = 0;

            $sheet = $spreadsheet->setActiveSheetIndex(5);
            for ($j = 0; $j < $last; $j++) {

                $ResultDetail = array();

                $surveyjobSeqP1 = $Result[$j]['SURVEYJOB_SEQ_P1'];
                $surveyjobSeqP2 = $Result[$j]['SURVEYJOB_SEQ_P2'];
                $check = 'project';

                include './sva/querySvaDetailData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultDetail[] = $row;
                }
                $haserror = true;
                if ($sts == 'e') {
                    for ($i = 0; $i < count($ResultDetail); $i++) {
                        if (!$surveyjobSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                        } else if (!$surveyjobSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                        } else {
                            if ($ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P1'] != $ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P2']) $problemDesc .= "ประเภทเอกสารสิทธิไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['METHODOFSURVEY_NAME_P1'] != $ResultDetail[$i]['METHODOFSURVEY_NAME_P2']) $problemDesc .= "วิธีการรังวัดไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['SURVEYJOB_NO_P1'] != $ResultDetail[$i]['SURVEYJOB_NO_P2']) $problemDesc .= "เลขที่คำขอรังวัด (ร.ว.12) ไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['SURVEYJOB_DATE_P1'] != $ResultDetail[$i]['SURVEYJOB_DATE_P2']) $problemDesc .= "วันที่รับคำขอไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['TYPEOFSURVEY_NAME_P1'] != $ResultDetail[$i]['TYPEOFSURVEY_NAME_P2']) $problemDesc .= "ประเภทการรังวัดไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['PROJECT_NAME_P1'] != $ResultDetail[$i]['PROJECT_NAME_P2']) $problemDesc .= "ชื่อโครงการไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['PROJECT_DOCNO_P1'] != $ResultDetail[$i]['PROJECT_DOCNO_P2']) $problemDesc .= "เลขที่หนังสือไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['PROJECT_DOCDATE_P1'] != $ResultDetail[$i]['PROJECT_DOCDATE_P2']) $problemDesc .= "ลงวันที่ไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['PROJECT_OFFICEOWNER_P1'] != $ResultDetail[$i]['PROJECT_OFFICEOWNER_P2']) $problemDesc .= "ชื่อหน่วยงานไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['PROJECTTYPE_NAME_P1'] != $ResultDetail[$i]['PROJECTTYPE_NAME_P2']) $problemDesc .= "ประเภทโครงการไม่ตรงกัน\n";
                            if ($ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P1'] != $ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P2']) $problemDesc .= "ประเภทรังวัดหลักไม่ตรงกัน\n";
                        }
                        if ($problemDesc == '') {
                            $haserror = false;
                            continue;
                        }
                        $sheet->setCellValue('P' . (5 + $current + $i), rtrim($problemDesc));
                        $problemDesc = '';
                    }
                }

                if (!$haserror) continue;
                else $errorProject++;


                if (count($ResultDetail) <= 0) {
                    $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                    $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                    $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                    $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                    $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                    if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                        $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                        if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                            $sheet->setCellValue('B' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                        }
                        if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                            $sheet->setCellValue('C' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                        }
                        if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                            $sheet->setCellValue('D' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                        }
                        if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                            $sheet->setCellValue('E' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                        }
                    } else {

                        $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                        if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                            $sheet->setCellValue('B' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                        }
                        if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                            $sheet->setCellValue('C' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                        }
                        if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                            $sheet->setCellValue('D' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                        }
                        if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                            $sheet->setCellValue('E' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                        }
                    }
                    $current += 2;
                } else {
                    $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultDetail) * 2) - 1));
                    $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + (count($ResultDetail)) - 1));
                    $sheet->mergeCells('F' . (5 + $current + (count($ResultDetail))) . ':F' . (5 + $current + (count($ResultDetail) * 2) - 1));

                    if ($Result[$j]['SURVEYJOB_SEQ_P1']) {

                        $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                        if (empty($Result[$j]['SURVEYJOB_NO_P1'])) {
                            $sheet->setCellValue('B' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P1']);
                        }
                        if (empty($Result[$j]['SURVEYJOB_DATE_P1'])) {
                            $sheet->setCellValue('C' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P1']);
                        }
                        if (empty($Result[$j]['TYPEOFSURVEY_NAME_P1'])) {
                            $sheet->setCellValue('D' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P1']);
                        }
                        if (empty($Result[$j]['JOBSTATUS_NAME_P1'])) {
                            $sheet->setCellValue('E' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P1']);
                        }
                    } else {

                        $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                        if (empty($Result[$j]['SURVEYJOB_NO_P2'])) {
                            $sheet->setCellValue('B' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('B' . (5 + $current), $Result[$j]['SURVEYJOB_NO_P2']);
                        }
                        if (empty($Result[$j]['SURVEYJOB_DATE_P2'])) {
                            $sheet->setCellValue('C' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('C' . (5 + $current), $Result[$j]['SURVEYJOB_DATE_P2']);
                        }
                        if (empty($Result[$j]['TYPEOFSURVEY_NAME_P2'])) {
                            $sheet->setCellValue('D' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TYPEOFSURVEY_NAME_P2']);
                        }
                        if (empty($Result[$j]['JOBSTATUS_NAME_P2'])) {
                            $sheet->setCellValue('E' . (5 + $current), NULL);
                        } else {
                            $sheet->setCellValue('E' . (5 + $current), $Result[$j]['JOBSTATUS_NAME_P2']);
                        }
                    }
                    $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                    $sheet->setCellValue('F' . (5 + $current + count($ResultDetail)), 'พัฒน์ฯ 2');

                    for ($i = 0; $i < count($ResultDetail); $i++) {
                        if (empty($ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P1'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P1']);
                        }
                        if (empty($ResultDetail[$i]['METHODOFSURVEY_NAME_P1'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultDetail[$i]['METHODOFSURVEY_NAME_P1']);
                        }
                        if (empty($ResultDetail[$i]['SURVEYJOB_NO_P1'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultDetail[$i]['SURVEYJOB_NO_P1']);
                        }
                        if (empty($ResultDetail[$i]['SURVEYJOB_DATE_P1'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultDetail[$i]['SURVEYJOB_DATE_P1']);
                        }
                        if (empty($ResultDetail[$i]['TYPEOFSURVEY_NAME_P1'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultDetail[$i]['TYPEOFSURVEY_NAME_P1']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_NAME_P1'])) {
                            $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + $i), $ResultDetail[$i]['PROJECT_NAME_P1']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_DOCNO_P1'])) {
                            $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + $i), $ResultDetail[$i]['PROJECT_DOCNO_P1']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_DOCDATE_P1'])) {
                            $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('N' . (5 + $current + $i), $ResultDetail[$i]['PROJECT_DOCDATE_P1']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_OFFICEOWNER_P1'])) {
                            $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('O' . (5 + $current + $i), $ResultDetail[$i]['PROJECT_OFFICEOWNER_P1']);
                        }
                        if (empty($ResultDetail[$i]['PROJECTTYPE_NAME_P1'])) {
                            $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('P' . (5 + $current + $i), $ResultDetail[$i]['PROJECTTYPE_NAME_P1']);
                        }
                        if (empty($ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P1'])) {
                            $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('Q' . (5 + $current + $i), $ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P1']);
                        }


                        if (empty($ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P2'])) {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PRINTPLATE_TYPE_NAME_P2']);
                        }
                        if (empty($ResultDetail[$i]['METHODOFSURVEY_NAME_P2'])) {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['METHODOFSURVEY_NAME_P2']);
                        }
                        if (empty($ResultDetail[$i]['SURVEYJOB_NO_P2'])) {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['SURVEYJOB_NO_P2']);
                        }
                        if (empty($ResultDetail[$i]['SURVEYJOB_DATE_P2'])) {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['SURVEYJOB_DATE_P2']);
                        }
                        if (empty($ResultDetail[$i]['TYPEOFSURVEY_NAME_P2'])) {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['TYPEOFSURVEY_NAME_P2']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_NAME_P2'])) {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROJECT_NAME_P2']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_DOCNO_P2'])) {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('M' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROJECT_DOCNO_P2']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_DOCDATE_P2'])) {
                            $sheet->setCellValue('N' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('N' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROJECT_DOCDATE_P2']);
                        }
                        if (empty($ResultDetail[$i]['PROJECT_OFFICEOWNER_P2'])) {
                            $sheet->setCellValue('O' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('O' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROJECT_OFFICEOWNER_P2']);
                        }
                        if (empty($ResultDetail[$i]['PROJECTTYPE_NAME_P2'])) {
                            $sheet->setCellValue('P' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('P' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['PROJECTTYPE_NAME_P2']);
                        }
                        if (empty($ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P2'])) {
                            $sheet->setCellValue('Q' . (5 + $current + count($ResultDetail) + $i), NULL);
                        } else {
                            $sheet->setCellValue('Q' . (5 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['MAINTYPEOFSURVEY_NAME_P2']);
                        }





                        $projectCellIndex = (5 + $current + count($ResultDetail) + $i);
                    }

                    $current += count($ResultDetail) * 2;
                }
            }

        }


        $sheet = $spreadsheet->setActiveSheetIndex(0);


        $sheet->mergeCells('A1:X1');
        $sheet->mergeCells('A2:X2');
        $sheet->mergeCells('A3:X3');
        $sheet->getColumnDimension('X')->setWidth(50);

        $fileTypeName = '';
        switch ($jobgroup_seq) {
            case 1:
                $fileTypeName = 'งานสำนักงานที่ดิน';
                break;
            case 2:
                $fileTypeName = 'งานรังวัดเอกชน';
                break;
            case 3:
                $fileTypeName = 'งานโครงการ';
                break;
        }

        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            $fileName =  $branchName . '-SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            $fileName =  $branchName . '-SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขที่คำขอรังวัด')
            ->setCellValue('C4', 'วันที่รับคำขอ')
            ->setCellValue('D4', 'ประเภทการรังวัด')
            ->setCellValue('E4', 'สถานะเรื่องรังวัด')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'ลำดับหมุดหลักเขต')
            ->setCellValue('H4', 'ชื่อหมุดหลักเขต')
            ->setCellValue('I4', 'เนื้อที่พิกัดฉาก')
            ->setCellValue('J4', 'เลขที่เอกสารสิทธิ')
            ->setCellValue('K4', 'เลขที่ดิน')
            ->setCellValue('L4', 'หน้าสำรวจ')
            ->setCellValue('M4', 'ประเภทรูปแปลงที่ดิน')
            ->setCellValue('N4', 'ผลรวมของผลบวก คูณ พิกัดเหนือ')
            ->setCellValue('O4', 'ผลรวมของผลบวก คูณ พิกัดใต้')
            ->setCellValue('P4', 'จังหวัด')
            ->setCellValue('Q4', 'อำเภอ')
            ->setCellValue('R4', 'ตำบล')
            ->setCellValue('S4', 'เนื้อที่เดิม')
            ->setCellValue('T4', 'เนื้อที่คำนวณ')
            ->setCellValue('U4', 'เนื้อที่ใช้')
            ->setCellValue('V4', 'เนื้อที่จริง')
            ->setCellValue('W4', 'ระวาง UTM')
            ->setCellValue('X4', 'หมายเหตุ')
            ->getStyle('A1:X4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->getColumnDimension('N')->setWidth(50);
        $sheet->mergeCells('A1:N1');
        $sheet->mergeCells('A2:N2');
        $sheet->mergeCells('A3:N3');




        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            $fileName =  $branchName . '-SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            $fileName =  $branchName . '-SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขที่คำขอรังวัด')
            ->setCellValue('C4', 'วันที่รับคำขอ')
            ->setCellValue('D4', 'ประเภทการรังวัด')
            ->setCellValue('E4', 'สถานะเรื่องรังวัด')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'ชื่อผู้ขอรังวัด')
            ->setCellValue('H4', 'วันที่ทำการรังวัด')
            ->setCellValue('I4', 'วันที่สิ้นสุดรังวัด')
            ->setCellValue('J4', 'ชนิดหลักเขตที่ใชัรังวัด')
            ->setCellValue('K4', 'จำนวนหลักเขตที่ใช้ปัก')
            ->setCellValue('L4', 'ชื่อผู้ตรวจ')
            ->setCellValue('M4', 'ผู้ตรวจมุม,ระยะ')
            ->setCellValue('N4', 'หมายเหตุ')
            ->getStyle('A1:N4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);




        $sheet = $spreadsheet->setActiveSheetIndex(2);
        $sheet->getColumnDimension('I')->setWidth(50);
        $sheet->mergeCells('A1:I1');
        $sheet->mergeCells('A2:I2');
        $sheet->mergeCells('A3:I3');


        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            $fileName =  $branchName . '-SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            $fileName =  $branchName . '-SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขที่คำขอรังวัด')
            ->setCellValue('C4', 'วันที่รับคำขอ')
            ->setCellValue('D4', 'ประเภทการรังวัด')
            ->setCellValue('E4', 'สถานะเรื่องรังวัด')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'สถานะงานรังวัด')
            ->setCellValue('G4', 'วันที่บันทึกพัฒน์ฯ 1')
            ->setCellValue('H4', 'วันที่บันทึกพัฒน์ฯ 2')
            ->setCellValue('I4', 'หมายเหตุ')
            ->getStyle('A1:I4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $sheet->getColumnDimension('O')->setWidth(50);
        $sheet->mergeCells('A1:O1');
        $sheet->mergeCells('A2:O2');
        $sheet->mergeCells('A3:O3');


        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            $fileName =  $branchName . '-SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
            $sheet->setCellValue('A3', $branchName);
            $bn = 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            $fileName =  $branchName . '-SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขที่คำขอรังวัด')
            ->setCellValue('C4', 'วันที่รับคำขอ')
            ->setCellValue('D4', 'ประเภทการรังวัด')
            ->setCellValue('E4', 'สถานะเรื่องรังวัด')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'เลขที่ บ.ท.ด.59')
            ->setCellValue('H4', 'วันที่ทำการถอนจ่าย')
            ->setCellValue('I4', 'จำนวนเงินที่ถอนจ่าย')
            ->setCellValue('J4', 'จำนวนเงินที่เรียกเพิ่ม/คืนเงิน')
            ->setCellValue('K4', 'สถานะ')
            ->setCellValue('L4', 'จำนวนเงินรวม')
            ->setCellValue('M4', 'จำนวนเงินที่เรียกเพิ่มจากผู้ขอ')
            ->setCellValue('N4', 'จำนวนเงินที่คืนผู้ขอ')
            ->setCellValue('O4', 'หมายเหตุ')
            ->getStyle('A1:O4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);



        $sheet = $spreadsheet->setActiveSheetIndex(4);
        if ($private) {
            $sheet->getColumnDimension('Q')->setWidth(50);
            $sheet->mergeCells('A1:Q1');
            $sheet->mergeCells('A2:Q2');
            $sheet->mergeCells('A3:Q3');


            if ($sts == 'e') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
                $fileName =  $branchName . '-SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            } else if ($sts == 's') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
                $fileName =  $branchName . '-SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            }


            $sheet->setCellValue('A4', '')
                ->setCellValue('B4', 'เลขที่คำขอรังวัด')
                ->setCellValue('C4', 'วันที่รับคำขอ')
                ->setCellValue('D4', 'ประเภทการรังวัด')
                ->setCellValue('E4', 'สถานะเรื่องรังวัด')
                ->getStyle('A1:E4')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
                ->setCellValue('G4', 'สำนักงานรังวัดเอกชน')
                ->setCellValue('H4', 'ช่างรังวัดเอกชน')
                ->setCellValue('I4', 'โทรศัพท์มือถือช่างรังวัดเอกชน')
                ->setCellValue('J4', 'วันที่นัดรังวัด')
                ->setCellValue('K4', 'เวลานัดรังวัด')
                ->setCellValue('L4', 'จำนวนวันที่รังวัด')
                ->setCellValue('M4', 'นายช่างรังวัดสำนักงานที่ดินรับผิดชอบ')
                ->setCellValue('N4', 'โทรศัพท์มือถือนายช่างรังวัดสำนักงานที่ดินรับผิดชอบ')
                ->setCellValue('O4', 'หมายเหตุการรังวัด')
                ->setCellValue('P4', 'ชื่อผู้บันทึกนัดรังวัด')
                ->setCellValue('Q4', 'หมายเหตุ')
                ->getStyle('A1:Q4')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        } else {
            $sheet->getColumnDimension('N')->setWidth(50);
            $sheet->mergeCells('A1:N1');
            $sheet->mergeCells('A2:N2');
            $sheet->mergeCells('A3:N3');


            if ($sts == 'e') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
                $fileName =  $branchName . '-SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            } else if ($sts == 's') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
                $fileName =  $branchName . '-SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            }
            $sheet->setCellValue('A4', '')
                ->setCellValue('B4', 'เลขที่คำขอรังวัด')
                ->setCellValue('C4', 'วันที่รับคำขอ')
                ->setCellValue('D4', 'ประเภทการรังวัด')
                ->setCellValue('E4', 'สถานะเรื่องรังวัด')
                ->getStyle('A1:E4')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            if ($private) {
                $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
                    ->setCellValue('G4', 'สำนักงานรังวัดเอกชน')
                    ->setCellValue('H4', 'ช่างรังวัดเอกชน')
                    ->setCellValue('I4', 'โทรศัพท์มือถือช่างรังวัดเอกชน')
                    ->setCellValue('J4', 'วันที่นัดรังวัด')
                    ->setCellValue('K4', 'เวลานัดรังวัด')
                    ->setCellValue('L4', 'จำนวนวันที่รังวัด')
                    ->setCellValue('M4', 'นายช่างรังวัดสำนักงานที่ดินรับผิดชอบ')
                    ->setCellValue('N4', 'โทรศัพท์มือถือนายช่างรังวัดสำนักงานที่ดินรับผิดชอบ')
                    ->setCellValue('O4', 'หมายเหตุการรังวัด')
                    ->setCellValue('P4', 'ชื่อผู้บันทึกนัดรังวัด')
                    ->setCellValue('Q4', 'หมายเหตุ')
                    ->getStyle('A1:Q4')
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else {
                $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
                    ->setCellValue('G4', 'วันที่นัดรังวัด')
                    ->setCellValue('H4', 'เวลานัดรังวัด')
                    ->setCellValue('I4', 'จำนวนวันที่รังวัด')
                    ->setCellValue('J4', 'ชื่อช่างรังวัด')
                    ->setCellValue('K4', 'หมายเลขโทรศัพท์มือถือ')
                    ->setCellValue('L4', 'รายละเอียดการนัด')
                    ->setCellValue('M4', 'ชื่อผู้บันทึกนัดรังวัด')
                    ->setCellValue('N4', 'หมายเหตุ')
                    ->getStyle('A1:N4')
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        }


        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];

        if ($jobgroup_seq == 3) {

            $sheet = $spreadsheet->setActiveSheetIndex(5);
            $sheet->getColumnDimension('R')->setWidth(50);
            $sheet->mergeCells('A1:R1');
            $sheet->mergeCells('A2:R2');
            $sheet->mergeCells('A3:R3');


            if ($sts == 'e') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
                $fileName =  $branchName . '-SVC ที่ข้อมูลแตกต่างกัน - ' . $fileTypeName;
            } else if ($sts == 's') {
                $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
                $sheet->setCellValue('A2', 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName);
                $sheet->setCellValue('A3', $branchName);
                $bn = 'SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
                $fileName =  $branchName . '-SVC ที่ถ่ายโอนสำเร็จ - ' . $fileTypeName;
            }


            $sheet->setCellValue('A4', '')
                ->setCellValue('B4', 'เลขที่คำขอรังวัด')
                ->setCellValue('C4', 'วันที่รับคำขอ')
                ->setCellValue('D4', 'ประเภทการรังวัด')
                ->setCellValue('E4', 'สถานะเรื่องรังวัด')
                ->getStyle('A1:E4')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
                ->setCellValue('G4', 'ประเภทเอกสารสิทธิ')
                ->setCellValue('H4', 'วิธีการรังวัด')
                ->setCellValue('I4', 'เลขที่คำขอรังวัด (ร.ว.12)')
                ->setCellValue('J4', 'วันที่รับคำขอ')
                ->setCellValue('K4', 'ประเภทการรังวัด')
                ->setCellValue('L4', 'ชื่อโครงการ')
                ->setCellValue('M4', 'เลขที่หนังสือ')
                ->setCellValue('N4', 'ลงวันที่')
                ->setCellValue('O4', 'หน่วยงาน')
                ->setCellValue('P4', 'ประเภทโครงการ')
                ->setCellValue('Q4', 'ประเภทการรังวัดหลัก')
                ->setCellValue('R4', 'หมายเหตุ')
                ->getStyle('A1:R4')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


            $sheet->getStyle('A1:R' . $projectCellIndex)->applyFromArray($styleArray);
            $sheet->getStyle('A1:R4')->getFont()->setBold(true);
            $sheet->getStyle('A5:E' . $projectCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
            $sheet->getStyle('R5:R' . $projectCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


            $sheet->getStyle('A4:R' . $projectCellIndex)
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $sheet->getStyle('R4:R' . $projectCellIndex)
                ->getAlignment()
                ->setWrapText(true);
        }


        for ($j = 0; $j < 5; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);

            if ($j == 0) {
                $sheet->getStyle('A1:X' . $surveyCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:X4')->getFont()->setBold(true);
                $sheet->getStyle('A5:E' . $surveyCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('X5:X' . $surveyCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


                $sheet->getStyle('A4:X' . $surveyCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $sheet->getStyle('X4:X' . $surveyCellIndex)
                    ->getAlignment()
                    ->setWrapText(true);
            } else if ($j == 1) {

                $sheet->getStyle('A1:N' . $ownerCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:N4')->getFont()->setBold(true);
                $sheet->getStyle('A5:E' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('N5:N' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


                $sheet->getStyle('A4:N' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('N4:N' . $ownerCellIndex)
                    ->getAlignment()
                    ->setWrapText(true);
            } else if ($j == 2) {

                $sheet->getStyle('A1:I' . $surveyStsCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:I4')->getFont()->setBold(true);
                $sheet->getStyle('A5:E' . $surveyStsCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('I5:I' . $surveyCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $sheet->getStyle('A4:I' . $surveyStsCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $sheet->getStyle('I4:I' . $surveyStsCellIndex)
                    ->getAlignment()
                    ->setWrapText(true);
            } else if ($j == 3) {
                $sheet->getStyle('A1:O' . $expenseCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:O4')->getFont()->setBold(true);
                $sheet->getStyle('A5:E' . $expenseCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                $sheet->getStyle('O5:O' . $expenseCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $sheet->getStyle('A4:O' . $expenseCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $sheet->getStyle('O4:O' . $expenseCellIndex)
                    ->getAlignment()
                    ->setWrapText(true);
            } else {
                if ($private) {
                    $sheet->getStyle('A1:Q' . $appointCellIndex)->applyFromArray($styleArray);
                    $sheet->getStyle('A1:Q4')->getFont()->setBold(true);
                    $sheet->getStyle('A5:E' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                    $sheet->getStyle('Q5:Q' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->getStyle('A4:Q' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                    $sheet->getStyle('Q4:Q' . $appointCellIndex)
                        ->getAlignment()
                        ->setWrapText(true);
                } else {
                    $sheet->getStyle('A1:N' . $appointCellIndex)->applyFromArray($styleArray);
                    $sheet->getStyle('A1:N4')->getFont()->setBold(true);
                    $sheet->getStyle('A5:E' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
                        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
                    $sheet->getStyle('N5:N' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
                        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                    $sheet->getStyle('A4:N' . $appointCellIndex)
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                    $sheet->getStyle('N4:N' . $appointCellIndex)
                        ->getAlignment()
                        ->setWrapText(true);
                }
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //  $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        // if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
        //      $fileName = urlencode($fileName);
        // }
        // $fileName = urlencode($fileName);
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save('php://output');


        break;
    case "digital_map":

        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $udmCellIndex = 0;
        $errorUdm = 0;

        $current = 0;
        //===================UDM DETAIL=====================================
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();

            $udmparcelSeqP1 = $Result[$j]['UDM_PARCEL_SEQ_P1'];
            $udmparcelSeqP2 = $Result[$j]['UDM_PARCEL_SEQ_P2'];
            $haserror = true;
            include './sva/querySvaDetailData.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultDetail[] = $row;
            }

            if ($sts == 'e') {
                for ($i = 0; $i < count($ResultDetail); $i++) {
                    $problemDesc = '';
                    if (!$udmparcelSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$udmparcelSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                    } else {

                        if ($ResultDetail[$i]['OLDUTM_P1'] != $ResultDetail[$i]['OLDUTM_P2']) $problemDesc .= "ระวาง UTM ก่อนปรับรูปแปลงไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['NEWUTM_P1'] != $ResultDetail[$i]['NEWUTM_P2']) $problemDesc .= "ระวาง UTM หลังปรับรูปแปลงไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['OLDAREA_P1'] != $ResultDetail[$i]['OLDAREA_P2']) $problemDesc .= "เนื้อที่ก่อนปรับรูปแปลงไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['NEWAREA_P1'] != $ResultDetail[$i]['NEWAREA_P2']) $problemDesc .= "เนื้อที่หลังปรับรูปแปลงไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['OLDSCALE_SEQ_P1'] != $ResultDetail[$i]['OLDSCALE_SEQ_P2']) $problemDesc .= "มาตราส่วนระวางก่อนปรับรูปแปลงไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['NEWSCALE_SEQ_P1'] != $ResultDetail[$i]['NEWSCALE_SEQ_P2']) $problemDesc .= "มาตราส่วนหลังปรับรูปแปลงไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['OLDPERIMETER_P1'] != $ResultDetail[$i]['OLDPERIMETER_P2']) $problemDesc .= "เส้นรอบรูปก่อนปรับรูปแปลงไม่ตรงกัน\n";
                        if ($ResultDetail[$i]['NEWPERIMETER_P1'] != $ResultDetail[$i]['NEWPERIMETER_P2']) $problemDesc .= "เส้นรอบรูปหลังปรับรูปแปลงไม่ตรงกัน\n";
                    }

                    if ($problemDesc == '') {
                        $haserror = false;
                        continue;
                    }
                    $sheet->setCellValue('P' . (6 + $current + $i), rtrim($problemDesc));
                    $problemDesc = '';
                }
            }

            if (!$haserror) continue;
            else $errorUdm++;


            if (count($ResultDetail) <= 0) {
                $sheet->mergeCells('A' . (6 + $current) . ':A' . (7 + $current));
                $sheet->mergeCells('B' . (6 + $current) . ':B' . (7 + $current));
                $sheet->mergeCells('C' . (6 + $current) . ':C' . (7 + $current));
                $sheet->mergeCells('D' . (6 + $current) . ':D' . (7 + $current));
                $sheet->mergeCells('E' . (6 + $current) . ':E' . (7 + $current));
                $sheet->mergeCells('F' . (6 + $current) . ':F' . (7 + $current));

                if ($Result[$j]['UDM_PARCEL_SEQ_P1']) {

                    $sheet->setCellValue('A' . (6 + $current), ($j + 1));
                    if (empty($Result[$j]['UTM_P1'])) {
                        $sheet->setCellValue('B' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (6 + $current), $Result[$j]['UTM_P1']);
                    }
                    if (empty($Result[$j]['UTMMAP4_P1'])) {
                        $sheet->setCellValue('C' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (6 + $current), $Result[$j]['UTMMAP4_P1']);
                    }
                    if (empty($Result[$j]['SCALE_NAME_P1'])) {
                        $sheet->setCellValue('D' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (6 + $current), $Result[$j]['SCALE_NAME_P1']);
                    }
                    if (empty($Result[$j]['LAND_NO_P1'])) {
                        $sheet->setCellValue('E' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (6 + $current), $Result[$j]['LAND_NO_P1']);
                    }
                    if (empty($Result[$j]['PARCELTYPE_DESC_P1'])) {
                        $sheet->setCellValue('F' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (6 + $current), $Result[$j]['PARCELTYPE_DESC_P1']);
                    }
                } 
                else {
                    $sheet->setCellValue('A' . (6 + $current), ($j + 1));
                    if (empty($Result[$j]['UTM_P2'])) {
                        $sheet->setCellValue('B' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (6 + $current), $Result[$j]['UTM_P2']);
                    }
                    if (empty($Result[$j]['UTMMAP4_P2'])) {
                        $sheet->setCellValue('C' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (6 + $current), $Result[$j]['UTMMAP4_P2']);
                    }
                    if (empty($Result[$j]['SCALE_NAME_P2'])) {
                        $sheet->setCellValue('D' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (6 + $current), $Result[$j]['SCALE_NAME_P2']);
                    }
                    if (empty($Result[$j]['LAND_NO_P2'])) {
                        $sheet->setCellValue('E' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (6 + $current), $Result[$j]['LAND_NO_P2']);
                    }
                    if (empty($Result[$j]['PARCELTYPE_DESC_P2'])) {
                        $sheet->setCellValue('F' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (6 + $current), $Result[$j]['PARCELTYPE_DESC_P2']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (6 + $current) . ':A' . (6 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('B' . (6 + $current) . ':B' . (6 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('C' . (6 + $current) . ':C' . (6 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('D' . (6 + $current) . ':D' . (6 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('E' . (6 + $current) . ':E' . (6 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('F' . (6 + $current) . ':F' . (6 + $current + (count($ResultDetail) * 2) - 1));
                $sheet->mergeCells('G' . (6 + $current) . ':G' . (6 + $current + (count($ResultDetail)) - 1));
                $sheet->mergeCells('G' . (6 + $current + (count($ResultDetail))) . ':G' . (6 + $current + (count($ResultDetail) * 2) - 1));



                if ($Result[$j]['UDM_PARCEL_SEQ_P1']) {

                    $sheet->setCellValue('A' . (6 + $current), ($j + 1));
                    if (empty($Result[$j]['UTM_P1'])) {
                        $sheet->setCellValue('B' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (6 + $current), $Result[$j]['UTM_P1']);
                    }
                    if (empty($Result[$j]['UTMMAP4_P1'])) {
                        $sheet->setCellValue('C' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (6 + $current), $Result[$j]['UTMMAP4_P1']);
                    }
                    if (empty($Result[$j]['SCALE_NAME_P1'])) {
                        $sheet->setCellValue('D' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (6 + $current), $Result[$j]['SCALE_NAME_P1']);
                    }
                    if (empty($Result[$j]['LAND_NO_P1'])) {
                        $sheet->setCellValue('E' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (6 + $current), $Result[$j]['LAND_NO_P1']);
                    }
                    if (empty($Result[$j]['PARCELTYPE_DESC_P1'])) {
                        $sheet->setCellValue('F' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (6 + $current), $Result[$j]['PARCELTYPE_DESC_P1']);
                    }
                } 
                else {
                    $sheet->setCellValue('A' . (6 + $current), ($j + 1));
                    if (empty($Result[$j]['UTM_P2'])) {
                        $sheet->setCellValue('B' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (6 + $current), $Result[$j]['UTM_P2']);
                    }
                    if (empty($Result[$j]['UTMMAP4_P2'])) {
                        $sheet->setCellValue('C' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (6 + $current), $Result[$j]['UTMMAP4_P2']);
                    }
                    if (empty($Result[$j]['SCALE_NAME_P2'])) {
                        $sheet->setCellValue('D' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (6 + $current), $Result[$j]['SCALE_NAME_P2']);
                    }
                    if (empty($Result[$j]['LAND_NO_P2'])) {
                        $sheet->setCellValue('E' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (6 + $current), $Result[$j]['LAND_NO_P2']);
                    }
                    if (empty($Result[$j]['PARCELTYPE_DESC_P2'])) {
                        $sheet->setCellValue('F' . (6 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (6 + $current), $Result[$j]['PARCELTYPE_DESC_P2']);
                    }
                }

                $sheet->setCellValue('G' . (6 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('G' . (6 + $current + count($ResultDetail)), 'พัฒน์ฯ 2');

                for ($i = 0; $i < count($ResultDetail); $i++) {
                    if (empty($ResultDetail[$i]['OLDUTM_P1'])) {
                        $sheet->setCellValue('H' . (6 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (6 + $current + $i), $ResultDetail[$i]['OLDUTM_P1']);
                    }
                    if (empty($ResultDetail[$i]['NEWUTM_P1'])) {
                        $sheet->setCellValue('L' . (6 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (6 + $current + $i), $ResultDetail[$i]['NEWUTM_P1']);
                    }
                    if (empty($ResultDetail[$i]['OLDAREA_P1'])) {
                        $sheet->setCellValue('I' . (6 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (6 + $current + $i), $ResultDetail[$i]['OLDAREA_P1']);
                    }
                    if (empty($ResultDetail[$i]['NEWAREA_P1'])) {
                        $sheet->setCellValue('M' . (6 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (6 + $current + $i), $ResultDetail[$i]['NEWAREA_P1']);
                    }
                    if (empty($ResultDetail[$i]['OLDSCALE_SEQ_P1'])) {
                        $sheet->setCellValue('J' . (6 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (6 + $current + $i), $ResultDetail[$i]['OLDSCALE_SEQ_P1']);
                    }
                    if (empty($ResultDetail[$i]['NEWSCALE_SEQ_P1'])) {
                        $sheet->setCellValue('N' . (6 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (6 + $current + $i), $ResultDetail[$i]['NEWSCALE_SEQ_P1']);
                    }
                    if (empty($ResultDetail[$i]['OLDPERIMETER_P1'])) {
                        $sheet->setCellValue('K' . (6 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (6 + $current + $i), $ResultDetail[$i]['OLDPERIMETER_P1']);
                    }
                    if (empty($ResultDetail[$i]['NEWPERIMETER_P1'])) {
                        $sheet->setCellValue('O' . (6 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (6 + $current + $i), $ResultDetail[$i]['NEWPERIMETER_P1']);
                    }



                    if (empty($ResultDetail[$i]['OLDUTM_P2'])) {
                        $sheet->setCellValue('H' . (6 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (6 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['OLDUTM_P2']);
                    }
                    if (empty($ResultDetail[$i]['NEWUTM_P2'])) {
                        $sheet->setCellValue('L' . (6 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (6 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['NEWUTM_P2']);
                    }
                    if (empty($ResultDetail[$i]['OLDAREA_P2'])) {
                        $sheet->setCellValue('I' . (6 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (6 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['OLDAREA_P2']);
                    }
                    if (empty($ResultDetail[$i]['NEWAREA_P2'])) {
                        $sheet->setCellValue('M' . (6 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (6 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['NEWAREA_P2']);
                    }
                    if (empty($ResultDetail[$i]['OLDSCALE_SEQ_P2'])) {
                        $sheet->setCellValue('J' . (6 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (6 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['OLDSCALE_SEQ_P2']);
                    }
                    if (empty($ResultDetail[$i]['NEWSCALE_SEQ_P2'])) {
                        $sheet->setCellValue('N' . (6 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (6 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['NEWSCALE_SEQ_P2']);
                    }
                    if (empty($ResultDetail[$i]['OLDPERIMETER_P2'])) {
                        $sheet->setCellValue('K' . (6 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (6 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['OLDPERIMETER_P2']);
                    }
                    if (empty($ResultDetail[$i]['NEWPERIMETER_P2'])) {
                        $sheet->setCellValue('O' . (6 + $current + count($ResultDetail) + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (6 + $current + count($ResultDetail) + $i), $ResultDetail[$i]['NEWPERIMETER_P2']);
                    }
                }
                $udmCellIndex = (6 + $current + count($ResultDetail) + $i) - 1;
                $current += count($ResultDetail) * 2;
            }
        }


        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];



        $sheet->mergeCells('A1:P1');
        $sheet->mergeCells('A2:P2');
        $sheet->mergeCells('A3:P3');
        $sheet->mergeCells('A4:A5');
        $sheet->mergeCells('B4:B5');
        $sheet->mergeCells('C4:C5');
        $sheet->mergeCells('D4:D5');
        $sheet->mergeCells('E4:E5');
        $sheet->mergeCells('F4:F5');
        $sheet->mergeCells('G4:G5');
        $sheet->mergeCells('H4:K4');
        $sheet->mergeCells('L4:O4');
        $sheet->mergeCells('P4:P5');
        $sheet->getColumnDimension('P')->setWidth(50);

        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $errorUdm . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลระบบปรับปรุงรูปแปลงแผนที่ดิจิทัลข้อมูลแตกต่างกัน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลระบบปรับปรุงรูปแปลงแผนที่ดิจิทัลข้อมูลแตกต่างกัน ';
            $fileName =  $branchName . '-ข้อมูลระบบปรับปรุงรูปแปลงแผนที่ดิจิทัลที่ข้อมูลแตกต่างกัน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลระบบปรับปรุงรูปแปลงแผนที่ดิจิทัลที่ถ่ายโอนสำเร็จ');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลระบบปรับปรุงรูปแปลงแผนที่ดิจิทัลที่ถ่ายโอนสำเร็จ - ';
            $fileName =  $branchName . '-ข้อมูลระบบปรับปรุงรูปแปลงแผนที่ดิจิทัลที่ถ่ายโอนสำเร็จ';
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'หมายเลขระวางแผนที่')
            ->setCellValue('C4', 'แผ่นที่')
            ->setCellValue('D4', 'มาตราส่วน')
            ->setCellValue('E4', 'เลขที่ดิน')
            ->setCellValue('F4', 'ประเภทที่ดิน')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2')
            ->setCellValue('H4', 'ข้อมูลก่อนปรับปรุงรูปแปลง')
            ->setCellValue('L4', 'ข้อมูลหลังปรับปรุงรูปแปลง')
            ->setCellValue('H5', 'ระวาง UTM')
            ->setCellValue('I5', 'เนื้อที่')
            ->setCellValue('J5', 'มาตราส่วนระวาง UTM')
            ->setCellValue('K5', 'เส้นรอบรูปแปลงที่ดิน')
            ->setCellValue('L5', 'ระวาง UTM')
            ->setCellValue('M5', 'เนื้อที่')
            ->setCellValue('N5', 'มาตราส่วนระวาง UTM')
            ->setCellValue('O5', 'เส้นรอบรูปแปลงที่ดิน')
            ->setCellValue('P4', 'หมายเหตุ')
            ->getStyle('A1:P4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->getStyle('A1:P' . $udmCellIndex)->applyFromArray($styleArray);
        $sheet->getStyle('A1:P5')->getFont()->setBold(true);
        $sheet->getStyle('A6:G' . $udmCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $sheet->getStyle('P6:P' . $udmCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT)
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);


        $sheet->getStyle('A6:P' . $udmCellIndex)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('P6:P' . $udmCellIndex)
            ->getAlignment()
            ->setWrapText(true);


        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        // if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
        //      $fileName = urlencode($fileName);
        // }
        // $fileName = urlencode($fileName);
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save('php://output');


        break;
    default:
        # code...
        break;
}
