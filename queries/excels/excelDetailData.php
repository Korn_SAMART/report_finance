<?php
require '../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


$landoffice = $_GET['landoffice'];
$amphur = !isset($_GET['amphur']) || $_GET['amphur'] == 'all' ? '' : $_GET['amphur'];
$tambon = !isset($_GET['tambon']) || $_GET['tambon'] == 'all' ? '' : $_GET['tambon'];
$regno = !isset($_GET['regno']) ? '' : $_GET['regno'];
$namecondo = !isset($_GET['namecondo']) ? '' : $_GET['namecondo'];
$branchName = !isset($_GET['branchName']) ? '' : $_GET['branchName'];

if ($amphur != 'all') include 'changeAPStoAPTC.php';

$Result = array();
$branch = '';

include 'queryRegCondo.php';
while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
    $Result[] = $row;
}


$Result = array_values(array_unique($Result, SORT_REGULAR));

usort($Result, function ($a, $b) {
    if (!isset($a['AMPHUR_CODE'])) $a['AMPHUR_CODE'] = 0;
    if (!isset($b['AMPHUR_CODE'])) $b['AMPHUR_CODE'] = 0;
    if (!isset($a['TUMBON_CODE'])) $a['TUMBON_CODE'] = 0;
    if (!isset($b['TUMBON_CODE'])) $b['TUMBON_CODE'] = 0;
    return (int)$a['AMPHUR_CODE'] - (int)$b['AMPHUR_CODE']
        ?: (int)$a['TUMBON_CODE'] - (int)$b['TUMBON_CODE']
        ?: (int)$a['REG_NO'] - (int)$b['REG_NO'];
});

//Declare spreadsheet
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->mergeCells('A1:F1');
$sheet->setCellValue('A1', 'รายงานทะเบียน อาคารชุด');

$fileName = 'รายงานทะเบียน-อาคารชุด';
$sheet->mergeCells('A2:F2');
$sheet->setCellValue('A2', $branchName);
$sheet->setCellValue('A3', 'ลำดับ')
    ->setCellValue('B3', 'เลขที่ทะเบียนอาคารชุด')
    ->setCellValue('C3', 'ชื่ออาคารชุด')
    ->setCellValue('D3', 'อำเภอ')
    ->setCellValue('E3', 'ตำบล')
    ->setCellValue('F3', 'ปัญหาที่พบ')
    ->getStyle('A1:F3')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$last = count($Result);
for ($i = 0; $i < $last; $i++) {
    $sheet->setCellValue('A' . (4 + $i), ($i + 1))
        ->setCellValue('B' . (4 + $i), $Result[$i]['REG_NO']);
    if (empty($Result[$i]['NAME_CONDO'])) {
        $sheet->setCellValue('C' . (4 + $i), NULL);
    } else {
        $sheet->setCellValue('C' . (4 + $i), $Result[$i]['NAME_CONDO']);
    }
    if (empty($Result[$i]['AMPHUR_DESC'])) {
        $sheet->setCellValue('D' . (4 + $i), NULL);
    } else {
        $sheet->setCellValue('D' . (4 + $i), $Result[$i]['AMPHUR_DESC']);
    }
    if (empty($Result[$i]['TUMBON_DESC'])) {
        $sheet->setCellValue('E' . (4 + $i), NULL);
    } else {
        $sheet->setCellValue('E' . (4 + $i), $Result[$i]['TUMBON_DESC']);
    }

}



// Add Problem parcel description column

for ($i = 0; $i < count($Result); $i++) {
    $typeNumber = 1;

 //   $amphur = !isset($Result[$i]['AMPHUR_CODE']) ? '' : $Result[$i]['AMPHUR_CODE'];
    $regno = !isset($Result[$i]['REG_NO'])? '' : $Result[$i]['REG_NO'];
    $namecondo = !isset($Result[$i]['NAME_CONDO'])? '' : $Result[$i]['NAME_CONDO'];


    if($amphur!='null') include 'changeAPStoAPTC.php';

    $ResultProblem = array();

    $table = 'CONDO';
    include 'queryCheckRegCondo.php';
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $ResultProblem[] = $row;
    }

    $data = [];
    $data[0] = $ResultProblem;
    // $data[1] = $ResultImg;
    $problemDesc = "";


    if (isset($data[0][0])) {

        // $problemDesc = $problemDesc . $typeNumber . ". CONDO_REG\n";
        $typeNumber++;
        for ($j = 0; $j < count($data[0]); $j++) {
  
            // $problemDesc = $problemDesc . "ข้อมูลแถวที่ " . ($j + 1) . "\n";
            $problemDesc = $problemDesc . "ข้อมูลเอกสารสิทธิอาคารชุด (CONDO_REG)\n";
            //Check is null
            $data[0][$j]["REG_NO"] = isset($data[0][$j]["REG_NO"]) ? $data[0][$j]["REG_NO"] : null;
            $data[0][$j]["NO"] = isset($data[0][$j]["NO"]) ? $data[0][$j]["NO"] : null;
            $data[0][$j]["MGR_OFF_PROV"] = isset($data[0][$j]["MGR_OFF_PROV"]) ? $data[0][$j]["MGR_OFF_PROV"] : null;
            $data[0][$j]["MAS_MGR_OFF_PROV"] = isset($data[0][$j]["MAS_MGR_OFF_PROV"]) ? $data[0][$j]["MAS_MGR_OFF_PROV"] : null;
            $data[0][$j]["MGR_OFF_AMP"] = isset($data[0][$j]["MGR_OFF_AMP"]) ? $data[0][$j]["MGR_OFF_AMP"] : null;
            $data[0][$j]["MAS_MGR_OFF_AMP"] = isset($data[0][$j]["MAS_MGR_OFF_AMP"]) ? $data[0][$j]["MAS_MGR_OFF_AMP"] : null;
            $data[0][$j]["MGR_OFF_TUMB"] = isset($data[0][$j]["MGR_OFF_TUMB"]) ? $data[0][$j]["MGR_OFF_TUMB"] : null;
            $data[0][$j]["MAS_MGR_OFF_TUMB"] = isset($data[0][$j]["MAS_MGR_OFF_TUMB"]) ? $data[0][$j]["MAS_MGR_OFF_TUMB"] : null;
            $data[0][$j]["TITLE_MGR"] = isset($data[0][$j]["TITLE_MGR"]) ? $data[0][$j]["TITLE_MGR"] : null;
            $data[0][$j]["MAS_TITLE_NAME"] = isset($data[0][$j]["MAS_TITLE_NAME"]) ? $data[0][$j]["MAS_TITLE_NAME"] : null;
            $data[0][$j]["MGR_PROV"] = isset($data[0][$j]["MGR_PROV"]) ? $data[0][$j]["MGR_PROV"] : null;
            $data[0][$j]["MAS_MGR_PROV"] = isset($data[0][$j]["MAS_MGR_PROV"]) ? $data[0][$j]["MAS_MGR_PROV"] : null;
            $data[0][$j]["MGR_AMPHUR"] = isset($data[0][$j]["MGR_AMPHUR"]) ? $data[0][$j]["MGR_AMPHUR"] : null;
            $data[0][$j]["MAS_MGR_AMPHUR"] = isset($data[0][$j]["MAS_MGR_AMPHUR"]) ? $data[0][$j]["MAS_MGR_AMPHUR"] : null;
            $data[0][$j]["MGR_TUMBON"] = isset($data[0][$j]["MGR_TUMBON"]) ? $data[0][$j]["MGR_TUMBON"] : null;
            $data[0][$j]["MAS_MGR_TUMBON"] = isset($data[0][$j]["MAS_MGR_TUMBON"]) ? $data[0][$j]["MAS_MGR_TUMBON"] : null;
            $data[0][$j]["NHAN"] = isset($data[0][$j]["NHAN"]) ? $data[0][$j]["NHAN"] : null;
            $data[0][$j]["NWAH"] = isset($data[0][$j]["NWAH"]) ? $data[0][$j]["NWAH"] : null;

            // เลขทะเบียนอาคารชุดผิดรูปแบบ (รูปแบบคือ เลขลำดับ/ปี พ.ศ.)
            $regno = explode("/", $data[0][$j]["REG_NO"]);
            
            if (count($regno) != 2 || !is_numeric($regno[0]) || strlen($regno[1]) != 4 || $regno[1] < 2460 || $regno[1] > date('Y')+543) {
                $problemDesc = $problemDesc . "- เลขทะเบียนอาคารชุดผิดรูปแบบ (รูปแบบคือ เลขลำดับ/ปี พ.ศ.)\n";
            }
            // เลขทะเบียนอาคารชุดซ้ำ
            if ($data[0][$j]["NO"] == 1) {
                $problemDesc = $problemDesc . "- เลขทะเบียนอาคารชุดซ้ำ\n";
            }
            // ไม่ระบุเลขที่เอกสารสิทธิที่ตั้งอาคารชุด
            if ($data[0][$j]["REG_NO"] == null) {
                $problemDesc = $problemDesc . "- ไม่ระบุเลขที่เอกสารสิทธิที่ตั้งอาคารชุด\n";
            }
            // ไม่มีชื่อจังหวัดในข้อมูลที่อยู่ของนิติบุคคล
            if ($data[0][$j]["MGR_OFF_PROV"] == null) {
                $problemDesc = $problemDesc . "- ไม่มีชื่อจังหวัดในข้อมูลที่อยู่ของนิติบุคคล\n";
            }
            // ชื่อจังหวัดในข้อมูลที่อยู่ของนิติบุคคลไม่ถูกต้อง 
            if ($data[0][$j]["MGR_OFF_PROV"] != $data[0][$j]["MAS_MGR_OFF_PROV"]) {
                $problemDesc = $problemDesc . "- ชื่อจังหวัดในข้อมูลที่อยู่ของนิติบุคคลไม่ถูกต้อง\n";
            }
            // ไม่มีชื่ออำเภอในข้อมูลที่อยู่ของนิติบุคคล
            if ($data[0][$j]["MGR_OFF_AMP"] == null) {
                $problemDesc = $problemDesc . "- ไม่มีชื่ออำเภอในข้อมูลที่อยู่ของนิติบุคคล\n";
            }
            // ชื่ออำเภอในข้อมูลที่อยู่ของนิติบุคคลไม่ถูกต้อง
            if ($data[0][$j]["MGR_OFF_AMP"] != $data[0][$j]["MAS_MGR_OFF_AMP"]) {
                $problemDesc = $problemDesc . "- ชื่ออำเภอในข้อมูลที่อยู่ของนิติบุคคลไม่ถูกต้อง\n";
            }
            // ไม่มีชื่อตำบลในข้อมูลที่อยู่ของนิติบุคคล
            if ($data[0][$j]["MGR_OFF_TUMB"] == null) {
                $problemDesc = $problemDesc . "- ไม่มีชื่อตำบลในข้อมูลที่อยู่ของนิติบุคคล\n";
            }
            // ชื่อตำบลในข้อมูลที่อยู่ของนิติบุคคลไม่ถูกต้อง
            if ($data[0][$j]["MGR_OFF_TUMB"] != $data[0][$j]["MAS_MGR_OFF_TUMB"]) {
                $problemDesc = $problemDesc . "- ชื่อตำบลในข้อมูลที่อยู่ของนิติบุคคลไม่ถูกต้อง\n";
            }
            // ไม่มีคำนำหน้าชื่อในข้อมูลของนิติบุคคล
            if ($data[0][$j]["TITLE_MGR"] == null) {
                $problemDesc = $problemDesc . "- ไม่มีคำนำหน้าชื่อในข้อมูลของนิติบุคคล\n";
            }
            // คำนำหน้าชื่อของนิติบุคคลไม่ถูกต้อง
            if ($data[0][$j]["TITLE_MGR"] != $data[0][$j]["MAS_TITLE_NAME"]) {
                $problemDesc = $problemDesc . "- คำนำหน้าชื่อของนิติบุคคลไม่ถูกต้อง\n";
            }
            // ไม่มีชื่อจังหวัดในข้อมูลที่อยู่ของผู้จัดการนิติบุคคล
            if ($data[0][$j]["MGR_PROV"] == null) {
                $problemDesc = $problemDesc . "- ไม่มีชื่อจังหวัดในข้อมูลที่อยู่ของผู้จัดการนิติบุคคล\n";
            }
            // ชื่อจังหวัดในข้อมูลที่อยู่ของผู้จัดการนิติบุคคลไม่ถูกต้อง
            if ($data[0][$j]["MGR_PROV"] != $data[0][$j]["MAS_MGR_PROV"]) {
                $problemDesc = $problemDesc . "- ชื่อจังหวัดในข้อมูลที่อยู่ของผู้จัดการนิติบุคคลไม่ถูกต้อง\n";
            }
            // ไม่มีชื่ออำเภอในข้อมูลที่อยู่ของผู้จัดการนิติบุคคล
            if ($data[0][$j]["MGR_AMPHUR"] == null) {
                $problemDesc = $problemDesc . "- ไม่มีชื่ออำเภอในข้อมูลที่อยู่ของผู้จัดการนิติบุคคล\n";
            }
            // ชื่ออำเภอในข้อมูลที่อยู่ของผู้จัดการนิติบุคคลไม่ถูกต้อง
            if ($data[0][$j]["MGR_AMPHUR"] != $data[0][$j]["MAS_MGR_AMPHUR"]) {
                $problemDesc = $problemDesc . "- ชื่ออำเภอในข้อมูลที่อยู่ของผู้จัดการนิติบุคคลไม่ถูกต้อง\n";
            }
            // ไม่มีชื่อตำบลในข้อมูลที่อยู่ของผู้จัดการนิติบุคคล
            if ($data[0][$j]["MGR_TUMBON"] == null) {
                $problemDesc = $problemDesc . "- ไม่มีชื่อตำบลในข้อมูลที่อยู่ของผู้จัดการนิติบุคคล\n";
            }
            // ชื่อตำบลในข้อมูลที่อยู่ของผู้จัดการนิติบุคคลไม่ถูกต้อง
            if ($data[0][$j]["MGR_TUMBON"] != $data[0][$j]["MAS_MGR_TUMBON"]) {
                $problemDesc = $problemDesc . "- ชื่อตำบลในข้อมูลที่อยู่ของผู้จัดการนิติบุคคลไม่ถูกต้อง\n";
            }
            // ตรวจสอบเนื้อที่ "งาน" เกิน 3 งาน (ตัวอย่างเช่น 4 งาน ใส่ข้อมูลเป็น 1 ไร่)
            if (intval($data[0][$j]["NHAN"]) > 3) {
                $problemDesc = $problemDesc . "- ตรวจสอบเนื้อที่ \"งาน\" เกิน 3 งาน (ตัวอย่างเช่น 4 งาน ใส่ข้อมูลเป็น 1 ไร่)\n";
            }
            // ตรวจสอบเนื้อที่ \"ตารางวา\" เกิน 99 ตารางวา (ตัวอย่างเช่น 100 ตารางวา ใส่ข้อมูลเป็น 1 งาน)
            if (intval($data[0][$j]["NWAH"]) > 99) {
                $problemDesc = $problemDesc . "- ตรวจสอบเนื้อที่ \"ตารางวา\" เกิน 99 ตารางวา (ตัวอย่างเช่น 100 ตารางวา ใส่ข้อมูลเป็น 1 งาน)\n";
            }


        }

    }
    $sheet->setCellValue('F' . (4 + $i), $problemDesc);
    $problemDesc = "";
}


$sheet->getStyle('A1:F3')->getFont()->setBold(true);
$sheet->getColumnDimension('A')->setAutoSize(true);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setAutoSize(true);
$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getStyle('B3:B' . $last)
    ->getNumberFormat()
    ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT);
$sheet->getStyle('B3:B' . $last)
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
