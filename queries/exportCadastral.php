<?php
require '../plugins/vendor/autoload.php';
include 'func.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$count_temp = 0;
$problemDesc = "";
$bn = $_REQUEST['branchName'];

$finCol = $_REQUEST['type'] =='s'? 'W' : 'X';
$title = 'รายการข้อมูลภาพลักษณ์ต้นร่าง ('.getPrintplateTypeName($_REQUEST['printplateType']).')';
if($_REQUEST['type'] =='e') $title .= ' ที่แตกต่างกัน';
$fileName = date('y/m/d').'-'.$title.'-'.$bn;


$sheet->mergeCells('A2:'.$finCol.'2');
$sheet->setCellValue('A2',$title);

$sheet->mergeCells('A3:'.$finCol.'3');
$sheet->setCellValue('A3',$bn);

$sheet->mergeCells('A4:L4');
$sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
$sheet->mergeCells('M4:'.$finCol.'4');
$sheet->setCellValue('M4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

$sheet->setCellValue('A5','')
    ->setCellValue('B5','หมายเลขแฟ้ม')
    ->setCellValue('C5','เลขที่ต้นร่าง')
    ->setCellValue('D5','ครั้งที่รังวัด')
    ->setCellValue('E5','อำเภอ')
    ->setCellValue('F5','ตำบล')
    ->setCellValue('G5','ประเภทรังวัด')
    ->setCellValue('H5','วันที่รังวัด')
    ->setCellValue('I5','ประเภทเอกสารสิทธิ')
    ->setCellValue('J5','ลำดับภาพ')
    ->setCellValue('K5','ภาพลักษณ์ต้นร่าง')
    ->setCellValue('L5','Path ที่ทำการถ่ายโอน')

    ->setCellValue('M5','หมายเลขแฟ้ม')
    ->setCellValue('N5','เลขที่ต้นร่าง')
    ->setCellValue('O5','ครั้งที่รังวัด')
    ->setCellValue('P5','อำเภอ')
    ->setCellValue('Q5','ตำบล')
    ->setCellValue('R5','ประเภทรังวัด')
    ->setCellValue('S5','วันที่รังวัด')
    ->setCellValue('T5','ประเภทเอกสารสิทธิ')
    ->setCellValue('U5','ลำดับภาพ')
    ->setCellValue('V5','ภาพลักษณ์ต้นร่าง')
    ->setCellValue('W5','Path ภาพลักษณ์ต้นร่าง');
if($_REQUEST['type'] =='e') $sheet->setCellValue('X5','หมายเหตุ');
    
$sheet->getStyle('A2:'.$finCol.'5')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

// DATA
include 'queryCadastralDiff.php';


for($i=0; $i<count($Result); $i++){
    $str = '';
    $sheet->setCellValue('A'.(6+$current), ($current+1));

    $str = empty($Result[$i]['SHEETCODE'])? '' : $Result[$i]['SHEETCODE'];
    $str .= '-';
    $str .= empty($Result[$i]['BOX_NO'])? '' : $Result[$i]['BOX_NO'];
    $sheet->setCellValue('B'.(6+$current), $str);
    $str = empty($Result[$i]['CADASTRAL_NO'])? '-' : $Result[$i]['CADASTRAL_NO'];
    $sheet->setCellValue('C'.(6+$current), $str);
    $str = empty($Result[$i]['NUMOFSURVEY_QTY'])? '-' : $Result[$i]['NUMOFSURVEY_QTY'];
    $sheet->setCellValue('D'.(6+$current), $str);
    $str = empty($Result[$i]['AMPHUR_NAME'])? '-' : $Result[$i]['AMPHUR_NAME'];
    $sheet->setCellValue('E'.(6+$current), $str);
    $str = empty($Result[$i]['TAMBOL_NAME'])? '-' : $Result[$i]['TAMBOL_NAME'];
    $sheet->setCellValue('F'.(6+$current), $str);
    $str = empty($Result[$i]['TYPEOFSURVEY_NAME'])? '-' : $Result[$i]['TYPEOFSURVEY_NAME'];
    $sheet->setCellValue('G'.(6+$current), $str);
    $str = empty($Result[$i]['SURVEY_DATE'])? '-' : convertDtm($Result[$i]['SURVEY_DATE']);
    $sheet->setCellValue('H'.(6+$current), $str);
    $str = empty($Result[$i]['PRINTPLATE_TYPE_SEQ'])? 'ไม่สามารถแยกประเภทเอกสารได้' : getPrintplateTypeName($Result[$i]['PRINTPLATE_TYPE_SEQ']);
    $sheet->setCellValue('I'.(6+$current), $str);
    $str = empty($Result[$i]['CADASTRAL_IMAGE_ORDER'])? '-' : $Result[$i]['CADASTRAL_IMAGE_ORDER'];
    $sheet->setCellValue('J'.(6+$current), $str);
    $str = empty($Result[$i]['SURVEYDOCTYPE_NAME'])? '-' : $Result[$i]['SURVEYDOCTYPE_NAME'];
    $sheet->setCellValue('K'.(6+$current), $str);
    $str = empty($Result[$i]['IMGPATH'])? '-' : $Result[$i]['IMGPATH'];
    $sheet->setCellValue('L'.(6+$current), $str);

    $str = empty($Result[$i]['SHEETCODE_1'])? '' : $Result[$i]['SHEETCODE_1'];
    $str .= '-';
    $str .= empty($Result[$i]['BOX_NO_1'])? '' : $Result[$i]['BOX_NO_1'];
    $sheet->setCellValue('M'.(6+$current), $str);
    $str = empty($Result[$i]['CADASTRAL_NO_1'])? '-' : $Result[$i]['CADASTRAL_NO_1'];
    $sheet->setCellValue('N'.(6+$current), $str);
    $str = empty($Result[$i]['NUMOFSURVEY_QTY_1'])? '-' : $Result[$i]['NUMOFSURVEY_QTY_1'];
    $sheet->setCellValue('O'.(6+$current), $str);
    $str = empty($Result[$i]['AMPHUR_NAME_1'])? '-' : $Result[$i]['AMPHUR_NAME_1'];
    $sheet->setCellValue('P'.(6+$current), $str);
    $str = empty($Result[$i]['TAMBOL_NAME_1'])? '-' : $Result[$i]['TAMBOL_NAME_1'];
    $sheet->setCellValue('Q'.(6+$current), $str);
    $str = empty($Result[$i]['TYPEOFSURVEY_NAME_1'])? '-' : $Result[$i]['TYPEOFSURVEY_NAME_1'];
    $sheet->setCellValue('R'.(6+$current), $str);
    $str = empty($Result[$i]['SURVEY_DATE_1'])? '-' : convertDtm($Result[$i]['SURVEY_DATE_1']);
    $sheet->setCellValue('S'.(6+$current), $str);
    $str = empty($Result[$i]['PRINTPLATE_TYPE_SEQ_1'])? 'ไม่สามารถแยกประเภทเอกสารได้' : getPrintplateTypeName($Result[$i]['PRINTPLATE_TYPE_SEQ_1']);
    $sheet->setCellValue('T'.(6+$current), $str);
    $str = empty($Result[$i]['CADASTRAL_IMAGE_ORDER_1'])? '-' : $Result[$i]['CADASTRAL_IMAGE_ORDER_1'];
    $sheet->setCellValue('U'.(6+$current), $str);
    $str = empty($Result[$i]['SURVEYDOCTYPE_NAME_1'])? '-' : $Result[$i]['SURVEYDOCTYPE_NAME_1'];
    $sheet->setCellValue('V'.(6+$current), $str);
    $str = empty($Result[$i]['IMGPATH_1'])? '-' : $Result[$i]['IMGPATH_1'];
    $sheet->setCellValue('W'.(6+$current), $str);
    if($_REQUEST['type'] == 'e'){
        if(empty($Result[$i]['CADASTRAL_IMAGE_SEQ'])){
            $problemDesc .= 'ไม่มีข้อมูลในโครงการพัฒน์ฯ 1';
        } else if(empty($Result[$i]['CADASTRAL_IMAGE_SEQ_1'])){
            $problemDesc .= 'ไม่มีข้อมูลในโครงการพัฒน์ฯ 2';
        } else {
            if($Result[$i]['SHEETCODE'] != $Result[$i]['SHEETCODE_1']
                || $Result[$i]['BOX_NO'] != $Result[$i]['BOX_NO_1']){
                    $problemDesc .= "หมายเลขแฟ้มไม่ตรงกัน\n";
            }
            if($Result[$i]['CADASTRAL_NO'] != $Result[$i]['CADASTRAL_NO_1']) $problemDesc .= "เลขที่ต้นร่างไม่ตรงกัน\n";
            if($Result[$i]['NUMOFSURVEY_QTY'] != $Result[$i]['NUMOFSURVEY_QTY_1']) $problemDesc .= "ครั้งที่รังวัดไม่ตรงกัน\n";
            if($Result[$i]['AMPHUR_NAME'] != $Result[$i]['AMPHUR_NAME_1']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
            if($Result[$i]['TAMBOL_NAME'] != $Result[$i]['TAMBOL_NAME_1']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
            if($Result[$i]['TYPEOFSURVEY_NAME'] != $Result[$i]['TYPEOFSURVEY_NAME_1']) $problemDesc .= "ประเภทการรังวัดไม่ตรงกัน\n";
            if($Result[$i]['SURVEY_DATE'] != $Result[$i]['SURVEY_DATE_1']) $problemDesc .= "วันที่รังวัดไม่ตรงกัน\n";
            if($Result[$i]['PRINTPLATE_TYPE_SEQ'] != $Result[$i]['PRINTPLATE_TYPE_SEQ_1']) $problemDesc .= "ประเภทเอกสารสิทธิไม่ตรงกัน\n";
            if($Result[$i]['CADASTRAL_IMAGE_ORDER'] != $Result[$i]['CADASTRAL_IMAGE_ORDER_1']) $problemDesc .= "ลำดับภาพต้นร่างไม่ตรงกัน\n";
            if($Result[$i]['SURVEYDOCTYPE_NAME'] != $Result[$i]['SURVEYDOCTYPE_NAME_1']) $problemDesc .= "ภาพลักษณ์ต้นร่างไม่ตรงกัน\n";
            if($Result[$i]['IMGPATH'] != $Result[$i]['IMGPATH_1']) $problemDesc .= "Path ภาพลักษณ์ต้นร่างไม่ตรงกัน\n";
        }
        $sheet->setCellValue('X'.(6+$current), rtrim($problemDesc));
        $problemDesc = "";
    }
    $current += 1;
}

$sheet->mergeCells('A1:'.$finCol.'1');
$sheet->setCellValue('A1','จำนวนทั้งหมด: '.number_format($current));

// DATA

$sheet->getStyle('A2:'.$finCol.($current + 5))->applyFromArray($styleArray);

$sheet->getStyle('A2:'.$finCol.'5')
    ->getFont()
    ->setBold(true);
foreach(range('A',$finCol) as $columnID) {
    $sheet->getColumnDimension($columnID)
        ->setAutoSize(true);
}

$maxWidth = 40;
foreach ($spreadsheet->getAllSheets() as $sheet) {
    $sheet->calculateColumnWidths();
    foreach ($sheet->getColumnDimensions() as $colDim) {
        if (!$colDim->getAutoSize()) {
            continue;
        }
        $colWidth = $colDim->getWidth();
        if ($colWidth > $maxWidth) {
            $colDim->setAutoSize(false);
            $colDim->setWidth($maxWidth);
        }
    }
}

foreach(range('B','D') as $columnID) {
    $sheet->getColumnDimension($columnID)
        ->setAutoSize(false);
    $sheet->getColumnDimension($columnID)
        ->setWidth(15);
}
$sheet->getColumnDimension('J')
        ->setAutoSize(false);
$sheet->getColumnDimension('J')
        ->setWidth(15);

foreach(range('M','O') as $columnID) {
    $sheet->getColumnDimension($columnID)
        ->setAutoSize(false);
    $sheet->getColumnDimension($columnID)
        ->setWidth(15);
}
$sheet->getColumnDimension('U')
        ->setAutoSize(false);
$sheet->getColumnDimension('U')
        ->setWidth(15);

if(count($Result)>0){
    $sheet->getStyle('A6:W' . (count($Result)+5))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle('A6:W' . (count($Result)+5))
        ->getAlignment()
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    $sheet->getStyle('A6:'.$finCol . (count($Result)+5))
            ->getAlignment()
            ->setWrapText(true);
}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
