<?php
require '../plugins/vendor/autoload.php';
include 'func.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$count_temp = 0;
$problemDesc = "";
$bn = $_REQUEST['branchName'];


if($_REQUEST['printplateType']==4){
    $finCol = $_REQUEST['type'] =='s'? 'O' : 'P';
} else if($_REQUEST['printplateType']==10 || $_REQUEST['printplateType']==11 || $_REQUEST['printplateType']==8){
    $finCol = $_REQUEST['type'] =='s'? 'S' : 'T';
} else {
    $finCol = $_REQUEST['type'] =='s'? 'Q' : 'R';
}

$title = 'รายการข้อมูลภาพลักษณ์เอกสารสิทธิ ('.getPrintplateTypeName($_REQUEST['printplateType']).')';
if($_REQUEST['type'] =='e') $title .= ' ที่แตกต่างกัน';
$fileName = date('y/m/d').'-'.$title.'-'.$bn;


$sheet->mergeCells('A2:'.$finCol.'2');
$sheet->setCellValue('A2',$title);

$sheet->mergeCells('A3:'.$finCol.'3');
$sheet->setCellValue('A3',$bn);

$sheet->mergeCells('A4:I4');
$sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
$sheet->mergeCells('J4:'.$finCol.'4');
$sheet->setCellValue('J4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

$sheet->setCellValue('A5','');
if($_REQUEST['printplateType']<4 || $_REQUEST['printplateType']==17){
    $sheet->setCellValue('B5','อำเภอ')
        ->setCellValue('C5','ตำบล')
        ->setCellValue('D5','เอกสารสิทธิ')
        ->setCellValue('E5','หน้าสำรวจ')
        ->setCellValue('F5','ลำดับภาพ')
        ->setCellValue('G5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('H5','ชื่อไฟล์ ที่ทำการถ่ายโอน')
        ->setCellValue('I5','Path ที่ทำการถ่ายโอน')

        ->setCellValue('J5', 'อำเภอ')
        ->setCellValue('K5','ตำบล')
        ->setCellValue('L5','เอกสารสิทธิ')
        ->setCellValue('M5','หน้าสำรวจ')
        ->setCellValue('N5','ลำดับภาพ')
        ->setCellValue('O5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('P5','ชื่อไฟล์ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('Q5','Path ภาพลักษณ์เอกสารสิทธิ');    
} else if($_REQUEST['printplateType']==10){
    $sheet->setCellValue('B5','อำเภอ')
        ->setCellValue('C5','ตำบล')
        ->setCellValue('D5','เลขห้อง')
        ->setCellValue('E5','เลขทะเบียนอาคารชุด')
        ->setCellValue('F5','ชื่ออาคารชุด')
        ->setCellValue('G5','ลำดับภาพ')
        ->setCellValue('H5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('I5','ชื่อไฟล์ ที่ทำการถ่ายโอน')
        ->setCellValue('J5','Path ที่ทำการถ่ายโอน')

        ->setCellValue('K5','อำเภอ')
        ->setCellValue('L5','ตำบล')
        ->setCellValue('M5','เลขห้อง')
        ->setCellValue('N5','เลขทะเบียนอาคารชุด')
        ->setCellValue('O5','ชื่ออาคารชุด')
        ->setCellValue('P5','ลำดับภาพ')
        ->setCellValue('Q5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('R5','ชื่อไฟล์ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('S5','Path ภาพลักษณ์เอกสารสิทธิ'); 
} else if ($_REQUEST['printplateType']==11){
    $sheet->setCellValue('B5','อำเภอ')
        ->setCellValue('C5','ตำบล')
        ->setCellValue('D5','หมู่ที่')
        ->setCellValue('E5','บ้านเลขที่')
        ->setCellValue('F5','รหัสประจำบ้าน')
        ->setCellValue('G5','ลำดับภาพ')
        ->setCellValue('H5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('I5','ชื่อไฟล์ ที่ทำการถ่ายโอน')
        ->setCellValue('J5','Path ที่ทำการถ่ายโอน')

        ->setCellValue('K5','อำเภอ')
        ->setCellValue('L5','ตำบล')
        ->setCellValue('M5','หมู่ที่')
        ->setCellValue('N5','บ้านเลขที่')
        ->setCellValue('O5','รหัสประจำบ้าน')
        ->setCellValue('P5','ลำดับภาพ')
        ->setCellValue('Q5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('R5','ชื่อไฟล์ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('S5','Path ภาพลักษณ์เอกสารสิทธิ'); 
} else if ($_REQUEST['printplateType']==13){
    $sheet->setCellValue('B5','อำเภอ')
        ->setCellValue('C5','ตำบล')
        ->setCellValue('D5','เลขทะเบียนอาคารชุด')
        ->setCellValue('E5','ชื่ออาคารชุด')
        ->setCellValue('F5','ลำดับภาพ')
        ->setCellValue('G5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('H5','ชื่อไฟล์ ที่ทำการถ่ายโอน')
        ->setCellValue('I5','Path ที่ทำการถ่ายโอน')

        ->setCellValue('J5', 'อำเภอ')
        ->setCellValue('K5','ตำบล')
        ->setCellValue('L5','เลขทะเบียนอาคารชุด')
        ->setCellValue('M5','ชื่ออาคารชุด')
        ->setCellValue('N5','ลำดับภาพ')
        ->setCellValue('O5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('P5','ชื่อไฟล์ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('Q5','Path ภาพลักษณ์เอกสารสิทธิ'); 
} else if ($_REQUEST['printplateType']==8){
    $sheet->setCellValue('B5','อำเภอ')
        ->setCellValue('C5','ตำบล')
        ->setCellValue('D5','เลขเอกสารสิทธิ')
        ->setCellValue('E5','ปีที่ออก')
        ->setCellValue('F5','ชื่อน.ส.ล.')
        ->setCellValue('G5','ลำดับภาพ')
        ->setCellValue('H5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('I5','ชื่อไฟล์ ที่ทำการถ่ายโอน')
        ->setCellValue('J5','Path ที่ทำการถ่ายโอน')

        ->setCellValue('K5','อำเภอ')
        ->setCellValue('L5','ตำบล')
        ->setCellValue('M5','เลขเอกสารสิทธิ')
        ->setCellValue('N5','ปีที่ออก')
        ->setCellValue('O5','ชื่อน.ส.ล.')
        ->setCellValue('P5','ลำดับภาพ')
        ->setCellValue('Q5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('R5','ชื่อไฟล์ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('S5','Path ภาพลักษณ์เอกสารสิทธิ');    
} else if ($_REQUEST['printplateType']==4) {
    $sheet->setCellValue('B5','อำเภอ')
        ->setCellValue('C5','ตำบล')
        ->setCellValue('D5','เอกสารสิทธิ')
        ->setCellValue('E5','ลำดับภาพ')
        ->setCellValue('F5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('G5','ชื่อไฟล์ ที่ทำการถ่ายโอน')
        ->setCellValue('H5','Path ที่ทำการถ่ายโอน')

        ->setCellValue('I5', 'อำเภอ')
        ->setCellValue('J5','ตำบล')
        ->setCellValue('K5','เอกสารสิทธิ')
        ->setCellValue('L5','ลำดับภาพ')
        ->setCellValue('M5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('N5','ชื่อไฟล์ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('O5','Path ภาพลักษณ์เอกสารสิทธิ');
} else {
    $sheet->setCellValue('B5','อำเภอ')
        ->setCellValue('C5','ตำบล')
        ->setCellValue('D5','หมู่ที่')
        ->setCellValue('E5','เอกสารสิทธิ')
        ->setCellValue('F5','ลำดับภาพ')
        ->setCellValue('G5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('H5','ชื่อไฟล์ ที่ทำการถ่ายโอน')
        ->setCellValue('I5','Path ที่ทำการถ่ายโอน')

        ->setCellValue('J5', 'อำเภอ')
        ->setCellValue('K5','ตำบล')
        ->setCellValue('L5','หมู่ที่')
        ->setCellValue('M5','เอกสารสิทธิ')
        ->setCellValue('N5','ลำดับภาพ')
        ->setCellValue('O5','ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('P5','ชื่อไฟล์ภาพลักษณ์เอกสารสิทธิ')
        ->setCellValue('Q5','Path ภาพลักษณ์เอกสารสิทธิ');
}
if($_REQUEST['type']=='e') $sheet->setCellValue($finCol.'5', 'หมายเหตุ');
    
$sheet->getStyle('A2:'.$finCol.'5')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

// DATA
include 'queryImageDiff.php';

for($i=0; $i<count($Result); $i++){
    $str = '';
    $sheet->setCellValue('A'.(6+$current), ($current+1));

    $str = empty($Result[$i]['AMPHUR_NAME'])? '-' : $Result[$i]['AMPHUR_NAME'];
    $sheet->setCellValue('B'.(6+$current), $str);
    $str = empty($Result[$i]['TAMBOL_NAME'])? '-' : $Result[$i]['TAMBOL_NAME'];
    $sheet->setCellValue('C'.(6+$current),$str);

    if($_REQUEST['printplateType']<4 || $_REQUEST['printplateType']==17){
        $str = empty($Result[$i]['NO'])? '-' : $Result[$i]['NO'];
        $sheet->setCellValue('D'.(6+$current), $str);
        $str = empty($Result[$i]['SURVEY'])? '-' : $Result[$i]['SURVEY'];
        $sheet->setCellValue('E'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_ORDER'])? '-' : $Result[$i]['IMAGE_ORDER'];
        $sheet->setCellValue('F'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_PNAME'])? '-' : $Result[$i]['IMAGE_PNAME'];
        $sheet->setCellValue('G'.(6+$current), $str);
        $str = empty($Result[$i]['FNAME'])? '-' : $Result[$i]['FNAME'];
        $sheet->setCellValue('H'.(6+$current), $str);
        $str = empty($Result[$i]['FURL'])? '-' : $Result[$i]['FURL'];
        $sheet->setCellValue('I'.(6+$current), $str);

        $str = empty($Result[$i]['AMPHUR_NAME_1'])? '-' : $Result[$i]['AMPHUR_NAME_1'];
        $sheet->setCellValue('J'.(6+$current), $str);
        $str = empty($Result[$i]['TAMBOL_NAME_1'])? '-' : $Result[$i]['TAMBOL_NAME_1'];
        $sheet->setCellValue('K'.(6+$current),$str);
        $str = empty($Result[$i]['NO_1'])? '-' : $Result[$i]['NO_1'];
        $sheet->setCellValue('L'.(6+$current), $str);
        $str = empty($Result[$i]['SURVEY_1'])? '-' : $Result[$i]['SURVEY_1'];
        $sheet->setCellValue('M'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_ORDER_1'])? '-' : $Result[$i]['IMAGE_ORDER_1'];
        $sheet->setCellValue('N'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_PNAME_1'])? '-' : $Result[$i]['IMAGE_PNAME_1'];
        $sheet->setCellValue('O'.(6+$current), $str);
        $str = empty($Result[$i]['FNAME_1'])? '-' : $Result[$i]['FNAME_1'];
        $sheet->setCellValue('P'.(6+$current), $str);
        $str = empty($Result[$i]['FURL_1'])? '-' : $Result[$i]['FURL_1'];
        $sheet->setCellValue('Q'.(6+$current), $str);

        if($_REQUEST['type']=='e'){
            if(empty($Result[$i]['IMAGE_SEQ'])){
                $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
            } else if (empty($Result[$i]['IMAGE_SEQ_1'])){
                $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
            } else {
                if($Result[$i]['AMPHUR_NAME'] != $Result[$i]['AMPHUR_NAME_1']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                if($Result[$i]['TAMBOL_NAME'] != $Result[$i]['TAMBOL_NAME_1']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                if($Result[$i]['NO'] != $Result[$i]['NO_1']) $problemDesc .= "เลขเอกสารสิทธิไม่ตรงกัน\n";
                if($Result[$i]['SURVEY'] != $Result[$i]['SURVEY_1']) $problemDesc .= "เลขหน้าสำรวจไม่ตรงกัน\n";
                if($Result[$i]['IMAGE_ORDER'] != $Result[$i]['IMAGE_ORDER_1']) $problemDesc .= "ลำดับภาพลักษณ์ไม่ตรงกัน\n";
                if($Result[$i]['IMAGE_PNAME'] != $Result[$i]['IMAGE_PNAME_1']) $problemDesc .= "ภาพลักษณ์เอกสารสิทธิไม่ตรงกัน\n";
                if($Result[$i]['FNAME'] != $Result[$i]['FNAME_1']) $problemDesc .= "ชื่อไฟล์ภาพลักษณ์ไม่ตรงกัน\n";
                if($Result[$i]['FURL'] != $Result[$i]['FURL_1']) $problemDesc .= "Path ภาพลักษณ์ไม่ตรงกัน\n";
            }
            $sheet->setCellValue($finCol.(6+$current), rtrim($problemDesc));
            $problemDesc = '';
        }
    } else if ($_REQUEST['printplateType']==10){
        $str = empty($Result[$i]['NO'])? '-' : $Result[$i]['NO'];
        $sheet->setCellValue('D'.(6+$current), $str);
        $str = empty($Result[$i]['CONDO_ID'])? '' : $Result[$i]['CONDO_ID'];
        $str .= '/';
        $str .= empty($Result[$i]['YEAR'])? '' : $Result[$i]['YEAR'];
        $sheet->setCellValue('E'.(6+$current), $str);
        $str = empty($Result[$i]['NAME'])? '-' : $Result[$i]['NAME'];
        $sheet->setCellValue('F'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_ORDER'])? '-' : $Result[$i]['IMAGE_ORDER'];
        $sheet->setCellValue('G'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_PNAME'])? '-' : $Result[$i]['IMAGE_PNAME'];
        $sheet->setCellValue('H'.(6+$current), $str);
        $str = empty($Result[$i]['FNAME'])? '-' : $Result[$i]['FNAME'];
        $sheet->setCellValue('I'.(6+$current), $str);
        $str = empty($Result[$i]['FURL'])? '-' : $Result[$i]['FURL'];
        $sheet->setCellValue('J'.(6+$current), $str);

        $str = empty($Result[$i]['AMPHUR_NAME_1'])? '-' : $Result[$i]['AMPHUR_NAME_1'];
        $sheet->setCellValue('K'.(6+$current), $str);
        $str = empty($Result[$i]['TAMBOL_NAME_1'])? '-' : $Result[$i]['TAMBOL_NAME_1'];
        $sheet->setCellValue('L'.(6+$current),$str);
        $str = empty($Result[$i]['NO_1'])? '-' : $Result[$i]['NO_1'];
        $sheet->setCellValue('M'.(6+$current), $str);
        $str = empty($Result[$i]['CONDO_ID_1'])? '' : $Result[$i]['CONDO_ID_1'];
        $str .= '/';
        $str .= empty($Result[$i]['YEAR_1'])? '' : $Result[$i]['YEAR_1'];
        $sheet->setCellValue('N'.(6+$current), $str);
        $str = empty($Result[$i]['NAME_1'])? '-' : $Result[$i]['NAME_1'];
        $sheet->setCellValue('O'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_ORDER_1'])? '-' : $Result[$i]['IMAGE_ORDER_1'];
        $sheet->setCellValue('P'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_PNAME_1'])? '-' : $Result[$i]['IMAGE_PNAME_1'];
        $sheet->setCellValue('Q'.(6+$current), $str);
        $str = empty($Result[$i]['FNAME_1'])? '-' : $Result[$i]['FNAME_1'];
        $sheet->setCellValue('R'.(6+$current), $str);
        $str = empty($Result[$i]['FURL_1'])? '-' : $Result[$i]['FURL_1'];
        $sheet->setCellValue('S'.(6+$current), $str);

        if($_REQUEST['type']=='e'){
            if(empty($Result[$i]['IMAGE_SEQ'])){
                $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
            } else if (empty($Result[$i]['IMAGE_SEQ_1'])){
                $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
            } else {
                if($Result[$i]['AMPHUR_NAME'] != $Result[$i]['AMPHUR_NAME_1']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                if($Result[$i]['TAMBOL_NAME'] != $Result[$i]['TAMBOL_NAME_1']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                if($Result[$i]['NO'] != $Result[$i]['NO_1']) $problemDesc .= "เลขห้องชุดไม่ตรงกัน\n";
                if($Result[$i]['CONDO_ID'] != $Result[$i]['CONDO_ID']
                    || $Result[$i]['YEAR'] != $Result[$i]['YEAR']) 
                        $problemDesc .= "เลขทะเบียนอาคารขุดไม่ตรงกัน\n";
                if($Result[$i]['NAME'] != $Result[$i]['NAME_1']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                if($Result[$i]['IMAGE_ORDER'] != $Result[$i]['IMAGE_ORDER_1']) $problemDesc .= "ลำดับภาพลักษณ์ไม่ตรงกัน\n";
                if($Result[$i]['IMAGE_PNAME'] != $Result[$i]['IMAGE_PNAME_1']) $problemDesc .= "ภาพลักษณ์เอกสารสิทธิไม่ตรงกัน\n";
                if($Result[$i]['FNAME'] != $Result[$i]['FNAME_1']) $problemDesc .= "ชื่อไฟล์ภาพลักษณ์ไม่ตรงกัน\n";
                if($Result[$i]['FURL'] != $Result[$i]['FURL_1']) $problemDesc .= "Path ภาพลักษณ์ไม่ตรงกัน\n";
            }
            $sheet->setCellValue($finCol.(6+$current), rtrim($problemDesc));
            $problemDesc = '';
        }
    } else if($_REQUEST['printplateType']==11){
        //NO DATa
    } else if($_REQUEST['printplateType']==13){
        //NO DATa
    } else if($_REQUEST['printplateType']==8){
        $str = empty($Result[$i]['NO'])? '-' : $Result[$i]['NO'];
        $sheet->setCellValue('D'.(6+$current), $str);
        $str = empty($Result[$i]['YEAR'])? '-' : $Result[$i]['YEAR'];
        $sheet->setCellValue('E'.(6+$current), $str);
        $str = empty($Result[$i]['NAME'])? '-' : $Result[$i]['NAME'];
        $sheet->setCellValue('F'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_ORDER'])? '-' : $Result[$i]['IMAGE_ORDER'];
        $sheet->setCellValue('G'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_PNAME'])? '-' : $Result[$i]['IMAGE_PNAME'];
        $sheet->setCellValue('H'.(6+$current), $str);
        $str = empty($Result[$i]['FNAME'])? '-' : $Result[$i]['FNAME'];
        $sheet->setCellValue('I'.(6+$current), $str);
        $str = empty($Result[$i]['FURL'])? '-' : $Result[$i]['FURL'];
        $sheet->setCellValue('J'.(6+$current), $str);

        $str = empty($Result[$i]['AMPHUR_NAME_1'])? '-' : $Result[$i]['AMPHUR_NAME_1'];
        $sheet->setCellValue('K'.(6+$current), $str);
        $str = empty($Result[$i]['TAMBOL_NAME_1'])? '-' : $Result[$i]['TAMBOL_NAME_1'];
        $sheet->setCellValue('L'.(6+$current),$str);
        $str = empty($Result[$i]['NO_1'])? '-' : $Result[$i]['NO_1'];
        $sheet->setCellValue('M'.(6+$current), $str);
        $str = empty($Result[$i]['YEAR_1'])? '-' : $Result[$i]['YEAR_1'];
        $sheet->setCellValue('N'.(6+$current), $str);
        $str = empty($Result[$i]['NAME_1'])? '-' : $Result[$i]['NAME_1'];
        $sheet->setCellValue('O'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_ORDER_1'])? '-' : $Result[$i]['IMAGE_ORDER_1'];
        $sheet->setCellValue('P'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_PNAME_1'])? '-' : $Result[$i]['IMAGE_PNAME_1'];
        $sheet->setCellValue('Q'.(6+$current), $str);
        $str = empty($Result[$i]['FNAME_1'])? '-' : $Result[$i]['FNAME_1'];
        $sheet->setCellValue('R'.(6+$current), $str);
        $str = empty($Result[$i]['FURL_1'])? '-' : $Result[$i]['FURL_1'];
        $sheet->setCellValue('S'.(6+$current), $str);

        if($_REQUEST['type']=='e'){
            if(empty($Result[$i]['IMAGE_SEQ'])){
                $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
            } else if (empty($Result[$i]['IMAGE_SEQ_1'])){
                $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
            } else {
                if($Result[$i]['AMPHUR_NAME'] != $Result[$i]['AMPHUR_NAME_1']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                if($Result[$i]['TAMBOL_NAME'] != $Result[$i]['TAMBOL_NAME_1']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                if($Result[$i]['NO'] != $Result[$i]['NO_1']) $problemDesc .= "เลขเอกสารสิทธิไม่ตรงกัน\n";
                if($Result[$i]['YEAR'] != $Result[$i]['YEAR']) $problemDesc .= "ปีที่ออกไม่ตรงกัน\n";
                if($Result[$i]['NAME'] != $Result[$i]['NAME_1']) $problemDesc .= "ชื่อเอกสารสิทธิไม่ตรงกัน\n";
                if($Result[$i]['IMAGE_ORDER'] != $Result[$i]['IMAGE_ORDER_1']) $problemDesc .= "ลำดับภาพลักษณ์ไม่ตรงกัน\n";
                if($Result[$i]['IMAGE_PNAME'] != $Result[$i]['IMAGE_PNAME_1']) $problemDesc .= "ภาพลักษณ์เอกสารสิทธิไม่ตรงกัน\n";
                if($Result[$i]['FNAME'] != $Result[$i]['FNAME_1']) $problemDesc .= "ชื่อไฟล์ภาพลักษณ์ไม่ตรงกัน\n";
                if($Result[$i]['FURL'] != $Result[$i]['FURL_1']) $problemDesc .= "Path ภาพลักษณ์ไม่ตรงกัน\n";
            }
            $sheet->setCellValue($finCol.(6+$current), rtrim($problemDesc));
            $problemDesc = '';
        }
    } else if($_REQUEST['printplateType']==4){
        $str = empty($Result[$i]['NO'])? '-' : $Result[$i]['NO'];
        $sheet->setCellValue('D'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_ORDER'])? '-' : $Result[$i]['IMAGE_ORDER'];
        $sheet->setCellValue('E'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_PNAME'])? '-' : $Result[$i]['IMAGE_PNAME'];
        $sheet->setCellValue('F'.(6+$current), $str);
        $str = empty($Result[$i]['FNAME'])? '-' : $Result[$i]['FNAME'];
        $sheet->setCellValue('G'.(6+$current), $str);
        $str = empty($Result[$i]['FURL'])? '-' : $Result[$i]['FURL'];
        $sheet->setCellValue('H'.(6+$current), $str);

        $str = empty($Result[$i]['AMPHUR_NAME_1'])? '-' : $Result[$i]['AMPHUR_NAME_1'];
        $sheet->setCellValue('I'.(6+$current), $str);
        $str = empty($Result[$i]['TAMBOL_NAME_1'])? '-' : $Result[$i]['TAMBOL_NAME_1'];
        $sheet->setCellValue('J'.(6+$current),$str);
        $str = empty($Result[$i]['NO_1'])? '-' : $Result[$i]['NO_1'];
        $sheet->setCellValue('K'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_ORDER_1'])? '-' : $Result[$i]['IMAGE_ORDER_1'];
        $sheet->setCellValue('L'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_PNAME_1'])? '-' : $Result[$i]['IMAGE_PNAME_1'];
        $sheet->setCellValue('M'.(6+$current), $str);
        $str = empty($Result[$i]['FNAME_1'])? '-' : $Result[$i]['FNAME_1'];
        $sheet->setCellValue('N'.(6+$current), $str);
        $str = empty($Result[$i]['FURL_1'])? '-' : $Result[$i]['FURL_1'];
        $sheet->setCellValue('O'.(6+$current), $str);

        if($_REQUEST['type']=='e'){
            if(empty($Result[$i]['IMAGE_SEQ'])){
                $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
            } else if (empty($Result[$i]['IMAGE_SEQ_1'])){
                $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
            } else {
                if($Result[$i]['AMPHUR_NAME'] != $Result[$i]['AMPHUR_NAME_1']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                if($Result[$i]['TAMBOL_NAME'] != $Result[$i]['TAMBOL_NAME_1']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                if($Result[$i]['NO'] != $Result[$i]['NO_1']) $problemDesc .= "เลขเอกสารสิทธิไม่ตรงกัน\n";
                if($Result[$i]['IMAGE_ORDER'] != $Result[$i]['IMAGE_ORDER_1']) $problemDesc .= "ลำดับภาพลักษณ์ไม่ตรงกัน\n";
                if($Result[$i]['IMAGE_PNAME'] != $Result[$i]['IMAGE_PNAME_1']) $problemDesc .= "ภาพลักษณ์เอกสารสิทธิไม่ตรงกัน\n";
                if($Result[$i]['FNAME'] != $Result[$i]['FNAME_1']) $problemDesc .= "ชื่อไฟล์ภาพลักษณ์ไม่ตรงกัน\n";
                if($Result[$i]['FURL'] != $Result[$i]['FURL_1']) $problemDesc .= "Path ภาพลักษณ์ไม่ตรงกัน\n";
            }
            $sheet->setCellValue($finCol.(6+$current), rtrim($problemDesc));
            $problemDesc = '';
        }
    } else {
        $str = empty($Result[$i]['MOO'])? '-' : $Result[$i]['MOO'];
        $sheet->setCellValue('D'.(6+$current), $str);
        $str = empty($Result[$i]['NO'])? '-' : $Result[$i]['NO'];
        $sheet->setCellValue('E'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_ORDER'])? '-' : $Result[$i]['IMAGE_ORDER'];
        $sheet->setCellValue('F'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_PNAME'])? '-' : $Result[$i]['IMAGE_PNAME'];
        $sheet->setCellValue('G'.(6+$current), $str);
        $str = empty($Result[$i]['FNAME'])? '-' : $Result[$i]['FNAME'];
        $sheet->setCellValue('H'.(6+$current), $str);
        $str = empty($Result[$i]['FURL'])? '-' : $Result[$i]['FURL'];
        $sheet->setCellValue('I'.(6+$current), $str);

        $str = empty($Result[$i]['AMPHUR_NAME_1'])? '-' : $Result[$i]['AMPHUR_NAME_1'];
        $sheet->setCellValue('J'.(6+$current), $str);
        $str = empty($Result[$i]['TAMBOL_NAME_1'])? '-' : $Result[$i]['TAMBOL_NAME_1'];
        $sheet->setCellValue('K'.(6+$current),$str);
        $str = empty($Result[$i]['MOO_1'])? '-' : $Result[$i]['MOO_1'];
        $sheet->setCellValue('L'.(6+$current), $str);
        $str = empty($Result[$i]['NO_1'])? '-' : $Result[$i]['NO_1'];
        $sheet->setCellValue('M'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_ORDER_1'])? '-' : $Result[$i]['IMAGE_ORDER_1'];
        $sheet->setCellValue('N'.(6+$current), $str);
        $str = empty($Result[$i]['IMAGE_PNAME_1'])? '-' : $Result[$i]['IMAGE_PNAME_1'];
        $sheet->setCellValue('O'.(6+$current), $str);
        $str = empty($Result[$i]['FNAME_1'])? '-' : $Result[$i]['FNAME_1'];
        $sheet->setCellValue('P'.(6+$current), $str);
        $str = empty($Result[$i]['FURL_1'])? '-' : $Result[$i]['FURL_1'];
        $sheet->setCellValue('Q'.(6+$current), $str);

        if($_REQUEST['type']=='e'){
            if(empty($Result[$i]['IMAGE_SEQ'])){
                $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
            } else if (empty($Result[$i]['IMAGE_SEQ_1'])){
                $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
            } else {
                if($Result[$i]['AMPHUR_NAME'] != $Result[$i]['AMPHUR_NAME_1']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                if($Result[$i]['TAMBOL_NAME'] != $Result[$i]['TAMBOL_NAME_1']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                if($Result[$i]['MOO'] != $Result[$i]['MOO_1']) $problemDesc .= "เลขหมู่ไม่ตรงกัน\n";
                if($Result[$i]['NO'] != $Result[$i]['NO_1']) $problemDesc .= "เลขเอกสารสิทธิไม่ตรงกัน\n";
                if($Result[$i]['IMAGE_ORDER'] != $Result[$i]['IMAGE_ORDER_1']) $problemDesc .= "ลำดับภาพลักษณ์ไม่ตรงกัน\n";
                if($Result[$i]['IMAGE_PNAME'] != $Result[$i]['IMAGE_PNAME_1']) $problemDesc .= "ภาพลักษณ์เอกสารสิทธิไม่ตรงกัน\n";
                if($Result[$i]['FNAME'] != $Result[$i]['FNAME_1']) $problemDesc .= "ชื่อไฟล์ภาพลักษณ์ไม่ตรงกัน\n";
                if($Result[$i]['FURL'] != $Result[$i]['FURL_1']) $problemDesc .= "Path ภาพลักษณ์ไม่ตรงกัน\n";
            }
            $sheet->setCellValue($finCol.(6+$current), rtrim($problemDesc));
            $problemDesc = '';
        }
    }

    $current += 1;
}


$sheet->mergeCells('A1:'.$finCol.'1');
$sheet->setCellValue('A1','จำนวนทั้งหมด: '.number_format($current));

// DATA

$sheet->getStyle('A2:'.$finCol.($current + 5))->applyFromArray($styleArray);

$sheet->getStyle('A2:'.$finCol.'5')
    ->getFont()
    ->setBold(true);
foreach(range('A',$finCol) as $columnID) {
    $sheet->getColumnDimension($columnID)
        ->setAutoSize(true);
}

$maxWidth = 50;
foreach ($spreadsheet->getAllSheets() as $sheet) {
    $sheet->calculateColumnWidths();
    foreach ($sheet->getColumnDimensions() as $colDim) {
        if (!$colDim->getAutoSize()) {
            continue;
        }
        $colWidth = $colDim->getWidth();
        if ($colWidth > $maxWidth) {
            $colDim->setAutoSize(false);
            $colDim->setWidth($maxWidth);
        }
    }
}

if($_REQUEST['printplateType']<4 || $_REQUEST['printplateType']==17){
    $sheet->getColumnDimension('F')
            ->setAutoSize(false);
    $sheet->getColumnDimension('F')
            ->setWidth(15);
    
    $sheet->getColumnDimension('N')
            ->setAutoSize(false);
    $sheet->getColumnDimension('N')
            ->setWidth(15);
} else if ($_REQUEST['printplateType']==10){
    $sheet->getColumnDimension('G')
            ->setAutoSize(false);
    $sheet->getColumnDimension('G')
            ->setWidth(15);
    
    $sheet->getColumnDimension('P')
            ->setAutoSize(false);
    $sheet->getColumnDimension('P')
            ->setWidth(15);

} else if ($_REQUEST['printplateType']==11){
    $sheet->getColumnDimension('G')
            ->setAutoSize(false);
    $sheet->getColumnDimension('G')
            ->setWidth(15);
    
    $sheet->getColumnDimension('P')
            ->setAutoSize(false);
    $sheet->getColumnDimension('P')
            ->setWidth(15);

    $sheet->getColumnDimension('D')
            ->setAutoSize(false);
    $sheet->getColumnDimension('D')
            ->setWidth(10);
    
    $sheet->getColumnDimension('M')
            ->setAutoSize(false);
    $sheet->getColumnDimension('M')
            ->setWidth(10);
}


if(count($Result)>0){
    $sheet->getStyle('A6:W' . (count($Result)+5))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle('A6:W' . (count($Result)+5))
        ->getAlignment()
        ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
    $sheet->getStyle('A6:'.$finCol . (count($Result)+5))
            ->getAlignment()
            ->setWrapText(true);
}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
