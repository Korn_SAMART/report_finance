<?php
require '../plugins/vendor/autoload.php';
include 'func.php';
include 'queryMapdoldiff2.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$temp = $_REQUEST['temp'];
$type = $_REQUEST['type'];
$bn = $_REQUEST['branchName'];


if($type=='s'){
    $spreadsheet->getActivesheet()
        ->setTitle('ข้อมูลตรงกัน');
    $spreadsheet->createSheet(1)
        ->setTitle('ข้อมูลไม่ตรงกัน');

    for($n=0; $n<2; $n++){
        
        $current = 0;
        $count_temp = 0;
        $problemDesc = "";
        $sheet = $spreadsheet->setActiveSheetIndex($n);

        $finCol = $type =='s'? 'O' : 'P';
        $title = 'รายการข้อมูลรูปแปลงที่ดิน';
        $title .= $temp==0? "ชั้นเผยแพร่" : "ชั้นรอจดทะเบียน";
        $title .= ' ('.getPrintplateTypeName($_REQUEST['printplateType']).')';
        $fileName = date('y/m/d').'-'.$title.'-'.$bn;
        if($type =='e') $title .= ' ที่แตกต่างกัน';

        $sheet->mergeCells('A2:'.$finCol.'2');
        $sheet->setCellValue('A2',$title);
    
        $sheet->mergeCells('A3:'.$finCol.'3');
        $sheet->setCellValue('A3',$bn);
    
        $sheet->mergeCells('A4:H4');
        $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('I4:'.$finCol.'4');
        $sheet->setCellValue('I4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
    
        $sheet->setCellValue('A5','')
            ->setCellValue('B5','ลำดับรูปแปลง')
            ->setCellValue('C5','หมายเลขระวาง')
            ->setCellValue('D5','แผนที่')
            ->setCellValue('E5','มาตราส่วน')
            ->setCellValue('F5','เลขที่ดิน')
            ->setCellValue('G5','โซน')
            ->setCellValue('H5','ระวาง')
    
            ->setCellValue('I5','ลำดับรูปแปลง')
            ->setCellValue('J5','หมายเลขระวาง')
            ->setCellValue('K5','แผนที่')
            ->setCellValue('L5','มาตราส่วน')
            ->setCellValue('M5','เลขที่ดิน')
            ->setCellValue('N5','โซน')
            ->setCellValue('O5','ระวาง');
        if($type =='e') $sheet->setCellValue('P5','หมายเหตุ');
            
        $sheet->getStyle('A2:'.$finCol.'5')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    
        // DATA
        $Result = getData($_REQUEST['landoffice'],$_REQUEST['printplateType'],$type,$temp);
    
        for($i=0; $i<count($Result); $i++){
            $str = '';
            $sheet->setCellValue('A'.(6+$current), ($current+1));
    
            $str = empty($Result[$i]['MAP_PARCEL_SEQ'])? '-' : $Result[$i]['MAP_PARCEL_SEQ'];
            $sheet->setCellValue('B'.(6+$current), $str);
            $str = empty($Result[$i]['UTMMAP1'])? '-' : $Result[$i]['UTMMAP1'];
            $str .= empty($Result[$i]['UTMMAP2'])? '-' : ' '.numberToRomanRepresentation($Result[$i]['UTMMAP2']);
            $str .= empty($Result[$i]['UTMMAP3'])? '' : ' '.$Result[$i]['UTMMAP3'];
            $sheet->setCellValue('C'.(6+$current), $str);
            $str = empty($Result[$i]['UTMMAP4'])? '-' : $Result[$i]['UTMMAP4'];
            $sheet->setCellValue('D'.(6+$current), $str);
            $str = empty($Result[$i]['UTMSCALE'])? '-' : $Result[$i]['UTMSCALE'];
            $sheet->setCellValue('E'.(6+$current), $str);
            $str = empty($Result[$i]['LAND_NO'])? '-' : $Result[$i]['LAND_NO'];
            $sheet->setCellValue('F'.(6+$current), $str);
            $str = empty($Result[$i]['MAP_PARCEL_SEQ'])? '-' : $Result[$i]['ZONE'];
            $sheet->setCellValue('G'.(6+$current), $str);
            $str = empty($Result[$i]['MAP_PARCEL_SEQ'])? '-' : $Result[$i]['RAVANG'];
            $sheet->setCellValue('H'.(6+$current), $str);
    
            $str = empty($Result[$i]['MAP_PARCEL_SEQ_1'])? '-' : $Result[$i]['MAP_PARCEL_SEQ_1'];
            $sheet->setCellValue('I'.(6+$current), $str);
            $str = empty($Result[$i]['UTMMAP1_1'])? '-' : $Result[$i]['UTMMAP1_1'];
            $str .= empty($Result[$i]['UTMMAP2_1'])? '-' : ' '.numberToRomanRepresentation($Result[$i]['UTMMAP2_1']);
            $str .= empty($Result[$i]['UTMMAP3_1'])? '' : ' '.$Result[$i]['UTMMAP3_1'];
            $sheet->setCellValue('J'.(6+$current), $str);
            $str = empty($Result[$i]['UTMMAP4_1'])? '-' : $Result[$i]['UTMMAP4_1'];
            $sheet->setCellValue('K'.(6+$current), $str);
            $str = empty($Result[$i]['UTMSCALE_1'])? '-' : $Result[$i]['UTMSCALE_1'];
            $sheet->setCellValue('L'.(6+$current), $str);
            $str = empty($Result[$i]['LAND_NO_1'])? '-' : $Result[$i]['LAND_NO_1'];
            $sheet->setCellValue('M'.(6+$current), $str);
            $sheet->setCellValue('N'.(6+$current), $Result[$i]['ZONE']);
            $sheet->setCellValue('O'.(6+$current), $Result[$i]['RAVANG']);
    
            if($type == 'e'){
                if(empty($Result[$i]['MAP_PARCEL_SEQ'])){
                    $sheet->setCellValue('B'.(6+$current), NULL);
                    $problemDesc .= 'ไม่มีข้อมูลในโครงการพัฒน์ฯ 1';
                } else if(empty($Result[$i]['MAP_PARCEL_SEQ_1'])){
                    $sheet->setCellValue('I'.(6+$current), NULL);
                    $problemDesc .= 'ไม่มีข้อมูลในโครงการพัฒน์ฯ 2';
                } else {
                    if($Result[$i]['UTMMAP1'] != $Result[$i]['UTMMAP1_1']
                        || $Result[$i]['UTMMAP2'] != $Result[$i]['UTMMAP2_1']
                        || $Result[$i]['UTMMAP3'] != $Result[$i]['UTMMAP3_1']){
                            $problemDesc .= "หมายเลขระวางไม่ตรงกัน\n";
                    }
                    if($Result[$i]['UTMMAP4'] != $Result[$i]['UTMMAP4_1']) $problemDesc .= "แผน่ที่ระวางไม่ตรงกัน\n";
                    if($Result[$i]['UTMSCALE'] != $Result[$i]['UTMSCALE_1']) $problemDesc .= "มาตราส่วนระวางไม่ตรงกัน\n";
                    if($Result[$i]['LAND_NO'] != $Result[$i]['LAND_NO_1']) $problemDesc .= "เลขที่ดินไม่ตรงกัน\n";
                }
                $sheet->setCellValue('P'.(6+$current), rtrim($problemDesc));
                $problemDesc = "";
            }
            $current += 1;
        }
    
        $sheet->mergeCells('A1:'.$finCol.'1');
        $sheet->setCellValue('A1','จำนวนทั้งหมด: '.number_format($current));
    
        // DATA
    
        $sheet->getStyle('A1:'.$finCol.($current + 5))->applyFromArray($styleArray);
    
        $sheet->getStyle('A2:'.$finCol.'5')
            ->getFont()
            ->setBold(true);
        foreach(range('A',$finCol) as $columnID) {
            $sheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
    
        if(count($Result)>0){
            $sheet->getStyle('B6:B' . (count($Result)+5))
                ->getNumberFormat()
                ->setFormatCode('#');
            $sheet->getStyle('I6:I' . (count($Result)+5))
                ->getNumberFormat()
                ->setFormatCode('#'); 
    
            $sheet->getStyle('A6:O' . (count($Result)+5))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A6:O' . (count($Result)+5))
                ->getAlignment()
                ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
            $sheet->getStyle('A6:'.$finCol . (count($Result)+5))
                    ->getAlignment()
                    ->setWrapText(true);
            
        }
        $type = 'e';
    }
    $sheet = $spreadsheet->setActiveSheetIndex(0);
} else {
    $current = 0;
    $count_temp = 0;
    $problemDesc = "";

    $finCol = 'I';
    $title = 'รายการข้อมูลรูปแปลงที่ดิน';
    $title .= $temp==0? "ชั้นเผยแพร่" : "ชั้นรอจดทะเบียน";
    $title .= ' ('.getPrintplateTypeName($_REQUEST['printplateType']).') ที่ถ่ายโอนไม่สำเร็จ';
    $fileName = date('y/m/d').'-'.$title.'-'.$bn;

    $sheet->mergeCells('A2:'.$finCol.'2');
    $sheet->setCellValue('A2',$title);

    $sheet->mergeCells('A3:'.$finCol.'3');
    $sheet->setCellValue('A3',$bn);

    $sheet->setCellValue('A4','')
        ->setCellValue('B4','ลำดับรูปแปลง')
        ->setCellValue('C4','หมายเลขระวาง')
        ->setCellValue('D4','แผนที่')
        ->setCellValue('E4','มาตราส่วน')
        ->setCellValue('F4','เลขที่ดิน')
        ->setCellValue('G4','โซน')
        ->setCellValue('H4','ระวาง')
        ->setCellValue('I4','หมายเหตุ');
        
    $sheet->getStyle('A2:'.$finCol.'4')
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

    // DATA
    $Result = getData($_REQUEST['landoffice'],$_REQUEST['printplateType'],$type,$temp);

    for($i=0; $i<count($Result); $i++){
        $str = '';
        $sheet->setCellValue('A'.(5+$current), ($current+1));

        $str = empty($Result[$i]['MAP_PARCEL_SEQ'])? '-' : $Result[$i]['MAP_PARCEL_SEQ'];
        $sheet->setCellValue('B'.(5+$current), $str);
        $str = empty($Result[$i]['UTMMAP1'])? '-' : $Result[$i]['UTMMAP1'];
        $str .= empty($Result[$i]['UTMMAP2'])? '-' : ' '.numberToRomanRepresentation($Result[$i]['UTMMAP2']);
        $str .= empty($Result[$i]['UTMMAP3'])? '' : ' '.$Result[$i]['UTMMAP3'];
        $sheet->setCellValue('C'.(5+$current), $str);
        $str = empty($Result[$i]['UTMMAP4'])? '-' : $Result[$i]['UTMMAP4'];
        $sheet->setCellValue('D'.(5+$current), $str);
        $str = empty($Result[$i]['UTMSCALE'])? '-' : $Result[$i]['UTMSCALE'];
        $sheet->setCellValue('E'.(5+$current), $str);
        $str = empty($Result[$i]['LAND_NO'])? '-' : $Result[$i]['LAND_NO'];
        $sheet->setCellValue('F'.(5+$current), $str);
        $str = empty($Result[$i]['MAP_PARCEL_SEQ'])? '-' : $Result[$i]['ZONE'];
        $sheet->setCellValue('G'.(5+$current), $str);
        $str = empty($Result[$i]['MAP_PARCEL_SEQ'])? '-' : $Result[$i]['RAVANG'];
        $sheet->setCellValue('H'.(5+$current), $str);
        $str = empty($Result[$i]['LOG1_MIGRATE_NOTE'])? '-' : $Result[$i]['LOG1_MIGRATE_NOTE'];
        $sheet->setCellValue('I'.(5+$current), $str);
        $current += 1;
    }
    $sheet->mergeCells('A1:'.$finCol.'1');
    $sheet->setCellValue('A1','จำนวนทั้งหมด: '.number_format($current));

    // DATA

    $sheet->getStyle('A1:'.$finCol.($current + 4))->applyFromArray($styleArray);

    $sheet->getStyle('A2:'.$finCol.'4')
        ->getFont()
        ->setBold(true);
    foreach(range('A',$finCol) as $columnID) {
        $sheet->getColumnDimension($columnID)
            ->setAutoSize(true);
    }

    if(count($Result)>0){
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getNumberFormat()
            ->setFormatCode('#');

        $sheet->getStyle('A5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A5:I' . (count($Result)+4))
            ->getAlignment()
            ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
        $sheet->getStyle('A5:'.$finCol . (count($Result)+4))
                ->getAlignment()
                ->setWrapText(true);
        
    }
}



$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
