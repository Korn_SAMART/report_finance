<?php
require '../../plugins/vendor/autoload.php';
include('../func.php');


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];



$btd59no = !isset($_POST['btd59no']) ? '' : $_POST['btd59no'];
$btd59year = !isset($_POST['btd59year']) ? '' : $_POST['btd59year'];
$datestart = !isset($_POST['datestart'])? '' : $_POST['datestart'];
$dateend = !isset($_POST['dateend'])? '' : $_POST['dateend'];
$receipcate = !isset($_POST['receipcate'])? '' : $_POST['receipcate'];

$rcptstart = !isset($_POST['rcptstart'])? '' : $_POST['rcptstart'];
$rcptend = !isset($_POST['rcptend'])? '' : $_POST['rcptend'];
$selectrcpt = !isset($_POST['selectrcpt'])? '' : $_POST['selectrcpt'];
$selectsts = !isset($_POST['selectsts'])? '' : $_POST['selectsts'];


$Result = array();
$landoffice = !isset($_GET['landoffice'])? '' : $_GET['landoffice'];
$branchName = !isset($_GET['branchName'])? '' : $_GET['branchName'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();


if($sts == 'e') include 'queryFinDataError.php';
if($sts == 's') include 'queryFinData.php';

while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}


switch ($checknum){
    case '6' : 
        if($sts == 'e'){
            $spreadsheet->getActivesheet()
                        ->setTitle('ถ่ายโอนไม่สำเร็จ');
            $current = 0;
            $problemDesc = "";

            $sheet->mergeCells('A1:K1');
            $sheet->mergeCells('A2:K2');
            $sheet->setCellValue('A2', 'รายการข้อมูล บ.ท.ด.59 ถ่ายโอนไม่สำเร็จ');
            $bn = 'รายการข้อมูล บ.ท.ด.59 ถ่ายโอนไม่สำเร็จ';
            $fileName = date("Y/m/d") . '-รายการข้อมูล บ.ท.ด.59 ถ่ายโอนไม่สำเร็จ-'.$branchName ;

            $sheet->mergeCells('A3:K3');
            $sheet->setCellValue('A3', $branchName);

            $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขที่ บ.ท.ด.59')
            ->setCellValue('C4', 'วันที่วาง บ.ท.ด.59')       
            ->setCellValue('D4', 'ผู้วางเงินมัดจำ')
            ->setCellValue('E4', 'ประเภทคำขอ')
            ->setCellValue('F4', 'เลขที่เอกสารสิทธิ')       
            ->setCellValue('G4', 'หน้าสำรวจ')
            ->setCellValue('H4', 'ตำบล')
            ->setCellValue('I4', 'อำเภอ')
            ->setCellValue('J4', 'เงินที่วาง')
            ->setCellValue('K4', 'เงินคงเหลือ')

            ->getStyle('A2:K4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            for ($i=0; $i<count($Result); $i++) {
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));

                    $str = empty($Result[$i]['BOOK59_NO'])? '-' : $Result[$i]['BOOK59_NO'];
                    $sheet->setCellValue('B'.(5+$current), $str);
                    $str = empty($Result[$i]['BOOK59_DATE'])? '-' : $Result[$i]['BOOK59_DATE'];
                    $sheet->setCellValue('C'.(5+$current), $str);
                    $str = empty($Result[$i]['PAYER_NAME'])? '-' : $Result[$i]['PAYER_NAME'];
                    $sheet->setCellValue('D'.(5+$current), $str);
                    $str = empty($Result[$i]['RECEIPT_CATEGORY'])? '-' : $Result[$i]['RECEIPT_CATEGORY'];
                    $sheet->setCellValue('E'.(5+$current), $str);
                    $str = empty($Result[$i]['PARCEL_NO'])? '-' : $Result[$i]['PARCEL_NO'];
                    $sheet->setCellValue('F'.(5+$current), $str);
                    $str = empty($Result[$i]['SURVEY_NO'])? '-' : $Result[$i]['SURVEY_NO'];
                    $sheet->setCellValue('G'.(5+$current), $str);
                    $str = empty($Result[$i]['ADDR_TAMBOL'])? '-' : $Result[$i]['ADDR_TAMBOL'];
                    $sheet->setCellValue('H'.(5+$current), $str);
                    $str = empty($Result[$i]['ADDR_AMPHUR'])? '-' : $Result[$i]['ADDR_AMPHUR'];
                    $sheet->setCellValue('I'.(5+$current), $str);
                    $str = empty($Result[$i]['STR_MNY'])? '-' : $Result[$i]['STR_MNY'];
                    $sheet->setCellValue('J'.(5+$current), $str);
                    $str = empty($Result[$i]['REMAIN_MNY'])? '-' : $Result[$i]['REMAIN_MNY'];
                    $sheet->setCellValue('K'.(5+$current), $str);

                    $current += 1;            
            }

            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($current));
            $sheet->getStyle('A1:K'.($current + 4))
                ->applyFromArray($styleArray);
            $sheet->getStyle('A2:K4')->getFont()->setBold(true);

            $sheet->getColumnDimension('A')->setAutoSize(true);
            $sheet->getColumnDimension('B')->setAutoSize(true);
            $sheet->getColumnDimension('C')->setAutoSize(true);
            $sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(50);
            $sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(50);
            $sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(30);
            $sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(30);
            $sheet->getColumnDimension('H')->setAutoSize(true);
            $sheet->getColumnDimension('I')->setAutoSize(true);
            $sheet->getColumnDimension('J')->setAutoSize(true);
            $sheet->getColumnDimension('K')->setAutoSize(true);

            if(count($Result) > 0){
                $sheet->getStyle('A5:A' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B5:B' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('C5:C' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D5:D' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('E5:E' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F5:F' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('G5:G' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('H5:H' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('I5:I' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('J5:J' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('K5:K' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

                $sheet->getStyle('A5:A' . (count($Result)+4))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0'); 
                $sheet->getStyle('J5:J' . (count($Result)+4))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0.00'); 
                $sheet->getStyle('K5:K' . (count($Result)+4))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0.00'); 
            }
        }

        if($sts == 's'){
            $spreadsheet->getActivesheet()
                ->setTitle('ข้อมูลตรงกัน');
            $spreadsheet->createSheet(1)
                ->setTitle('ข้อมูลไม่ตรงกัน');
            for($n=0; $n<2; $n++){
                $current = 0;
                $problemDesc = "";
                $count_temp1 =0;
                $count_temp2 =0;
                $sheet = $spreadsheet->setActiveSheetIndex($n);
                $sheet->mergeCells('A1:V1');
                $sheet->mergeCells('A2:V2');

                $title = 'รายการข้อมูล บ.ท.ด.59';

                $sheet->setCellValue('A2', $title);
                $bn = $title;
                $fileName = date("Y/m/d").'-'.$title.'-'.$branchName;

                $sheet->mergeCells('A3:V3');
                $sheet->setCellValue('A3', $branchName);
        
                $sheet->mergeCells('A4:K4');
                $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
                $sheet->mergeCells('L4:V4');
                $sheet->setCellValue('L4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
        
                $sheet->setCellValue('A5', '')
                    ->setCellValue('B5', 'เลขที่ บ.ท.ด.59')
                    ->setCellValue('C5', 'วันที่วาง บ.ท.ด.59')       
                    ->setCellValue('D5', 'ผู้วางเงินมัดจำ')
                    ->setCellValue('E5', 'ประเภทคำขอ')
                    ->setCellValue('F5', 'เลขที่เอกสารสิทธิ')       
                    ->setCellValue('G5', 'หน้าสำรวจ')
                    ->setCellValue('H5', 'ตำบล')
                    ->setCellValue('I5', 'อำเภอ')
                    ->setCellValue('J5', 'เงินที่วาง')
                    ->setCellValue('K5', 'เงินคงเหลือ')
        
                    ->setCellValue('L5', 'เลขที่ บ.ท.ด.59')
                    ->setCellValue('M5', 'วันที่วาง บ.ท.ด.59')       
                    ->setCellValue('N5', 'ผู้วางเงินมัดจำ')
                    ->setCellValue('O5', 'ประเภทคำขอ')
                    ->setCellValue('P5', 'เลขที่เอกสารสิทธิ')       
                    ->setCellValue('Q5', 'หน้าสำรวจ')
                    ->setCellValue('R5', 'ตำบล')
                    ->setCellValue('S5', 'อำเภอ')
                    ->setCellValue('T5', 'เงินที่วาง')
                    ->setCellValue('U5', 'เงินคงเหลือ')
                    ->setCellValue('V5', 'หมายเหตุ')
                    ->getStyle('A2:V5')
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    
                    for ($i = 0; $i < count($Result); $i++) {
                        if($n == 0 ){
                            if($Result[$i]['BOOK59_NO_P1'] == $Result[$i]['BOOK59_NO_P2'] && $Result[$i]['BOOK59_DATE_P1'] == $Result[$i]['BOOK59_DATE_P2'] &&
                                $Result[$i]['PAYER_NAME_P1'] == $Result[$i]['PAYER_NAME_P2'] && $Result[$i]['RECEIPT_CATEGORY_P1'] == $Result[$i]['RECEIPT_CATEGORY_P2'] &&
                                $Result[$i]['PARCEL_NO_P1'] == $Result[$i]['PARCEL_NO_P2'] && $Result[$i]['SURVEY_NO_P1'] == $Result[$i]['SURVEY_NO_P2'] &&
                                $Result[$i]['ADDR_TAMBOL_P1'] == $Result[$i]['ADDR_TAMBOL_P2'] && $Result[$i]['ADDR_AMPHUR_P1'] == $Result[$i]['ADDR_AMPHUR_P2'] &&
                                $Result[$i]['STR_MNY_P1'] == $Result[$i]['STR_MNY_P2'] && $Result[$i]['REMAIN_MNY_P1'] == $Result[$i]['REMAIN_MNY_P2'] ){
            
                                $sheet->setCellValue('A'. (6 + $current), ($current + 1));
                                $sheet->setCellValue('B' . (6 + $current), $Result[$i]['BOOK59_NO_P1']);
                                $sheet->setCellValue('C' . (6 + $current), $Result[$i]['BOOK59_DATE_P1']);
                                $sheet->setCellValue('D' . (6 + $current), $Result[$i]['PAYER_NAME_P1']);
                                $sheet->setCellValue('E' . (6 + $current), $Result[$i]['RECEIPT_CATEGORY_P1']);
                                $sheet->setCellValue('F' . (6 + $current), $Result[$i]['PARCEL_NO_P1']);
                                $sheet->setCellValue('G' . (6 + $current), $Result[$i]['SURVEY_NO_P1']);
                                $sheet->setCellValue('H' . (6 + $current), $Result[$i]['ADDR_TAMBOL_P1']);
                                $sheet->setCellValue('I' . (6 + $current), $Result[$i]['ADDR_AMPHUR_P1']);
                                $sheet->setCellValue('J' . (6 + $current), $Result[$i]['STR_MNY_P1']);
                                $sheet->setCellValue('K' . (6 + $current), $Result[$i]['REMAIN_MNY_P1']);
            
                                $sheet->setCellValue('L' . (6 + $current), $Result[$i]['BOOK59_NO_P2']);
                                $sheet->setCellValue('M' . (6 + $current), $Result[$i]['BOOK59_DATE_P2']);
                                $sheet->setCellValue('N' . (6 + $current), $Result[$i]['PAYER_NAME_P2']);
                                $sheet->setCellValue('O' . (6 + $current), $Result[$i]['RECEIPT_CATEGORY_P2']);
                                $sheet->setCellValue('P' . (6 + $current), $Result[$i]['PARCEL_NO_P2']);
                                $sheet->setCellValue('Q' . (6 + $current), $Result[$i]['SURVEY_NO_P2']);
                                $sheet->setCellValue('R' . (6 + $current), $Result[$i]['ADDR_TAMBOL_P2']);
                                $sheet->setCellValue('S' . (6 + $current), $Result[$i]['ADDR_AMPHUR_P2']);
                                $sheet->setCellValue('T' . (6 + $current), $Result[$i]['STR_MNY_P2']);
                                $sheet->setCellValue('U' . (6 + $current), $Result[$i]['REMAIN_MNY_P2']);
            
                                $sheet->setCellValue('V' . (6 + $current), $problemDesc);
                                $current += 1;
                                $count_temp1 += 1;
                            }
                        }
                        if($n == 1){
                            if($Result[$i]['BOOK59_NO_P1'] != $Result[$i]['BOOK59_NO_P2'] || $Result[$i]['BOOK59_DATE_P1'] != $Result[$i]['BOOK59_DATE_P2'] ||
                                $Result[$i]['PAYER_NAME_P1'] != $Result[$i]['PAYER_NAME_P2'] || $Result[$i]['RECEIPT_CATEGORY_P1'] != $Result[$i]['RECEIPT_CATEGORY_P2'] ||
                                $Result[$i]['PARCEL_NO_P1'] != $Result[$i]['PARCEL_NO_P2'] || $Result[$i]['SURVEY_NO_P1'] != $Result[$i]['SURVEY_NO_P2'] ||
                                $Result[$i]['ADDR_TAMBOL_P1'] != $Result[$i]['ADDR_TAMBOL_P2'] || $Result[$i]['ADDR_AMPHUR_P1'] != $Result[$i]['ADDR_AMPHUR_P2'] ||
                                $Result[$i]['STR_MNY_P1'] != $Result[$i]['STR_MNY_P2'] || $Result[$i]['REMAIN_MNY_P1'] != $Result[$i]['REMAIN_MNY_P2'] ){

                                $sheet->setCellValue('A'. (6 + $current), ($current + 1));
                                $sheet->setCellValue('B' . (6 + $current), $Result[$i]['BOOK59_NO_P1']);
                                $sheet->setCellValue('C' . (6 + $current), $Result[$i]['BOOK59_DATE_P1']);
                                $sheet->setCellValue('D' . (6 + $current), $Result[$i]['PAYER_NAME_P1']);
                                $sheet->setCellValue('E' . (6 + $current), $Result[$i]['RECEIPT_CATEGORY_P1']);
                                $sheet->setCellValue('F' . (6 + $current), $Result[$i]['PARCEL_NO_P1']);
                                $sheet->setCellValue('G' . (6 + $current), $Result[$i]['SURVEY_NO_P1']);
                                $sheet->setCellValue('H' . (6 + $current), $Result[$i]['ADDR_TAMBOL_P1']);
                                $sheet->setCellValue('I' . (6 + $current), $Result[$i]['ADDR_AMPHUR_P1']);
                                $sheet->setCellValue('J' . (6 + $current), $Result[$i]['STR_MNY_P1']);
                                $sheet->setCellValue('K' . (6 + $current), $Result[$i]['REMAIN_MNY_P1']);

                                $sheet->setCellValue('L' . (6 + $current), $Result[$i]['BOOK59_NO_P2']);
                                $sheet->setCellValue('M' . (6 + $current), $Result[$i]['BOOK59_DATE_P2']);
                                $sheet->setCellValue('N' . (6 + $current), $Result[$i]['PAYER_NAME_P2']);
                                $sheet->setCellValue('O' . (6 + $current), $Result[$i]['RECEIPT_CATEGORY_P2']);
                                $sheet->setCellValue('P' . (6 + $current), $Result[$i]['PARCEL_NO_P2']);
                                $sheet->setCellValue('Q' . (6 + $current), $Result[$i]['SURVEY_NO_P2']);
                                $sheet->setCellValue('R' . (6 + $current), $Result[$i]['ADDR_TAMBOL_P2']);
                                $sheet->setCellValue('S' . (6 + $current), $Result[$i]['ADDR_AMPHUR_P2']);
                                $sheet->setCellValue('T' . (6 + $current), $Result[$i]['STR_MNY_P2']);
                                $sheet->setCellValue('V' . (6 + $current), $Result[$i]['REMAIN_MNY_P2']);

                                
                                if(empty($Result[$i]['BOOK59_SEQ_P1'])){
                                    $problemDesc  = $problemDesc . "ไม่มีข้อมูล บ.ท.ด.59 ในพัฒน์ฯ 1 ";

                                }else if(empty($Result[$i]['BOOK59_SEQ_P2'])){
                                    $problemDesc  = $problemDesc . "ไม่มีข้อมูล บ.ท.ด.59 ในพัฒน์ฯ 2 ";

                                }else{
                                    if($Result[$i]['BOOK59_NO_P1'] != $Result[$i]['BOOK59_NO_P2']) $problemDesc  = $problemDesc . "เลขที่ บ.ท.ด.59  ไม่ตรงกัน \n";
                                    if($Result[$i]['BOOK59_DATE_P1'] != $Result[$i]['BOOK59_DATE_P2']) $problemDesc  = $problemDesc . "วันที่วาง บ.ท.ด.59  ไม่ตรงกัน \n";
                                    if($Result[$i]['PAYER_NAME_P1'] != $Result[$i]['PAYER_NAME_P2']) $problemDesc  = $problemDesc . "ผู้วางเงินมัดจำ  ไม่ตรงกัน \n";
                                    if($Result[$i]['RECEIPT_CATEGORY_P1'] != $Result[$i]['RECEIPT_CATEGORY_P2']) $problemDesc  = $problemDesc . "ประเภทคำขอ  ไม่ตรงกัน \n";
                                    if($Result[$i]['PARCEL_NO_P1'] != $Result[$i]['PARCEL_NO_P2']) $problemDesc  = $problemDesc . "เลขที่เอกสารสิทธิ  ไม่ตรงกัน \n";
                                    if($Result[$i]['SURVEY_NO_P1'] != $Result[$i]['SURVEY_NO_P2']) $problemDesc  = $problemDesc . "หน้าสำรวจ  ไม่ตรงกัน \n";
                                    if($Result[$i]['ADDR_TAMBOL_P1'] != $Result[$i]['ADDR_TAMBOL_P2']) $problemDesc  = $problemDesc . "ตำบล  ไม่ตรงกัน \n";
                                    if($Result[$i]['ADDR_AMPHUR_P1'] != $Result[$i]['ADDR_AMPHUR_P2']) $problemDesc  = $problemDesc . "อำเภอ  ไม่ตรงกัน \n";
                                    if($Result[$i]['STR_MNY_P1'] != $Result[$i]['STR_MNY_P2']) $problemDesc  = $problemDesc . "เงินที่วาง  ไม่ตรงกัน \n";
                                    if($Result[$i]['REMAIN_MNY_P1'] != $Result[$i]['REMAIN_MNY_P2']) $problemDesc  = $problemDesc . "เงินคงเหลือ  ไม่ตรงกัน \n";
                                }
                                $sheet->setCellValue('V' . (6 + $current), $problemDesc);
                                $problemDesc = "";
                                $current += 1;
                                $count_temp2 += 1;
                            }
                        }
                    }
                $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '. number_format($current));
                $sheet->getStyle('A1:V'.($current + 5))
                      ->applyFromArray($styleArray);
                $sheet->getStyle('A2:V5')->getFont()->setBold(true);

                $sheet->getColumnDimension('A')->setAutoSize(true);
                $sheet->getColumnDimension('B')->setAutoSize(true);
                $sheet->getColumnDimension('C')->setAutoSize(true);
                $sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(50);
                $sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(50);
                $sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(30);
                $sheet->getColumnDimension('G')->setAutoSize(false)->setWidth(30);
                $sheet->getColumnDimension('H')->setAutoSize(true);
                $sheet->getColumnDimension('I')->setAutoSize(true);
                $sheet->getColumnDimension('J')->setAutoSize(true);
                $sheet->getColumnDimension('K')->setAutoSize(true);
                $sheet->getColumnDimension('L')->setAutoSize(true);
                $sheet->getColumnDimension('M')->setAutoSize(true);
                $sheet->getColumnDimension('N')->setAutoSize(false)->setWidth(50);
                $sheet->getColumnDimension('O')->setAutoSize(false)->setWidth(50);
                $sheet->getColumnDimension('P')->setAutoSize(false)->setWidth(30);
                $sheet->getColumnDimension('Q')->setAutoSize(false)->setWidth(30);
                $sheet->getColumnDimension('R')->setAutoSize(true);
                $sheet->getColumnDimension('S')->setAutoSize(true);
                $sheet->getColumnDimension('T')->setAutoSize(true);
                $sheet->getColumnDimension('U')->setAutoSize(true);
                $sheet->getColumnDimension('V')->setAutoSize(true);

                

                $sheet->getStyle('A6:A' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B6:B' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('C6:C' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D6:D' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('E6:E' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F6:F' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('G6:G' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('H6:H' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('I6:I' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('J6:J' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('K6:K' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('L6:L' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('M6:M' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('N6:N' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('O6:O' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('P6:P' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('Q6:Q' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('R6:R' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('S6:S' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('T6:T' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('U6:U' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('V6:V' . (count($Result)+5))
                    ->getAlignment()
                    ->setWrapText(true);

                $sheet->getStyle('A6:A' . (count($Result)+5))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0'); 
                $sheet->getStyle('J6:J' . (count($Result)+5))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0.00'); 
                $sheet->getStyle('K6:K' . (count($Result)+5))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0.00'); 
                $sheet->getStyle('T6:T' . (count($Result)+5))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0.00'); 
                $sheet->getStyle('U6:U' . (count($Result)+5))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0.00');   
            }
            $sheet = $spreadsheet->setActiveSheetIndex(0);
        }
    break;

    case '7' : 
        if($sts == 'e'){
            $spreadsheet->getActivesheet()
                        ->setTitle('ถ่ายโอนไม่สำเร็จ');
            $current = 0;
            $problemDesc = "";

            $sheet->mergeCells('A1:J1');
            $sheet->mergeCells('A2:J2');
            $sheet->setCellValue('A2', 'รายการข้อมูลใบเสร็จ ถ่ายโอนไม่สำเร็จ');
            $bn = 'รายการข้อมูลใบเสร็จ ถ่ายโอนไม่สำเร็จ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลใบเสร็จ ถ่ายโอนไม่สำเร็จ-'.$branchName ;

            $sheet->mergeCells('A3:J3');
            $sheet->setCellValue('A3', $branchName);

            $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขที่คิว')
            ->setCellValue('C4', 'เลขที่ใบสั่ง')       
            ->setCellValue('D4', 'ประเภทคำขอ')
            ->setCellValue('E4', 'ได้รับเงินจาก')
            ->setCellValue('F4', 'คู่สัญญา')       
            ->setCellValue('G4', 'จำนวนเงิน')
            ->setCellValue('H4', 'เลขที่ใบเสร็จ')
            ->setCellValue('I4', 'วันที่ใบเสร็จ')
            ->setCellValue('J4', 'สถานะ')

            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            for ($i=0; $i<count($Result); $i++) {
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));

                    empty($Result[i]['QUEUE_NO']) ? $sheet->setCellValue('B' . (5 + $current), NULL) : $sheet->setCellValue('B' . (5 + $current), $Result[$i]['QUEUE_NO']);
                    empty($Result[i]['ORDER_NO']) ? $sheet->setCellValue('C' . (5 + $current), NULL) : $sheet->setCellValue('C' . (5 + $current), $Result[$i]['ORDER_NO']);
                    empty($Result[i]['RCPT_CAT']) ? $sheet->setCellValue('D' . (5 + $current), NULL) : $sheet->setCellValue('D' . (5 + $current), $Result[$i]['RCPT_CAT']);
                    empty($Result[i]['PAYER_NAME']) ? $sheet->setCellValue('E' . (5 + $current), NULL) : $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PAYER_NAME']);
                    empty($Result[i]['PARTY_NAME']) ? $sheet->setCellValue('F' . (5 + $current), NULL) : $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PARTY_NAME']);
                    empty($Result[i]['RCPT_TOTAL_MNY']) ? $sheet->setCellValue('G' . (5 + $current), NULL) : $sheet->setCellValue('G' . (5 + $current), $Result[$i]['RCPT_TOTAL_MNY']);
                    empty($Result[i]['RECEIPT_NO']) ? $sheet->setCellValue('H' . (5 + $current), NULL) : $sheet->setCellValue('H' . (5 + $current), $Result[$i]['RECEIPT_NO']);
                    empty($Result[i]['RECEIPT_DTM']) ? $sheet->setCellValue('I' . (5 + $current), NULL) : $sheet->setCellValue('I' . (5 + $current), $Result[$i]['RECEIPT_DTM']);
                    empty($Result[i]['STATUS']) ? $sheet->setCellValue('J' . (5 + $current), NULL) : $sheet->setCellValue('J' . (5 + $current), $Result[$i]['STATUS']);

                    $current += 1;            
            }

            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($current));
            $sheet->getStyle('A1:J'.($current + 4))
                ->applyFromArray($styleArray);
            $sheet->getStyle('A2:J4')->getFont()->setBold(true);

            $sheet->getColumnDimension('A')->setAutoSize(true);
            $sheet->getColumnDimension('B')->setAutoSize(true);
            $sheet->getColumnDimension('C')->setAutoSize(true);
            $sheet->getColumnDimension('D')->setAutoSize(true);
            $sheet->getColumnDimension('E')->setAutoSize(true);
            $sheet->getColumnDimension('F')->setAutoSize(true);
            $sheet->getColumnDimension('G')->setAutoSize(true);
            $sheet->getColumnDimension('H')->setAutoSize(true);
            $sheet->getColumnDimension('I')->setAutoSize(true);
            $sheet->getColumnDimension('J')->setAutoSize(true);

            if(count($Result) > 0){
                $sheet->getStyle('A5:A' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B5:B' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('C5:C' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D5:D' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('E5:E' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F5:F' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('G5:G' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('H5:H' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('I5:I' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('J5:J' . (count($Result)+4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

            }
        }

        if($sts == 's'){
            $spreadsheet->getActivesheet()
                ->setTitle('ข้อมูลตรงกัน');
            $spreadsheet->createSheet(1)
                ->setTitle('ข้อมูลไม่ตรงกัน');
            for($n=0; $n<2; $n++){
                $current = 0;
                $problemDesc = "";
                $sheet = $spreadsheet->setActiveSheetIndex($n);
                $sheet->mergeCells('A1:T1');
                $sheet->mergeCells('A2:T2');

                $title = 'รายการข้อมูลใบเสร็จ';

                $sheet->setCellValue('A2', $title);
                $bn = $title;
                $fileName = date("Y/m/d").'-'.$title.'-'.$branchName;

                $sheet->mergeCells('A3:T3');
                $sheet->setCellValue('A3', $branchName);
        
                $sheet->mergeCells('A4:J4');
                $sheet->setCellValue('A4', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
                $sheet->mergeCells('K4:T4');
                $sheet->setCellValue('K4', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');
        
                $sheet->setCellValue('A5', '')
                ->setCellValue('B5', 'เลขที่คิว')
                ->setCellValue('C5', 'เลขที่ใบสั่ง')       
                ->setCellValue('D5', 'ประเภทคำขอ')
                ->setCellValue('E5', 'ได้รับเงินจาก')
                ->setCellValue('F5', 'คู่สัญญา')       
                ->setCellValue('G5', 'จำนวนเงิน')
                ->setCellValue('H5', 'เลขที่ใบเสร็จ')
                ->setCellValue('I5', 'วันที่ใบเสร็จ')
                ->setCellValue('J5', 'สถานะ')
    
                ->setCellValue('K5', 'เลขที่คิว')
                ->setCellValue('L5', 'เลขที่ใบสั่ง')       
                ->setCellValue('M5', 'ประเภทคำขอ')
                ->setCellValue('N5', 'ได้รับเงินจาก')
                ->setCellValue('O5', 'คู่สัญญา')       
                ->setCellValue('P5', 'จำนวนเงิน')
                ->setCellValue('Q5', 'เลขที่ใบเสร็จ')
                ->setCellValue('R5', 'วันที่ใบเสร็จ')
                ->setCellValue('S5', 'สถานะ')
    
                ->setCellValue('T5', 'หมายเหตุ')

                ->getStyle('A2:T5')
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    
                    for ($i = 0; $i < count($Result); $i++) {
                        if($n == 0 ){
                            if($Result[$i]['QUEUE_NO_P1'] == $Result[$i]['QUEUE_NO_P2'] && $Result[$i]['ORDER_NO_P1'] == $Result[$i]['ORDER_NO_P2'] &&
                                $Result[$i]['RCPT_CAT_P1'] == $Result[$i]['RCPT_CAT_P2'] && $Result[$i]['PAYER_NAME_P1'] == $Result[$i]['PAYER_NAME_P2'] &&
                                $Result[$i]['PARTY_NAME_P1'] == $Result[$i]['PARTY_NAME_P2'] && $Result[$i]['RCPT_TOTAL_MNY_P1'] == $Result[$i]['RCPT_TOTAL_MNY_P2'] &&
                                $Result[$i]['RECEIPT_NO_P1'] == $Result[$i]['RECEIPT_NO_P2'] && $Result[$i]['RECEIPT_DTM_P1'] == $Result[$i]['RECEIPT_DTM_P2'] &&
                                $Result[$i]['STATUS_P1'] == $Result[$i]['STATUS_P2']  ){

                                $sheet->setCellValue('A'. (6 + $current), ($current + 1));

                                $str = empty($Result[$i]['QUEUE_NO_P1'])? '-' : $Result[$i]['QUEUE_NO_P1'];
                                $sheet->setCellValue('B'.(6+$current), $str);
                                $str = empty($Result[$i]['ORDER_NO_P1'])? '-' : $Result[$i]['ORDER_NO_P1'];
                                $sheet->setCellValue('C'.(6+$current), $str);
                                $str = empty($Result[$i]['RCPT_CAT_P1'])? '-' : $Result[$i]['RCPT_CAT_P1'];
                                $sheet->setCellValue('D'.(6+$current), $str);
                                $str = empty($Result[$i]['PAYER_NAME_P1'])? '-' : $Result[$i]['PAYER_NAME_P1'];
                                $sheet->setCellValue('E'.(6+$current), $str);
                                $str = empty($Result[$i]['PARTY_NAME_P1'])? '-' : $Result[$i]['PARTY_NAME_P1'];
                                $sheet->setCellValue('F'.(6+$current), $str);
                                $str = empty($Result[$i]['RCPT_TOTAL_MNY_P1'])? '-' : $Result[$i]['RCPT_TOTAL_MNY_P1'];
                                $sheet->setCellValue('G'.(6+$current), $str);
                                $str = empty($Result[$i]['RECEIPT_NO_P1'])? '-' : $Result[$i]['RECEIPT_NO_P1'];
                                $sheet->setCellValue('H'.(6+$current), $str);
                                $str = empty($Result[$i]['RECEIPT_DTM_P1'])? '-' : convertDtm($Result[$i]['RECEIPT_DTM_P1']);
                                $sheet->setCellValue('I'.(6+$current), $str);
                                $str = empty($Result[$i]['STATUS_P1'])? '-' : $Result[$i]['STATUS_P1'];
                                $sheet->setCellValue('J'.(6+$current), $str);

                                $str = empty($Result[$i]['QUEUE_NO_P2'])? '-' : $Result[$i]['QUEUE_NO_P2'];
                                $sheet->setCellValue('K'.(6+$current), $str);
                                $str = empty($Result[$i]['ORDER_NO_P2'])? '-' : $Result[$i]['ORDER_NO_P2'];
                                $sheet->setCellValue('L'.(6+$current), $str);
                                $str = empty($Result[$i]['RCPT_CAT_P2'])? '-' : $Result[$i]['RCPT_CAT_P2'];
                                $sheet->setCellValue('M'.(6+$current), $str);
                                $str = empty($Result[$i]['PAYER_NAME_P2'])? '-' : $Result[$i]['PAYER_NAME_P2'];
                                $sheet->setCellValue('N'.(6+$current), $str);
                                $str = empty($Result[$i]['PARTY_NAME_P2'])? '-' : $Result[$i]['PARTY_NAME_P2'];
                                $sheet->setCellValue('O'.(6+$current), $str);
                                $str = empty($Result[$i]['RCPT_TOTAL_MNY_P2'])? '-' : $Result[$i]['RCPT_TOTAL_MNY_P2'];
                                $sheet->setCellValue('P'.(6+$current), $str);
                                $str = empty($Result[$i]['RECEIPT_NO_P2'])? '-' : $Result[$i]['RECEIPT_NO_P2'];
                                $sheet->setCellValue('Q'.(6+$current), $str);
                                $str = empty($Result[$i]['RECEIPT_DTM_P2'])? '-' : convertDtm($Result[$i]['RECEIPT_DTM_P2']);
                                $sheet->setCellValue('R'.(6+$current), $str);
                                $str = empty($Result[$i]['STATUS_P2'])? '-' : $Result[$i]['STATUS_P2'];
                                $sheet->setCellValue('S'.(6+$current), $str);

            
                                $sheet->setCellValue('T' . (6 + $current), $problemDesc);
                                $current += 1;
                            }
                        }
                        if($n == 1){
                            if($Result[$i]['QUEUE_NO_P1'] != $Result[$i]['QUEUE_NO_P2'] || $Result[$i]['ORDER_NO_P1'] != $Result[$i]['ORDER_NO_P2'] ||
                                $Result[$i]['RCPT_CAT_P1'] != $Result[$i]['RCPT_CAT_P2'] || $Result[$i]['PAYER_NAME_P1'] != $Result[$i]['PAYER_NAME_P2'] ||
                                $Result[$i]['PARTY_NAME_P1'] != $Result[$i]['PARTY_NAME_P2'] || $Result[$i]['RCPT_TOTAL_MNY_P1'] != $Result[$i]['RCPT_TOTAL_MNY_P2'] ||
                                $Result[$i]['RECEIPT_NO_P1'] != $Result[$i]['RECEIPT_NO_P2'] || $Result[$i]['RECEIPT_DTM_P1'] != $Result[$i]['RECEIPT_DTM_P2'] ||
                                $Result[$i]['STATUS_P1'] != $Result[$i]['STATUS_P2']  ){

                                $sheet->setCellValue('A'. (6 + $current), ($current + 1));
                                $str = empty($Result[$i]['QUEUE_NO_P1'])? '-' : $Result[$i]['QUEUE_NO_P1'];
                                $sheet->setCellValue('B'.(6+$current), $str);
                                $str = empty($Result[$i]['ORDER_NO_P1'])? '-' : $Result[$i]['ORDER_NO_P1'];
                                $sheet->setCellValue('C'.(6+$current), $str);
                                $str = empty($Result[$i]['RCPT_CAT_P1'])? '-' : $Result[$i]['RCPT_CAT_P1'];
                                $sheet->setCellValue('D'.(6+$current), $str);
                                $str = empty($Result[$i]['PAYER_NAME_P1'])? '-' : $Result[$i]['PAYER_NAME_P1'];
                                $sheet->setCellValue('E'.(6+$current), $str);
                                $str = empty($Result[$i]['PARTY_NAME_P1'])? '-' : $Result[$i]['PARTY_NAME_P1'];
                                $sheet->setCellValue('F'.(6+$current), $str);
                                $str = empty($Result[$i]['RCPT_TOTAL_MNY_P1'])? '-' : $Result[$i]['RCPT_TOTAL_MNY_P1'];
                                $sheet->setCellValue('G'.(6+$current), $str);
                                $str = empty($Result[$i]['RECEIPT_NO_P1'])? '-' : $Result[$i]['RECEIPT_NO_P1'];
                                $sheet->setCellValue('H'.(6+$current), $str);
                                $str = empty($Result[$i]['RECEIPT_DTM_P1'])? '-' : convertDtm($Result[$i]['RECEIPT_DTM_P1']);
                                $sheet->setCellValue('I'.(6+$current), $str);
                                $str = empty($Result[$i]['STATUS_P1'])? '-' : $Result[$i]['STATUS_P1'];
                                $sheet->setCellValue('J'.(6+$current), $str);

                                $str = empty($Result[$i]['QUEUE_NO_P2'])? '-' : $Result[$i]['QUEUE_NO_P2'];
                                $sheet->setCellValue('K'.(6+$current), $str);
                                $str = empty($Result[$i]['ORDER_NO_P2'])? '-' : $Result[$i]['ORDER_NO_P2'];
                                $sheet->setCellValue('L'.(6+$current), $str);
                                $str = empty($Result[$i]['RCPT_CAT_P2'])? '-' : $Result[$i]['RCPT_CAT_P2'];
                                $sheet->setCellValue('M'.(6+$current), $str);
                                $str = empty($Result[$i]['PAYER_NAME_P2'])? '-' : $Result[$i]['PAYER_NAME_P2'];
                                $sheet->setCellValue('N'.(6+$current), $str);
                                $str = empty($Result[$i]['PARTY_NAME_P2'])? '-' : $Result[$i]['PARTY_NAME_P2'];
                                $sheet->setCellValue('O'.(6+$current), $str);
                                $str = empty($Result[$i]['RCPT_TOTAL_MNY_P2'])? '-' : $Result[$i]['RCPT_TOTAL_MNY_P2'];
                                $sheet->setCellValue('P'.(6+$current), $str);
                                $str = empty($Result[$i]['RECEIPT_NO_P2'])? '-' : $Result[$i]['RECEIPT_NO_P2'];
                                $sheet->setCellValue('Q'.(6+$current), $str);
                                $str = empty($Result[$i]['RECEIPT_DTM_P2'])? '-' : convertDtm($Result[$i]['RECEIPT_DTM_P2']);
                                $sheet->setCellValue('R'.(6+$current), $str);
                                $str = empty($Result[$i]['STATUS_P2'])? '-' : $Result[$i]['STATUS_P2'];
                                $sheet->setCellValue('S'.(6+$current), $str);

                                if($Result[$i]['QUEUE_NO_P1'] != $Result[$i]['QUEUE_NO_P2']) $problemDesc  = $problemDesc . "เลขที่คิว ไม่ตรงกัน \n";
                                if($Result[$i]['ORDER_NO_P1'] != $Result[$i]['ORDER_NO_P2']) $problemDesc  = $problemDesc . "เลขที่ใบสั่ง ไม่ตรงกัน \n";
                                if($Result[$i]['RCPT_CAT_P1'] != $Result[$i]['RCPT_CAT_P2']) $problemDesc  = $problemDesc . "ประเภทคำขอ ไม่ตรงกัน \n";
                                if($Result[$i]['PAYER_NAME_P1'] != $Result[$i]['PAYER_NAME_P2']) $problemDesc  = $problemDesc . "ได้รับเงินจาก ไม่ตรงกัน \n";
                                if($Result[$i]['PARTY_NAME_P1'] != $Result[$i]['PARTY_NAME_P2']) $problemDesc  = $problemDesc . "คู้สัญญา ไม่ตรงกัน \n";
                                if($Result[$i]['RCPT_TOTAL_MNY_P1'] != $Result[$i]['RCPT_TOTAL_MNY_P2']) $problemDesc  = $problemDesc . "จำนวนเงิน ไม่ตรงกัน \n";
                                if($Result[$i]['RECEIPT_NO_P1'] != $Result[$i]['RECEIPT_NO_P2']) $problemDesc  = $problemDesc . "เลขที่ใบเสร็จ ไม่ตรงกัน \n";
                                if($Result[$i]['RECEIPT_DTM_P1'] != $Result[$i]['RECEIPT_DTM_P2']) $problemDesc  = $problemDesc . "วันที่ใบเสร็จ ไม่ตรงกัน \n";
                                if($Result[$i]['STATUS_P1'] != $Result[$i]['STATUS_P2']) $problemDesc  = $problemDesc . "สถานะ ไม่ตรงกัน \n";


                                $sheet->setCellValue('T' . (6 + $current), $problemDesc);
                                $problemDesc = "";
                                $current += 1;
                            }
                        }
                    }
                $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '. number_format($current));
                $sheet->getStyle('A1:T'.($current + 5))
                      ->applyFromArray($styleArray);
                $sheet->getStyle('A2:T5')->getFont()->setBold(true);

                $sheet->getColumnDimension('A')->setAutoSize(true);
                $sheet->getColumnDimension('B')->setAutoSize(true);
                $sheet->getColumnDimension('C')->setAutoSize(true);
                $sheet->getColumnDimension('D')->setAutoSize(false)->setWidth(50);
                $sheet->getColumnDimension('E')->setAutoSize(false)->setWidth(50);
                $sheet->getColumnDimension('F')->setAutoSize(false)->setWidth(50);
                $sheet->getColumnDimension('G')->setAutoSize(true);
                $sheet->getColumnDimension('H')->setAutoSize(true);
                $sheet->getColumnDimension('I')->setAutoSize(true);
                $sheet->getColumnDimension('J')->setAutoSize(true);
                $sheet->getColumnDimension('K')->setAutoSize(true);
                $sheet->getColumnDimension('L')->setAutoSize(true);
                $sheet->getColumnDimension('M')->setAutoSize(false)->setWidth(50);
                $sheet->getColumnDimension('N')->setAutoSize(false)->setWidth(50);
                $sheet->getColumnDimension('O')->setAutoSize(false)->setWidth(50);
                $sheet->getColumnDimension('P')->setAutoSize(true);
                $sheet->getColumnDimension('Q')->setAutoSize(true);
                $sheet->getColumnDimension('R')->setAutoSize(true);
                $sheet->getColumnDimension('S')->setAutoSize(true);
                $sheet->getColumnDimension('T')->setAutoSize(true);

                

                $sheet->getStyle('A6:A' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('B6:B' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('C6:C' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D6:D' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('E6:E' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('F6:F' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('G6:G' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('H6:H' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('I6:I' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('J6:J' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('K6:K' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('L6:L' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('M6:M' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('N6:N' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('O6:O' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->getStyle('P6:P' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $sheet->getStyle('Q6:Q' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('R6:R' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('S6:S' . (count($Result)+5))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('T6:T' . (count($Result)+5))
                    ->getAlignment()
                    ->setWrapText(true);


                $sheet->getStyle('A6:A' . (count($Result)+5))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0'); 
                $sheet->getStyle('P6:P' . (count($Result)+5))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0.00'); 
                $sheet->getStyle('G6:G' . (count($Result)+5))
                    ->getNumberFormat()
                    ->setFormatCode('#,##0.00'); 


            }
            $sheet = $spreadsheet->setActiveSheetIndex(0);
        }  
    break;




}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
