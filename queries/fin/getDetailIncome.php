<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';

    $landoffice = !isset($_POST['landoffice'])? '' : $_POST['landoffice'];
    $detailOrder = !isset($_POST['detailOrder'])? '' : $_POST['detailOrder'];
    $detailDate = !isset($_POST['detailDate'])? '' : $_POST['detailDate'];
    $detailRec = !isset($_POST['detailRec'])? '' : $_POST['detailRec'];

    $detailOrder_sql = $detailOrder == '' ? '' : " AND RCPT.ORDER_NO = :detailOrder"; 
    $detailDate_sql = $detailDate == '' ? '' : " AND RCPT.RECEIPT_DTM LIKE :detailDate "; 
    $detailRec_sql = $detailRec == '' ? '' : " AND RCPT.RECEIPT_NO = :detailRec  "; 

    $Result = array();
    
 
    $select = "WITH P1 AS (
                SELECT DISTINCT RCPT.ORDER_NO, RCPT.RECEIPT_CATEGORY
                    ,RCPTI.INCOME_MNY, RCPT.RECEIPT_NO, RCPT.RECEIPT_DTM, RCPT.RECEIPT_STS, IC.INCOME_NAME, IC.INCOME_ID
                    ,CASE EXC_FLAG 
                    WHEN '1' THEN 'ปกติ' 
                    WHEN '2' THEN 'ยกเว้น'
                    WHEN '3' THEN 'ต่างสำนักงาน' END EXC_FLAG
                FROM MGT1.TB_FIN_PAY_RCPT RCPT
                LEFT JOIN MGT1.TB_FIN_PAY_RCPT_PART_INCOME RCPTI
                    ON RCPTI.RECEIPT_SEQ = RCPT.RECEIPT_SEQ
                LEFT JOIN MGT1.TB_FIN_MAS_INCOME IC
                    ON RCPTI.INCOME_ID = IC.INCOME_ID
                WHERE  RCPT.LANDOFFICE_SEQ = :landoffice 
                ".$detailOrder_sql
                .$detailDate_sql
                .$detailRec_sql."
            ),
            P2 AS (
                    SELECT DISTINCT RCPT.ORDER_NO, RCPT.RECEIPT_CATEGORY
                    ,RCPTI.INCOME_MNY, RCPT.RECEIPT_NO, RCPT.RECEIPT_DTM, RCPT.RECEIPT_STS, IC.INCOME_NAME, IC.INCOME_ID 
                    ,CASE EXC_FLAG 
                    WHEN '1' THEN 'ปกติ' 
                    WHEN '2' THEN 'ยกเว้น'
                    WHEN '3' THEN 'ต่างสำนักงาน' END EXC_FLAG
                FROM FIN.TB_FIN_PAY_RCPT RCPT
                LEFT JOIN FIN.TB_FIN_PAY_RCPT_PART_INCOME RCPTI
                    ON RCPTI.RECEIPT_SEQ = RCPT.RECEIPT_SEQ
                LEFT JOIN FIN.TB_FIN_MAS_INCOME IC
                    ON RCPTI.INCOME_ID = IC.INCOME_ID
                WHERE  RCPT.LANDOFFICE_SEQ = :landoffice 
                ".$detailOrder_sql
                .$detailDate_sql
                .$detailRec_sql."
            )
            SELECT P1.INCOME_NAME AS INCOME_NAME_P1 ,  P2.INCOME_NAME AS INCOME_NAME_P2
                ,P1.INCOME_MNY AS INCOME_MNY_P1 ,  P2.INCOME_MNY AS INCOME_MNY_P2
                ,P1.EXC_FLAG AS EXC_FLAG_P1 ,  P2.EXC_FLAG AS EXC_FLAG_P2
            FROM P1
            FULL OUTER JOIN P2
                ON P1.INCOME_ID = P2.INCOME_ID
            ORDER BY P1.EXC_FLAG, P2.INCOME_NAME";
    
    // echo($select);
    $stid = oci_parse($conn, $select); 
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    if ($detailOrder != '') oci_bind_by_name($stid, ':detailOrder', $detailOrder);
    if ($detailDate != '') oci_bind_by_name($stid, ':detailDate', $detailDate);
    if ($detailRec != '') oci_bind_by_name($stid, ':detailRec', $detailRec);
    oci_execute($stid);


    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    
    
    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);


?>

