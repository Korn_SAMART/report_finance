<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';

    $landoffice = !isset($_POST['landoffice'])? '' : $_POST['landoffice'];
    $detailOrder = !isset($_POST['detailOrder'])? '' : $_POST['detailOrder'];
    $detailDate = !isset($_POST['detailDate'])? '' : $_POST['detailDate'];
    $detailRec = !isset($_POST['detailRec'])? '' : $_POST['detailRec'];

    $detailOrder_sql = $detailOrder == '' ? '' : " AND RCPT.ORDER_NO = :detailOrder"; 
    $detailDate_sql = $detailDate == '' ? '' : " AND RCPT.RECEIPT_DTM LIKE :detailDate "; 
    $detailRec_sql = $detailRec == '' ? '' : " AND RCPT.RECEIPT_NO = :detailRec  "; 

    $Result = array();
    
 
    $select = "WITH P1 AS ( 
                SELECT RCPT.RECEIPT_NO, RCPT.RECEIPT_DTM, RCPT.LANDOFFICE_NAME
                        , CASE RCPT.RECEIPT_TYPE 
                        WHEN 'R' THEN 'ใบสั่งรายได้แผ่นดิน'
                        WHEN 'W' THEN 'ใบสั่งรายได้แผ่นดิน'
                        WHEN 'S' THEN 'ใบสั่งเงินมัดจำรังวัด' END AS RECEIPT_TYPE
                        , CASE WHEN OW.BTD59_NO IS NOT NULL THEN OW.BTD59_NO ||'/' || OW.BUDGET_YEAR ELSE '-' END AS BTD59_NO
                        , RCPT.RECEIPT_GRP_TYPE
                        , REC.RCPT_CAT, REC.OPT_SHOW, RCPT.ORDER_NO ||' '|| RCPT.ORDER_DTM AS ORDER_NO
                        ,CASE WHEN RCPT.PAYER_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(RCPT.PAYER_TITLE, '...', RCPT.PAYER_FNAME)||' '|| RCPT.PAYER_LNAME)
                        ELSE RCPT.PAYER_TITLE||RCPT.PAYER_FNAME||' '||RCPT.PAYER_LNAME END AS PAYER_NAME

                        ,CASE WHEN REC.PARTY_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(REC.PARTY_TITLE, '...', REC.PARTY_FNAME)||' '|| REC.PARTY_LNAME)
                        ELSE REC.PARTY_TITLE||REC.PARTY_FNAME||' '||REC.PARTY_LNAME END AS PARTY_NAME
                        , REC.QUEUE_NO ||' '|| REC.QUEUE_DATE AS QUEUE_NO, REC.ASSET_MNY, REC.EVALUATE_MNY
                        , REC.PLATE_ABBR_NAME, REC.PARCEL_NO, REC.SHEET_NO, REC.LAND_NO, REC.SURVEY_NO, REC.MOO, REC.ADDR_TAMBOL, REC.ADDR_AMPHUR, REC.ADDR_PROVINCE
                        , RCPT.CANCEL_CAUSE_REM
                FROM MGT1.TB_FIN_PAY_RCPT RCPT
                LEFT JOIN MGT1.TB_FIN_ORDER_RECEIPT REC
                    ON REC.ORDER_NO = RCPT.ORDER_NO
                    AND REC.ORDER_DATE = RCPT.ORDER_DTM
                    AND REC.LANDOFFICE_SEQ = RCPT.LANDOFFICE_SEQ
                LEFT JOIN FIN.TB_FIN_ORDER_WITHDRAW OW
                    ON RCPT.ORDER_NO = OW.ORDER_NO
                    AND RCPT.ORDER_DTM = OW.ORDER_DATE
                    AND RCPT.LANDOFFICE_SEQ = OW.LANDOFFICE_SEQ
                WHERE  RCPT.LANDOFFICE_SEQ = :landoffice 
                    ".$detailOrder_sql
                    .$detailDate_sql
                    .$detailRec_sql."
                ),
                P2 AS (
                SELECT RCPT.RECEIPT_NO, RCPT.RECEIPT_DTM, RCPT.LANDOFFICE_NAME
                        , CASE RCPT.RECEIPT_TYPE 
                        WHEN 'R' THEN 'ใบสั่งรายได้แผ่นดิน'
                        WHEN 'W' THEN 'ใบสั่งรายได้แผ่นดิน'
                        WHEN 'S' THEN 'ใบสั่งเงินมัดจำรังวัด' END AS RECEIPT_TYPE
                        , CASE WHEN OW.BTD59_NO IS NOT NULL THEN OW.BTD59_NO ||'/' || OW.BUDGET_YEAR ELSE '-' END AS BTD59_NO
                        , RCPT.RECEIPT_GRP_TYPE
                        , REC.RCPT_CAT, REC.OPT_SHOW, RCPT.ORDER_NO ||' '|| RCPT.ORDER_DTM AS ORDER_NO
                        ,CASE WHEN RCPT.PAYER_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(RCPT.PAYER_TITLE, '...', RCPT.PAYER_FNAME)||' '|| RCPT.PAYER_LNAME)
                        ELSE RCPT.PAYER_TITLE||RCPT.PAYER_FNAME||' '||RCPT.PAYER_LNAME END AS PAYER_NAME

                        ,CASE WHEN REC.PARTY_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(REC.PARTY_TITLE, '...', REC.PARTY_FNAME)||' '|| REC.PARTY_LNAME)
                        ELSE REC.PARTY_TITLE||REC.PARTY_FNAME||' '||REC.PARTY_LNAME END AS PARTY_NAME
                        , REC.QUEUE_NO ||' '|| REC.QUEUE_DATE AS QUEUE_NO, REC.ASSET_MNY, REC.EVALUATE_MNY
                        , REC.PLATE_ABBR_NAME, REC.PARCEL_NO, REC.SHEET_NO, REC.LAND_NO, REC.SURVEY_NO, REC.MOO, REC.ADDR_TAMBOL, REC.ADDR_AMPHUR, REC.ADDR_PROVINCE
                        , RCPT.CANCEL_CAUSE_REM
                FROM FIN.TB_FIN_PAY_RCPT RCPT
                LEFT JOIN FIN.TB_FIN_ORDER_RECEIPT REC
                    ON REC.ORDER_NO = RCPT.ORDER_NO
                    AND REC.ORDER_DATE = RCPT.ORDER_DTM
                    AND REC.LANDOFFICE_SEQ = RCPT.LANDOFFICE_SEQ
                LEFT JOIN FIN.TB_FIN_ORDER_WITHDRAW OW
                    ON RCPT.ORDER_NO = OW.ORDER_NO
                    AND RCPT.ORDER_DTM = OW.ORDER_DATE
                    AND RCPT.LANDOFFICE_SEQ = OW.LANDOFFICE_SEQ
                WHERE  RCPT.LANDOFFICE_SEQ = :landoffice 
                    ".$detailOrder_sql
                    .$detailDate_sql
                    .$detailRec_sql."
                )
                SELECT P1.RECEIPT_NO AS RECEIPT_NO_P1 ,  P2.RECEIPT_NO AS RECEIPT_NO_P2
            ,P1.RECEIPT_DTM AS RECEIPT_DTM_P1 ,  P2.RECEIPT_DTM AS RECEIPT_DTM_P2
            ,P1.LANDOFFICE_NAME AS LANDOFFICE_NAME_RECEIPT_P1 ,  P2.LANDOFFICE_NAME AS LANDOFFICE_NAME_RECEIPT_P2
            ,P1.RECEIPT_TYPE AS RECEIPT_TYPE_P1 ,  P2.RECEIPT_TYPE AS RECEIPT_TYPE_P2
            ,P1.BTD59_NO AS BTD59_NO_P1 ,  P2.BTD59_NO AS BTD59_NO_P2
            ,P1.RECEIPT_GRP_TYPE AS RCPT_GROUP_P1 ,  P2.RECEIPT_GRP_TYPE AS RCPT_GROUP_P2
            ,P1.RCPT_CAT AS RCPT_CAT_P1 ,  P2.RCPT_CAT AS RCPT_CAT_P2
            ,P1.OPT_SHOW AS OPT_SHOW_P1 ,  P2.OPT_SHOW AS OPT_SHOW_P2
            ,P1.ORDER_NO AS ORDER_NO_P1 ,  P2.ORDER_NO AS ORDER_NO_P2
            ,P1.PAYER_NAME AS PAYER_NAME_P1 ,  P2.PAYER_NAME AS PAYER_NAME_P2
            ,P1.PARTY_NAME AS PARTY_NAME_P1 ,  P2.PARTY_NAME AS PARTY_NAME_P2
            ,P1.QUEUE_NO AS QUEUE_NO_P1 ,  P2.QUEUE_NO AS QUEUE_NO_P2
            ,P1.ASSET_MNY AS ASSET_MNY_P1 ,  P2.ASSET_MNY AS ASSET_MNY_P2
            ,P1.EVALUATE_MNY AS EVALUATE_MNY_P1 ,  P2.EVALUATE_MNY AS EVALUATE_MNY_P2
            ,P1.PLATE_ABBR_NAME AS PLATE_ABBR_NAME_P1 ,  P2.PLATE_ABBR_NAME AS PLATE_ABBR_NAME_P2
            ,P1.PARCEL_NO AS PARCEL_NO_P1 ,  P2.PARCEL_NO AS PARCEL_NO_P2
            ,P1.SHEET_NO AS SHEET_NO_P1 ,  P2.SHEET_NO AS SHEET_NO_P2
            ,P1.LAND_NO AS LAND_NO_P1 ,  P2.LAND_NO AS LAND_NO_P2
            ,P1.SURVEY_NO AS SURVEY_NO_P1 ,  P2.SURVEY_NO AS SURVEY_NO_P2
            ,P1.MOO AS MOO_P1 ,  P2.MOO AS MOO_P2
            ,P1.ADDR_TAMBOL AS ADDR_TAMBOL_P1 ,  P2.ADDR_TAMBOL AS ADDR_TAMBOL_P2
            ,P1.ADDR_AMPHUR AS ADDR_AMPHUR_P1 ,  P2.ADDR_AMPHUR AS ADDR_AMPHUR_P2
            ,P1.ADDR_PROVINCE AS ADDR_PROVINCE_P1 ,  P2.ADDR_PROVINCE AS ADDR_PROVINCE_P2
            ,P1.CANCEL_CAUSE_REM AS CANCEL_CAUSE_REM_P1 ,  P2.CANCEL_CAUSE_REM AS CANCEL_CAUSE_REM_P2
                FROM P1
                FULL OUTER JOIN P2
                    ON P1.RECEIPT_NO = P2.RECEIPT_NO";

    // echo($select);
    $stid = oci_parse($conn, $select); 
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    if ($detailOrder != '') oci_bind_by_name($stid, ':detailOrder', $detailOrder);
    if ($detailDate != '') oci_bind_by_name($stid, ':detailDate', $detailDate);
    if ($detailRec != '') oci_bind_by_name($stid, ':detailRec', $detailRec);
    oci_execute($stid);


    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    
    
    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);


?>

