<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';

    $landoffice = !isset($_POST['landoffice'])? '' : $_POST['landoffice'];
    $Result = array();
    
 
    $select = "WITH RECV AS(
                SELECT TO_NUMBER('1') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS RECV_COUNT
                FROM(
                    SELECT  LANDOFFICE_SEQ,LANDOFFICE_NAME_TH, GFMIS_CAPITAL_PAY_ID, GFMIS_CAPITAL_ID, AREA_CODE 
                    FROM MGT1.TB_MAS_LANDOFFICE
                    WHERE RECORD_STATUS = 'N' ) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                UNION
                SELECT  TO_NUMBER('2') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS RECV_COUNT
                FROM(
                    SELECT LANDOFFICE_SEQ, T1.REV_OWNER_ID,
                        OWNER_NAME,OWNER_CODE FROM MGT1.TB_FIN_MAS_REV_OWNER T1
                        LEFT OUTER JOIN MGT1.TB_FIN_MAS_REV_OWNER_MAPPING T2
                        ON T1.REV_OWNER_ID  =  T2.REV_OWNER_ID 
                        WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' ) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                UNION
                SELECT TO_NUMBER('3') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS RECV_COUNT
                FROM(
                    SELECT BANK_BOOK_SEQ,   LANDOFFICE_SEQ,BANK,  BANK_BRANCH, ACCOUNT_TYPE,
                            CASE
                            WHEN ACCOUNT_TYPE= '1' THEN 'ออมทรัพย์'
                            WHEN ACCOUNT_TYPE= '2' THEN 'กระแสรายวัน'
                            ELSE ''
                            END ACCOUNT_TYPE_,
                            ACCOUNT_NAME, ACCOUNT_NO, BALANCE_MNY
                    FROM MGT1.TB_FIN_SEN_BANK_BOOK
                            WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N' ) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                UNION
                SELECT TO_NUMBER('4') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS RECV_COUNT
                FROM(
                    SELECT REV_930_SEQ,   LANDOFFICE_SEQ, PARENT_LANDOFFICE_SEQ, REV_930_TREASURY_MNY,
                            REV_930_OFFICE_MNY,   REV_930_WIP_MNY,  REV_930_BANK_MNY
                            FROM MGT1.TB_FIN_SUR_REV_930
                            WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N' ) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                UNION
                -- SELECT TO_NUMBER('5') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS RECV_COUNT
                -- FROM(
                --     SELECT INCOME_GROUP_ID,  INCOME_GROUP_NAME, CLOSE_DTM,  LANDOFFICE_SEQ, INCOME_MNY
                --             FROM MGT1.TB_FIN_INP_IMPORT_CLOSE
                --         WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N') P1
                -- LEFT OUTER JOIN
                --     (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                --     ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                --     WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                -- UNION
                SELECT TO_NUMBER('6') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS RECV_COUNT
                FROM(
                    SELECT T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO,  T1.BOOK59_DATE,
                        T1.BUDGET_YEAR, T1.BOOK59_STS, SUM(T2.STR_MNY) STR_MNY, SUM(T2.REMAIN_MNY) REMAIN_MNY_,
                            ASSET_MNY,  EVALUATE_MNY, PLATE_ABBR_NAME,  PARCEL_NO, SURVEY_NO,  LAND_NO,  SHEET_NO,
                            MOO,  ADDR_TAMBOL, ADDR_AMPHUR,  ADDR_PROVINCE
                            FROM MGT1.TB_FIN_SUR_BOOK59 T1
                            LEFT OUTER JOIN MGT1.TB_FIN_SUR_PART_INCOME T2
                            ON T1.BOOK59_SEQ  =  T2.BOOK59_SEQ 
                            LEFT OUTER JOIN MGT1.TB_FIN_PAY_TITLE_DEED T3
                            ON T1.TITLE_DEED_SEQ  =  T3.TITLE_DEED_SEQ 
                            WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'
                        GROUP BY T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO,  T1.BOOK59_DATE,
                    T1.BUDGET_YEAR, T1.BOOK59_STS, 
                            ASSET_MNY,  EVALUATE_MNY, PLATE_ABBR_NAME,  PARCEL_NO, SURVEY_NO,  LAND_NO,  SHEET_NO,
                            MOO,  ADDR_TAMBOL, ADDR_AMPHUR,  ADDR_PROVINCE) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                UNION
                SELECT TO_NUMBER('9') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS RECV_COUNT
                FROM(
                    SELECT LANDOFFICE_SEQ, RECEIPT_SEQ FROM MGT1.TB_FIN_PAY_RCPT
                    WHERE LANDOFFICE_SEQ = :landoffice ) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                ),
                OK AS (
                SELECT TO_NUMBER('1') AS TOPIC,  COUNT(P1.LANDOFFICE_SEQ) AS OK_COUNT
                FROM(
                    SELECT  LANDOFFICE_SEQ,LANDOFFICE_NAME_TH, GFMIS_CAPITAL_PAY_ID, GFMIS_CAPITAL_ID, AREA_CODE 
                    FROM MAS.TB_MAS_LANDOFFICE
                        WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N' ) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                UNION
                SELECT TO_NUMBER('2') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS OK_COUNT
                FROM(
                    SELECT LANDOFFICE_SEQ, T1.REV_OWNER_ID,
                        OWNER_NAME,OWNER_CODE FROM FIN.TB_FIN_MAS_REV_OWNER T1
                        LEFT OUTER JOIN FIN.TB_FIN_MAS_REV_OWNER_MAPPING T2
                        ON T1.REV_OWNER_ID  =  T2.REV_OWNER_ID 
                        WHERE LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' ) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                UNION
                SELECT TO_NUMBER('3') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS OK_COUNT
                FROM(
                    SELECT BANK_BOOK_SEQ,   LANDOFFICE_SEQ,BANK,  BANK_BRANCH, ACCOUNT_TYPE,
                            CASE
                            WHEN ACCOUNT_TYPE= '1' THEN 'ออมทรัพย์'
                            WHEN ACCOUNT_TYPE= '2' THEN 'กระแสรายวัน'
                            ELSE ''
                            END ACCOUNT_TYPE_,
                            ACCOUNT_NAME, ACCOUNT_NO, BALANCE_MNY
                    FROM FIN.TB_FIN_SEN_BANK_BOOK
                            WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N' ) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                UNION
                SELECT TO_NUMBER('4') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS OK_COUNT
                FROM(
                    SELECT REV_930_SEQ,   LANDOFFICE_SEQ, PARENT_LANDOFFICE_SEQ, REV_930_TREASURY_MNY,
                            REV_930_OFFICE_MNY,   REV_930_WIP_MNY,  REV_930_BANK_MNY
                            FROM FIN.TB_FIN_SUR_REV_930
                            WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N' ) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                UNION
                -- SELECT TO_NUMBER('5') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS OK_COUNT
                -- FROM(
                --     SELECT INCOME_GROUP_ID,  INCOME_GROUP_NAME, CLOSE_DTM,  LANDOFFICE_SEQ,   INCOME_MNY
                --             FROM FIN.TB_FIN_INP_IMPORT_CLOSE
                --         WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N') P1
                -- LEFT OUTER JOIN
                --     (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                --     ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                --     WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                -- UNION
                SELECT TO_NUMBER('6') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS OK_COUNT
                FROM(
                    SELECT T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO,  T1.BOOK59_DATE,
                        T1.BUDGET_YEAR, T1.BOOK59_STS, SUM(T2.STR_MNY) STR_MNY, SUM(T2.REMAIN_MNY) REMAIN_MNY_,
                            ASSET_MNY,  EVALUATE_MNY, PLATE_ABBR_NAME,  PARCEL_NO, SURVEY_NO,  LAND_NO,  SHEET_NO,
                            MOO,  ADDR_TAMBOL, ADDR_AMPHUR,  ADDR_PROVINCE
                            FROM FIN.TB_FIN_SUR_BOOK59 T1
                            LEFT OUTER JOIN FIN.TB_FIN_SUR_PART_INCOME T2
                            ON T1.BOOK59_SEQ  =  T2.BOOK59_SEQ 
                            LEFT OUTER JOIN FIN.TB_FIN_PAY_TITLE_DEED T3
                            ON T1.TITLE_DEED_SEQ  =  T3.TITLE_DEED_SEQ 
                            WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'
                            GROUP BY T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO,  T1.BOOK59_DATE,
                        T1.BUDGET_YEAR, T1.BOOK59_STS, 
                            ASSET_MNY,  EVALUATE_MNY, PLATE_ABBR_NAME,  PARCEL_NO, SURVEY_NO,  LAND_NO,  SHEET_NO,
                            MOO,  ADDR_TAMBOL, ADDR_AMPHUR,  ADDR_PROVINCE) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                    
                UNION
                SELECT TO_NUMBER('9') AS TOPIC, COUNT(P1.LANDOFFICE_SEQ) AS RECV_COUNT
                FROM(
                    SELECT LANDOFFICE_SEQ, RECEIPT_SEQ FROM FIN.TB_FIN_PAY_RCPT
                    WHERE LANDOFFICE_SEQ = :landoffice ) P1
                LEFT OUTER JOIN
                    (SELECT LANDOFFICE_SEQ , LANDOFFICE_NAME_TH FROM MAS.TB_MAS_LANDOFFICE) P2
                    ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    WHERE P1.LANDOFFICE_SEQ = :landoffice
                )
                SELECT 
                CASE RECV.TOPIC WHEN 1 THEN 'ข้อมูลสำนักงานที่ดิน'
                            WHEN 2 THEN 'ข้อมูลเชื่อมโยงศูนย์ต้นทุนเจ้าของรายได้'
                            WHEN 3 THEN 'ข้อมูลเลขที่บัญชีธนาคาร'
                            WHEN 4 THEN 'ข้อมูลเงินมัดจำรังวัด'
                            WHEN 5 THEN 'ข้อมูลตั้งต้นเงินที่รับ'
                            WHEN 6 THEN 'ข้อมูล บ.ท.ด.59' 
                            WHEN 9 THEN 'ข้อมูลใบเสร็จทั้งหมด'
                            END AS TYPE,
                RECV.RECV_COUNT AS RECEIVE,
                OK.OK_COUNT AS MIGRATE_SUCCESS,
                (RECV.RECV_COUNT - OK.OK_COUNT) AS MIGRATE_ERROR
                
                FROM RECV, OK
                WHERE RECV.TOPIC = OK.TOPIC ";

    $stid = oci_parse($conn, $select); 
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    oci_execute($stid);


    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    
    
    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);


?>

