<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';

    $landoffice = !isset($_POST['landoffice'])? '' : $_POST['landoffice'];
    $Result = array();
    
 
    $select = "SELECT RECEIPT_CATEGORY
    FROM FIN.TB_FIN_SUR_BOOK59
    WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N'
    GROUP BY RECEIPT_CATEGORY
    ORDER BY NLSSORT(RECEIPT_CATEGORY, 'NLS_SORT=THAI_DICTIONARY')";

    $stid = oci_parse($conn, $select); 
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    oci_execute($stid);


    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    
    
    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);


?>

