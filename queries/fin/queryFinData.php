<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //
            $select = "WITH P1 AS(
                        SELECT LANDOFFICE_SEQ,LANDOFFICE_NAME_TH, GFMIS_CAPITAL_PAY_ID, GFMIS_CAPITAL_ID, AREA_CODE
                        FROM MGT1.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice
                        ),
                        P2 AS(
                        SELECT LANDOFFICE_SEQ,LANDOFFICE_NAME_TH, GFMIS_CAPITAL_PAY_ID, GFMIS_CAPITAL_ID, AREA_CODE
                        FROM MAS.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice
                        )
                    SELECT P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 ,  P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
                            ,P1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 ,  P2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
                            ,P1.GFMIS_CAPITAL_PAY_ID AS GFMIS_CAPITAL_PAY_ID_P1 ,  P2.GFMIS_CAPITAL_PAY_ID AS GFMIS_CAPITAL_PAY_ID_P2
                            ,P1.GFMIS_CAPITAL_ID AS GFMIS_CAPITAL_ID_P1 ,  P2.GFMIS_CAPITAL_ID AS GFMIS_CAPITAL_ID_P2
                            ,P1.AREA_CODE AS AREA_CODE_P1 ,  P2.AREA_CODE AS AREA_CODE_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    ORDER BY P1.LANDOFFICE_SEQ
                            ,P2.LANDOFFICE_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            oci_execute($stid);
        break;
        case '2': //
            $select = "WITH P1 AS(
                        SELECT LANDOFFICE_SEQ, T1.REV_OWNER_ID, OWNER_NAME,OWNER_CODE, GROUP_NAME, T3.REVENUE_GROUP_ID
                        FROM MGT1.TB_FIN_MAS_REV_OWNER T1
                        LEFT OUTER JOIN MGT1.TB_FIN_MAS_REV_OWNER_MAPPING T2
                            ON T1.REV_OWNER_ID  =  T2.REV_OWNER_ID 
                        LEFT OUTER JOIN MGT1.TB_FIN_MAS_REV_GROUP T3
                            ON T2.REVENUE_GROUP_ID = T3.REVENUE_GROUP_ID
                        WHERE T1.RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice
                        ),
                        P2 AS(
                        SELECT LANDOFFICE_SEQ, T1.REV_OWNER_ID, OWNER_NAME,OWNER_CODE, GROUP_NAME ,T3.REVENUE_GROUP_ID
                        FROM FIN.TB_FIN_MAS_REV_OWNER T1
                        LEFT OUTER JOIN FIN.TB_FIN_MAS_REV_OWNER_MAPPING T2
                            ON T1.REV_OWNER_ID  =  T2.REV_OWNER_ID 
                        LEFT OUTER JOIN FIN.TB_FIN_MAS_REV_GROUP T3
                            ON T2.REVENUE_GROUP_ID = T3.REVENUE_GROUP_ID
                        WHERE T1.RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice
                        )
                    SELECT P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 ,  P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
                            ,P1.REV_OWNER_ID AS REV_OWNER_ID_P1 ,  P2.REV_OWNER_ID AS REV_OWNER_ID_P2
                            ,P1.OWNER_NAME AS OWNER_NAME_P1 ,  P2.OWNER_NAME AS OWNER_NAME_P2
                            ,P1.OWNER_CODE AS OWNER_CODE_P1 ,  P2.OWNER_CODE AS OWNER_CODE_P2
                            ,P1.GROUP_NAME AS GROUP_NAME_P1 ,  P2.GROUP_NAME AS GROUP_NAME_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                        AND P1.REV_OWNER_ID = P2.REV_OWNER_ID
                        AND P1.OWNER_NAME = P2.OWNER_NAME
                        AND P1.OWNER_CODE = P2.OWNER_CODE
                        AND P1.REVENUE_GROUP_ID = P2.REVENUE_GROUP_ID
                    ORDER BY P1.OWNER_NAME
                            ,P2.OWNER_NAME";
                     
            $stid = oci_parse($conn, $select); 
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            oci_execute($stid);
        break;
        case '3': //
            $select = "WITH P1 AS(
                        SELECT BANK_BOOK_SEQ,  LANDOFFICE_SEQ,BANK, BANK_BRANCH, ACCOUNT_TYPE,
                        CASE
                        WHEN ACCOUNT_TYPE= '1' THEN 'ออมทรัพย์'
                        WHEN ACCOUNT_TYPE= '2' THEN 'กระแสรายวัน'
                        ELSE ''
                        END ACCOUNT_TYPE_,
                        ACCOUNT_NAME, ACCOUNT_NO, BALANCE_MNY, ZBANK
                        FROM MGT1.TB_FIN_SEN_BANK_BOOK
                        WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT BANK_BOOK_SEQ,  LANDOFFICE_SEQ,BANK, BANK_BRANCH, ACCOUNT_TYPE,
                        CASE
                        WHEN ACCOUNT_TYPE= '1' THEN 'ออมทรัพย์'
                        WHEN ACCOUNT_TYPE= '2' THEN 'กระแสรายวัน'
                        ELSE ''
                        END ACCOUNT_TYPE_,
                        ACCOUNT_NAME, ACCOUNT_NO, BALANCE_MNY, ZBANK
                        FROM FIN.TB_FIN_SEN_BANK_BOOK
                        WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N' 
                        )
                    SELECT P1.BANK_BOOK_SEQ AS BANK_BOOK_SEQ_P1 ,  P2.BANK_BOOK_SEQ AS BANK_BOOK_SEQ_P2
                        ,P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 ,  P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
                        ,P1.BANK AS BANK_P1 ,  P2.BANK AS BANK_P2
                        ,P1.ZBANK AS ZBANK_P1 ,  P2.ZBANK AS ZBANK_P2
                        ,P1.BANK_BRANCH AS BANK_BRANCH_P1 ,  P2.BANK_BRANCH AS BANK_BRANCH_P2
                        ,P1.ACCOUNT_TYPE AS ACCOUNT_TYPE_P1 ,  P2.ACCOUNT_TYPE AS ACCOUNT_TYPE_P2
                        ,P1.ACCOUNT_TYPE_ AS ACCOUNT_TYPE__P1 ,  P2.ACCOUNT_TYPE_ AS ACCOUNT_TYPE__P2
                        ,P1.ACCOUNT_NAME AS ACCOUNT_NAME_P1 ,  P2.ACCOUNT_NAME AS ACCOUNT_NAME_P2
                        ,P1.ACCOUNT_NO AS ACCOUNT_NO_P1 ,  P2.ACCOUNT_NO AS ACCOUNT_NO_P2
                        ,P1.BALANCE_MNY AS BALANCE_MNY_P1 ,  P2.BALANCE_MNY AS BALANCE_MNY_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.BANK_BOOK_SEQ = P2.BANK_BOOK_SEQ
                    ORDER BY P1.BANK
                    ,P2.BANK";
                     
            $stid = oci_parse($conn, $select); 
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            oci_execute($stid);
        break;

        case '4': //
            $select = "WITH P1 AS(
                        SELECT REV_930_SEQ, LANDOFFICE_SEQ, PARENT_LANDOFFICE_SEQ, REV_930_TREASURY_MNY
                            ,REV_930_OFFICE_MNY, REV_930_WIP_MNY, REV_930_BANK_MNY
                        FROM MGT1.TB_FIN_SUR_REV_930
                        WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT REV_930_SEQ, LANDOFFICE_SEQ, PARENT_LANDOFFICE_SEQ, REV_930_TREASURY_MNY
                            ,REV_930_OFFICE_MNY, REV_930_WIP_MNY, REV_930_BANK_MNY
                        FROM FIN.TB_FIN_SUR_REV_930
                        WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N'
                        )
                    SELECT P1.REV_930_TREASURY_MNY AS REV_930_TREASURY_MNY_P1 ,  P2.REV_930_TREASURY_MNY AS REV_930_TREASURY_MNY_P2
                        ,P1.REV_930_OFFICE_MNY AS REV_930_OFFICE_MNY_P1 ,  P2.REV_930_OFFICE_MNY AS REV_930_OFFICE_MNY_P2
                        ,P1.REV_930_WIP_MNY AS REV_930_WIP_MNY_P1 ,  P2.REV_930_WIP_MNY AS REV_930_WIP_MNY_P2
                        ,P1.REV_930_BANK_MNY AS REV_930_BANK_MNY_P1 ,  P2.REV_930_BANK_MNY AS REV_930_BANK_MNY_P2

                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.REV_930_SEQ = P2.REV_930_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            oci_execute($stid);
        break;

        case '5': //
            $select = "WITH P1 AS(
                        SELECT INCOME_GROUP_ID,  INCOME_GROUP_NAME, CLOSE_DTM,  LANDOFFICE_SEQ, INCOME_MNY
                        FROM MGT1.TB_FIN_INP_IMPORT_CLOSE
                        WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT INCOME_GROUP_ID,  INCOME_GROUP_NAME, CLOSE_DTM,  LANDOFFICE_SEQ, INCOME_MNY
                        FROM FIN.TB_FIN_INP_IMPORT_CLOSE
                        WHERE LANDOFFICE_SEQ = :landoffice  AND RECORD_STATUS = 'N'
                        )
                    SELECT P1.INCOME_GROUP_ID AS INCOME_GROUP_ID_P1 ,  P2.INCOME_GROUP_ID AS INCOME_GROUP_ID_P2
                        ,P1.INCOME_GROUP_NAME AS INCOME_GROUP_NAME_P1 ,  P2.INCOME_GROUP_NAME AS INCOME_GROUP_NAME_P2
                        ,P1.CLOSE_DTM AS CLOSE_DTM_P1 ,  P2.CLOSE_DTM AS CLOSE_DTM_P2
                        ,P1.INCOME_MNY AS INCOME_MNY_P1 ,  P2.INCOME_MNY AS INCOME_MNY_P2

                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.INCOME_GROUP_ID = P2.INCOME_GROUP_ID";
                     
            $stid = oci_parse($conn, $select); 
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            oci_execute($stid);
        break;

        case '6': //
            $btd59no_sql = $btd59no == '' ? '' : " AND T1.BOOK59_NO = :btd59no ";
            $btd59year_sql = $btd59year == '' ? '' : " AND T1.BUDGET_YEAR = :btd59year ";
            if($datestart != '' && $dateend != ''){
                $date_sql = " AND T1.BOOK59_DATE BETWEEN TO_DATE(:datestart,'YYYY-MM-DD') AND TO_DATE(:dateend,'YYYY-MM-DD')";
            }else if($datestart != '' && $dateend == ''){
                $date_sql = " AND T1.BOOK59_DATE >= TO_DATE(:datestart,'YYYY-MM-DD') ";
            }else if($datestart == '' && $dateend != ''){
                $date_sql = " AND T1.BOOK59_DATE <= TO_DATE(:dateend,'YYYY-MM-DD')";
            }else{
                $date_sql =  '';
            }
            $receipcate_sql = $receipcate == '' ? '' : " AND T1.RECEIPT_CATEGORY LIKE '' || :receipcate || '' "; 

            $select = "WITH P1 AS(
                    SELECT T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO, T1.BOOK59_DATE , T1.PAYER_NAME
                        ,T1.BUDGET_YEAR, T1.BOOK59_STS, T1.STR_TOTAL_MNY AS STR_MNY, T1.REMAIN_MNY AS REMAIN_MNY_
                        ,PLATE_ABBR_NAME, PARCEL_NO, SURVEY_NO
                        ,ADDR_TAMBOL, ADDR_AMPHUR, ADDR_PROVINCE , T1.RECEIPT_CATEGORY
                    FROM MGT1.TB_FIN_SUR_BOOK59 T1
                    LEFT OUTER JOIN MGT1.TB_FIN_SUR_PART_INCOME T2
                        ON T1.BOOK59_SEQ  =  T2.BOOK59_SEQ 
                    LEFT OUTER JOIN MGT1.TB_FIN_PAY_TITLE_DEED T3
                        ON T1.TITLE_DEED_SEQ  =  T3.TITLE_DEED_SEQ 
                    WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' 
                        ".$btd59no_sql
                        .$btd59year_sql
                        .$date_sql
                        .$receipcate_sql."
                    GROUP BY T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO,  T1.BOOK59_DATE, T1.PAYER_NAME
                            ,T1.BUDGET_YEAR, T1.BOOK59_STS, PLATE_ABBR_NAME, PARCEL_NO, T1.REMAIN_MNY , T1.STR_TOTAL_MNY
                            ,SURVEY_NO, ADDR_TAMBOL, ADDR_AMPHUR, ADDR_PROVINCE, T1.RECEIPT_CATEGORY
                        ),
            P2 AS(
                    SELECT T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO, T1.BOOK59_DATE , T1.PAYER_NAME
                         ,T1.BUDGET_YEAR, T1.BOOK59_STS, T1.STR_TOTAL_MNY AS STR_MNY, T1.REMAIN_MNY AS REMAIN_MNY_
                         ,PLATE_ABBR_NAME, PARCEL_NO, SURVEY_NO
                         ,ADDR_TAMBOL, ADDR_AMPHUR, ADDR_PROVINCE, T1.RECEIPT_CATEGORY
                    FROM FIN.TB_FIN_SUR_BOOK59 T1
                    LEFT OUTER JOIN FIN.TB_FIN_SUR_PART_INCOME T2
                        ON T1.BOOK59_SEQ  =  T2.BOOK59_SEQ 
                    LEFT OUTER JOIN FIN.TB_FIN_PAY_TITLE_DEED T3
                        ON T1.TITLE_DEED_SEQ  =  T3.TITLE_DEED_SEQ 
                    WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' 
                        ".$btd59no_sql
                        .$btd59year_sql
                        .$date_sql
                        .$receipcate_sql."
                    GROUP BY T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO,  T1.BOOK59_DATE, T1.PAYER_NAME
                            ,T1.BUDGET_YEAR, T1.BOOK59_STS, PLATE_ABBR_NAME, PARCEL_NO, T1.REMAIN_MNY , T1.STR_TOTAL_MNY
                            ,SURVEY_NO, ADDR_TAMBOL, ADDR_AMPHUR, ADDR_PROVINCE, T1.RECEIPT_CATEGORY
                    )
                SELECT P1.BOOK59_NO || '/' || P1.BUDGET_YEAR AS BOOK59_NO_P1 ,  P2.BOOK59_NO || '/' || P2.BUDGET_YEAR AS BOOK59_NO_P2
                    ,CASE WHEN SUBSTR(P1.BOOK59_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P1.BOOK59_DATE) ELSE TO_CHAR(P1.BOOK59_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS BOOK59_DATE_P1
                    ,CASE WHEN SUBSTR(P1.BOOK59_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P2.BOOK59_DATE) ELSE TO_CHAR(P2.BOOK59_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS BOOK59_DATE_P2
                    ,P1.PAYER_NAME AS PAYER_NAME_P1 ,  P2.PAYER_NAME AS PAYER_NAME_P2
                    ,P1.RECEIPT_CATEGORY AS RECEIPT_CATEGORY_P1 ,  P2.RECEIPT_CATEGORY AS RECEIPT_CATEGORY_P2
                    ,P1.PARCEL_NO AS PARCEL_NO_P1 ,  P2.PARCEL_NO AS PARCEL_NO_P2
                    ,P1.SURVEY_NO AS SURVEY_NO_P1 ,  P2.SURVEY_NO AS SURVEY_NO_P2
                    ,P1.ADDR_TAMBOL AS ADDR_TAMBOL_P1 ,  P2.ADDR_TAMBOL AS ADDR_TAMBOL_P2
                    ,P1.ADDR_AMPHUR AS ADDR_AMPHUR_P1 ,  P2.ADDR_AMPHUR AS ADDR_AMPHUR_P2
                    ,NVL(P1.STR_MNY,0) AS STR_MNY_P1 ,  NVL(P2.STR_MNY,0) AS STR_MNY_P2
                    ,NVL(P1.REMAIN_MNY_,0) AS REMAIN_MNY_P1 ,  NVL(P2.REMAIN_MNY_,0) AS REMAIN_MNY_P2
                    ,P1.BOOK59_SEQ AS BOOK59_SEQ_P1 ,  P2.BOOK59_SEQ AS BOOK59_SEQ_P2
                FROM P1
                FULL OUTER JOIN P2
                    ON P1.BOOK59_SEQ = P2.BOOK59_SEQ
                ORDER BY P1.BUDGET_YEAR, P1.BOOK59_NO
                        ,P2.BUDGET_YEAR, P2.BOOK59_NO";
		
            // echo $select;         
            $stid = oci_parse($conn, $select); 
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($btd59no != '') oci_bind_by_name($stid, ':btd59no', $btd59no);
            if ($btd59year != '') oci_bind_by_name($stid, ':btd59year', $btd59year);
            if ($datestart != '') oci_bind_by_name($stid, ':datestart', $datestart);
            if ($dateend != '') oci_bind_by_name($stid, ':dateend', $dateend);
            if ($receipcate != '') oci_bind_by_name($stid, ':receipcate', $receipcate);
            oci_execute($stid);
        break;

        case '7': //
            if($datestart != '' && $dateend != ''){
                $date_sql = " AND RCPT.RECEIPT_DTM BETWEEN TO_DATE(:datestart,'YYYY-MM-DD') AND TO_DATE(:dateend,'YYYY-MM-DD')";
            }else if($datestart != '' && $dateend == ''){
                $date_sql = " AND RCPT.RECEIPT_DTM >= TO_DATE(:datestart,'YYYY-MM-DD') ";
            }else if($datestart == '' && $dateend != ''){
                $date_sql = " AND RCPT.RECEIPT_DTM <= TO_DATE(:dateend,'YYYY-MM-DD')";
            }else{
                $date_sql =  '';
            }

            if($rcptstart != '' && $rcptend != ''){
                $rcpt_sql = " AND RCPT.RECEIPT_NO BETWEEN '' || :rcptstart || '' AND '' || :rcptend || '' ";
            }else if($rcptstart != '' && $rcptend == ''){
                $rcpt_sql = " AND RCPT.RECEIPT_NO = '' || :rcptstart || '' ";
            }else if($rcptstart == '' && $rcptend != ''){
                $rcpt_sql = " AND RCPT.RECEIPT_NO <=  '' ||  :rcptend || '' ";
            }else{
                $rcpt_sql =  '';
            }

            // $selectrcpt_sql = $selectrcpt == '' ? '' : " AND RCPT.RCPT_TYPE = '' || :selectrcpt || '' "; 

            if($selectrcpt == 'R' ){
                $selectrcpt_sql = " AND RCPT.RECEIPT_TYPE IN ('R','W') ";
            }else if($selectrcpt == 'S'){
                $selectrcpt_sql = "  AND RCPT.RECEIPT_TYPE IN ('S') ";  
            }else{
                $selectrcpt_sql = '';
            }

            if($selectsts == 'C' ){
                // $selectsts_sql = " AND REC.RECORD_STATUS = 'C' ";
                $selectsts_sql = " AND RCPT.ORDER_STS <> 'C' AND RCPT.RECORD_STATUS <> 'C' ";
            }else if($selectsts == 'D'){
                $selectsts_sql = "  AND RCPT.ORDER_STS = 'C' AND RCPT.RECORD_STATUS = 'C'  ";  
            }else{
                $selectsts_sql = '';
            }

            // $selectsts_sql = $selectsts == '' ? '' : "  AND RCPT.ORDER_STS = 'C' AND RCPT.RECORD_STATUS = 'C'  ";             


            $select = "WITH P1 AS (
                        SELECT REC.QUEUE_NO, CASE WHEN REC.ORDER_NO IS NULL THEN RCPT.ORDER_NO ELSE REC.ORDER_NO END AS ORDER_NO
                                ,CASE WHEN REC.RCPT_CAT IS NULL THEN RCPT.RECEIPT_CATEGORY ELSE REC.RCPT_CAT END AS RCPT_CAT
                                -- ,CASE WHEN REC.PAYER_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(REC.PAYER_TITLE, '...', REC.PAYER_FNAME)||' '|| REC.PAYER_LNAME)
                                -- ELSE REC.PAYER_TITLE||REC.PAYER_FNAME||' '||REC.PAYER_LNAME END AS PAYER_NAME
                                ,CASE WHEN RCPT.PAYER_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(RCPT.PAYER_TITLE, '...', RCPT.PAYER_FNAME)||' '|| RCPT.PAYER_LNAME)
                                ELSE RCPT.PAYER_TITLE||RCPT.PAYER_FNAME||' '||RCPT.PAYER_LNAME END AS PAYER_NAME

                                ,CASE WHEN REC.PARTY_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(REC.PARTY_TITLE, '...', REC.PARTY_FNAME)||' '|| REC.PARTY_LNAME)
                                ELSE REC.PARTY_TITLE||REC.PARTY_FNAME||' '||REC.PARTY_LNAME END AS PARTY_NAME

                                ,CASE WHEN REC.RCPT_TOTAL_MNY IS NULL THEN RCPT.TOTAL_MNY ELSE REC.RCPT_TOTAL_MNY END AS RCPT_TOTAL_MNY
                                ,RCPT.RECEIPT_NO, RCPT.RECEIPT_DTM
                                ,CASE WHEN RCPT.ORDER_STS = 'C' AND RCPT.RECORD_STATUS = 'C' THEN 'ยกเลิกใบเสร็จ' ELSE 'ชำระเงินแล้ว' END STATUS
                                -- WHEN RCPT.ORDER_STS <> 'C' AND RCPT.RECORD_STATUS <> 'C' THEN 'ชำระเงินแล้ว' END STATUS
                                ,RCPT.RECEIPT_DTM AS RECEIPT_DTM_EN 
                            FROM  FIN.TB_FIN_PAY_RCPT RCPT
                                LEFT JOIN FIN.TB_FIN_ORDER_RECEIPT REC
                            ON REC.ORDER_NO = RCPT.ORDER_NO
                            AND REC.ORDER_DATE = RCPT.ORDER_DTM
                            AND REC.LANDOFFICE_SEQ = RCPT.LANDOFFICE_SEQ
                        WHERE RCPT.LANDOFFICE_SEQ = :landoffice 
                        ".$date_sql
                        .$selectrcpt_sql
                        .$selectsts_sql
                        .$rcpt_sql."
                        ),
                    P2 AS (
                        SELECT REC.QUEUE_NO, CASE WHEN REC.ORDER_NO IS NULL THEN RCPT.ORDER_NO ELSE REC.ORDER_NO END AS ORDER_NO
                            ,CASE WHEN REC.RCPT_CAT IS NULL THEN RCPT.RECEIPT_CATEGORY ELSE REC.RCPT_CAT END AS RCPT_CAT

                            ,CASE WHEN RCPT.PAYER_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(RCPT.PAYER_TITLE, '...', RCPT.PAYER_FNAME)||' '|| RCPT.PAYER_LNAME)
                            ELSE RCPT.PAYER_TITLE||RCPT.PAYER_FNAME||' '||RCPT.PAYER_LNAME END AS PAYER_NAME

                            ,CASE WHEN REC.PARTY_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(REC.PARTY_TITLE, '...', REC.PARTY_FNAME)||' '|| REC.PARTY_LNAME)
                            ELSE REC.PARTY_TITLE||REC.PARTY_FNAME||' '||REC.PARTY_LNAME END AS PARTY_NAME

                            ,CASE WHEN REC.RCPT_TOTAL_MNY IS NULL THEN RCPT.TOTAL_MNY ELSE REC.RCPT_TOTAL_MNY END AS RCPT_TOTAL_MNY
                            ,RCPT.RECEIPT_NO, RCPT.RECEIPT_DTM
                            ,CASE WHEN RCPT.ORDER_STS = 'C' AND RCPT.RECORD_STATUS = 'C' THEN 'ยกเลิกใบเสร็จ' ELSE 'ชำระเงินแล้ว' END STATUS
                            ,RCPT.RECEIPT_DTM AS RECEIPT_DTM_EN 
                        FROM  FIN.TB_FIN_PAY_RCPT RCPT
                        LEFT JOIN FIN.TB_FIN_ORDER_RECEIPT REC
                            ON REC.ORDER_NO = RCPT.ORDER_NO
                            AND REC.ORDER_DATE = RCPT.ORDER_DTM
                            AND REC.LANDOFFICE_SEQ = RCPT.LANDOFFICE_SEQ
                        WHERE RCPT.LANDOFFICE_SEQ = :landoffice 
                        ".$date_sql
                        .$selectrcpt_sql
                        .$selectsts_sql
                        .$rcpt_sql."
                    )
                SELECT NVL(P1.QUEUE_NO,' ') AS QUEUE_NO_P1 ,  NVL(P2.QUEUE_NO,' ') AS QUEUE_NO_P2
                ,P1.ORDER_NO AS ORDER_NO_P1 ,  P2.ORDER_NO AS ORDER_NO_P2
                ,NVL(P1.RCPT_CAT,' ') AS RCPT_CAT_P1 ,  NVL(P2.RCPT_CAT,' ') AS RCPT_CAT_P2
                ,P1.PAYER_NAME AS PAYER_NAME_P1 ,  P2.PAYER_NAME AS PAYER_NAME_P2
                ,P1.PARTY_NAME AS PARTY_NAME_P1 ,  P2.PARTY_NAME AS PARTY_NAME_P2
                ,NVL(P1.RCPT_TOTAL_MNY,0) AS RCPT_TOTAL_MNY_P1 ,  NVL(P2.RCPT_TOTAL_MNY,0) AS RCPT_TOTAL_MNY_P2
                ,P1.RECEIPT_NO AS RECEIPT_NO_P1 ,  P2.RECEIPT_NO AS RECEIPT_NO_P2
                ,P1.RECEIPT_DTM AS RECEIPT_DTM_P1 ,  P2.RECEIPT_DTM AS RECEIPT_DTM_P2
                ,P1.STATUS AS STATUS_P1 ,  P2.STATUS AS STATUS_P2
                ,P1.RECEIPT_DTM_EN AS RECEIPT_DTM_EN_P1 ,  P2.RECEIPT_DTM_EN AS RECEIPT_DTM_EN_P2
                FROM P1
                FULL OUTER JOIN P2
                    ON NVL(P1.QUEUE_NO,0) = NVL(P2.QUEUE_NO,0)
                    AND P1.ORDER_NO = P2.ORDER_NO
                    AND P1.RECEIPT_NO = P2.RECEIPT_NO
                    AND P1.RECEIPT_DTM = P2.RECEIPT_DTM
                 ORDER BY TO_DATE(P1.RECEIPT_DTM_EN), P1.RECEIPT_NO
                        ,TO_DATE(P2.RECEIPT_DTM_EN), P2.RECEIPT_NO
                OFFSET 300000 ROWS 
                FETCH NEXT 150000 ROWS ONLY";
		
            // echo $select;         
            $stid = oci_parse($conn, $select); 
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($datestart != '') oci_bind_by_name($stid, ':datestart', $datestart);
            if ($dateend != '') oci_bind_by_name($stid, ':dateend', $dateend);
            if ($rcptstart != '') oci_bind_by_name($stid, ':rcptstart', $rcptstart);
            if ($rcptend != '') oci_bind_by_name($stid, ':rcptend', $rcptend);
            if ($selectrcpt != '') oci_bind_by_name($stid, ':selectrcpt', $selectrcpt);
            if ($selectsts != '') oci_bind_by_name($stid, ':selectsts', $selectsts);
            oci_execute($stid);
        break;

    }
?>