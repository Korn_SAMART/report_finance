<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '6': 
            $select = "SELECT T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO, T1.BOOK59_DATE , T1.PAYER_NAME
                            ,T1.BUDGET_YEAR, T1.BOOK59_STS, T1.STR_TOTAL_MNY AS STR_MNY, T1.REMAIN_MNY AS REMAIN_MNY_
                            ,PLATE_ABBR_NAME, PARCEL_NO, SURVEY_NO, ADDR_TAMBOL, ADDR_AMPHUR, ADDR_PROVINCE , T1.RECEIPT_CATEGORY
                        FROM MGT1.TB_FIN_SUR_BOOK59 T1
                        LEFT OUTER JOIN MGT1.TB_FIN_SUR_PART_INCOME T2
                            ON T1.BOOK59_SEQ  =  T2.BOOK59_SEQ 
                        LEFT OUTER JOIN MGT1.TB_FIN_PAY_TITLE_DEED T3
                            ON T1.TITLE_DEED_SEQ  =  T3.TITLE_DEED_SEQ 
                        WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' 
                        GROUP BY T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO,  T1.BOOK59_DATE, T1.PAYER_NAME
                                ,T1.BUDGET_YEAR, T1.BOOK59_STS, PLATE_ABBR_NAME, PARCEL_NO, T1.REMAIN_MNY , T1.STR_TOTAL_MNY
                                ,SURVEY_NO, ADDR_TAMBOL, ADDR_AMPHUR, ADDR_PROVINCE, T1.RECEIPT_CATEGORY
                        MINUS
                        SELECT T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO, T1.BOOK59_DATE , T1.PAYER_NAME
                            ,T1.BUDGET_YEAR, T1.BOOK59_STS, T1.STR_TOTAL_MNY AS STR_MNY, T1.REMAIN_MNY AS REMAIN_MNY_
                            ,PLATE_ABBR_NAME, PARCEL_NO, SURVEY_NO, ADDR_TAMBOL, ADDR_AMPHUR, ADDR_PROVINCE, T1.RECEIPT_CATEGORY
                        FROM FIN.TB_FIN_SUR_BOOK59 T1
                        LEFT OUTER JOIN FIN.TB_FIN_SUR_PART_INCOME T2
                            ON T1.BOOK59_SEQ  =  T2.BOOK59_SEQ 
                        LEFT OUTER JOIN FIN.TB_FIN_PAY_TITLE_DEED T3
                            ON T1.TITLE_DEED_SEQ  =  T3.TITLE_DEED_SEQ 
                        WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N' 
                        GROUP BY T1.BOOK59_SEQ, T1.RECEIPT_SEQ, T1.LANDOFFICE_SEQ,  T1.BOOK59_NO,  T1.BOOK59_DATE, T1.PAYER_NAME
                                ,T1.BUDGET_YEAR, T1.BOOK59_STS, PLATE_ABBR_NAME, PARCEL_NO, T1.REMAIN_MNY , T1.STR_TOTAL_MNY
                                ,SURVEY_NO, ADDR_TAMBOL, ADDR_AMPHUR, ADDR_PROVINCE, T1.RECEIPT_CATEGORY";
		
            // echo $select;         
            $stid = oci_parse($conn, $select); 
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            oci_execute($stid);
        break;
        case '7': 
            $select = "SELECT REC.QUEUE_NO, CASE WHEN REC.ORDER_NO IS NULL THEN RCPT.ORDER_NO ELSE REC.ORDER_NO END AS ORDER_NO
                        ,CASE WHEN REC.RCPT_CAT IS NULL THEN RCPT.RECEIPT_CATEGORY ELSE REC.RCPT_CAT END AS RCPT_CAT
                        ,CASE WHEN RCPT.PAYER_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(RCPT.PAYER_TITLE, '...', RCPT.PAYER_FNAME)||' '|| RCPT.PAYER_LNAME)
                        ELSE RCPT.PAYER_TITLE||RCPT.PAYER_FNAME||' '||RCPT.PAYER_LNAME END AS PAYER_NAME
                        ,CASE WHEN REC.PARTY_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(REC.PARTY_TITLE, '...', REC.PARTY_FNAME)||' '|| REC.PARTY_LNAME)
                        ELSE REC.PARTY_TITLE||REC.PARTY_FNAME||' '||REC.PARTY_LNAME END AS PARTY_NAME
                        ,CASE WHEN REC.RCPT_TOTAL_MNY IS NULL THEN RCPT.TOTAL_MNY ELSE REC.RCPT_TOTAL_MNY END AS RCPT_TOTAL_MNY
                        ,RCPT.RECEIPT_NO
                        ,CASE WHEN SUBSTR(RCPT.RECEIPT_DTM, -4, 4) > 2400 
                        THEN TO_CHAR(RCPT.RECEIPT_DTM) ELSE TO_CHAR(RCPT.RECEIPT_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECEIPT_DTM
                        ,CASE WHEN RCPT.ORDER_STS = 'C' AND RCPT.RECORD_STATUS = 'C' THEN 'ยกเลิกใบเสร็จ'
                        WHEN RCPT.ORDER_STS <> 'C' AND RCPT.RECORD_STATUS <> 'C' THEN 'ชำระเงินแล้ว' END STATUS
                        ,RCPT.RECEIPT_DTM AS RECEIPT_DTM_EN 
                    FROM  FIN.TB_FIN_PAY_RCPT RCPT
                    LEFT JOIN FIN.TB_FIN_ORDER_RECEIPT REC
                        ON REC.ORDER_NO = RCPT.ORDER_NO
                        AND REC.ORDER_DATE = RCPT.ORDER_DTM
                        AND REC.LANDOFFICE_SEQ = RCPT.LANDOFFICE_SEQ
                    WHERE RCPT.LANDOFFICE_SEQ = :landoffice
                    MINUS
                    SELECT REC.QUEUE_NO, CASE WHEN REC.ORDER_NO IS NULL THEN RCPT.ORDER_NO ELSE REC.ORDER_NO END AS ORDER_NO
                        ,CASE WHEN REC.RCPT_CAT IS NULL THEN RCPT.RECEIPT_CATEGORY ELSE REC.RCPT_CAT END AS RCPT_CAT
                        ,CASE WHEN RCPT.PAYER_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(RCPT.PAYER_TITLE, '...', RCPT.PAYER_FNAME)||' '|| RCPT.PAYER_LNAME)
                        ELSE RCPT.PAYER_TITLE||RCPT.PAYER_FNAME||' '||RCPT.PAYER_LNAME END AS PAYER_NAME
                        ,CASE WHEN REC.PARTY_TITLE LIKE '%...%' THEN TRIM(TRAILING ' ' FROM replace(REC.PARTY_TITLE, '...', REC.PARTY_FNAME)||' '|| REC.PARTY_LNAME)
                        ELSE REC.PARTY_TITLE||REC.PARTY_FNAME||' '||REC.PARTY_LNAME END AS PARTY_NAME
                        ,CASE WHEN REC.RCPT_TOTAL_MNY IS NULL THEN RCPT.TOTAL_MNY ELSE REC.RCPT_TOTAL_MNY END AS RCPT_TOTAL_MNY
                        ,RCPT.RECEIPT_NO
                        ,CASE WHEN SUBSTR(RCPT.RECEIPT_DTM, -4, 4) > 2400 
                        THEN TO_CHAR(RCPT.RECEIPT_DTM) ELSE TO_CHAR(RCPT.RECEIPT_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECEIPT_DTM
                        ,CASE WHEN RCPT.ORDER_STS = 'C' AND RCPT.RECORD_STATUS = 'C' THEN 'ยกเลิกใบเสร็จ'
                        WHEN RCPT.ORDER_STS <> 'C' AND RCPT.RECORD_STATUS <> 'C' THEN 'ชำระเงินแล้ว' END STATUS
                        ,RCPT.RECEIPT_DTM AS RECEIPT_DTM_EN 
                    FROM  FIN.TB_FIN_PAY_RCPT RCPT
                    LEFT JOIN FIN.TB_FIN_ORDER_RECEIPT REC
                        ON REC.ORDER_NO = RCPT.ORDER_NO
                        AND REC.ORDER_DATE = RCPT.ORDER_DTM
                        AND REC.LANDOFFICE_SEQ = RCPT.LANDOFFICE_SEQ
                    WHERE RCPT.LANDOFFICE_SEQ = :landoffice ";
		
            // echo $select;         
            $stid = oci_parse($conn, $select); 
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            oci_execute($stid);
        break;
    }
?>