<?php

function numberToRomanRepresentation($number) {
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
}

function getPrintplateTypeName($number) {
    $returnValue = '';
    $map = array(
        1 => 'โฉนดที่ดิน',
        2 => 'โฉนดตราจอง',
        17 => 'ตราจอง',
        3 => 'ตราจองที่ตราว่าได้ทำประโยชน์แล้ว',
        5 => "หนังสือรับรองการทำประโยชน์ (น.ส.3)",
        4 => "หนังสือรับรองการทำประโยชน์ (น.ส.3 ก.)",
        8 => "หนังสือสำคัญสำหรับที่หลวง (น.ส.ล.)",
        23 => "หนังสืออนุญาต",
        13 => "อาคารชุด",
        10 => "หนังสือกรรมสิทธิ์ห้องชุด (อ.ช.2)",
        9 => "ที่สาธารณประโยชน์",
        7 => "หลักฐานการแจ้งการครอบครองที่ดิน (ส.ค.1)",
        6 => "ใบจอง (น.ส.2)",
        11 => "อาคารโรงเรือน",
        15 => "ใบไต่สวน (น.ส.5)",
        16 => "แบบหมายเลข3",
        18 => "โฉนดแผนที่ (จส7)",
        19 => "ก.ส.น.5",
        20 => "น.ค.3",
        21 => "ส.ป.ก.",
        22 => "ที่ราชพัสดุ",
        28 => "ทางสาธารณประโยชน์",
        12 => "อื่นๆ",
        14 => "ไม่มีเอกสาร/หลักฐาน",
        '-1' => "ไม่สามารถแยกประเภทเอกสารได้",
        9999 => "ไม่สามารถแยกประเภทเอกสารได้"
    );
    foreach($map as $seq => $name){
        if($number == $seq){
            $returnValue = $name;
            break;
        }
    }
    return $returnValue;
}

function convertDtm($dtm){
    $str = explode(" ",$dtm);
    $day = $str[0];
    $mon = $str[1];
    $year = $str[count($str)-1] < 2400? intval($str[count($str)-1])+543 : $str[count($str)-1];
    return $day .' '. $mon .' '. $year;
}

