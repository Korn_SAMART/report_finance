<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice = $_POST['landoffice'];

        $selectAmphur = "SELECT AM.AMPHUR_SEQ,AM.AMPHUR_NAME,L.LANDOFFICE_SEQ
                        FROM MAS.TB_MAS_AMPHUR AM
                        INNER JOIN(
                            SELECT AMPHUR_SEQ, LANDOFFICE_SEQ
                            FROM REG.TB_REG_PARCEL 
                            WHERE LANDOFFICE_SEQ = :landoffice AND RECORD_STATUS = 'N'
                            GROUP BY AMPHUR_SEQ, LANDOFFICE_SEQ
                        )L
                            ON AM.AMPHUR_SEQ = L.AMPHUR_SEQ
                        WHERE AM.RECORD_STATUS = 'N'
                        ORDER BY NLSSORT(AM.AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY')";

    $stid = oci_parse($conn, $selectAmphur);
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
