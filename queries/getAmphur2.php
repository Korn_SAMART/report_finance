<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice = $_REQUEST['landoffice'];

    $sql = "SELECT AMPHUR_SEQ, AMPHUR_NAME ";
    $sql .= "FROM MAS.TB_MAS_AMPHUR AP ";
    $sql .= "WHERE RECORD_STATUS = 'N' AND EXISTS ( ";
    $sql .= "SELECT PROVINCE_SEQ FROM MAS.TB_MAS_LANDOFFICE ";
    $sql .= "WHERE PROVINCE_SEQ = AP.PROVINCE_SEQ AND LANDOFFICE_SEQ = :landoffice) ";
    $sql .= "ORDER BY NLSSORT(AP.AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY')";

    // echo $sql."\n";
    $stid = oci_parse($conn, $sql);
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
