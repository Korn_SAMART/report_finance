<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice = $_REQUEST['landoffice'];
    $start = isset($_REQUEST['start'])? $_REQUEST['start'] : "";
    $end = isset($_REQUEST['end'])? $_REQUEST['end'] : date("Y-m-d");

    $condition = "";
    if($start!="") $condition .= "AND Q.MANAGE_QUEUE_DTM BETWEEN TO_DATE(:dtmStart,'YYYY-MM-DD') AND TO_DATE(:dtmEnd,'YYYY-MM-DD') ";

    if($_REQUEST['n']==1){
        $sql = "WITH P2 AS (
                SELECT B.BOOK_ACC_SEQ, Q.MANAGE_QUEUE_DTM, Q.MANAGE_QUEUE_NO, P.PROCESS_REGIST_NAME
                    ,R.REQUEST_SEQ, B.PROCESS_SEQ, B.BOOK_TYPE_SEQ, B.BOOK_STS_SEQ, B.BOOK_ACC_INFORM_DTM, R.REQUEST_FIXED
                FROM REG.TB_REG_BOOK_ACC B
                INNER JOIN REG.TB_REG_PROCESS P
                    ON P.PROCESS_SEQ = B.PROCESS_SEQ
                    AND P.RECORD_STATUS = 'N'
                INNER JOIN REG.TB_REG_REQUEST R
                    ON R.REQUEST_SEQ = P.REQUEST_SEQ
                    AND R.RECORD_STATUS = 'N'
                INNER JOIN REG.TB_REG_MANAGE_QUEUE Q
                    ON Q.REQUEST_TEMP_SEQ = R.REQUEST_SEQ
                    AND Q.RECORD_STATUS = 'N'
                WHERE B.LANDOFFICE_SEQ = :landoffice AND B.RECORD_STATUS = 'N'
                    AND B.BOOK_STS_SEQ IN (15,1) --AND R.REQUEST_FIXED = 0
                ".$condition." 
                ORDER BY Q.MANAGE_QUEUE_DTM DESC, MANAGE_QUEUE_NO DESC
            ), P1 AS (
                SELECT B.BOOK_ACC_SEQ, Q.MANAGE_QUEUE_DTM, Q.MANAGE_QUEUE_NO, P.PROCESS_REGIST_NAME
                    ,R.REQUEST_SEQ, B.PROCESS_SEQ, B.BOOK_TYPE_SEQ, B.BOOK_STS_SEQ, B.BOOK_ACC_INFORM_DTM, R.REQUEST_FIXED
                FROM MGT1.TB_REG_BOOK_ACC B
                INNER JOIN MGT1.TB_REG_PROCESS P
                    ON P.PROCESS_SEQ = B.PROCESS_SEQ
                    AND P.RECORD_STATUS = 'N'
                INNER JOIN MGT1.TB_REG_REQUEST R
                    ON R.REQUEST_SEQ = P.REQUEST_SEQ
                    AND R.RECORD_STATUS = 'N'
                INNER JOIN MGT1.TB_REG_MANAGE_QUEUE Q
                    ON Q.REQUEST_TEMP_SEQ = R.REQUEST_SEQ
                    AND Q.RECORD_STATUS = 'N'
                WHERE B.LANDOFFICE_SEQ = :landoffice AND B.RECORD_STATUS = 'N'
                    AND B.BOOK_STS_SEQ IN (15,1) --AND R.REQUEST_FIXED = 0
                    ".$condition." 
                ORDER BY Q.MANAGE_QUEUE_DTM DESC, MANAGE_QUEUE_NO DESC
            )
            SELECT DISTINCT P1.BOOK_ACC_SEQ, P1.MANAGE_QUEUE_DTM, P1.MANAGE_QUEUE_NO, P1.PROCESS_REGIST_NAME
                ,P1.REQUEST_SEQ, P1.PROCESS_SEQ, P1.BOOK_TYPE_SEQ, P1.BOOK_STS_SEQ
                ,P1.BOOK_ACC_INFORM_DTM, P1.REQUEST_FIXED
                ,P2.BOOK_ACC_SEQ AS BOOK_ACC_SEQ_1, P2.MANAGE_QUEUE_DTM AS MANAGE_QUEUE_DTM_1, P2.MANAGE_QUEUE_NO AS MANAGE_QUEUE_NO_1, P2.PROCESS_REGIST_NAME AS PROCESS_REGIST_NAME_1
                ,P2.REQUEST_SEQ AS REQUEST_SEQ_1, P2.PROCESS_SEQ AS PROCESS_SEQ_1, P2.BOOK_TYPE_SEQ AS BOOK_TYPE_SEQ_1, P2.BOOK_STS_SEQ AS BOOK_STS_SEQ_1
                ,P2.BOOK_ACC_INFORM_DTM AS BOOK_ACC_INFORM_DTM_1, P2.REQUEST_FIXED AS REQUEST_FIXED_1
            FROM P2
            LEFT JOIN P1
                ON P1.BOOK_ACC_SEQ = P2.BOOK_ACC_SEQ 
            ORDER BY NVL(P2.MANAGE_QUEUE_DTM,P1.MANAGE_QUEUE_DTM) DESC, NVL(P2.MANAGE_QUEUE_NO,P1.MANAGE_QUEUE_NO) DESC";
    } else if ($_REQUEST['n']==2){
        $sql = "WITH P2 AS (    
                    SELECT P.PROCESS_SEQ, PARCEL_NO, PARCEL_LAND_NO, CONDOROOM_RNO, CONSTRUCT_ADDR_HNO
                        ,NVL(NVL(NVL(PC.PRINTPLATE_TYPE_SEQ,PCL.PRINTPLATE_TYPE_SEQ),CD.PRINTPLATE_TYPE_SEQ),CTR.PRINTPLATE_TYPE_SEQ) AS PRINTPLATE_TYPE_SEQ
                        ,PP.PROCESS_PARCEL_TEMP_SEQ
                    FROM REG.TB_REG_BOOK_ACC B
                    INNER JOIN REG.TB_REG_PROCESS P
                        ON P.PROCESS_SEQ = B.PROCESS_SEQ
                        AND P.RECORD_STATUS = 'N'
                    LEFT JOIN REG.TB_REG_PROCESS_PARCEL_TEMP PP
                        ON PP.PROCESS_TEMP_SEQ = P.PROCESS_SEQ
                        AND PP.RECORD_STATUS = 'N'
                    LEFT JOIN REG.TB_REG_PARCEL PC
                        ON PC.PARCEL_SEQ = PP.PARCEL_SEQ
                    LEFT JOIN REG.TB_REG_PARCEL_LAND PCL
                        ON PCL.PARCEL_LAND_SEQ = PP.PARCEL_LAND_SEQ
                    LEFT JOIN REG.TB_REG_CONDOROOM CD
                        ON CD.CONDOROOM_SEQ = PP.CONDOROOM_SEQ
                    LEFT JOIN REG.TB_REG_CONSTRUCT CTR
                        ON CTR.CONSTRUCT_SEQ = PP.CONSTRUCT_SEQ
                    LEFT JOIN REG.TB_REG_CONSTRUCT_ADDR ADDR
                        ON ADDR.CONSTRUCT_ADDR_SEQ = CTR.CONSTRUCT_ADDR_SEQ
                    WHERE B.LANDOFFICE_SEQ = :landoffice AND B.RECORD_STATUS = 'N'
                        AND B.BOOK_STS_SEQ IN (15,1)
                        ".$condition." 
                ), P1 AS (
                    SELECT P.PROCESS_SEQ, PARCEL_NO, PARCEL_LAND_NO, CONDOROOM_RNO, CONSTRUCT_ADDR_HNO
                        ,NVL(NVL(NVL(PC.PRINTPLATE_TYPE_SEQ,PCL.PRINTPLATE_TYPE_SEQ),CD.PRINTPLATE_TYPE_SEQ),CTR.PRINTPLATE_TYPE_SEQ) AS PRINTPLATE_TYPE_SEQ
                        ,PP.PROCESS_PARCEL_TEMP_SEQ
                    FROM MGT1.TB_REG_BOOK_ACC B
                    INNER JOIN MGT1.TB_REG_PROCESS P
                        ON P.PROCESS_SEQ = B.PROCESS_SEQ
                        AND P.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_REG_PROCESS_PARCEL_TEMP PP
                        ON PP.PROCESS_TEMP_SEQ = P.PROCESS_SEQ
                        AND PP.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_REG_PARCEL PC
                        ON PC.PARCEL_SEQ = PP.PARCEL_SEQ
                    LEFT JOIN MGT1.TB_REG_PARCEL_LAND PCL
                        ON PCL.PARCEL_LAND_SEQ = PP.PARCEL_LAND_SEQ
                    LEFT JOIN MGT1.TB_REG_CONDOROOM CD
                        ON CD.CONDOROOM_SEQ = PP.CONDOROOM_SEQ
                    LEFT JOIN MGT1.TB_REG_CONSTRUCT CTR
                        ON CTR.CONSTRUCT_SEQ = PP.CONSTRUCT_SEQ
                    LEFT JOIN MGT1.TB_REG_CONSTRUCT_ADDR ADDR
                        ON ADDR.CONSTRUCT_ADDR_SEQ = CTR.CONSTRUCT_ADDR_SEQ
                    WHERE B.LANDOFFICE_SEQ = :landoffice AND B.RECORD_STATUS = 'N'
                        AND B.BOOK_STS_SEQ IN (15,1)
                        ".$condition." 
                )
                SELECT DISTINCT P1.PROCESS_SEQ, P1.PARCEL_NO, P1.PARCEL_LAND_NO, P1.CONDOROOM_RNO, P1.CONSTRUCT_ADDR_HNO, P1.PRINTPLATE_TYPE_SEQ, P1.PROCESS_PARCEL_TEMP_SEQ
                    ,P2.PROCESS_SEQ AS PROCESS_SEQ_1, P2.PARCEL_NO AS PRACEL_NO_1, P2.PARCEL_LAND_NO AS PARCEL_LAND_NO_1, P2.CONDOROOM_RNO AS CONDOROOM_RNO_1
                    ,P2.CONSTRUCT_ADDR_HNO AS CONSTRUCT_ADDR_HNO_1, P2.PRINTPLATE_TYPE_SEQ AS PRINTPLATE_TYPE_SEQ_1, P2.PROCESS_PARCEL_TEMP_SEQ AS PROCESS_PARCEL_TEMP_SEQ_1
                FROM P2
                LEFT JOIN P1
                    ON P1.PROCESS_PARCEL_TEMP_SEQ = P2.PROCESS_PARCEL_TEMP_SEQ ";
    } else if ($_REQUEST['n']==3){
        $sql = "WITH P2 AS (
                SELECT P.PROCESS_SEQ, TITLE_NAME, PROCESS_PROMISOR_TEMP_FNAME, PROCESS_PROMISOR_TEMP_MNAME, PROCESS_PROMISOR_TEMP_LNAME
                    ,PROCESS_PROMISOR_TEMP_SEQ
                FROM REG.TB_REG_BOOK_ACC B
                INNER JOIN REG.TB_REG_PROCESS P
                    ON P.PROCESS_SEQ = B.PROCESS_SEQ
                    AND P.RECORD_STATUS = 'N'
                LEFT JOIN REG.TB_REG_PROCESS_PROMISOR_TEMP SOR
                    ON SOR.PROCESS_TEMP_SEQ = P.PROCESS_SEQ
                    AND SOR.RECORD_STATUS = 'N'
                LEFT JOIN MAS.TB_MAS_TITLE T
                    ON T.TITLE_SEQ = SOR.TITLE_SEQ
                WHERE B.LANDOFFICE_SEQ = :landoffice AND B.RECORD_STATUS = 'N'
                    AND B.BOOK_STS_SEQ IN (15,1)
                    ".$condition." 
                ORDER BY PROCESS_SEQ, PROCESS_PROMISOR_TEMP_ORDER
            ), P1 AS (
                SELECT P.PROCESS_SEQ, TITLE_NAME, PROCESS_PROMISOR_TEMP_FNAME, PROCESS_PROMISOR_TEMP_MNAME, PROCESS_PROMISOR_TEMP_LNAME
                    ,PROCESS_PROMISOR_TEMP_SEQ
                FROM MGT1.TB_REG_BOOK_ACC B
                INNER JOIN MGT1.TB_REG_PROCESS P
                    ON P.PROCESS_SEQ = B.PROCESS_SEQ
                    AND P.RECORD_STATUS = 'N'
                LEFT JOIN MGT1.TB_REG_PROCESS_PROMISOR_TEMP SOR
                    ON SOR.PROCESS_TEMP_SEQ = P.PROCESS_SEQ
                    AND SOR.RECORD_STATUS = 'N'
                LEFT JOIN MAS.TB_MAS_TITLE T
                    ON T.TITLE_SEQ = SOR.TITLE_SEQ
                WHERE B.LANDOFFICE_SEQ = :landoffice AND B.RECORD_STATUS = 'N'
                    AND B.BOOK_STS_SEQ IN (15,1)
                    ".$condition." 
                ORDER BY PROCESS_SEQ, PROCESS_PROMISOR_TEMP_ORDER
            )
            SELECT DISTINCT P1.PROCESS_SEQ, P1.TITLE_NAME, P1.PROCESS_PROMISOR_TEMP_FNAME, P1.PROCESS_PROMISOR_TEMP_MNAME, P1.PROCESS_PROMISOR_TEMP_LNAME
                ,P1.PROCESS_PROMISOR_TEMP_SEQ
                ,P2.PROCESS_SEQ AS PROCESS_SEQ_1, P1.TITLE_NAME AS TITLE_NAME_1, P1.PROCESS_PROMISOR_TEMP_FNAME AS PROCESS_PROMISOR_TEMP_FNAME_1
                ,P1.PROCESS_PROMISOR_TEMP_MNAME AS PROCESS_PROMISOR_TEMP_MNAME_1, P1.PROCESS_PROMISOR_TEMP_LNAME AS PROCESS_PROMISOE_TEMP_LNAME_1
                ,P1.PROCESS_PROMISOR_TEMP_SEQ AS PROCESS_PROMISOR_TEMP_SEQ_1
            FROM P2
            LEFT JOIN P1
                ON P1.PROCESS_PROMISOR_TEMP_SEQ = P2.PROCESS_PROMISOR_TEMP_SEQ ";
    }
    

    // echo $sql."\n";
    $stid = oci_parse($conn, $sql);
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    if($start!="") {
        oci_bind_by_name($stid, ':dtmStart', $start);
        oci_bind_by_name($stid, ':dtmEnd', $end);
    }
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
