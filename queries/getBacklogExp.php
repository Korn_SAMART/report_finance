<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice = $_REQUEST['landoffice'];
    $start = isset($_REQUEST['start'])? $_REQUEST['start'] : "";
    $end = isset($_REQUEST['end'])? $_REQUEST['end'] : date("Y-m-d");

    $condition = "";
    if($start!="") $condition .= " AND J.DOC_RECEIVE_DATE BETWEEN TO_DATE(:dtmStart,'YYYY-MM-DD') AND TO_DATE(:dtmEnd,'YYYY-MM-DD') ";

    $sql = "WITH P1 AS(
            SELECT DISTINCT
                    J.JOB_SEQ,J.LANDOFFICE_SEQ,J.DOC_RECEIVE_DATE,J.DOC_RECEIVE_NO,J.TOPIC,
                    CASE 
                        WHEN J.END_JOB_FLAG = 1 THEN 'ยุติ'
                        WHEN J.END_JOB_FLAG=2 THEN 'จำหน่าย'
                        WHEN J.END_JOB_FLAG=2 THEN 'ยกเลิก'
                        WHEN J.END_JOB_FLAG IN(0,4) THEN 'ยังไม่ยุติ'
                        ELSE J.END_JOB_FLAG 
                    END JOB_STATUS,
                J.END_JOB_DATE, J.RECORD_STATUS
                FROM MGT1.TB_EXP_JOB J 
            WHERE landoffice_seq IN(
                SELECT landoffice_seq
                FROM mas.tb_mas_landoffice
                WHERE(landoffice_seq=:landoffice OR (ref_landoffice_seq=:landoffice AND landoffice_type_seq NOT IN(6,7,8,9))) AND record_status='N') ".$condition." 
    ), P2 AS (
            SELECT DISTINCT
                    J.JOB_SEQ,J.LANDOFFICE_SEQ,J.DOC_RECEIVE_DATE,J.DOC_RECEIVE_NO,J.TOPIC,
                    CASE 
                        WHEN J.END_JOB_FLAG = 1 THEN 'ยุติ'
                        WHEN J.END_JOB_FLAG=2 THEN 'จำหน่าย'
                        WHEN J.END_JOB_FLAG=2 THEN 'ยกเลิก'
                        WHEN J.END_JOB_FLAG IN(0,4) THEN 'ยังไม่ยุติ'
                        ELSE J.END_JOB_FLAG 
                    END JOB_STATUS,
                J.END_JOB_DATE, J.RECORD_STATUS
                FROM EXP.TB_EXP_JOB J 
            WHERE landoffice_seq IN(
                SELECT landoffice_seq
                FROM mas.tb_mas_landoffice
                WHERE(landoffice_seq=:landoffice OR (ref_landoffice_seq=:landoffice AND landoffice_type_seq NOT IN(6,7,8,9))) AND record_status='N') ".$condition." 
    )
    SELECT P1.JOB_SEQ AS JOB_SEQ_P1 ,  P2.JOB_SEQ AS JOB_SEQ_P2
    ,P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 ,  P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
    ,P1.DOC_RECEIVE_DATE AS DOC_RECEIVE_DATE_P1 ,  P2.DOC_RECEIVE_DATE AS DOC_RECEIVE_DATE_P2
    ,P1.DOC_RECEIVE_NO AS DOC_RECEIVE_NO_P1 ,  P2.DOC_RECEIVE_NO AS DOC_RECEIVE_NO_P2
    ,P1.JOB_STATUS AS JOB_STATUS_P1 ,  P2.JOB_STATUS AS JOB_STATUS_P2
    ,P1.END_JOB_DATE AS END_JOB_DATE_P1 ,  P2.END_JOB_DATE AS END_JOB_DATE_P2
    ,P1.TOPIC AS JOB_NAME_P1 ,  P2.TOPIC AS JOB_NAME_P2

    
    FROM P1
    FULL OUTER JOIN P2
        ON P1.JOB_SEQ = P2.JOB_SEQ
    ORDER BY NVL(P2.DOC_RECEIVE_DATE,P1.DOC_RECEIVE_DATE) ASC , NVL(P1.DOC_RECEIVE_DATE,P2.DOC_RECEIVE_DATE) ASC";
    

    // echo $sql."\n";
    $stid = oci_parse($conn, $sql);
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    if($start!="") {
        oci_bind_by_name($stid, ':dtmStart', $start);
        oci_bind_by_name($stid, ':dtmEnd', $end);
    }
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }

    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
