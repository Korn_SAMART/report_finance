<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice = $_REQUEST['landoffice'];
    $start = isset($_REQUEST['start'])? $_REQUEST['start'] : "";
    $end = isset($_REQUEST['end'])? $_REQUEST['end'] : date("Y-m-d");

    $condition = "";
    if($start!="") $condition .= "AND Q.MANAGE_QUEUE_DTM BETWEEN TO_DATE(:dtmStart,'YYYY-MM-DD') AND TO_DATE(:dtmEnd,'YYYY-MM-DD') ";

    if($_REQUEST['n']==1){
        $sql = "SELECT 
        P1.REQ_QUEUE_NO AS REQ_QUEUE_NO_P1, P2.REQ_QUEUE_NO AS REQ_QUEUE_NO_P2,
             CASE WHEN SUBSTR(P1.REQ_QUEUE_DATE, -4, 4) > 2500 
             THEN TO_CHAR(P1.REQ_QUEUE_DATE) ELSE TO_CHAR(P1.REQ_QUEUE_DATE, 'DD MON YYYY', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REQ_QUEUE_DATE_P1,
             CASE WHEN SUBSTR(P2.REQ_QUEUE_DATE, -4, 4) > 2500 
             THEN TO_CHAR(P2.REQ_QUEUE_DATE) ELSE TO_CHAR(P2.REQ_QUEUE_DATE, 'DD MON YYYY', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS REQ_QUEUE_DATE_P2,
             P1.SURVEYJOB_NO AS SURVEYJOB_NO_P1, P2.SURVEYJOB_NO AS SURVEYJOB_NO_P2,
             P1.OWNER_REQUEST AS OWNER_REQUEST_P1, P2.OWNER_REQUEST AS OWNER_REQUEST_P2,
             P1.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P1, P2.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P2,
             P1.HALT_DETAIL AS HALT_DETAIL_P1, P2.HALT_DETAIL AS HALT_DETAIL_P2
     FROM
         (
     SELECT 
         SJ.SURVEYJOB_SEQ,SJ.REQ_QUEUE_NO,SJ.REQ_QUEUE_DATE,SJ.SURVEYJOB_NO,SO.OWNER_FNAME||' '||SO.OWNER_LNAME AS OWNER_REQUEST,ST.TYPEOFSURVEY_NAME,HS.HALT_DETAIL
     FROM SVO.TB_SVA_SURVEYJOB SJ
     LEFT OUTER JOIN (
         SELECT SURVEYJOB_SEQ, SURVEYOR_SEQ, SURVEYCHARGE_SEQ, APPOINTMENT_SEQ, APPOINTMENT_DTM 
         FROM SVO.TB_SVA_APPOINTMENT 
         WHERE RECORD_STATUS = 'N' AND (CANCEL_FLAG <> '1' OR CANCEL_FLAG IS NULL)
     ) AP 
         ON SJ.SURVEYJOB_SEQ = AP.SURVEYJOB_SEQ
     LEFT OUTER JOIN SVO.TB_SVA_HALTSURVEY HS 
         ON 
             HS.SURVEYJOB_SEQ = SJ.SURVEYJOB_SEQ 
         AND HS.RECORD_STATUS = 'N'
     LEFT OUTER JOIN SVO.TB_SVA_MAS_TYPEOFSURVEY ST
         ON SJ.TYPEOFSURVEY_SEQ = ST.TYPEOFSURVEY_SEQ
     LEFT OUTER JOIN (
         SELECT O.OWNER_FNAME,O.OWNER_LNAME,O.SURVEYJOB_SEQ
         FROM SVO.TB_SVA_OWNER O
         INNER JOIN (
         SELECT MIN(OWNER_FIRST_FLAG) AS OWNER_FIRST_FLAG,SURVEYJOB_SEQ
         FROM SVO.TB_SVA_OWNER
         GROUP BY SURVEYJOB_SEQ
         )OO
             ON
                 O.OWNER_FIRST_FLAG = OO.OWNER_FIRST_FLAG
             AND O.SURVEYJOB_SEQ = OO.SURVEYJOB_SEQ
         WHERE O.RECORD_STATUS = 'N'
     )SO
         ON SJ.SURVEYJOB_SEQ = SO.SURVEYJOB_SEQ  
     WHERE 
         SJ.RECORD_STATUS = 'N'  
     AND SJ.SURVEYJOBHALT_FLAG IN ( '1' , '2')
     AND  SJ.SURVEYJOB_SEQ NOT IN (
             SELECT SURVEYJOB_SEQ 
             FROM SVO.TB_SVA_SENDREGISTER 
             WHERE RECORD_STATUS = 'N'
         )
     AND SJ.TYPEOFSURVEY_SEQ <> 4
     AND SJ.JOBSTATUS_SEQ < 17 
     AND SJ.LANDOFFICE_SEQ = :landoffice
     GROUP BY SJ.SURVEYJOB_SEQ,SJ.SURVEYJOB_DATE,SJ.REQ_QUEUE_NO,SJ.REQ_QUEUE_DATE,SJ.SURVEYJOB_NO,SO.OWNER_FNAME||' '||SO.OWNER_LNAME,ST.TYPEOFSURVEY_NAME,HS.HALT_DETAIL
     --ORDER BY SJ.SURVEYJOB_DATE,SJ.SURVEYJOB_NO
     )P2,(
     SELECT 
         SJ.SURVEYJOB_SEQ,SJ.REQ_QUEUE_NO,SJ.REQ_QUEUE_DATE,SJ.SURVEYJOB_NO,SO.OWNER_FNAME||' '||SO.OWNER_LNAME AS OWNER_REQUEST,ST.TYPEOFSURVEY_NAME,HS.HALT_DETAIL
     FROM MGT1.TB_SVA_SURVEYJOB SJ
     LEFT OUTER JOIN (
         SELECT SURVEYJOB_SEQ, SURVEYOR_SEQ, SURVEYCHARGE_SEQ, APPOINTMENT_SEQ, APPOINTMENT_DTM 
         FROM MGT1.TB_SVA_APPOINTMENT 
         WHERE RECORD_STATUS = 'N' AND (CANCEL_FLAG <> '1' OR CANCEL_FLAG IS NULL)
     ) AP 
         ON SJ.SURVEYJOB_SEQ = AP.SURVEYJOB_SEQ
     LEFT OUTER JOIN MGT1.TB_SVA_HALTSURVEY HS 
         ON 
             HS.SURVEYJOB_SEQ = SJ.SURVEYJOB_SEQ 
         AND HS.RECORD_STATUS = 'N'
     LEFT OUTER JOIN MGT1.TB_SVA_MAS_TYPEOFSURVEY ST
         ON SJ.TYPEOFSURVEY_SEQ = ST.TYPEOFSURVEY_SEQ
     LEFT OUTER JOIN (
         SELECT O.OWNER_FNAME,O.OWNER_LNAME,O.SURVEYJOB_SEQ
         FROM MGT1.TB_SVA_OWNER O
         INNER JOIN (
         SELECT MIN(OWNER_FIRST_FLAG) AS OWNER_FIRST_FLAG,SURVEYJOB_SEQ
         FROM MGT1.TB_SVA_OWNER
         GROUP BY SURVEYJOB_SEQ
         )OO
             ON
                 O.OWNER_FIRST_FLAG = OO.OWNER_FIRST_FLAG
             AND O.SURVEYJOB_SEQ = OO.SURVEYJOB_SEQ
         WHERE O.RECORD_STATUS = 'N'
     )SO
         ON SJ.SURVEYJOB_SEQ = SO.SURVEYJOB_SEQ  
     WHERE 
         SJ.RECORD_STATUS = 'N'  
     AND SJ.SURVEYJOBHALT_FLAG IN ( '1' , '2')
     AND  SJ.SURVEYJOB_SEQ NOT IN (
             SELECT SURVEYJOB_SEQ 
             FROM MGT1.TB_SVA_SENDREGISTER 
             WHERE RECORD_STATUS = 'N'
         )
     AND SJ.TYPEOFSURVEY_SEQ <> 4
     AND SJ.JOBSTATUS_SEQ < 17 
     AND SJ.LANDOFFICE_SEQ = :landoffice
     GROUP BY SJ.SURVEYJOB_SEQ,SJ.SURVEYJOB_DATE,SJ.REQ_QUEUE_NO,SJ.REQ_QUEUE_DATE,SJ.SURVEYJOB_NO,SO.OWNER_FNAME||' '||SO.OWNER_LNAME,ST.TYPEOFSURVEY_NAME,HS.HALT_DETAIL
     --ORDER BY SJ.SURVEYJOB_DATE,SJ.SURVEYJOB_NO
     )P1
     WHERE 
         P2.SURVEYJOB_SEQ = P1.SURVEYJOB_SEQ
     AND P2.REQ_QUEUE_NO = P1.REQ_QUEUE_NO
     AND P2.HALT_DETAIL = P1.HALT_DETAIL
     GROUP BY
         P1.REQ_QUEUE_NO,P1.REQ_QUEUE_DATE,P1.SURVEYJOB_NO,P1.OWNER_REQUEST,P1.TYPEOFSURVEY_NAME,P1.HALT_DETAIL,
         P2.REQ_QUEUE_NO,P2.REQ_QUEUE_DATE,P2.SURVEYJOB_NO,P2.OWNER_REQUEST,P2.TYPEOFSURVEY_NAME,P2.HALT_DETAIL
     ORDER BY P2.REQ_QUEUE_DATE,P1.SURVEYJOB_NO
     ";


    }
    else if($_REQUEST['n']==2) {
        $sql = "WITH P1 AS (
                    SELECT DISTINCT
                        JB.SURVEYJOB_SEQ,
                        JB.SURVEYJOB_NO
                    ,   SO.OWNER_NAME--so.owner_fname||' '||so.owner_lname AS owner_name
                    ,   SY.SURVEYOR_FNAME||' '||SY.SURVEYOR_LNAME AS SURVEYOR_NAME
                    ,   MT.TYPEOFSURVEY_NAME
                    ,   AP.APPOINTMENT_DTM
                    ,   MJ.JOBSTATUS_NAME
                    FROM MGT1.TB_SVA_SURVEYJOB JB
                    LEFT JOIN (
                        SELECT A1.*
                        FROM MGT1.TB_SVA_APPOINTMENT A1
                        INNER JOIN (
                            SELECT SURVEYJOB_SEQ,MAX(APPOINTMENT_DTM) AS APPOINTMENT_DTM
                            FROM MGT1.TB_SVA_APPOINTMENT
                            WHERE RECORD_STATUS = 'N' AND (CANCEL_FLAG = '0' OR CANCEL_FLAG IS NULL)
                            GROUP BY SURVEYJOB_SEQ
                        )A2
                            ON 
                                A1.SURVEYJOB_SEQ = A2.SURVEYJOB_SEQ
                            AND A1.APPOINTMENT_DTM = A2.APPOINTMENT_DTM
                    )AP 
                        ON 
                            JB.SURVEYJOB_SEQ = AP.SURVEYJOB_SEQ AND AP.RECORD_STATUS = 'N' AND AP.CANCEL_FLAG = '0' 
                    LEFT JOIN MGT1.TB_SVA_MAS_JOBSTATUS MJ 
                        ON JB.JOBSTATUS_SEQ = MJ.JOBSTATUS_SEQ
                    LEFT OUTER JOIN MGT1.TB_SVA_MAS_TYPEOFSURVEY MT
                        ON JB.TYPEOFSURVEY_SEQ = MT.TYPEOFSURVEY_SEQ
                    LEFT OUTER JOIN MGT1.TB_SVA_SURVEYOR SY
                        ON AP.SURVEYOR_SEQ = SY.SURVEYOR_SEQ
                    LEFT OUTER JOIN (
                        SELECT LISTAGG(O.OWNER_FNAME||' '||O.OWNER_LNAME,', ') WITHIN GROUP(ORDER BY O.SURVEYJOB_SEQ) AS OWNER_NAME,O.SURVEYJOB_SEQ
                        FROM MGT1.TB_SVA_OWNER O
                        INNER JOIN (
                            SELECT MIN(OWNER_FIRST_FLAG) AS OWNER_FIRST_FLAG,SURVEYJOB_SEQ
                            FROM MGT1.TB_SVA_OWNER
                            GROUP BY SURVEYJOB_SEQ
                        )OO
                            ON
                                O.OWNER_FIRST_FLAG = OO.OWNER_FIRST_FLAG
                            AND O.SURVEYJOB_SEQ = OO.SURVEYJOB_SEQ
                        WHERE O.RECORD_STATUS = 'N'
                        GROUP BY O.SURVEYJOB_SEQ
                    )SO
                        ON JB.SURVEYJOB_SEQ = SO.SURVEYJOB_SEQ  
                    WHERE 
                        JB.RECORD_STATUS = 'N'
                    AND JB.LANDOFFICE_SEQ = :landoffice
                    AND JB.JOBGROUP_SEQ = 1
                    AND (JB.SURVEYJOBCANCEL_FLAG  = '0' OR JB.SURVEYJOBCANCEL_FLAG  IS NULL)
                    AND (JB.SURVEYJOBHALT_FLAG  = '0' OR JB.SURVEYJOBHALT_FLAG  IS NULL)
                    --AND jb.jobstatus_seq < 17 
                    AND AP.APPOINTMENT_DTM IS NOT NULL
                    AND JB.JOBSTATUS_SEQ IN (4,5,6,7,8,9,10,11)
                    ORDER BY AP.APPOINTMENT_DTM
                
                ),
                P2 AS (
                    SELECT DISTINCT
                    JB.SURVEYJOB_SEQ,
                        JB.SURVEYJOB_NO
                    ,   SO.OWNER_NAME--so.owner_fname||' '||so.owner_lname AS owner_name
                    ,   SY.SURVEYOR_FNAME||' '||SY.SURVEYOR_LNAME AS SURVEYOR_NAME
                    ,   MT.TYPEOFSURVEY_NAME
                    ,   AP.APPOINTMENT_DTM
                    ,   MJ.JOBSTATUS_NAME
                    FROM SVO.TB_SVA_SURVEYJOB JB
                    LEFT JOIN (
                        SELECT A1.*
                        FROM SVO.TB_SVA_APPOINTMENT A1
                        INNER JOIN (
                            SELECT SURVEYJOB_SEQ,MAX(APPOINTMENT_DTM) AS APPOINTMENT_DTM
                            FROM SVO.TB_SVA_APPOINTMENT
                            WHERE RECORD_STATUS = 'N' AND (CANCEL_FLAG = '0' OR CANCEL_FLAG IS NULL)
                            GROUP BY SURVEYJOB_SEQ
                        )A2
                            ON 
                                A1.SURVEYJOB_SEQ = A2.SURVEYJOB_SEQ
                            AND A1.APPOINTMENT_DTM = A2.APPOINTMENT_DTM
                    )AP 
                        ON 
                            JB.SURVEYJOB_SEQ = AP.SURVEYJOB_SEQ AND AP.RECORD_STATUS = 'N' AND AP.CANCEL_FLAG = '0' 
                    LEFT JOIN SVO.TB_SVA_MAS_JOBSTATUS MJ 
                        ON JB.JOBSTATUS_SEQ = MJ.JOBSTATUS_SEQ
                    LEFT OUTER JOIN SVO.TB_SVA_MAS_TYPEOFSURVEY MT
                        ON JB.TYPEOFSURVEY_SEQ = MT.TYPEOFSURVEY_SEQ
                    LEFT OUTER JOIN SVO.TB_SVA_SURVEYOR SY
                        ON AP.SURVEYOR_SEQ = SY.SURVEYOR_SEQ
                    LEFT OUTER JOIN (
                        SELECT LISTAGG(O.OWNER_FNAME||' '||O.OWNER_LNAME,', ') WITHIN GROUP(ORDER BY O.SURVEYJOB_SEQ) AS OWNER_NAME,O.SURVEYJOB_SEQ
                        FROM SVO.TB_SVA_OWNER O
                        INNER JOIN (
                            SELECT MIN(OWNER_FIRST_FLAG) AS OWNER_FIRST_FLAG,SURVEYJOB_SEQ
                            FROM SVO.TB_SVA_OWNER
                            GROUP BY SURVEYJOB_SEQ
                        )OO
                            ON
                                O.OWNER_FIRST_FLAG = OO.OWNER_FIRST_FLAG
                            AND O.SURVEYJOB_SEQ = OO.SURVEYJOB_SEQ
                        WHERE O.RECORD_STATUS = 'N'
                        GROUP BY O.SURVEYJOB_SEQ
                    )SO
                        ON JB.SURVEYJOB_SEQ = SO.SURVEYJOB_SEQ  
                    WHERE 
                        JB.RECORD_STATUS = 'N'
                    AND JB.LANDOFFICE_SEQ = :landoffice
                    AND JB.JOBGROUP_SEQ = 1
                    AND (JB.SURVEYJOBCANCEL_FLAG  = '0' OR JB.SURVEYJOBCANCEL_FLAG  IS NULL)
                    AND (JB.SURVEYJOBHALT_FLAG  = '0' OR JB.SURVEYJOBHALT_FLAG  IS NULL)
                    --AND jb.jobstatus_seq < 17 
                    AND AP.APPOINTMENT_DTM IS NOT NULL
                    AND JB.JOBSTATUS_SEQ IN (4,5,6,7,8,9,10,11)
                    ORDER BY AP.APPOINTMENT_DTM
                )
                SELECT P1.SURVEYJOB_NO AS SURVEYJOB_NO_P1, P2.SURVEYJOB_NO AS SURVEYJOB_NO_P2,
                P1.OWNER_NAME AS OWNER_NAME_P1, P2.OWNER_NAME AS OWNER_NAME_P2,
                P1.SURVEYOR_NAME AS SURVEYOR_NAME_P1, P2.SURVEYOR_NAME AS SURVEYOR_NAME_P2,
                P1.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P1, P2.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P2,
                CASE WHEN SUBSTR(P1.APPOINTMENT_DTM, -4, 4) > 2500 
                THEN TO_CHAR(P1.APPOINTMENT_DTM) ELSE TO_CHAR(P1.APPOINTMENT_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS APPOINTMENT_DTM_P1,
                CASE WHEN SUBSTR(P2.APPOINTMENT_DTM, -4, 4) > 2500 
                THEN TO_CHAR(P2.APPOINTMENT_DTM) ELSE TO_CHAR(P2.APPOINTMENT_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS APPOINTMENT_DTM_P2,
                P1.JOBSTATUS_NAME AS JOBSTATUS_NAME_P1, P2.JOBSTATUS_NAME AS JOBSTATUS_NAME_P2
                FROM P1
                INNER JOIN P2
                ON P1.SURVEYJOB_SEQ = P2.SURVEYJOB_SEQ";
    }
    else  {
        $sql = "WITH P1 AS (
            SELECT DISTINCT
                                JB.SURVEYJOB_NO
                            ,   JB.SURVEYJOB_SEQ
                            ,   SO.OWNER_NAME
                            ,   SY.PRIVATE_SVY_NAME
                            ,   SY.PRIVATE_OFFICE_NAME
                            ,   MT.TYPEOFSURVEY_NAME
                            ,   AP.APPOINTMENT_DTM
                            ,   MJ.JOBSTATUS_NAME
                            FROM MGT1.TB_SVA_SURVEYJOB JB
                            LEFT JOIN (
                                SELECT A1.*
                                FROM MGT1.TB_SVA_APPOINTMENT A1
                                INNER JOIN (
                                    SELECT SURVEYJOB_SEQ,MAX(APPOINTMENT_DTM) AS APPOINTMENT_DTM
                                    FROM MGT1.TB_SVA_APPOINTMENT
                                    WHERE RECORD_STATUS = 'N' AND (CANCEL_FLAG = '0' OR CANCEL_FLAG IS NULL)
                                    GROUP BY SURVEYJOB_SEQ
                                )A2
                                    ON 
                                        A1.SURVEYJOB_SEQ = A2.SURVEYJOB_SEQ
                                    AND A1.APPOINTMENT_DTM = A2.APPOINTMENT_DTM
                            )AP 
                                ON 
                                    JB.SURVEYJOB_SEQ = AP.SURVEYJOB_SEQ AND AP.RECORD_STATUS = 'N' AND AP.CANCEL_FLAG = '0' 
                            LEFT JOIN MGT1.TB_SVA_MAS_JOBSTATUS MJ 
                                ON JB.JOBSTATUS_SEQ = MJ.JOBSTATUS_SEQ
                            LEFT OUTER JOIN MGT1.TB_SVA_MAS_TYPEOFSURVEY MT
                                ON JB.TYPEOFSURVEY_SEQ = MT.TYPEOFSURVEY_SEQ
                            LEFT OUTER JOIN (
                                SELECT Y.PRIVATE_SURVEYOR_SEQ,Y.PRIVATE_SVY_FNAME||' '||Y.PRIVATE_SVY_LNAME AS PRIVATE_SVY_NAME,O.PRIVATE_OFFICE_NAME
                                FROM MGT1.TB_SVA_MAS_PRIVATE_SURVEYOR Y
                                LEFT OUTER JOIN MGT1.TB_SVA_MAS_PRIVATE_OFFICE O
                                    ON O.PRIVATE_OFFICE_SEQ = Y.PRIVATE_OFFICE_SEQ
                            )SY
                                ON AP.PRIVATE_SURVEYOR_SEQ = SY.PRIVATE_SURVEYOR_SEQ
                            LEFT OUTER JOIN (
                                SELECT LISTAGG(O.OWNER_FNAME||' '||O.OWNER_LNAME,', ') WITHIN GROUP(ORDER BY O.SURVEYJOB_SEQ) AS OWNER_NAME,O.SURVEYJOB_SEQ
                                FROM MGT1.TB_SVA_OWNER O
                                INNER JOIN (
                            SELECT MIN(OWNER_FIRST_FLAG) AS OWNER_FIRST_FLAG,SURVEYJOB_SEQ
                            FROM MGT1.TB_SVA_OWNER
                            GROUP BY SURVEYJOB_SEQ
                                )OO
                                    ON
                                        O.OWNER_FIRST_FLAG = OO.OWNER_FIRST_FLAG
                                    AND O.SURVEYJOB_SEQ = OO.SURVEYJOB_SEQ
                                WHERE O.RECORD_STATUS = 'N'
                                GROUP BY O.SURVEYJOB_SEQ
                            )SO
                                ON JB.SURVEYJOB_SEQ = SO.SURVEYJOB_SEQ  
                            WHERE 
                                JB.RECORD_STATUS = 'N'
                            AND JB.LANDOFFICE_SEQ = :landoffice
                            AND JB.JOBGROUP_SEQ = 2
                            AND (JB.SURVEYJOBCANCEL_FLAG  = '0' OR JB.SURVEYJOBCANCEL_FLAG  IS NULL)
                            AND (JB.SURVEYJOBHALT_FLAG  = '0' OR JB.SURVEYJOBHALT_FLAG  IS NULL)
                            --AND JB.JOBSTATUS_SEQ < 17 
                            AND AP.APPOINTMENT_DTM IS NOT NULL
                            AND JB.JOBSTATUS_SEQ IN (4,5,6,7,8,9,10,11)
                            ORDER BY AP.APPOINTMENT_DTM
            
            
            ),
            P2 AS (
            SELECT DISTINCT
                                JB.SURVEYJOB_NO
                            ,   JB.SURVEYJOB_SEQ
                            ,   SO.OWNER_NAME
                            ,   SY.PRIVATE_SVY_NAME
                            ,   SY.PRIVATE_OFFICE_NAME
                            ,   MT.TYPEOFSURVEY_NAME
                            ,   AP.APPOINTMENT_DTM
                            ,   MJ.JOBSTATUS_NAME
                            FROM SVO.TB_SVA_SURVEYJOB JB
                            LEFT JOIN (
                                SELECT A1.*
                                FROM SVO.TB_SVA_APPOINTMENT A1
                                INNER JOIN (
                                    SELECT SURVEYJOB_SEQ,MAX(APPOINTMENT_DTM) AS APPOINTMENT_DTM
                                    FROM SVO.TB_SVA_APPOINTMENT
                                    WHERE RECORD_STATUS = 'N' AND (CANCEL_FLAG = '0' OR CANCEL_FLAG IS NULL)
                                    GROUP BY SURVEYJOB_SEQ
                                )A2
                                    ON 
                                        A1.SURVEYJOB_SEQ = A2.SURVEYJOB_SEQ
                                    AND A1.APPOINTMENT_DTM = A2.APPOINTMENT_DTM
                            )AP 
                                ON 
                                    JB.SURVEYJOB_SEQ = AP.SURVEYJOB_SEQ AND AP.RECORD_STATUS = 'N' AND AP.CANCEL_FLAG = '0' 
                            LEFT JOIN SVO.TB_SVA_MAS_JOBSTATUS MJ 
                                ON JB.JOBSTATUS_SEQ = MJ.JOBSTATUS_SEQ
                            LEFT OUTER JOIN SVO.TB_SVA_MAS_TYPEOFSURVEY MT
                                ON JB.TYPEOFSURVEY_SEQ = MT.TYPEOFSURVEY_SEQ
                            LEFT OUTER JOIN (
                                SELECT Y.PRIVATE_SURVEYOR_SEQ,Y.PRIVATE_SVY_FNAME||' '||Y.PRIVATE_SVY_LNAME AS PRIVATE_SVY_NAME,O.PRIVATE_OFFICE_NAME
                                FROM SVO.TB_SVA_MAS_PRIVATE_SURVEYOR Y
                                LEFT OUTER JOIN SVO.TB_SVA_MAS_PRIVATE_OFFICE O
                                    ON O.PRIVATE_OFFICE_SEQ = Y.PRIVATE_OFFICE_SEQ
                            )SY
                                ON AP.PRIVATE_SURVEYOR_SEQ = SY.PRIVATE_SURVEYOR_SEQ
                            LEFT OUTER JOIN (
                                SELECT LISTAGG(O.OWNER_FNAME||' '||O.OWNER_LNAME,', ') WITHIN GROUP(ORDER BY O.SURVEYJOB_SEQ) AS OWNER_NAME,O.SURVEYJOB_SEQ
                                FROM SVO.TB_SVA_OWNER O
                                INNER JOIN (
                            SELECT MIN(OWNER_FIRST_FLAG) AS OWNER_FIRST_FLAG,SURVEYJOB_SEQ
                            FROM SVO.TB_SVA_OWNER
                            GROUP BY SURVEYJOB_SEQ
                                )OO
                                    ON
                                        O.OWNER_FIRST_FLAG = OO.OWNER_FIRST_FLAG
                                    AND O.SURVEYJOB_SEQ = OO.SURVEYJOB_SEQ
                                WHERE O.RECORD_STATUS = 'N'
                                GROUP BY O.SURVEYJOB_SEQ
                            )SO
                                ON JB.SURVEYJOB_SEQ = SO.SURVEYJOB_SEQ  
                            WHERE 
                                JB.RECORD_STATUS = 'N'
                            AND JB.LANDOFFICE_SEQ = :landoffice
                            AND JB.JOBGROUP_SEQ = 2
                            AND (JB.SURVEYJOBCANCEL_FLAG  = '0' OR JB.SURVEYJOBCANCEL_FLAG  IS NULL)
                            AND (JB.SURVEYJOBHALT_FLAG  = '0' OR JB.SURVEYJOBHALT_FLAG  IS NULL)
                            --AND JB.JOBSTATUS_SEQ < 17 
                            AND AP.APPOINTMENT_DTM IS NOT NULL
                            AND JB.JOBSTATUS_SEQ IN (4,5,6,7,8,9,10,11)
                            ORDER BY AP.APPOINTMENT_DTM
            )
             SELECT P1.SURVEYJOB_NO AS SURVEYJOB_NO_P1, P2.SURVEYJOB_NO AS SURVEYJOB_NO_P2,
                            P1.OWNER_NAME AS OWNER_NAME_P1, P2.OWNER_NAME AS OWNER_NAME_P2,
                            P1.PRIVATE_SVY_NAME AS PRIVATE_SVY_NAME_P1, P2.PRIVATE_SVY_NAME AS PRIVATE_SVY_NAME_P2,
                            P1.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P1, P2.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P2,
                            CASE WHEN SUBSTR(P1.APPOINTMENT_DTM, -4, 4) > 2500 
                            THEN TO_CHAR(P1.APPOINTMENT_DTM) ELSE TO_CHAR(P1.APPOINTMENT_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS APPOINTMENT_DTM_P1,
                            CASE WHEN SUBSTR(P2.APPOINTMENT_DTM, -4, 4) > 2500 
                            THEN TO_CHAR(P2.APPOINTMENT_DTM) ELSE TO_CHAR(P2.APPOINTMENT_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS APPOINTMENT_DTM_P2,
                            P1.JOBSTATUS_NAME AS JOBSTATUS_NAME_P1, P2.JOBSTATUS_NAME AS JOBSTATUS_NAME_P2
                            FROM P1
                            INNER JOIN P2
                            ON P1.SURVEYJOB_SEQ = P2.SURVEYJOB_SEQ";
    }

    
    $stid = oci_parse($conn, $sql);
    oci_bind_by_name($stid, ':landoffice', $landoffice);

    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }

    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
