<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice     = $_REQUEST['landoffice'];
    $printplateType = $_REQUEST['printplateType'];
    $no             = isset($_REQUEST['no']) ? $_REQUEST['no'] : "";
    $sheet          = isset($_REQUEST['sheet']) ? $_REQUEST['sheet'] : "";
    $box            = isset($_REQUEST['box']) ? $_REQUEST['box'] : "";
    $num            = isset($_REQUEST['num']) ? $_REQUEST['num'] : "";

    $condition = "";

    //MUST TO HAVE
    $condition .= "WHERE C.LANDOFFICE_SEQ = :landoffice AND C.RECORD_STATUS = 'N' ";
    $condition .= $printplateType==9999? "AND PRINTPLATE_TYPE_SEQ IS NULL " : "AND PRINTPLATE_TYPE_SEQ = :printplateType ";

    //AMPHUR
    if($_REQUEST['amphur']!=""){
        $amphur = strpos($_REQUEST['amphur'],',')? explode(',',$_REQUEST['amphur']) : $_REQUEST['amphur'];
        if(strpos($_REQUEST['amphur'],',')){
            for($i=0; $i<count($amphur); $i++){
                $condition .= $i==0? 'AND CADASTRAL_AMPHUR_SEQ IN (:amphur'.$i.'' : ', :amphur'.$i.'';
            }
            $condition .= ') ';
        } else {
            $condition .= 'AND CADASTRAL_AMPHUR_SEQ = :amphur ';
        }
    }

    //TAMBOL
    if($_REQUEST['tambol']!=""){
        $tambol = strpos($_REQUEST['tambol'],',')? explode(',',$_REQUEST['tambol']) : $_REQUEST['tambol'];
        if(strpos($_REQUEST['tambol'],',')){
            for($i=0; $i<count($tambol); $i++){
                $condition .= $i==0? 'AND CADASTRAL_TAMBOL_SEQ IN (:tambol'.$i.'' : ', :tambol'.$i.'';
            }
            $condition .= ') ';
        } else {
            $condition .= 'AND CADASTRAL_TAMBOL_SEQ = :tambol ';
        }
    }

    if($no!="")     $condition .= "AND CADASTRAL_NO = :no ";
    if($sheet!="")  $condition .= "AND SHEETCODE = :sheet ";
    if($box!="")    $condition .= "AND BOX_NO = :box ";
    if($num!="")    $condition .= "AND NUMOFSURVEY_QTY = :num ";

    $sql = "";
    $sql .= "WITH RECV AS ( ";
    $sql .=     "SELECT C.CADASTRAL_SEQ, CADASTRAL_NO, SHEETCODE, BOX_NO, NUMOFSURVEY_QTY, C.TYPEOFSURVEY_SEQ, CADASTRAL_AMPHUR_SEQ, CADASTRAL_TAMBOL_SEQ, SURVEY_DATE, C.SURVEYJOB_SEQ, NVL(PRINTPLATE_TYPE_SEQ,9999) AS PRINTPLATE_TYPE_SEQ ";
    $sql .=     "FROM MGT1.TB_SVA_CADASTRAL  C ";
    $sql .=     "INNER JOIN MGT1.TB_SVA_CADASTRAL_IMAGE CI ";
    $sql .=         "ON CI.CADASTRAL_SEQ = C.CADASTRAL_SEQ ";
    $sql .=         "AND CI.RECORD_STATUS IN ('N', 'W', 'E') ";
    $sql .=     "LEFT JOIN MGT1.TB_SVA_SURVEYJOB J ";
    $sql .=         "ON C.SURVEYJOB_SEQ = J.SURVEYJOB_SEQ ";
    $sql .=         "AND J.RECORD_STATUS = 'N' ";
    $sql .=     $condition;
    $sql .= "), OK AS ( ";
    $sql .=     "SELECT C.CADASTRAL_SEQ, CADASTRAL_NO, SHEETCODE, BOX_NO, NUMOFSURVEY_QTY, C.TYPEOFSURVEY_SEQ, CADASTRAL_AMPHUR_SEQ, CADASTRAL_TAMBOL_SEQ, SURVEY_DATE, C.SURVEYJOB_SEQ, NVL(PRINTPLATE_TYPE_SEQ,9999) AS PRINTPLATE_TYPE_SEQ ";
    $sql .=     "FROM SVO.TB_SVA_CADASTRAL C ";
    $sql .=     "INNER JOIN SVO.TB_SVA_CADASTRAL_IMAGE CI ";
    $sql .=         "ON CI.CADASTRAL_SEQ = C.CADASTRAL_SEQ ";
    $sql .=         "AND CI.RECORD_STATUS IN ('N', 'W', 'E') ";
    $sql .=     "LEFT JOIN SVO.TB_SVA_SURVEYJOB J ";
    $sql .=         "ON C.SURVEYJOB_SEQ = J.SURVEYJOB_SEQ ";
    $sql .=         "AND J.RECORD_STATUS = 'N' ";
    $sql .=     $condition;
    $sql .= ") ";
    $sql .= "SELECT DISTINCT NVL(RECV.CADASTRAL_NO,OK.CADASTRAL_NO) AS NO, NVL(RECV.SHEETCODE,OK.SHEETCODE) AS SHEET, NVL(RECV.BOX_NO,OK.BOX_NO) AS BOX, NVL(RECV.NUMOFSURVEY_QTY,OK.NUMOFSURVEY_QTY) AS NUM ";
    $sql .=     ", TYPEOFSURVEY_NAME, AMPHUR_NAME, TAMBOL_NAME, TO_DATE(NVL(RECV.SURVEY_DATE,OK.SURVEY_DATE),'DD/MM/YYYY') AS DTM, NVL(RECV.PRINTPLATE_TYPE_SEQ,OK.PRINTPLATE_TYPE_SEQ) AS PT ";
    $sql .=     ", RECV.CADASTRAL_SEQ AS SEQ1, OK.CADASTRAL_SEQ AS SEQ2 ";
    $sql .= "FROM RECV ";
    $sql .= "RIGHT JOIN OK ";
    $sql .=     "ON RECV.CADASTRAL_NO = OK.CADASTRAL_NO ";
    $sql .=     "AND RECV.SHEETCODE = OK.SHEETCODE ";
    $sql .=     "AND RECV.BOX_NO = OK.BOX_NO ";
    $sql .=     "AND RECV.NUMOFSURVEY_QTY = OK.NUMOFSURVEY_QTY ";
    $sql .=     "AND RECV.CADASTRAL_SEQ = OK.CADASTRAL_SEQ ";
    $sql .= "LEFT JOIN SVO.TB_SVA_MAS_TYPEOFSURVEY TS ";
    $sql .=     "ON TS.TYPEOFSURVEY_SEQ = NVL(RECV.TYPEOFSURVEY_SEQ,OK.TYPEOFSURVEY_SEQ) ";
    $sql .= "LEFT JOIN MAS.TB_MAS_AMPHUR AP ";
    $sql .=     "ON AP.AMPHUR_SEQ = NVL(RECV.CADASTRAL_AMPHUR_SEQ,OK.CADASTRAL_AMPHUR_SEQ) ";
    $sql .= "LEFT JOIN MAS.TB_MAS_TAMBOL TB ";
    $sql .=     "ON TB.TAMBOL_SEQ = NVL(RECV.CADASTRAL_TAMBOL_SEQ,OK.CADASTRAL_TAMBOL_SEQ) ";
    $sql .= "LEFT JOIN MAS.TB_MAS_PRINTPLATE_TYPE PT ";
    $sql .=     "ON PT.PRINTPLATE_TYPE_SEQ = NVL(RECV.PRINTPLATE_TYPE_SEQ,OK.PRINTPLATE_TYPE_SEQ) ";
    $sql .= "ORDER BY  ";
    $sql .=     "NVL(RECV.SHEETCODE,OK.SHEETCODE), NVL(RECV.BOX_NO,OK.BOX_NO), NVL(RECV.CADASTRAL_NO,OK.CADASTRAL_NO), NVL(RECV.NUMOFSURVEY_QTY,OK.NUMOFSURVEY_QTY) ";
    $sql .=     ", NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY') ";
    
    // echo $condition;
    // echo $sql;
    $Result = array();
    if($printplateType!=13 && $printplateType!=11){
        $stid = oci_parse($conn, $sql);
        oci_bind_by_name($stid, ':landoffice', $landoffice);
        if($printplateType!=9999) oci_bind_by_name($stid, ':printplateType', $printplateType);
        if($no!="")     oci_bind_by_name($stid, ':no', $no);
        if($sheet!="")  oci_bind_by_name($stid, ':sheet', $sheet);
        if($box!="")    oci_bind_by_name($stid, ':box', $box);
        if($num!="")    oci_bind_by_name($stid, ':num', $num);
        
        if($_REQUEST['amphur']!=""){
            if(strpos($_REQUEST['amphur'],',')){
                for($i=0; $i<count($amphur); $i++){
                    oci_bind_by_name($stid, ':amphur'.$i, $amphur[$i]);
                }
            } else {
                oci_bind_by_name($stid, ':amphur', $amphur);
            }
        }
        if($_REQUEST['tambol']!=""){
            if(strpos($_REQUEST['tambol'],',')){
                for($i=0; $i<count($tambol); $i++){
                    oci_bind_by_name($stid, ':tambol'.$i, $tambol[$i]);
                }
            } else {
                oci_bind_by_name($stid, ':tambol', $tambol);
            }  
        }  
        oci_execute($stid);

        while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
            $Result[] = $row;
        }
    }

    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
    