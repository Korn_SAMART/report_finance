<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $seq1 = $_REQUEST['seq1'];
    $seq2 = $_REQUEST['seq2'];

    $sql = "";
    $sql .= "WITH RECV AS (  ";   
    $sql .=     "SELECT * FROM MGT1.TB_SVA_CADASTRAL_IMAGE ";
    $sql .=     "WHERE CADASTRAL_SEQ = :seq1 ";
    $sql .= "), OK AS ( ";
    $sql .=     "SELECT * FROM SVO.TB_SVA_CADASTRAL_IMAGE ";
    $sql .=     "WHERE CADASTRAL_SEQ = :seq2 ";
    $sql .= ") ";
    $sql .= "SELECT NVL(RECV.CADASTRAL_IMAGE_ORDER,OK.CADASTRAL_IMAGE_ORDER) AS ORD, SURVEYDOCTYPE_NAME, RECV.IMAGE_PATH AS IMG1, OK.IMAGE_PATH AS IMG2 ";
    $sql .= "FROM RECV ";
    $sql .= "RIGHT JOIN OK ";
    $sql .=     "ON RECV.CADASTRAL_IMAGE_ORDER = OK.CADASTRAL_IMAGE_ORDER ";
    $sql .=     "AND RECV.SURVEYDOCTYPE_SEQ = OK.SURVEYDOCTYPE_SEQ ";
    $sql .= "LEFT JOIN SVO.TB_SVA_MAS_SURVEYDOCTYPE SD ";
    $sql .=     "ON SD.SURVEYDOCTYPE_SEQ = NVL(RECV.SURVEYDOCTYPE_SEQ,OK.SURVEYDOCTYPE_SEQ) ";
    $sql .= "ORDER BY NVL(RECV.CADASTRAL_IMAGE_ORDER,OK.CADASTRAL_IMAGE_ORDER) ";

    // echo $sql."\n";
    $stid = oci_parse($conn, $sql);
    oci_bind_by_name($stid, ':seq1', $seq1);
    oci_bind_by_name($stid, ':seq2', $seq2);
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
