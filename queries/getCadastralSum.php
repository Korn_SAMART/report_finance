<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice = $_REQUEST['landoffice'];

    $sql = "
    WITH RECV AS (
     SELECT NVL(T3.printplate_type_seq,9999) AS PRINTPLATE_TYPE_SEQ, count(CADASTRAL_IMAGE_SEQ) TOTAL 
     FROM MGT1.tb_sva_cadastral_image T2
     LEFT OUTER JOIN  MGT1.tb_sva_cadastral T1
            ON T2.CADASTRAL_SEQ = T1.CADASTRAL_SEQ
     LEFT OUTER JOIN  MGT1.tb_sva_surveyjob T3
            ON T1.SURVEYJOB_SEQ = T3.SURVEYJOB_SEQ AND T3.record_status = 'N'
      WHERE T1.LANDOFFICE_SEQ = :landoffice 
            AND T1.record_status = 'N'  AND T2.record_status IN ('N','W','E')
        GROUP BY NVL(T3.printplate_type_seq,9999)
    ),
    OK AS (
     SELECT NVL(T3.printplate_type_seq,9999) AS PRINTPLATE_TYPE_SEQ, count(CADASTRAL_IMAGE_SEQ) TOTAL 
     FROM SVO.tb_sva_cadastral_image T2
     LEFT OUTER JOIN  SVO.tb_sva_cadastral T1
            ON T2.CADASTRAL_SEQ = T1.CADASTRAL_SEQ
     LEFT OUTER JOIN  SVO.tb_sva_surveyjob T3
            ON T1.SURVEYJOB_SEQ = T3.SURVEYJOB_SEQ AND T3.record_status = 'N'
      WHERE T1.LANDOFFICE_SEQ = :landoffice  
            AND T1.record_status = 'N'  AND T2.record_status IN ('N','W','E')
        GROUP BY T3.printplate_type_seq
    )
    select NVL(mp.printplate_type_seq,9999) AS PRINTPLATE_TYPE_SEQ, NVL(PRINTPLATE_TYPE_NAME,'ไม่สามารถแยกประเภทเอกสารได้') AS PRINTPLATE_TYPE_NAME, NVL(RECV.TOTAL,0) AS RECEIVE, NVL(OK.TOTAL,0) AS MIGRATE_SUCCESS, (NVL(RECV.TOTAL,0)-NVL(OK.TOTAL,0)) as MIGRATE_ERROR
    FROM MGT1.TB_REG_MAS_PRINTPLATE_TYPE MP
    FULL OUTER JOIN RECV ON RECV.PRINTPLATE_TYPE_SEQ = MP.PRINTPLATE_TYPE_SEQ
    LEFT OUTER JOIN OK ON OK.PRINTPLATE_TYPE_SEQ = RECV.PRINTPLATE_TYPE_SEQ
    WHERE MP.PRINTPLATE_TYPE_SEQ  in (1,2,17,3,5,4,8,23,13,10,9,7,6,11,15,16,18,19,20,21,22,28,12,14) OR RECV.PRINTPLATE_TYPE_SEQ = 9999 OR OK.PRINTPLATE_TYPE_SEQ = 9999
    ORDER BY (case MP.printplate_type_seq 
    when 1 then 1 when 2 then 2 when 17 then 3 when 3 then 4
    when 5 then 5 when 4 then 6 when 8 then 7 when 23 then 8
    when 13 then 9 when 10 then 10 when 9 then 11 when 7 then 12
    when 6 then 13 when 11 then 14 when 15 then 15 when 16 then 16
    when 18 then 17 when 19 then 18 when 20 then 19 when 21 then 20
    when 22 then 21 when 28 then 22 when 12 then 23 when 14 then 24 end)
  ";

    // echo $sql."\n";
    $stid = oci_parse($conn, $sql);
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    $jsonData = array(
        "data" => $Result
    );
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
