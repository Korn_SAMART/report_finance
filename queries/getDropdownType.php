<?php
header('Content-Type: application/json; charset=utf-8');
//require_once 'isXmlHttpRequest.php';
include '../database/conn.php';

$query = '';

switch ($_POST['type']) {
    case 'secret':
        $query = "SELECT SECRET.DOC_SECRET_SEQ, SECRET.DOC_SECRET_NAME
                FROM CTN.TB_CTN_MAS_DOC_SECRET SECRET
                WHERE SECRET.DOC_SECRET_SEQ <> 1
                ORDER BY SECRET.DOC_SECRET_SEQ";
        break;
    case 'bld':
        $query = "SELECT BLD.BLD_SEQ,BLD.BLD_NAME
                FROM INV.TB_INV_MAS_BLD BLD
                ORDER BY BLD.BLD_SEQ";
        break;
    case 'inv':
        $query = "SELECT TOPIC_ASSET.TOPIC_ASSET_SEQ,TOPIC_ASSET.TOPIC_ASSET_NAME
                FROM INV.TB_INV_MAS_TOPIC_ASSET TOPIC_ASSET
                ORDER BY TOPIC_ASSET.TOPIC_ASSET_SEQ";
        break;
    case 'doc_fin':
        $query = "SELECT PLATE.PLATE_SEQ,PLATE.PLATE_NAME
                FROM INV.TB_INV_MAS_PLATE PLATE
                WHERE PLATE.PLATE_TYPE_SEQ = 2
                ORDER BY PLATE.PLATE_SEQ";
        break;
    case 'doc':
        $query = "SELECT PLATE.PLATE_SEQ,PLATE.PLATE_NAME
                    FROM INV.TB_INV_MAS_PLATE PLATE
                    WHERE PLATE.PLATE_TYPE_SEQ IN (3,4)
                    ORDER BY PLATE.PLATE_SEQ";
        break;
    case 'doc_parcel':
        $query = "SELECT PLATE.PLATE_SEQ,PLATE.PLATE_NAME
                    FROM INV.TB_INV_MAS_PLATE PLATE
                    WHERE PLATE.PLATE_TYPE_SEQ = 1
                    ORDER BY PLATE.PLATE_SEQ";
        break;
    case 'mat':
        $query = "SELECT MAT.MAT_SEQ,MAT.MAT_NAME
                    FROM INV.TB_INV_MAS_MAT MAT
                    ORDER BY MAT.MAT_SEQ";
        break;
    case 'district':
        $query = "SELECT PIECE.PIECE_SEQ,PIECE.PIECE_NAME
                    FROM INV.TB_INV_MAS_PIECE PIECE
                    ORDER BY PIECE.PIECE_SEQ";
        break;
    case 'landoffice_type':
        $query = " SELECT PSNTYPE.PSN_TYPE_SEQ, PSNTYPE.PSN_TYPE_NAME
                    FROM HRM.TB_HRM_MAS_PSNTYPE PSNTYPE
                    ORDER BY PSNTYPE.PSN_TYPE_SEQ";
        break;
    case 'jobstatus':
        $query = " SELECT JOBSTATUS.JOBSTATUS_SEQ, JOBSTATUS.JOBSTATUS_NAME
                    FROM SVO.TB_SVA_MAS_JOBSTATUS JOBSTATUS
                    ORDER BY JOBSTATUS.JOBSTATUS_SEQ";
        break;
    case 'scale':
        $query = " SELECT SCALE.SCALE_SEQ, SCALE.SCALE_NAME
                    FROM SVO.TB_SVA_MAS_SCALE SCALE
                    ORDER BY SCALE.SCALE_SEQ";
        break;
    case 'parceltype':
        $query = " SELECT PARCELTYPE.PARCELTYPE_SEQ, PARCELTYPE.PARCELTYPE_DESC
                FROM SVO.TB_UDM_MAS_PARCELTYPE PARCELTYPE
                ORDER BY PARCELTYPE.PARCELTYPE_SEQ";
        break;
    default:
        break;
}



$stid = oci_parse($conn, $query);
oci_execute($stid);
$Result = array();
while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
    $Result[] = $row;
}
echo json_encode($Result, JSON_UNESCAPED_UNICODE);
oci_free_statement($stid);
oci_close($conn);
