<?php
    header('Content-Type: application/json; charset=utf-8;');
    include '../database/conn.php';

    $Result = array();
    include 'queryExpensesList.php';
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);

    oci_free_statement($stid);
    oci_close($conn);
?>
