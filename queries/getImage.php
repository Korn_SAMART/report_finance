<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice     = $_REQUEST['landoffice'];
    $printplateType = $_REQUEST['printplateType'];
    $parcelNo       = isset($_REQUEST['parcelNo']) ? $_REQUEST['parcelNo'] : "";
    $survey         = isset($_REQUEST['survey']) ? $_REQUEST['survey'] : "";
    $condoReg       = isset($_REQUEST['condoReg']) ? $_REQUEST['condoReg'] : "";
    $condoName      = isset($_REQUEST['condoName']) ? $_REQUEST['condoName'] : "";
    $condoroom      = isset($_REQUEST['condoroom']) ? $_REQUEST['condoroom'] : "";
    $constrHid      = isset($_REQUEST['constrHid']) ? $_REQUEST['constrHid'] : "";
    $constrHno      = isset($_REQUEST['constrHno']) ? $_REQUEST['constrHno'] : "";
    $constrMoo      = isset($_REQUEST['constrMoo']) ? $_REQUEST['constrMoo'] : "";
    $parcelLandNo   = isset($_REQUEST['parcelLandNo']) ? $_REQUEST['parcelLandNo'] : "";
    $parcelLandMoo  = isset($_REQUEST['parcelLandMoo']) ? $_REQUEST['parcelLandMoo'] : "";
    $nslNo          = isset($_REQUEST['nslNo']) ? $_REQUEST['nslNo'] : "";
    $obtainYear     = isset($_REQUEST['obtainYear']) ? $_REQUEST['obtainYear'] : "";
    $parcelLandName = isset($_REQUEST['parcelLandName']) ? $_REQUEST['parcelLandName'] : "";
    $st = ($printplateType!=1&&$printplateType!=10)? "P." : "";
    // echo "";
    // print_r($_REQUEST);

    $condition = "";

    //MUST TO HAVE
    $condition .= "WHERE P.LANDOFFICE_SEQ = :landoffice AND P.RECORD_STATUS = 'N' ";
    if($printplateType!=13) $condition .= "AND P.PRINTPLATE_TYPE_SEQ = :printplateType ";
    //AMPHUR
    if($_REQUEST['amphur']!=""){
        $amphur = strpos($_REQUEST['amphur'],',')? explode(',',$_REQUEST['amphur']) : $_REQUEST['amphur'];
        if(strpos($_REQUEST['amphur'],',')){
            for($i=0; $i<count($amphur); $i++){
                $condition .= $i==0? 'AND '.$st.'AMPHUR_SEQ IN (:amphur'.$i.'' : ', :amphur'.$i.'';
            }
            $condition .= ') ';
        } else {
            $condition .= 'AND '.$st.'AMPHUR_SEQ = :amphur ';
        }
    }

    //TAMBOL
    if($_REQUEST['tambol']!=""){
        $tambol = strpos($_REQUEST['tambol'],',')? explode(',',$_REQUEST['tambol']) : $_REQUEST['tambol'];
        if(strpos($_REQUEST['tambol'],',')){
            for($i=0; $i<count($tambol); $i++){
                $condition .= $i==0? 'AND '.$st.'TAMBOL_SEQ IN (:tambol'.$i.'' : ', :tambol'.$i.'';
            }
            $condition .= ') ';
        } else {
            $condition .= 'AND '.$st.'TAMBOL_SEQ = :tambol ';
        }
    }

    if($parcelNo!="")       $condition .= "AND PARCEL_NO = :parcelNo ";
    if($survey!="")         $condition .= $printplateType==1? "AND PARCEL_SURVEY_NO = :survey " :"AND PARCEL_LAND_SURVEY_NO = :survey ";
    if($condoReg!="")       $condition .= "AND CONDO_ID||'/'||CONDO_REG_YEAR LIKE '%'||:condoReg||'%' ";
    if($condoName!="")      $condition .= "AND CONDO_NAME_TH LIKE '%'||:condoName||'%' ";
    if($condoroom!="")      $condition .= "AND CONDOROOM_RNO = :condoroom ";
    if($constrHid!="")      $condition .= "AND CONSTRUCT_ADDR_HID = :constrHid ";
    if($constrHno!="")      $condition .= "AND CONSTRUCT_ADDR_HNO = :constrHno ";
    if($constrMoo!="")      $condition .= "AND CONSTRUCT_ADDR_Moo = :constrMoo ";
    if($parcelLandNo!="")   $condition .= "AND P.PARCEL_LAND_NO = :parcelLandNo ";
    if($parcelLandMoo!="")  $condition .= "AND P.PARCEL_LAND_MOO = :parcelLandMoo ";
    if($parcelLandName!="") $condition .= "AND P.PARCEL_LAND_NAME LIKE '%'||:parcelLandName||'%' ";
    if($obtainYear!="")     $condition .= "AND P.PARCEL_LAND_OBTAIN_DTM = :obtainYear ";

    $sql = "";
    if($printplateType == 1){
        // echo 'parcel';
        $sql = "WITH RECV AS ( ";
        $sql .=    "SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_NO, PARCEL_SURVEY_NO, P.PARCEL_SEQ ";
        $sql .=    "FROM MGT1.TB_REG_PARCEL P ";
        $sql .=    "INNER JOIN MGT1.TB_REG_PARCEL_IMAGE PI ";
        $sql .=        "ON P.PARCEL_SEQ  = PI.PARCEL_SEQ ";
        $sql .=        "AND PI.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=   $condition;
        $sql .=    "UNION ";
        $sql .=    "SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_NO, PARCEL_SURVEY_NO, P.PARCEL_SEQ ";
        $sql .=    "FROM MGT1.TB_REG_PARCEL P ";
        $sql .=    "INNER JOIN MGT1.TB_REG_PARCEL_INDEX PI ";
        $sql .=        "ON PI.PARCEL_SEQ = P.PARCEL_SEQ ";
        $sql .=        "AND PI.RECORD_STATUS = 'N' ";
        $sql .=    "INNER JOIN MGT1.TB_REG_PARCEL_HSFS PH ";
        $sql .=        "ON PH.PARCEL_INDEX_SEQ = PI.PARCEL_INDEX_SEQ ";
        $sql .=        "AND PH.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=   $condition;
        $sql .= "), OK AS ( ";
        $sql .=     "SELECT DISTINCT  LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_NO, PARCEL_SURVEY_NO, P.PARCEL_SEQ ";
        $sql .=     "FROM REG.TB_REG_PARCEL P ";
        $sql .=     "INNER JOIN REG.TB_REG_PARCEL_IMAGE PI ";
        $sql .=         "ON P.PARCEL_SEQ  = PI.PARCEL_SEQ ";
        $sql .=         "AND PI.RECORD_STATUS IN ('N', 'W', 'E')";
        $sql .=     $condition;
        $sql .=    "UNION ";
        $sql .=    "SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_NO, PARCEL_SURVEY_NO, P.PARCEL_SEQ ";
        $sql .=    "FROM REG.TB_REG_PARCEL P ";
        $sql .=    "INNER JOIN REG.TB_REG_PARCEL_INDEX PI ";
        $sql .=        "ON PI.PARCEL_SEQ = P.PARCEL_SEQ ";
        $sql .=        "AND PI.RECORD_STATUS = 'N' ";
        $sql .=    "INNER JOIN REG.TB_REG_PARCEL_HSFS PH ";
        $sql .=        "ON PH.PARCEL_INDEX_SEQ = PI.PARCEL_INDEX_SEQ ";
        $sql .=        "AND PH.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=   $condition;
        $sql .= ") ";
        $sql .= "SELECT NVL(RECV.LANDOFFICE_SEQ,OK.LANDOFFICE_SEQ) AS LANDOFFICE, AMPHUR_NAME, TAMBOL_NAME ";
        $sql .=     ",NVL(RECV.PARCEL_NO,OK.PARCEL_NO) AS NO, NVL(RECV.PARCEL_SURVEY_NO,OK.PARCEL_SURVEY_NO) AS SURVEY ";
        $sql .=     ",RECV.PARCEL_SEQ, OK.PARCEL_SEQ AS PARCEL_SEQ_1 ";
        $sql .= "FROM RECV ";
        $sql .= "RIGHT JOIN OK ";
        $sql .=     "ON RECV.AMPHUR_SEQ = OK.AMPHUR_SEQ ";
        $sql .=     "AND RECV.PARCEL_NO = OK.PARCEL_NO ";
        $sql .= "LEFT JOIN MAS.TB_MAS_AMPHUR AP ";
        $sql .=     "ON NVL(RECV.AMPHUR_SEQ,OK.AMPHUR_SEQ) = AP.AMPHUR_SEQ ";
        $sql .= "LEFT JOIN MAS.TB_MAS_TAMBOL TB ";
        $sql .=     "ON NVL(RECV.TAMBOL_SEQ,OK.TAMBOL_SEQ) = TB.TAMBOL_SEQ ";
        $sql .= "ORDER BY NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY'), TO_NUMBER(NVL(RECV.PARCEL_NO,OK.PARCEL_NO)) ";
    } else if($printplateType==10){
        // echo 'condoroom';
        $sql = "WITH RECV AS ( ";
        $sql .=     "SELECT DISTINCT P.LANDOFFICE_SEQ, C.AMPHUR_SEQ, C.TAMBOL_SEQ, P.CONDOROOM_RNO, C.CONDO_ID||'/'||C.CONDO_REG_YEAR AS CONDO_REG, C.CONDO_NAME_TH, P.CONDOROOM_SEQ ";
        $sql .=     "FROM MGT1.TB_REG_CONDOROOM P ";
        $sql .=     "INNER JOIN MGT1.TB_REG_CONDOROOM_IMAGE PI ";
        $sql .=         "ON P.CONDOROOM_SEQ = PI.CONDOROOM_SEQ ";
        $sql .=         "AND PI.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=     "LEFT JOIN MGT1.TB_REG_CONDO_BLD B ";
        $sql .=         "ON P.BLD_SEQ = B.BLD_SEQ ";
        $sql .=         "AND B.RECORD_STATUS = 'N' ";
        $sql .=     "LEFT JOIN MGT1.TB_REG_CONDO C ";
        $sql .=         "ON C.CONDO_SEQ = B.CONDO_SEQ ";
        $sql .=         "AND C.RECORD_STATUS = 'N' ";
        $sql .=     $condition;
        $sql .=    "UNION ";
        $sql .=    "SELECT DISTINCT P.LANDOFFICE_SEQ, C.AMPHUR_SEQ, C.TAMBOL_SEQ, P.CONDOROOM_RNO, C.CONDO_ID||'/'||C.CONDO_REG_YEAR AS CONDO_REG, C.CONDO_NAME_TH, P.CONDOROOM_SEQ ";
        $sql .=    "FROM MGT1.TB_REG_CONDOROOM P ";
        $sql .=    "INNER JOIN MGT1.TB_REG_CONDOROOM_INDEX PI ";
        $sql .=        "ON PI.CONDOROOM_SEQ = P.CONDOROOM_SEQ ";
        $sql .=        "AND PI.RECORD_STATUS = 'N' ";
        $sql .=    "INNER JOIN MGT1.TB_REG_CONDOROOM_HSFS PH ";
        $sql .=        "ON PH.CONDOROOM_INDEX_SEQ = PI.CONDOROOM_INDEX_SEQ ";
        $sql .=        "AND PH.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=    "LEFT JOIN MGT1.TB_REG_CONDO_BLD B ";
        $sql .=        "ON P.BLD_SEQ = B.BLD_SEQ ";
        $sql .=        "AND B.RECORD_STATUS = 'N' ";
        $sql .=    "LEFT JOIN MGT1.TB_REG_CONDO C ";
        $sql .=        "ON C.CONDO_SEQ = B.CONDO_SEQ ";
        $sql .=        "AND C.RECORD_STATUS = 'N' ";
        $sql .=    $condition;
        $sql .= "), OK AS ( ";
        $sql .= "SELECT DISTINCT P.LANDOFFICE_SEQ, C.AMPHUR_SEQ, C.TAMBOL_SEQ, P.CONDOROOM_RNO, C.CONDO_ID||'/'||C.CONDO_REG_YEAR AS CONDO_REG, C.CONDO_NAME_TH, P.CONDOROOM_SEQ ";
        $sql .=     "FROM REG.TB_REG_CONDOROOM P ";
        $sql .=     "INNER JOIN REG.TB_REG_CONDOROOM_IMAGE PI ";
        $sql .=         "ON P.CONDOROOM_SEQ = PI.CONDOROOM_SEQ ";
        $sql .=         "AND PI.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=     "LEFT JOIN REG.TB_REG_CONDO_BLD B ";
        $sql .=         "ON P.BLD_SEQ = B.BLD_SEQ ";
        $sql .=         "AND B.RECORD_STATUS = 'N' ";
        $sql .=     "LEFT JOIN REG.TB_REG_CONDO C ";
        $sql .=         "ON C.CONDO_SEQ = B.CONDO_SEQ ";
        $sql .=         "AND C.RECORD_STATUS = 'N' ";
        $sql .=     $condition;
        $sql .=    "UNION ";
        $sql .=    "SELECT DISTINCT P.LANDOFFICE_SEQ, C.AMPHUR_SEQ, C.TAMBOL_SEQ, P.CONDOROOM_RNO, C.CONDO_ID||'/'||C.CONDO_REG_YEAR AS CONDO_REG, C.CONDO_NAME_TH, P.CONDOROOM_SEQ ";
        $sql .=    "FROM REG.TB_REG_CONDOROOM P ";
        $sql .=    "INNER JOIN REG.TB_REG_CONDOROOM_INDEX PI ";
        $sql .=        "ON PI.CONDOROOM_SEQ = P.CONDOROOM_SEQ ";
        $sql .=        "AND PI.RECORD_STATUS = 'N' ";
        $sql .=    "INNER JOIN REG.TB_REG_CONDOROOM_HSFS PH ";
        $sql .=        "ON PH.CONDOROOM_INDEX_SEQ = PI.CONDOROOM_INDEX_SEQ ";
        $sql .=        "AND PH.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=    "LEFT JOIN REG.TB_REG_CONDO_BLD B ";
        $sql .=        "ON P.BLD_SEQ = B.BLD_SEQ ";
        $sql .=        "AND B.RECORD_STATUS = 'N' ";
        $sql .=    "LEFT JOIN REG.TB_REG_CONDO C ";
        $sql .=        "ON C.CONDO_SEQ = B.CONDO_SEQ ";
        $sql .=        "AND C.RECORD_STATUS = 'N' ";
        $sql .=    $condition;
        $sql .= ") ";
        $sql .= "SELECT NVL(RECV.LANDOFFICE_SEQ,OK.LANDOFFICE_SEQ) AS LANDOFFICE, AMPHUR_NAME, TAMBOL_NAME ";
        $sql .=     ",NVL(RECV.CONDOROOM_RNO,OK.CONDOROOM_RNO) AS NO, NVL(RECV.CONDO_REG,OK.CONDO_REG) AS CONDO_REG, NVL(RECV.CONDO_NAME_TH,OK.CONDO_NAME_TH) AS CONDO_NAME ";
        $sql .=     ",RECV.CONDOROOM_SEQ, OK.CONDOROOM_SEQ AS CONDOROOM_SEQ_1 ";
        $sql .= "FROM RECV ";
        $sql .= "RIGHT JOIN OK ";
        $sql .=     "ON RECV.AMPHUR_SEQ = OK.AMPHUR_SEQ ";
        $sql .=     "AND RECV.TAMBOL_SEQ = OK.TAMBOL_SEQ ";
        $sql .=     "AND RECV.CONDO_REG = OK.CONDO_REG ";
        $sql .=     "AND RECV.CONDOROOM_RNO  = OK.CONDOROOM_RNO ";
        $sql .=     "AND RECV.CONDO_REG = OK.CONDO_REG ";
        $sql .= "LEFT JOIN MAS.TB_MAS_AMPHUR AP ";
        $sql .=     "ON NVL(RECV.AMPHUR_SEQ,OK.AMPHUR_SEQ) = AP.AMPHUR_SEQ ";
        $sql .= "LEFT JOIN MAS.TB_MAS_TAMBOL TB ";
        $sql .=     "ON NVL(RECV.TAMBOL_SEQ,OK.TAMBOL_SEQ) = TB.TAMBOL_SEQ ";
        $sql .= "ORDER BY NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY') ";
        $sql .=     ",TO_NUMBER(REGEXP_SUBSTR(NVL(RECV.CONDO_REG,OK.CONDO_REG),'[^/]+',1,2)) ";
        $sql .=     ",TO_NUMBER(REGEXP_SUBSTR(NVL(RECV.CONDO_REG,OK.CONDO_REG),'[^/]+',1)) ";
        $sql .=     ",REGEXP_SUBSTR(NVL(RECV.CONDOROOM_RNO,OK.CONDOROOM_RNO), '^\D*') NULLS FIRST, TO_NUMBER(REGEXP_SUBSTR(NVL(RECV.CONDOROOM_RNO,OK.CONDOROOM_RNO), '\d+'))
                    ,TO_NUMBER(REGEXP_SUBSTR(NVL(RECV.CONDOROOM_RNO,OK.CONDOROOM_RNO), '[^/]+',1,2)) ";
    } else if($printplateType==11){

    } else if($printplateType==13){
        $sql .= "WITH RECV AS (
                    SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, CONDO_ID||'/'||CONDO_REG_YEAR AS CONDO_REG, CONDO_NAME_TH, P.CONDO_SEQ
                    FROM MGT1.TB_REG_CONDO P
                    INNER JOIN MGT1.TB_REG_CONDO_INDEX PI
                        ON PI.CONDO_SEQ = P.CONDO_SEQ
                        AND PI.RECORD_STATUS = 'N'
                    INNER JOIN MGT1.TB_REG_CONDO_HSFS PH
                        ON PH.CONDO_INDEX_SEQ = PI.CONDO_INDEX_SEQ
                        AND PH.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=     $condition;
        $sql .= "), OK AS (
                    SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, CONDO_ID||'/'||CONDO_REG_YEAR AS CONDO_REG, CONDO_NAME_TH, P.CONDO_SEQ
                    FROM REG.TB_REG_CONDO P
                    INNER JOIN REG.TB_REG_CONDO_INDEX PI
                        ON PI.CONDO_SEQ = P.CONDO_SEQ
                        AND PI.RECORD_STATUS = 'N'
                    INNER JOIN REG.TB_REG_CONDO_HSFS PH
                        ON PH.CONDO_INDEX_SEQ = PI.CONDO_INDEX_SEQ
                        AND PH.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=     $condition;
        $sql .= ")
                SELECT NVL(RECV.LANDOFFICE_SEQ,OK.LANDOFFICE_SEQ) AS LANDOFFICE, AMPHUR_NAME, TAMBOL_NAME
                    , NVL(RECV.CONDO_REG,OK.CONDO_REG) AS CONDO_REG, NVL(RECV.CONDO_NAME_TH,OK.CONDO_NAME_TH) AS CONDO_NAME
                    , RECV.CONDO_SEQ, OK.CONDO_SEQ AS CONDO_SEQ_1
                FROM RECV
                RIGHT JOIN OK
                    ON RECV.LANDOFFICE_SEQ = OK.LANDOFFICE_SEQ
                    AND RECV.AMPHUR_SEQ = OK.AMPHUR_SEQ
                    AND RECV.TAMBOL_SEQ = OK.TAMBOL_SEQ
                    AND RECV.CONDO_REG = OK.CONDO_REG
                LEFT JOIN MAS.TB_MAS_AMPHUR AP
                    ON AP.AMPHUR_SEQ = NVL(RECV.AMPHUR_SEQ,OK.AMPHUR_SEQ)
                LEFT JOIN MAS.TB_MAS_TAMBOL TB
                    ON TB.TAMBOL_SEQ = NVL(RECV.TAMBOL_SEQ,OK.TAMBOL_SEQ)
                ORDER BY NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY')
                    ,TO_NUMBER(REGEXP_SUBSTR(NVL(RECV.CONDO_REG,OK.CONDO_REG),'[^/]+',1,2))
                    ,TO_NUMBER(REGEXP_SUBSTR(NVL(RECV.CONDO_REG,OK.CONDO_REG),'[^/]+',1))";
    } else{
        // echo 'parcel_land';
        $sql = "WITH RECV AS ( ";
        $sql .=     "SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_MOO, PARCEL_LAND_NO, PARCEL_LAND_OBTAIN_DTM, PARCEL_LAND_NAME, PARCEL_LAND_SURVEY_NO, P.PARCEL_LAND_SEQ, NULL AS NSL_NO ";
        $sql .=     "FROM MGT1.TB_REG_PARCEL_LAND P ";
        $sql .=     "INNER JOIN MGT1.TB_REG_PARCEL_LAND_IMAGE PI ";
        $sql .=         "ON P.PARCEL_LAND_SEQ  = PI.PARCEL_LAND_SEQ ";
        $sql .=         "AND PI.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=     $condition;
        $sql .=    "UNION ";
        $sql .=    "SELECT DISTINCT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_MOO, PARCEL_LAND_NO, PARCEL_LAND_OBTAIN_DTM, PARCEL_LAND_NAME, PARCEL_LAND_SURVEY_NO, P.PARCEL_LAND_SEQ, NULL AS NSL_NO ";
        $sql .=    "FROM MGT1.TB_REG_PARCEL_LAND P ";
        $sql .=    "INNER JOIN MGT1.TB_REG_PARCEL_LAND_INDEX PI ";
        $sql .=        "ON PI.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ ";
        $sql .=        "AND PI.RECORD_STATUS = 'N' ";
        $sql .=    "INNER JOIN MGT1.TB_REG_PARCEL_LAND_HSFS PH ";
        $sql .=        "ON PH.PARCEL_LAND_INDEX_SEQ = PI.PARCEL_LAND_INDEX_SEQ ";
        $sql .=        "AND PH.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=    $condition;
        $sql .= "), OK AS ( ";
        $sql .=     "SELECT DISTINCT P.LANDOFFICE_SEQ, P.AMPHUR_SEQ, P.TAMBOL_SEQ, P.PARCEL_LAND_MOO, P.PARCEL_LAND_NO, P.PARCEL_LAND_OBTAIN_DTM, P.PARCEL_LAND_NAME, P.PARCEL_LAND_SURVEY_NO, P.PARCEL_LAND_SEQ, P2.PARCEL_LAND_NO AS NSL_NO ";
        $sql .=     "FROM REG.TB_REG_PARCEL_LAND P ";
        $sql .=     "INNER JOIN REG.TB_REG_PARCEL_LAND_IMAGE PI ";
        $sql .=         "ON P.PARCEL_LAND_SEQ  = PI.PARCEL_LAND_SEQ ";
        $sql .=         "AND PI.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=     "LEFT JOIN REG.TB_REG_PARCEL_LAND P2 ";
        $sql .=         "ON P.NSL_SEQ = P2.PARCEL_LAND_NO ";
        $sql .=         "AND P2.RECORD_STATUS = 'N' ";
        $sql .=         "AND P.PRINTPLATE_TYPE_SEQ = 23 AND P2.PRINTPLATE_TYPE_SEQ = 8 ";
        $sql .=     $condition;
        $sql .=    "UNION ";
        $sql .=    "SELECT DISTINCT P.LANDOFFICE_SEQ, P.AMPHUR_SEQ, P.TAMBOL_SEQ, P.PARCEL_LAND_MOO, P.PARCEL_LAND_NO, P.PARCEL_LAND_OBTAIN_DTM, P.PARCEL_LAND_NAME, P.PARCEL_LAND_SURVEY_NO, P.PARCEL_LAND_SEQ, P2.PARCEL_LAND_NO AS NSL_NO ";
        $sql .=    "FROM REG.TB_REG_PARCEL_LAND P ";
        $sql .=    "INNER JOIN MGT1.TB_REG_PARCEL_LAND_INDEX PI ";
        $sql .=        "ON PI.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ ";
        $sql .=        "AND PI.RECORD_STATUS = 'N' ";
        $sql .=    "INNER JOIN MGT1.TB_REG_PARCEL_LAND_HSFS PH ";
        $sql .=        "ON PH.PARCEL_LAND_INDEX_SEQ = PI.PARCEL_LAND_INDEX_SEQ ";
        $sql .=        "AND PH.RECORD_STATUS IN ('N', 'W', 'E') ";
        $sql .=    "LEFT JOIN REG.TB_REG_PARCEL_LAND P2 ";
        $sql .=        "ON P.NSL_SEQ = P2.PARCEL_LAND_NO ";
        $sql .=        "AND P2.RECORD_STATUS = 'N' ";
        $sql .=        "AND P.PRINTPLATE_TYPE_SEQ = 23 AND P2.PRINTPLATE_TYPE_SEQ = 8 ";
        $sql .=    $condition;
        if($nslNo!="") $sql .= "AND P2.PARCEL_LAND_NO = :nslNo ";     
        $sql .= ") ";
        $sql .= "SELECT NVL(RECV.LANDOFFICE_SEQ,OK.LANDOFFICE_SEQ) AS LANDOFFICE, AMPHUR_NAME, TAMBOL_NAME, NVL(RECV.PARCEL_LAND_MOO,OK.PARCEL_LAND_MOO) AS MOO ";
        $sql .=     ",NVL(RECV.PARCEL_LAND_NO,OK.PARCEL_LAND_NO) AS NO, NVL(RECV.PARCEL_LAND_SURVEY_NO,OK.PARCEL_LAND_SURVEY_NO) AS SURVEY ";
        $sql .=     ",NVL(RECV.PARCEL_LAND_OBTAIN_DTM,OK.PARCEL_LAND_OBTAIN_DTM) AS YEAR, NVL(RECV.PARCEL_LAND_NAME,OK.PARCEL_LAND_NAME) AS NAME ";
        $sql .=     ",RECV.PARCEL_LAND_SEQ, OK.PARCEL_LAND_SEQ AS PARCEL_LAND_SEQ_1 ";
        $sql .=     ",OK.NSL_NO ";
        $sql .= "FROM RECV ";
        $sql .= "RIGHT JOIN OK ";
        $sql .=     "ON RECV.PARCEL_LAND_SEQ = OK.PARCEL_LAND_SEQ ";
        $sql .= "LEFT JOIN MAS.TB_MAS_AMPHUR AP ";
        $sql .=     "ON NVL(RECV.AMPHUR_SEQ,OK.AMPHUR_SEQ) = AP.AMPHUR_SEQ ";
        $sql .= "LEFT JOIN MAS.TB_MAS_TAMBOL TB ";
        $sql .=     "ON NVL(RECV.TAMBOL_SEQ,OK.TAMBOL_SEQ) = TB.TAMBOL_SEQ ";
        $sql .= "ORDER BY NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY'), TO_NUMBER(NVL(RECV.PARCEL_LAND_MOO,OK.PARCEL_LAND_MOO)), 
                    REGEXP_SUBSTR(NVL(RECV.PARCEL_LAND_NO,OK.PARCEL_LAND_NO), '^\D*') NULLS FIRST, TO_NUMBER(REGEXP_SUBSTR(NVL(RECV.PARCEL_LAND_NO,OK.PARCEL_LAND_NO), '\d+')) ";
        // echo $condition;
    }
    // include 'queryImage.php';
    // echo $sql;
    $Result = array();
    if($printplateType!=11){
        // echo $sql;
        $stid = oci_parse($conn, $sql);
        oci_bind_by_name($stid, ':landoffice', $landoffice);
        if($printplateType!=13) oci_bind_by_name($stid, ':printplateType', $printplateType);
        if($parcelNo!="")       oci_bind_by_name($stid, ':parcelNo', $parcelNo);
        if($survey!="")         oci_bind_by_name($stid, ':survey', $survey);
        if($condoReg!="")       oci_bind_by_name($stid, ':condoReg', $condoReg);
        if($condoName!="")      oci_bind_by_name($stid, ':condoName', $condoName);
        if($condoroom!="")      oci_bind_by_name($stid, ':condoroom', $condoroom);
        if($constrHid!="")      oci_bind_by_name($stid, ':constrHid', $constrHid);
        if($constrHno!="")      oci_bind_by_name($stid, ':constrHno', $constrHno);
        if($constrMoo!="")      oci_bind_by_name($stid, ':constrMoo', $constrMoo);
        if($parcelLandNo!="")   oci_bind_by_name($stid, ':parcelLandNo', $parcelLandNo);
        if($parcelLandMoo!="")  oci_bind_by_name($stid, ':parcelLandMoo', $parcelLandMoo);
        if($parcelLandName!="") oci_bind_by_name($stid, ':parcelLandName', $parcelLandName);
        if($obtainYear!="")     oci_bind_by_name($stid, ':obtainYear', $obtainYear); 
        if($_REQUEST['amphur']!=""){
            if(strpos($_REQUEST['amphur'],',')){
                for($i=0; $i<count($amphur); $i++){
                    oci_bind_by_name($stid, ':amphur'.$i, $amphur[$i]);
                }
            } else {
                oci_bind_by_name($stid, ':amphur', $amphur);
            }
        }
        if($_REQUEST['tambol']!=""){
            if(strpos($_REQUEST['tambol'],',')){
                for($i=0; $i<count($tambol); $i++){
                    oci_bind_by_name($stid, ':tambol'.$i, $tambol[$i]);
                }
            } else {
                oci_bind_by_name($stid, ':tambol', $tambol);
            }  
        }  
        oci_execute($stid);

        while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
            $Result[] = $row;
        }
    }

    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
    