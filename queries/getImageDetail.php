<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $seq1 = $_REQUEST['seq1'];
    $seq2 = $_REQUEST['seq2'];
    $printplateType = $_REQUEST['printplateType'];

    if($printplateType==1) $table = "PARCEL";
    else if($printplateType==10) $table = "CONDOROOM";
    else $table = "PARCEL_LAND";

    $sql = "";
    $sql .= "WITH RECV AS( ";
    $sql .=     "SELECT ".$table."_IMAGE_ORDER, ".$table."_IMAGE_PNAME, ".$table."_IMAGE_FILENAME, ".$table."_IMAGE_URL, ".$table."_IMAGE_SEQ ";
    $sql .=     "FROM MGT1.TB_REG_".$table."_IMAGE ";
    $sql .=     "WHERE ".$table."_SEQ = :seq1 AND RECORD_STATUS IN ('N', 'W', 'E') ";
    $sql .= "), OK AS ( ";
    $sql .=     "SELECT ".$table."_IMAGE_ORDER, ".$table."_IMAGE_PNAME, ".$table."_IMAGE_FILENAME, ".$table."_IMAGE_URL, ".$table."_IMAGE_SEQ ";
    $sql .=     "FROM REG.TB_REG_".$table."_IMAGE ";
    $sql .=     "WHERE ".$table."_SEQ = :seq2 AND RECORD_STATUS IN ('N', 'W', 'E') ";
    $sql .= ") ";
    $sql .= "SELECT NVL(RECV.".$table."_IMAGE_ORDER,OK.".$table."_IMAGE_ORDER) AS ODR, NVL(RECV.".$table."_IMAGE_PNAME,OK.".$table."_IMAGE_PNAME) AS NAME ";
    $sql .=     ",RECV.".$table."_IMAGE_FILENAME AS FILENAME, RECV.".$table."_IMAGE_URL AS URL ";
    $sql .=     ",OK.".$table."_IMAGE_FILENAME AS FILENAME_1, OK.".$table."_IMAGE_URL AS URL_1 ";
    $sql .= "FROM RECV ";
    $sql .= "RIGHT JOIN OK ";
    $sql .=     "ON RECV.".$table."_IMAGE_SEQ = OK.".$table."_IMAGE_SEQ ";
    $sql .= "ORDER BY TO_NUMBER(NVL(RECV.".$table."_IMAGE_ORDER,OK.".$table."_IMAGE_ORDER)) ";

    // echo $sql."\n";
    $Result = array();

    if($printplateType!=13 && $printplateType!=11) {
        $stid = oci_parse($conn, $sql);
        oci_bind_by_name($stid, ':seq1', $seq1);
        oci_bind_by_name($stid, ':seq2', $seq2);
        oci_execute($stid);
        while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
            $Result[] = $row;
        }
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
