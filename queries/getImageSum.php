<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice = $_REQUEST['landoffice'];

    $sql = "with recvall as
    (select p.printplate_type_seq, count(*) as count from mgt1.tb_reg_parcel_image pi
    inner join mgt1.tb_reg_parcel p
      on p.parcel_seq = pi.parcel_seq
      and p.record_status = 'N' and p.printplate_type_seq = 1
    where p.landoffice_seq = :landoffice and pi.record_status in ('N', 'W', 'E')
    group by p.printplate_type_seq
    union
    select p.printplate_type_seq, count(*) as count from mgt1.tb_reg_parcel_land_image pi
    inner join mgt1.tb_reg_parcel_land p
      on p.parcel_land_seq = pi.parcel_land_seq
      and p.record_status = 'N' and p.printplate_type_seq <> 1
    where p.landoffice_seq = :landoffice and pi.record_status in ('N', 'W', 'E')
    group by p.printplate_type_seq
    union
    select 10 as printplate_type_seq, count(*) as count from mgt1.tb_reg_condoroom_image pi
    inner join mgt1.tb_reg_condoroom p
      on p.condoroom_seq = pi.condoroom_seq
      and p.record_status = 'N'
    where p.landoffice_seq = :landoffice and pi.record_status in ('N', 'W', 'E')),
  ok as
    (select p.printplate_type_seq, count(*) as count from reg.tb_reg_parcel_image pi
    inner join mgt1.tb_reg_parcel_image p1
      on p1.parcel_image_seq = pi.parcel_image_seq
    inner join reg.tb_reg_parcel p
      on p.parcel_seq = pi.parcel_seq
      and p.record_status = 'N'  and p.printplate_type_seq = 1
    where p.landoffice_seq = :landoffice and pi.record_status in ('N', 'W', 'E')
    group by p.printplate_type_seq 
    union
    select p.printplate_type_seq, count(*) as count from reg.tb_reg_parcel_land_image pi
    inner join mgt1.tb_reg_parcel_land_image p1
      on p1.parcel_land_image_seq = pi.parcel_land_image_seq
    inner join reg.tb_reg_parcel_land p
      on p.parcel_land_seq = pi.parcel_land_seq
      and p.record_status = 'N' and p.printplate_type_seq <> 1
    where p.landoffice_seq = :landoffice and pi.record_status in ('N', 'W', 'E')
    group by p.printplate_type_seq
    union
    select p.printplate_type_seq, count(*) as count from reg.tb_reg_condoroom_image pi
    inner join mgt1.tb_reg_condoroom_image p1
      on p1.condoroom_image_seq = pi.condoroom_image_seq
    inner join reg.tb_reg_condoroom p
      on p.condoroom_seq = pi.condoroom_seq
      and p.record_status = 'N'
    where p.landoffice_seq = :landoffice and pi.record_status in ('N', 'W', 'E')
    group by p.printplate_type_seq),
  mp as
      (select printplate_type_seq, printplate_type_name from mgt1.tb_reg_mas_printplate_type where PRINTPLATE_TYPE_SEQ  in (1,2,17,3,5,4,8,23,13,10,9,7,6,11,15,16,18,19,20,21,22,28,12,14)
      order by (case printplate_type_seq 
      when 1 then 1 when 2 then 2 when 17 then 3 when 3 then 4
      when 5 then 5 when 4 then 6 when 8 then 7 when 23 then 8
      when 13 then 9 when 10 then 10 when 9 then 11 when 7 then 12
      when 6 then 13 when 11 then 14 when 15 then 15 when 16 then 16
      when 18 then 17 when 19 then 18 when 20 then 19 when 21 then 20
      when 22 then 21 when 28 then 22 when 12 then 23 when 14 then 24 end)),
  recv2 AS (
      SELECT NVL(par.printplate_type_seq,9999) AS PRINTPLATE_TYPE_SEQ, COUNT(img.PARCEL_LAND_HSFS_SEQ) TOTAL 
      FROM MGT1.tb_reg_parcel_land_hsfs img
      LEFT OUTER JOIN MGT1.tb_reg_parcel_land_index ind
          ON img.PARCEL_LAND_INDEX_SEQ = ind.PARCEL_LAND_INDEX_SEQ
      LEFT OUTER JOIN MGT1.tb_reg_parcel_land par
          ON ind.parcel_land_seq = par.parcel_land_seq
      WHERE par.landoffice_seq= :landoffice AND par.record_status ='N' AND ind.record_status ='N' AND par.printplate_type_seq  <> 1
          AND img.record_status IN ('N','W','E')  
      GROUP BY NVL(par.printplate_type_seq,9999)
  UNION
      SELECT 10 as printplate_type_seq, COUNT(img.CONDOROOM_HSFS_SEQ) TOTAL
      FROM MGT1.tb_reg_condoroom_hsfs img
      LEFT OUTER JOIN MGT1.tb_reg_condoroom_index ind
          ON img.CONDOROOM_INDEX_SEQ = ind.CONDOROOM_INDEX_SEQ
      LEFT OUTER JOIN MGT1.tb_reg_condoroom par
          ON ind.CONDOROOM_SEQ = par.CONDOROOM_SEQ
      WHERE par.landoffice_seq= :landoffice AND par.record_status ='N' AND ind.record_status ='N' 
          AND img.record_status IN ('N','W','E') 
  UNION
      SELECT 13 as printplate_type_seq,COUNT(img.CONDO_HSFS_SEQ) TOTAL
      FROM MGT1.tb_reg_condo_hsfs img
      LEFT OUTER JOIN MGT1.tb_reg_condo_index ind
          ON img.CONDO_INDEX_SEQ = ind.CONDO_INDEX_SEQ
      LEFT OUTER JOIN MGT1.tb_reg_condo par
          ON ind.CONDO_SEQ = par.CONDO_SEQ
      WHERE par.landoffice_seq= :landoffice AND par.record_status ='N' AND ind.record_status ='N' 
          AND img.record_status IN ('N','W','E') 
  UNION
      SELECT 1 as printplate_type_seq, COUNT(img.PARCEL_HSFS_SEQ) TOTAL
      FROM MGT1.tb_reg_parcel_hsfs img
      LEFT OUTER JOIN MGT1.tb_reg_parcel_index ind
          ON img.PARCEL_INDEX_SEQ = ind.PARCEL_INDEX_SEQ
      LEFT OUTER JOIN MGT1.tb_reg_parcel par
          ON ind.parcel_seq = par.parcel_seq
      WHERE par.landoffice_seq=:landoffice AND par.record_status ='N' AND ind.record_status ='N' 
          AND img.record_status IN ('N','W','E')  AND par.printplate_type_seq = 1
  UNION(
      SELECT 11 AS printplate_type_seq, COUNT(img.CONSTRUCT_HSFS_SEQ) TOTAL
      FROM MGT1.tb_reg_construct_hsfs img
      LEFT OUTER JOIN MGT1.tb_reg_construct_index ind
          ON img.CONSTRUCT_INDEX_SEQ = ind.CONSTRUCT_INDEX_SEQ
      LEFT OUTER JOIN MGT1.tb_reg_construct RC
          ON ind.CONSTRUCT_SEQ = RC.CONSTRUCT_SEQ
       INNER JOIN (  SELECT RPC.CONSTRUCT_SEQ, RP.PARCEL_SEQ LAND_SEQ
       FROM MGT1.TB_REG_PARCEL RP INNER JOIN MGT1.TB_REG_PARCEL_CONSTRUCT RPC 
          ON  RP.PARCEL_SEQ = RPC.PARCEL_SEQ
       WHERE RP.RECORD_STATUS = 'N' AND RPC.RECORD_STATUS = 'N' AND RP.LANDOFFICE_SEQ = :landoffice
       UNION
      SELECT RPC.CONSTRUCT_SEQ, RP.PARCEL_LAND_SEQ LAND_SEQ
       FROM MGT1.TB_REG_PARCEL_LAND RP INNER JOIN MGT1.TB_REG_PARCEL_LAND_CONSTRUCT RPC 
          ON  RP.PARCEL_LAND_SEQ = RPC.PARCEL_LAND_SEQ
       WHERE RP.RECORD_STATUS = 'N' AND RPC.RECORD_STATUS = 'N' AND RP.LANDOFFICE_SEQ = :landoffice ) G
          ON RC.CONSTRUCT_SEQ = G.CONSTRUCT_SEQ
      WHERE  RC.record_status ='N' AND ind.record_status ='N' 
          AND img.record_status IN ('N','W','E')
      )
  ),
  OK2 AS (
      SELECT 1 as printplate_type_seq, COUNT(img.PARCEL_HSFS_SEQ) TOTAL
      FROM REG.tb_reg_parcel_hsfs img
      inner join mgt1.tb_reg_parcel_hsfs img2
        on img.parcel_hsfs_seq = img2.parcel_hsfs_seq
         AND img2.record_status IN ('N','W','E')
      LEFT OUTER JOIN REG.tb_reg_parcel_index ind
          ON img.PARCEL_INDEX_SEQ = ind.PARCEL_INDEX_SEQ
      LEFT OUTER JOIN REG.tb_reg_parcel par
          ON ind.parcel_seq = par.parcel_seq
      WHERE par.landoffice_seq=:landoffice AND par.record_status ='N' AND ind.record_status ='N'  
          AND par.printplate_type_seq = 1
  UNION
      SELECT NVL(par.printplate_type_seq,9999) AS PRINTPLATE_TYPE_SEQ, COUNT(img.PARCEL_LAND_HSFS_SEQ) TOTAL 
      FROM REG.tb_reg_parcel_land_hsfs img
      inner join mgt1.tb_reg_parcel_land_hsfs img2
        on img.parcel_land_hsfs_seq = img2.parcel_land_hsfs_seq
         AND img2.record_status IN ('N','W','E')
      LEFT OUTER JOIN REG.tb_reg_parcel_land_index ind
          ON img.PARCEL_LAND_INDEX_SEQ = ind.PARCEL_LAND_INDEX_SEQ
      LEFT OUTER JOIN REG.tb_reg_parcel_land par
          ON ind.parcel_land_seq = par.parcel_land_seq
      WHERE par.landoffice_seq= :landoffice AND par.record_status ='N' AND ind.record_status ='N'  AND par.printplate_type_seq  <> 1
      AND img.record_status IN ('N','W','E')  
      GROUP BY NVL(par.printplate_type_seq,9999)
  UNION
      SELECT 10 as printplate_type_seq, COUNT(img.CONDOROOM_HSFS_SEQ) TOTAL
      FROM REG.tb_reg_condoroom_hsfs img
      inner join mgt1.tb_reg_condoroom_hsfs img2
        on img.condoroom_hsfs_seq = img2.condoroom_hsfs_seq
         AND img2.record_status IN ('N','W','E')
      LEFT OUTER JOIN REG.tb_reg_condoroom_index ind
          ON img.CONDOROOM_INDEX_SEQ = ind.CONDOROOM_INDEX_SEQ
      LEFT OUTER JOIN REG.tb_reg_condoroom par
          ON ind.CONDOROOM_SEQ = par.CONDOROOM_SEQ
      WHERE par.landoffice_seq= :landoffice AND par.record_status ='N' AND ind.record_status ='N' 
          AND img.record_status IN ('N','W','E') 
  UNION
      SELECT 13 as printplate_type_seq, COUNT(img.CONDO_HSFS_SEQ) TOTAL
      FROM REG.tb_reg_condo_hsfs img
      inner join mgt1.tb_reg_condo_hsfs img2
        on img.condo_hsfs_seq = img2.condo_hsfs_seq
         AND img2.record_status IN ('N','W','E')
      LEFT OUTER JOIN REG.tb_reg_condo_index ind
          ON img.CONDO_INDEX_SEQ = ind.CONDO_INDEX_SEQ
      LEFT OUTER JOIN REG.tb_reg_condo par
          ON ind.CONDO_SEQ = par.CONDO_SEQ
      WHERE par.landoffice_seq=:landoffice AND par.record_status ='N' AND ind.record_status ='N' 
          AND img.record_status IN ('N','W','E')
  UNION(
      SELECT 11 AS printplate_type_seq, COUNT(img.CONSTRUCT_HSFS_SEQ) TOTAL
      FROM REG.tb_reg_construct_hsfs img
      inner join mgt1.tb_reg_construct_hsfs img2
        on img.construct_hsfs_seq = img2.construct_hsfs_seq
         AND img2.record_status IN ('N','W','E')
      LEFT OUTER JOIN REG.tb_reg_construct_index ind
          ON img.CONSTRUCT_INDEX_SEQ = ind.CONSTRUCT_INDEX_SEQ
      LEFT OUTER JOIN REG.tb_reg_construct RC
          ON ind.CONSTRUCT_SEQ = RC.CONSTRUCT_SEQ
       INNER JOIN (  SELECT RPC.CONSTRUCT_SEQ, RP.PARCEL_SEQ LAND_SEQ
       FROM REG.TB_REG_PARCEL RP INNER JOIN REG.TB_REG_PARCEL_CONSTRUCT RPC 
          ON  RP.PARCEL_SEQ = RPC.PARCEL_SEQ
       WHERE RP.RECORD_STATUS = 'N' AND RPC.RECORD_STATUS = 'N' AND RP.LANDOFFICE_SEQ = :landoffice 
       UNION
      SELECT RPC.CONSTRUCT_SEQ, RP.PARCEL_LAND_SEQ LAND_SEQ
       FROM REG.TB_REG_PARCEL_LAND RP INNER JOIN REG.TB_REG_PARCEL_LAND_CONSTRUCT RPC 
          ON  RP.PARCEL_LAND_SEQ = RPC.PARCEL_LAND_SEQ
       WHERE RP.RECORD_STATUS = 'N' AND RPC.RECORD_STATUS = 'N' AND RP.LANDOFFICE_SEQ = :landoffice )G
          ON RC.CONSTRUCT_SEQ = G.CONSTRUCT_SEQ
      WHERE  RC.record_status ='N' AND ind.record_status ='N' 
          AND img.record_status IN ('N','W','E')
      )
  )
  select mp.printplate_type_name, nvl(recvall.count,0) as RECEIVE, nvl(ok.count,0) as MIGRATE_SUCCESS, (nvl(recvall.count,0)-nvl(ok.count,0)) as MIGRATE_ERROR,
      NVL(RECV2.TOTAL,0) AS RECEIVE2, NVL(OK2.TOTAL,0) AS MIGRATE_SUCCESS2, (NVL(RECV2.TOTAL,0)-NVL(OK2.TOTAL,0)) as MIGRATE_ERROR2
  from mp
  left join recvall
      on recvall.printplate_type_seq = mp.printplate_type_seq
  left join ok
    on mp.printplate_type_seq = ok.printplate_type_seq
  left join recv2
      on recv2.printplate_type_seq = mp.printplate_type_seq
  left join ok2
    on mp.printplate_type_seq = ok2.printplate_type_seq
  ";

    // echo $sql."\n";
    $stid = oci_parse($conn, $sql);
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    $jsonData = array(
        "data" => $Result
    );
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
