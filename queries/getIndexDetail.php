<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $seq1 = $_REQUEST['seq1'];
    $seq2 = $_REQUEST['seq2'];
    $printplateType = $_REQUEST['printplateType'];

    if($printplateType==1) $table = "PARCEL";
    else if($printplateType==10) $table = "CONDOROOM";
    else if($printplateType==13) $table = "CONDO";
    else $table = "PARCEL_LAND";

    $sql = '';
    $sql .= "WITH RECV AS ( ";
    $sql .=     "SELECT DISTINCT ".$table."_INDEX_REGDTM, ".$table."_INDEX_ORDER AS ".$table."_INDEX_ORDER, REGISTER_SEQ, ".$table."_INDEX_REGNAME, ".$table."_HSFS_ORDER, ".$table."_HSFS_PNO, COUNT(DOCUMENT_SEQ) OVER (PARTITION BY PH.".$table."_INDEX_SEQ, DOCUMENT_SEQ) AS NUM ";
    $sql .=         ", DOCUMENT_SEQ, ".$table."_HSFS_PNAME, ".$table."_HSFS_URL, ".$table."_HSFS_FILENAME, ".$table."_HSFS_SEQ ";
    $sql .=     "FROM MGT1.TB_REG_".$table."_HSFS PH ";
    $sql .=     "INNER JOIN MGT1.TB_REG_".$table."_INDEX PI ";
    $sql .=         "ON PI.".$table."_INDEX_SEQ = PH.".$table."_INDEX_SEQ ";
    $sql .=         "AND PI.RECORD_STATUS = 'N' ";
    $sql .=     "WHERE PH.RECORD_STATUS IN ('N', 'W', 'E') AND ".$table."_SEQ = :seq1 ";
    $sql .= "), OK AS ( ";
    $sql .=    " SELECT DISTINCT ".$table."_INDEX_REGDTM, ".$table."_INDEX_ORDER AS ".$table."_INDEX_ORDER, REGISTER_SEQ, ".$table."_INDEX_REGNAME, ".$table."_HSFS_ORDER, ".$table."_HSFS_PNO, COUNT(DOCUMENT_SEQ) OVER (PARTITION BY PH.".$table."_INDEX_SEQ, DOCUMENT_SEQ) AS NUM ";
    $sql .=         ", DOCUMENT_SEQ, ".$table."_HSFS_PNAME, ".$table."_HSFS_URL, ".$table."_HSFS_FILENAME, ".$table."_HSFS_SEQ ";
    $sql .=     "FROM REG.TB_REG_".$table."_HSFS PH ";
    $sql .=     "INNER JOIN REG.TB_REG_".$table."_INDEX PI ";
    $sql .=         "ON PI.".$table."_INDEX_SEQ = PH.".$table."_INDEX_SEQ ";
    $sql .=         "AND PI.RECORD_STATUS = 'N' ";
    $sql .=     "WHERE PH.RECORD_STATUS IN ('N', 'W', 'E') AND ".$table."_SEQ = :seq2 ";
    $sql .= ") ";
    $sql .= "SELECT DISTINCT NVL(TO_DATE(RECV.".$table."_INDEX_REGDTM,'DD/MM/YYYY'),TO_DATE(OK.".$table."_INDEX_REGDTM,'DD/MM/YYYY')) AS DTM ";
    $sql .=     ", NVL(RECV.".$table."_INDEX_ORDER,OK.".$table."_INDEX_ORDER) AS ORD_INX, NVL(RECV.".$table."_INDEX_REGNAME,OK.".$table."_INDEX_REGNAME) AS REGIST ";
    $sql .=     ", NVL(RECV.".$table."_HSFS_ORDER,OK.".$table."_HSFS_ORDER) AS ORD_HF, NVL(RECV.".$table."_HSFS_PNO,OK.".$table."_HSFS_PNO) AS CPAGE, NVL(RECV.NUM,OK.NUM) AS NPAGE ";
    $sql .=     ", NVL(RECV.".$table."_HSFS_PNAME,OK.".$table."_HSFS_PNAME) AS DOC_NAME ";
    $sql .=     ", RECV.".$table."_HSFS_URL AS URL, RECV.".$table."_HSFS_FILENAME AS FILENAME, OK.".$table."_HSFS_URL AS URL_1, OK.".$table."_HSFS_FILENAME AS FILENAME_1 ";
    $sql .= "FROM RECV ";
    $sql .= "RIGHT JOIN OK ";
    $sql .=     "ON RECV.".$table."_HSFS_SEQ = OK.".$table."_HSFS_SEQ ";
    $sql .= "ORDER BY NVL(TO_DATE(RECV.".$table."_INDEX_REGDTM,'DD/MM/YYYY'),TO_DATE(OK.".$table."_INDEX_REGDTM,'DD/MM/YYYY')), NVL(RECV.".$table."_INDEX_ORDER,OK.".$table."_INDEX_ORDER) ";
    $sql .=     ",NVL(RECV.".$table."_HSFS_ORDER,OK.".$table."_HSFS_ORDER) ,NVL(RECV.".$table."_HSFS_PNO,OK.".$table."_HSFS_PNO) ";

    // echo $sql."\n";
    $stid = oci_parse($conn, $sql);
    oci_bind_by_name($stid, ':seq1', $seq1);
    oci_bind_by_name($stid, ':seq2', $seq2);
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
