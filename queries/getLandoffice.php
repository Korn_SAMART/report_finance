<?php
    session_start();
    if(!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')){
        include '403.php';
        exit(0);
    }

    header('Content-Type: application/json; charset=utf-8');
    include '../database/conn.php';

    $province = '';
    if(isset($_GET['province'])){
        $province = $_GET['province'];
    }

    $selectLandoffice ="SELECT LANDOFFICE_SEQ, LANDOFFICE_NAME_TH
                        FROM MAS.TB_MAS_LANDOFFICE
                        WHERE PROVINCE_SEQ = :province AND LANDOFFICE_SEQ IN (
                            SELECT LANDOFFICE_SEQ FROM DATAM.TB_MAS_LANDOFFICE
                            WHERE TYPE IS NULL
                            )
                        ORDER BY NLSSORT(LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY')";
                        
    $stid = oci_parse($conn, $selectLandoffice);
    oci_bind_by_name($stid, ':province', $province);
    oci_execute($stid);
    $Result = array();
    $branch = '';
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);



?>
