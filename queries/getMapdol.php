<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';

    $requestData = $_REQUEST;
    // print_r($requestData);
    // echo "\n".$requestData['landoffice'];

    $condition = " ";
    if($requestData["utmScale"]!="") $condition .= "AND UTMSCALE = :utmScale ";
    if($requestData["utmmap1"]!="") $condition .= "AND UTMMAP1 = :utmmap1 ";
    if($requestData["utmmap2"]!="") $condition .= "AND UTMMAP2 = :utmmap2 ";
    if($requestData["utmmap3"]!="") $condition .= "AND UTMMAP3 = :utmmap3 ";
    if($requestData["utmmap4"]!="") $condition .= "AND UTMMAP4 = :utmmap4 ";
    if($requestData["landno"]!="") $condition .= "AND LAND_NO = :landno ";

    include '../database/conn.php';

    $sql = "WITH RECV AS ( ";
    if($requestData['rvType']=="" || $requestData['rvType']=="1") {
        $sql .= "SELECT LANDOFFICE_SEQ, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE, UTMSCALE, UTMMAP1||' '||TRIM(TO_CHAR(UTMMAP2,'RN'))||' '||UTMMAP3 AS UTM, UTMMAP4, LAND_NO, 47 AS ZONE, 'ยู ที เอ็ม' AS RV, LOG1_MIGRATE_NOTE ";
        $sql .= $requestData['table']=="" ? "FROM MGT1.MAP_LAND_GIS_47 " : "FROM MGT1.MAP_LAND_TEMP_47 ";
        $sql .= "WHERE LANDOFFICE_SEQ = :landoffice AND NVL(RECORD_STATUS,'N') = 'N' AND NVL(PARCEL_TYPE,99) = :parcelType ";
        $sql .= $condition;
        $sql .= "UNION ";
        $sql .= "SELECT LANDOFFICE_SEQ, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE, UTMSCALE, UTMMAP1||' '||TRIM(TO_CHAR(UTMMAP2,'RN'))||' '||UTMMAP3 AS UTM, UTMMAP4, LAND_NO, 48 AS ZONE, 'ยู ที เอ็ม' AS RV, LOG1_MIGRATE_NOTE ";
        $sql .= $requestData['table']=="" ? "FROM MGT1.MAP_LAND_GIS_48 " : "FROM MGT1.MAP_LAND_TEMP_48 ";
        $sql .= "WHERE LANDOFFICE_SEQ = :landoffice AND NVL(RECORD_STATUS,'N') = 'N' AND NVL(PARCEL_TYPE,99) = :parcelType ";
        $sql .= $condition;
    }
    if($requestData['parcelType']>1 && ($requestData['rvType']=="" || $requestData['rvType']=="2")){
        if($requestData['rvType']=="") $sql .= "UNION ";
        $sql .= "SELECT LANDOFFICE_SEQ, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE, UTMSCALE, UTMMAP1||UTMMAP2 AS UTM, UTMMAP4, LAND_NO, 47 AS ZONE, 'น.ส.3 ก' AS RV, LOG1_MIGRATE_NOTE ";
        $sql .= "FROM MGT1.MAP_LAND_NS3K".$requestData["table"]."_47 ";
        $sql .= "WHERE LANDOFFICE_SEQ = :landoffice AND NVL(RECORD_STATUS,'N') = 'N' AND NVL(PARCEL_TYPE,99) = :parcelType ";
        $sql .= $condition;
        $sql .= "UNION ";
        $sql .= "SELECT LANDOFFICE_SEQ, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE, UTMSCALE, UTMMAP1||UTMMAP2 AS UTM, UTMMAP4, LAND_NO, 48 AS ZONE, 'น.ส.3 ก' AS RV, LOG1_MIGRATE_NOTE ";
        $sql .= "FROM MGT1.MAP_LAND_NS3K".$requestData["table"]."_48 ";
        $sql .= "WHERE LANDOFFICE_SEQ = :landoffice AND NVL(RECORD_STATUS,'N') = 'N' AND NVL(PARCEL_TYPE,99) = :parcelType ";
        $sql .= $condition;
    }
    $sql .= "), ";

    $sql .= "OK AS ( ";
    if($requestData['rvType']=="" || $requestData['rvType']=="1") {
        $sql .= "SELECT LANDOFFICE_SEQ, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE, UTMSCALE, UTMMAP1||' '||TRIM(TO_CHAR(UTMMAP2,'RN'))||' '||UTMMAP3 AS UTM, UTMMAP4, LAND_NO, 47 AS ZONE, 'ยู ที เอ็ม' AS RV, NULL AS LOG1_MIGRATE_NOTE ";
        $sql .= $requestData['table']=="" ? "FROM MAPDOL.MAP_LAND_GIS_47 " : "FROM MAPDOL.MAP_LAND_TEMP_47 ";
        $sql .= "WHERE LANDOFFICE_SEQ = :landoffice AND NVL(RECORD_STATUS,'N') = 'N' AND NVL(PARCEL_TYPE,99) = :parcelType ";
        $sql .= $condition;
        $sql .= "UNION ";
        $sql .= "SELECT LANDOFFICE_SEQ, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE, UTMSCALE, UTMMAP1||' '||TRIM(TO_CHAR(UTMMAP2,'RN'))||' '||UTMMAP3 AS UTM, UTMMAP4, LAND_NO, 48 AS ZONE, 'ยู ที เอ็ม' AS RV, NULL AS LOG1_MIGRATE_NOTE ";
        $sql .= $requestData['table']=="" ? "FROM MAPDOL.MAP_LAND_GIS_48 " : "FROM MAPDOL.MAP_LAND_TEMP_48 ";
        $sql .= "WHERE LANDOFFICE_SEQ = :landoffice AND NVL(RECORD_STATUS,'N') = 'N' AND NVL(PARCEL_TYPE,99) = :parcelType ";
        $sql .= $condition;
    }
    if($requestData['parcelType']>1 && ($requestData['rvType']=="" || $requestData['rvType']=="2")){
        if($requestData['rvType']=="") $sql .= "UNION ";
        $sql .= "SELECT LANDOFFICE_SEQ, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE, UTMSCALE, UTMMAP1||UTMMAP2 AS UTM, UTMMAP4, LAND_NO, 47 AS ZONE, 'น.ส.3 ก' AS RV, NULL AS LOG1_MIGRATE_NOTE ";
        $sql .= "FROM MAPDOL.MAP_LAND_NS3K".$requestData["table"]."_47 ";
        $sql .= "WHERE LANDOFFICE_SEQ = :landoffice AND NVL(RECORD_STATUS,'N') = 'N' AND NVL(PARCEL_TYPE,99) = :parcelType ";
        $sql .= $condition;
        $sql .= "UNION ";
        $sql .= "SELECT LANDOFFICE_SEQ, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE, UTMSCALE, UTMMAP1||UTMMAP2 AS UTM, UTMMAP4, LAND_NO, 48 AS ZONE, 'น.ส.3 ก' AS RV, NULL AS LOG1_MIGRATE_NOTE ";
        $sql .= "FROM MAPDOL.MAP_LAND_NS3K".$requestData["table"]."_48 ";
        $sql .= "WHERE LANDOFFICE_SEQ = :landoffice AND NVL(RECORD_STATUS,'N') = 'N' AND NVL(PARCEL_TYPE,99) = :parcelType ";
        $sql .= $condition;
    }
    $sql .= ") ";

    $sql .= "SELECT ROW_NUMBER() OVER (ORDER BY NVL(RECV.UTMSCALE,OK.UTMSCALE), NVL(RECV.UTM,OK.UTM), NVL(RECV.UTMMAP4,OK.UTMMAP4), TO_NUMBER(NVL(RECV.LAND_NO,OK.LAND_NO))) AS NUM, ";
    $sql .= "RECV.*, ";
    $sql .= "OK.UTMSCALE AS UTMSCALE_1, OK.UTM AS UTM_1, OK.UTMMAP4 AS UTMMAP4_1, OK.LAND_NO AS LAND_NO_1, OK.ZONE AS ZONE_1, OK.RV AS RV_1 ";
    $sql .= "FROM RECV ";
    $sql .= "FULL OUTER JOIN OK ";
    $sql .= "ON RECV.LANDOFFICE_SEQ||RECV.PARCEL_TYPE||RECV.UTMSCALE||RECV.UTM||RECV.UTMMAP4||RECV.LAND_NO||RECV.ZONE ";
    $sql .= "= OK.LANDOFFICE_SEQ||OK.PARCEL_TYPE||OK.UTMSCALE||OK.UTM||OK.UTMMAP4||OK.LAND_NO||OK.ZONE ";

    $sql .= "ORDER BY NVL(RECV.UTMSCALE,OK.UTMSCALE), NVL(RECV.UTM,OK.UTM), NVL(RECV.UTMMAP4,OK.UTMMAP4), TO_NUMBER(NVL(RECV.LAND_NO,OK.LAND_NO)) ";

    // echo "\n".$sql;
    $stid = oci_parse($conn, $sql);
    if($requestData["utmScale"]!="") oci_bind_by_name($stid, ':utmScale', $requestData['utmScale']);
    if($requestData["utmmap1"]!="") oci_bind_by_name($stid, ':utmmap1', $requestData['utmmap1']);
    if($requestData["utmmap2"]!="") oci_bind_by_name($stid, ':utmmap2', $requestData['utmmap2']);
    if($requestData["utmmap3"]!="") oci_bind_by_name($stid, ':utmmap3', $requestData['utmmap3']);
    if($requestData["utmmap4"]!="") oci_bind_by_name($stid, ':utmmap4', $requestData['utmmap4']);
    if($requestData["landno"]!="") oci_bind_by_name($stid, ':landno', $requestData['landno']);
    oci_bind_by_name($stid, ':landoffice', $requestData['landoffice']);
    oci_bind_by_name($stid, ':parcelType', $requestData['parcelType']);
    oci_execute($stid);

    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;   
    }

    $jsonData = array(
        "data" => $Result
    );

    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);

?>