<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';

    $requestData = $_REQUEST;
    // print_r($requestData);
    // echo "\n".$requestData['landoffice'];

    $condition = "WHERE M.LANDOFFICE_SEQ = :landoffice AND NVL(M.RECORD_STATUS,'N') = 'N' ";
    $condition2 = "";
    // $condition .= "AND NVL(M.PARCEL_TYPE,99) = :parcelType ";
    $condition .=  "AND PT.PRINTPLATE_TYPE_SEQ = :printplateType ";

    if($requestData["utmScale"]!="") $condition .= "AND M.UTMSCALE = :utmScale ";
    if($requestData["utmmap1"]!="") $condition .= "AND M.UTMMAP1 = :utmmap1 ";
    if($requestData["utmmap2"]!="") $condition .= "AND M.UTMMAP2 = :utmmap2 ";
    if($requestData["utmmap3"]!="") $condition .= "AND M.UTMMAP3 = :utmmap3 ";
    if($requestData["utmmap4"]!="") $condition .= "AND M.UTMMAP4 = :utmmap4 ";
    if($requestData["landno"]!="") $condition .= "AND M.LAND_NO = :landno ";

    $leftMas = "";
    $leftMas .= "LEFT JOIN SVO.TB_UDM_MAS_PARCELTYPE PT
                    ON PT.PARCELTYPE_SEQ = M.PARCEL_TYPE
                LEFT JOIN MGT1.TB_SVA_MAS_SCALE S
                    ON M.UTMSCALE = S.SCALE_NAME ";
    $leftParcel = "";
    $leftParcel .= "LEFT JOIN MGT1.TB_REG_PARCEL P
                        ON P.LANDOFFICE_SEQ = :landoffice
                        AND P.RECORD_STATUS = 'N'
                        AND S.SCALE_SEQ = P.UTMSCALE_SEQ
                        AND M.UTMMAP1 = P.PARCEL_UTMMAP1
                        AND M.UTMMAP2 = P.PARCEL_UTMMAP2
                        AND TO_CHAR(M.UTMMAP3) = P.PARCEL_UTMMAP3
                        AND M.UTMMAP4 = P.PARCEL_UTMMAP4
                        AND M.LAND_NO = TO_CHAR(P.PARCEL_UTM_LAND_NO)
                        AND PT.PRINTPLATE_TYPE_SEQ = P.PRINTPLATE_TYPE_SEQ ";
    $leftParcelLand = "";
    $leftParcelLand .= "LEFT JOIN MGT1.TB_REG_PARCEL_LAND PL
                            ON PL.RECORD_STATUS = 'N'
                            AND PL.LANDOFFICE_SEQ = :landoffice
                            AND S.SCALE_SEQ = PL.UTMSCALE_SEQ
                            AND M.UTMMAP1 = PL.PARCEL_LAND_UTMMAP1
                            AND M.UTMMAP2 = PL.PARCEL_LAND_UTMMAP2
                            AND TO_CHAR(M.UTMMAP3) = PL.PARCEL_LAND_UTMMAP3
                            AND M.UTMMAP4 = PL.PARCEL_LAND_UTMMAP4
                            AND M.LAND_NO = TO_CHAR(PL.PARCEL_LAND_UTM_LAND_NO)
                            AND PT.PRINTPLATE_TYPE_SEQ = PL.PRINTPLATE_TYPE_SEQ ";
    $leftParcelLand2 ="";
    $leftParcelLand2 = "LEFT JOIN MGT1.TB_REG_PARCEL_LAND PL
                            ON PL.RECORD_STATUS = 'N'
                            AND PL.LANDOFFICE_SEQ = :landoffice
                            AND S.SCALE_SEQ = PL.AIRPHOTOSCALE_SEQ
                            AND M.UTMMAP1 = PL.PARCEL_LAND_AIRPHOTOMAP1
                            AND M.UTMMAP2 = PL.PARCEL_LAND_AIRPHOTOMAP2
                            AND M.UTMMAP4 = PL.PARCEL_LAND_AIRPHOTOMAP3
                            AND TO_CHAR(M.LAND_NO) = TO_CHAR(PL.PARCEL_LAND_AIRPHOTO_LAND_NO) 
                            AND PT.PRINTPLATE_TYPE_SEQ = PL.PRINTPLATE_TYPE_SEQ ";

    include '../database/conn.php';

    $sql = "";
    $sql .=     "WITH RECV AS ( ";

    if($requestData['rvType']=="" || $requestData['rvType']=="1") {
        if($requestData['zone']=="47"){
            $sql .=     "SELECT DISTINCT M.LANDOFFICE_SEQ, NVL(M.PARCEL_TYPE,99) AS PARCEL_TYPE, PT.PRINTPLATE_TYPE_SEQ, M.UTMSCALE, M.UTMMAP1, M.UTMMAP2, M.UTMMAP3, M.UTMMAP4, M.LAND_NO, 47 AS ZONE, 1 AS RV, M.LOG1_MIGRATE_NOTE
                            , NVL(P.PARCEL_NO,PL.PARCEL_LAND_NO) AS NO, NVL(P.AMPHUR_SEQ,PL.AMPHUR_SEQ) AS AMPHUR, NVL(P.TAMBOL_SEQ,PL.TAMBOL_SEQ) AS TAMBOL, NVL(P.PRINTPLATE_TYPE_SEQ,PL.PRINTPLATE_TYPE_SEQ) AS PT
                            , NVL(P.PARCEL_MOO,PL.PARCEL_LAND_MOO) AS MOO
                            , M.MAP_PARCEL_SEQ, P.PARCEL_SEQ, PL.PARCEL_LAND_SEQ
                    FROM MGT1.MAP_LAND_GIS_47 M ";
            $sql .=     $leftMas;
            $sql .=     $leftParcel;
            $sql .=     $leftParcelLand;
            $sql .=     $condition;
        }
    // $sql .=     "UNION ";
        else if($requestData['zone']=="48"){
            $sql .=     "SELECT DISTINCT M.LANDOFFICE_SEQ, NVL(M.PARCEL_TYPE,99) AS PARCEL_TYPE, PT.PRINTPLATE_TYPE_SEQ, M.UTMSCALE, M.UTMMAP1, M.UTMMAP2, M.UTMMAP3, M.UTMMAP4, M.LAND_NO, 48 AS ZONE, 1 AS RV, M.LOG1_MIGRATE_NOTE
                            , NVL(P.PARCEL_NO,PL.PARCEL_LAND_NO) AS NO, NVL(P.AMPHUR_SEQ,PL.AMPHUR_SEQ) AS AMPHUR, NVL(P.TAMBOL_SEQ,PL.TAMBOL_SEQ) AS TAMBOL, NVL(P.PRINTPLATE_TYPE_SEQ,PL.PRINTPLATE_TYPE_SEQ) AS PT
                            , NVL(P.PARCEL_MOO,PL.PARCEL_LAND_MOO) AS MOO
                            , M.MAP_PARCEL_SEQ, P.PARCEL_SEQ, PL.PARCEL_LAND_SEQ
                    FROM MGT1.MAP_LAND_GIS_48 M ";
            $sql .=     $leftMas;
            $sql .=     $leftParcel;
            $sql .=     $leftParcelLand;
            $sql .=     $condition;
        }
    }

    if($requestData['printplateType']>1 && ($requestData['rvType']=="" || $requestData['rvType']=="2")){
        if($requestData['rvType']=="") $sql .=     "UNION ";
        if($requestData['zone']=="47"){
            $sql .=     "SELECT DISTINCT M.LANDOFFICE_SEQ, NVL(M.PARCEL_TYPE,99) AS PARCEL_TYPE, PT.PRINTPLATE_TYPE_SEQ, M.UTMSCALE, M.UTMMAP1, M.UTMMAP2, NULL AS UTMMAP3, M.UTMMAP4, M.LAND_NO, 47 AS ZONE, 2 AS RV, M.LOG1_MIGRATE_NOTE
                            , PL.PARCEL_LAND_NO AS NO, PL.AMPHUR_SEQ AS AMPHUR, PL.TAMBOL_SEQ AS TAMBOL, PL.PRINTPLATE_TYPE_SEQ AS PT
                            , PL.PARCEL_LAND_MOO AS MOO
                            , M.MAP_PARCEL_SEQ, NULL AS PARCEL_SEQ, PL.PARCEL_LAND_SEQ
                        FROM MGT1.MAP_LAND_NS3K_47 M ";
            $sql .=     $leftMas;
            $sql .=     $leftParcelLand2;
            $sql .=     $condition;
        }
        // $sql .=     "UNION ";
        else if ($requestData['zone']=="48"){
            $sql .=     "SELECT DISTINCT M.LANDOFFICE_SEQ, NVL(M.PARCEL_TYPE,99) AS PARCEL_TYPE, PT.PRINTPLATE_TYPE_SEQ, M.UTMSCALE, M.UTMMAP1, M.UTMMAP2, NULL AS UTMMAP3, M.UTMMAP4, M.LAND_NO, 48 AS ZONE, 2 AS RV, M.LOG1_MIGRATE_NOTE
                            , PL.PARCEL_LAND_NO AS NO, PL.AMPHUR_SEQ AS AMPHUR, PL.TAMBOL_SEQ AS TAMBOL, PL.PRINTPLATE_TYPE_SEQ AS PT
                            , PL.PARCEL_LAND_MOO AS MOO
                            , M.MAP_PARCEL_SEQ, NULL AS PARCEL_SEQ, PL.PARCEL_LAND_SEQ
                        FROM MGT1.MAP_LAND_NS3K_48 M ";
            $sql .=     $leftMas;
            $sql .=     $leftParcelLand2;
            $sql .=     $condition;
        }
    }

    $sql .=     "), OK AS ( ";

    if($requestData['rvType']=="" || $requestData['rvType']=="1") {
        if($requestData['zone']=="47"){
            $sql .=     "SELECT DISTINCT M.LANDOFFICE_SEQ, NVL(M.PARCEL_TYPE,99) AS PARCEL_TYPE, PT.PRINTPLATE_TYPE_SEQ, M.UTMSCALE, M.UTMMAP1, M.UTMMAP2, M.UTMMAP3, M.UTMMAP4, M.LAND_NO, 47 AS ZONE, 1 AS RV, NULL AS LOG
                            , NVL(P.PARCEL_NO,PL.PARCEL_LAND_NO) AS NO, NVL(P.AMPHUR_SEQ,PL.AMPHUR_SEQ) AS AMPHUR, NVL(P.TAMBOL_SEQ,PL.TAMBOL_SEQ) AS TAMBOL, NVL(P.PRINTPLATE_TYPE_SEQ,PL.PRINTPLATE_TYPE_SEQ) AS PT
                            , NVL(P.PARCEL_MOO,PL.PARCEL_LAND_MOO) AS MOO
                            , M.MAP_PARCEL_SEQ, P.PARCEL_SEQ, PL.PARCEL_LAND_SEQ
                        FROM MAPDOL.MAP_LAND_GIS_47 M ";
            $sql .=     $leftMas;
            $sql .=     $leftParcel;
            $sql .=     $leftParcelLand;
            $sql .=     $condition;
        }
        // $sql .=     "UNION ";
        else if($requestData['zone']=="48"){
            $sql .=     "SELECT DISTINCT M.LANDOFFICE_SEQ, NVL(M.PARCEL_TYPE,99) AS PARCEL_TYPE, PT.PRINTPLATE_TYPE_SEQ, M.UTMSCALE, M.UTMMAP1, M.UTMMAP2, M.UTMMAP3, M.UTMMAP4, M.LAND_NO, 48 AS ZONE, 1 AS RV, NULL AS LOG
                            , NVL(P.PARCEL_NO,PL.PARCEL_LAND_NO) AS NO, NVL(P.AMPHUR_SEQ,PL.AMPHUR_SEQ) AS AMPHUR, NVL(P.TAMBOL_SEQ,PL.TAMBOL_SEQ) AS TAMBOL, NVL(P.PRINTPLATE_TYPE_SEQ,PL.PRINTPLATE_TYPE_SEQ) AS PT
                            , NVL(P.PARCEL_MOO,PL.PARCEL_LAND_MOO) AS MOO
                            , M.MAP_PARCEL_SEQ, P.PARCEL_SEQ, PL.PARCEL_LAND_SEQ
                        FROM MAPDOL.MAP_LAND_GIS_48 M ";
            $sql .=     $leftMas;
            $sql .=     $leftParcel;
            $sql .=     $leftParcelLand;
            $sql .=     $condition;
        }
    }
    
    if($requestData['printplateType']>1 && ($requestData['rvType']=="" || $requestData['rvType']=="2")){
        if($requestData['rvType']=="") $sql .=     "UNION ";
        if($requestData['zone']=="47"){
            $sql .=     "SELECT DISTINCT M.LANDOFFICE_SEQ, NVL(M.PARCEL_TYPE,99) AS PARCEL_TYPE, PT.PRINTPLATE_TYPE_SEQ, M.UTMSCALE, M.UTMMAP1, M.UTMMAP2, NULL AS UTMMAP3, M.UTMMAP4, M.LAND_NO, 47 AS ZONE, 2 AS RV, NULL AS LOG
                            , PL.PARCEL_LAND_NO AS NO, PL.AMPHUR_SEQ AS AMPHUR, PL.TAMBOL_SEQ AS TAMBOL, PL.PRINTPLATE_TYPE_SEQ AS PT
                            , PL.PARCEL_LAND_MOO AS MOO
                            , M.MAP_PARCEL_SEQ, NULL AS PARCEL_SEQ, PL.PARCEL_LAND_SEQ
                        FROM MAPDOL.MAP_LAND_NS3K_47 M ";
            $sql .=     $leftMas;
            $sql .=     $leftParcelLand2;
            $sql .=     $condition;
        }
        // $sql .=     "UNION ";
        else if($requestData['zone']=="48"){
            $sql .=     "SELECT DISTINCT M.LANDOFFICE_SEQ, NVL(M.PARCEL_TYPE,99) AS PARCEL_TYPE, PT.PRINTPLATE_TYPE_SEQ, M.UTMSCALE, M.UTMMAP1, M.UTMMAP2, NULL AS UTMMAP3, M.UTMMAP4, M.LAND_NO, 48 AS ZONE, 2 AS RV, NULL AS LOG
                            , PL.PARCEL_LAND_NO AS NO, PL.AMPHUR_SEQ AS AMPHUR, PL.TAMBOL_SEQ AS TAMBOL, PL.PRINTPLATE_TYPE_SEQ AS PT
                            , PL.PARCEL_LAND_MOO AS MOO
                            , M.MAP_PARCEL_SEQ, NULL AS PARCEL_SEQ, PL.PARCEL_LAND_SEQ
                        FROM MAPDOL.MAP_LAND_NS3K_48 M ";
            $sql .=     $leftMas;
            $sql .=     $leftParcelLand2;
            $sql .=     $condition;
        }
    }
    $sql .= ") 
            SELECT NVL(RECV.LANDOFFICE_SEQ,OK.LANDOFFICE_SEQ) AS LANDOFFICE, AMPHUR_NAME, TAMBOL_NAME, NVL(RECV.MOO,OK.MOO) AS MOO, NVL(RECV.NO,OK.NO) AS NO, NVL(RECV.PRINTPLATE_TYPE_SEQ,OK.PRINTPLATE_TYPE_SEQ) AS PT
                , NVL(RECV.PARCEL_TYPE,OK.PARCEL_TYPE) AS PT_SVO
                , RECV.UTMMAP1||' '||TRIM(TO_CHAR(RECV.UTMMAP2,'RN'))||' '||RECV.UTMMAP3 AS UTM, RECV.UTMMAP4, RECV.UTMSCALE, RECV.LAND_NO, RECV.ZONE, RECV.RV
                , OK.UTMMAP1||' '||TRIM(TO_CHAR(OK.UTMMAP2,'RN'))||' '||OK.UTMMAP3 AS UTM_1, OK.UTMMAP4 AS UTMMAP4_1, OK.UTMSCALE AS UTMSCALE_1, OK.LAND_NO LAND_NO_1, OK.ZONE AS ZONE_1, OK.RV AS RV_1
            FROM RECV
            RIGHT JOIN OK
                ON RECV.MAP_PARCEL_SEQ = OK.MAP_PARCEL_SEQ
                AND NVL(RECV.PARCEL_SEQ,0) = NVL(OK.PARCEL_SEQ,0)
                AND NVL(RECV.PARCEL_LAND_SEQ,0) = NVL(OK.PARCEL_LAND_SEQ,0)
            LEFT JOIN MAS.TB_MAS_AMPHUR AP
                ON AP.AMPHUR_SEQ = NVL(RECV.AMPHUR,OK.AMPHUR)
            LEFT JOIN MAS.TB_MAS_TAMBOL TB
                ON TB.TAMBOL_SEQ = NVL(RECV.TAMBOL,OK.TAMBOL) ";

    $sql .= "ORDER BY NVL(RECV.UTMMAP1,OK.UTMMAP1),NVL(RECV.UTMMAP2,OK.UTMMAP2),NVL(RECV.UTMMAP3,OK.UTMMAP3),NVL(RECV.UTMSCALE,OK.UTMSCALE),NVL(RECV.UTMMAP4,OK.UTMMAP4)
            , REGEXP_SUBSTR(NVL(RECV.LAND_NO,OK.LAND_NO), '^\D*') NULLS FIRST, TO_NUMBER(REGEXP_SUBSTR(NVL(RECV.LAND_NO,OK.LAND_NO), '\d+'))
            , NVL(RECV.MAP_PARCEL_SEQ,OK.MAP_PARCEL_SEQ)
            , REGEXP_SUBSTR(NVL(RECV.NO,OK.NO), '^\D*') NULLS FIRST, TO_NUMBER(REGEXP_SUBSTR(NVL(RECV.NO,OK.NO), '\d+'))";
    // echo "\n".$sql;
    $stid = oci_parse($conn, $sql);
    if($requestData["utmScale"]!="") oci_bind_by_name($stid, ':utmScale', $requestData['utmScale']);
    if($requestData["utmmap1"]!="") oci_bind_by_name($stid, ':utmmap1', $requestData['utmmap1']);
    if($requestData["utmmap2"]!="") oci_bind_by_name($stid, ':utmmap2', $requestData['utmmap2']);
    if($requestData["utmmap3"]!="") oci_bind_by_name($stid, ':utmmap3', $requestData['utmmap3']);
    if($requestData["utmmap4"]!="") oci_bind_by_name($stid, ':utmmap4', $requestData['utmmap4']);
    if($requestData["landno"]!="") oci_bind_by_name($stid, ':landno', $requestData['landno']);
    oci_bind_by_name($stid, ':landoffice', $requestData['landoffice']);
    // oci_bind_by_name($stid, ':parcelType', $requestData['parcelType']);
    oci_bind_by_name($stid, ':printplateType', $requestData['printplateType']);

    oci_execute($stid);

    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;   
    }

    $jsonData = array(
        "data" => $Result
    );

    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);

?>