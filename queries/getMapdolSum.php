<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice = $_REQUEST['landoffice'];

    $sql = "with recvAll as (
            select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE , count(*) as count
              from mgt1.map_land_gis_47
              where landoffice_seq = :landoffice and NVL(RECORD_STATUS,'N') = 'N' and nvl(parcel_type,0) <> 3 AND LOG1_VERSION_STS IS NULL
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mgt1.map_land_gis_48
              where landoffice_seq = :landoffice and NVL(RECORD_STATUS,'N') = 'N' and nvl(parcel_type,0) <> 3 AND LOG1_VERSION_STS IS NULL
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mgt1.map_land_ns3k_47
              where landoffice_seq = :landoffice and NVL(RECORD_STATUS,'N') = 'N' and nvl(parcel_type,0) <> 1 AND LOG1_VERSION_STS IS NULL
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mgt1.map_land_ns3k_48
              where landoffice_seq = :landoffice and NVL(RECORD_STATUS,'N') = 'N' and nvl(parcel_type,0) <> 1 AND LOG1_VERSION_STS IS NULL
              group by landoffice_seq, NVL(parcel_type,99)
            ), recvAll_temp as (
            select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mgt1.map_land_temp_47
              where landoffice_seq = :landoffice and NVL(RECORD_STATUS,'N') = 'N' and nvl(parcel_type,0) <> 3 AND LOG1_VERSION_STS IS NULL
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mgt1.map_land_temp_48
              where landoffice_seq = :landoffice and NVL(RECORD_STATUS,'N') = 'N' and nvl(parcel_type,0) <> 3 AND LOG1_VERSION_STS IS NULL
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mgt1.map_land_ns3k_temp_47
              where landoffice_seq = :landoffice and NVL(RECORD_STATUS,'N') = 'N' and nvl(parcel_type,0) <> 1 AND LOG1_VERSION_STS IS NULL
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mgt1.map_land_ns3k_temp_48
              where landoffice_seq = :landoffice and NVL(RECORD_STATUS,'N') = 'N' and nvl(parcel_type,0) <> 1 AND LOG1_VERSION_STS IS NULL
              group by landoffice_seq, NVL(parcel_type,99)
            ), OK as (
            select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE , count(*) as count
              from mapdol.map_land_gis_47
              where landoffice_seq = :landoffice and nvl(parcel_type,0) <> 3
                and create_dtm < to_date('08/03/2021','DD/MM/YYYY')
                and (NVL(RECORD_STATUS,'N') = 'N' or (record_status in ('D', 'C') and last_upd_dtm > to_date('07/03/2021','DD/MM/YYYY')))
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mapdol.map_land_gis_48
              where landoffice_seq = :landoffice and nvl(parcel_type,0) <> 3
                and create_dtm < to_date('08/03/2021','DD/MM/YYYY')
                and (NVL(RECORD_STATUS,'N') = 'N' or (record_status in ('D', 'C') and last_upd_dtm > to_date('07/03/2021','DD/MM/YYYY')))
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mapdol.map_land_ns3k_47
              where landoffice_seq = :landoffice and nvl(parcel_type,0) <> 1
                and create_dtm < to_date('08/03/2021','DD/MM/YYYY')
                and (NVL(RECORD_STATUS,'N') = 'N' or (record_status in ('D', 'C') and last_upd_dtm > to_date('07/03/2021','DD/MM/YYYY')))
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mapdol.map_land_ns3k_48
              where landoffice_seq = :landoffice and nvl(parcel_type,0) <> 1
                and create_dtm < to_date('08/03/2021','DD/MM/YYYY')
                and (NVL(RECORD_STATUS,'N') = 'N' or (record_status in ('D', 'C') and last_upd_dtm > to_date('07/03/2021','DD/MM/YYYY')))
              group by landoffice_seq, NVL(parcel_type,99)
            ), OK_temp as (
            select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE , count(*) as count
              from mapdol.map_land_temp_47
              where landoffice_seq = :landoffice and nvl(parcel_type,0) <> 3
                and create_dtm < to_date('08/03/2021','DD/MM/YYYY')
                and (NVL(RECORD_STATUS,'N') = 'N' or (record_status in ('D', 'C') and last_upd_dtm > to_date('07/03/2021','DD/MM/YYYY')))
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mapdol.map_land_temp_48
              where landoffice_seq = :landoffice and nvl(parcel_type,0) <> 3
                and create_dtm < to_date('08/03/2021','DD/MM/YYYY')
                and (NVL(RECORD_STATUS,'N') = 'N' or (record_status in ('D', 'C') and last_upd_dtm > to_date('07/03/2021','DD/MM/YYYY')))
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mapdol.map_land_ns3k_temp_47
              where landoffice_seq = :landoffice and nvl(parcel_type,0) <> 1
                and create_dtm < to_date('08/03/2021','DD/MM/YYYY')
                and (NVL(RECORD_STATUS,'N') = 'N' or (record_status in ('D', 'C') and last_upd_dtm > to_date('07/03/2021','DD/MM/YYYY')))
              group by landoffice_seq, NVL(parcel_type,99)
            union
              select landoffice_seq, NVL(parcel_type,99) AS PARCEL_TYPE, count(*) as count
              from mapdol.map_land_ns3k_temp_48
              where landoffice_seq = :landoffice and nvl(parcel_type,0) <> 1
                and create_dtm < to_date('08/03/2021','DD/MM/YYYY')
                and (NVL(RECORD_STATUS,'N') = 'N' or (record_status in ('D', 'C') and last_upd_dtm > to_date('07/03/2021','DD/MM/YYYY')))
              group by landoffice_seq, NVL(parcel_type,99)
            ), p1 as
              (select parcel_type, sum(count) as recvAll from recvAll
              group by parcel_type),
            p2 as (select parcel_type, sum(count) as OK from OK
              group by parcel_type),
            p1_temp as
              (select parcel_type, sum(count) as recvAll from recvAll_temp
              group by parcel_type),
            p2_temp as (select parcel_type, sum(count) as OK from OK_temp
              group by parcel_type),
            pt as
                (select parceltype_seq as parcel_type, parceltype_desc from svo.tb_udm_mas_parceltype
                )
            select pt.parceltype_desc,
            nvl(p1.recvAll,0) as RECEIVE, nvl(p2.OK,0) as MIGRATE_SUCCESS, (nvl(p1.recvAll,0)-nvl(p2.OK,0)) as MIGRATE_ERROR,
            nvl(p1_temp.recvAll,0) as RECEIVE_Temp, nvl(p2_temp.OK,0) as MIGRATE_SUCCESS_Temp, (nvl(p1_temp.recvAll,0) - nvl(p2_temp.OK,0)) as MIGRATE_ERROR_Temp
            from pt
            left join p1_temp
                on p1_temp.parcel_type = pt.parcel_type
            left join p2
              on pt.parcel_type = p2.parcel_type
            left join p1
              on pt.parcel_type = p1.parcel_type
            left join p2_temp
              on pt.parcel_type = p2_temp.parcel_type
            order by (case pt.parcel_type
                    when 1 then 1 when 4 then 2 when 5 then 3 when 2 then 4
                    when 3 then 5 when 7 then 6 when 19 then 7 when 6 then 8
                    when 8 then 9 when 9 then 10 when 10 then 11 when 11 then 12
                    when 12 then 13 when 13 then 14 when 14 then 15 when 15 then 16
                    when 16 then 17 when 17 then 18 when 18 then 19 when 99 then 99 end)
  ";

    // echo $sql."\n";
    $stid = oci_parse($conn, $sql);
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    $jsonData = array(
        "data" => $Result
    );
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
