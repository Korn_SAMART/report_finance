<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';

    $select = "WITH RECALL AS(
                SELECT 1 AS NUM, 'ระบบจัดการสิทธิการใช้งาน(ADM)' AS TYPE, NULL AS RECEIVE
                FROM DUAL
                UNION
                SELECT 1.1 AS NUM, 'ข้อมูลสิทธิ' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ADM_MAS_PERMISSION
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 1.2 AS NUM, 'ข้อมูลระบบงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ADM_MAS_SYSTEM
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 1.3 AS NUM, 'ข้อมูลหน้าจอ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ADM_MAS_SCREEN
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 1.4 AS NUM, 'ข้อมูลกลุ่มผู้ใช้งาน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ADM_MAS_USER_GROUP
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 1.5 AS NUM, 'ข้อมูลกลุ่มเมนู' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ADM_MENU_GROUP
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 1.6 AS NUM, 'ข้อมูลกลุ่มตำแหน่ง' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ADM_MAS_POSITION_GROUP
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 2 AS NUM, 'ระบบปรับปรุงข้อมูล(APS)' AS TYPE, NULL AS RECEIVE
                FROM DUAL
                UNION
                SELECT 2.1 AS NUM, 'ข้อมูลห้องชุด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_APS_MAS_ROOM_USAGE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2.2 AS NUM, 'ข้อมูลพื้นที่' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_APS_MAS_POSSESSORY
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2.3 AS NUM, 'ข้อมูลชั้น' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_APS_MAS_CONDO_FLOOR
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 3 AS NUM, 'ระบบงานอำนวยการในสำนักงานที่ดิน(งานสารบรรณ)(CTN)' AS TYPE, NULL AS RECEIVE
                FROM DUAL
                UNION
                SELECT 3.1 AS NUM, 'ข้อมูลประเภทหนังสือ' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_CTN_MAS_DOC_CLASS
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3.2 AS NUM, 'ข้อมูลชั้นความลับ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_CTN_MAS_DOC_SECRET
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3.3 AS NUM, 'ข้อมูลหมวดหมู่เอกสาร' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_CTN_MAS_DOC_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3.4 AS NUM, 'ข้อมูลเลขหนังสือของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_CTN_MAS_LANDOFFICE_LETTER
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 4 AS NUM, 'ระบบงานกลุ่มงานวิชาการที่ดินในสำนักงานที่ดิน(EXP)' AS TYPE, NULL AS RECEIVE
                FROM DUAL
                UNION
                SELECT 4.1 AS NUM, 'ข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_EXP_MAS_ILLEGALITY_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.2 AS NUM, 'ข้อมูลประเภทงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_JOB_GROUP
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.3 AS NUM, 'ข้อมูลขั้นตอนการดำเนินการ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_PROC_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.4 AS NUM, 'ข้อมูลประเภทการร้องเรียน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_REQUEST_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.5 AS NUM, 'ข้อมูลเรื่อง' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_SUB_JOB
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.6 AS NUM, 'ข้อมูลประเภทการขอใช้ประโยชน์' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_USE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.7 AS NUM, 'ข้อมูลสถานะงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_WORK_STS
                WHERE RECORD_STATUS = 'N'
                ),
            OK AS(
                SELECT 1 AS NUM, 'ระบบจัดการสิทธิการใช้งาน(ADM)' AS TYPE, NULL AS RECEIVE
                FROM DUAL
                UNION
                SELECT 1.1 AS NUM, 'ข้อมูลสิทธิ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ADM_MAS_PERMISSION
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 1.2 AS NUM, 'ข้อมูลระบบงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ADM_MAS_SYSTEM
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 1.3 AS NUM, 'ข้อมูลหน้าจอ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ADM_MAS_SCREEN
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 1.4 AS NUM, 'ข้อมูลกลุ่มผู้ใช้งาน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ADM_MAS_USER_GROUP
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 1.5 AS NUM, 'ข้อมูลกลุ่มเมนู' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ADM_MENU_GROUP
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 1.6 AS NUM, 'ข้อมูลกลุ่มตำแหน่ง' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ADM_MAS_POSITION_GROUP
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 2 AS NUM, 'ระบบปรับปรุงข้อมูล(APS)' AS TYPE, NULL AS RECEIVE
                FROM DUAL
                UNION
                SELECT 2.1 AS NUM, 'ข้อมูลห้องชุด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_APS_MAS_ROOM_USAGE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2.2 AS NUM, 'ข้อมูลพื้นที่' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_APS_MAS_POSSESSORY
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2.3 AS NUM, 'ข้อมูลชั้น' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_APS_MAS_CONDO_FLOOR
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 3 AS NUM, 'ระบบงานอำนวยการในสำนักงานที่ดิน(งานสารบรรณ)(CTN)' AS TYPE, NULL AS RECEIVE
                FROM DUAL
                UNION
                SELECT 3.1 AS NUM, 'ข้อมูลประเภทหนังสือ' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_CTN_MAS_DOC_CLASS
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3.2 AS NUM, 'ข้อมูลชั้นความลับ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_CTN_MAS_DOC_SECRET
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3.3 AS NUM, 'ข้อมูลหมวดหมู่เอกสาร' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_CTN_MAS_DOC_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3.4 AS NUM, 'ข้อมูลเลขหนังสือของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_CTN_MAS_LANDOFFICE_LETTER
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 4 AS NUM, 'ระบบงานกลุ่มงานวิชาการที่ดินในสำนักงานที่ดิน(EXP)' AS TYPE, NULL AS RECEIVE
                FROM DUAL
                UNION
                SELECT 4.1 AS NUM, 'ข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_EXP_MAS_ILLEGALITY_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.2 AS NUM, 'ข้อมูลประเภทงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_JOB_GROUP
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.3 AS NUM, 'ข้อมูลขั้นตอนการดำเนินการ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_PROC_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.4 AS NUM, 'ข้อมูลประเภทการร้องเรียน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_REQUEST_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.5 AS NUM, 'ข้อมูลเรื่อง' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_SUB_JOB
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.6 AS NUM, 'ข้อมูลประเภทการขอใช้ประโยชน์' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_USE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4.7 AS NUM, 'ข้อมูลสถานะงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_WORK_STS
                WHERE RECORD_STATUS = 'N'
                )
            SELECT NVL(RECALL.NUM,OK.NUM) AS NUM, NVL(RECALL.TYPE,OK.TYPE) AS TYPE ,
            RECALL.RECEIVE AS RECEIVE, 
            OK.RECEIVE AS MIGRATE_SUCCESS,
            RECALL.RECEIVE-OK.RECEIVE AS MIGRATE_ERROR 
            FROM RECALL
            INNER JOIN OK
                ON RECALL.TYPE = OK.TYPE
            ORDER BY NVL(RECALL.NUM,OK.NUM)";
     
    $stid = oci_parse($conn, $select); 
    oci_execute($stid);
    
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }

    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);

?>