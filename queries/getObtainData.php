<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';

    $landoffice = $_POST['landoffice'];
    $processSeqP1 = !isset($_POST['processSeqP1'])? '' : $_POST['processSeqP1'];
    $processSeqP2 = !isset($_POST['processSeqP2'])? '' : $_POST['processSeqP2'];
    $Result = array();


    if($processSeqP1 != null){
        $table = 'Obtain1';
        include 'queryObtainData.php';
        while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
            $Result[] = $row;
        }
    }
    
    if($processSeqP2 != null){
        $table = 'Obtain2';
        include 'queryObtainData.php';
        while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
            $Result[] = $row;
        }
    }

    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);


?>