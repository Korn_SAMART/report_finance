<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';

    $parcelSeqP1 = !isset($_POST['parcelSeqP1'])? '' : $_POST['parcelSeqP1'];
    $parcelSeqP2 = !isset($_POST['parcelSeqP2'])? '' : $_POST['parcelSeqP2'];
    $check = $_POST['check'];
    
    $Result = array();


    if($parcelSeqP1 != null){
        $table = 'P1';
        include 'queryProcessData.php';
        while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
            $Result[] = $row;
        }
    }
    if($parcelSeqP2 != null){
        $table = 'P2';
        include 'queryProcessData.php';
        while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
            $Result[] = $row;
        }
    }
        

    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);


?>