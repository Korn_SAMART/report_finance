<?php
    session_start();
    /*if(!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')){
        include '403.php';
        exit(0);
    }*/

    header('Content-Type: application/json; charset=utf-8');

    include '../database/conn.php';

    $selectProvince = " SELECT DISTINCT PV.PROVINCE_SEQ, PV.PROVINCE_NAME
                        FROM MAS.TB_MAS_PROVINCE PV
                        INNER JOIN MAS.TB_MAS_LANDOFFICE L
                            ON L.PROVINCE_SEQ = PV.PROVINCE_SEQ
                            AND L.LANDOFFICE_SEQ IN (
                                SELECT LANDOFFICE_SEQ FROM DATAM.TB_MAS_LANDOFFICE
                                WHERE TYPE IS NULL
                                )
                        ORDER BY  NLSSORT(PV.PROVINCE_NAME, 'NLS_SORT=THAI_DICTIONARY')";
    $stid = oci_parse($conn, $selectProvince);
    oci_execute($stid);
    $Result = array();
    $branch = '';
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
