<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $amphur = strpos($_REQUEST['amphur'],',')? explode(',',$_REQUEST['amphur']) : $_REQUEST['amphur'];
    $condition = '';
    if(strpos($_REQUEST['amphur'],',')){
        for($i=0; $i<count($amphur); $i++){
            $condition .= $i==0? 'IN (:amphur'.$i.'' : ', :amphur'.$i.'';
        }
        $condition .= ') ';
    } else {
        $condition .= '= :amphur ';
    }
    // echo $condition;

    $sql = "SELECT TAMBOL_SEQ, TAMBOL_NAME ";
    $sql .= "FROM MAS.TB_MAS_TAMBOL ";
    $sql .= "WHERE RECORD_STATUS = 'N' AND AMPHUR_SEQ ";
    $sql .= $condition;
    $sql .= "ORDER BY NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY')";

    // echo $sql."\n";
    $stid = oci_parse($conn, $sql);
    if(strpos($_REQUEST['amphur'],',')){
        for($i=0; $i<count($amphur); $i++){
            oci_bind_by_name($stid, ':amphur'.$i, $amphur[$i]);
        }
    } else {
        oci_bind_by_name($stid, ':amphur', $amphur);
    }
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
