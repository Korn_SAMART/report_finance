<?php
    header('Content-Type: application/json; charset=utf-8');
    // require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';
    
    $landoffice = $_POST['landoffice'];
    $amphur = $_POST['amphur'];

    $selectTambon = "
    SELECT DISTINCT
        tb.tambol_seq, tb.tambol_name
    FROM
        MAS.tb_mas_tambol tb
    WHERE 
        EXISTS (
            SELECT
                amphur_seq
            FROM
                mas.tb_mas_amphur ap
            WHERE
                tb.amphur_seq = ap.amphur_seq
                AND amphur_seq = :amphur
    
        ) 
        ORDER BY NLSSORT(tb.tambol_name, 'NLS_SORT=THAI_DICTIONARY') ";

    $stid = oci_parse($conn, $selectTambon);
    oci_bind_by_name($stid, ':amphur', $amphur);
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
