<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'queryMASData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}


$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->mergeCells('A1:F1');
$sheet->setCellValue('A1', 'ข้อมูลการใช้ประโยชน์ในที่ดิน ');

$bn = 'ข้อมูลการใช้ประโยชน์ในที่ดิน';
$fileName = 'mas-7 ข้อมูลการใช้ประโยชน์ในที่ดิน';

$sheet->mergeCells('A2:C2');
$sheet->setCellValue('A2', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
$sheet->mergeCells('D2:F2');
$sheet->setCellValue('D2', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

$sheet->setCellValue('A3', 'ลำดับ')
    ->setCellValue('B3', 'SEQ')
    ->setCellValue('C3', 'ชื่อการใช้ประโยชน์ที่ดิน')
    ->setCellValue('D3', 'SEQ')
    ->setCellValue('E3', 'ชื่อการใช้ประโยชน์ที่ดิน')
    ->setCellValue('F3', 'หมายเหตุ')
    ->getStyle('A1:F3')
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);



$last = count($Result);
$current = 0;
for ($i = 0; $i < $last; $i++) {
    if($sts = 'e'){
        if($Result[$i]['LANDUSED_SEQ_P1'] != $Result[$i]['LANDUSED_SEQ_P2'] || Result[$i]['LANDUSED_NAME_P1'] != Result[$i]['LANDUSED_NAME_P2']){
            $sheet->setCellValue('A' . (4 + $current), ($current + 1))
            if (empty($Result[$i]['LANDUSED_SEQ_P1'])) {
                $sheet->setCellValue('B' . (4 + $current), NULL);
                $sheet->setCellValue('F' . (4 + $current), 'LANDUSED_SEQ_P1 NULL');
            } else {
                $sheet->setCellValue('B' . (4 + $current), $Result[$i]['LANDUSED_SEQ_P1']);
            }
            if (empty($Result[$i]['LANDUSED_NAME_P1'])) {
                $sheet->setCellValue('C' . (4 + $current), NULL);
                $sheet->setCellValue('F' . (4 + $current), 'LANDUSED_NAME_P1 NULL');
            } else {
                $sheet->setCellValue('C' . (4 + $current), $Result[$i]['LANDUSED_NAME_P1 NULL']);
            }
            if (empty($Result[$i]['LANDUSED_SEQ_P2'])) {
                $sheet->setCellValue('D' . (4 + $current), NULL);
                $sheet->setCellValue('F' . (4 + $current), 'LANDUSED_SEQ_P2 NULL');
            } else {
                $sheet->setCellValue('D' . (4 + $current), $Result[$i]['LANDUSED_SEQ_P1 NULL']);
            }
            if (empty($Result[$i]['LANDUSED_NAME_P2'])) {
                $sheet->setCellValue('E' . (4 + $current), NULL);
                $sheet->setCellValue('F' . (4 + $current), 'LANDUSED_NAME_P2 NULL');
            } else {
                $sheet->setCellValue('E' . (4 + $current), $Result[$i]['LANDUSED_NAME_P2 NULL']);
            }
            $current += 1;
        }
        else if($current > 0) $current -= 1;
    }
}



$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];

$sheet->getStyle('A1:F4'.(count($Result)+3))->applyFromArray($styleArray);
$sheet->getStyle('A1:F3')->getFont()->setBold(true);
$sheet->getColumnDimension('A')->setAutoSize(true);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setAutoSize(true);
$sheet->getColumnDimension('F')->setAutoSize(true);

$sheet->getStyle('A4:A' . (count($Result)+3))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('B4:B' . (count($Result)+3))
    ->getNumberFormat()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('C4:B' . (count($Result)+3))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('D4:B' . (count($Result)+3))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('E4:B' . (count($Result)+3))
    ->getAlignment()
    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$sheet->getStyle('F4:E' . (count($Result)+3))
    ->getAlignment()
    ->setWrapText(true);

$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
