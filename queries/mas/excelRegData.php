<?php
require '../../plugins/vendor/autoload.php';
include '../database/conn.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$Result = array();

$val = explode('=', $argv[1])[1];


// $landoffice = !isset($_GET['landoffice']) ? '' : $_GET['landoffice'];
// $branchName = !isset($_GET['branchName']) ? '' : $_GET['branchName'];
$amphur = !isset($_GET['amphur'])? '' : $_GET['amphur'];
$tambon = !isset($_GET['tambon'])? '' : $_GET['tambon'];
$parcelNo = !isset($_GET['parcelNo'])? '' : $_GET['parcelNo']; //รับตัวเดียว
// $check = !isset($_GET['check']) ? '' : $_GET['check'];
// $sts = !isset($_GET['sts']) ? '' : $_GET['sts'];


$check = explode('=', $argv[1])[1];
$sts = explode('=', $argv[2])[1];
$landoffice = explode('=', $argv[3])[1];
$branchName = explode('=', $argv[4])[1];

include 'queryAllData.php';
while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
    $Result[] = $row;
}


$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$spreadsheet->getActiveSheet()->setTitle('ข้อมูลเอกสารสิทธิ');
$spreadsheet->createSheet(1)->setTitle('ภาระผูกพัน');
$spreadsheet->createSheet(2)->setTitle('ผู้ถือกรรมสิทธิ์');
$spreadsheet->createSheet(3)->setTitle('รายการจดทะเบียน');
$spreadsheet->createSheet(4)->setTitle('ข้อมูลอายัด');

switch ($check) {
    case '1':
        $last =  20000;//count($Result);
        $current = 0;
        $problemDesc = "";
        for ($j = 0; $j < 2; $j++) {
            $current = 0;
            $sheet = $spreadsheet->setActiveSheetIndex($j);
            for ($i = 0; $i < $last; $i++) {

                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));

                //  if ($sts == 'e') {
                if ($Result[$i]['PARCEL_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($i + 1));
                    if (empty($Result[$i]['PARCEL_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PARCEL_NO']);
                    }
                    if (empty($Result[$i]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$i]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$i]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TAMBOL_NAME']);
                    }
                    $current += 2;
                }
                // }
            }
        }
        echo 'alldata done <br>';

        $sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

        $current = 0;


        for ($i = 0; $i < $last; $i++) {
            $sheet = $spreadsheet->setActiveSheetIndex(0);
            $ResultDetail = array();
            $ResultOlgt = array();

            $dataSeqP1 = $Result[$i]['PARCEL_SEQ_P1'];
            $dataSeqP2 = $Result[$i]['PARCEL_SEQ_P2'];

            include 'queryDetailData.php';
            // while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);
            // }
            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }

            if (empty($ResultDetail[0]['PARCEL_NO_P1'])) {
                $sheet->setCellValue('E' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (5 + $current), $ResultDetail[0]['PARCEL_NO_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('F' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (5 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AREA_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultDetail[0]['AREA_P1']);
            }
            if (empty($ResultDetail[0]['UTM_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultDetail[0]['UTM_P1']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultDetail[0]['ORIGIN_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_OWNER_NUM_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultDetail[0]['PARCEL_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_OPT_FLAG_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultDetail[0]['PARCEL_OPT_FLAG_P1'] == 0 ? 'นอกเขตเทศบาล' : 'ในเขตเทศบาล');
            }
            if (empty($ResultDetail[0]['PARCEL_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('N' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (5 + $current), $ResultDetail[0]['PARCEL_SEQUEST_STS_P1'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if (empty($ResultDetail[0]['PARCELUSED_DESC_P1'])) {
                $sheet->setCellValue('O' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (5 + $current), $ResultDetail[0]['PARCEL_LANDUSED_DESC_P1']);
            }


            if (empty($ResultDetail[0]['PARCEL_NO_P2'])) {
                $sheet->setCellValue('E' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (6 + $current), $ResultDetail[0]['PARCEL_NO_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AREA_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultDetail[0]['AREA_P2']);
            }
            if (empty($ResultDetail[0]['UTM_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultDetail[0]['UTM_P2']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultDetail[0]['ORIGIN_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_OWNER_NUM_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultDetail[0]['PARCEL_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_OPT_FLAG_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultDetail[0]['PARCEL_OPT_FLAG_P2'] == 0 ? 'นอกเขตเทศบาล' : 'ในเขตเทศบาล');
            }
            if (empty($ResultDetail[0]['PARCEL_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultDetail[0]['PARCEL_SEQUEST_STS_P2'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if (empty($ResultDetail[0]['PARCELUSED_DESC_P2'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $ResultDetail[0]['PARCEL_LANDUSED_DESC_P2']);
            }



            $sheet = $spreadsheet->setActiveSheetIndex(1);
            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('E' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('F' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('K' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P1'])) {
                    $sheet->setCellValue('L' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('E' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('K' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('K' . (6 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('L' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (6 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            $current += 2;
        }
        echo 'olgt done <br>';

        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $sheet->mergeCells('A1:P1');
        $sheet->mergeCells('A2:P2');
        $sheet->mergeCells('A3:P3');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนไม่สำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนไม่สำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนไม่สำเร็จ - โฉนดที่ดิน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('E4', 'เลขที่เอกสารสิทธิ')
            ->setCellValue('F4', 'ตำบล/แขวง')
            ->setCellValue('G4', 'อำเภอ/เขต')
            ->setCellValue('H4', 'ขนาดพื้นที่')
            ->setCellValue('I4', 'ระวาง UTM')
            ->setCellValue('J4', 'ระวางศูนย์กำเนิด')
            ->setCellValue('K4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('L4', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'สถานะ อปท.')
            ->setCellValue('N4', 'สถานะการอายัด')
            ->setCellValue('O4', 'การใช้ประโยชน์')
            ->setCellValue('P4', 'หมายเหตุ')
            ->getStyle('A1:P4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->mergeCells('A1:M1');
        $sheet->mergeCells('A2:M2');
        $sheet->mergeCells('A3:M3');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนไม่สำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนไม่สำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนไม่สำเร็จ - โฉนดที่ดิน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->setCellValue('E4', 'ชื่อหนังสือสัญญา');
        $sheet->setCellValue('F4', 'วันที่หนังสือสัญญา')
            ->setCellValue('G4', 'วันที่เริ่มสัญญา')
            ->setCellValue('H4', 'ราคาทุนทรัพย์')
            ->setCellValue('I4', 'ระยะเวลา')
            ->setCellValue('J4', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('K4', 'ผู้ให้สัญญา')
            ->setCellValue('L4', 'ผู้รับสัญญา')
            ->setCellValue('M4', 'หมายเหตุ')
            ->getStyle('A1:M4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);



        for ($j = 0; $j < 2; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);
            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
                'font' => array(
                    'bold'  => false,
                    'color' => array('rgb' => '000000'),
                    'size'  => 16,
                    'name'  => 'TH SarabunPSK'
                )
            ];
            if ($j == 0) {
                $sheet->getStyle('A1:P' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:P4')->getFont()->setBold(true);
            } else {
                $sheet->getStyle('A1:M' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:M4')->getFont()->setBold(true);
            }


            $sheet->getStyle('A4:A' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('B4:B' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('C4:C' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('D4:D' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('E4:E' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('F4:F' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('G4:G' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('H4:H' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('I4:I' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('J4:J' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('K4:K' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('L4:L' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('M4:M' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('N4:N' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('O4:O' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('O4:P' . ($last * 2 + 4))
                ->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            foreach (range('A', 'P') as $columnID) {
                $sheet->getColumnDimension($columnID)->setAutoSize(true);
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        // if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
        //     $fileName = urlencode($fileName);
        // }
        $fileName = urlencode($fileName);
        header('Content-Disposition: attachment; filename="' . 'a' . '.xlsx"');
        $writer->save('C:\Users\datam\Desktop\a.xlsx');
        echo 'done';


        break;
    case '2':
    case '3':
    case '4': //ns3a
        $last = count($Result);
        $current = 0;
        $problemDesc = "";

        for ($i = 0; $i < $last; $i++) {
            $sheet->mergeCells('A' . (3 + $current) . ':A' . (4 + $current));
            $sheet->mergeCells('B' . (3 + $current) . ':B' . (4 + $current));
            $sheet->mergeCells('C' . (3 + $current) . ':C' . (4 + $current));
            $sheet->mergeCells('D' . (3 + $current) . ':D' . (4 + $current));

            if ($Result[$i]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (3 + $current), ($i + 1));

                if (empty($Result[$i]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (3 + $current), $Result[$i]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$i]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('C' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (3 + $current), $Result[$i]['AMPHUR_NAME']);
                }

                if (empty($Result[$i]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('D' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (3 + $current), $Result[$i]['TAMBOL_NAME']);
                }
                $current += 2;
            }
        }
        $sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

        $current = 0;


        for ($i = 0; $i < $last; $i++) {
            $ResultDetail = array();
            $ResultOlgt = array();

            $dataSeqP1 = $Result[$i]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$i]['PARCEL_LAND_SEQ_P2'];

            include 'queryDetailData.php';
            // while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);
            // }
            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }

            if (empty($ResultDetail[0]['PARCEL_LAND_NO_P1'])) {
                $sheet->setCellValue('E' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_NO_P1']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('F' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (3 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('G' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (3 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('H' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (3 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AREA_P1'])) {
                $sheet->setCellValue('I' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (3 + $current), $ResultDetail[0]['AREA_P1']);
            }
            if (empty($ResultDetail[0]['UTM_P1'])) {
                $sheet->setCellValue('J' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (3 + $current), $ResultDetail[0]['UTM_P1']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P1'])) {
                $sheet->setCellValue('K' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (3 + $current), $ResultDetail[0]['ORIGIN_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('L' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (3 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'])) {
                $sheet->setCellValue('M' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'])) {
                $sheet->setCellValue('N' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('O' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'])) {
                $sheet->setCellValue('P' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1']);
            }


            if (empty($ResultDetail[0]['PARCEL_LAND_NO_P2'])) {
                $sheet->setCellValue('E' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('E' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_NO_P2']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('F' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (4 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('G' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (4 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('H' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (4 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AREA_P2'])) {
                $sheet->setCellValue('I' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (4 + $current), $ResultDetail[0]['AREA_P2']);
            }
            if (empty($ResultDetail[0]['UTM_P2'])) {
                $sheet->setCellValue('J' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (4 + $current), $ResultDetail[0]['UTM_P2']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P2'])) {
                $sheet->setCellValue('K' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (4 + $current), $ResultDetail[0]['ORIGIN_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('L' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (4 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2'])) {
                $sheet->setCellValue('M' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2'])) {
                $sheet->setCellValue('N' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('O' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2'])) {
                $sheet->setCellValue('P' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']);
            }

            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('Q' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('R' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('S' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('T' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (3 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('U' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (3 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('V' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (3 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('W' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('W' . (3 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[0 + 1]['OWN_P1'])) {
                    $sheet->setCellValue('X' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('X' . (3 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('Q' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('R' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('S' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('T' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (4 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('U' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (4 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('V' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (4 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('W' . (4 + $current), NULL);
                } else {
                    $sheet->setCellValue('W' . (4 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[1]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[1]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('X' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('X' . (3 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            $current += 2;
        }




        $sheet->mergeCells('A1:D1');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'ข้อมูลการใช้ประโยชน์ในที่ดิน ERROR');
            $bn = 'ข้อมูลการใช้ประโยชน์ในที่ดิน error';
            $fileName = 'mas-7 ข้อมูลการใช้ประโยชน์ในที่ดิน error';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ');
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
            $fileName = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
        }
        $sheet->setCellValue('A2', '')
            ->setCellValue('B2', 'เลขเอกสารสิทธิ')
            ->setCellValue('C2', 'อำเภอ')
            ->setCellValue('D2', 'ตำบล')
            ->getStyle('A1:D2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->setCellValue('E1', 'ข้อมูลเอกสารสิทธิ');
        $sheet->mergeCells('E1:P1');
        $sheet->setCellValue('E2', 'เลขที่เอกสารสิทธิ');
        $sheet->setCellValue('F2', 'ชื่อประเภทเอกสารสิทธิ')
            ->setCellValue('G2', 'ตำบล/แขวง')
            ->setCellValue('H2', 'อำเภอ/เขต')
            ->setCellValue('I2', 'ขนาดพื้นที่')
            ->setCellValue('J2', 'ระวาง UTM')
            ->setCellValue('K2', 'ระวางศูนย์กำเนิด')
            ->setCellValue('L2', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('M2', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('N2', 'สถานะ อปท.')
            ->setCellValue('O2', 'สถานะการอายัด')
            ->setCellValue('P2', 'การใช้ประโยชน์')
            ->getStyle('A1:P2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('Q1', 'ภาระผูกพัน');
        $sheet->mergeCells('Q1:X1');
        $sheet->setCellValue('Q2', 'ชื่อหนังสือสัญญา');
        $sheet->setCellValue('R2', 'วันที่หนังสือสัญญา')
            ->setCellValue('S2', 'วันที่เริ่มสัญญา')
            ->setCellValue('T2', 'ราคาทุนทรัพย์')
            ->setCellValue('U2', 'ระยะเวลา')
            ->setCellValue('V2', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('W2', 'ผู้ให้สัญญา')
            ->setCellValue('X2', 'ผู้รับสัญญา')
            ->getStyle('A1:X2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];

        $sheet->getStyle('A1:X' . ($last * 2 + 2))->applyFromArray($styleArray);
        $sheet->getStyle('A1:X2')->getFont()->setBold(true);


        $sheet->getStyle('A3:A' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B3:B' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C3:C' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D3:D' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E3:E' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F3:F' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G3:G' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H3:H' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I3:I' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J3:J' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K3:K' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L3:L' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('M3:M' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('N3:N' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('O3:O' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('P3:P' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Q3:Q' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('R3:R' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('S3:S' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('T3:T' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('U3:U' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('V3:V' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('W3:W' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('X3:X' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        foreach (range('A', 'X') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
            $fileName = urlencode($fileName);
        }
        header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
        $writer->save('php://output');



        break;
        // ======================== NS3 / SNSL ========================
    case '5':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";

        for ($i = 0; $i < $last; $i++) {
            $sheet->mergeCells('A' . (3 + $current) . ':A' . (4 + $current));
            $sheet->mergeCells('B' . (3 + $current) . ':B' . (4 + $current));
            $sheet->mergeCells('C' . (3 + $current) . ':C' . (4 + $current));
            $sheet->mergeCells('D' . (3 + $current) . ':D' . (4 + $current));
            $sheet->mergeCells('E' . (3 + $current) . ':E' . (4 + $current));

            if ($Result[$i]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (3 + $current), ($i + 1));

                if (empty($Result[$i]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (3 + $current), $Result[$i]['PARCEL_LAND_NO']);
                }

                if (empty($Result[$i]['MOO'])) {
                    $sheet->setCellValue('C' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (3 + $current), $Result[$i]['MOO']);
                }

                if (empty($Result[$i]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('D' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (3 + $current), $Result[$i]['AMPHUR_NAME']);
                }

                if (empty($Result[$i]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('E' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (3 + $current), $Result[$i]['TAMBOL_NAME']);
                }
                $current += 2;
            }
        }
        $sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

        $current = 0;


        for ($i = 0; $i < $last; $i++) {
            $ResultDetail = array();
            $ResultOlgt = array();

            $dataSeqP1 = $Result[$i]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$i]['PARCEL_LAND_SEQ_P2'];

            include 'queryDetailData.php';
            // while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);
            // }
            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }

            if (empty($ResultDetail[0]['PARCEL_LAND_NO_P1'])) {
                $sheet->setCellValue('F' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_NO_P1']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('G' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (3 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('H' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (3 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('I' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (3 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_MOO_P1'])) {
                $sheet->setCellValue('J' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_MOO_P1']);
            }
            if (empty($ResultDetail[0]['AREA_P1'])) {
                $sheet->setCellValue('K' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (3 + $current), $ResultDetail[0]['AREA_P1']);
            }
            if (empty($ResultDetail[0]['UTM_P1'])) {
                $sheet->setCellValue('L' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (3 + $current), $ResultDetail[0]['UTM_P1']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P1'])) {
                $sheet->setCellValue('M' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (3 + $current), $ResultDetail[0]['ORIGIN_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('N' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (3 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'])) {
                $sheet->setCellValue('O' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'])) {
                $sheet->setCellValue('P' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('Q' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'])) {
                $sheet->setCellValue('R' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_NO_P2'])) {
                $sheet->setCellValue('F' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_NO_P2']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('G' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (4 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('H' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (4 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('I' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (4 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_MOO_P2'])) {
                $sheet->setCellValue('J' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_MOO_P2']);
            }
            if (empty($ResultDetail[0]['AREA_P2'])) {
                $sheet->setCellValue('K' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (4 + $current), $ResultDetail[0]['AREA_P2']);
            }
            if (empty($ResultDetail[0]['UTM_P2'])) {
                $sheet->setCellValue('L' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (4 + $current), $ResultDetail[0]['UTM_P2']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P2'])) {
                $sheet->setCellValue('M' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (4 + $current), $ResultDetail[0]['ORIGIN_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('N' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (4 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2'])) {
                $sheet->setCellValue('O' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2'])) {
                $sheet->setCellValue('P' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('Q' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2'])) {
                $sheet->setCellValue('R' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']);
            }

            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('S' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('T' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('U' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('V' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (3 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('W' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('W' . (3 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('X' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('X' . (3 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('Y' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('Y' . (3 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[0 + 1]['OWN_P1'])) {
                    $sheet->setCellValue('Z' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('Z' . (3 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('S' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('T' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('U' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('V' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (4 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('W' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('W' . (4 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('X' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('X' . (4 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('Y' . (4 + $current), NULL);
                } else {
                    $sheet->setCellValue('Y' . (4 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[1]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[1]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('Z' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('Z' . (3 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            $current += 2;
        }




        $sheet->mergeCells('A1:E1');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'ข้อมูลการใช้ประโยชน์ในที่ดิน ERROR');
            $bn = 'ข้อมูลการใช้ประโยชน์ในที่ดิน error';
            $fileName = 'mas-7 ข้อมูลการใช้ประโยชน์ในที่ดิน error';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ');
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
            $fileName = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
        }
        $sheet->setCellValue('A2', '')
            ->setCellValue('B2', 'เลขเอกสารสิทธิ')
            ->setCellValue('C2', 'หมู่')
            ->setCellValue('D2', 'อำเภอ')
            ->setCellValue('E2', 'ตำบล')
            ->getStyle('A1:E2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->setCellValue('F1', 'ข้อมูลเอกสารสิทธิ');
        $sheet->mergeCells('F1:Q1');
        $sheet->setCellValue('F2', 'เลขที่เอกสารสิทธิ');
        $sheet->setCellValue('G2', 'ชื่อประเภทเอกสารสิทธิ')
            ->setCellValue('H2', 'ตำบล/แขวง')
            ->setCellValue('I2', 'อำเภอ/เขต')
            ->setCellValue('J2', 'หมู่')
            ->setCellValue('K2', 'ขนาดพื้นที่')
            ->setCellValue('L2', 'ระวาง UTM')
            ->setCellValue('M2', 'ระวางศูนย์กำเนิด')
            ->setCellValue('N2', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('O2', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('P2', 'สถานะ อปท.')
            ->setCellValue('Q2', 'สถานะการอายัด')
            ->setCellValue('R2', 'การใช้ประโยชน์')
            ->getStyle('A1:R2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('S1', 'ภาระผูกพัน');
        $sheet->mergeCells('S1:Z1');
        $sheet->setCellValue('S2', 'ชื่อหนังสือสัญญา');
        $sheet->setCellValue('T2', 'วันที่หนังสือสัญญา')
            ->setCellValue('U2', 'วันที่เริ่มสัญญา')
            ->setCellValue('V2', 'ราคาทุนทรัพย์')
            ->setCellValue('W2', 'ระยะเวลา')
            ->setCellValue('X2', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('Y2', 'ผู้ให้สัญญา')
            ->setCellValue('Z2', 'ผู้รับสัญญา')
            ->getStyle('A1:Z2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];

        $sheet->getStyle('A1:Z' . ($last * 2 + 2))->applyFromArray($styleArray);
        $sheet->getStyle('A1:Z2')->getFont()->setBold(true);


        $sheet->getStyle('A3:A' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B3:B' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C3:C' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D3:D' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E3:E' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F3:F' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G3:G' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H3:H' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I3:I' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J3:J' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K3:K' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L3:L' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('M3:M' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('N3:N' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('O3:O' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('P3:P' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Q3:Q' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('R3:R' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('S3:S' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('T3:T' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('U3:U' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('V3:V' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('W3:W' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('X3:X' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Y3:Y' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Z3:Z' . ($last * 2 + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        foreach (range('A', 'Z') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
            $fileName = urlencode($fileName);
        }
        header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
        $writer->save('php://output');


        break;
        // ================== NSL ===================
    case '8':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";

        for ($i = 0; $i < $last; $i++) {
            $sheet->mergeCells('A' . (3 + $current) . ':A' . (4 + $current));
            $sheet->mergeCells('B' . (3 + $current) . ':B' . (4 + $current));
            $sheet->mergeCells('C' . (3 + $current) . ':C' . (4 + $current));
            $sheet->mergeCells('D' . (3 + $current) . ':D' . (4 + $current));
            $sheet->mergeCells('E' . (3 + $current) . ':E' . (4 + $current));
            $sheet->mergeCells('F' . (3 + $current) . ':F' . (4 + $current));

            if ($Result[$i]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (3 + $current), ($i + 1));

                if (empty($Result[$i]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (3 + $current), $Result[$i]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$i]['PARCEL_LAND_NAME'])) {
                    $sheet->setCellValue('C' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (3 + $current), $Result[$i]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$i]['YEAR'])) {
                    $sheet->setCellValue('D' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (3 + $current), $Result[$i]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$i]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('E' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (3 + $current), $Result[$i]['AMPHUR_NAME']);
                }

                if (empty($Result[$i]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('F' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (3 + $current), $Result[$i]['TAMBOL_NAME']);
                }
                $current += 2;
            }
        }
        $sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

        $current = 0;


        for ($i = 0; $i < $last; $i++) {
            $ResultDetail = array();
            $ResultOlgt = array();

            $dataSeqP1 = $Result[$i]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$i]['PARCEL_LAND_SEQ_P2'];

            include 'queryDetailData.php';
            // while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);
            // }
            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }

            if (empty($ResultDetail[0]['PARCEL_LAND_NO_P1'])) {
                $sheet->setCellValue('G' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_NO_P1']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('H' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (3 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('I' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (3 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('J' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (3 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AREA_P1'])) {
                $sheet->setCellValue('K' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (3 + $current), $ResultDetail[0]['AREA_P1']);
            }
            if (empty($ResultDetail[0]['UTM_P1'])) {
                $sheet->setCellValue('L' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (3 + $current), $ResultDetail[0]['UTM_P1']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P1'])) {
                $sheet->setCellValue('M' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (3 + $current), $ResultDetail[0]['ORIGIN_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('N' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (3 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'])) {
                $sheet->setCellValue('O' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'])) {
                $sheet->setCellValue('P' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('Q' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'])) {
                $sheet->setCellValue('R' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1']);
            }


            if (empty($ResultDetail[0]['PARCEL_LAND_NO_P2'])) {
                $sheet->setCellValue('G' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_NO_P2']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('H' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (4 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('I' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (4 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('J' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (4 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AREA_P2'])) {
                $sheet->setCellValue('K' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (4 + $current), $ResultDetail[0]['AREA_P2']);
            }
            if (empty($ResultDetail[0]['UTM_P2'])) {
                $sheet->setCellValue('L' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (4 + $current), $ResultDetail[0]['UTM_P2']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P2'])) {
                $sheet->setCellValue('M' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (4 + $current), $ResultDetail[0]['ORIGIN_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('N' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (4 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2'])) {
                $sheet->setCellValue('O' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2'])) {
                $sheet->setCellValue('P' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('Q' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2'])) {
                $sheet->setCellValue('R' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']);
            }

            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('S' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('T' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('U' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('V' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (3 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('W' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('W' . (3 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('X' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('X' . (3 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('Y' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('Y' . (3 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[0 + 1]['OWN_P1'])) {
                    $sheet->setCellValue('Z' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('Z' . (3 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('S' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('T' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('U' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('V' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (4 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('W' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('W' . (4 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('X' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('X' . (4 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('Y' . (4 + $current), NULL);
                } else {
                    $sheet->setCellValue('Y' . (4 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[1]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[1]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('Z' . (4 + $current), NULL);
                } else {
                    $sheet->setCellValue('Z' . (4 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            $current += 2;
        }




        $sheet->mergeCells('A1:F1');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'ข้อมูลการใช้ประโยชน์ในที่ดิน ERROR');
            $bn = 'ข้อมูลการใช้ประโยชน์ในที่ดิน error';
            $fileName = 'mas-7 ข้อมูลการใช้ประโยชน์ในที่ดิน error';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ');
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
            $fileName = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
        }
        $sheet->setCellValue('A2', '')
            ->setCellValue('B2', 'เลขเอกสารสิทธิ')
            ->setCellValue('C2', 'ชื่อ น.ส.ล.')
            ->setCellValue('D2', 'ปีที่ออก')
            ->setCellValue('E2', 'อำเภอ')
            ->setCellValue('F2', 'ตำบล')
            ->getStyle('A1:F2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->setCellValue('G1', 'ข้อมูลเอกสารสิทธิ');
        $sheet->mergeCells('G1:R1');
        $sheet->setCellValue('G2', 'เลขที่เอกสารสิทธิ');
        $sheet->setCellValue('H2', 'ชื่อประเภทเอกสารสิทธิ')
            ->setCellValue('I2', 'ตำบล/แขวง')
            ->setCellValue('J2', 'อำเภอ/เขต')
            ->setCellValue('K2', 'ขนาดพื้นที่')
            ->setCellValue('L2', 'ระวาง UTM')
            ->setCellValue('M2', 'ระวางศูนย์กำเนิด')
            ->setCellValue('N2', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('O2', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('P2', 'สถานะ อปท.')
            ->setCellValue('Q2', 'สถานะการอายัด')
            ->setCellValue('R2', 'การใช้ประโยชน์')
            ->getStyle('A1:R2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('S1', 'ภาระผูกพัน');
        $sheet->mergeCells('S1:Z1');
        $sheet->setCellValue('S2', 'ชื่อหนังสือสัญญา');
        $sheet->setCellValue('T2', 'วันที่หนังสือสัญญา')
            ->setCellValue('U2', 'วันที่เริ่มสัญญา')
            ->setCellValue('V2', 'ราคาทุนทรัพย์')
            ->setCellValue('W2', 'ระยะเวลา')
            ->setCellValue('X2', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('Y2', 'ผู้ให้สัญญา')
            ->setCellValue('Z2', 'ผู้รับสัญญา')
            ->getStyle('A1:Z2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];

        $sheet->getStyle('A1:Z' . ($last + 2))->applyFromArray($styleArray);
        $sheet->getStyle('A1:Z2')->getFont()->setBold(true);


        $sheet->getStyle('A3:A' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B3:B' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C3:C' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D3:D' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E3:E' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F3:F' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G3:G' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H3:H' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I3:I' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J3:J' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K3:K' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L3:L' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('M3:M' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('N3:N' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('O3:O' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('P3:P' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Q3:Q' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('R3:R' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('S3:S' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('T3:T' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('U3:U' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('V3:V' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('W3:W' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('X3:X' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Y3:Y' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Z3:Z' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        foreach (range('A', 'Z') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
            $fileName = urlencode($fileName);
        }
        header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
        $writer->save('php://output');



        break;
    case '23':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";

        for ($i = 0; $i < $last; $i++) {
            $sheet->mergeCells('A' . (3 + $current) . ':A' . (4 + $current));
            $sheet->mergeCells('B' . (3 + $current) . ':B' . (4 + $current));
            $sheet->mergeCells('C' . (3 + $current) . ':C' . (4 + $current));
            $sheet->mergeCells('D' . (3 + $current) . ':D' . (4 + $current));
            $sheet->mergeCells('E' . (3 + $current) . ':E' . (4 + $current));

            if ($Result[$i]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (3 + $current), ($i + 1));

                if (empty($Result[$i]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (3 + $current), $Result[$i]['PARCEL_LAND_NO']);
                }

                if (empty($Result[$i]['MOO'])) {
                    $sheet->setCellValue('C' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (3 + $current), $Result[$i]['MOO']);
                }

                if (empty($Result[$i]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('D' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (3 + $current), $Result[$i]['AMPHUR_NAME']);
                }

                if (empty($Result[$i]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('E' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (3 + $current), $Result[$i]['TAMBOL_NAME']);
                }
                $current += 2;
            }
        }
        $sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

        $current = 0;


        for ($i = 0; $i < $last; $i++) {
            $ResultDetail = array();
            $ResultOlgt = array();

            $dataSeqP1 = $Result[$i]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$i]['PARCEL_LAND_SEQ_P2'];

            include 'queryDetailData.php';
            // while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);
            // }
            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }

            if (empty($ResultDetail[0]['PARCEL_LAND_NO_P1'])) {
                $sheet->setCellValue('F' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_NO_P1']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('G' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (3 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('H' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (3 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('I' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (3 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_MOO_P1'])) {
                $sheet->setCellValue('J' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_MOO_P1']);
            }
            if (empty($ResultDetail[0]['AREA_P1'])) {
                $sheet->setCellValue('K' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (3 + $current), $ResultDetail[0]['AREA_P1']);
            }
            if (empty($ResultDetail[0]['UTM_P1'])) {
                $sheet->setCellValue('L' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (3 + $current), $ResultDetail[0]['UTM_P1']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P1'])) {
                $sheet->setCellValue('M' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (3 + $current), $ResultDetail[0]['ORIGIN_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('N' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (3 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'])) {
                $sheet->setCellValue('O' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'])) {
                $sheet->setCellValue('P' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('Q' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'])) {
                $sheet->setCellValue('R' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (3 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_NO_P2'])) {
                $sheet->setCellValue('F' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_NO_P2']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('G' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (4 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('H' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (4 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('I' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (4 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_MOO_P2'])) {
                $sheet->setCellValue('J' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_MOO_P2']);
            }
            if (empty($ResultDetail[0]['AREA_P2'])) {
                $sheet->setCellValue('K' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (4 + $current), $ResultDetail[0]['AREA_P2']);
            }
            if (empty($ResultDetail[0]['UTM_P2'])) {
                $sheet->setCellValue('L' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (4 + $current), $ResultDetail[0]['UTM_P2']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P2'])) {
                $sheet->setCellValue('M' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (4 + $current), $ResultDetail[0]['ORIGIN_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('N' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (4 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2'])) {
                $sheet->setCellValue('O' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2'])) {
                $sheet->setCellValue('P' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('Q' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2'])) {
                $sheet->setCellValue('R' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (4 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']);
            }

            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('S' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('T' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('U' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('V' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (3 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('W' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('W' . (3 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('X' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('X' . (3 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('Y' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('Y' . (3 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[0 + 1]['OWN_P1'])) {
                    $sheet->setCellValue('Z' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('Z' . (3 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('S' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('T' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('U' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('V' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (4 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('W' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('W' . (4 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('X' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('X' . (4 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('Y' . (4 + $current), NULL);
                } else {
                    $sheet->setCellValue('Y' . (4 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[1]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[1]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('Z' . (4 + $current), NULL);
                } else {
                    $sheet->setCellValue('Z' . (4 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            $current += 2;
        }




        $sheet->mergeCells('A1:E1');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'ข้อมูลการใช้ประโยชน์ในที่ดิน ERROR');
            $bn = 'ข้อมูลการใช้ประโยชน์ในที่ดิน error';
            $fileName = 'mas-7 ข้อมูลการใช้ประโยชน์ในที่ดิน error';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ');
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
            $fileName = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
        }
        $sheet->setCellValue('A2', '')
            ->setCellValue('B2', 'เลขเอกสารสิทธิ')
            ->setCellValue('C2', 'หมู่')
            ->setCellValue('D2', 'อำเภอ')
            ->setCellValue('E2', 'ตำบล')
            ->getStyle('A1:E2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->setCellValue('F1', 'ข้อมูลเอกสารสิทธิ');
        $sheet->mergeCells('F1:Q1');
        $sheet->setCellValue('F2', 'เลขที่เอกสารสิทธิ');
        $sheet->setCellValue('G2', 'ชื่อประเภทเอกสารสิทธิ')
            ->setCellValue('H2', 'ตำบล/แขวง')
            ->setCellValue('I2', 'อำเภอ/เขต')
            ->setCellValue('J2', 'หมู่')
            ->setCellValue('K2', 'ขนาดพื้นที่')
            ->setCellValue('L2', 'ระวาง UTM')
            ->setCellValue('M2', 'ระวางศูนย์กำเนิด')
            ->setCellValue('N2', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('O2', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('P2', 'สถานะ อปท.')
            ->setCellValue('Q2', 'สถานะการอายัด')
            ->setCellValue('R2', 'การใช้ประโยชน์')
            ->getStyle('A1:R2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('S1', 'ภาระผูกพัน');
        $sheet->mergeCells('S1:Z1');
        $sheet->setCellValue('S2', 'ชื่อหนังสือสัญญา');
        $sheet->setCellValue('T2', 'วันที่หนังสือสัญญา')
            ->setCellValue('U2', 'วันที่เริ่มสัญญา')
            ->setCellValue('V2', 'ราคาทุนทรัพย์')
            ->setCellValue('W2', 'ระยะเวลา')
            ->setCellValue('X2', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('Y2', 'ผู้ให้สัญญา')
            ->setCellValue('Z2', 'ผู้รับสัญญา')
            ->getStyle('A1:Z2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];

        $sheet->getStyle('A1:Z' . ($last + 2))->applyFromArray($styleArray);
        $sheet->getStyle('A1:Z2')->getFont()->setBold(true);


        $sheet->getStyle('A3:A' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B3:B' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C3:C' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D3:D' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E3:E' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F3:F' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G3:G' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H3:H' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I3:I' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J3:J' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K3:K' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L3:L' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('M3:M' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('N3:N' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('O3:O' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('P3:P' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Q3:Q' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('R3:R' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('S3:S' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('T3:T' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('U3:U' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('V3:V' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('W3:W' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('X3:X' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Y3:Y' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Z3:Z' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        foreach (range('A', 'Z') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
            $fileName = urlencode($fileName);
        }
        header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
        $writer->save('php://output');


        break;
        // CONDO
    case '13':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";

        for ($i = 0; $i < $last; $i++) {
            $sheet->mergeCells('A' . (3 + $current) . ':A' . (4 + $current));
            $sheet->mergeCells('B' . (3 + $current) . ':B' . (4 + $current));
            $sheet->mergeCells('C' . (3 + $current) . ':C' . (4 + $current));
            $sheet->mergeCells('D' . (3 + $current) . ':D' . (4 + $current));
            $sheet->mergeCells('E' . (3 + $current) . ':E' . (4 + $current));

            if ($Result[$i]['CONDO_SEQ_P2']) {
                $sheet->setCellValue('A' . (3 + $current), ($i + 1));

                if (empty($Result[$i]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('B' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (3 + $current), $Result[$i]['AMPHUR_NAME']);
                }

                if (empty($Result[$i]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('C' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (3 + $current), $Result[$i]['TAMBOL_NAME']);
                }

                if (empty($Result[$i]['CONDO_REG_YEAR'])) {
                    $sheet->setCellValue('D' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (3 + $current), $Result[$i]['CONDO_REG_YEAR']);
                }

                if (empty($Result[$i]['CONDO_NAME_TH'])) {
                    $sheet->setCellValue('E' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (3 + $current), $Result[$i]['CONDO_NAME_TH']);
                }
                $current += 2;
            }
        }
        $sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

        $current = 0;


        for ($i = 0; $i < $last; $i++) {
            $ResultDetail = array();

            $dataSeqP1 = $Result[$i]['CONDO_SEQ_P1'];
            $dataSeqP2 = $Result[$i]['CONDO_SEQ_P2'];

            include 'queryDetailData.php';
            // while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);
            // }

            if (empty($ResultDetail[0]['CONDO_ID_P1'])) {
                $sheet->setCellValue('F' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (3 + $current), $ResultDetail[0]['CONDO_ID_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_NAME_TH_P1'])) {
                $sheet->setCellValue('G' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (3 + $current), $ResultDetail[0]['CONDO_NAME_TH_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('H' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (3 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('I' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (3 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_ROOM_NUM_P1'])) {
                $sheet->setCellValue('J' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (3 + $current), $ResultDetail[0]['CONDO_ROOM_NUM_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_RECV_DATE_P1'])) {
                $sheet->setCellValue('K' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (3 + $current), $ResultDetail[0]['CONDO_RECV_DATE_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_REGISTER_DATE_P1'])) {
                $sheet->setCellValue('L' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (3 + $current), $ResultDetail[0]['CONDO_REGISTER_DATE_P1']);
            }
            if (empty($ResultDetail[0]['CORP_ID_P1'])) {
                $sheet->setCellValue('M' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (3 + $current), $ResultDetail[0]['CORP_ID_P1']);
            }
            if (empty($ResultDetail[0]['ADDR_P1'])) {
                $sheet->setCellValue('N' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (3 + $current), $ResultDetail[0]['ADDR_P1']);
            }


            if (empty($ResultDetail[0]['CONDO_ID_P2'])) {
                $sheet->setCellValue('F' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (4 + $current), $ResultDetail[0]['CONDO_ID_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_NAME_TH_P2'])) {
                $sheet->setCellValue('G' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (4 + $current), $ResultDetail[0]['CONDO_NAME_TH_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('H' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (4 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('I' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (4 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_ROOM_NUM_P2'])) {
                $sheet->setCellValue('J' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (4 + $current), $ResultDetail[0]['CONDO_ROOM_NUM_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_RECV_DATE_P2'])) {
                $sheet->setCellValue('K' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (4 + $current), $ResultDetail[0]['CONDO_RECV_DATE_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_REGISTER_DATE_P2'])) {
                $sheet->setCellValue('L' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (4 + $current), $ResultDetail[0]['CONDO_REGISTER_DATE_P2']);
            }
            if (empty($ResultDetail[0]['CORP_ID_P2'])) {
                $sheet->setCellValue('M' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (4 + $current), $ResultDetail[0]['CORP_ID_P2']);
            }
            if (empty($ResultDetail[0]['ADDR_P2'])) {
                $sheet->setCellValue('N' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (4 + $current), $ResultDetail[0]['ADDR_P2']);
            }

            $current += 2;
        }




        $sheet->mergeCells('A1:E1');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'ข้อมูลการใช้ประโยชน์ในที่ดิน ERROR');
            $bn = 'ข้อมูลการใช้ประโยชน์ในที่ดิน error';
            $fileName = 'mas-7 ข้อมูลการใช้ประโยชน์ในที่ดิน error';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ');
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
            $fileName = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
        }
        $sheet->setCellValue('A2', '')
            ->setCellValue('B2', 'อำเภอ')
            ->setCellValue('C2', 'ตำบล')
            ->setCellValue('D2', 'เลขที่อาคารชุด')
            ->setCellValue('E2', 'ชื่ออาคารชุด')
            ->getStyle('A1:E2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->setCellValue('F1', 'ข้อมูลเอกสารสิทธิ');
        $sheet->mergeCells('F1:Q1');
        $sheet->setCellValue('F2', 'เลขที่อาคารชุด');
        $sheet->setCellValue('G2', 'ชื่ออาคารชุด')
            ->setCellValue('H2', 'อำเภอ/เขต')
            ->setCellValue('I2', 'ตำบล/แขวง')
            ->setCellValue('J2', 'จำนวนห้องชุด')
            ->setCellValue('K2', 'วันที่ได้รับอนุญาต')
            ->setCellValue('L2', 'วันที่จดทะเบียนอาคารชุด')
            ->setCellValue('M2', 'เลขที่นิติบุคคลอาคารชุด')
            ->setCellValue('N2', 'ที่ตั้งนิติบุคคลอาคารชุด')
            ->getStyle('A1:N2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);



        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];

        $sheet->getStyle('A1:N' . ($last + 2))->applyFromArray($styleArray);
        $sheet->getStyle('A1:N2')->getFont()->setBold(true);


        $sheet->getStyle('A3:A' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B3:B' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C3:C' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D3:D' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E3:E' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F3:F' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G3:G' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H3:H' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I3:I' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J3:J' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K3:K' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L3:L' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('M3:M' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('N3:N' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        foreach (range('A', 'N') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
            $fileName = urlencode($fileName);
        }
        header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
        $writer->save('php://output');


        break;
    case '9':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";

        for ($i = 0; $i < $last; $i++) {
            $sheet->mergeCells('A' . (3 + $current) . ':A' . (4 + $current));
            $sheet->mergeCells('B' . (3 + $current) . ':B' . (4 + $current));
            $sheet->mergeCells('C' . (3 + $current) . ':C' . (4 + $current));
            $sheet->mergeCells('D' . (3 + $current) . ':D' . (4 + $current));
            $sheet->mergeCells('E' . (3 + $current) . ':E' . (4 + $current));
            $sheet->mergeCells('F' . (3 + $current) . ':F' . (4 + $current));

            if ($Result[$i]['CONDOROOM_SEQ_P2']) {
                $sheet->setCellValue('A' . (3 + $current), ($i + 1));

                if (empty($Result[$i]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('B' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (3 + $current), $Result[$i]['AMPHUR_NAME']);
                }

                if (empty($Result[$i]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('C' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (3 + $current), $Result[$i]['TAMBOL_NAME']);
                }

                if (empty($Result[$i]['CONDOROOM_RNO'])) {
                    $sheet->setCellValue('D' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (3 + $current), $Result[$i]['CONDOROOM_RNO']);
                }

                if (empty($Result[$i]['CONDO_REG_YEAR'])) {
                    $sheet->setCellValue('E' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (3 + $current), $Result[$i]['CONDO_REG_YEAR']);
                }
                if (empty($Result[$i]['CONDO_NAME_TH'])) {
                    $sheet->setCellValue('F' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (3 + $current), $Result[$i]['CONDO_NAME_TH']);
                }
                $current += 2;
            }
        }
        $sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

        $current = 0;


        for ($i = 0; $i < $last; $i++) {
            $ResultDetail = array();
            $ResultOlgt = array();

            $dataSeqP1 = $Result[$i]['CONDOROOM_SEQ_P1'];
            $dataSeqP2 = $Result[$i]['CONDOROOM_SEQ_P2'];

            include 'queryDetailData.php';
            // while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);
            // }
            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }

            if (empty($ResultDetail[0]['CONDOROOM_RNO_P1'])) {
                $sheet->setCellValue('G' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (3 + $current), $ResultDetail[0]['CONDOROOM_RNO_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_NAME_TH_P1'])) {
                $sheet->setCellValue('H' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (3 + $current), $ResultDetail[0]['CONDO_NAME_TH_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_ID_P1'])) {
                $sheet->setCellValue('I' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (3 + $current), $ResultDetail[0]['CONDO_ID_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_NAME_TH_P1'])) {
                $sheet->setCellValue('J' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (3 + $current), $ResultDetail[0]['CONDO_NAME_TH_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('K' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (3 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('L' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (3 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_AREA_NUM_P1'])) {
                $sheet->setCellValue('M' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (3 + $current), $ResultDetail[0]['CONDOROOM_AREA_NUM_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('N' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (3 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_OWNER_NUM_P1'])) {
                $sheet->setCellValue('O' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (3 + $current), $ResultDetail[0]['CONDOROOM_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('P' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (3 + $current), $ResultDetail[0]['CONDOROOM_SEQUEST_STS_P1']);
            }

            if (empty($ResultDetail[0]['CONDOROOM_RNO_P2'])) {
                $sheet->setCellValue('G' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (4 + $current), $ResultDetail[0]['CONDOROOM_RNO_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_NAME_TH_P2'])) {
                $sheet->setCellValue('H' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (4 + $current), $ResultDetail[0]['CONDO_NAME_TH_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_ID_P2'])) {
                $sheet->setCellValue('I' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (4 + $current), $ResultDetail[0]['CONDO_ID_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_NAME_TH_P2'])) {
                $sheet->setCellValue('J' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (4 + $current), $ResultDetail[0]['CONDO_NAME_TH_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('K' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (4 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('L' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (4 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_AREA_NUM_P2'])) {
                $sheet->setCellValue('M' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (4 + $current), $ResultDetail[0]['CONDOROOM_AREA_NUM_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('N' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (4 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_OWNER_NUM_P2'])) {
                $sheet->setCellValue('O' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (4 + $current), $ResultDetail[0]['CONDOROOM_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('P' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (4 + $current), $ResultDetail[0]['CONDOROOM_SEQUEST_STS_P2']);
            }


            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('Q' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('R' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('S' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('T' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (3 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('U' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (3 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('V' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (3 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('W' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('W' . (3 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[1]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[1]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P1'])) {
                    $sheet->setCellValue('X' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('X' . (3 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('Q' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('R' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('S' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('T' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (4 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('U' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (4 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('V' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (4 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('W' . (4 + $current), NULL);
                } else {
                    $sheet->setCellValue('W' . (4 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[1]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[1]['PARTY_TYPE_P2'] == 4) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('X' . (4 + $current), NULL);
                } else {
                    $sheet->setCellValue('X' . (4 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            $current += 2;
        }


        $sheet->mergeCells('A1:F1');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'ข้อมูลการใช้ประโยชน์ในที่ดิน ERROR');
            $bn = 'ข้อมูลการใช้ประโยชน์ในที่ดิน error';
            $fileName = 'mas-7 ข้อมูลการใช้ประโยชน์ในที่ดิน error';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ');
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
            $fileName = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
        }
        $sheet->setCellValue('A2', '')
            ->setCellValue('B2', 'อำเภอ')
            ->setCellValue('C2', 'ตำบล')
            ->setCellValue('D2', 'เลขที่ห้อง')
            ->setCellValue('E2', 'เลขที่อาคารชุด')
            ->setCellValue('F2', 'ชื่ออาคารชุด')
            ->getStyle('A1:F2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->setCellValue('G1', 'ข้อมูลเอกสารสิทธิ');
        $sheet->mergeCells('G1:P1');
        $sheet->setCellValue('G2', 'เลขที่ห้องชุด');
        $sheet->setCellValue('H2', 'ชื่ออาคาร')
            ->setCellValue('I2', 'เลขที่อาคารชุด')
            ->setCellValue('J2', 'ชื่ออาคารชุด')
            ->setCellValue('K2', 'ตำบล/แขวง')
            ->setCellValue('L2', 'อำเภอ/เขต')
            ->setCellValue('M2', 'ขนาดพื้นที่')
            ->setCellValue('N2', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('O2', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('P2', 'สถานะการอายัด')
            ->getStyle('A1:P2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('Q1', 'ภาระผูกพัน');
        $sheet->mergeCells('Q1:X1');
        $sheet->setCellValue('Q2', 'ชื่อหนังสือสัญญา');
        $sheet->setCellValue('R2', 'วันที่หนังสือสัญญา')
            ->setCellValue('S2', 'วันที่เริ่มสัญญา')
            ->setCellValue('T2', 'ราคาทุนทรัพย์')
            ->setCellValue('U2', 'ระยะเวลา')
            ->setCellValue('V2', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('W2', 'ผู้ให้สัญญา')
            ->setCellValue('X2', 'ผู้รับสัญญา')
            ->getStyle('A1:X2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];

        $sheet->getStyle('A1:X' . ($last * 2 + 2))->applyFromArray($styleArray);
        $sheet->getStyle('A1:X2')->getFont()->setBold(true);


        $sheet->getStyle('A3:A' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B3:B' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C3:C' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D3:D' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E3:E' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F3:F' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G3:G' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H3:H' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I3:I' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J3:J' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K3:K' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L3:L' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('M3:M' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('N3:N' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('O3:O' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('P3:P' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Q3:Q' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('R3:R' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('S3:S' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('T3:T' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('U3:U' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('V3:V' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('W3:W' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('X3:X' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        foreach (range('A', 'X') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
            $fileName = urlencode($fileName);
        }
        header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
        $writer->save('php://output');


        break;
    case 'c':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";

        for ($i = 0; $i < $last; $i++) {
            $sheet->mergeCells('A' . (3 + $current) . ':A' . (4 + $current));
            $sheet->mergeCells('B' . (3 + $current) . ':B' . (4 + $current));
            $sheet->mergeCells('C' . (3 + $current) . ':C' . (4 + $current));
            $sheet->mergeCells('D' . (3 + $current) . ':D' . (4 + $current));
            $sheet->mergeCells('E' . (3 + $current) . ':E' . (4 + $current));
            $sheet->mergeCells('F' . (3 + $current) . ':F' . (4 + $current));

            if ($Result[$i]['CONSTRUCT_SEQ_P2']) {
                $sheet->setCellValue('A' . (3 + $current), ($i + 1));

                if (empty($Result[$i]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('B' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (3 + $current), $Result[$i]['AMPHUR_NAME']);
                }

                if (empty($Result[$i]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('C' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (3 + $current), $Result[$i]['TAMBOL_NAME']);
                }

                if (empty($Result[$i]['CONSTRUCT_ADDR_HID'])) {
                    $sheet->setCellValue('D' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (3 + $current), $Result[$i]['CONSTRUCT_ADDR_HID']);
                }

                if (empty($Result[$i]['CONSTRUCT_ADDR_HNO'])) {
                    $sheet->setCellValue('E' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (3 + $current), $Result[$i]['CONSTRUCT_ADDR_HNO']);
                }
                if (empty($Result[$i]['CONSTRUCT_ADDR_MOO'])) {
                    $sheet->setCellValue('F' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (3 + $current), $Result[$i]['CONSTRUCT_ADDR_MOO']);
                }
                if (empty($Result[$i]['CONSTRUCTION_NAME'])) {
                    $sheet->setCellValue('G' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('G' . (3 + $current), $Result[$i]['CONSTRUCTION_NAME']);
                }

                $current += 2;
            }
        }
        $sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

        $current = 0;


        for ($i = 0; $i < $last; $i++) {
            $ResultDetail = array();
            $ResultOlgt = array();

            $dataSeqP1 = $Result[$i]['CONSTRUCT_SEQ_P1'];
            $dataSeqP2 = $Result[$i]['CONSTRUCT_SEQ_P2'];

            include 'queryDetailData.php';
            // while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);
            // }
            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }

            if (empty($ResultDetail[0]['ADDR_P1'])) {
                $sheet->setCellValue('H' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (3 + $current), $ResultDetail[0]['ADDR_P1']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_AREA_NUM_P1'])) {
                $sheet->setCellValue('I' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (3 + $current), $ResultDetail[0]['CONSTRUCT_AREA_NUM_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('J' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (3 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('K' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (3 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_NO_P1'])) {
                $sheet->setCellValue('L' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (3 + $current), $ResultDetail[0]['PARCEL_NO_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('M' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (3 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('N' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (3 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_PROCESS_DTM_P1'])) {
                $sheet->setCellValue('O' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (3 + $current), $ResultDetail[0]['CONSTRUCT_PROCESS_DTM_P1']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_REGIST_NAME_P1'])) {
                $sheet->setCellValue('P' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (3 + $current), $ResultDetail[0]['CONSTRUCT_REGIST_NAME_P1']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('Q' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (3 + $current), $ResultDetail[0]['CONDOROOM_SEQUEST_STS_P1']);
            }

            if (empty($ResultDetail[0]['ADDR_P2'])) {
                $sheet->setCellValue('H' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (4 + $current), $ResultDetail[0]['ADDR_P2']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_AREA_NUM_P2'])) {
                $sheet->setCellValue('I' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (4 + $current), $ResultDetail[0]['CONSTRUCT_AREA_NUM_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('J' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (4 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('K' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (4 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_NO_P2'])) {
                $sheet->setCellValue('L' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (4 + $current), $ResultDetail[0]['PARCEL_NO_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('M' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (4 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('N' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (4 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_PROCESS_DTM_P2'])) {
                $sheet->setCellValue('O' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (4 + $current), $ResultDetail[0]['CONSTRUCT_PROCESS_DTM_P2']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_REGIST_NAME_P2'])) {
                $sheet->setCellValue('P' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (4 + $current), $ResultDetail[0]['CONSTRUCT_REGIST_NAME_P2']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('Q' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (4 + $current), $ResultDetail[0]['CONDOROOM_SEQUEST_STS_P2']);
            }



            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('R' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('S' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('T' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (3 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('U' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (3 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('V' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (3 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('W' . (3 + $current), NULL);
            } else {
                $sheet->setCellValue('W' . (3 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('X' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('X' . (3 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[1]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[1]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P1'])) {
                    $sheet->setCellValue('Y' . (3 + $current), NULL);
                } else {
                    $sheet->setCellValue('Y' . (3 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('R' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('S' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('T' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('T' . (4 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('U' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('U' . (4 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('V' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('V' . (4 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('W' . (4 + $current), NULL);
            } else {
                $sheet->setCellValue('W' . (4 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('X' . (4 + $current), NULL);
                } else {
                    $sheet->setCellValue('X' . (4 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[1]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[1]['PARTY_TYPE_P2'] == 4) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('Y' . (4 + $current), NULL);
                } else {
                    $sheet->setCellValue('Y' . (4 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            $current += 2;
        }


        $sheet->mergeCells('A1:G1');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'ข้อมูลการใช้ประโยชน์ในที่ดิน ERROR');
            $bn = 'ข้อมูลการใช้ประโยชน์ในที่ดิน error';
            $fileName = 'mas-7 ข้อมูลการใช้ประโยชน์ในที่ดิน error';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ');
            $bn = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
            $fileName = 'ข้อมูลทะเบียนที่ดินและรายการจดทะเบียนที่ถ่ายโอนสำเร็จ';
        }
        $sheet->setCellValue('A2', '')
            ->setCellValue('B2', 'อำเภอ')
            ->setCellValue('C2', 'ตำบล')
            ->setCellValue('D2', 'รหัสประจำบ้าน')
            ->setCellValue('E2', 'เลขที่บ้าน')
            ->setCellValue('F2', 'หมู่')
            ->setCellValue('G2', 'ชื่อสิ่งปลูกสร้าง')
            ->getStyle('A1:G2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->setCellValue('H1', 'ข้อมูลเอกสารสิทธิ');
        $sheet->mergeCells('H1:P1');
        $sheet->setCellValue('H2', 'ที่ตั้งนิติบุคคลอาคารชุด');
        $sheet->setCellValue('I2', 'เนื้อที่')
            ->setCellValue('J2', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('K2', 'ชื่อประเภทเอกสารสิทธิ')
            ->setCellValue('L2', 'เลขที่เอกสารสิทธิ')
            ->setCellValue('M2', 'ตำบล/แขวง')
            ->setCellValue('N2', 'อำเภอ/เขต')
            ->setCellValue('O2', 'วันที่จดทะเบียนล่าสุด')
            ->setCellValue('P2', 'ประเภทจดทะเบียนล่าสุด')
            ->setCellValue('Q2', 'สถานะการอายัด')
            ->getStyle('A1:Q2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('R1', 'ภาระผูกพัน');
        $sheet->mergeCells('R1:Y1');
        $sheet->setCellValue('R2', 'ชื่อหนังสือสัญญา');
        $sheet->setCellValue('S2', 'วันที่หนังสือสัญญา')
            ->setCellValue('T2', 'วันที่เริ่มสัญญา')
            ->setCellValue('U2', 'ราคาทุนทรัพย์')
            ->setCellValue('V2', 'ระยะเวลา')
            ->setCellValue('W2', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('X2', 'ผู้ให้สัญญา')
            ->setCellValue('Y2', 'ผู้รับสัญญา')
            ->getStyle('A1:Y2')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '00000000'],
                ],
            ],
            'font' => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 16,
                'name'  => 'TH SarabunPSK'
            )
        ];

        $sheet->getStyle('A1:Y' . ($last * 2 + 2))->applyFromArray($styleArray);
        $sheet->getStyle('A1:Y2')->getFont()->setBold(true);


        $sheet->getStyle('A3:A' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B3:B' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C3:C' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D3:D' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E3:E' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F3:F' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G3:G' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H3:H' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I3:I' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J3:J' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K3:K' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L3:L' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('M3:M' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('N3:N' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('O3:O' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('P3:P' . $last + 2)
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Q3:Q' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('R3:R' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('S3:S' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('T3:T' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('U3:U' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('V3:V' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('W3:W' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('X3:X' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Y3:Y' . ($last + 2))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        foreach (range('A', 'Y') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
            $fileName = urlencode($fileName);
        }
        header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
        $writer->save('php://output');


        break;
    default:
        # code...
        break;
}

