<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'queryADMData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}



switch ($checknum){
    case '1' : //ข้อมูลสิทธิ
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสิทธิ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลสิทธิ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสิทธิ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสิทธิ');
            $bn = 'รายการข้อมูลสิทธิ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสิทธิ';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อสิทธิ')
        
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อสิทธิ')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['PERMISSION_SEQ_P1'] != $Result[$i]['PERMISSION_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PERMISSION_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PERMISSION_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PERMISSION_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PERMISSION_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PERMISSION_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['PERMISSION_NAME_P2']);


                    if(empty($Result[$i]['PERMISSION_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสิทธิ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['PERMISSION_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสิทธิ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['PERMISSION_ID_P1'] != $Result[$i]['PERMISSION_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['PERMISSION_NAME_P1'] != $Result[$i]['PERMISSION_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อสิทธิ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['PERMISSION_SEQ_P1'] == $Result[$i]['PERMISSION_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PERMISSION_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PERMISSION_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PERMISSION_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PERMISSION_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PERMISSION_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['PERMISSION_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }
        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //ข้อมูลระบบงาน
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลระบบงาน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลระบบงาน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลระบบงาน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลระบบงาน');
            $bn = 'รายการข้อมูลระบบงาน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลระบบงาน';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อย่อ')       
            ->setCellValue('D4', 'ชื่อระบบงาน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ชื่อย่อ')       
            ->setCellValue('G4', 'ชื่อระบบงาน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['SYSTEM_SEQ_P1'] != $Result[$i]['SYSTEM_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['SYSTEM_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['SYSTEM_ABBR_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['SYSTEM_NAME_TH_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['SYSTEM_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['SYSTEM_ABBR_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['SYSTEM_NAME_TH_P2']);


                    if(empty($Result[$i]['SYSTEM_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลระบบงาน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['SYSTEM_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลระบบงาน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['SYSTEM_ABBR_P1'] != $Result[$i]['SYSTEM_ABBR_P2']) $problemDesc  = $problemDesc . "ชื่อย่อ ไม่ตรงกัน ";
                        if($Result[$i]['SYSTEM_NAME_TH_P1'] != $Result[$i]['SYSTEM_NAME_TH_P2']) $problemDesc  = $problemDesc . "ชื่อระบบงาน ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['SYSTEM_SEQ_P1'] == $Result[$i]['SYSTEM_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['SYSTEM_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['SYSTEM_ABBR_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['SYSTEM_NAME_TH_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['SYSTEM_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['SYSTEM_ABBR_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['SYSTEM_NAME_TH_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }
        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //ข้อมูลหน้าจอ
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหน้าจอ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลหน้าจอ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหน้าจอ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหน้าจอ');
            $bn = 'รายการข้อมูลหน้าจอ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหน้าจอ';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อหน้าจอ')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อหน้าจอ')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['SCREEN_SEQ_P1'] != $Result[$i]['SCREEN_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['SCREEN_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['SCREEN_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['SCREEN_NAME_TH_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['SCREEN_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['SCREEN_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['SCREEN_NAME_TH_P2']);


                    if(empty($Result[$i]['SCREEN_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหน้าจอ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['SCREEN_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหน้าจอ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['SCREEN_ID_P1'] != $Result[$i]['SCREEN_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['SCREEN_NAME_TH_P1'] != $Result[$i]['SCREEN_NAME_TH_P2']) $problemDesc  = $problemDesc . "ชื่อหน้าจอ ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['SCREEN_SEQ_P1'] == $Result[$i]['SCREEN_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['SCREEN_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['SCREEN_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['SCREEN_NAME_TH_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['SCREEN_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['SCREEN_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['SCREEN_NAME_TH_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }
        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '4' : //ข้อมูลกลุ่มผู้ใช้งาน
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลกลุ่มผู้ใช้งาน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลกลุ่มผู้ใช้งาน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลกลุ่มผู้ใช้งาน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลกลุ่มผู้ใช้งาน');
            $bn = 'รายการข้อมูลกลุ่มผู้ใช้งาน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลกลุ่มผู้ใช้งาน';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อกลุ่มผู้ใช้งาน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อกลุ่มผู้ใช้งาน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['USER_GROUP_SEQ_P1'] != $Result[$i]['USER_GROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['USER_GROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['USER_GROUP_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['USER_GROUP_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['USER_GROUP_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['USER_GROUP_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['USER_GROUP_NAME_P2']);


                    if(empty($Result[$i]['USER_GROUP_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลกลุ่มผู้ใช้งาน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['USER_GROUP_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลกลุ่มผู้ใช้งาน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['USER_GROUP_ID_P1'] != $Result[$i]['USER_GROUP_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['USER_GROUP_NAME_P1'] != $Result[$i]['USER_GROUP_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อกลุ่มผู้ใช้งาน ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['USER_GROUP_SEQ_P1'] == $Result[$i]['USER_GROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['USER_GROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['USER_GROUP_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['USER_GROUP_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['USER_GROUP_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['USER_GROUP_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['USER_GROUP_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }
        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '5' : //ข้อมูลกลุ่มเมนู
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลกลุ่มเมนู ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลกลุ่มเมนู ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลกลุ่มเมนู ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลกลุ่มเมนู');
            $bn = 'รายการข้อมูลกลุ่มเมนู';
            $fileName = date("Y/m/d") . '-รายการข้อมูลกลุ่มเมนู';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อกลุ่มเมนู')       
            ->setCellValue('D4', 'รายละเอียดกลุ่มเมนู')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ชื่อกลุ่มเมนู')       
            ->setCellValue('G4', 'รายละเอียดกลุ่มเมนู')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['MENU_GROUP_SEQ_P1'] != $Result[$i]['MENU_GROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['MENU_GROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['MENU_GROUP_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['MENU_GROUP_DESC_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['MENU_GROUP_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['MENU_GROUP_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['MENU_GROUP_DESC_P2']);

                    if(empty($Result[$i]['MENU_GROUP_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลกลุ่มเมนู ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['MENU_GROUP_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลกลุ่มเมนู ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['MENU_GROUP_NAME_P1'] != $Result[$i]['MENU_GROUP_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อกลุ่มเมนู ไม่ตรงกัน ";
                        if($Result[$i]['MENU_GROUP_DESC_P1'] != $Result[$i]['MENU_GROUP_DESC_P2']) $problemDesc  = $problemDesc . "รายละเอียดกลุ่มเมนู ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['MENU_GROUP_SEQ_P1'] == $Result[$i]['MENU_GROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['MENU_GROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['MENU_GROUP_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['MENU_GROUP_DESC_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['MENU_GROUP_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['MENU_GROUP_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['MENU_GROUP_DESC_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }
        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '6' : //ข้อมูลกลุ่มตำแหน่ง
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลกลุ่มตำแหน่ง ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลกลุ่มตำแหน่ง ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลกลุ่มตำแหน่ง ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลกลุ่มตำแหน่ง');
            $bn = 'รายการข้อมูลกลุ่มตำแหน่ง';
            $fileName = date("Y/m/d") . '-รายการข้อมูลกลุ่มตำแหน่ง';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อกลุ่มตำแหน่ง')
        
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อกลุ่มตำแหน่ง')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['POSITION_GROUP_SEQ_P1'] != $Result[$i]['POSITION_GROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['POSITION_GROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['POSITION_GROUP_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['POSITION_GROUP_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['POSITION_GROUP_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['POSITION_GROUP_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['POSITION_GROUP_NAME_P2']);


                    if(empty($Result[$i]['POSITION_GROUP_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลกลุ่มตำแหน่ง ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['POSITION_GROUP_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลกลุ่มตำแหน่ง ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['POSITION_GROUP_ID_P1'] != $Result[$i]['POSITION_GROUP_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['POSITION_GROUP_NAME_P1'] != $Result[$i]['POSITION_GROUP_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อกลุ่มตำแหน่ง ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['POSITION_GROUP_SEQ_P1'] == $Result[$i]['POSITION_GROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['POSITION_GROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['POSITION_GROUP_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['POSITION_GROUP_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['POSITION_GROUP_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['POSITION_GROUP_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['POSITION_GROUP_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }
        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    


}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
