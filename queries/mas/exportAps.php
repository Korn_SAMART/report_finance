<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'queryAPSData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}



switch ($checknum){
    case '1' : //ข้อมูลห้องชุด
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลห้องชุด ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลห้องชุด ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลห้องชุด ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลห้องชุด');
            $bn = 'รายการข้อมูลห้องชุด';
            $fileName = date("Y/m/d") . '-รายการข้อมูลห้องชุด';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อห้องชุด')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อห้องชุด')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['ROOM_USAGE_SEQ_P1'] != $Result[$i]['ROOM_USAGE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['ROOM_USAGE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['ROOM_USAGE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['ROOM_USAGE_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['ROOM_USAGE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['ROOM_USAGE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['ROOM_USAGE_NAME_P2']);


                    if(empty($Result[$i]['ROOM_USAGE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลห้องชุด ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['ROOM_USAGE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลห้องชุด ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['ROOM_USAGE_ID_P1'] != $Result[$i]['ROOM_USAGE_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['ROOM_USAGE_NAME_P1'] != $Result[$i]['ROOM_USAGE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อห้องชุด ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['ROOM_USAGE_SEQ_P1'] == $Result[$i]['ROOM_USAGE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['ROOM_USAGE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['ROOM_USAGE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['ROOM_USAGE_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['ROOM_USAGE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['ROOM_USAGE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['ROOM_USAGE_NAME_P2']);
                                    
                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }
        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //ข้อมูลพื้นที่
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลพื้นที่ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลพื้นที่ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลพื้นที่ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลพื้นที่');
            $bn = 'รายการข้อมูลพื้นที่';
            $fileName = date("Y/m/d") . '-รายการข้อมูลพื้นที่';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อพื้นที่')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อพื้นที่')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['POSSESSORY_SEQ_P1'] != $Result[$i]['POSSESSORY_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['POSSESSORY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['POSSESSORY_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['POSSESSORY_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['POSSESSORY_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['POSSESSORY_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['POSSESSORY_NAME_P2']);


                    if(empty($Result[$i]['POSSESSORY_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลพื้นที่ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['POSSESSORY_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลพื้นที่ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['POSSESSORY_ID_P1'] != $Result[$i]['POSSESSORY_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['POSSESSORY_NAME_P1'] != $Result[$i]['POSSESSORY_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อพื้นที่ ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['POSSESSORY_SEQ_P1'] == $Result[$i]['POSSESSORY_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['POSSESSORY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['POSSESSORY_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['POSSESSORY_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['POSSESSORY_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['POSSESSORY_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['POSSESSORY_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }
        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //ข้อมูลชั้น
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายกาข้อมูลชั้น ที่แตกต่างกัน' );
            $bn = 'รายกาข้อมูลชั้น ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายกาข้อมูลชั้น ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายกาข้อมูลชั้น');
            $bn = 'รายกาข้อมูลชั้น';
            $fileName = date("Y/m/d") . '-รายกาข้อมูลชั้น';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อชั้น')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อชั้น')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['FLOOR_SEQ_P1'] != $Result[$i]['FLOOR_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['FLOOR_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['FLOOR_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['FLOOR_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['FLOOR_NAME_P2']);

                    if(empty($Result[$i]['FLOOR_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลชั้น ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['FLOOR_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลชั้น ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['FLOOR_NAME_P1'] != $Result[$i]['FLOOR_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อชั้น ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['FLOOR_SEQ_P1'] == $Result[$i]['FLOOR_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['FLOOR_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['FLOOR_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['FLOOR_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['FLOOR_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    


}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
