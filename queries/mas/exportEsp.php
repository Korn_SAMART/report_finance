<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'queryESPData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}



switch ($checknum){
    case '1' : //ข้อมูลวันที่
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลวันที่ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลวันที่ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลวันที่ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลวันที่');
            $bn = 'รายการข้อมูลวันที่';
            $fileName = date("Y/m/d") . '-รายการข้อมูลวันที่';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'สถานะวันที่')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'สถานะวันที่')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['DAY_SEQ_P1'] != $Result[$i]['DAY_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['DAY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['DAY_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['DAY_FLAG_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['DAY_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['DAY_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['DAY_FLAG_P2']);


                    if(empty($Result[$i]['DAY_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลวันที่ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['DAY_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลวันที่ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['DAY_ID_P1'] != $Result[$i]['DAY_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['DAY_FLAG_P1'] != $Result[$i]['DAY_FLAG_P2']) $problemDesc  = $problemDesc . "สถานะวันที่ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['DAY_SEQ_P1'] == $Result[$i]['DAY_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['DAY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['DAY_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['DAY_FLAG_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['DAY_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['DAY_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['DAY_FLAG_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //สถานะการดึงข้อมูลทะเบียนราษฏร์
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสถานะการดึงข้อมูลทะเบียนราษฏร์ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลสถานะการดึงข้อมูลทะเบียนราษฏร์ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสถานะการดึงข้อมูลทะเบียนราษฏร์ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสถานะการดึงข้อมูลทะเบียนราษฏร์');
            $bn = 'รายการข้อมูลสถานะการดึงข้อมูลทะเบียนราษฏร์';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสถานะการดึงข้อมูลทะเบียนราษฏร์';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'CODE')       
            ->setCellValue('D4', 'คำอธิบาย')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'CODE')       
            ->setCellValue('G4', 'คำอธิบาย')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['MOI_STATUS_SEQ_P1'] != $Result[$i]['MOI_STATUS_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['MOI_STATUS_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['MOI_STATUS_CODE_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['MOI_STATUS_DESC_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['MOI_STATUS_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['MOI_STATUS_CODE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['MOI_STATUS_DESC_P2']);


                    if(empty($Result[$i]['MOI_STATUS_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสถานะการดึงข้อมูลทะเบียนราษฏร์ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['MOI_STATUS_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสถานะการดึงข้อมูลทะเบียนราษฏร์ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['MOI_STATUS_CODE_P1'] != $Result[$i]['MOI_STATUS_CODE_P2']) $problemDesc  = $problemDesc . "CODE ไม่ตรงกัน ";
                        if($Result[$i]['MOI_STATUS_DESC_P1'] != $Result[$i]['MOI_STATUS_DESC_P2']) $problemDesc  = $problemDesc . "คำอธิบาย ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['MOI_STATUS_SEQ_P1'] == $Result[$i]['MOI_STATUS_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['MOI_STATUS_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['MOI_STATUS_CODE_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['MOI_STATUS_DESC_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['MOI_STATUS_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['MOI_STATUS_CODE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['MOI_STATUS_DESC_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //ข้อมูลเดือน
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลเดือน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลเดือน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลเดือน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลเดือน');
            $bn = 'รายการข้อมูลเดือน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลเดือน';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อเดือนไทย')
            ->setCellValue('E4', 'ชื่อเดือนอังกฤษ')

            ->setCellValue('F4', 'SEQ')
            ->setCellValue('G4', 'ID')
            ->setCellValue('H4', 'ชื่อเดือนไทย')
            ->setCellValue('I4', 'ชื่อเดือนอังกฤษ')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['MONTH_SEQ_P1'] != $Result[$i]['MONTH_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['MONTH_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['MONTH_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['MONTH_NAME_TH_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['MONTH_NAME_EN_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['MONTH_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['MONTH_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['MONTH_NAME_TH_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['MONTH_NAME_EN_P2']);

                    if(empty($Result[$i]['MONTH_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลเดือน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['MONTH_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลเดือน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['MONTH_ID_P1'] != $Result[$i]['MONTH_ID_P1']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['MONTH_NAME_TH_P1'] != $Result[$i]['MONTH_NAME_TH_P2']) $problemDesc  = $problemDesc . "ชื่อเดือนไทย ไม่ตรงกัน ";
                        if($Result[$i]['MONTH_NAME_EN_P1'] != $Result[$i]['MONTH_NAME_EN_P2']) $problemDesc  = $problemDesc . "ชื่อเดือนอังกฤษ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['MONTH_SEQ_P1'] == $Result[$i]['MONTH_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['MONTH_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['MONTH_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['MONTH_NAME_TH_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['MONTH_NAME_EN_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['MONTH_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['MONTH_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['MONTH_NAME_TH_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['MONTH_NAME_EN_P2']);

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '4' : //ข้อมูลเวลา
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลเวลา ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลเวลา ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลเวลา ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลเวลา');
            $bn = 'รายการข้อมูลเวลา';
            $fileName = date("Y/m/d") . '-รายการข้อมูลเวลา';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อช่วงเวลา')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อช่วงเวลา')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['TIME_RANG_SEQ_P1'] != $Result[$i]['TIME_RANG_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TIME_RANG_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TIME_RANG_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TIME_RANG_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TIME_RANG_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TIME_RANG_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TIME_RANG_NAME_P2']);


                    if(empty($Result[$i]['TIME_RANG_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลเวลา ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['TIME_RANG_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลเวลา ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['TIME_RANG_ID_P1'] != $Result[$i]['TIME_RANG_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['TIME_RANG_NAME_P1'] != $Result[$i]['TIME_RANG_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อช่วงเวลา ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['TIME_RANG_SEQ_P1'] == $Result[$i]['TIME_RANG_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TIME_RANG_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TIME_RANG_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TIME_RANG_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TIME_RANG_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TIME_RANG_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TIME_RANG_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;





}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
