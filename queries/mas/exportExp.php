<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'queryEXPData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}



switch ($checknum){
    case '1' : //ข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย');
            $bn = 'รายการข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if(substr($Result[$i]['ILLEGALITY_TYPE_NAME_P1'],0,10) != substr($Result[$i]['ILLEGALITY_TYPE_NAME_P2'],0,10) ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['ILLEGALITY_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['ILLEGALITY_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['ILLEGALITY_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['ILLEGALITY_TYPE_NAME_P2']);

                    if(empty($Result[$i]['ILLEGALITY_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหนังสือ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['ILLEGALITY_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหนังสือ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['ILLEGALITY_TYPE_NAME_P1'] != $Result[$i]['ILLEGALITY_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทหนังสือ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if(substr($Result[$i]['ILLEGALITY_TYPE_NAME_P1'],0,10) == substr($Result[$i]['ILLEGALITY_TYPE_NAME_P2'],0,10) ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['ILLEGALITY_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['ILLEGALITY_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['ILLEGALITY_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['ILLEGALITY_TYPE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //ข้อมูลประเภทงาน
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทงาน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทงาน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทงาน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทงาน');
            $bn = 'รายการข้อมูลประเภทงาน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทงาน';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทงาน')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทงาน')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['JOB_GROUP_SEQ_P1'] != $Result[$i]['JOB_GROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['JOB_GROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['JOB_GROUP_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['JOB_GROUP_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['JOB_GROUP_NAME_P2']);

                    if(empty($Result[$i]['JOB_GROUP_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทงาน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['JOB_GROUP_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทงาน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['JOB_GROUP_NAME_P1'] != $Result[$i]['JOB_GROUP_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทงาน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['JOB_GROUP_SEQ_P1'] == $Result[$i]['JOB_GROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['JOB_GROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['JOB_GROUP_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['JOB_GROUP_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['JOB_GROUP_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //ข้อมูลขั้นตอนการดำเนินการ
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลขั้นตอนการดำเนินการ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลขั้นตอนการดำเนินการ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลขั้นตอนการดำเนินการ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลขั้นตอนการดำเนินการ');
            $bn = 'รายการข้อมูลขั้นตอนการดำเนินการ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลขั้นตอนการดำเนินการ';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อขั้นตอนการดำเนินการ')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อขั้นตอนการดำเนินการ')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['PROC_TYPE_SEQ_P1'] != $Result[$i]['PROC_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PROC_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PROC_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PROC_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PROC_TYPE_NAME_P2']);

                    if(empty($Result[$i]['PROC_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลขั้นตอนการดำเนินการ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['PROC_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลขั้นตอนการดำเนินการ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['PROC_TYPE_NAME_P1'] != $Result[$i]['PROC_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อขั้นตอนการดำเนินการ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['PROC_TYPE_SEQ_P1'] == $Result[$i]['PROC_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PROC_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PROC_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PROC_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PROC_TYPE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '4' : //ข้อมูลประเภทการร้องเรียน
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทการร้องเรียน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทการร้องเรียน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทการร้องเรียน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทการร้องเรียน');
            $bn = 'รายการข้อมูลประเภทการร้องเรียน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทการร้องเรียน';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทการร้องเรียน')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทการร้องเรียน')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['REQUEST_NAME_P1'] != $Result[$i]['REQUEST_NAME_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['REQUEST_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['REQUEST_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['REQUEST_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['REQUEST_NAME_P2']);

                    if(empty($Result[$i]['REQUEST_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทการร้องเรียน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['REQUEST_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทการร้องเรียน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['REQUEST_NAME_P1'] != $Result[$i]['REQUEST_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทการร้องเรียน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['REQUEST_NAME_P1'] == $Result[$i]['REQUEST_NAME_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['REQUEST_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['REQUEST_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['REQUEST_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['REQUEST_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '5' : //ข้อมูลเรื่อง
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลเรื่อง ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลเรื่อง ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลเรื่อง ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลเรื่อง');
            $bn = 'รายการข้อมูลเรื่อง';
            $fileName = date("Y/m/d") . '-รายการข้อมูลเรื่อง';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อเรื่อง')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อเรื่อง')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['SUB_JOB_SEQ_P1'] != $Result[$i]['SUB_JOB_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['SUB_JOB_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['SUB_JOB_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['SUB_JOB_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['SUB_JOB_NAME_P2']);

                    if(empty($Result[$i]['SUB_JOB_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลเรื่อง ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['SUB_JOB_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลเรื่อง ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['SUB_JOB_NAME_P1'] != $Result[$i]['SUB_JOB_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อเรื่อง ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['SUB_JOB_SEQ_P1'] == $Result[$i]['SUB_JOB_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['SUB_JOB_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['SUB_JOB_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['SUB_JOB_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['SUB_JOB_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '6' : //ข้อมูลประเภทการขอใช้ประโยชน์
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทการขอใช้ประโยชน์ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทการขอใช้ประโยชน์ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทการขอใช้ประโยชน์ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทการขอใช้ประโยชน์');
            $bn = 'รายการข้อมูลประเภทการขอใช้ประโยชน์';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทการขอใช้ประโยชน์';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทการขอใช้ประโยชน์')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทการขอใช้ประโยชน์')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['USE_TYPE_NAME_P1'] != $Result[$i]['USE_TYPE_NAME_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['USE_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['USE_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['USE_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['USE_TYPE_NAME_P2']);

                    if(empty($Result[$i]['USE_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทการขอใช้ประโยชน์ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['USE_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทการขอใช้ประโยชน์ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['USE_TYPE_NAME_P1'] != $Result[$i]['USE_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทการขอใช้ประโยชน์ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['USE_TYPE_NAME_P1'] == $Result[$i]['USE_TYPE_NAME_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['USE_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['USE_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['USE_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['USE_TYPE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '7' : //ข้อมูลสถานะงาน
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสถานะงาน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลสถานะงาน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสถานะงาน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสถานะงาน');
            $bn = 'รายการข้อมูลสถานะงาน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสถานะงาน';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อสถานะงาน')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อสถานะงาน')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['WORK_STS_SEQ_P1'] != $Result[$i]['WORK_STS_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['WORK_STS_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['WORK_STS_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['WORK_STS_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['WORK_STS_NAME_P2']);

                    if(empty($Result[$i]['WORK_STS_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสถานะงาน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['WORK_STS_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสถานะงาน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['WORK_STS_NAME_P1'] != $Result[$i]['WORK_STS_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อสถานะงาน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['WORK_STS_SEQ_P1'] == $Result[$i]['WORK_STS_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['WORK_STS_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['WORK_STS_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['WORK_STS_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['WORK_STS_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

   



}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
