<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'queryFINData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}



switch ($checknum){
    case '1' : //การตั้งค่าในระบบ
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลการตั้งค่าในระบบ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลการตั้งค่าในระบบ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลการตั้งค่าในระบบ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลการตั้งค่าในระบบ');
            $bn = 'รายการข้อมูลการตั้งค่าในระบบ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลการตั้งค่าในระบบ';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'ID')
            ->setCellValue('C4', 'ชื่อการตั้งค่าในระบบ')       
            ->setCellValue('D4', 'ID')
            ->setCellValue('E4', 'ชื่อการตั้งค่าในระบบ')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['CONFIG_NO_P1'] != $Result[$i]['CONFIG_NO_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['CONFIG_NO_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['CONFIG_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['CONFIG_NO_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['CONFIG_NAME_P2']);

                    if(empty($Result[$i]['CONFIG_NO_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลการตั้งค่าในระบบ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['CONFIG_NO_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลการตั้งค่าในระบบ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['CONFIG_NAME_P1'] != $Result[$i]['CONFIG_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อการตั้งค่าในระบบ ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['CONFIG_NO_P1'] == $Result[$i]['CONFIG_NO_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['CONFIG_NO_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['CONFIG_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['CONFIG_NO_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['CONFIG_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //ประเภทการเบิก
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทการเบิก ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทการเบิก ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทการเบิก ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทการเบิก');
            $bn = 'รายการข้อมูลประเภทการเบิก';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทการเบิก';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'ID')
            ->setCellValue('C4', 'ชื่อประเภทการเบิก')       
            ->setCellValue('D4', 'ID')
            ->setCellValue('E4', 'ชื่อประเภทการเบิก')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['FUND_TYPE_ID_P1'] != $Result[$i]['FUND_TYPE_ID_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['FUND_TYPE_ID_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['FUND_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['FUND_TYPE_ID_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['FUND_NAME_P2']);

                    if(empty($Result[$i]['FUND_TYPE_ID_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทการเบิก ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";

                    }else if(empty($Result[$i]['FUND_TYPE_ID_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทการเบิก ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['FUND_NAME_P1'] != $Result[$i]['FUND_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทการเบิก ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['FUND_TYPE_ID_P1'] == $Result[$i]['FUND_TYPE_ID_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['FUND_TYPE_ID_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['FUND_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['FUND_TYPE_ID_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['FUND_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //หมวดหมู่เงิน
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหมวดหมู่เงิน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลหมวดหมู่เงิน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหมวดหมู่เงิน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหมวดหมู่เงิน');
            $bn = 'รายการข้อมูลหมวดหมู่เงิน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหมวดหมู่เงิน';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'ID')
            ->setCellValue('C4', 'รหัสหมวดหมู่เงิน')       
            ->setCellValue('D4', 'ชื่อหมวดหมู่เงิน')
            
            ->setCellValue('E4', 'ID')
            ->setCellValue('F4', 'รหัสหมวดหมู่เงิน')       
            ->setCellValue('G4', 'ชื่อหมวดหมู่เงิน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['INCOME_ID_P1'] != $Result[$i]['INCOME_ID_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['INCOME_ID_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['INCOME_CODE_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['INCOME_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['INCOME_ID_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['INCOME_CODE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['INCOME_NAME_P2']);


                    if(empty($Result[$i]['INCOME_ID_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหมวดหมู่เงิน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";

                    }else if(empty($Result[$i]['INCOME_ID_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหมวดหมู่เงิน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['INCOME_CODE_P1'] != $Result[$i]['INCOME_CODE_P2']) $problemDesc  = $problemDesc . "รหัสหมวดหมู่เงิน ไม่ตรงกัน ";
                        if($Result[$i]['INCOME_NAME_P1'] != $Result[$i]['INCOME_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อหมวดหมู่เงิน ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['INCOME_ID_P1'] == $Result[$i]['INCOME_ID_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['INCOME_ID_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['INCOME_CODE_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['INCOME_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['INCOME_ID_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['INCOME_CODE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['INCOME_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '4' : //รายได้
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลรายได้ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลรายได้ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลรายได้ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลรายได้');
            $bn = 'รายการข้อมูลรายได้';
            $fileName = date("Y/m/d") . '-รายการข้อมูลรายได้';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'ID')
            ->setCellValue('C4', 'รหัสรายได้')       
            ->setCellValue('D4', 'ชื่อรายได้')
            
            ->setCellValue('E4', 'ID')
            ->setCellValue('F4', 'รหัสรายได้')       
            ->setCellValue('G4', 'ชื่อรายได้')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['REVENUE_ID_P1'] != $Result[$i]['REVENUE_ID_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['REVENUE_ID_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['REVENUE_CODE_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['REVENUE_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['REVENUE_ID_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['REVENUE_CODE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['REVENUE_NAME_P2']);


                    if(empty($Result[$i]['REVENUE_ID_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลรายได้ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";

                    }else if(empty($Result[$i]['REVENUE_ID_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลรายได้ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['REVENUE_CODE_P1'] != $Result[$i]['REVENUE_CODE_P2']) $problemDesc  = $problemDesc . "รหัสรายได้ ไม่ตรงกัน ";
                        if($Result[$i]['REVENUE_NAME_P1'] != $Result[$i]['REVENUE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อรายได้ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['REVENUE_ID_P1'] == $Result[$i]['REVENUE_ID_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['REVENUE_ID_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['REVENUE_CODE_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['REVENUE_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['REVENUE_ID_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['REVENUE_CODE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['REVENUE_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '5' : //ชื่อประเภทการจัดเก็บ
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการชื่อประเภทการจัดเก็บ ที่แตกต่างกัน' );
            $bn = 'รายการชื่อประเภทการจัดเก็บ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการชื่อประเภทการจัดเก็บ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการชื่อประเภทการจัดเก็บ');
            $bn = 'รายการชื่อประเภทการจัดเก็บ';
            $fileName = date("Y/m/d") . '-รายการชื่อประเภทการจัดเก็บ';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'ID')
            ->setCellValue('C4', 'ชื่อย่อ')       
            ->setCellValue('D4', 'ชื่อประเภทการจัดเก็บ')
            
            ->setCellValue('E4', 'ID')
            ->setCellValue('F4', 'ชื่อย่อ')       
            ->setCellValue('G4', 'ชื่อประเภทการจัดเก็บ')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['STORE_ID_P1'] != $Result[$i]['STORE_ID_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['STORE_ID_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['STORE_ABBR_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['STORE_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['STORE_ID_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['STORE_ABBR_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['STORE_NAME_P2']);


                    if(empty($Result[$i]['STORE_ID_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลชื่อประเภทการจัดเก็บ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";

                    }else if(empty($Result[$i]['STORE_ID_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลชื่อประเภทการจัดเก็บ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['STORE_ABBR_NAME_P1'] != $Result[$i]['STORE_ABBR_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อย่อ ไม่ตรงกัน ";
                        if($Result[$i]['STORE_NAME_P1'] != $Result[$i]['STORE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทการจัดเก็บ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['STORE_ID_P1'] == $Result[$i]['STORE_ID_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['STORE_ID_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['STORE_ABBR_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['STORE_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['STORE_ID_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['STORE_ABBR_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['STORE_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    


}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
