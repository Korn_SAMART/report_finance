<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'queryHRMData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}



switch ($checknum){
    case '1' : //ข้อมูลตำแหน่ง
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลตำแหน่ง ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลตำแหน่ง ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลตำแหน่ง ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลตำแหน่ง');
            $bn = 'รายการข้อมูลตำแหน่ง';
            $fileName = date("Y/m/d") . '-รายการข้อมูลตำแหน่ง';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อตำแหน่ง')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อตำแหน่ง')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['DIV_POSITION_SEQ_P1'] != $Result[$i]['DIV_POSITION_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['DIV_POSITION_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['DIV_POSITION_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['DIV_POSITION_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['DIV_POSITION_NAME_P2']);

                    if(empty($Result[$i]['DIV_POSITION_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลตำแหน่ง ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['DIV_POSITION_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลตำแหน่ง ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['DIV_POSITION_NAME_P1'] != $Result[$i]['DIV_POSITION_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อตำแหน่ง ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['DIV_POSITION_SEQ_P1'] == $Result[$i]['DIV_POSITION_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['DIV_POSITION_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['DIV_POSITION_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['DIV_POSITION_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['DIV_POSITION_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //ประเภทบุคลากร
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทบุคลากร ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทบุคลากร ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทบุคลากร ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทบุคลากร');
            $bn = 'รายการข้อมูลประเภทบุคลากร';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทบุคลากร';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทบุคลากร')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทบุคลากร')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['PSN_TYPE_SEQ_P1'] != $Result[$i]['PSN_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PSN_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PSN_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PSN_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PSN_TYPE_NAME_P2']);

                    if(empty($Result[$i]['PSN_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทบุคลากร ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['PSN_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทบุคลากร ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['PSN_TYPE_NAME_P1'] != $Result[$i]['PSN_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทบุคลากร ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['PSN_TYPE_SEQ_P1'] == $Result[$i]['PSN_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PSN_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PSN_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PSN_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PSN_TYPE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //สิทธิการลา
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสิทธิการลา ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลสิทธิการลา ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสิทธิการลา ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสิทธิการลา');
            $bn = 'รายการข้อมูลสิทธิการลา';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสิทธิการลา';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทการลา')       
            ->setCellValue('D4', 'ประเภทบุคลากร')
            ->setCellValue('E4', 'จำนวนที่ลาได้')

            ->setCellValue('F4', 'SEQ')
            ->setCellValue('G4', 'ชื่อประเภทการลา')
            ->setCellValue('H4', 'ประเภทบุคลากร')
            ->setCellValue('I4', 'จำนวนที่ลาได้')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['LEAVE_RIGHT_SEQ_P1'] != $Result[$i]['LEAVE_RIGHT_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LEAVE_RIGHT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LEAVE_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PSN_TYPE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LEAVE_AMOUNT_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['LEAVE_RIGHT_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LEAVE_TYPE_NAME_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['PSN_TYPE_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['LEAVE_AMOUNT_P2']);

                    if(empty($Result[$i]['LEAVE_RIGHT_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสิทธิการลา ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['LEAVE_RIGHT_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสิทธิการลา ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['LEAVE_TYPE_NAME_P1'] != $Result[$i]['LEAVE_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทการลา ไม่ตรงกัน ";
                        if($Result[$i]['PSN_TYPE_NAME_P1'] != $Result[$i]['PSN_TYPE_NAME_P1']) $problemDesc  = $problemDesc . "ประเภทบุคลากร ไม่ตรงกัน ";
                        if($Result[$i]['LEAVE_AMOUNT_P1'] != $Result[$i]['LEAVE_AMOUNT_P2']) $problemDesc  = $problemDesc . "จำนวนที่ลาได้ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['LEAVE_RIGHT_SEQ_P1'] == $Result[$i]['LEAVE_RIGHT_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LEAVE_RIGHT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LEAVE_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PSN_TYPE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LEAVE_AMOUNT_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['LEAVE_RIGHT_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LEAVE_TYPE_NAME_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['PSN_TYPE_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['LEAVE_AMOUNT_P2']);

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;



}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
