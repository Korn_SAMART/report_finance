<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'queryINVData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}



switch ($checknum){
    case '1' : //ประเภทครุภัณฑ์
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทครุภัณฑ์ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทครุภัณฑ์ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทครุภัณฑ์ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทครุภัณฑ์');
            $bn = 'รายการข้อมูลประเภทครุภัณฑ์';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทครุภัณฑ์';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อประเภทครุภัณฑ์')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อประเภทครุภัณฑ์')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['TYPE_ASSET_SEQ_P1'] != $Result[$i]['TYPE_ASSET_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TYPE_ASSET_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TYPE_ASSET_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TYPE_ASSET_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TYPE_ASSET_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TYPE_ASSET_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TYPE_ASSET_NAME_P2']);


                    if(empty($Result[$i]['TYPE_ASSET_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทครุภัณฑ์ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";

                        

                    }else if(empty($Result[$i]['TYPE_ASSET_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทครุภัณฑ์ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['TYPE_ASSET_ID_P1'] != $Result[$i]['TYPE_ASSET_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['TYPE_ASSET_NAME_P1'] != $Result[$i]['TYPE_ASSET_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทครุภัณฑ์ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['TYPE_ASSET_SEQ_P1'] == $Result[$i]['TYPE_ASSET_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TYPE_ASSET_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TYPE_ASSET_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TYPE_ASSET_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TYPE_ASSET_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TYPE_ASSET_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TYPE_ASSET_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //ประเภทและชื่อวัสดุ
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทและชื่อวัสดุ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทและชื่อวัสดุ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทและชื่อวัสดุ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทและชื่อวัสดุ');
            $bn = 'รายการข้อมูลประเภทและชื่อวัสดุ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทและชื่อวัสดุ';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทและชื่อวัสดุ')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทและชื่อวัสดุ')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['MAT_SEQ_P1'] != $Result[$i]['MAT_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['MAT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['MAT_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['MAT_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['MAT_NAME_P2']);

                    if(empty($Result[$i]['MAT_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทและชื่อวัสดุ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['MAT_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทและชื่อวัสดุ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['MAT_NAME_P1'] != $Result[$i]['MAT_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทและชื่อวัสดุ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['MAT_SEQ_P1'] == $Result[$i]['MAT_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['MAT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['MAT_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['MAT_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['MAT_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //ประเภทและชื่อแบบพิมพ์
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทและชื่อแบบพิมพ์ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทและชื่อแบบพิมพ์ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทและชื่อแบบพิมพ์ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทและชื่อแบบพิมพ์');
            $bn = 'รายการข้อมูลประเภทและชื่อแบบพิมพ์';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทและชื่อแบบพิมพ์';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อย่อ')       
            ->setCellValue('D4', 'ชื่อแบบพิมพ์')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ชื่อย่อ')       
            ->setCellValue('G4', 'ชื่อแบบพิมพ์')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['PLATE_SEQ_P1'] != $Result[$i]['PLATE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PLATE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PLATE_ABBR_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PLATE_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PLATE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PLATE_ABBR_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['PLATE_NAME_P2']);


                    if(empty($Result[$i]['PLATE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทและชื่อแบบพิมพ์ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['PLATE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทและชื่อแบบพิมพ์ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['PLATE_ABBR_NAME_P1'] != $Result[$i]['PLATE_ABBR_NAME_P1']) $problemDesc  = $problemDesc . "ชื่อย่อ ไม่ตรงกัน ";
                        if($Result[$i]['PLATE_NAME_P1'] != $Result[$i]['PLATE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อแบบพิมพ์ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['PLATE_SEQ_P1'] == $Result[$i]['PLATE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PLATE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PLATE_ABBR_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PLATE_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PLATE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PLATE_ABBR_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['PLATE_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '4' : //ประเภทหลักเขต
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทหลักเขต ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทหลักเขต ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทหลักเขต ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทหลักเขต');
            $bn = 'รายการข้อมูลประเภทหลักเขต';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทหลักเขต';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทหลักเขต')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทหลักเขต')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['PIECE_SEQ_P1'] != $Result[$i]['PIECE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PIECE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PIECE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PIECE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PIECE_NAME_P2']);

                    if(empty($Result[$i]['PIECE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหลักเขต ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['PIECE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหลักเขต ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['PIECE_NAME_P1'] != $Result[$i]['PIECE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทหลักเขต ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['PIECE_SEQ_P1'] == $Result[$i]['PIECE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PIECE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PIECE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PIECE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PIECE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);

    break;

    case '5' : //ประเภทชื่ออาคารและสิ่งก่อสร้าง
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทชื่ออาคารและสิ่งก่อสร้าง ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทชื่ออาคารและสิ่งก่อสร้าง ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทชื่ออาคารและสิ่งก่อสร้าง ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทชื่ออาคารและสิ่งก่อสร้าง');
            $bn = 'รายการข้อมูลประเภทชื่ออาคารและสิ่งก่อสร้าง';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทชื่ออาคารและสิ่งก่อสร้าง';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่ออาคารและสิ่งก่อสร้าง')       
            ->setCellValue('D4', 'อายุการใช้งาน')
            ->setCellValue('E4', 'อัตราร้อยละต่อปี')

            ->setCellValue('F4', 'SEQ')
            ->setCellValue('G4', 'ชื่ออาคารและสิ่งก่อสร้าง')
            ->setCellValue('H4', 'อายุการใช้งาน')
            ->setCellValue('I4', 'อัตราร้อยละต่อปี')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['BLD_SEQ_P1'] != $Result[$i]['BLD_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['BLD_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['BLD_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['BLD_YEAR_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['BLD_RATE_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['BLD_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['BLD_NAME_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['BLD_YEAR_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['BLD_RATE_P2']);

                    if(empty($Result[$i]['BLD_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทชื่ออาคารและสิ่งก่อสร้าง ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['BLD_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทชื่ออาคารและสิ่งก่อสร้าง ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['BLD_NAME_P1'] != $Result[$i]['BLD_NAME_P2']) $problemDesc  = $problemDesc . "ชื่ออาคารและสิ่งก่อสร้าง ไม่ตรงกัน ";
                        if($Result[$i]['BLD_YEAR_P1'] != $Result[$i]['BLD_YEAR_P2']) $problemDesc  = $problemDesc . "อายุการใช้งาน ไม่ตรงกัน ";
                        if($Result[$i]['BLD_RATE_P1'] != $Result[$i]['BLD_RATE_P2']) $problemDesc  = $problemDesc . "อัตราร้อยละต่อปี ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['BLD_SEQ_P1'] == $Result[$i]['BLD_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['BLD_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['BLD_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['BLD_YEAR_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['BLD_RATE_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['BLD_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['BLD_NAME_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['BLD_YEAR_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['BLD_RATE_P2']);

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;



}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
