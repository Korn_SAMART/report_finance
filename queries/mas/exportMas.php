<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'queryMASData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}



switch ($checknum){
    case '6' : //ข้อมูลสถาบันการเงิน
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสถาบันการเงิน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลสถาบันการเงิน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสถาบันการเงิน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสถาบันการเงิน');
            $bn = 'รายการข้อมูลสถาบันการเงิน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสถาบันการเงิน';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อสถาบันการเงิน	')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อสถาบันการเงิน	')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['FINANCE_SEQ_P1'] != $Result[$i]['FINANCE_SEQ_P2'] ){

                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['FINANCE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['FINANCE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['FINANCE_THAI_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['FINANCE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['FINANCE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['FINANCE_THAI_NAME_P2']);

                    if(empty($Result[$i]['FINANCE_SEQ_P1'])){
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสถาบันการเงิน ในพัฒน์ฯ 1 ";

                    }else if(empty($Result[$i]['FINANCE_SEQ_P2'])){
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสถาบันการเงิน ในพัฒน์ฯ 2 ";

                    }else{
                        if($Result[$i]['FINANCE_SEQ_P1'] != $Result[$i]['FINANCE_SEQ_P2']) $problemDesc  = $problemDesc . "ชื่อการใช้ประโยชน์ที่ดิน ไม่ตรงกัน \n";
                        if($Result[$i]['FINANCE_ID_P1'] != $Result[$i]['FINANCE_ID_P2']) $problemDesc  = $problemDesc . "ชื่อการใช้ประโยชน์ที่ดิน ไม่ตรงกัน \n";
                        if($Result[$i]['FINANCE_THAI_NAME_P1'] != $Result[$i]['FINANCE_THAI_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อการใช้ประโยชน์ที่ดิน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['FINANCE_SEQ_P1'] == $Result[$i]['FINANCE_SEQ_P2'] ){

                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['FINANCE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['FINANCE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['FINANCE_THAI_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['FINANCE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['FINANCE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['FINANCE_THAI_NAME_P2']);


                    // if(empty($Result[$i]['FINANCE_SEQ_P1'])){
                    //     $problemDesc  = $problemDesc . "ไม่พบข้อมูล การใช้ประโยชน์ในที่ดิน ในพัฒน์ฯ 1 ";

                    // }else if(empty($Result[$i]['FINANCE_SEQ_P2'])){
                    //     $problemDesc  = $problemDesc . "ไม่พบข้อมูล การใช้ประโยชน์ในที่ดิน ในพัฒน์ฯ 2 ";

                    // }else{
                    //     if($Result[$i]['FINANCE_SEQ_P1'] != $Result[$i]['FINANCE_SEQ_P2']) $problemDesc  = $problemDesc . "ชื่อการใช้ประโยชน์ที่ดิน ไม่ตรงกัน \n";
                    //     if($Result[$i]['FINANCE_ID_P1'] != $Result[$i]['FINANCE_ID_P2']) $problemDesc  = $problemDesc . "ชื่อการใช้ประโยชน์ที่ดิน ไม่ตรงกัน \n";
                    //     if($Result[$i]['FINANCE_THAI_NAME_P1'] != $Result[$i]['FINANCE_THAI_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อการใช้ประโยชน์ที่ดิน ไม่ตรงกัน ";

                    // }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+5))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '7' : //ข้อมูลการใช้ประโยชน์ในที่ดิน
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลการใช้ประโยชน์ในที่ดิน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลการใช้ประโยชน์ในที่ดิน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลการใช้ประโยชน์ในที่ดิน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลการใช้ประโยชน์ในที่ดิน');
            $bn = 'รายการข้อมูลการใช้ประโยชน์ในที่ดิน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลการใช้ประโยชน์ในที่ดิน';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อการใช้ประโยชน์ที่ดิน')
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อการใช้ประโยชน์ที่ดิน')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['LANDUSED_SEQ_P1'] != $Result[$i]['LANDUSED_SEQ_P2'] || $Result[$i]['LANDUSED_NAME_P1'] != $Result[$i]['LANDUSED_NAME_P2']){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDUSED_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LANDUSED_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDUSED_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDUSED_NAME_P2']);

                    if(empty($Result[$i]['LANDUSED_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลการใช้ประโยชน์ในที่ดิน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";

                    }else if(empty($Result[$i]['LANDUSED_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลการใช้ประโยชน์ในที่ดิน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['LANDUSED_NAME_P1'] != $Result[$i]['LANDUSED_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อการใช้ประโยชน์ที่ดิน ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['LANDUSED_SEQ_P1'] == $Result[$i]['LANDUSED_SEQ_P2'] || $Result[$i]['LANDUSED_NAME_P1'] == $Result[$i]['LANDUSED_NAME_P2']){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDUSED_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LANDUSED_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDUSED_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDUSED_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+5))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+5))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '14' : //ข้อมูลหมวดสถาบันการเงิน
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหมวดสถาบันการเงิน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลหมวดสถาบันการเงิน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหมวดสถาบันการเงิน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหมวดสถาบันการเงิน');
            $bn = 'รายการข้อมูลหมวดสถาบันการเงิน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหมวดสถาบันการเงิน';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อหมวดสถาบันการเงิน')
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อหมวดสถาบันการเงิน')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['FINANCE_TYPE_SEQ_P1'] != $Result[$i]['FINANCE_TYPE_SEQ_P2']){

                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['FINANCE_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['FINANCE_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['FINANCE_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['FINANCE_TYPE_NAME_P2']);

                    if(empty($Result[$i]['FINANCE_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหมวดสถาบันการเงิน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['FINANCE_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหมวดสถาบันการเงิน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['FINANCE_TYPE_NAME_P1'] != $Result[$i]['FINANCE_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อหมวดสถาบันการเงิน ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;

                }
            }
            else{
                if($Result[$i]['FINANCE_TYPE_SEQ_P1'] == $Result[$i]['FINANCE_TYPE_SEQ_P2'] || $Result[$i]['FINANCE_TYPE_NAME_P1'] == $Result[$i]['FINANCE_TYPE_NAME_P2']){

                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['FINANCE_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['FINANCE_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['FINANCE_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['FINANCE_TYPE_NAME_P2']);


                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;

                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '15' : //ข้อมูลประเภทศาล
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทศาล ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทศาล ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทศาล ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทศาล');
            $bn = 'รายการข้อมูลประเภทศาล';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทศาล';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อประเภทศาล')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อประเภทศาล')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['COURT_TYPE_SEQ_P1'] != $Result[$i]['COURT_TYPE_SEQ_P2'] ){
                    
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['COURT_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['COURT_TYPE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['COURT_TYPE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['COURT_TYPE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['COURT_TYPE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['COURT_TYPE_NAME_P2']);

                    if(empty($Result[$i]['COURT_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทศาล ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['COURT_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทศาล ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['COURT_TYPE_ID_P1'] != $Result[$i]['COURT_TYPE_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน \n";
                        if($Result[$i]['COURT_TYPE_NAME_P1'] != $Result[$i]['COURT_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทศาล ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['COURT_TYPE_SEQ_P1'] == $Result[$i]['COURT_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['COURT_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['COURT_TYPE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['COURT_TYPE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['COURT_TYPE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['COURT_TYPE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['COURT_TYPE_NAME_P2']);

                    // if(empty($Result[$i]['COURT_TYPE_SEQ_P1'])){
                    //     $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทศาล ในพัฒน์ฯ 1 ";

                    // }else if(empty($Result[$i]['COURT_TYPE_SEQ_P2'])){
                    //     $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทศาล ในพัฒน์ฯ 2 ";

                    // }else{
                    //     if($Result[$i]['COURT_TYPE_ID_P1'] != $Result[$i]['COURT_TYPE_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                    //     if($Result[$i]['COURT_TYPE_NAME_P1'] != $Result[$i]['COURT_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทศาล ไม่ตรงกัน ";

                    // }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '11' : //ข้อมูลชื่อศาล
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลชื่อศาล ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลชื่อศาล ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลชื่อศาล ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลชื่อศาล');
            $bn = 'รายการข้อมูลชื่อศาล';
            $fileName = date("Y/m/d") . '-รายการข้อมูลชื่อศาล';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อศาล')
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อศาล')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['COURT_SEQ_P1'] != $Result[$i]['COURT_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['COURT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['COURT_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['COURT_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['COURT_NAME_P2']);

                    if(empty($Result[$i]['COURT_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลชื่อศาล ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['COURT_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลชื่อศาล ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['COURT_NAME_P1'] != $Result[$i]['COURT_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อศาล ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['COURT_SEQ_P1'] == $Result[$i]['COURT_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['COURT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['COURT_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['COURT_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['COURT_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '16' : //ข้อมูลสัญชาติ
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสัญชาติ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลสัญชาติ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสัญชาติ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสัญชาติ');
            $bn = 'รายการข้อมูลสัญชาติ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสัญชาติ';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อสัญชาติ')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อสัญชาติ')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['NATIONALITY_SEQ_P1'] != $Result[$i]['NATIONALITY_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['NATIONALITY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['NATIONALITY_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['NATIONALITY_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['NATIONALITY_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['NATIONALITY_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['NATIONALITY_NAME_P2']);

                    if(empty($Result[$i]['NATIONALITY_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสัญชาติ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['NATIONALITY_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสัญชาติ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['NATIONALITY_ID_P1'] != $Result[$i]['NATIONALITY_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['NATIONALITY_NAME_P1'] != $Result[$i]['NATIONALITY_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อสัญชาติ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['NATIONALITY_SEQ_P1'] == $Result[$i]['NATIONALITY_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['NATIONALITY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['NATIONALITY_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['NATIONALITY_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['NATIONALITY_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['NATIONALITY_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['NATIONALITY_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '17' : //ข้อมูลเชื้อชาติ
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลเชื้อชาติ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลเชื้อชาติ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลเชื้อชาติ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลเชื้อชาติ');
            $bn = 'รายการข้อมูลเชื้อชาติ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลเชื้อชาติ';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อเชื้อชาติ')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อเชื้อชาติ')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['RACE_SEQ_P1'] != $Result[$i]['RACE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['RACE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['RACE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['RACE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['RACE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['RACE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['RACE_NAME_P2']);

                    if(empty($Result[$i]['RACE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสัญชาติ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['RACE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสัญชาติ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['RACE_ID_P1'] != $Result[$i]['RACE_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['RACE_NAME_P1'] != $Result[$i]['RACE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อเชื้อชาติ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['RACE_SEQ_P1'] == $Result[$i]['RACE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['RACE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['RACE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['RACE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['RACE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['RACE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['RACE_NAME_P2']);


                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '18' : //ข้อมูลศาสนา
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลศาสนา ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลศาสนา ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลศาสนา ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลศาสนา');
            $bn = 'รายการข้อมูลศาสนา';
            $fileName = date("Y/m/d") . '-รายการข้อมูลศาสนา';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อศาสนา')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อศาสนา')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['RELIGION_SEQ_P1'] != $Result[$i]['RELIGION_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['RELIGION_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['RELIGION_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['RELIGION_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['RELIGION_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['RELIGION_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['RELIGION_NAME_P2']);

                    if(empty($Result[$i]['RELIGION_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลศาสนา ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['RELIGION_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลศาสนา ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['RELIGION_ID_P1'] != $Result[$i]['RELIGION_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['RELIGION_NAME_P1'] != $Result[$i]['RELIGION_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อศาสนา ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['RELIGION_SEQ_P1'] == $Result[$i]['RELIGION_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['RELIGION_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['RELIGION_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['RELIGION_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['RELIGION_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['RELIGION_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['RELIGION_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '10' : //ข้อมูลคำนำหน้าชื่อ
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลคำนำหน้าชื่อ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลคำนำหน้าชื่อ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลคำนำหน้าชื่อ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลคำนำหน้าชื่อ');
            $bn = 'รายการข้อมูลคำนำหน้าชื่อ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลคำนำหน้าชื่อ';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อย่อคำนำหน้าชื่อ')       
            ->setCellValue('D4', 'คำนำหน้าชื่อ')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ชื่อย่อคำนำหน้าชื่อ')
            ->setCellValue('G4', 'คำนำหน้าชื่อ')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['TITLE_SEQ_P1'] != $Result[$i]['TITLE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TITLE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TITLE_ABBR_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TITLE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TITLE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TITLE_ABBR_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TITLE_NAME_P2']);

                    if(empty($Result[$i]['TITLE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลคำนำหน้าชื่อ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['TITLE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลคำนำหน้าชื่อ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['RELIGION_ID_P1'] != $Result[$i]['RELIGION_ID_P2']) $problemDesc  = $problemDesc . "ชื่อย่อคำนำหน้าชื่อ ไม่ตรงกัน ";
                        if($Result[$i]['RELIGION_NAME_P1'] != $Result[$i]['RELIGION_NAME_P2']) $problemDesc  = $problemDesc . "คำนำหน้าชื่อ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['TITLE_SEQ_P1'] == $Result[$i]['TITLE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TITLE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TITLE_ABBR_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TITLE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TITLE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TITLE_ABBR_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TITLE_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true)
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

    break;

    case '19' : //ข้อมูลหน่วยวัด
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหน่วยวัด ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลหน่วยวัด ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหน่วยวัด ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหน่วยวัด');
            $bn = 'รายการข้อมูลหน่วยวัด';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหน่วยวัด';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'หน่วยนับ')       
            ->setCellValue('D4', 'ชื่อย่อหน่วยนับ')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'หน่วยนับ')
            ->setCellValue('G4', 'ชื่อย่อหน่วยนับ')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['UNIT_SEQ_P1'] != $Result[$i]['UNIT_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['UNIT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['UNIT_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['UNIT_ABBR_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['UNIT_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['UNIT_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['UNIT_ABBR_P2']);

                    if(empty($Result[$i]['UNIT_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหน่วยวัด ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['UNIT_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหน่วยวัด ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['UNIT_NAME_P1'] != $Result[$i]['UNIT_NAME_P2']) $problemDesc  = $problemDesc . "หน่วยนับ ไม่ตรงกัน ";
                        if($Result[$i]['UNIT_ABBR_P1'] != $Result[$i]['UNIT_ABBR_P2']) $problemDesc  = $problemDesc . "ชื่อย่อหน่วยนับ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['UNIT_SEQ_P1'] == $Result[$i]['UNIT_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['UNIT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['UNIT_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['UNIT_ABBR_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['UNIT_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['UNIT_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['UNIT_ABBR_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '20' : //ข้อมูลกลุ่มประเทศ
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลกลุ่มประเทศ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลกลุ่มประเทศ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลกลุ่มประเทศ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลกลุ่มประเทศ');
            $bn = 'รายการข้อมูลกลุ่มประเทศ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลกลุ่มประเทศ';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อกลุ่มประเทศ')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')
            ->setCellValue('G4', 'ชื่อกลุ่มประเทศ')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['COUNTRY_GROUP_SEQ_P1'] != $Result[$i]['COUNTRY_GROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['COUNTRY_GROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['COUNTRY_GROUP_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['COUNTRY_GROUP_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['COUNTRY_GROUP_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['COUNTRY_GROUP_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['COUNTRY_GROUP_NAME_P2']);

                    if(empty($Result[$i]['COUNTRY_GROUP_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลกลุ่มประเทศ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['COUNTRY_GROUP_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลกลุ่มประเทศ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['COUNTRY_GROUP_ID_P1'] != $Result[$i]['COUNTRY_GROUP_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['COUNTRY_GROUP_NAME_P1'] != $Result[$i]['COUNTRY_GROUP_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อกลุ่มประเทศ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['COUNTRY_GROUP_SEQ_P1'] == $Result[$i]['COUNTRY_GROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['COUNTRY_GROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['COUNTRY_GROUP_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['COUNTRY_GROUP_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['COUNTRY_GROUP_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['COUNTRY_GROUP_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['COUNTRY_GROUP_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '1' : //ข้อมูลชื่อประเทศ
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลชื่อประเทศ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลชื่อประเทศ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลชื่อประเทศ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลชื่อประเทศ');
            $bn = 'รายการข้อมูลชื่อประเทศ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลชื่อประเทศ';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อประเทศ')
            ->setCellValue('E4', 'ชื่อประเทศ(EN)')

            ->setCellValue('F4', 'SEQ')
            ->setCellValue('G4', 'ID')
            ->setCellValue('H4', 'ชื่อประเทศ')
            ->setCellValue('I4', 'ชื่อประเทศ(EN)')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['COUNTRY_SEQ_P1'] != $Result[$i]['COUNTRY_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['COUNTRY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['COUNTRY_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['COUNTRY_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['COUNTRY_NAME_EN_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['COUNTRY_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['COUNTRY_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['COUNTRY_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['COUNTRY_NAME_EN_P2']);

                    if(empty($Result[$i]['COUNTRY_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลชื่อประเทศ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['COUNTRY_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลชื่อประเทศ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['COUNTRY_ID_P1'] != $Result[$i]['COUNTRY_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['COUNTRY_NAME_P1'] != $Result[$i]['COUNTRY_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเทศ ไม่ตรงกัน ";
                        if($Result[$i]['COUNTRY_NAME_EN_P1'] != $Result[$i]['COUNTRY_NAME_EN_P2']) $problemDesc  = $problemDesc . "ชื่อประเทศ(EN) ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['COUNTRY_SEQ_P1'] == $Result[$i]['COUNTRY_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['COUNTRY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['COUNTRY_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['COUNTRY_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['COUNTRY_NAME_EN_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['COUNTRY_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['COUNTRY_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['COUNTRY_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['COUNTRY_NAME_EN_P2']);

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '21' : //ข้อมูลภูมิภาค
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลภูมิภาค ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลภูมิภาค ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลภูมิภาค ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลภูมิภาค');
            $bn = 'รายการข้อมูลภูมิภาค';
            $fileName = date("Y/m/d") . '-รายการข้อมูลภูมิภาค';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อภูมิภาค/ส่วน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')
            ->setCellValue('G4', 'ชื่อภูมิภาค/ส่วน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['REGION_SEQ_P1'] != $Result[$i]['REGION_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['REGION_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['REGION_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['REGION_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['REGION_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['REGION_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['REGION_NAME_P2']);

                    if(empty($Result[$i]['REGION_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลภูมิภาค ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['REGION_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลภูมิภาค ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['REGION_ID_P1'] != $Result[$i]['REGION_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['REGION_NAME_P1'] != $Result[$i]['REGION_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อภูมิภาค/ส่วน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['REGION_SEQ_P1'] == $Result[$i]['REGION_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['REGION_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['REGION_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['REGION_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['REGION_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['REGION_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['REGION_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //ข้อมูลจังหวัด
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลจังหวัด ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลจังหวัด ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลจังหวัด ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลจังหวัด');
            $bn = 'รายการข้อมูลจังหวัด';
            $fileName = date("Y/m/d") . '-รายการข้อมูลจังหวัด';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อจังหวัด')
            ->setCellValue('E4', 'ชื่อย่อจังหวัด')

            ->setCellValue('F4', 'SEQ')
            ->setCellValue('G4', 'ID')
            ->setCellValue('H4', 'ชื่อจังหวัด')
            ->setCellValue('I4', 'ชื่อย่อจังหวัด')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['PROVINCE_SEQ_P1'] != $Result[$i]['PROVINCE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PROVINCE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PROVINCE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PROVINCE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PROVINCE_ABBR_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PROVINCE_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['PROVINCE_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['PROVINCE_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['PROVINCE_ABBR_P2']);

                    if(empty($Result[$i]['PROVINCE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลจังหวัด ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['PROVINCE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลจังหวัด ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['PROVINCE_ID_P1'] != $Result[$i]['PROVINCE_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['PROVINCE_NAME_P1'] != $Result[$i]['PROVINCE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อจังหวัด ไม่ตรงกัน ";
                        if($Result[$i]['PROVINCE_ABBR_P1'] != $Result[$i]['PROVINCE_ABBR_P2']) $problemDesc  = $problemDesc . "ชื่อย่อจังหวัด ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['PROVINCE_SEQ_P1'] == $Result[$i]['PROVINCE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PROVINCE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PROVINCE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PROVINCE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PROVINCE_ABBR_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PROVINCE_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['PROVINCE_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['PROVINCE_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['PROVINCE_ABBR_P2']);

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //ข้อมูลอำเภอ
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลอำเภอ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลอำเภอ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลอำเภอ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลอำเภอ');
            $bn = 'รายการข้อมูลอำเภอ';
            $fileName = date("Y/m/d") . '-รายการข้อมูลอำเภอ';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อจังหวัด')
            ->setCellValue('E4', 'ชื่ออำเภอ')

            ->setCellValue('F4', 'SEQ')
            ->setCellValue('G4', 'ID')
            ->setCellValue('H4', 'ชื่อจังหวัด')
            ->setCellValue('I4', 'ชื่ออำเภอ')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['AMPHUR_SEQ_P1'] != $Result[$i]['AMPHUR_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['AMPHUR_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['AMPHUR_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PROVINCE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['AMPHUR_NAME_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['AMPHUR_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['AMPHUR_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['PROVINCE_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['AMPHUR_NAME_P2']);

                    if(empty($Result[$i]['AMPHUR_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลอำเภอ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['AMPHUR_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลอำเภอ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['AMPHUR_ID_P1'] != $Result[$i]['AMPHUR_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['PROVINCE_NAME_P1'] != $Result[$i]['PROVINCE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อจังหวัด ไม่ตรงกัน ";
                        if($Result[$i]['AMPHUR_NAME_P1'] != $Result[$i]['AMPHUR_NAME_P2']) $problemDesc  = $problemDesc . "ชื่ออำเภอ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['AMPHUR_SEQ_P1'] == $Result[$i]['AMPHUR_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['AMPHUR_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['AMPHUR_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PROVINCE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['AMPHUR_NAME_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['AMPHUR_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['AMPHUR_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['PROVINCE_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['AMPHUR_NAME_P2']);

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '4' : //ข้อมูลตำบล
        $sheet->mergeCells('A1:L1');
        $sheet->mergeCells('A2:L2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลตำบล ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลตำบล ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลตำบล ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลตำบล');
            $bn = 'รายการข้อมูลตำบล';
            $fileName = date("Y/m/d") . '-รายการข้อมูลตำบล';
        }

        $sheet->mergeCells('A3:F3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('G3:L3');
        $sheet->setCellValue('G3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อจังหวัด')
            ->setCellValue('E4', 'ชื่ออำเภอ')
            ->setCellValue('F4', 'ชื่อตำบล')

            ->setCellValue('G4', 'SEQ')
            ->setCellValue('H4', 'ID')
            ->setCellValue('I4', 'ชื่อจังหวัด')
            ->setCellValue('J4', 'ชื่ออำเภอ')
            ->setCellValue('K4', 'ชื่อตำบล')

            ->setCellValue('L4', 'หมายเหตุ')
            ->getStyle('A2:L4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['TAMBOL_SEQ_P1'] != $Result[$i]['TAMBOL_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TAMBOL_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TAMBOL_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PROVINCE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['AMPHUR_NAME_P1']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TAMBOL_NAME_P1']);

                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TAMBOL_SEQ_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['TAMBOL_ID_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['AMPHUR_NAME_P2']);
                    $sheet->setCellValue('J' . (5 + $current), $Result[$i]['PROVINCE_NAME_P2']);
                    $sheet->setCellValue('K' . (5 + $current), $Result[$i]['TAMBOL_NAME_P2']);

                    if(empty($Result[$i]['TAMBOL_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลตำบล ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['TAMBOL_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลตำบล ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['TAMBOL_ID_P1'] != $Result[$i]['TAMBOL_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['PROVINCE_NAME_P1'] != $Result[$i]['PROVINCE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อจังหวัด ไม่ตรงกัน ";
                        if($Result[$i]['AMPHUR_NAME_P1'] != $Result[$i]['AMPHUR_NAME_P2']) $problemDesc  = $problemDesc . "ชื่ออำเภอ ไม่ตรงกัน ";
                        if($Result[$i]['TAMBOL_NAME_P1'] != $Result[$i]['TAMBOL_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อตำบล ไม่ตรงกัน ";


                    }

                    $sheet->setCellValue('L' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['TAMBOL_SEQ_P1'] == $Result[$i]['TAMBOL_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TAMBOL_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TAMBOL_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PROVINCE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['AMPHUR_NAME_P1']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TAMBOL_NAME_P1']);

                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TAMBOL_SEQ_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['TAMBOL_ID_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['AMPHUR_NAME_P2']);
                    $sheet->setCellValue('J' . (5 + $current), $Result[$i]['PROVINCE_NAME_P2']);
                    $sheet->setCellValue('K' . (5 + $current), $Result[$i]['TAMBOL_NAME_P2']);

                    $sheet->setCellValue('L' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:L'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:L'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:L4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K5:K' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L5:L' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '22' : //ข้อมูลประเภทหน่วยงาน
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทหน่วยงาน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทหน่วยงาน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทหน่วยงาน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทหน่วยงาน');
            $bn = 'รายการข้อมูลประเภทหน่วยงาน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทหน่วยงาน';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อประเภทหน่วยงาน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อประเภทหน่วยงาน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['OFFICE_TYPE_SEQ_P1'] != $Result[$i]['OFFICE_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['OFFICE_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['OFFICE_TYPE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['OFFICE_TYPE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['OFFICE_TYPE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['OFFICE_TYPE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['OFFICE_TYPE_NAME_P2']);

                    if(empty($Result[$i]['OFFICE_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหน่วยงาน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['OFFICE_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหน่วยงาน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['OFFICE_TYPE_ID_P1'] != $Result[$i]['OFFICE_TYPE_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['OFFICE_TYPE_NAME_P1'] != $Result[$i]['OFFICE_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทหน่วยงาน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['OFFICE_TYPE_SEQ_P1'] == $Result[$i]['OFFICE_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['OFFICE_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['OFFICE_TYPE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['OFFICE_TYPE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['OFFICE_TYPE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['OFFICE_TYPE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['OFFICE_TYPE_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '23' : //ข้อมูลหน่วยงาน
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหน่วยงาน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลหน่วยงาน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหน่วยงาน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหน่วยงาน');
            $bn = 'รายการข้อมูลหน่วยงาน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหน่วยงาน';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อระดับหน่วยงาน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อระดับหน่วยงาน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['OFFICE_LEVEL_SEQ_P1'] != $Result[$i]['OFFICE_LEVEL_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['OFFICE_LEVEL_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['OFFICE_LEVEL_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['OFFICE_LEVEL_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['OFFICE_LEVEL_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['OFFICE_LEVEL_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['OFFICE_LEVEL_NAME_P2']);

                    if(empty($Result[$i]['OFFICE_LEVEL_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหน่วยงาน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";

                    }else if(empty($Result[$i]['OFFICE_LEVEL_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหน่วยงาน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['OFFICE_LEVEL_ID_P1'] != $Result[$i]['OFFICE_LEVEL_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['OFFICE_LEVEL_NAME_P1'] != $Result[$i]['OFFICE_LEVEL_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อระดับหน่วยงาน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['OFFICE_LEVEL_SEQ_P1'] == $Result[$i]['OFFICE_LEVEL_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['OFFICE_LEVEL_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['OFFICE_LEVEL_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['OFFICE_LEVEL_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['OFFICE_LEVEL_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['OFFICE_LEVEL_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['OFFICE_LEVEL_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '5' : //ข้อมูลประเภทหน่วยงาน
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลแสดงประเภทหน่วยงาน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลแสดงประเภทหน่วยงาน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลแสดงประเภทหน่วยงาน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลแสดงประเภทหน่วยงาน');
            $bn = 'รายการข้อมูลแสดงประเภทหน่วยงาน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลแสดงประเภทหน่วยงาน';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อประเภทหน่วยงาน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อประเภทหน่วยงาน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['LANDOFFICE_TYPE_SEQ_P1'] != $Result[$i]['LANDOFFICE_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_NAME_P2']);

                    if(empty($Result[$i]['LANDOFFICE_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหน่วยงาน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['LANDOFFICE_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหน่วยงาน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['LANDOFFICE_TYPE_ID_P1'] != $Result[$i]['LANDOFFICE_TYPE_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['LANDOFFICE_TYPE_NAME_P1'] != $Result[$i]['LANDOFFICE_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทหน่วยงาน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['LANDOFFICE_TYPE_SEQ_P1'] == $Result[$i]['LANDOFFICE_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '24' : //ข้อมูลหน่วยงาน(ภายนอก)
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหน่วยงาน(ภายนอก) ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลหน่วยงาน(ภายนอก) ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหน่วยงาน(ภายนอก) ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหน่วยงาน(ภายนอก)');
            $bn = 'รายการข้อมูลหน่วยงาน(ภายนอก)';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหน่วยงาน(ภายนอก)';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อสำนักงาน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อสำนักงาน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['LANDOFFICE_SEQ_P1'] != $Result[$i]['LANDOFFICE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LANDOFFICE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['LANDOFFICE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P2']);

                    if(empty($Result[$i]['LANDOFFICE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหน่วยงาน(ภายนอก) ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['LANDOFFICE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหน่วยงาน(ภายนอก) ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['LANDOFFICE_ID_P1'] != $Result[$i]['LANDOFFICE_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['LANDOFFICE_NAME_TH_P1'] != $Result[$i]['LANDOFFICE_NAME_TH_P2']) $problemDesc  = $problemDesc . "ชื่อสำนักงาน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['LANDOFFICE_SEQ_P1'] == $Result[$i]['LANDOFFICE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LANDOFFICE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['LANDOFFICE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '25' : //ข้อมูลหน่วยงาน(ภายใน)
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหน่วยงาน(ภายใน) ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลหน่วยงาน(ภายใน) ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหน่วยงาน(ภายใน) ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหน่วยงาน(ภายใน)');
            $bn = 'รายการข้อมูลหน่วยงาน(ภายใน)';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหน่วยงาน(ภายใน)';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อสำนักงาน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ID')       
            ->setCellValue('G4', 'ชื่อสำนักงาน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['LANDOFFICE_SEQ_P1'] != $Result[$i]['LANDOFFICE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LANDOFFICE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['LANDOFFICE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P2']);

                    if(empty($Result[$i]['LANDOFFICE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหน่วยงาน(ภายใน) ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['LANDOFFICE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลหน่วยงาน(ภายใน) ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['LANDOFFICE_ID_P1'] != $Result[$i]['LANDOFFICE_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['LANDOFFICE_NAME_TH_P1'] != $Result[$i]['LANDOFFICE_NAME_TH_P2']) $problemDesc  = $problemDesc . "ชื่อสำนักงาน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['LANDOFFICE_SEQ_P1'] == $Result[$i]['LANDOFFICE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LANDOFFICE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['LANDOFFICE_ID_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '26' : //ข้อมูลฝ่าย
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลฝ่าย ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลฝ่าย ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลฝ่าย ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลฝ่าย');
            $bn = 'รายการข้อมูลฝ่าย';
            $fileName = date("Y/m/d") . '-รายการข้อมูลฝ่าย';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อฝ่าย')       
            ->setCellValue('D4', 'ชื่อสำนักงาน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ชื่อฝ่าย')       
            ->setCellValue('G4', 'ชื่อสำนักงาน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['LANDOFFICE_SEQ_P1'] != $Result[$i]['LANDOFFICE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P2']);

                    if(empty($Result[$i]['LANDOFFICE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลฝ่าย ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['LANDOFFICE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลฝ่าย ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['LANDOFFICE_TYPE_NAME_P1'] != $Result[$i]['LANDOFFICE_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อฝ่าย ไม่ตรงกัน ";
                        if($Result[$i]['LANDOFFICE_NAME_TH_P1'] != $Result[$i]['LANDOFFICE_NAME_TH_P2']) $problemDesc  = $problemDesc . "ชื่อสำนักงาน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['LANDOFFICE_SEQ_P1'] == $Result[$i]['LANDOFFICE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDOFFICE_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['LANDOFFICE_TYPE_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '12' : //ข้อมูลพื้นที่รับผิดชอบของสำนักงานที่ดิน
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลพื้นที่รับผิดชอบของสำนักงานที่ดิน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลพื้นที่รับผิดชอบของสำนักงานที่ดิน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลพื้นที่รับผิดชอบของสำนักงานที่ดิน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลพื้นที่รับผิดชอบของสำนักงานที่ดิน');
            $bn = 'รายการข้อมูลพื้นที่รับผิดชอบของสำนักงานที่ดิน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลพื้นที่รับผิดชอบของสำนักงานที่ดิน';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่ออำเภอ')       
            ->setCellValue('D4', 'ชื่อสำนักงาน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ชื่ออำเภอ')       
            ->setCellValue('G4', 'ชื่อสำนักงาน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['LANDOFFICE_AREA_SEQ_P1'] != $Result[$i]['LANDOFFICE_AREA_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDOFFICE_AREA_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['AMPHUR_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDOFFICE_AREA_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['AMPHUR_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P2']);

                    if(empty($Result[$i]['LANDOFFICE_AREA_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลพื้นที่รับผิดชอบของสำนักงานที่ดิน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['LANDOFFICE_AREA_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลพื้นที่รับผิดชอบของสำนักงานที่ดิน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['AMPHUR_NAME_P1'] != $Result[$i]['AMPHUR_NAME_P2']) $problemDesc  = $problemDesc . "ชื่ออำเภอ ไม่ตรงกัน ";
                        if($Result[$i]['LANDOFFICE_NAME_TH_P1'] != $Result[$i]['LANDOFFICE_NAME_TH_P2']) $problemDesc  = $problemDesc . "ชื่อสำนักงาน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['LANDOFFICE_AREA_SEQ_P1'] == $Result[$i]['LANDOFFICE_AREA_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LANDOFFICE_AREA_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['AMPHUR_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LANDOFFICE_AREA_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['AMPHUR_NAME_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['LANDOFFICE_NAME_TH_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;
    
    case '13' : //ข้อมูลประเภท อปท.
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภท อปท. ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภท อปท. ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภท อปท. ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภท อปท.');
            $bn = 'รายการข้อมูลประเภท อปท.';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภท อปท.';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภท อปท.')
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภท อปท.')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['OPT_TYPE_SEQ_P1'] != $Result[$i]['OPT_TYPE_SEQ_P2']){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['OPT_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['OPT_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['OPT_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['OPT_TYPE_NAME_P2']);

                    if(empty($Result[$i]['OPT_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภท อปท. ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['OPT_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภท อปท. ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['OPT_TYPE_NAME_P1'] != $Result[$i]['OPT_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภท อปท. ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['OPT_TYPE_SEQ_P1'] == $Result[$i]['OPT_TYPE_SEQ_P2']){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['OPT_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['OPT_TYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['OPT_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['OPT_TYPE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '8' : //ข้อมูล อปท.
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูล อปท. ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูล อปท. ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูล อปท. ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูล อปท.');
            $bn = 'รายการข้อมูล อปท.';
            $fileName = date("Y/m/d") . '-รายการข้อมูล อปท.';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อ อปท.')
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อ อปท.')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['OPT_SEQ_P1'] != $Result[$i]['OPT_SEQ_P2']){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['OPT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['OPT_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['OPT_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['OPT_NAME_P2']);

                    if(empty($Result[$i]['OPT_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูล อปท. ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['OPT_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูล อปท. ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['OPT_NAME_P1'] != $Result[$i]['OPT_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อ อปท. ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['OPT_SEQ_P1'] == $Result[$i]['OPT_SEQ_P2']){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['OPT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['OPT_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['OPT_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['OPT_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;


    case '9' : //ข้อมูลประเภทเอกสารสิทธิ์
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทเอกสารสิทธิ์ ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทเอกสารสิทธิ์ ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทเอกสารสิทธิ์ ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทเอกสารสิทธิ์');
            $bn = 'รายการข้อมูลประเภทเอกสารสิทธิ์';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทเอกสารสิทธิ์';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อย่อ')
            ->setCellValue('E4', 'ชื่อเอกสารสิทธิ์')

            ->setCellValue('F4', 'SEQ')
            ->setCellValue('G4', 'ID')
            ->setCellValue('H4', 'ชื่อย่อ')
            ->setCellValue('I4', 'ชื่อเอกสารสิทธิ์')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['PRINTPLATE_TYPE_SEQ_P1'] != $Result[$i]['PRINTPLATE_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_ABBR_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_NAME_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_ABBR_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_NAME_P2']);

                    if(empty($Result[$i]['PRINTPLATE_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทเอกสารสิทธิ์ ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['PRINTPLATE_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทเอกสารสิทธิ์ ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['PRINTPLATE_TYPE_ID_P1'] != $Result[$i]['PRINTPLATE_TYPE_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['PRINTPLATE_TYPE_ABBR_P1'] != $Result[$i]['PRINTPLATE_TYPE_ABBR_P2']) $problemDesc  = $problemDesc . "ชื่อย่อ ไม่ตรงกัน ";
                        if($Result[$i]['PRINTPLATE_TYPE_NAME_P1'] != $Result[$i]['PRINTPLATE_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อเอกสารสิทธิ์ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['PRINTPLATE_TYPE_SEQ_P1'] == $Result[$i]['PRINTPLATE_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_ABBR_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_NAME_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_ABBR_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['PRINTPLATE_TYPE_NAME_P2']);

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '27' : //ข้อมูลวันหยุด
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลวันหยุด ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลวันหยุด ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลวันหยุด ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลวันหยุด');
            $bn = 'รายการข้อมูลวันหยุด';
            $fileName = date("Y/m/d") . '-รายการข้อมูลวันหยุด';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'วันเดือนปี')       
            ->setCellValue('D4', 'รายละเอียด')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'วันเดือนปี')       
            ->setCellValue('G4', 'รายละเอียด')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['HOLIDAY_DATE_P1'] != $Result[$i]['HOLIDAY_DATE_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['HOLIDAY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['HOLIDAY_DATE_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['HOLIDAY_SEQ_P2']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['HOLIDAY_DATE_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['HOLIDAY_DATE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['HOLIDAY_DATE_P2']);


                    if(empty($Result[$i]['HOLIDAY_DATE_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลวันหยุด ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['HOLIDAY_DATE_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลวันหยุด ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['HOLIDAY_SEQ_P1'] != $Result[$i]['HOLIDAY_SEQ_P2']) $problemDesc  = $problemDesc . "SEQ ไม่ตรงกัน ";
                        if($Result[$i]['HOLIDAY_DESCRIPTION_P1'] != $Result[$i]['HOLIDAY_DESCRIPTION_P2']) $problemDesc  = $problemDesc . "รายละเอียด ไม่ตรงกัน ";


                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['HOLIDAY_DATE_P1'] == $Result[$i]['HOLIDAY_DATE_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['HOLIDAY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['HOLIDAY_DATE_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['HOLIDAY_DESCRIPTION_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['HOLIDAY_DATE_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['HOLIDAY_DATE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['HOLIDAY_DESCRIPTION_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;


}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
