<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'queryREGData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}



switch ($checknum){
    case '1' : //การได้มา
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลการได้มา ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลการได้มา ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลการได้มา ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลการได้มา');
            $bn = 'รายการข้อมูลการได้มา';
            $fileName = date("Y/m/d") . '-รายการข้อมูลการได้มา';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อการได้มา')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อการได้มา')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['OBTAIN_SEQ_P1'] != $Result[$i]['OBTAIN_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['OBTAIN_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['OBTAIN_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['OBTAIN_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['OBTAIN_NAME_P2']);

                    if(empty($Result[$i]['OBTAIN_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลการได้มา ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['OBTAIN_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลการได้มา ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['OBTAIN_NAME_P1'] != $Result[$i]['OBTAIN_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อการได้มา ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['OBTAIN_SEQ_P1'] == $Result[$i]['OBTAIN_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['OBTAIN_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['OBTAIN_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['OBTAIN_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['OBTAIN_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //ประเภทการจดทะเบียน
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทการจดทะเบียน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทการจดทะเบียน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทการจดทะเบียน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทการจดทะเบียน');
            $bn = 'รายการข้อมูลประเภทการจดทะเบียน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทการจดทะเบียน';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อย่อ')       
            ->setCellValue('D4', 'ชื่อประเภทการจดทะเบียน')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'ชื่อย่อ')       
            ->setCellValue('G4', 'ชื่อประเภทการจดทะเบียน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['REGISTER_SEQ_P1'] != $Result[$i]['REGISTER_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['REGISTER_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['REGISTER_ABBR_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['REGISTER_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['REGISTER_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['REGISTER_ABBR_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['REGISTER_NAME_P2']);

                    if(empty($Result[$i]['REGISTER_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทการจดทะเบียน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['REGISTER_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทการจดทะเบียน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['REGISTER_ABBR_P1'] != $Result[$i]['REGISTER_ABBR_P2']) $problemDesc  = $problemDesc . "ชื่อย่อ ไม่ตรงกัน ";
                        if($Result[$i]['REGISTER_NAME_P1'] != $Result[$i]['REGISTER_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทการจดทะเบียน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['REGISTER_SEQ_P1'] == $Result[$i]['REGISTER_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['REGISTER_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['REGISTER_ABBR_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['REGISTER_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['REGISTER_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['REGISTER_ABBR_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['REGISTER_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //ชื่อประเภทบัญชีคุม
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลชื่อประเภทบัญชีคุม ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลชื่อประเภทบัญชีคุม ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลชื่อประเภทบัญชีคุม ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลชื่อประเภทบัญชีคุม');
            $bn = 'รายการข้อมูลชื่อประเภทบัญชีคุม';
            $fileName = date("Y/m/d") . '-รายการข้อมูลชื่อประเภทบัญชีคุม';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทบัญชีคุม')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทบัญชีคุม')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['BOOK_TYPE_SEQ_P1'] != $Result[$i]['BOOK_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['BOOK_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['BOOK_TYPE_NAME_P1']);

                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['BOOK_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['BOOK_TYPE_NAME_P2']);

                    if(empty($Result[$i]['BOOK_TYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบชื่อประเภทบัญชีคุม ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['BOOK_TYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบชื่อประเภทบัญชีคุม ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['BOOK_TYPE_NAME_P1'] != $Result[$i]['BOOK_TYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทบัญชีคุม ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['BOOK_TYPE_SEQ_P1'] == $Result[$i]['BOOK_TYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['BOOK_TYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['BOOK_TYPE_NAME_P1']);

                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['BOOK_TYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['BOOK_TYPE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '4' : //รายการเอกสารจดทะเบียน
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการเอกสารจดทะเบียน ที่แตกต่างกัน' );
            $bn = 'รายการเอกสารจดทะเบียน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการเอกสารจดทะเบียน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการเอกสารจดทะเบียน');
            $bn = 'รายการเอกสารจดทะเบียน';
            $fileName = date("Y/m/d") . '-รายการเอกสารจดทะเบียน';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ID')       
            ->setCellValue('D4', 'ชื่อย่อ')
            ->setCellValue('E4', 'ชื่อย่อรายการเอกสารจดทะเบียน')

            ->setCellValue('F4', 'SEQ')
            ->setCellValue('G4', 'ID')
            ->setCellValue('H4', 'ชื่อย่อ')
            ->setCellValue('I4', 'ชื่อย่อรายการเอกสารจดทะเบียน')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['DOCUMENT_SEQ_P1'] != $Result[$i]['DOCUMENT_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['DOCUMENT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['DOCUMENT_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['DOCUMENT_ABBR_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['DOCUMENT_NAME_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['DOCUMENT_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['DOCUMENT_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['DOCUMENT_ABBR_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['DOCUMENT_NAME_P2']);

                    if(empty($Result[$i]['DOCUMENT_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบรายการเอกสารจดทะเบียน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['DOCUMENT_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบรายการเอกสารจดทะเบียน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['DOCUMENT_ID_P1'] != $Result[$i]['DOCUMENT_ID_P2']) $problemDesc  = $problemDesc . "ID ไม่ตรงกัน ";
                        if($Result[$i]['DOCUMENT_ABBR_P1'] != $Result[$i]['DOCUMENT_ABBR_P2']) $problemDesc  = $problemDesc . "ชื่อย่อ ไม่ตรงกัน ";
                        if($Result[$i]['DOCUMENT_NAME_P1'] != $Result[$i]['DOCUMENT_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อเอกสารสิทธิ์ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['DOCUMENT_SEQ_P1'] == $Result[$i]['DOCUMENT_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['DOCUMENT_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['DOCUMENT_ID_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['DOCUMENT_ABBR_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['DOCUMENT_NAME_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['DOCUMENT_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['DOCUMENT_ID_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['DOCUMENT_ABBR_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['DOCUMENT_NAME_P2']);

                    $sheet->setCellValue('J' . (4 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;



}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
