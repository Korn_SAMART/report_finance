<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'querySVAData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}

switch ($checknum){
    case '1' : //ประเภทการรังวัด
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทการรังวัด ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทการรังวัด ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทการรังวัด ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทการรังวัด');
            $bn = 'รายการข้อมูลประเภทการรังวัด';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทการรังวัด';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทการรังวัดหลัก')       
            ->setCellValue('D4', 'ชื่อประเภทการรังวัด')
            ->setCellValue('E4', 'ชื่อย่อ')

            ->setCellValue('F4', 'SEQ')
            ->setCellValue('G4', 'ชื่อประเภทการรังวัดหลัก')
            ->setCellValue('H4', 'ชื่อประเภทการรังวัด')
            ->setCellValue('I4', 'ชื่อย่อ')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['TYPEOFSURVEY_SEQ_P1'] != $Result[$i]['TYPEOFSURVEY_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TYPEOFSURVEY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['MAINTYPEOFSURVEY_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TYPEOFSURVEY_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TYPEOFSURVEY_SHORTNAME_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TYPEOFSURVEY_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['MAINTYPEOFSURVEY_NAME_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['TYPEOFSURVEY_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['TYPEOFSURVEY_SHORTNAME_P2']);

                    if(empty($Result[$i]['TYPEOFSURVEY_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทการรังวัด ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['TYPEOFSURVEY_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทการรังวัด ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['MAINTYPEOFSURVEY_NAME_P1'] != $Result[$i]['MAINTYPEOFSURVEY_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทการรังวัดหลัก ไม่ตรงกัน ";
                        if($Result[$i]['TYPEOFSURVEY_NAME_P1'] != $Result[$i]['TYPEOFSURVEY_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทการรังวัด ไม่ตรงกัน ";
                        if($Result[$i]['TYPEOFSURVEY_SHORTNAME_P1'] != $Result[$i]['TYPEOFSURVEY_SHORTNAME_P2']) $problemDesc  = $problemDesc . "ชื่อย่อ ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['TYPEOFSURVEY_SEQ_P1'] == $Result[$i]['TYPEOFSURVEY_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TYPEOFSURVEY_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['MAINTYPEOFSURVEY_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TYPEOFSURVEY_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TYPEOFSURVEY_SHORTNAME_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TYPEOFSURVEY_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['MAINTYPEOFSURVEY_NAME_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['TYPEOFSURVEY_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['TYPEOFSURVEY_SHORTNAME_P2']);

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //ประเภทหลักฐานการรังวัด
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทหลักฐานการรังวัด ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทหลักฐานการรังวัด ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทหลักฐานการรังวัด ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทหลักฐานการรังวัด');
            $bn = 'รายการข้อมูลประเภทหลักฐานการรังวัด';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทหลักฐานการรังวัด';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ประเภทหลักฐานการรังวัด')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ประเภทหลักฐานการรังวัด')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['SURVEYDOCTYPE_SEQ_P1'] != $Result[$i]['SURVEYDOCTYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['SURVEYDOCTYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['SURVEYDOCTYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['SURVEYDOCTYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['SURVEYDOCTYPE_NAME_P2']);

                    if(empty($Result[$i]['SURVEYDOCTYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหลักฐานการรังวัด ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['SURVEYDOCTYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทหลักฐานการรังวัด ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['COURT_NAME_P1'] != $Result[$i]['COURT_NAME_P2']) $problemDesc  = $problemDesc . "ประเภทหลักฐานการรังวัด ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['SURVEYDOCTYPE_SEQ_P1'] == $Result[$i]['SURVEYDOCTYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['SURVEYDOCTYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['SURVEYDOCTYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['SURVEYDOCTYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['SURVEYDOCTYPE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //ประเภทเครื่องมือการรังวัด
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทเครื่องมือการรังวัด ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทเครื่องมือการรังวัด ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทเครื่องมือการรังวัด ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทเครื่องมือการรังวัด');
            $bn = 'รายการข้อมูลประเภทเครื่องมือการรังวัด';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทเครื่องมือการรังวัด';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ประเภทเครื่องมือการรังวัด')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ประเภทเครื่องมือการรังวัด')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['EQUIPMENTTYPE_SEQ_P1'] != $Result[$i]['EQUIPMENTTYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['EQUIPMENTTYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['EQUIPMENTTYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['EQUIPMENTTYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['EQUIPMENTTYPE_NAME_P2']);

                    if(empty($Result[$i]['EQUIPMENTTYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทเครื่องมือการรังวัด ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";

                    }else if(empty($Result[$i]['EQUIPMENTTYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทเครื่องมือการรังวัด ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['EQUIPMENTTYPE_NAME_P1'] != $Result[$i]['EQUIPMENTTYPE_NAME_P2']) $problemDesc  = $problemDesc . "ประเภทเครื่องมือการรังวัด ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['EQUIPMENTTYPE_SEQ_P1'] == $Result[$i]['EQUIPMENTTYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['EQUIPMENTTYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['EQUIPMENTTYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['EQUIPMENTTYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['EQUIPMENTTYPE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '4' : //สำนักงานรังวัดเอกชน
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสำนักงานรังวัดเอกชน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลสำนักงานรังวัดเอกชน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสำนักงานรังวัดเอกชน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสำนักงานรังวัดเอกชน');
            $bn = 'รายการข้อมูลสำนักงานรังวัดเอกชน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสำนักงานรังวัดเอกชน';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อสำนักงานรังวัดเอกชน')       
            ->setCellValue('D4', 'เลขที่ใบอนุญาต')
            ->setCellValue('E4', 'วันที่ขึ้นทะเบียน')

            ->setCellValue('F4', 'SEQ')
            ->setCellValue('G4', 'ชื่อสำนักงานรังวัดเอกชน')
            ->setCellValue('H4', 'เลขที่ใบอนุญาต')
            ->setCellValue('I4', 'วันที่ขึ้นทะเบียน')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['PRIVATE_OFFICE_SEQ_P1'] != $Result[$i]['PRIVATE_OFFICE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_CERTIFICATE_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_REGIST_DATE_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_NAME_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_CERTIFICATE_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_REGIST_DATE_P2']);

                    if(empty($Result[$i]['PRIVATE_OFFICE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสำนักงานรังวัดเอกชน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['PRIVATE_OFFICE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสำนักงานรังวัดเอกชน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['PRIVATE_OFFICE_NAME_P1'] != $Result[$i]['PRIVATE_OFFICE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อสำนักงานรังวัดเอกชน ไม่ตรงกัน ";
                        if($Result[$i]['PRIVATE_OFFICE_CERTIFICATE_P1'] != $Result[$i]['PRIVATE_OFFICE_CERTIFICATE_P2']) $problemDesc  = $problemDesc . "เลขที่ใบอนุญาต ไม่ตรงกัน ";
                        if($Result[$i]['PRIVATE_OFFICE_REGIST_DATE_P1'] != $Result[$i]['PRIVATE_OFFICE_REGIST_DATE_P2']) $problemDesc  = $problemDesc . "วันที่ขึ้นทะเบียน ไม่ตรงกัน ";

                    }

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['PRIVATE_OFFICE_SEQ_P1'] == $Result[$i]['PRIVATE_OFFICE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_CERTIFICATE_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_REGIST_DATE_P1']);

                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_SEQ_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_NAME_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_CERTIFICATE_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['PRIVATE_OFFICE_REGIST_DATE_P2']);

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '5' : //ข้อมูลระวาง
        $sheet->mergeCells('A1:L1');
        $sheet->mergeCells('A2:L2');

        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลระวาง ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลระวาง ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลระวาง ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลระวาง');
            $bn = 'รายการข้อมูลระวาง';
            $fileName = date("Y/m/d") . '-รายการข้อมูลระวาง';
        }

        $sheet->mergeCells('A3:F3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('G3:L3');
        $sheet->setCellValue('G3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ประเภทระวาง')       
            ->setCellValue('D4', 'หมายเลขระวาง')
            ->setCellValue('E4', 'แผ่นที่')
            ->setCellValue('F4', 'มาตราส่วน')

            ->setCellValue('G4', 'SEQ')
            ->setCellValue('H4', 'ประเภทระวาง')
            ->setCellValue('I4', 'หมายเลขระวาง')
            ->setCellValue('J4', 'แผ่นที่')
            ->setCellValue('K4', 'มาตราส่วน')

            ->setCellValue('L4', 'หมายเหตุ')
            ->getStyle('A2:L4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['SHEET_SEQ_P1'] != $Result[$i]['SHEET_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['SHEET_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['SHEETTYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['UTM_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['SHEET_UTMMAP4_P1']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['SCALE_NAME_P1']);

                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['SHEET_SEQ_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['SHEETTYPE_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['UTM_P2']);
                    $sheet->setCellValue('J' . (5 + $current), $Result[$i]['SHEET_UTMMAP4_P2']);
                    $sheet->setCellValue('K' . (5 + $current), $Result[$i]['SCALE_NAME_P2']);

                    if(empty($Result[$i]['SHEET_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลระวาง ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['SHEET_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลระวาง ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['SHEETTYPE_NAME_P1'] != $Result[$i]['SHEETTYPE_NAME_P2']) $problemDesc  = $problemDesc . "ประเภทระวาง ไม่ตรงกัน ";
                        if($Result[$i]['UTM_P1'] != $Result[$i]['UTM_P2']) $problemDesc  = $problemDesc . "หมายเลขระวาง ไม่ตรงกัน ";
                        if($Result[$i]['SHEET_UTMMAP4_P1'] != $Result[$i]['SHEET_UTMMAP4_P2']) $problemDesc  = $problemDesc . "แผ่นที่ ไม่ตรงกัน ";
                        if($Result[$i]['SCALE_NAME_P1'] != $Result[$i]['SCALE_NAME_P2']) $problemDesc  = $problemDesc . "มาตราส่วน ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('L' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['SHEET_SEQ_P1'] == $Result[$i]['SHEET_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['SHEET_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['SHEETTYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['UTM_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['SHEET_UTMMAP4_P1']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['SCALE_NAME_P1']);

                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['SHEET_SEQ_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['SHEETTYPE_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['UTM_P2']);
                    $sheet->setCellValue('J' . (5 + $current), $Result[$i]['SHEET_UTMMAP4_P2']);
                    $sheet->setCellValue('K' . (5 + $current), $Result[$i]['SCALE_NAME_P2']);

                    $sheet->setCellValue('L' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:L'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:L'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:L4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K5:K' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L5:L' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '6' : //สถานะงานรังวัด
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสถานะงานรังวัด ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลสถานะงานรังวัด ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสถานะงานรังวัด ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลสถานะงานรังวัด');
            $bn = 'รายการข้อมูลสถานะงานรังวัด';
            $fileName = date("Y/m/d") . '-รายการข้อมูลสถานะงานรังวัด';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อสถานะงานรังวัด')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อสถานะงานรังวัด')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['JOBSTATUS_SEQ_P1'] != $Result[$i]['JOBSTATUS_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['JOBSTATUS_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['JOBSTATUS_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['JOBSTATUS_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['JOBSTATUS_NAME_P2']);

                    if(empty($Result[$i]['JOBSTATUS_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสถานะงานรังวัด ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['JOBSTATUS_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลสถานะงานรังวัด ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['JOBSTATUS_NAME_P1'] != $Result[$i]['JOBSTATUS_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อสถานะงานรังวัด ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['JOBSTATUS_SEQ_P1'] == $Result[$i]['JOBSTATUS_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['JOBSTATUS_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['JOBSTATUS_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['JOBSTATUS_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['JOBSTATUS_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '7' : //ประเภทจดหมายแจ้ง
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทจดหมายแจ้ง ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทจดหมายแจ้ง ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทจดหมายแจ้ง ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทจดหมายแจ้ง');
            $bn = 'รายการข้อมูลประเภทจดหมายแจ้ง';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทจดหมายแจ้ง';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทจดหมายแจ้ง')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทจดหมายแจ้ง')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['LETTERTYPE_SEQ_P1'] != $Result[$i]['LETTERTYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LETTERTYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LETTERTYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LETTERTYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LETTERTYPE_NAME_P2']);

                    if(empty($Result[$i]['LETTERTYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทจดหมายแจ้ง ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";

                    }else if(empty($Result[$i]['LETTERTYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทจดหมายแจ้ง ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['LETTERTYPE_NAME_P1'] != $Result[$i]['LETTERTYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทจดหมายแจ้ง ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['LETTERTYPE_SEQ_P1'] == $Result[$i]['LETTERTYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['LETTERTYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['LETTERTYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['LETTERTYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['LETTERTYPE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
        ->getAlignment()
        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '8' : //ประเภทกลุ่มงานรังวัด
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทกลุ่มงานรังวัด ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทกลุ่มงานรังวัด ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทกลุ่มงานรังวัด ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทกลุ่มงานรังวัด');
            $bn = 'รายการข้อมูลประเภทกลุ่มงานรังวัด';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทกลุ่มงานรังวัด';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทกลุ่มงานรังวัด')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทกลุ่มงานรังวัด')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['JOBGROUP_SEQ_P1'] != $Result[$i]['JOBGROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['JOBGROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['JOBGROUPNAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['JOBGROUP_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['JOBGROUPNAME_P2']);

                    if(empty($Result[$i]['JOBGROUP_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทกลุ่มงานรังวัด ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['JOBGROUP_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทกลุ่มงานรังวัด ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['JOBGROUPNAME_P1'] != $Result[$i]['JOBGROUPNAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทกลุ่มงานรังวัด ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['JOBGROUP_SEQ_P1'] == $Result[$i]['JOBGROUP_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['JOBGROUP_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['JOBGROUPNAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['JOBGROUP_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['JOBGROUPNAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);

        
        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '9' : //ประเภทกลุ่มงานเอกสารสิทธิในที่ดิน
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทกลุ่มงานเอกสารสิทธิในที่ดิน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลประเภทกลุ่มงานเอกสารสิทธิในที่ดิน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทกลุ่มงานเอกสารสิทธิในที่ดิน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลประเภทกลุ่มงานเอกสารสิทธิในที่ดิน');
            $bn = 'รายการข้อมูลประเภทกลุ่มงานเอกสารสิทธิในที่ดิน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลประเภทกลุ่มงานเอกสารสิทธิในที่ดิน';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'ชื่อประเภทกลุ่มงานเอกสารสิทธิในที่ดิน')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'ชื่อประเภทกลุ่มงานเอกสารสิทธิในที่ดิน')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['GROUPTYPE_SEQ_P1'] != $Result[$i]['GROUPTYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['GROUPTYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['GROUPTYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['GROUPTYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['GROUPTYPE_NAME_P2']);

                    if(empty($Result[$i]['GROUPTYPE_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทกลุ่มงานเอกสารสิทธิในที่ดิน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['GROUPTYPE_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลประเภทกลุ่มงานเอกสารสิทธิในที่ดิน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['GROUPTYPE_NAME_P1'] != $Result[$i]['GROUPTYPE_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อประเภทกลุ่มงานเอกสารสิทธิในที่ดิน ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['GROUPTYPE_SEQ_P1'] == $Result[$i]['GROUPTYPE_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['GROUPTYPE_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['GROUPTYPE_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['GROUPTYPE_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['GROUPTYPE_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:F4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;



}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
