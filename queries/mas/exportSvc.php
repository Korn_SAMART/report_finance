<?php
require '../../plugins/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$styleArray = [
    'borders' => [
        'allBorders' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '00000000'],
        ],
    ],
    'font' => array(
        'bold'  => false,
        'color' => array('rgb' => '000000'),
        'size'  => 16,
        'name'  => 'TH SarabunPSK'
    )
];
$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$current = 0;
$problemDesc = "";
$count_temp1 =0;
$count_temp2 =0;


$Result = array();
$checksys = !isset($_GET['sysType']) ? '' : $_GET['sysType'];
$checknum = !isset($_GET['type']) ? '' : $_GET['type'];
$sts = !isset($_GET['sts']) ? '' : $_GET['sts'];

include 'querySVCData.php';
while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
    $Result[] = $row;
}

switch ($checknum){
    case '1' : //หมุดเส้นโครงาน
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหมุดเส้นโครงงาน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลหมุดเส้นโครงงาน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหมุดเส้นโครงงาน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหมุดเส้นโครงงาน');
            $bn = 'รายการข้อมูลหมุดเส้นโครงงาน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหมุดเส้นโครงงาน';
        }

        $sheet->mergeCells('A3:E3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('F3:J3');
        $sheet->setCellValue('F3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'โซน')
            ->setCellValue('C4', 'จังหวัด')
            ->setCellValue('D4', 'ชื่อเส้นโครงงาน')       
            ->setCellValue('E4', 'หมุดเส้นโครงงาน')
            
            ->setCellValue('F4', 'โซน')
            ->setCellValue('G4', 'จังหวัด')
            ->setCellValue('H4', 'ชื่อเส้นโครงงาน')       
            ->setCellValue('I4', 'หมุดเส้นโครงงาน')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A2:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['TV_ZONE_P1'] != $Result[$i]['TV_ZONE_P2'] && 
                    $Result[$i]['TV_PROV_P1'] != $Result[$i]['TV_PROV_P2'] && 
                    $Result[$i]['TV_NAME_P1'] != $Result[$i]['TV_NAME_P2'] && 
                    $Result[$i]['TVC_ORDERNO_P1'] != $Result[$i]['TVC_ORDERNO_P2'] && 
                    $Result[$i]['TVC_CTRLNAME_P1'] != $Result[$i]['TVC_CTRLNAME_P2'] ){

                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TV_ZONE_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TV_PROV_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TV_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TVC_CTRLNAME_P1']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TV_ZONE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TV_PROV_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['TV_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['TVC_CTRLNAME_P2']);

                    if($Result[$i]['TV_ZONE_P1'] != $Result[$i]['TV_ZONE_P2']) $problemDesc  = $problemDesc . "โซน ไม่ตรงกัน ";
                    if($Result[$i]['TV_PROV_P1'] != $Result[$i]['TV_PROV_P2']) $problemDesc  = $problemDesc . "จังหวัด ไม่ตรงกัน";
                    if($Result[$i]['TV_NAME_P1'] != $Result[$i]['TV_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อเส้นโครงงาน ไม่ตรงกัน ";
                    if($Result[$i]['TVC_CTRLNAME_P1'] != $Result[$i]['TVC_CTRLNAME_P2']) $problemDesc  = $problemDesc . "หมุดเส้นโครงงาน ไม่ตรงกัน ";

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['TV_ZONE_P1'] == $Result[$i]['TV_ZONE_P2'] && 
                   $Result[$i]['TV_PROV_P1'] == $Result[$i]['TV_PROV_P2'] && 
                   $Result[$i]['TV_NAME_P1'] == $Result[$i]['TV_NAME_P2'] && 
                   $Result[$i]['TVC_ORDERNO_P1'] == $Result[$i]['TVC_ORDERNO_P2'] && 
                   $Result[$i]['TVC_CTRLNAME_P1'] == $Result[$i]['TVC_CTRLNAME_P2'] ){

                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TV_ZONE_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TV_PROV_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TV_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TVC_CTRLNAME_P1']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TV_ZONE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TV_PROV_P2']);
                    $sheet->setCellValue('H' . (5 + $current), $Result[$i]['TV_NAME_P2']);
                    $sheet->setCellValue('I' . (5 + $current), $Result[$i]['TVC_CTRLNAME_P2']);

                    $sheet->setCellValue('J' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:J'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:J'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:J4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I5:I' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J5:J' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '2' : //หมุดดาวเทียม
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหมุดดาวเทียม ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลหมุดดาวเทียม ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหมุดดาวเทียม ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลหมุดดาวเทียม');
            $bn = 'รายการข้อมูลหมุดดาวเทียม';
            $fileName = date("Y/m/d") . '-รายการข้อมูลหมุดดาวเทียม';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'โซน')       
            ->setCellValue('D4', 'ชื่อหมุดดาวเทียม')
            
            ->setCellValue('E4', 'SEQ')
            ->setCellValue('F4', 'โซน')       
            ->setCellValue('G4', 'ชื่อหมุดดาวเทียม')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['GPSMARK_SEQ_P1'] != $Result[$i]['GPSMARK_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['GPSMARK_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['GM_ZONE_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['GM_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['GPSMARK_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['GM_ZONE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['GM_NAME_P2']);

                    if(empty($Result[$i]['GPSMARK_SEQ_P1'])){
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";

                    }else if(empty($Result[$i]['GPSMARK_SEQ_P2'])){
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";

                    }else{
                        if($Result[$i]['AMPHUR_NAME_P1'] != $Result[$i]['AMPHUR_NAME_P2']) $problemDesc  = $problemDesc . "โซน ไม่ตรงกัน ";
                        if($Result[$i]['LANDOFFICE_NAME_TH_P1'] != $Result[$i]['LANDOFFICE_NAME_TH_P2']) $problemDesc  = $problemDesc . "ชื่อหมุดดาวเทียม ไม่ตรงกัน ";

                    }
                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['GPSMARK_SEQ_P1'] == $Result[$i]['GPSMARK_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['GPSMARK_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['GM_ZONE_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['GM_NAME_P1']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['GPSMARK_SEQ_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['GM_ZONE_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['GM_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '3' : //ระวาง 4000 โซน 47
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลระวาง 4000 โซน 47 ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลระวาง 4000 โซน 47 ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลระวาง 4000 โซน 47 ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลระวาง 4000 โซน 47');
            $bn = 'รายการข้อมูลระวาง 4000 โซน 47';
            $fileName = date("Y/m/d") . '-รายการข้อมูลระวาง 4000 โซน 47';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'หมายเลขระวาง')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'หมายเลขระวาง')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['INDEX_SEQ_P1'] != $Result[$i]['INDEX_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['INDEX_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['INDEX_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['INDEX_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['INDEX_NAME_P2']);

                    if(empty($Result[$i]['INDEX_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลการใช้ประโยชน์ในที่ดิน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['INDEX_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลการใช้ประโยชน์ในที่ดิน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['INDEX_NAME_P1'] != $Result[$i]['INDEX_NAME_P2']) $problemDesc  = $problemDesc . "หมายเลขระวาง ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['INDEX_SEQ_P1'] == $Result[$i]['INDEX_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['INDEX_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['INDEX_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['INDEX_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['INDEX_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '4' : //ระวาง 4000 โซน 48
        $sheet->mergeCells('A1:F1');
        $sheet->mergeCells('A2:F2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลระวาง 4000 โซน 48 ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลระวาง 4000 โซน 48 ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลระวาง 4000 โซน 48 ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลระวาง 4000 โซน 48');
            $bn = 'รายการข้อมูลระวาง 4000 โซน 48';
            $fileName = date("Y/m/d") . '-รายการข้อมูลระวาง 4000 โซน 48';
        }

        $sheet->mergeCells('A3:C3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('D3:F3');
        $sheet->setCellValue('D3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'SEQ')
            ->setCellValue('C4', 'หมายเลขระวาง')       
            ->setCellValue('D4', 'SEQ')
            ->setCellValue('E4', 'หมายเลขระวาง')
            ->setCellValue('F4', 'หมายเหตุ')
            ->getStyle('A2:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['INDEX_SEQ_P1'] != $Result[$i]['INDEX_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['INDEX_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['INDEX_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['INDEX_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['INDEX_NAME_P2']);

                    if(empty($Result[$i]['INDEX_SEQ_P1'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลการใช้ประโยชน์ในที่ดิน ในพัฒน์ฯ 1 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูกเพิ่มโดยผู้ดูแลระบบ";


                    }else if(empty($Result[$i]['INDEX_SEQ_P2'])){
                        // $problemDesc  = $problemDesc . "ไม่พบข้อมูลการใช้ประโยชน์ในที่ดิน ในพัฒน์ฯ 2 ";
                        $problemDesc  = $problemDesc . "ข้อมูลถูก ลบ/ยกเลิก โดยผู้ดูแลระบบ";


                    }else{
                        if($Result[$i]['INDEX_NAME_P1'] != $Result[$i]['INDEX_NAME_P2']) $problemDesc  = $problemDesc . "หมายเลขระวาง ไม่ตรงกัน ";
                    }

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['INDEX_SEQ_P1'] == $Result[$i]['INDEX_SEQ_P2'] ){
                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['INDEX_SEQ_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['INDEX_NAME_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['INDEX_SEQ_P2']);
                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['INDEX_NAME_P2']);

                    $sheet->setCellValue('F' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:F'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:F'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);


        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;

    case '5' : //เส้นโครงาน
        $sheet->mergeCells('A1:H1');
        $sheet->mergeCells('A2:H2');
        if($sts == 'e'){
            $sheet->setCellValue('A2', 'รายการข้อมูลเส้นโครงาน ที่แตกต่างกัน' );
            $bn = 'รายการข้อมูลเส้นโครงาน ที่แตกต่างกัน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลเส้นโครงาน ที่แตกต่างกัน';
        }else if($sts == 's'){
            $sheet->setCellValue('A2', 'รายการข้อมูลเส้นโครงาน');
            $bn = 'รายการข้อมูลเส้นโครงาน';
            $fileName = date("Y/m/d") . '-รายการข้อมูลเส้นโครงาน';
        }

        $sheet->mergeCells('A3:D3');
        $sheet->setCellValue('A3', 'ข้อมูลรับมอบจากโครงการพัฒฯ 1');
        $sheet->mergeCells('E3:H3');
        $sheet->setCellValue('E3', 'ถ่ายโอนสำเร็จโครงการพัฒฯ 2');

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'โซน')
            ->setCellValue('C4', 'รหัสจังหวัด')       
            ->setCellValue('D4', 'ชื่อเส้นโครงาน')
            
            ->setCellValue('E4', 'โซน')
            ->setCellValue('F4', 'รหัสจังหวัด')       
            ->setCellValue('G4', 'ชื่อเส้นโครงาน')
            ->setCellValue('H4', 'หมายเหตุ')
            ->getStyle('A2:H4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $last = count($Result);
        for ($i = 0; $i < $last; $i++) {
            if($sts == 'e'){
                if($Result[$i]['TV_ZONE_P1'] != $Result[$i]['TV_ZONE_P2'] && $Result[$i]['TV_PROV_P1'] != $Result[$i]['TV_PROV_P2'] 
                && $Result[$i]['TV_NAME_P1'] != $Result[$i]['TV_NAME_P2'] && $Result[$i]['TV_DIVISION_P1'] != $Result[$i]['TV_DIVISION_P2'] 
                && $Result[$i]['TV_AMOUNT_QTY_P1'] != $Result[$i]['TV_AMOUNT_QTY_P1'] ){

                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TV_ZONE_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TV_PROV_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TV_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TV_ZONE_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TV_PROV_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TV_NAME_P2']);

                    if($Result[$i]['TV_ZONE_P1'] != $Result[$i]['TV_ZONE_P2']) $problemDesc  = $problemDesc . "โซน ไม่ตรงกัน ";
                    if($Result[$i]['TV_PROV_P1'] != $Result[$i]['TV_PROV_P2']) $problemDesc  = $problemDesc . "รหัสจังหวัด ไม่ตรงกัน ";
                    if($Result[$i]['TV_NAME_P1'] != $Result[$i]['TV_NAME_P2']) $problemDesc  = $problemDesc . "ชื่อเส้นโครงาน ไม่ตรงกัน ";

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $problemDesc = "";
                    $current += 1;
                    $count_temp1 += 1;
                }
            }
            else{
                if($Result[$i]['TV_ZONE_P1'] == $Result[$i]['TV_ZONE_P2'] && $Result[$i]['TV_PROV_P1'] == $Result[$i]['TV_PROV_P2'] 
                && $Result[$i]['TV_NAME_P1'] == $Result[$i]['TV_NAME_P2'] && $Result[$i]['TV_DIVISION_P1'] == $Result[$i]['TV_DIVISION_P2'] 
                && $Result[$i]['TV_AMOUNT_QTY_P1'] == $Result[$i]['TV_AMOUNT_QTY_P1'] ){

                    $sheet->setCellValue('A'. (5 + $current), ($current + 1));
                    $sheet->setCellValue('B' . (5 + $current), $Result[$i]['TV_ZONE_P1']);
                    $sheet->setCellValue('C' . (5 + $current), $Result[$i]['TV_PROV_P1']);
                    $sheet->setCellValue('D' . (5 + $current), $Result[$i]['TV_NAME_P1']);

                    $sheet->setCellValue('E' . (5 + $current), $Result[$i]['TV_ZONE_P2']);
                    $sheet->setCellValue('F' . (5 + $current), $Result[$i]['TV_PROV_P2']);
                    $sheet->setCellValue('G' . (5 + $current), $Result[$i]['TV_NAME_P2']);

                    $sheet->setCellValue('H' . (5 + $current), $problemDesc);
                    $current += 1;
                    $count_temp2 += 1;
                }
            }
        }

        if($sts == 'e'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp1));
            $sheet->getStyle('A1:H'.($count_temp1 + 4))->applyFromArray($styleArray);
        }else if($sts == 's'){
            $sheet->setCellValue('A1', 'จำนวนทั้งหมด: '.number_format($count_temp2));
            $sheet->getStyle('A1:H'.($count_temp2 + 4))->applyFromArray($styleArray);
        }
        $sheet->getStyle('A2:H4')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);

        $sheet->getStyle('A5:A' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B5:B' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C5:C' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D5:D' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E5:E' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F5:F' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G5:G' . (count($Result)+4))
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H5:H' . (count($Result)+4))
            ->getAlignment()
            ->setWrapText(true);
    break;



}


$writer = new Xlsx($spreadsheet);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
    $fileName = urlencode($fileName);
}
header('Content-Disposition: attachment; filename="' . $fileName . '.xlsx"');
$writer->save('php://output');
