<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
  
    // $landoffice = $_POST['landoffice'];
    // $amphur = !isset($_POST['amphur'])? '' : $_POST['amphur'];
    // $tambon = !isset($_POST['tambon'])? '' : $_POST['tambon'];
    // $parcelNo = !isset($_POST['parcelNo'])? '' : $_POST['parcelNo']; //รับตัวเดียว
    // $parcelMoo = !isset($_POST['parcelMoo'])? '' : $_POST['parcelMoo'];
    // $parcelYear = !isset($_POST['parcelYear'])? '' : $_POST['parcelYear'];
    // $condoName = !isset($_POST['condoName'])? '' : $_POST['condoName'];
    // $condoroomNo = !isset($_POST['condoroomNo'])? '' : $_POST['condoroomNo'];

    // /// Construct
    // $constrMoo = !isset($_POST['constrMoo'])? '' : $_POST['constrMoo']; //หมู่
    // $constrHno = !isset($_POST['constrHno'])? '' : $_POST['constrHno']; //เลขที่บ้าน
    // $constrHid = !isset($_POST['constrHid'])? '' : $_POST['constrHid']; //รหัสบ้าน
    // $constrName = !isset($_POST['constrName'])? '' : $_POST['constrName']; //ชื่อสิ่งปลูกสร้าง

    $checksys = $_POST['sysType'];
    $checknum = $_POST['type'];
    $Result = array();
    
    switch ($checksys) {
        case 'adm': //1
            include 'queryADMData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'aps': //2
            include 'queryAPSData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'ctn': //3
            include 'queryCTNData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'exp': //4
            include 'queryEXPData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'fin': //5
            include 'queryFINData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'gis': //6
            include 'queryGISData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'hrm': //7
            include 'queryHRMData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'inv': //8
            include 'queryINVData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'mas': //9
            include 'queryMASData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'reg': //10
            include 'queryREGData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'evd': //11
            include 'queryEVDData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'sva': //12
            include 'querySVAData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'svc': //13
            include 'querySVCData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'usp': //14
            include 'queryUSPData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'esp': //15
            include 'queryESPData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'ech': //16
            include 'queryECHData.php';
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;
    }

    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);


?>