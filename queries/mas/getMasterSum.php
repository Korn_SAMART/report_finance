<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';

    $checknum = $_POST['type'];
    $Result = array();
    
    switch ($checknum) {
        case 'adm': //1
            $select = "WITH P1 AS(
                        SELECT 1 AS NUM, 'ข้อมูลสิทธิ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_ADM_MAS_PERMISSION
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ข้อมูลระบบงาน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_ADM_MAS_SYSTEM
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ข้อมูลหน้าจอ' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_ADM_MAS_SCREEN
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 4 AS NUM, 'ข้อมูลกลุ่มผู้ใช้งาน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_ADM_MAS_USER_GROUP
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 5 AS NUM, 'ข้อมูลกลุ่มเมนู' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_ADM_MENU_GROUP
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 6 AS NUM, 'ข้อมูลกลุ่มตำแหน่ง' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_ADM_MAS_POSITION_GROUP
                        WHERE RECORD_STATUS = 'N'
                        ),
                    P2 AS(
                    SELECT 1 AS NUM, 'ข้อมูลสิทธิ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM ADM.TB_ADM_MAS_PERMISSION
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ข้อมูลระบบงาน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM ADM.TB_ADM_MAS_SYSTEM
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ข้อมูลหน้าจอ' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM ADM.TB_ADM_MAS_SCREEN
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 4 AS NUM, 'ข้อมูลกลุ่มผู้ใช้งาน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM ADM.TB_ADM_MAS_USER_GROUP
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 5 AS NUM, 'ข้อมูลกลุ่มเมนู' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM ADM.TB_ADM_MENU_GROUP
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 6 AS NUM, 'ข้อมูลกลุ่มตำแหน่ง' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM ADM.TB_ADM_MAS_POSITION_GROUP
                        WHERE RECORD_STATUS = 'N'
                        ),
                    DP2 AS(
                        SELECT 1 AS NUM, 'ข้อมูลสิทธิ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_ADM_MAS_PERMISSION 
                        WHERE RECORD_STATUS = 'N' 
                        AND PERMISSION_SEQ  
                        NOT IN( SELECT PERMISSION_SEQ
                                FROM ADM.TB_ADM_MAS_PERMISSION
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ข้อมูลระบบงาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_ADM_MAS_SYSTEM 
                        WHERE RECORD_STATUS = 'N' 
                        AND SYSTEM_SEQ 
                        NOT IN( SELECT SYSTEM_SEQ 
                                FROM ADM.TB_ADM_MAS_SYSTEM
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ข้อมูลหน้าจอ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_ADM_MAS_SCREEN 
                        WHERE RECORD_STATUS = 'N' 
                        AND SCREEN_SEQ 
                        NOT IN( SELECT SCREEN_SEQ 
                                FROM ADM.TB_ADM_MAS_SCREEN
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'ข้อมูลกลุ่มผู้ใช้งาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_ADM_MAS_USER_GROUP 
                        WHERE RECORD_STATUS = 'N' 
                        AND USER_GROUP_SEQ 
                        NOT IN( SELECT USER_GROUP_SEQ 
                                FROM ADM.TB_ADM_MAS_USER_GROUP
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 5 AS NUM, 'ข้อมูลกลุ่มเมนู' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_ADM_MENU_GROUP 
                        WHERE RECORD_STATUS = 'N' 
                        AND MENU_GROUP_SEQ 
                        NOT IN( SELECT MENU_GROUP_SEQ 
                                FROM ADM.TB_ADM_MENU_GROUP
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 6 AS NUM, 'ข้อมูลกลุ่มตำแหน่ง' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_ADM_MAS_POSITION_GROUP 
                        WHERE RECORD_STATUS = 'N' 
                        AND POSITION_GROUP_SEQ 
                        NOT IN( SELECT POSITION_GROUP_SEQ 
                                FROM ADM.TB_ADM_MAS_POSITION_GROUP
                                WHERE RECORD_STATUS = 'N')
                        ),
                    AP2 AS(
                        SELECT 1 AS NUM, 'ข้อมูลสิทธิ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM ADM.TB_ADM_MAS_PERMISSION 
                        WHERE RECORD_STATUS = 'N' 
                        AND PERMISSION_SEQ  
                        NOT IN( SELECT PERMISSION_SEQ
                                FROM MGT1.TB_ADM_MAS_PERMISSION
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ข้อมูลระบบงาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM ADM.TB_ADM_MAS_SYSTEM 
                        WHERE RECORD_STATUS = 'N' 
                        AND SYSTEM_SEQ 
                        NOT IN( SELECT SYSTEM_SEQ 
                                FROM MGT1.TB_ADM_MAS_SYSTEM
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ข้อมูลหน้าจอ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM ADM.TB_ADM_MAS_SCREEN 
                        WHERE RECORD_STATUS = 'N' 
                        AND SCREEN_SEQ 
                        NOT IN( SELECT SCREEN_SEQ 
                                FROM MGT1.TB_ADM_MAS_SCREEN
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'ข้อมูลกลุ่มผู้ใช้งาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM ADM.TB_ADM_MAS_USER_GROUP 
                        WHERE RECORD_STATUS = 'N' 
                        AND USER_GROUP_SEQ 
                        NOT IN( SELECT USER_GROUP_SEQ 
                                FROM MGT1.TB_ADM_MAS_USER_GROUP
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 5 AS NUM, 'ข้อมูลกลุ่มเมนู' AS TYPE, COUNT(*) AS RECEIVE
                        FROM ADM.TB_ADM_MENU_GROUP 
                        WHERE RECORD_STATUS = 'N' 
                        AND MENU_GROUP_SEQ 
                        NOT IN( SELECT MENU_GROUP_SEQ 
                                FROM MGT1.TB_ADM_MENU_GROUP
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 6 AS NUM, 'ข้อมูลกลุ่มตำแหน่ง' AS TYPE, COUNT(*) AS RECEIVE
                        FROM ADM.TB_ADM_MAS_POSITION_GROUP 
                        WHERE RECORD_STATUS = 'N' 
                        AND POSITION_GROUP_SEQ 
                        NOT IN( SELECT POSITION_GROUP_SEQ 
                                FROM MGT1.TB_ADM_MAS_POSITION_GROUP
                                WHERE RECORD_STATUS = 'N')
                        )
                    SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
                    NVL(P1.RECEIVE,0) AS P1, 
                    NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
                    FROM P2
                    FULL OUTER JOIN  P1
                        ON P1.TYPE = P2.TYPE
                    FULL OUTER JOIN  AP2
                        ON P1.TYPE = AP2.TYPE
                    FULL OUTER JOIN  DP2
                        ON P1.TYPE = DP2.TYPE
                    ORDER BY NVL(P1.NUM,P2.NUM)";

            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'aps': //2
            $select = "WITH P1 AS(
                        SELECT 1 AS NUM, 'ข้อมูลห้องชุด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_APS_MAS_ROOM_USAGE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ข้อมูลพื้นที่' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_APS_MAS_POSSESSORY
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ข้อมูลชั้น' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_APS_MAS_CONDO_FLOOR
                        WHERE RECORD_STATUS = 'N'
                        ),
                    P2 AS(
                        SELECT 1 AS NUM, 'ข้อมูลห้องชุด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM APS.TB_APS_MAS_ROOM_USAGE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ข้อมูลพื้นที่' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM APS.TB_APS_MAS_POSSESSORY
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ข้อมูลชั้น' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM APS.TB_APS_MAS_CONDO_FLOOR
                        WHERE RECORD_STATUS = 'N'
                        ),
                    DP2 AS(
                        SELECT 1 AS NUM, 'ข้อมูลห้องชุด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_APS_MAS_ROOM_USAGE 
                        WHERE RECORD_STATUS = 'N' 
                        AND ROOM_USAGE_SEQ  
                        NOT IN( SELECT ROOM_USAGE_SEQ
                                FROM APS.TB_APS_MAS_ROOM_USAGE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ข้อมูลพื้นที่' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_APS_MAS_POSSESSORY 
                        WHERE RECORD_STATUS = 'N' 
                        AND POSSESSORY_SEQ 
                        NOT IN( SELECT POSSESSORY_SEQ 
                                FROM APS.TB_APS_MAS_POSSESSORY
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ข้อมูลชั้น' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_APS_MAS_CONDO_FLOOR 
                        WHERE RECORD_STATUS = 'N' 
                        AND FLOOR_SEQ 
                        NOT IN( SELECT FLOOR_SEQ 
                                FROM APS.TB_APS_MAS_CONDO_FLOOR
                                WHERE RECORD_STATUS = 'N')
                        ),
                    AP2 AS(
                        SELECT 1 AS NUM, 'ข้อมูลห้องชุด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM APS.TB_APS_MAS_ROOM_USAGE 
                        WHERE RECORD_STATUS = 'N' 
                        AND ROOM_USAGE_SEQ  
                        NOT IN( SELECT ROOM_USAGE_SEQ
                                FROM MGT1.TB_APS_MAS_ROOM_USAGE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ข้อมูลพื้นที่' AS TYPE, COUNT(*) AS RECEIVE
                        FROM APS.TB_APS_MAS_POSSESSORY 
                        WHERE RECORD_STATUS = 'N' 
                        AND POSSESSORY_SEQ 
                        NOT IN( SELECT POSSESSORY_SEQ 
                                FROM MGT1.TB_APS_MAS_POSSESSORY
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ข้อมูลชั้น' AS TYPE, COUNT(*) AS RECEIVE
                        FROM APS.TB_APS_MAS_CONDO_FLOOR 
                        WHERE RECORD_STATUS = 'N' 
                        AND FLOOR_SEQ 
                        NOT IN( SELECT FLOOR_SEQ 
                                FROM MGT1.TB_APS_MAS_CONDO_FLOOR
                                WHERE RECORD_STATUS = 'N')
                        )
                    SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
                    NVL(P1.RECEIVE,0) AS P1, 
                    NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
                    FROM P2
                    FULL OUTER JOIN  P1
                        ON P1.TYPE = P2.TYPE
                    FULL OUTER JOIN  AP2
                        ON P1.TYPE = AP2.TYPE
                    FULL OUTER JOIN  DP2
                        ON P1.TYPE = DP2.TYPE
                    ORDER BY NVL(P1.NUM,P2.NUM)";

            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'ctn': //3
            $select = "WITH P1 AS(
                        SELECT 1 AS NUM, 'ข้อมูลประเภทหนังสือ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_CTN_MAS_DOC_CLASS
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ข้อมูลชั้นความลับ' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_CTN_MAS_DOC_SECRET
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ข้อมูลหมวดหมู่เอกสาร' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_CTN_MAS_DOC_TYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 4 AS NUM, 'ข้อมูลเลขหนังสือของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_CTN_MAS_LANDOFFICE_LETTER
                        WHERE RECORD_STATUS = 'N'
                        ),
                    P2 AS(
                        SELECT 1 AS NUM, 'ข้อมูลประเภทหนังสือ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM CTN.TB_CTN_MAS_DOC_CLASS
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ข้อมูลชั้นความลับ' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM CTN.TB_CTN_MAS_DOC_SECRET
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ข้อมูลหมวดหมู่เอกสาร' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM CTN.TB_CTN_MAS_DOC_TYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 4 AS NUM, 'ข้อมูลเลขหนังสือของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM CTN.TB_CTN_MAS_LANDOFFICE_LETTER
                        WHERE RECORD_STATUS = 'N'
                        ),
                    DP2 AS(
                        SELECT 1 AS NUM, 'ข้อมูลประเภทหนังสือ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_CTN_MAS_DOC_CLASS 
                        WHERE RECORD_STATUS = 'N' 
                        AND DOC_CLASS_SEQ  
                        NOT IN( SELECT DOC_CLASS_SEQ
                                FROM CTN.TB_CTN_MAS_DOC_CLASS
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ข้อมูลชั้นความลับ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_CTN_MAS_DOC_SECRET 
                        WHERE RECORD_STATUS = 'N' 
                        AND DOC_SECRET_SEQ 
                        NOT IN( SELECT DOC_SECRET_SEQ 
                                FROM CTN.TB_CTN_MAS_DOC_SECRET
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ข้อมูลหมวดหมู่เอกสาร' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_CTN_MAS_DOC_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND DOC_TYPE_SEQ 
                        NOT IN( SELECT DOC_TYPE_SEQ 
                                FROM CTN.TB_CTN_MAS_DOC_TYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'ข้อมูลเลขหนังสือของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_CTN_MAS_LANDOFFICE_LETTER 
                        WHERE RECORD_STATUS = 'N' 
                        AND LANDOFFICE_SEQ 
                        NOT IN( SELECT LANDOFFICE_SEQ 
                                FROM CTN.TB_CTN_MAS_LANDOFFICE_LETTER
                                WHERE RECORD_STATUS = 'N')
                        ),
                    AP2 AS(
                        SELECT 1 AS NUM, 'ข้อมูลประเภทหนังสือ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM CTN.TB_CTN_MAS_DOC_CLASS 
                        WHERE RECORD_STATUS = 'N' 
                        AND DOC_CLASS_SEQ  
                        NOT IN( SELECT DOC_CLASS_SEQ
                                FROM MGT1.TB_CTN_MAS_DOC_CLASS
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ข้อมูลชั้นความลับ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM CTN.TB_CTN_MAS_DOC_SECRET 
                        WHERE RECORD_STATUS = 'N' 
                        AND DOC_SECRET_SEQ 
                        NOT IN( SELECT DOC_SECRET_SEQ 
                                FROM MGT1.TB_CTN_MAS_DOC_SECRET
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ข้อมูลหมวดหมู่เอกสาร' AS TYPE, COUNT(*) AS RECEIVE
                        FROM CTN.TB_CTN_MAS_DOC_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND DOC_TYPE_SEQ 
                        NOT IN( SELECT DOC_TYPE_SEQ 
                                FROM MGT1.TB_CTN_MAS_DOC_TYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'ข้อมูลเลขหนังสือของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM CTN.TB_CTN_MAS_LANDOFFICE_LETTER 
                        WHERE RECORD_STATUS = 'N' 
                        AND LANDOFFICE_SEQ 
                        NOT IN( SELECT LANDOFFICE_SEQ 
                                FROM MGT1.TB_CTN_MAS_LANDOFFICE_LETTER
                                WHERE RECORD_STATUS = 'N')
                        )
                    SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
                    NVL(P1.RECEIVE,0) AS P1, 
                    NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
                    FROM P2
                    FULL OUTER JOIN  P1
                        ON P1.TYPE = P2.TYPE
                    FULL OUTER JOIN  AP2
                        ON P1.TYPE = AP2.TYPE
                    FULL OUTER JOIN  DP2
                        ON P1.TYPE = DP2.TYPE
                    ORDER BY NVL(P1.NUM,P2.NUM)";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'exp': //4
            $select = "WITH P1 AS(
                SELECT 1 AS NUM, 'ข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_EXP_MAS_ILLEGALITY_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ข้อมูลประเภทงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_JOB_GROUP
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ข้อมูลขั้นตอนการดำเนินการ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_PROC_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4 AS NUM, 'ข้อมูลประเภทการร้องเรียน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_REQUEST_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 5 AS NUM, 'ข้อมูลเรื่อง' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_SUB_JOB
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 6 AS NUM, 'ข้อมูลประเภทการขอใช้ประโยชน์' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_USE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 7 AS NUM, 'ข้อมูลสถานะงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_EXP_MAS_WORK_STS
                WHERE RECORD_STATUS = 'N'
                ),
            P2 AS(
                SELECT 1 AS NUM, 'ข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย' AS TYPE, COUNT(*) AS RECEIVE
                FROM EXP.TB_EXP_MAS_ILLEGALITY_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ข้อมูลประเภทงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM EXP.TB_EXP_MAS_JOB_GROUP
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ข้อมูลขั้นตอนการดำเนินการ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM EXP.TB_EXP_MAS_PROC_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4 AS NUM, 'ข้อมูลประเภทการร้องเรียน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM EXP.TB_EXP_MAS_REQUEST_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 5 AS NUM, 'ข้อมูลเรื่อง' AS TYPE, COUNT(*) AS RECEIVE  
                FROM EXP.TB_EXP_MAS_SUB_JOB
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 6 AS NUM, 'ข้อมูลประเภทการขอใช้ประโยชน์' AS TYPE, COUNT(*) AS RECEIVE  
                FROM EXP.TB_EXP_MAS_USE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 7 AS NUM, 'ข้อมูลสถานะงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM EXP.TB_EXP_MAS_WORK_STS
                WHERE RECORD_STATUS = 'N'
                ),
            DP2 AS(
                SELECT 1 AS NUM, 'ข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_EXP_MAS_ILLEGALITY_TYPE 
                WHERE RECORD_STATUS = 'N' 
                AND ILLEGALITY_TYPE_NAME  
                NOT IN( SELECT ILLEGALITY_TYPE_NAME
                        FROM EXP.TB_EXP_MAS_ILLEGALITY_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ข้อมูลประเภทงาน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_EXP_MAS_JOB_GROUP 
                WHERE RECORD_STATUS = 'N' 
                AND JOB_GROUP_SEQ 
                NOT IN( SELECT JOB_GROUP_SEQ 
                        FROM EXP.TB_EXP_MAS_JOB_GROUP
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'ข้อมูลขั้นตอนการดำเนินการ' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_EXP_MAS_PROC_TYPE 
                WHERE RECORD_STATUS = 'N' 
                AND PROC_TYPE_SEQ 
                NOT IN( SELECT PROC_TYPE_SEQ 
                        FROM EXP.TB_EXP_MAS_PROC_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 4 AS NUM, 'ข้อมูลประเภทการร้องเรียน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_EXP_MAS_REQUEST_TYPE 
                WHERE RECORD_STATUS = 'N' 
                AND REQUEST_NAME 
                NOT IN( SELECT REQUEST_NAME 
                        FROM EXP.TB_EXP_MAS_REQUEST_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 5 AS NUM, 'ข้อมูลเรื่อง' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_EXP_MAS_SUB_JOB 
                WHERE RECORD_STATUS = 'N' 
                AND SUB_JOB_SEQ 
                NOT IN( SELECT SUB_JOB_SEQ 
                        FROM EXP.TB_EXP_MAS_SUB_JOB
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 6 AS NUM, 'ข้อมูลประเภทการขอใช้ประโยชน์' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_EXP_MAS_USE_TYPE 
                WHERE RECORD_STATUS = 'N' 
                AND USE_TYPE_NAME 
                NOT IN( SELECT USE_TYPE_NAME 
                        FROM EXP.TB_EXP_MAS_USE_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 7 AS NUM, 'ข้อมูลสถานะงาน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_EXP_MAS_WORK_STS 
                WHERE RECORD_STATUS = 'N' 
                AND WORK_STS_SEQ 
                NOT IN( SELECT WORK_STS_SEQ 
                        FROM EXP.TB_EXP_MAS_WORK_STS
                        WHERE RECORD_STATUS = 'N')
                ),
            AP2 AS(
                SELECT 1 AS NUM, 'ข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย' AS TYPE, COUNT(*) AS RECEIVE
                FROM EXP.TB_EXP_MAS_ILLEGALITY_TYPE 
                WHERE RECORD_STATUS = 'N' 
                AND ILLEGALITY_TYPE_NAME  
                NOT IN( SELECT ILLEGALITY_TYPE_NAME
                        FROM MGT1.TB_EXP_MAS_ILLEGALITY_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ข้อมูลประเภทงาน' AS TYPE, COUNT(*) AS RECEIVE
                FROM EXP.TB_EXP_MAS_JOB_GROUP 
                WHERE RECORD_STATUS = 'N' 
                AND JOB_GROUP_SEQ 
                NOT IN( SELECT JOB_GROUP_SEQ 
                        FROM MGT1.TB_EXP_MAS_JOB_GROUP
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'ข้อมูลขั้นตอนการดำเนินการ' AS TYPE, COUNT(*) AS RECEIVE
                FROM EXP.TB_EXP_MAS_PROC_TYPE 
                WHERE RECORD_STATUS = 'N' 
                AND PROC_TYPE_SEQ 
                NOT IN( SELECT PROC_TYPE_SEQ 
                        FROM MGT1.TB_EXP_MAS_PROC_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 4 AS NUM, 'ข้อมูลประเภทการร้องเรียน' AS TYPE, COUNT(*) AS RECEIVE
                FROM EXP.TB_EXP_MAS_REQUEST_TYPE 
                WHERE RECORD_STATUS = 'N' 
                AND REQUEST_NAME 
                NOT IN( SELECT REQUEST_NAME 
                        FROM MGT1.TB_EXP_MAS_REQUEST_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 5 AS NUM, 'ข้อมูลเรื่อง' AS TYPE, COUNT(*) AS RECEIVE
                FROM EXP.TB_EXP_MAS_SUB_JOB 
                WHERE RECORD_STATUS = 'N' 
                AND SUB_JOB_SEQ 
                NOT IN( SELECT SUB_JOB_SEQ 
                        FROM MGT1.TB_EXP_MAS_SUB_JOB
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 6 AS NUM, 'ข้อมูลประเภทการขอใช้ประโยชน์' AS TYPE, COUNT(*) AS RECEIVE
                FROM EXP.TB_EXP_MAS_USE_TYPE 
                WHERE RECORD_STATUS = 'N' 
                AND USE_TYPE_NAME 
                NOT IN( SELECT USE_TYPE_NAME 
                        FROM MGT1.TB_EXP_MAS_USE_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 7 AS NUM, 'ข้อมูลสถานะงาน' AS TYPE, COUNT(*) AS RECEIVE
                FROM EXP.TB_EXP_MAS_WORK_STS 
                WHERE RECORD_STATUS = 'N' 
                AND WORK_STS_SEQ 
                NOT IN( SELECT WORK_STS_SEQ 
                        FROM MGT1.TB_EXP_MAS_WORK_STS
                        WHERE RECORD_STATUS = 'N')
                )
            SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
            NVL(P1.RECEIVE,0) AS P1, 
            NVL(P2.RECEIVE,0) AS P2,
            AP2.RECEIVE AS AP2RECEIVE,
            DP2.RECEIVE AS DP2RECEIVE
            FROM P2
            FULL OUTER JOIN  P1
                ON P1.TYPE = P2.TYPE
            FULL OUTER JOIN  AP2
                 ON P1.TYPE = AP2.TYPE
            FULL OUTER JOIN  DP2
                 ON P1.TYPE = DP2.TYPE
            ORDER BY NVL(P1.NUM,P2.NUM)";

            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'fin': //5
            $select = "WITH P1 AS(
                        SELECT 1 AS NUM, 'การตั้งค่าในระบบ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_FIN_MAS_CONFIG
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ประเภทการขอเบิก' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_FIN_MAS_FUND_TYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'หมวดหมู่เงิน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_FIN_MAS_INCOME
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 4 AS NUM, 'รายได้' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_FIN_MAS_REV
                        WHERE RECORD_STATUS = 'N'
                        ),
                    P2 AS(
                        SELECT 1 AS NUM, 'การตั้งค่าในระบบ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM FIN.TB_FIN_MAS_CONFIG
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ประเภทการขอเบิก' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM FIN.TB_FIN_MAS_FUND_TYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'หมวดหมู่เงิน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM FIN.TB_FIN_MAS_INCOME
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 4 AS NUM, 'รายได้' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM FIN.TB_FIN_MAS_REV
                        WHERE RECORD_STATUS = 'N'
                        ),
                    DP2 AS(
                        SELECT 1 AS NUM, 'การตั้งค่าในระบบ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_FIN_MAS_CONFIG 
                        WHERE RECORD_STATUS = 'N' 
                        AND CONFIG_NO  
                        NOT IN( SELECT CONFIG_NO
                                FROM FIN.TB_FIN_MAS_CONFIG
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ประเภทการขอเบิก' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_FIN_MAS_FUND_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND FUND_TYPE_ID 
                        NOT IN( SELECT FUND_TYPE_ID 
                                FROM FIN.TB_FIN_MAS_FUND_TYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'หมวดหมู่เงิน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_FIN_MAS_INCOME 
                        WHERE RECORD_STATUS = 'N' 
                        AND INCOME_ID 
                        NOT IN( SELECT INCOME_ID 
                                FROM FIN.TB_FIN_MAS_INCOME
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'รายได้' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_FIN_MAS_REV 
                        WHERE RECORD_STATUS = 'N' 
                        AND REVENUE_ID 
                        NOT IN( SELECT REVENUE_ID 
                                FROM FIN.TB_FIN_MAS_REV
                                WHERE RECORD_STATUS = 'N')
                        ),
                    AP2 AS(
                        SELECT 1 AS NUM, 'การตั้งค่าในระบบ' AS TYPE, COUNT(*) AS RECEIVE
                        FROM FIN.TB_FIN_MAS_CONFIG 
                        WHERE RECORD_STATUS = 'N' 
                        AND CONFIG_NO  
                        NOT IN( SELECT CONFIG_NO
                                FROM MGT1.TB_FIN_MAS_CONFIG
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ประเภทการขอเบิก' AS TYPE, COUNT(*) AS RECEIVE
                        FROM FIN.TB_FIN_MAS_FUND_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND FUND_TYPE_ID 
                        NOT IN( SELECT FUND_TYPE_ID 
                                FROM MGT1.TB_FIN_MAS_FUND_TYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'หมวดหมู่เงิน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM FIN.TB_FIN_MAS_INCOME 
                        WHERE RECORD_STATUS = 'N' 
                        AND INCOME_ID 
                        NOT IN( SELECT INCOME_ID 
                                FROM MGT1.TB_FIN_MAS_INCOME
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'รายได้' AS TYPE, COUNT(*) AS RECEIVE
                        FROM FIN.TB_FIN_MAS_REV 
                        WHERE RECORD_STATUS = 'N' 
                        AND REVENUE_ID 
                        NOT IN( SELECT REVENUE_ID 
                                FROM MGT1.TB_FIN_MAS_REV
                                WHERE RECORD_STATUS = 'N')
                        )
                    SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
                    NVL(P1.RECEIVE,0) AS P1, 
                    NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
                    FROM P2
                    FULL OUTER JOIN  P1
                        ON P1.TYPE = P2.TYPE
                    FULL OUTER JOIN  AP2
                        ON P1.TYPE = AP2.TYPE
                    FULL OUTER JOIN  DP2
                        ON P1.TYPE = DP2.TYPE
                    ORDER BY NVL(P1.NUM,P2.NUM)";
            
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);

            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'gis': //6
            $select = "WITH P1 AS(
                SELECT 1 AS NUM, 'ตารางอ้างอิงข้อมูลพิกัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_GIS_MAS_CRS
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ตารางคำอธิบายข้อมูล' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_GIS_MAS_DEFINITION_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ตารางกลุ่มสินค้า' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_GIS_MAS_PRODUCT_GROUP
                WHERE RECORD_STATUS = 'N'
                ),
            P2 AS(
                SELECT 1 AS NUM, 'ตารางอ้างอิงข้อมูลพิกัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM GIS.TB_GIS_MAS_CRS
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ตารางคำอธิบายข้อมูล' AS TYPE, COUNT(*) AS RECEIVE  
                FROM GIS.TB_GIS_MAS_DEFINITION_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ตารางกลุ่มสินค้า' AS TYPE, COUNT(*) AS RECEIVE  
                FROM GIS.TB_GIS_MAS_PRODUCT_GROUP
                WHERE RECORD_STATUS = 'N'
                ),
            DP2 AS(
                SELECT 1 AS NUM, 'ตารางอ้างอิงข้อมูลพิกัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_GIS_MAS_CRS 
                WHERE RECORD_STATUS = 'N' 
                AND CRS_CODE  
                NOT IN( SELECT CRS_CODE
                        FROM GIS.TB_GIS_MAS_CRS
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ตารางคำอธิบายข้อมูล' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_GIS_MAS_DEFINITION_TYPE 
                WHERE RECORD_STATUS = 'N' 
                AND DEFINIT_SEQ 
                NOT IN( SELECT DEFINIT_SEQ 
                        FROM GIS.TB_GIS_MAS_DEFINITION_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'ตารางกลุ่มสินค้า' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_GIS_MAS_PRODUCT_GROUP 
                WHERE RECORD_STATUS = 'N' 
                AND PRODUCT_GROUP_SEQ 
                NOT IN( SELECT PRODUCT_GROUP_SEQ 
                        FROM GIS.TB_GIS_MAS_PRODUCT_GROUP
                        WHERE RECORD_STATUS = 'N')
                ),
            AP2 AS(
                SELECT 1 AS NUM, 'ตารางอ้างอิงข้อมูลพิกัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM GIS.TB_GIS_MAS_CRS 
                WHERE RECORD_STATUS = 'N' 
                AND CRS_CODE  
                NOT IN( SELECT CRS_CODE
                        FROM MGT1.TB_GIS_MAS_CRS
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ตารางคำอธิบายข้อมูล' AS TYPE, COUNT(*) AS RECEIVE
                FROM GIS.TB_GIS_MAS_DEFINITION_TYPE 
                WHERE RECORD_STATUS = 'N' 
                AND DEFINIT_SEQ 
                NOT IN( SELECT DEFINIT_SEQ 
                        FROM MGT1.TB_GIS_MAS_DEFINITION_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'ตารางกลุ่มสินค้า' AS TYPE, COUNT(*) AS RECEIVE
                FROM GIS.TB_GIS_MAS_PRODUCT_GROUP 
                WHERE RECORD_STATUS = 'N' 
                AND PRODUCT_GROUP_SEQ 
                NOT IN( SELECT PRODUCT_GROUP_SEQ 
                        FROM MGT1.TB_GIS_MAS_PRODUCT_GROUP
                        WHERE RECORD_STATUS = 'N')
                )
            SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
            NVL(P1.RECEIVE,0) AS P1, 
            NVL(P2.RECEIVE,0) AS P2,
            AP2.RECEIVE AS AP2RECEIVE,
            DP2.RECEIVE AS DP2RECEIVE
            FROM P2
            FULL OUTER JOIN  P1
                ON P1.TYPE = P2.TYPE
            FULL OUTER JOIN  AP2
                 ON P1.TYPE = AP2.TYPE
            FULL OUTER JOIN  DP2
                 ON P1.TYPE = DP2.TYPE
            ORDER BY NVL(P1.NUM,P2.NUM)";

            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'hrm': //7
            $select = "WITH P1 AS(
                SELECT 1 AS NUM, 'ตำแหน่ง' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_HRM_MAS_DIV_POSITION
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ประเภทบุคลากร' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_HRM_MAS_PSNTYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'สิทธิ์การลา' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_HRM_MAS_LEAVE_RIGHT
                WHERE RECORD_STATUS = 'N'
                ),
            P2 AS(
                SELECT 1 AS NUM, 'ตำแหน่ง' AS TYPE, COUNT(*) AS RECEIVE
                FROM HRM.TB_HRM_MAS_DIV_POSITION
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ประเภทบุคลากร' AS TYPE, COUNT(*) AS RECEIVE  
                FROM HRM.TB_HRM_MAS_PSNTYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'สิทธิ์การลา' AS TYPE, COUNT(*) AS RECEIVE  
                FROM HRM.TB_HRM_MAS_LEAVE_RIGHT
                WHERE RECORD_STATUS = 'N'
                ),
            DP2 AS(
                SELECT 1 AS NUM, 'ตำแหน่ง' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_HRM_MAS_DIV_POSITION 
                WHERE RECORD_STATUS = 'N' 
                AND DIV_POSITION_SEQ  
                NOT IN( SELECT DIV_POSITION_SEQ
                        FROM HRM.TB_HRM_MAS_DIV_POSITION
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ประเภทบุคลากร' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_HRM_MAS_PSNTYPE 
                WHERE RECORD_STATUS = 'N' 
                AND PSN_TYPE_SEQ 
                NOT IN( SELECT PSN_TYPE_SEQ 
                        FROM HRM.TB_HRM_MAS_PSNTYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'สิทธิ์การลา' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_HRM_MAS_LEAVE_RIGHT 
                WHERE RECORD_STATUS = 'N' 
                AND LEAVE_RIGHT_SEQ 
                NOT IN( SELECT LEAVE_RIGHT_SEQ 
                        FROM HRM.TB_HRM_MAS_LEAVE_RIGHT
                        WHERE RECORD_STATUS = 'N')
                ),
            AP2 AS(
                SELECT 1 AS NUM, 'ตำแหน่ง' AS TYPE, COUNT(*) AS RECEIVE
                FROM HRM.TB_HRM_MAS_DIV_POSITION 
                WHERE RECORD_STATUS = 'N' 
                AND DIV_POSITION_SEQ  
                NOT IN( SELECT DIV_POSITION_SEQ
                        FROM MGT1.TB_HRM_MAS_DIV_POSITION
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ประเภทบุคลากร' AS TYPE, COUNT(*) AS RECEIVE
                FROM HRM.TB_HRM_MAS_PSNTYPE 
                WHERE RECORD_STATUS = 'N' 
                AND PSN_TYPE_SEQ 
                NOT IN( SELECT PSN_TYPE_SEQ 
                        FROM MGT1.TB_HRM_MAS_PSNTYPE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'สิทธิ์การลา' AS TYPE, COUNT(*) AS RECEIVE
                FROM HRM.TB_HRM_MAS_LEAVE_RIGHT 
                WHERE RECORD_STATUS = 'N' 
                AND LEAVE_RIGHT_SEQ 
                NOT IN( SELECT LEAVE_RIGHT_SEQ 
                        FROM MGT1.TB_HRM_MAS_LEAVE_RIGHT
                        WHERE RECORD_STATUS = 'N')
                )
            SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
            NVL(P1.RECEIVE,0) AS P1, 
            NVL(P2.RECEIVE,0) AS P2,
            AP2.RECEIVE AS AP2RECEIVE,
            DP2.RECEIVE AS DP2RECEIVE
            FROM P2
            FULL OUTER JOIN  P1
                ON P1.TYPE = P2.TYPE
            FULL OUTER JOIN  AP2
                 ON P1.TYPE = AP2.TYPE
            FULL OUTER JOIN  DP2
                 ON P1.TYPE = DP2.TYPE
            ORDER BY NVL(P1.NUM,P2.NUM)";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'inv': //8
            $select = "WITH P1 AS(
                SELECT 1 AS NUM, 'ประเภทครุภัณฑ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_INV_MAS_TYPE_ASSET
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ประเภทและชื่อวัสดุ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_INV_MAS_MAT
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ประเภทและชื่อแบบพิมพ์' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_INV_MAS_PLATE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4 AS NUM, 'ประเภทหลักเขต' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_INV_MAS_PIECE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 5 AS NUM, 'ประเภทและชื่ออาคารและสิ่งก่อสร้าง' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_INV_MAS_BLD
                WHERE RECORD_STATUS = 'N'
                ),
            P2 AS(
                SELECT 1 AS NUM, 'ประเภทครุภัณฑ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM INV.TB_INV_MAS_TYPE_ASSET
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ประเภทและชื่อวัสดุ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM INV.TB_INV_MAS_MAT
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ประเภทและชื่อแบบพิมพ์' AS TYPE, COUNT(*) AS RECEIVE  
                FROM INV.TB_INV_MAS_PLATE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4 AS NUM, 'ประเภทหลักเขต' AS TYPE, COUNT(*) AS RECEIVE  
                FROM INV.TB_INV_MAS_PIECE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 5 AS NUM, 'ประเภทและชื่ออาคารและสิ่งก่อสร้าง' AS TYPE, COUNT(*) AS RECEIVE  
                FROM INV.TB_INV_MAS_BLD
                WHERE RECORD_STATUS = 'N'
                ),
            DP2 AS(
                SELECT 1 AS NUM, 'ประเภทครุภัณฑ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_INV_MAS_TYPE_ASSET 
                WHERE RECORD_STATUS = 'N' 
                AND TYPE_ASSET_SEQ  
                NOT IN( SELECT TYPE_ASSET_SEQ
                        FROM INV.TB_INV_MAS_TYPE_ASSET
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ประเภทและชื่อวัสดุ' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_INV_MAS_MAT 
                WHERE RECORD_STATUS = 'N' 
                AND MAT_SEQ 
                NOT IN( SELECT MAT_SEQ 
                        FROM INV.TB_INV_MAS_MAT
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'ประเภทและชื่อแบบพิมพ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_INV_MAS_PLATE 
                WHERE RECORD_STATUS = 'N' 
                AND PLATE_SEQ 
                NOT IN( SELECT PLATE_SEQ 
                        FROM INV.TB_INV_MAS_PLATE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 4 AS NUM, 'ประเภทหลักเขต' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_INV_MAS_PIECE 
                WHERE RECORD_STATUS = 'N' 
                AND PIECE_SEQ 
                NOT IN( SELECT PIECE_SEQ 
                        FROM INV.TB_INV_MAS_PIECE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 5 AS NUM, 'ประเภทและชื่ออาคารและสิ่งก่อสร้าง' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_INV_MAS_BLD 
                WHERE RECORD_STATUS = 'N' 
                AND BLD_SEQ 
                NOT IN( SELECT BLD_SEQ 
                        FROM INV.TB_INV_MAS_BLD
                        WHERE RECORD_STATUS = 'N')
                ),
            AP2 AS(
                SELECT 1 AS NUM, 'ประเภทครุภัณฑ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM INV.TB_INV_MAS_TYPE_ASSET 
                WHERE RECORD_STATUS = 'N' 
                AND TYPE_ASSET_SEQ  
                NOT IN( SELECT TYPE_ASSET_SEQ
                        FROM MGT1.TB_INV_MAS_TYPE_ASSET
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ประเภทและชื่อวัสดุ' AS TYPE, COUNT(*) AS RECEIVE
                FROM INV.TB_INV_MAS_MAT 
                WHERE RECORD_STATUS = 'N' 
                AND MAT_SEQ 
                NOT IN( SELECT MAT_SEQ 
                        FROM MGT1.TB_INV_MAS_MAT
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'ประเภทและชื่อแบบพิมพ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM INV.TB_INV_MAS_PLATE 
                WHERE RECORD_STATUS = 'N' 
                AND PLATE_SEQ 
                NOT IN( SELECT PLATE_SEQ 
                        FROM MGT1.TB_INV_MAS_PLATE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 4 AS NUM, 'ประเภทหลักเขต' AS TYPE, COUNT(*) AS RECEIVE
                FROM INV.TB_INV_MAS_PIECE 
                WHERE RECORD_STATUS = 'N' 
                AND PIECE_SEQ 
                NOT IN( SELECT PIECE_SEQ 
                        FROM MGT1.TB_INV_MAS_PIECE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 5 AS NUM, 'ประเภทและชื่ออาคารและสิ่งก่อสร้าง' AS TYPE, COUNT(*) AS RECEIVE
                FROM INV.TB_INV_MAS_BLD 
                WHERE RECORD_STATUS = 'N' 
                AND BLD_SEQ 
                NOT IN( SELECT BLD_SEQ 
                        FROM MGT1.TB_INV_MAS_BLD
                        WHERE RECORD_STATUS = 'N')
                )
            SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
            NVL(P1.RECEIVE,0) AS P1, 
            NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
            FROM P2
            FULL OUTER JOIN  P1
                ON P1.TYPE = P2.TYPE
            FULL OUTER JOIN  AP2
                 ON P1.TYPE = AP2.TYPE
            FULL OUTER JOIN  DP2
                 ON P1.TYPE = DP2.TYPE
            ORDER BY NVL(P1.NUM,P2.NUM)";
            
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'mas': //9
            $select = "WITH P1 AS(
                SELECT 1 AS NUM, 'ตารางข้อมูลการใช้ประโยชน์ในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_MAS_LANDUSED
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ตารางข้อมูลหมวดสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_FINANCE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ตารางข้อมูลสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_FINANCE
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 4 AS NUM, 'ตารางข้อมูลประเภทศาล' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_MAS_COURT_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 5 AS NUM, 'ตารางข้อมูลศาล' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_COURT
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 6 AS NUM, 'ตารางข้อมูลสัญชาติ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_NATIONALITY
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 7 AS NUM, 'ตารางข้อมูลเชื้อชาติ' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_MAS_RACE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 8 AS NUM, 'ตารางข้อมูลศาสนา' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_RELIGION
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 9 AS NUM, 'ตารางข้อมูลคำนำหน้า' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_TITLE
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 10 AS NUM, 'ตารางข้อมูลหน่วยวัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_MAS_UNIT
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 11 AS NUM, 'ตารางข้อมูลกลุ่มประเทศ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_PROVINCE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 12 AS NUM, 'ตารางข้อมูลประเทศ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_COUNTRY
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 13 AS NUM, 'ตารางข้อมูลภูมิภาค' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_REGION
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 14 AS NUM, 'ตารางข้อมูลจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_MAS_PROVINCE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 15 AS NUM, 'ตารางข้อมูลอำเภอ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_AMPHUR
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 16 AS NUM, 'ตารางข้อมูลตำบล' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_TAMBOL
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 17 AS NUM, 'ตารางข้อมูลประเภทหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_OFFICE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 18 AS NUM, 'ตารางข้อมูลระดับหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_OFFICE_LEVEL
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 19 AS NUM, 'ตารางข้อมูลแสดงประเภทหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_LANDOFFICE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 20 AS NUM, 'ตารางข้อมูลหน่วยงาน(ภายนอก)' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_LANDOFFICE
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (3)
                UNION 
                SELECT 21 AS NUM, 'ตารางข้อมูลหน่วยงาน(ภายใน)' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_LANDOFFICE
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2)
                UNION 
                SELECT 22 AS NUM, 'ตารางข้อมูลฝ่าย' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_LANDOFFICE 
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2) AND LANDOFFICE_TYPE_SEQ IN (11,12,13,14,15,16,17,18,19)
                UNION 
                SELECT 23 AS NUM, 'ตารางข้อมูลพื้นที่รับผิดชอบของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_LANDOFFICE_AREA
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 24 AS NUM, 'ตารางข้อมูลประเภท อปท.' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_OPT_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 25 AS NUM, 'ตารางข้อมูล อปท.' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_OPT
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 26 AS NUM, 'ตารางข้อมูลประเภทเอกสารสิทธิ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_REG_MAS_PRINTPLATE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 27 AS NUM, 'ตารางข้อมูลวันหยุด' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_HOLIDAY
                WHERE RECORD_STATUS = 'N'
                ),
            P2 AS(
                SELECT 1 AS NUM, 'ตารางข้อมูลการใช้ประโยชน์ในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MAS.TB_MAS_LANDUSED
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ตารางข้อมูลหมวดสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_FINANCE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ตารางข้อมูลสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_FINANCE
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 4 AS NUM, 'ตารางข้อมูลประเภทศาล' AS TYPE, COUNT(*) AS RECEIVE
                FROM MAS.TB_MAS_COURT_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 5 AS NUM, 'ตารางข้อมูลศาล' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_COURT
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 6 AS NUM, 'ตารางข้อมูลสัญชาติ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_NATIONALITY
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 7 AS NUM, 'ตารางข้อมูลเชื้อชาติ' AS TYPE, COUNT(*) AS RECEIVE
                FROM MAS.TB_MAS_RACE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 8 AS NUM, 'ตารางข้อมูลศาสนา' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_RELIGION
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 9 AS NUM, 'ตารางข้อมูลคำนำหน้า' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_TITLE
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 10 AS NUM, 'ตารางข้อมูลหน่วยวัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MAS.TB_MAS_UNIT
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 11 AS NUM, 'ตารางข้อมูลกลุ่มประเทศ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_PROVINCE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 12 AS NUM, 'ตารางข้อมูลประเทศ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_COUNTRY
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 13 AS NUM, 'ตารางข้อมูลภูมิภาค' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_REGION
                WHERE RECORD_STATUS = 'N'
                UNION
                SELECT 14 AS NUM, 'ตารางข้อมูลจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MAS.TB_MAS_PROVINCE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 15 AS NUM, 'ตารางข้อมูลอำเภอ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_AMPHUR
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 16 AS NUM, 'ตารางข้อมูลตำบล' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_TAMBOL
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 17 AS NUM, 'ตารางข้อมูลประเภทหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_OFFICE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 18 AS NUM, 'ตารางข้อมูลระดับหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_OFFICE_LEVEL
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 19 AS NUM, 'ตารางข้อมูลแสดงประเภทหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_LANDOFFICE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 20 AS NUM, 'ตารางข้อมูลหน่วยงาน(ภายนอก)' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_LANDOFFICE
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (3)
                UNION 
                SELECT 21 AS NUM, 'ตารางข้อมูลหน่วยงาน(ภายใน)' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_LANDOFFICE
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2)
                UNION
                SELECT 22 AS NUM, 'ตารางข้อมูลฝ่าย' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_LANDOFFICE 
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2) AND LANDOFFICE_TYPE_SEQ IN (11,12,13,14,15,16,17,18,19)
                UNION 
                SELECT 23 AS NUM, 'ตารางข้อมูลพื้นที่รับผิดชอบของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_LANDOFFICE_AREA
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 24 AS NUM, 'ตารางข้อมูลประเภท อปท.' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_OPT_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 25 AS NUM, 'ตารางข้อมูล อปท.' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_OPT
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 26 AS NUM, 'ตารางข้อมูลประเภทเอกสารสิทธิ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_PRINTPLATE_TYPE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 27 AS NUM, 'ตารางข้อมูลวันหยุด' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_HOLIDAY
                WHERE RECORD_STATUS = 'N'
                ),
            DP2 AS(
                SELECT 1 AS NUM, 'ตารางข้อมูลการใช้ประโยชน์ในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_MAS_LANDUSED 
                WHERE RECORD_STATUS = 'N' 
                AND LANDUSED_SEQ 
                NOT IN( SELECT LANDUSED_SEQ 
                        FROM MAS.TB_MAS_LANDUSED
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 2 AS NUM, 'ตารางข้อมูลหมวดสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_FINANCE_TYPE
                WHERE RECORD_STATUS = 'N'
                AND FINANCE_TYPE_SEQ 
                NOT IN( SELECT FINANCE_TYPE_SEQ 
                        FROM MAS.TB_MAS_FINANCE_TYPE
                        WHERE RECORD_STATUS = 'N')
                
                UNION 
                SELECT 3 AS NUM, 'ตารางข้อมูลสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_FINANCE
                WHERE RECORD_STATUS = 'N'
                AND FINANCE_SEQ 
                NOT IN( SELECT FINANCE_SEQ 
                        FROM MAS.TB_MAS_FINANCE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 4 AS NUM, 'ตารางข้อมูลประเภทศาล' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_MAS_COURT_TYPE
                WHERE RECORD_STATUS = 'N'
                AND COURT_TYPE_SEQ 
                NOT IN( SELECT COURT_TYPE_SEQ 
                        FROM MAS.TB_MAS_COURT_TYPE
                        WHERE RECORD_STATUS = 'N')
                        
                UNION 
                SELECT 5 AS NUM, 'ตารางข้อมูลศาล' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_COURT
                WHERE RECORD_STATUS = 'N'
                AND COURT_SEQ 
                NOT IN( SELECT COURT_SEQ 
                        FROM MAS.TB_MAS_COURT
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 6 AS NUM, 'ตารางข้อมูลสัญชาติ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_NATIONALITY
                WHERE RECORD_STATUS = 'N'
                AND NATIONALITY_SEQ 
                NOT IN( SELECT NATIONALITY_SEQ 
                        FROM MAS.TB_MAS_NATIONALITY
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 7 AS NUM, 'ตารางข้อมูลเชื้อชาติ' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_MAS_RACE
                WHERE RECORD_STATUS = 'N'
                AND RACE_SEQ 
                NOT IN( SELECT RACE_SEQ 
                        FROM MAS.TB_MAS_RACE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 8 AS NUM, 'ตารางข้อมูลศาสนา' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_RELIGION
                WHERE RECORD_STATUS = 'N'
                AND RELIGION_SEQ 
                NOT IN( SELECT RELIGION_SEQ 
                        FROM MAS.TB_MAS_RELIGION
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 9 AS NUM, 'ตารางข้อมูลคำนำหน้า' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_TITLE
                WHERE RECORD_STATUS = 'N'
                AND TITLE_SEQ 
                NOT IN( SELECT TITLE_SEQ 
                        FROM MAS.TB_MAS_TITLE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 10 AS NUM, 'ตารางข้อมูลหน่วยวัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_MAS_UNIT
                WHERE RECORD_STATUS = 'N'
                AND UNIT_SEQ 
                NOT IN( SELECT UNIT_SEQ 
                        FROM MAS.TB_MAS_UNIT
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 11 AS NUM, 'ตารางข้อมูลกลุ่มประเทศ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_COUNTRY_GROUP
                WHERE RECORD_STATUS = 'N'
                AND COUNTRY_GROUP_SEQ 
                NOT IN( SELECT COUNTRY_GROUP_SEQ 
                        FROM MAS.TB_MAS_COUNTRY_GROUP
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 12 AS NUM, 'ตารางข้อมูลประเทศ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_COUNTRY
                WHERE RECORD_STATUS = 'N'
                AND COUNTRY_SEQ 
                NOT IN( SELECT COUNTRY_SEQ 
                        FROM MAS.TB_MAS_COUNTRY
                        WHERE RECORD_STATUS = 'N')                     
                UNION 
                SELECT 13 AS NUM, 'ตารางข้อมูลภูมิภาค' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_REGION
                WHERE RECORD_STATUS = 'N'
                AND REGION_SEQ 
                NOT IN( SELECT REGION_SEQ 
                        FROM MAS.TB_MAS_REGION
                        WHERE RECORD_STATUS = 'N')  
                UNION
                SELECT 14 AS NUM, 'ตารางข้อมูลจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_MAS_PROVINCE
                WHERE RECORD_STATUS = 'N'
                AND PROVINCE_SEQ 
                NOT IN( SELECT PROVINCE_SEQ 
                        FROM MAS.TB_MAS_PROVINCE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 15 AS NUM, 'ตารางข้อมูลอำเภอ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_AMPHUR
                WHERE RECORD_STATUS = 'N'
                AND AMPHUR_SEQ 
                NOT IN( SELECT AMPHUR_SEQ 
                        FROM MAS.TB_MAS_AMPHUR
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 16 AS NUM, 'ตารางข้อมูลตำบล' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_TAMBOL
                WHERE RECORD_STATUS = 'N'
                AND TAMBOL_SEQ 
                NOT IN( SELECT TAMBOL_SEQ 
                        FROM MAS.TB_MAS_TAMBOL
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 17 AS NUM, 'ตารางข้อมูลประเภทหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_OFFICE_TYPE
                WHERE RECORD_STATUS = 'N'
                AND OFFICE_TYPE_SEQ 
                NOT IN( SELECT OFFICE_TYPE_SEQ 
                        FROM MAS.TB_MAS_OFFICE_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 18 AS NUM, 'ตารางข้อมูลระดับหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_OFFICE_LEVEL
                WHERE RECORD_STATUS = 'N'
                AND OFFICE_LEVEL_SEQ 
                NOT IN( SELECT OFFICE_LEVEL_SEQ 
                        FROM MAS.TB_MAS_OFFICE_LEVEL
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 19 AS NUM, 'ตารางข้อมูลแสดงประเภทหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_LANDOFFICE_TYPE
                WHERE RECORD_STATUS = 'N'
                AND LANDOFFICE_TYPE_SEQ 
                NOT IN( SELECT LANDOFFICE_TYPE_SEQ 
                        FROM MAS.TB_MAS_LANDOFFICE_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 20 AS NUM, 'ตารางข้อมูลหน่วยงาน(ภายนอก)' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_LANDOFFICE
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (3)
                AND LANDOFFICE_SEQ 
                NOT IN( SELECT LANDOFFICE_SEQ 
                        FROM MAS.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 21 AS NUM, 'ตารางข้อมูลหน่วยงาน(ภายใน)' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_LANDOFFICE
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2)
                AND LANDOFFICE_SEQ 
                NOT IN( SELECT LANDOFFICE_SEQ 
                        FROM MAS.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 22 AS NUM, 'ตารางข้อมูลฝ่าย' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_LANDOFFICE 
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2) AND LANDOFFICE_TYPE_SEQ IN (11,12,13,14,15,16,17,18,19)
                AND LANDOFFICE_SEQ 
                NOT IN( SELECT LANDOFFICE_SEQ 
                        FROM MAS.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 23 AS NUM, 'ตารางข้อมูลพื้นที่รับผิดชอบของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_LANDOFFICE_AREA
                WHERE RECORD_STATUS = 'N'
                AND LANDOFFICE_AREA_SEQ 
                NOT IN( SELECT LANDOFFICE_AREA_SEQ 
                        FROM MAS.TB_MAS_LANDOFFICE_AREA
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 24 AS NUM, 'ตารางข้อมูลประเภท อปท.' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_OPT_TYPE
                WHERE RECORD_STATUS = 'N'
                AND OPT_TYPE_SEQ 
                NOT IN( SELECT OPT_TYPE_SEQ 
                        FROM MAS.TB_MAS_OPT_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 25 AS NUM, 'ตารางข้อมูล อปท.' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_OPT
                WHERE RECORD_STATUS = 'N'
                AND OPT_SEQ 
                NOT IN( SELECT OPT_SEQ 
                        FROM MAS.TB_MAS_OPT
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 26 AS NUM, 'ตารางข้อมูลประเภทเอกสารสิทธิ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_REG_MAS_PRINTPLATE_TYPE
                WHERE RECORD_STATUS = 'N'
                AND PRINTPLATE_TYPE_SEQ 
                NOT IN( SELECT PRINTPLATE_TYPE_SEQ 
                        FROM MAS.TB_MAS_PRINTPLATE_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 27 AS NUM, 'ตารางข้อมูลวันหยุด' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_MAS_HOLIDAY
                WHERE RECORD_STATUS = 'N'
                AND HOLIDAY_SEQ 
                NOT IN( SELECT HOLIDAY_SEQ 
                        FROM MAS.TB_MAS_HOLIDAY
                        WHERE RECORD_STATUS = 'N')
                ),
            AP2 AS(
                SELECT 1 AS NUM, 'ตารางข้อมูลการใช้ประโยชน์ในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MAS.TB_MAS_LANDUSED 
                WHERE RECORD_STATUS = 'N' 
                AND LANDUSED_SEQ 
                NOT IN( SELECT LANDUSED_SEQ 
                        FROM MGT1.TB_MAS_LANDUSED
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 2 AS NUM, 'ตารางข้อมูลหมวดสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_FINANCE_TYPE
                WHERE RECORD_STATUS = 'N'
                AND FINANCE_TYPE_SEQ 
                NOT IN( SELECT FINANCE_TYPE_SEQ 
                        FROM MGT1.TB_MAS_FINANCE_TYPE
                        WHERE RECORD_STATUS = 'N')
                
                UNION 
                SELECT 3 AS NUM, 'ตารางข้อมูลสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_FINANCE
                WHERE RECORD_STATUS = 'N'
                AND FINANCE_SEQ 
                NOT IN( SELECT FINANCE_SEQ 
                        FROM MGT1.TB_MAS_FINANCE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 4 AS NUM, 'ตารางข้อมูลประเภทศาล' AS TYPE, COUNT(*) AS RECEIVE
                FROM MAS.TB_MAS_COURT_TYPE
                WHERE RECORD_STATUS = 'N'
                AND COURT_TYPE_SEQ 
                NOT IN( SELECT COURT_TYPE_SEQ 
                        FROM MGT1.TB_MAS_COURT_TYPE
                        WHERE RECORD_STATUS = 'N')
                        
                UNION 
                SELECT 5 AS NUM, 'ตารางข้อมูลศาล' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_COURT
                WHERE RECORD_STATUS = 'N'
                AND COURT_SEQ 
                NOT IN( SELECT COURT_SEQ 
                        FROM MGT1.TB_MAS_COURT
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 6 AS NUM, 'ตารางข้อมูลสัญชาติ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_NATIONALITY
                WHERE RECORD_STATUS = 'N'
                AND NATIONALITY_SEQ 
                NOT IN( SELECT NATIONALITY_SEQ 
                        FROM MGT1.TB_MAS_NATIONALITY
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 7 AS NUM, 'ตารางข้อมูลเชื้อชาติ' AS TYPE, COUNT(*) AS RECEIVE
                FROM MAS.TB_MAS_RACE
                WHERE RECORD_STATUS = 'N'
                AND RACE_SEQ 
                NOT IN( SELECT RACE_SEQ 
                        FROM MGT1.TB_MAS_RACE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 8 AS NUM, 'ตารางข้อมูลศาสนา' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_RELIGION
                WHERE RECORD_STATUS = 'N'
                AND RELIGION_SEQ 
                NOT IN( SELECT RELIGION_SEQ 
                        FROM MGT1.TB_MAS_RELIGION
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 9 AS NUM, 'ตารางข้อมูลคำนำหน้า' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_TITLE
                WHERE RECORD_STATUS = 'N'
                AND TITLE_SEQ 
                NOT IN( SELECT TITLE_SEQ 
                        FROM MGT1.TB_MAS_TITLE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 10 AS NUM, 'ตารางข้อมูลหน่วยวัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MAS.TB_MAS_UNIT
                WHERE RECORD_STATUS = 'N'
                AND UNIT_SEQ 
                NOT IN( SELECT UNIT_SEQ 
                        FROM MGT1.TB_MAS_UNIT
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 11 AS NUM, 'ตารางข้อมูลกลุ่มประเทศ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_COUNTRY_GROUP
                WHERE RECORD_STATUS = 'N'
                AND COUNTRY_GROUP_SEQ 
                NOT IN( SELECT COUNTRY_GROUP_SEQ 
                        FROM MGT1.TB_MAS_COUNTRY_GROUP
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 12 AS NUM, 'ตารางข้อมูลประเทศ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_COUNTRY
                WHERE RECORD_STATUS = 'N'
                AND COUNTRY_SEQ 
                NOT IN( SELECT COUNTRY_SEQ 
                        FROM MGT1.TB_MAS_COUNTRY
                        WHERE RECORD_STATUS = 'N')                     
                UNION 
                SELECT 13 AS NUM, 'ตารางข้อมูลภูมิภาค' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_REGION
                WHERE RECORD_STATUS = 'N'
                AND REGION_SEQ 
                NOT IN( SELECT REGION_SEQ 
                        FROM MGT1.TB_MAS_REGION
                        WHERE RECORD_STATUS = 'N')  
                UNION
                SELECT 14 AS NUM, 'ตารางข้อมูลจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
                FROM MAS.TB_MAS_PROVINCE
                WHERE RECORD_STATUS = 'N'
                AND PROVINCE_SEQ 
                NOT IN( SELECT PROVINCE_SEQ 
                        FROM MGT1.TB_MAS_PROVINCE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 15 AS NUM, 'ตารางข้อมูลอำเภอ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_AMPHUR
                WHERE RECORD_STATUS = 'N'
                AND AMPHUR_SEQ 
                NOT IN( SELECT AMPHUR_SEQ 
                        FROM MGT1.TB_MAS_AMPHUR
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 16 AS NUM, 'ตารางข้อมูลตำบล' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_TAMBOL
                WHERE RECORD_STATUS = 'N'
                AND TAMBOL_SEQ 
                NOT IN( SELECT TAMBOL_SEQ 
                        FROM MGT1.TB_MAS_TAMBOL
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 17 AS NUM, 'ตารางข้อมูลประเภทหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_OFFICE_TYPE
                WHERE RECORD_STATUS = 'N'
                AND OFFICE_TYPE_SEQ 
                NOT IN( SELECT OFFICE_TYPE_SEQ 
                        FROM MGT1.TB_MAS_OFFICE_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 18 AS NUM, 'ตารางข้อมูลระดับหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_OFFICE_LEVEL
                WHERE RECORD_STATUS = 'N'
                AND OFFICE_LEVEL_SEQ 
                NOT IN( SELECT OFFICE_LEVEL_SEQ 
                        FROM MGT1.TB_MAS_OFFICE_LEVEL
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 19 AS NUM, 'ตารางข้อมูลแสดงประเภทหน่วยงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_LANDOFFICE_TYPE
                WHERE RECORD_STATUS = 'N'
                AND LANDOFFICE_TYPE_SEQ 
                NOT IN( SELECT LANDOFFICE_TYPE_SEQ 
                        FROM MGT1.TB_MAS_LANDOFFICE_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 20 AS NUM, 'ตารางข้อมูลหน่วยงาน(ภายนอก)' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_LANDOFFICE
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (3)
                AND LANDOFFICE_SEQ 
                NOT IN( SELECT LANDOFFICE_SEQ 
                        FROM MGT1.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 21 AS NUM, 'ตารางข้อมูลหน่วยงาน(ภายใน)' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_LANDOFFICE
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2)
                AND LANDOFFICE_SEQ 
                NOT IN( SELECT LANDOFFICE_SEQ 
                        FROM MGT1.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 22 AS NUM, 'ตารางข้อมูลฝ่าย' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_LANDOFFICE 
                WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2) AND LANDOFFICE_TYPE_SEQ IN (11,12,13,14,15,16,17,18,19)
                AND LANDOFFICE_SEQ 
                NOT IN( SELECT LANDOFFICE_SEQ 
                        FROM MGT1.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 23 AS NUM, 'ตารางข้อมูลพื้นที่รับผิดชอบของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_LANDOFFICE_AREA
                WHERE RECORD_STATUS = 'N'
                AND LANDOFFICE_AREA_SEQ 
                NOT IN( SELECT LANDOFFICE_AREA_SEQ 
                        FROM MGT1.TB_MAS_LANDOFFICE_AREA
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 24 AS NUM, 'ตารางข้อมูลประเภท อปท.' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_OPT_TYPE
                WHERE RECORD_STATUS = 'N'
                AND OPT_TYPE_SEQ 
                NOT IN( SELECT OPT_TYPE_SEQ 
                        FROM MGT1.TB_MAS_OPT_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 25 AS NUM, 'ตารางข้อมูล อปท.' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_OPT
                WHERE RECORD_STATUS = 'N'
                AND OPT_SEQ 
                NOT IN( SELECT OPT_SEQ 
                        FROM MGT1.TB_MAS_OPT
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 26 AS NUM, 'ตารางข้อมูลประเภทเอกสารสิทธิ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_PRINTPLATE_TYPE
                WHERE RECORD_STATUS = 'N'
                AND PRINTPLATE_TYPE_SEQ 
                NOT IN( SELECT PRINTPLATE_TYPE_SEQ 
                        FROM MGT1.TB_REG_MAS_PRINTPLATE_TYPE
                        WHERE RECORD_STATUS = 'N')
                UNION 
                SELECT 27 AS NUM, 'ตารางข้อมูลวันหยุด' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MAS.TB_MAS_HOLIDAY
                WHERE RECORD_STATUS = 'N'
                AND HOLIDAY_SEQ 
                NOT IN( SELECT HOLIDAY_SEQ 
                        FROM MGT1.TB_MAS_HOLIDAY
                        WHERE RECORD_STATUS = 'N')
                    )
            SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
            NVL(P1.RECEIVE,0) AS P1, 
            NVL(P2.RECEIVE,0) AS P2,
            AP2.RECEIVE AS AP2RECEIVE,
            DP2.RECEIVE AS DP2RECEIVE
            FROM P2
            FULL OUTER JOIN  P1
                ON P1.TYPE = P2.TYPE
            FULL OUTER JOIN  AP2
                ON P1.TYPE = AP2.TYPE
            FULL OUTER JOIN  DP2
                ON P1.TYPE = DP2.TYPE
            ORDER BY NVL(P1.NUM,P2.NUM)";

            $stid = oci_parse($conn, $select); 
            oci_execute($stid);

            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        // case 'mas': //9
        //     $select = "WITH P1 AS(
        //                 SELECT 1 AS NUM, 'ตารางข้อมูลประเทศ' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_MAS_COUNTRY
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION
        //                 SELECT 2 AS NUM, 'ตารางข้อมูลจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_PROVINCE
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 3 AS NUM, 'ตารางข้อมูลอำเภอ' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_MAS_AMPHUR
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 4 AS NUM, 'ตารางข้อมูลตำบล' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_MAS_TAMBOL
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 5 AS NUM, 'ตารางข้อมูลชื่อสำนักงานที่ดิน' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_MAS_LANDOFFICE
        //                 WHERE RECORD_STATUS = 'N' 
        //                 UNION 
        //                 SELECT 6 AS NUM, 'ตารางข้อมูลสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_MAS_FINANCE
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 7 AS NUM, 'ตารางข้อมูลการใช้ประโยชน์ในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_LANDUSED
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 8 AS NUM, 'ชื่อเขตปกครองส่วนท้องถิ่น' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_MAS_OPT
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 9 AS NUM, 'ตารางข้อมูลประเภทเอกสารสิทธิ' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_REG_MAS_PRINTPLATE_TYPE
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 10 AS NUM, 'ตารางข้อมูลคำนำหน้า' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_MAS_TITLE
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION
        //                 SELECT 11 AS NUM, 'ตารางข้อมูลศาล' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_MAS_COURT
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 12 AS NUM, 'ตารางข้อมูลพื้นที่รับผิดชอบของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_MAS_LANDOFFICE_AREA
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 13 AS NUM, 'ข้อมูลการแบ่งองค์กรท้องถิ่นกับสำนักงานที่ดิน' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MGT1.TB_MAS_OPT_AREA
        //                 WHERE RECORD_STATUS = 'N'
        //                 ),
        //             P2 AS(
        //                 SELECT 1 AS NUM, 'ตารางข้อมูลประเทศ' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_COUNTRY
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION
        //                 SELECT 2 AS NUM, 'ตารางข้อมูลจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_PROVINCE
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 3 AS NUM, 'ตารางข้อมูลอำเภอ' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_AMPHUR
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 4 AS NUM, 'ตารางข้อมูลตำบล' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_TAMBOL
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 5 AS NUM, 'ตารางข้อมูลชื่อสำนักงานที่ดิน' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_LANDOFFICE
        //                 WHERE RECORD_STATUS = 'N' 
        //                 UNION 
        //                 SELECT 6 AS NUM, 'ตารางข้อมูลสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_FINANCE
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 7 AS NUM, 'ตารางข้อมูลการใช้ประโยชน์ในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_LANDUSED
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 8 AS NUM, 'ชื่อเขตปกครองส่วนท้องถิ่น' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_OPT
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 9 AS NUM, 'ตารางข้อมูลประเภทเอกสารสิทธิ' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_PRINTPLATE_TYPE
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 10 AS NUM, 'ตารางข้อมูลคำนำหน้า' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_TITLE
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION
        //                 SELECT 11 AS NUM, 'ตารางข้อมูลศาล' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_COURT
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 12 AS NUM, 'ตารางข้อมูลพื้นที่รับผิดชอบของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_LANDOFFICE_AREA
        //                 WHERE RECORD_STATUS = 'N'
        //                 UNION 
        //                 SELECT 13 AS NUM, 'ข้อมูลการแบ่งองค์กรท้องถิ่นกับสำนักงานที่ดิน' AS TYPE, COUNT(*) AS RECEIVE  
        //                 FROM MAS.TB_MAS_OPT_AREA
        //                 WHERE RECORD_STATUS = 'N'                
        //                 ),
        //             DP2 AS(
        //                 SELECT 1 AS NUM, 'ตารางข้อมูลประเทศ' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_COUNTRY 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND COUNTRY_SEQ 
        //                 NOT IN( SELECT COUNTRY_SEQ 
        //                         FROM MAS.TB_MAS_COUNTRY
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 2 AS NUM, 'ตารางข้อมูลจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_PROVINCE 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND PROVINCE_SEQ 
        //                 NOT IN( SELECT PROVINCE_SEQ 
        //                         FROM MAS.TB_MAS_PROVINCE
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 3 AS NUM, 'ตารางข้อมูลอำเภอ' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_AMPHUR 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND AMPHUR_SEQ 
        //                 NOT IN( SELECT AMPHUR_SEQ 
        //                         FROM MAS.TB_MAS_AMPHUR
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 4 AS NUM, 'ตารางข้อมูลตำบล' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_TAMBOL 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND TAMBOL_SEQ 
        //                 NOT IN( SELECT TAMBOL_SEQ 
        //                         FROM MAS.TB_MAS_TAMBOL
        //                         WHERE RECORD_STATUS = 'N')
        //                                 UNION
        //                 SELECT 5 AS NUM, 'ตารางข้อมูลชื่อสำนักงานที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_LANDOFFICE 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND LANDOFFICE_SEQ 
        //                 NOT IN( SELECT LANDOFFICE_SEQ 
        //                         FROM MAS.TB_MAS_LANDOFFICE
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 6 AS NUM, 'ตารางข้อมูลสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_FINANCE 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND FINANCE_SEQ 
        //                 NOT IN( SELECT FINANCE_SEQ 
        //                         FROM MAS.TB_MAS_FINANCE
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 7 AS NUM, 'ตารางข้อมูลการใช้ประโยชน์ในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_LANDUSED 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND LANDUSED_SEQ 
        //                 NOT IN( SELECT LANDUSED_SEQ 
        //                         FROM MAS.TB_MAS_LANDUSED
        //                         WHERE RECORD_STATUS = 'N')
        //                                 UNION
        //                 SELECT 8 AS NUM, 'ชื่อเขตปกครองส่วนท้องถิ่น' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_OPT 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND OPT_SEQ 
        //                 NOT IN( SELECT OPT_SEQ 
        //                         FROM MAS.TB_MAS_OPT
        //                         WHERE RECORD_STATUS = 'N')
        //                                 UNION
        //                 SELECT 9 AS NUM, 'ตารางข้อมูลประเภทเอกสารสิทธิ' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_REG_MAS_PRINTPLATE_TYPE 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND PRINTPLATE_TYPE_SEQ 
        //                 NOT IN( SELECT PRINTPLATE_TYPE_SEQ 
        //                         FROM MAS.TB_MAS_PRINTPLATE_TYPE
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 10 AS NUM, 'ตารางข้อมูลคำนำหน้า' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_TITLE 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND TITLE_SEQ 
        //                 NOT IN( SELECT TITLE_SEQ 
        //                         FROM MAS.TB_MAS_TITLE
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 11 AS NUM, 'ตารางข้อมูลศาล' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_COURT 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND COURT_SEQ 
        //                 NOT IN( SELECT COURT_SEQ 
        //                         FROM MAS.TB_MAS_COURT
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 12 AS NUM, 'ตารางข้อมูลพื้นที่รับผิดชอบของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_LANDOFFICE_AREA 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND LANDOFFICE_AREA_SEQ 
        //                 NOT IN( SELECT LANDOFFICE_AREA_SEQ 
        //                         FROM MAS.TB_MAS_LANDOFFICE_AREA
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 13 AS NUM, 'ข้อมูลการแบ่งองค์กรท้องถิ่นกับสำนักงานที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MGT1.TB_MAS_OPT_AREA 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND OPT_AREA_SEQ 
        //                 NOT IN( SELECT OPT_AREA_SEQ 
        //                         FROM MAS.TB_MAS_OPT_AREA
        //                         WHERE RECORD_STATUS = 'N')
        //                 ),
        //             AP2 AS(
        //                 SELECT 1 AS NUM, 'ตารางข้อมูลประเทศ' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_COUNTRY 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND COUNTRY_SEQ 
        //                 NOT IN( SELECT COUNTRY_SEQ 
        //                         FROM MGT1.TB_MAS_COUNTRY
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 2 AS NUM, 'ตารางข้อมูลจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_PROVINCE 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND PROVINCE_SEQ 
        //                 NOT IN( SELECT PROVINCE_SEQ 
        //                         FROM MGT1.TB_MAS_PROVINCE
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 3 AS NUM, 'ตารางข้อมูลอำเภอ' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_AMPHUR 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND AMPHUR_SEQ 
        //                 NOT IN( SELECT AMPHUR_SEQ 
        //                         FROM MGT1.TB_MAS_AMPHUR
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 4 AS NUM, 'ตารางข้อมูลตำบล' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_TAMBOL 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND TAMBOL_SEQ 
        //                 NOT IN( SELECT TAMBOL_SEQ 
        //                         FROM MGT1.TB_MAS_TAMBOL
        //                         WHERE RECORD_STATUS = 'N')
        //                                 UNION
        //                 SELECT 5 AS NUM, 'ตารางข้อมูลชื่อสำนักงานที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_LANDOFFICE 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND LANDOFFICE_SEQ 
        //                 NOT IN( SELECT LANDOFFICE_SEQ 
        //                         FROM MGT1.TB_MAS_LANDOFFICE
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 6 AS NUM, 'ตารางข้อมูลสถาบันการเงิน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_FINANCE 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND FINANCE_SEQ 
        //                 NOT IN( SELECT FINANCE_SEQ 
        //                         FROM MGT1.TB_MAS_FINANCE
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 7 AS NUM, 'ตารางข้อมูลการใช้ประโยชน์ในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_LANDUSED 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND LANDUSED_SEQ 
        //                 NOT IN( SELECT LANDUSED_SEQ 
        //                         FROM MGT1.TB_MAS_LANDUSED
        //                         WHERE RECORD_STATUS = 'N')
        //                                 UNION
        //                 SELECT 8 AS NUM, 'ชื่อเขตปกครองส่วนท้องถิ่น' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_OPT 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND OPT_SEQ 
        //                 NOT IN( SELECT OPT_SEQ 
        //                         FROM MGT1.TB_MAS_OPT
        //                         WHERE RECORD_STATUS = 'N')
        //                                 UNION
        //                 SELECT 9 AS NUM, 'ตารางข้อมูลประเภทเอกสารสิทธิ' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_PRINTPLATE_TYPE 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND PRINTPLATE_TYPE_SEQ 
        //                 NOT IN( SELECT PRINTPLATE_TYPE_SEQ 
        //                         FROM MGT1.TB_REG_MAS_PRINTPLATE_TYPE
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 10 AS NUM, 'ตารางข้อมูลคำนำหน้า' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_TITLE 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND TITLE_SEQ 
        //                 NOT IN( SELECT TITLE_SEQ 
        //                         FROM MGT1.TB_MAS_TITLE
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 11 AS NUM, 'ตารางข้อมูลศาล' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_COURT 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND COURT_SEQ 
        //                 NOT IN( SELECT COURT_SEQ 
        //                         FROM MGT1.TB_MAS_COURT
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 12 AS NUM, 'ตารางข้อมูลพื้นที่รับผิดชอบของสำนักงาน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_LANDOFFICE_AREA 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND LANDOFFICE_AREA_SEQ 
        //                 NOT IN( SELECT LANDOFFICE_AREA_SEQ 
        //                         FROM MGT1.TB_MAS_LANDOFFICE_AREA
        //                         WHERE RECORD_STATUS = 'N')
        //                 UNION
        //                 SELECT 13 AS NUM, 'ข้อมูลการแบ่งองค์กรท้องถิ่นกับสำนักงานที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
        //                 FROM MAS.TB_MAS_OPT_AREA 
        //                 WHERE RECORD_STATUS = 'N' 
        //                 AND OPT_AREA_SEQ 
        //                 NOT IN( SELECT OPT_AREA_SEQ 
        //                         FROM MGT1.TB_MAS_OPT_AREA
        //                         WHERE RECORD_STATUS = 'N')
        //                 )
        //             SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
        //             NVL(P1.RECEIVE,0) AS P1, 
        //             NVL(P2.RECEIVE,0) AS P2,
        //             CASE WHEN AP2.RECEIVE = 0 AND DP2.RECEIVE = 0 THEN null ELSE 
        //             'เพิ่มจากพัฒ1: ' || AP2.RECEIVE ||'   พัฒ2ยกเลิก:' || DP2.RECEIVE END AS Remark
        //             FROM P2
        //             FULL OUTER JOIN  P1
        //                 ON P1.TYPE = P2.TYPE
        //             FULL OUTER JOIN  AP2
        //                 ON P1.TYPE = AP2.TYPE
        //             FULL OUTER JOIN  DP2
        //                 ON P1.TYPE = DP2.TYPE
        //             ORDER BY NVL(P1.NUM,P2.NUM)";

        //     $stid = oci_parse($conn, $select); 
        //     oci_execute($stid);

        //     while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        //         $Result[] = $row;
        //     }
        // break;

        case 'reg': //10
            $select = "WITH P1 AS(
                        SELECT 1 AS NUM, 'การได้มา' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_REG_MAS_OBTAIN
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ประเภทการจดทะเบียน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_REG_MAS_REGISTER
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ชื่อประเภทบัญชีคุม' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_REG_MAS_BOOK_TYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION
                        SELECT 4 AS NUM, 'รายการเอกสารจดทะเบียน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_REG_MAS_DOCUMENT
                        WHERE RECORD_STATUS = 'N'
                        ),
                    P2 AS(
                        SELECT 1 AS NUM, 'การได้มา' AS TYPE, COUNT(*) AS RECEIVE
                        FROM REG.TB_REG_MAS_OBTAIN
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ประเภทการจดทะเบียน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM REG.TB_REG_MAS_REGISTER
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ชื่อประเภทบัญชีคุม' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM REG.TB_REG_MAS_BOOK_TYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION
                        SELECT 4 AS NUM, 'รายการเอกสารจดทะเบียน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM REG.TB_REG_MAS_DOCUMENT
                        WHERE RECORD_STATUS = 'N'
                        ),
                    DP2 AS(
                        SELECT 1 AS NUM, 'การได้มา' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_REG_MAS_OBTAIN 
                        WHERE RECORD_STATUS = 'N' 
                        AND OBTAIN_SEQ 
                        NOT IN( SELECT OBTAIN_SEQ 
                                FROM REG.TB_REG_MAS_OBTAIN
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ประเภทการจดทะเบียน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_REG_MAS_REGISTER 
                        WHERE RECORD_STATUS = 'N' 
                        AND REGISTER_SEQ 
                        NOT IN( SELECT REGISTER_SEQ 
                                FROM REG.TB_REG_MAS_REGISTER
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ชื่อประเภทบัญชีคุม' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_REG_MAS_BOOK_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND BOOK_TYPE_SEQ 
                        NOT IN( SELECT BOOK_TYPE_SEQ 
                                FROM REG.TB_REG_MAS_BOOK_TYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'รายการเอกสารจดทะเบียน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_REG_MAS_DOCUMENT 
                        WHERE RECORD_STATUS = 'N' 
                        AND DOCUMENT_SEQ 
                        NOT IN( SELECT DOCUMENT_SEQ 
                                FROM REG.TB_REG_MAS_DOCUMENT
                                WHERE RECORD_STATUS = 'N')
                        ),
                    AP2 AS(
                        SELECT 1 AS NUM, 'การได้มา' AS TYPE, COUNT(*) AS RECEIVE
                        FROM REG.TB_REG_MAS_OBTAIN 
                        WHERE RECORD_STATUS = 'N' 
                        AND OBTAIN_SEQ 
                        NOT IN( SELECT OBTAIN_SEQ 
                                FROM MGT1.TB_REG_MAS_OBTAIN
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ประเภทการจดทะเบียน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM REG.TB_REG_MAS_REGISTER 
                        WHERE RECORD_STATUS = 'N' 
                        AND REGISTER_SEQ 
                        NOT IN( SELECT REGISTER_SEQ 
                                FROM MGT1.TB_REG_MAS_REGISTER
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ชื่อประเภทบัญชีคุม' AS TYPE, COUNT(*) AS RECEIVE
                        FROM REG.TB_REG_MAS_BOOK_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND BOOK_TYPE_SEQ 
                        NOT IN( SELECT BOOK_TYPE_SEQ 
                                FROM MGT1.TB_REG_MAS_BOOK_TYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'รายการเอกสารจดทะเบียน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM REG.TB_REG_MAS_DOCUMENT 
                        WHERE RECORD_STATUS = 'N' 
                        AND DOCUMENT_SEQ 
                        NOT IN( SELECT DOCUMENT_SEQ 
                                FROM MGT1.TB_REG_MAS_DOCUMENT
                                WHERE RECORD_STATUS = 'N')
                    )
                    SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
                    NVL(P1.RECEIVE,0) AS P1, 
                    NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
                --     CASE WHEN AP2.RECEIVE = 0 AND DP2.RECEIVE = 0 THEN null ELSE 
                --     'เพิ่มจากพัฒ1: ' || AP2.RECEIVE ||'   พัฒ2ยกเลิก:' || DP2.RECEIVE END AS Remark
                    FROM P2
                    FULL OUTER JOIN  P1
                        ON P1.TYPE = P2.TYPE
                    FULL OUTER JOIN  AP2
                        ON P1.TYPE = AP2.TYPE
                    FULL OUTER JOIN  DP2
                        ON P1.TYPE = DP2.TYPE
                    ORDER BY NVL(P1.NUM,P2.NUM)";

            $stid = oci_parse($conn, $select); 
            oci_execute($stid);

            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'evd': //11
            $select = "WITH P1 AS(
                        SELECT 1 AS NUM, 'ชื่อย่อจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_EVD_MAS_PROVINCE_ABBR
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'สถานะนำเข้าภาพลักษณ์' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_EVD_MAS_IMAGE_STS
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ภาพลักษณ์ต้นร่างอัติโนมัต' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_EVD_MAS_DOCTYPE_MAPPING
                        WHERE RECORD_STATUS = 'N'
                        ),
                    P2 AS(
                        SELECT 1 AS NUM, 'ชื่อย่อจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM REG.TB_EVD_MAS_PROVINCE_ABBR
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'สถานะนำเข้าภาพลักษณ์' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM REG.TB_EVD_MAS_IMAGE_STS
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ภาพลักษณ์ต้นร่างอัติโนมัต' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM REG.TB_EVD_MAS_DOCTYPE_MAPPING
                        WHERE RECORD_STATUS = 'N'
                        ),
                    DP2 AS(
                        SELECT 1 AS NUM, 'ชื่อย่อจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_EVD_MAS_PROVINCE_ABBR 
                        WHERE RECORD_STATUS = 'N' 
                        AND PROVINCE_ABBR_SEQ 
                        NOT IN( SELECT PROVINCE_ABBR_SEQ 
                                FROM REG.TB_EVD_MAS_PROVINCE_ABBR
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'สถานะนำเข้าภาพลักษณ์' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_EVD_MAS_IMAGE_STS 
                        WHERE RECORD_STATUS = 'N' 
                        AND IMAGE_STS_SEQ 
                        NOT IN( SELECT IMAGE_STS_SEQ 
                                FROM REG.TB_EVD_MAS_IMAGE_STS
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ภาพลักษณ์ต้นร่างอัติโนมัต' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_EVD_MAS_DOCTYPE_MAPPING 
                        WHERE RECORD_STATUS = 'N' 
                        AND DOCTYPE_MAPPING_SEQ 
                        NOT IN( SELECT DOCTYPE_MAPPING_SEQ 
                                FROM REG.TB_REG_MAS_BOOK_TYPE
                                WHERE RECORD_STATUS = 'N')
                        ),
                    AP2 AS(
                        SELECT 1 AS NUM, 'ชื่อย่อจังหวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM REG.TB_EVD_MAS_PROVINCE_ABBR 
                        WHERE RECORD_STATUS = 'N' 
                        AND PROVINCE_ABBR_SEQ 
                        NOT IN( SELECT PROVINCE_ABBR_SEQ 
                                FROM MGT1.TB_EVD_MAS_PROVINCE_ABBR
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'สถานะนำเข้าภาพลักษณ์' AS TYPE, COUNT(*) AS RECEIVE
                        FROM REG.TB_EVD_MAS_IMAGE_STS 
                        WHERE RECORD_STATUS = 'N' 
                        AND IMAGE_STS_SEQ 
                        NOT IN( SELECT IMAGE_STS_SEQ 
                                FROM MGT1.TB_EVD_MAS_IMAGE_STS
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ภาพลักษณ์ต้นร่างอัติโนมัต' AS TYPE, COUNT(*) AS RECEIVE
                        FROM REG.TB_EVD_MAS_DOCTYPE_MAPPING 
                        WHERE RECORD_STATUS = 'N' 
                        AND DOCTYPE_MAPPING_SEQ 
                        NOT IN( SELECT DOCTYPE_MAPPING_SEQ 
                                FROM MGT1.TB_REG_MAS_BOOK_TYPE
                                WHERE RECORD_STATUS = 'N')
                        )
                    SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
                    NVL(P1.RECEIVE,0) AS P1, 
                    NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
                --     CASE WHEN AP2.RECEIVE = 0 AND DP2.RECEIVE = 0 THEN null ELSE 
                --     'เพิ่มจากพัฒ1: ' || AP2.RECEIVE ||'   พัฒ2ยกเลิก:' || DP2.RECEIVE END AS Remark
                    FROM P2
                    FULL OUTER JOIN  P1
                        ON P1.TYPE = P2.TYPE
                    FULL OUTER JOIN  AP2
                        ON P1.TYPE = AP2.TYPE
                    FULL OUTER JOIN  DP2
                        ON P1.TYPE = DP2.TYPE
                    ORDER BY NVL(P1.NUM,P2.NUM)";

            $stid = oci_parse($conn, $select); 
            oci_execute($stid);

            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'sva': //12
            $select = "WITH P1 AS(
                        SELECT 1 AS NUM, 'ประเภทการรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_TYPEOFSURVEY
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ประเภทหลักฐานการรังวัด' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVA_MAS_SURVEYDOCTYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ประเภทเครื่องมือรังวัด' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVA_MAS_EQUIPMENTTYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 4 AS NUM, 'สำนักงานเอกชนรังวัด' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVA_MAS_PRIVATE_OFFICE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 5 AS NUM, 'ข้อมูลระวาง' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVA_MAS_SHEET
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 6 AS NUM, 'สถานะงานรังวัด' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVA_MAS_JOBSTATUS
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 7 AS NUM, 'ประเภทจดหมายแจ้ง' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVA_MAS_LETTERTYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 8 AS NUM, 'ประเภทกลุ่มงานรังวัด' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVA_MAS_JOBGROUP
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 9 AS NUM, 'ประเภทกลุ่มงานเอกสารสิทธิในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVA_MAS_GROUPTYPE
                        WHERE RECORD_STATUS = 'N'   
                        ),
                    P2 AS(
                        SELECT 1 AS NUM, 'ประเภทการรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVA_MAS_TYPEOFSURVEY
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'ประเภทหลักฐานการรังวัด' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVA_MAS_SURVEYDOCTYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ประเภทเครื่องมือรังวัด' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVA_MAS_EQUIPMENTTYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 4 AS NUM, 'สำนักงานเอกชนรังวัด' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVA_MAS_PRIVATE_OFFICE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 5 AS NUM, 'ข้อมูลระวาง' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVA_MAS_SHEET
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 6 AS NUM, 'สถานะงานรังวัด' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVA_MAS_JOBSTATUS
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 7 AS NUM, 'ประเภทจดหมายแจ้ง' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVA_MAS_LETTERTYPE
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 8 AS NUM, 'ประเภทกลุ่มงานรังวัด' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVA_MAS_JOBGROUP
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 9 AS NUM, 'ประเภทกลุ่มงานเอกสารสิทธิในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVA_MAS_GROUPTYPE
                        WHERE RECORD_STATUS = 'N'   
                        ),
                    DP2 AS(
                        SELECT 1 AS NUM, 'ประเภทการรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_TYPEOFSURVEY 
                        WHERE RECORD_STATUS = 'N' 
                        AND TYPEOFSURVEY_SEQ 
                        NOT IN( SELECT TYPEOFSURVEY_SEQ 
                                FROM SVO.TB_SVA_MAS_TYPEOFSURVEY
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ประเภทหลักฐานการรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_SURVEYDOCTYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND SURVEYDOCTYPE_SEQ
                        NOT IN( SELECT SURVEYDOCTYPE_SEQ 
                                FROM SVO.TB_SVA_MAS_SURVEYDOCTYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ประเภทเครื่องมือรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_EQUIPMENTTYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND EQUIPMENTTYPE_SEQ 
                        NOT IN( SELECT EQUIPMENTTYPE_SEQ 
                                FROM SVO.TB_SVA_MAS_EQUIPMENTTYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'สำนักงานเอกชนรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_PRIVATE_OFFICE 
                        WHERE RECORD_STATUS = 'N' 
                        AND PRIVATE_OFFICE_SEQ 
                        NOT IN( SELECT PRIVATE_OFFICE_SEQ 
                                FROM SVO.TB_SVA_MAS_PRIVATE_OFFICE
                                WHERE RECORD_STATUS = 'N')                                
                        UNION
                        SELECT 5 AS NUM, 'ข้อมูลระวาง' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_SHEET 
                        WHERE RECORD_STATUS = 'N' 
                        AND SHEET_SEQ
                        NOT IN( SELECT SHEET_SEQ 
                                FROM SVO.TB_SVA_MAS_SHEET
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 6 AS NUM, 'สถานะงานรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_JOBSTATUS 
                        WHERE RECORD_STATUS = 'N' 
                        AND JOBSTATUS_SEQ 
                        NOT IN( SELECT JOBSTATUS_SEQ 
                                FROM SVO.TB_SVA_MAS_JOBSTATUS
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 7 AS NUM, 'ประเภทจดหมายแจ้ง' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_LETTERTYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND LETTERTYPE_SEQ 
                        NOT IN( SELECT LETTERTYPE_SEQ 
                                FROM SVO.TB_SVA_MAS_LETTERTYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 8 AS NUM, 'ประเภทกลุ่มงานรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_JOBGROUP 
                        WHERE RECORD_STATUS = 'N' 
                        AND JOBGROUP_SEQ 
                        NOT IN( SELECT JOBGROUP_SEQ 
                                FROM SVO.TB_SVA_MAS_JOBGROUP
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 9 AS NUM, 'ประเภทกลุ่มงานเอกสารสิทธิในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_GROUPTYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND GROUPTYPE_SEQ 
                        NOT IN( SELECT GROUPTYPE_SEQ 
                                FROM SVO.TB_SVA_MAS_GROUPTYPE
                                WHERE RECORD_STATUS = 'N')
                        ),
                    AP2 AS(
                        SELECT 1 AS NUM, 'ประเภทการรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVA_MAS_TYPEOFSURVEY 
                        WHERE RECORD_STATUS = 'N' 
                        AND TYPEOFSURVEY_SEQ 
                        NOT IN( SELECT TYPEOFSURVEY_SEQ 
                                FROM MGT1.TB_SVA_MAS_TYPEOFSURVEY
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'ประเภทหลักฐานการรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVA_MAS_SURVEYDOCTYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND SURVEYDOCTYPE_SEQ
                        NOT IN( SELECT SURVEYDOCTYPE_SEQ 
                                FROM MGT1.TB_SVA_MAS_SURVEYDOCTYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ประเภทเครื่องมือรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVA_MAS_EQUIPMENTTYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND EQUIPMENTTYPE_SEQ 
                        NOT IN( SELECT EQUIPMENTTYPE_SEQ 
                                FROM MGT1.TB_SVA_MAS_EQUIPMENTTYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'สำนักงานเอกชนรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVA_MAS_PRIVATE_OFFICE 
                        WHERE RECORD_STATUS = 'N' 
                        AND PRIVATE_OFFICE_SEQ 
                        NOT IN( SELECT PRIVATE_OFFICE_SEQ 
                                FROM MGT1.TB_SVA_MAS_PRIVATE_OFFICE
                                WHERE RECORD_STATUS = 'N')                                
                        UNION
                        SELECT 5 AS NUM, 'ข้อมูลระวาง' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVA_MAS_SHEET 
                        WHERE RECORD_STATUS = 'N' 
                        AND SHEET_SEQ
                        NOT IN( SELECT SHEET_SEQ 
                                FROM MGT1.TB_SVA_MAS_SHEET
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 6 AS NUM, 'สถานะงานรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVA_MAS_JOBSTATUS 
                        WHERE RECORD_STATUS = 'N' 
                        AND JOBSTATUS_SEQ 
                        NOT IN( SELECT JOBSTATUS_SEQ 
                                FROM SVO.TB_SVA_MAS_JOBSTATUS
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 7 AS NUM, 'ประเภทจดหมายแจ้ง' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVA_MAS_LETTERTYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND LETTERTYPE_SEQ 
                        NOT IN( SELECT LETTERTYPE_SEQ 
                                FROM MGT1.TB_SVA_MAS_LETTERTYPE
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 8 AS NUM, 'ประเภทกลุ่มงานรังวัด' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVA_MAS_JOBGROUP 
                        WHERE RECORD_STATUS = 'N' 
                        AND JOBGROUP_SEQ 
                        NOT IN( SELECT JOBGROUP_SEQ 
                                FROM MGT1.TB_SVA_MAS_JOBGROUP
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 9 AS NUM, 'ประเภทกลุ่มงานเอกสารสิทธิในที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVA_MAS_GROUPTYPE 
                        WHERE RECORD_STATUS = 'N' 
                        AND GROUPTYPE_SEQ 
                        NOT IN( SELECT GROUPTYPE_SEQ 
                                FROM MGT1.TB_SVA_MAS_GROUPTYPE
                                WHERE RECORD_STATUS = 'N')
                        )
                    SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
                    NVL(P1.RECEIVE,0) AS P1, 
                    NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
                --     CASE WHEN AP2.RECEIVE = 0 AND DP2.RECEIVE = 0 THEN null ELSE 
                --     'เพิ่มจากพัฒ1: ' || AP2.RECEIVE ||'   พัฒ2ยกเลิก:' || DP2.RECEIVE END AS Remark
                    FROM P2
                    FULL OUTER JOIN  P1
                        ON P1.TYPE = P2.TYPE
                    FULL OUTER JOIN  AP2
                        ON P1.TYPE = AP2.TYPE
                    FULL OUTER JOIN  DP2
                        ON P1.TYPE = DP2.TYPE
                    ORDER BY NVL(P1.NUM,P2.NUM)";

            $stid = oci_parse($conn, $select); 
            oci_execute($stid);

            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'svc': //13
            $select = "WITH P1 AS(
                        SELECT 1 AS NUM, 'หมุดเส้นโครงาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVC_MAS_CTRLMARK
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'หมุดดาวเทียม' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVC_MAS_GPSMARK
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ระวาง 4000 โซน 47' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVC_MAS_MAPSHEET4000_47
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 4 AS NUM, 'ระวาง 4000 โซน 48' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVC_MAS_MAPSHEET4000_48
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 5 AS NUM, 'เส้นโครงงาน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM MGT1.TB_SVC_MAS_TRAVERSE
                        WHERE RECORD_STATUS = 'N'
                        ),
                    P2 AS(
                        SELECT 1 AS NUM, 'หมุดเส้นโครงาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVC_MAS_CTRLMARK
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 2 AS NUM, 'หมุดดาวเทียม' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVC_MAS_GPSMARK
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 3 AS NUM, 'ระวาง 4000 โซน 47' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVC_MAS_MAPSHEET4000_47
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 4 AS NUM, 'ระวาง 4000 โซน 48' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVC_MAS_MAPSHEET4000_48
                        WHERE RECORD_STATUS = 'N'
                        UNION 
                        SELECT 5 AS NUM, 'เส้นโครงงาน' AS TYPE, COUNT(*) AS RECEIVE  
                        FROM SVO.TB_SVC_MAS_TRAVERSE
                        WHERE RECORD_STATUS = 'N'
                        ),
                    DP2 AS(
                        SELECT 1 AS NUM, 'หมุดเส้นโครงาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVC_MAS_CTRLMARK 
                        WHERE RECORD_STATUS = 'N' 
                        AND TV_ZONE||TV_PROV||TV_NAME||TVC_ORDERNO||TVC_CTRLNAME 
                        NOT IN( SELECT TV_ZONE||TV_PROV||TV_NAME||TVC_ORDERNO||TVC_CTRLNAME  
                                FROM SVO.TB_SVC_MAS_CTRLMARK
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'หมุดดาวเทียม' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVC_MAS_GPSMARK 
                        WHERE RECORD_STATUS = 'N' 
                        AND GPSMARK_SEQ 
                        NOT IN( SELECT GPSMARK_SEQ 
                                FROM SVO.TB_SVC_MAS_GPSMARK
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ระวาง 4000 โซน 47' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVC_MAS_MAPSHEET4000_47 
                        WHERE RECORD_STATUS = 'N' 
                        AND INDEX_SEQ 
                        NOT IN( SELECT INDEX_SEQ 
                                FROM SVO.TB_SVC_MAS_MAPSHEET4000_47
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'ระวาง 4000 โซน 48' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVC_MAS_MAPSHEET4000_48 
                        WHERE RECORD_STATUS = 'N' 
                        AND INDEX_SEQ 
                        NOT IN( SELECT INDEX_SEQ 
                                FROM SVO.TB_SVC_MAS_MAPSHEET4000_48
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 5 AS NUM, 'เส้นโครงงาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM MGT1.TB_SVC_MAS_TRAVERSE 
                        WHERE RECORD_STATUS = 'N' 
                        AND TV_ZONE||TV_PROV||TV_PROV||TV_PROV||TV_PROV 
                        NOT IN( SELECT TV_ZONE||TV_PROV||TV_PROV||TV_PROV||TV_PROV
                                FROM SVO.TB_SVC_MAS_TRAVERSE
                                WHERE RECORD_STATUS = 'N')
                        ),
                    AP2 AS(
                        SELECT 1 AS NUM, 'หมุดเส้นโครงาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVC_MAS_CTRLMARK 
                        WHERE RECORD_STATUS = 'N' 
                        AND TV_ZONE||TV_PROV||TV_NAME||TVC_ORDERNO||TVC_CTRLNAME 
                        NOT IN( SELECT TV_ZONE||TV_PROV||TV_NAME||TVC_ORDERNO||TVC_CTRLNAME  
                                FROM MGT1.TB_SVC_MAS_CTRLMARK
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 2 AS NUM, 'หมุดดาวเทียม' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVC_MAS_GPSMARK 
                        WHERE RECORD_STATUS = 'N' 
                        AND GPSMARK_SEQ 
                        NOT IN( SELECT GPSMARK_SEQ 
                                FROM MGT1.TB_SVC_MAS_GPSMARK
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 3 AS NUM, 'ระวาง 4000 โซน 47' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVC_MAS_MAPSHEET4000_47 
                        WHERE RECORD_STATUS = 'N' 
                        AND INDEX_SEQ 
                        NOT IN( SELECT INDEX_SEQ 
                                FROM MGT1.TB_SVC_MAS_MAPSHEET4000_47
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 4 AS NUM, 'ระวาง 4000 โซน 48' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVC_MAS_MAPSHEET4000_48 
                        WHERE RECORD_STATUS = 'N' 
                        AND INDEX_SEQ 
                        NOT IN( SELECT INDEX_SEQ 
                                FROM MGT1.TB_SVC_MAS_MAPSHEET4000_48
                                WHERE RECORD_STATUS = 'N')
                        UNION
                        SELECT 5 AS NUM, 'เส้นโครงงาน' AS TYPE, COUNT(*) AS RECEIVE
                        FROM SVO.TB_SVC_MAS_TRAVERSE 
                        WHERE RECORD_STATUS = 'N' 
                        AND TV_ZONE||TV_PROV||TV_PROV||TV_PROV||TV_PROV 
                        NOT IN( SELECT TV_ZONE||TV_PROV||TV_PROV||TV_PROV||TV_PROV
                                FROM MGT1.TB_SVC_MAS_TRAVERSE
                                WHERE RECORD_STATUS = 'N')
                        )
                    SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
                    NVL(P1.RECEIVE,0) AS P1, 
                    NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
                    FROM P2
                    FULL OUTER JOIN  P1
                        ON P1.TYPE = P2.TYPE
                    FULL OUTER JOIN  AP2
                        ON P1.TYPE = AP2.TYPE
                    FULL OUTER JOIN  DP2
                        ON P1.TYPE = DP2.TYPE
                    ORDER BY NVL(P1.NUM,P2.NUM)";

                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'usp': //14
            $select = "WITH P1 AS(
                SELECT 1 AS NUM, 'ประเภทครุภัณฑ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_USP_MAS_TYPE_ASSET
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ประเภทและชื่อวัสดุ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_USP_MAS_MAT
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ประเภทและชื่อแบบพิมพ์' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_USP_MAS_PLATE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4 AS NUM, 'ประเภทหลักเขต' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_USP_MAS_PIECE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 5 AS NUM, 'ประเภทและชื่ออาคารและสิ่งก่อสร้าง' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_USP_MAS_BLD
                WHERE RECORD_STATUS = 'N'
                ),
            P2 AS(
                SELECT 1 AS NUM, 'ประเภทครุภัณฑ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM USP.TB_USP_MAS_TYPE_ASSET
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ประเภทและชื่อวัสดุ' AS TYPE, COUNT(*) AS RECEIVE  
                FROM USP.TB_USP_MAS_MAT
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ประเภทและชื่อแบบพิมพ์' AS TYPE, COUNT(*) AS RECEIVE  
                FROM USP.TB_USP_MAS_PLATE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4 AS NUM, 'ประเภทหลักเขต' AS TYPE, COUNT(*) AS RECEIVE  
                FROM USP.TB_USP_MAS_PIECE
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 5 AS NUM, 'ประเภทและชื่ออาคารและสิ่งก่อสร้าง' AS TYPE, COUNT(*) AS RECEIVE  
                FROM USP.TB_USP_MAS_BLD
                WHERE RECORD_STATUS = 'N'
                ),
            DP2 AS(
                SELECT 1 AS NUM, 'ประเภทครุภัณฑ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_USP_MAS_TYPE_ASSET 
                WHERE RECORD_STATUS = 'N' 
                AND TYPE_ASSET_SEQ  
                NOT IN( SELECT TYPE_ASSET_SEQ
                        FROM USP.TB_USP_MAS_TYPE_ASSET
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ประเภทและชื่อวัสดุ' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_USP_MAS_MAT 
                WHERE RECORD_STATUS = 'N' 
                AND MAT_SEQ 
                NOT IN( SELECT MAT_SEQ 
                        FROM USP.TB_USP_MAS_MAT
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'ประเภทและชื่อแบบพิมพ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_USP_MAS_PLATE 
                WHERE RECORD_STATUS = 'N' 
                AND PLATE_SEQ 
                NOT IN( SELECT PLATE_SEQ 
                        FROM USP.TB_USP_MAS_PLATE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 4 AS NUM, 'ประเภทหลักเขต' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_USP_MAS_PIECE 
                WHERE RECORD_STATUS = 'N' 
                AND PIECE_SEQ 
                NOT IN( SELECT PIECE_SEQ 
                        FROM USP.TB_USP_MAS_PIECE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 5 AS NUM, 'ประเภทและชื่ออาคารและสิ่งก่อสร้าง' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_USP_MAS_BLD 
                WHERE RECORD_STATUS = 'N' 
                AND BLD_SEQ 
                NOT IN( SELECT BLD_SEQ 
                        FROM USP.TB_USP_MAS_BLD
                        WHERE RECORD_STATUS = 'N')
                ),
            AP2 AS(
                SELECT 1 AS NUM, 'ประเภทครุภัณฑ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM USP.TB_USP_MAS_TYPE_ASSET 
                WHERE RECORD_STATUS = 'N' 
                AND TYPE_ASSET_SEQ  
                NOT IN( SELECT TYPE_ASSET_SEQ
                        FROM MGT1.TB_USP_MAS_TYPE_ASSET
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ประเภทและชื่อวัสดุ' AS TYPE, COUNT(*) AS RECEIVE
                FROM USP.TB_USP_MAS_MAT 
                WHERE RECORD_STATUS = 'N' 
                AND MAT_SEQ 
                NOT IN( SELECT MAT_SEQ 
                        FROM MGT1.TB_USP_MAS_MAT
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'ประเภทและชื่อแบบพิมพ์' AS TYPE, COUNT(*) AS RECEIVE
                FROM USP.TB_USP_MAS_PLATE 
                WHERE RECORD_STATUS = 'N' 
                AND PLATE_SEQ 
                NOT IN( SELECT PLATE_SEQ 
                        FROM MGT1.TB_USP_MAS_PLATE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 4 AS NUM, 'ประเภทหลักเขต' AS TYPE, COUNT(*) AS RECEIVE
                FROM USP.TB_USP_MAS_PIECE 
                WHERE RECORD_STATUS = 'N' 
                AND PIECE_SEQ 
                NOT IN( SELECT PIECE_SEQ 
                        FROM MGT1.TB_USP_MAS_PIECE
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 5 AS NUM, 'ประเภทและชื่ออาคารและสิ่งก่อสร้าง' AS TYPE, COUNT(*) AS RECEIVE
                FROM USP.TB_USP_MAS_BLD 
                WHERE RECORD_STATUS = 'N' 
                AND BLD_SEQ 
                NOT IN( SELECT BLD_SEQ 
                        FROM MGT1.TB_USP_MAS_BLD
                        WHERE RECORD_STATUS = 'N')
                )
            SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
            NVL(P1.RECEIVE,0) AS P1, 
            NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
            FROM P2
            FULL OUTER JOIN  P1
                ON P1.TYPE = P2.TYPE
            FULL OUTER JOIN  AP2
                 ON P1.TYPE = AP2.TYPE
            FULL OUTER JOIN  DP2
                 ON P1.TYPE = DP2.TYPE
            ORDER BY NVL(P1.NUM,P2.NUM)";
            
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'esp': //15
            $select = "WITH P1 AS(
                SELECT 1 AS NUM, 'ข้อมูลวันที่' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ESP_MAS_DAY
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ข้อมูลสถานะการดึงข้อมูลทะเบียนราษฎร์' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ESP_MAS_MOI_STATUS
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ข้อมูลเดือน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ESP_MAS_MONTH
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4 AS NUM, 'ข้อมูลเวลา' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ESP_MAS_TIME_RANG
                WHERE RECORD_STATUS = 'N'
                ),
            P2 AS(
                SELECT 1 AS NUM, 'ข้อมูลวันที่' AS TYPE, COUNT(*) AS RECEIVE
                FROM ESP.TB_ESP_MAS_DAY
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ข้อมูลสถานะการดึงข้อมูลทะเบียนราษฎร์' AS TYPE, COUNT(*) AS RECEIVE  
                FROM ESP.TB_ESP_MAS_MOI_STATUS
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 3 AS NUM, 'ข้อมูลเดือน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM ESP.TB_ESP_MAS_MONTH
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 4 AS NUM, 'ข้อมูลเวลา' AS TYPE, COUNT(*) AS RECEIVE  
                FROM ESP.TB_ESP_MAS_TIME_RANG
                WHERE RECORD_STATUS = 'N'
                ),
            DP2 AS(
                SELECT 1 AS NUM, 'ข้อมูลวันที่' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ESP_MAS_DAY 
                WHERE RECORD_STATUS = 'N' 
                AND DAY_SEQ  
                NOT IN( SELECT DAY_SEQ
                        FROM ESP.TB_ESP_MAS_DAY
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ข้อมูลสถานะการดึงข้อมูลทะเบียนราษฎร์' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ESP_MAS_MOI_STATUS 
                WHERE RECORD_STATUS = 'N' 
                AND MOI_STATUS_SEQ 
                NOT IN( SELECT MOI_STATUS_SEQ 
                        FROM ESP.TB_ESP_MAS_MOI_STATUS
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'ข้อมูลเดือน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ESP_MAS_MONTH 
                WHERE RECORD_STATUS = 'N' 
                AND MONTH_SEQ 
                NOT IN( SELECT MONTH_SEQ 
                        FROM ESP.TB_ESP_MAS_MONTH
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 4 AS NUM, 'ข้อมูลเวลา' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ESP_MAS_TIME_RANG 
                WHERE RECORD_STATUS = 'N' 
                AND TIME_RANG_SEQ 
                NOT IN( SELECT TIME_RANG_SEQ 
                        FROM ESP.TB_ESP_MAS_TIME_RANG
                        WHERE RECORD_STATUS = 'N')
                ),
            AP2 AS(
                SELECT 1 AS NUM, 'ข้อมูลวันที่' AS TYPE, COUNT(*) AS RECEIVE
                FROM ESP.TB_ESP_MAS_DAY 
                WHERE RECORD_STATUS = 'N' 
                AND DAY_SEQ  
                NOT IN( SELECT DAY_SEQ
                        FROM MGT1.TB_ESP_MAS_DAY
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ข้อมูลสถานะการดึงข้อมูลทะเบียนราษฎร์' AS TYPE, COUNT(*) AS RECEIVE
                FROM ESP.TB_ESP_MAS_MOI_STATUS 
                WHERE RECORD_STATUS = 'N' 
                AND MOI_STATUS_SEQ 
                NOT IN( SELECT MOI_STATUS_SEQ 
                        FROM MGT1.TB_ESP_MAS_MOI_STATUS
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 3 AS NUM, 'ข้อมูลเดือน' AS TYPE, COUNT(*) AS RECEIVE
                FROM ESP.TB_ESP_MAS_MONTH 
                WHERE RECORD_STATUS = 'N' 
                AND MONTH_SEQ 
                NOT IN( SELECT MONTH_SEQ 
                        FROM MGT1.TB_ESP_MAS_MONTH
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 4 AS NUM, 'ข้อมูลเวลา' AS TYPE, COUNT(*) AS RECEIVE
                FROM ESP.TB_ESP_MAS_TIME_RANG 
                WHERE RECORD_STATUS = 'N' 
                AND TIME_RANG_SEQ 
                NOT IN( SELECT TIME_RANG_SEQ 
                        FROM MGT1.TB_ESP_MAS_TIME_RANG
                        WHERE RECORD_STATUS = 'N')
                )
            SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
            NVL(P1.RECEIVE,0) AS P1, 
            NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
            FROM P2
            FULL OUTER JOIN  P1
                ON P1.TYPE = P2.TYPE
            FULL OUTER JOIN  AP2
                 ON P1.TYPE = AP2.TYPE
            FULL OUTER JOIN  DP2
                 ON P1.TYPE = DP2.TYPE
            ORDER BY NVL(P1.NUM,P2.NUM)";

            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case 'ech': //16
            $select = "WITH P1 AS(
                SELECT 1 AS NUM, 'ข้อมูลบันทึกข้อตกลง' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ECH_MAS_MOU
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ข้อมูลรายชื่อองค์กรที่ทำการแลกเปลี่ยนข้อมูลกับกรมที่ดิน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM MGT1.TB_ECH_MAS_ORGANIZE
                WHERE RECORD_STATUS = 'N'
                ),
            P2 AS(
                SELECT 1 AS NUM, 'ข้อมูลบันทึกข้อตกลง' AS TYPE, COUNT(*) AS RECEIVE
                FROM ECH.TB_ECH_MAS_MOU
                WHERE RECORD_STATUS = 'N'
                UNION 
                SELECT 2 AS NUM, 'ข้อมูลรายชื่อองค์กรที่ทำการแลกเปลี่ยนข้อมูลกับกรมที่ดิน' AS TYPE, COUNT(*) AS RECEIVE  
                FROM ECH.TB_ECH_MAS_ORGANIZE
                WHERE RECORD_STATUS = 'N'
                ),
            DP2 AS(
                SELECT 1 AS NUM, 'ข้อมูลบันทึกข้อตกลง' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ECH_MAS_MOU 
                WHERE RECORD_STATUS = 'N' 
                AND MOU_SUBJECT  
                NOT IN( SELECT MOU_SUBJECT
                        FROM ECH.TB_ECH_MAS_MOU
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ข้อมูลรายชื่อองค์กรที่ทำการแลกเปลี่ยนข้อมูลกับกรมที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
                FROM MGT1.TB_ECH_MAS_ORGANIZE 
                WHERE RECORD_STATUS = 'N' 
                AND LANDOFFICE_SEQ 
                NOT IN( SELECT LANDOFFICE_SEQ 
                        FROM ECH.TB_ECH_MAS_ORGANIZE
                        WHERE RECORD_STATUS = 'N')
                ),
            AP2 AS(
                SELECT 1 AS NUM, 'ข้อมูลบันทึกข้อตกลง' AS TYPE, COUNT(*) AS RECEIVE
                FROM ECH.TB_ECH_MAS_MOU 
                WHERE RECORD_STATUS = 'N' 
                AND MOU_SUBJECT  
                NOT IN( SELECT MOU_SUBJECT
                        FROM MGT1.TB_ECH_MAS_MOU
                        WHERE RECORD_STATUS = 'N')
                UNION
                SELECT 2 AS NUM, 'ข้อมูลรายชื่อองค์กรที่ทำการแลกเปลี่ยนข้อมูลกับกรมที่ดิน' AS TYPE, COUNT(*) AS RECEIVE
                FROM ECH.TB_ECH_MAS_ORGANIZE 
                WHERE RECORD_STATUS = 'N' 
                AND LANDOFFICE_SEQ 
                NOT IN( SELECT LANDOFFICE_SEQ
                        FROM MGT1.TB_ECH_MAS_ORGANIZE
                        WHERE RECORD_STATUS = 'N')
                )
            SELECT NVL(P1.NUM,P2.NUM) AS NUM, NVL(P1.TYPE,P2.TYPE) AS TYPE ,
            NVL(P1.RECEIVE,0) AS P1, 
            NVL(P2.RECEIVE,0) AS P2,
                    AP2.RECEIVE AS AP2RECEIVE,
                    DP2.RECEIVE AS DP2RECEIVE
            FROM P2
            FULL OUTER JOIN  P1
                ON P1.TYPE = P2.TYPE
            FULL OUTER JOIN  AP2
                 ON P1.TYPE = AP2.TYPE
            FULL OUTER JOIN  DP2
                 ON P1.TYPE = DP2.TYPE
            ORDER BY NVL(P1.NUM,P2.NUM)";

            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;
    }


    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);


?>

