<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ข้อมูลสิทธิ
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ADM_MAS_PERMISSION
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ADM.TB_ADM_MAS_PERMISSION 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.PERMISSION_SEQ AS PERMISSION_SEQ_P1 , P2.PERMISSION_SEQ AS PERMISSION_SEQ_P2
                        ,P1.PERMISSION_ID AS PERMISSION_ID_P1 , P2.PERMISSION_ID AS PERMISSION_ID_P2
                        ,P1.PERMISSION_NAME AS PERMISSION_NAME_P1 , P2.PERMISSION_NAME AS PERMISSION_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PERMISSION_SEQ = P2.PERMISSION_SEQ
                    ORDER BY P1.PERMISSION_SEQ
                            ,P2.PERMISSION_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ข้อมูลระบบงาน    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ADM_MAS_SYSTEM
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ADM.TB_ADM_MAS_SYSTEM 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.SYSTEM_SEQ AS SYSTEM_SEQ_P1 , P2.SYSTEM_SEQ AS SYSTEM_SEQ_P2
                        ,P1.SYSTEM_ABBR AS SYSTEM_ABBR_P1 , P2.SYSTEM_ABBR AS SYSTEM_ABBR_P2
                        ,P1.SYSTEM_NAME_TH AS SYSTEM_NAME_TH_P1 , P2.SYSTEM_NAME_TH AS SYSTEM_NAME_TH_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.SYSTEM_SEQ = P2.SYSTEM_SEQ
                    ORDER BY P1.SYSTEM_SEQ
                            ,P2.SYSTEM_SEQ";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ข้อมูลหน้าจอ   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ADM_MAS_SCREEN
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ADM.TB_ADM_MAS_SCREEN 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.SCREEN_SEQ AS SCREEN_SEQ_P1 , P2.SCREEN_SEQ AS SCREEN_SEQ_P2
                        ,P1.SCREEN_ID AS SCREEN_ID_P1 , P2.SCREEN_ID AS SCREEN_ID_P2
                        ,P1.SCREEN_NAME_TH AS SCREEN_NAME_TH_P1 , P2.SCREEN_NAME_TH AS SCREEN_NAME_TH_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.SCREEN_SEQ = P2.SCREEN_SEQ
                    ORDER BY P1.SCREEN_SEQ
                            ,P2.SCREEN_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //ข้อมูลกลุ่มผู้ใช้งาน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ADM_MAS_USER_GROUP
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ADM.TB_ADM_MAS_USER_GROUP 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.USER_GROUP_SEQ AS USER_GROUP_SEQ_P1 , P2.USER_GROUP_SEQ AS USER_GROUP_SEQ_P2
                        ,P1.USER_GROUP_ID AS USER_GROUP_ID_P1 , P2.USER_GROUP_ID AS USER_GROUP_ID_P2
                        ,P1.USER_GROUP_NAME AS USER_GROUP_NAME_P1 , P2.USER_GROUP_NAME AS USER_GROUP_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.USER_GROUP_SEQ = P2.USER_GROUP_SEQ
                    ORDER BY SUBSTR(P1.USER_GROUP_ID,1,3), P1.USER_GROUP_SEQ
                            ,SUBSTR(P2.USER_GROUP_ID,1,3), P2.USER_GROUP_SEQ";
        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '5': //ข้อมูลกลุ่มเมนู
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ADM_MENU_GROUP
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ADM.TB_ADM_MENU_GROUP 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.MENU_GROUP_SEQ AS MENU_GROUP_SEQ_P1 , P2.MENU_GROUP_SEQ AS MENU_GROUP_SEQ_P2
                        ,P1.MENU_GROUP_NAME AS MENU_GROUP_NAME_P1 , P2.MENU_GROUP_NAME AS MENU_GROUP_NAME_P2
                        ,P1.MENU_GROUP_DESC AS MENU_GROUP_DESC_P1 , P2.MENU_GROUP_DESC AS MENU_GROUP_DESC_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.MENU_GROUP_SEQ = P2.MENU_GROUP_SEQ
                    LEFT OUTER JOIN ADM.TB_ADM_MAS_SYSTEM S1
                        ON SUBSTR(P1.MENU_GROUP_NAME,1,3) = S1.SYSTEM_ABBR
                    LEFT OUTER JOIN ADM.TB_ADM_MAS_SYSTEM S2
                        ON SUBSTR(P2.MENU_GROUP_NAME,1,3) = S2.SYSTEM_ABBR
                    ORDER BY SUBSTR(P1.MENU_GROUP_NAME,1,3), P1.MENU_GROUP_SEQ
                            ,SUBSTR(P2.MENU_GROUP_NAME,1,3), P2.MENU_GROUP_SEQ";
              
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '6': //ข้อมูลกลุ่มตำแหน่ง   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ADM_MAS_POSITION_GROUP
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ADM.TB_ADM_MAS_POSITION_GROUP 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.POSITION_GROUP_SEQ AS POSITION_GROUP_SEQ_P1 , P2.POSITION_GROUP_SEQ AS POSITION_GROUP_SEQ_P2
                        ,P1.POSITION_GROUP_ID AS POSITION_GROUP_ID_P1 , P2.POSITION_GROUP_ID AS POSITION_GROUP_ID_P2
                        ,P1.POSITION_GROUP_NAME AS POSITION_GROUP_NAME_P1 , P2.POSITION_GROUP_NAME AS POSITION_GROUP_NAME_P2
        --                        ,CASE NVL(P1.POSITION_GROUP_TYPE,'99') WHEN '0' THEN 'กลุ่มตำแหน่งส่วนกลาง'
        --                        WHEN '1' THEN 'กลุ่มตำแหน่งส่วนภูมิภาค' END AS POSITION_GROUP_TYPE_P1
        --                        ,CASE NVL(P2.POSITION_GROUP_TYPE,'99') WHEN '0' THEN 'กลุ่มตำแหน่งส่วนกลาง'
        --                        WHEN '1' THEN 'กลุ่มตำแหน่งส่วนภูมิภาค' END AS POSITION_GROUP_TYPE_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.POSITION_GROUP_SEQ = P2.POSITION_GROUP_SEQ
                    ORDER BY SUBSTR(P1.POSITION_GROUP_ID,1,3), P1.POSITION_GROUP_SEQ
                            ,SUBSTR(P2.POSITION_GROUP_ID,1,3), P2.POSITION_GROUP_SEQ";
               
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;
    }
?>