<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ข้อมูลห้องชุด
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_APS_MAS_ROOM_USAGE
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM APS.TB_APS_MAS_ROOM_USAGE 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.ROOM_USAGE_SEQ AS ROOM_USAGE_SEQ_P1 , P2.ROOM_USAGE_SEQ AS ROOM_USAGE_SEQ_P2
                        ,P1.ROOM_USAGE_NAME AS ROOM_USAGE_NAME_P1 , P2.ROOM_USAGE_NAME AS ROOM_USAGE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                        ,P1.ROOM_USAGE_ID AS ROOM_USAGE_ID_P1 , P2.ROOM_USAGE_ID AS ROOM_USAGE_ID_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.ROOM_USAGE_SEQ = P2.ROOM_USAGE_SEQ
                    ORDER BY P1.ROOM_USAGE_SEQ
                            ,P2.ROOM_USAGE_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ข้อมูลพื้นที่    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_APS_MAS_POSSESSORY
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM APS.TB_APS_MAS_POSSESSORY 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.POSSESSORY_SEQ AS POSSESSORY_SEQ_P1 , P2.POSSESSORY_SEQ AS POSSESSORY_SEQ_P2
                        ,P1.POSSESSORY_ID AS POSSESSORY_ID_P1 , P2.POSSESSORY_ID AS POSSESSORY_ID_P2
                        ,P1.POSSESSORY_NAME AS POSSESSORY_NAME_P1 , P2.POSSESSORY_NAME AS POSSESSORY_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.POSSESSORY_SEQ = P2.POSSESSORY_SEQ
                    ORDER BY P1.POSSESSORY_SEQ
                            ,P2.POSSESSORY_SEQ";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ข้อมูลชั้น   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_APS_MAS_CONDO_FLOOR
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM APS.TB_APS_MAS_CONDO_FLOOR 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.FLOOR_SEQ AS FLOOR_SEQ_P1 , P2.FLOOR_SEQ AS FLOOR_SEQ_P2
                        ,P1.FLOOR_NAME AS FLOOR_NAME_P1 , P2.FLOOR_NAME AS FLOOR_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.FLOOR_SEQ = P2.FLOOR_SEQ
                    ORDER BY P1.FLOOR_SEQ
                            ,P2.FLOOR_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

    }
?>