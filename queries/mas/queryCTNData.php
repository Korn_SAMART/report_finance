<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ข้อมูลประเภทหนังสือ
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_CTN_MAS_DOC_CLASS
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM CTN.TB_CTN_MAS_DOC_CLASS 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.DOC_CLASS_SEQ AS DOC_CLASS_SEQ_P1 ,  P2.DOC_CLASS_SEQ AS DOC_CLASS_SEQ_P2
                        ,P1.DOC_CLASS_NAME AS DOC_CLASS_NAME_P1 ,  P2.DOC_CLASS_NAME AS DOC_CLASS_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.DOC_CLASS_SEQ = P2.DOC_CLASS_SEQ
                    ORDER BY P1.DOC_CLASS_SEQ
                            ,P2.DOC_CLASS_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ข้อมูลชั้นความลับ    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_CTN_MAS_DOC_SECRET
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM CTN.TB_CTN_MAS_DOC_SECRET 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.DOC_SECRET_SEQ AS DOC_SECRET_SEQ_P1 ,  P2.DOC_SECRET_SEQ AS DOC_SECRET_SEQ_P2
                        ,P1.DOC_SECRET_NAME AS DOC_SECRET_NAME_P1 ,  P2.DOC_SECRET_NAME AS DOC_SECRET_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.DOC_SECRET_SEQ = P2.DOC_SECRET_SEQ
                    ORDER BY P1.DOC_SECRET_SEQ
                            ,P2.DOC_SECRET_SEQ";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ข้อมูลหมวดหมู่เอกสาร   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_CTN_MAS_DOC_TYPE
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM CTN.TB_CTN_MAS_DOC_TYPE 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.DOC_TYPE_SEQ AS DOC_TYPE_SEQ_P1 ,  P2.DOC_TYPE_SEQ AS DOC_TYPE_SEQ_P2
                        ,P1.DOC_TYPE_NAME AS DOC_TYPE_NAME_P1 ,  P2.DOC_TYPE_NAME AS DOC_TYPE_NAME_P2
        --                        ,P1.YEAR_EXPIRE AS YEAR_EXPIRE_P1 ,  P2.YEAR_EXPIRE AS YEAR_EXPIRE_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.DOC_TYPE_SEQ = P2.DOC_TYPE_SEQ
                    ORDER BY P1.DOC_TYPE_SEQ
                            ,P2.DOC_TYPE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //ข้อมูลเลขหนังสือของสำนักงาน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_CTN_MAS_LANDOFFICE_LETTER
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM CTN.TB_CTN_MAS_LANDOFFICE_LETTER 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 ,  P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
                        ,L1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 ,L2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
                        ,P1.LANDOFFICE_LETTER_NO AS LANDOFFICE_LETTER_NO_P1 ,  P2.LANDOFFICE_LETTER_NO AS LANDOFFICE_LETTER_NO_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE L1
                        ON P1.LANDOFFICE_SEQ = L1.LANDOFFICE_SEQ
                        AND L1.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE L2
                        ON P2.LANDOFFICE_SEQ = L2.LANDOFFICE_SEQ
                        AND L2.RECORD_STATUS = 'N'
                    ORDER BY P1.LANDOFFICE_SEQ
                            ,P2.LANDOFFICE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

    }
?>