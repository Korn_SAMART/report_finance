<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ข้อมูลบันทึกข้อตกลง
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ECH_MAS_MOU
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ECH.TB_ECH_MAS_MOU
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.MOU_SEQ AS MOU_SEQ_P1 ,  P2.MOU_SEQ AS MOU_SEQ_P2
                        ,P1.MOU_NO AS MOU_NO_P1 ,  P2.MOU_NO AS MOU_NO_P2
        --                        ,CASE P1.ORGANIZE_ID_1
        --                        WHEN 156 THEN 'กรมที่ดิน'
        --                        WHEN 244 THEN 'สำนักเทคโนโลยีสารสนเทศ'
        --                        END AS LANDOFFICE_DOL_P1
        --                        ,CASE P2.LANDOFFICE_SEQ_DOL
        --                        WHEN 156 THEN 'กรมที่ดิน'
        --                        WHEN 244 THEN 'สำนักเทคโนโลยีสารสนเทศ'
        --                        END AS LANDOFFICE_DOL_P2
        --                        ,T1.LANDOFFICE_NAME_TH AS LANDOFFICE_SEQ_EXCHG_P1, T2.LANDOFFICE_NAME_TH AS LANDOFFICE_SEQ_EXCHG_P2
                        ,P1.MOU_SUBJECT AS MOU_SUBJECT_P1 ,  P2.MOU_SUBJECT AS MOU_SUBJECT_P2
        --                        ,P1.SIGN_DATE AS SIGN_DATE_P1 ,  P2.SIGN_DATE AS SIGN_DATE_P2
        --                        ,P1.EFFECTIVE_DATE AS EFFECTIVE_DATE_P1 ,  P2.EFFECTIVE_DATE AS EFFECTIVE_DATE_P2
        --                        ,P1.EXPIRE_DATE AS EXPIRE_DATE_P1 ,  P2.EXPIRE_DATE AS EXPIRE_DATE_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2  
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.MOU_SEQ = P2.MOU_SEQ
                        OR P1.MOU_SUBJECT = P2.MOU_SUBJECT
                    LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE T1
                        ON P1.ORGANIZE_ID_2 = T1.LANDOFFICE_SEQ
                    LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE T2
                        ON P2.LANDOFFICE_SEQ_EXCHG = T2.LANDOFFICE_SEQ
                    ORDER BY P1.MOU_SEQ 
                            ,P2.MOU_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ข้อมูลรายชื่อองค์กรที่ทำการแลกเปลี่ยนข้อมูลกับกรมที่ดิน    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ECH_MAS_ORGANIZE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ECH.TB_ECH_MAS_ORGANIZE
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.ORGANIZE_SEQ AS ORGANIZE_SEQ_P1 ,  P2.ORGANIZE_SEQ AS ORGANIZE_SEQ_P2
                        ,P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 ,  P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
                        ,T1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1, T2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
                        ,CASE P1.ORGANIZE_FLAG
                        WHEN '0' THEN 'หน่วยงานภายใน'
                        WHEN '1' THEN 'หน่วยงานภายนอก'
                        END AS ORGANIZE_FLAG_P1
                        ,CASE P2.ORGANIZE_FLAG
                        WHEN '0' THEN 'หน่วยงานภายใน'
                        WHEN '1' THEN 'หน่วยงานภายนอก'
                        END AS ORGANIZE_FLAG_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2   
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE T1
                        ON P1.LANDOFFICE_SEQ = T1.LANDOFFICE_SEQ
                    LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE T2
                        ON P2.LANDOFFICE_SEQ = T2.LANDOFFICE_SEQ
                    ORDER BY P1.LANDOFFICE_SEQ
                            ,P2.LANDOFFICE_SEQ";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

    }
?>