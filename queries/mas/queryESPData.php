<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ข้อมูลวันที่
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ESP_MAS_DAY
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ESP.TB_ESP_MAS_DAY
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.DAY_SEQ AS DAY_SEQ_P1 ,  P2.DAY_SEQ AS DAY_SEQ_P2
                        ,P1.DAY_ID AS DAY_ID_P1 ,  P2.DAY_ID AS DAY_ID_P2
                        ,CASE P1.DAY_FLAG
                        WHEN '0' THEN '30 วัน'
                        WHEN '1' THEN '31 วัน'
                        END AS DAY_FLAG_P1
                        ,CASE P2.DAY_FLAG
                        WHEN '0' THEN '30 วัน'
                        WHEN '1' THEN '31 วัน'
                        END AS DAY_FLAG_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2  
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.DAY_SEQ = P2.DAY_SEQ
                    ORDER BY P1.DAY_SEQ
                            ,P2.DAY_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ข้อมูลสถานะการดึงข้อมูลทะเบียนราษฎร์    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ESP_MAS_MOI_STATUS
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ESP.TB_ESP_MAS_MOI_STATUS
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.MOI_STATUS_SEQ AS MOI_STATUS_SEQ_P1 ,  P2.MOI_STATUS_SEQ AS MOI_STATUS_SEQ_P2
                        ,P1.MOI_STATUS_CODE AS MOI_STATUS_CODE_P1 ,  P2.MOI_STATUS_CODE AS MOI_STATUS_CODE_P2
                        ,P1.MOI_STATUS_DESC AS MOI_STATUS_DESC_P1 ,  P2.MOI_STATUS_DESC AS MOI_STATUS_DESC_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2   
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.MOI_STATUS_SEQ = P2.MOI_STATUS_SEQ
                    ORDER BY P1.MOI_STATUS_SEQ 
                            ,P2.MOI_STATUS_SEQ";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ข้อมูลเดือน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ESP_MAS_MONTH
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ESP.TB_ESP_MAS_MONTH
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.MONTH_SEQ AS MONTH_SEQ_P1 ,  P2.MONTH_SEQ AS MONTH_SEQ_P2
                        ,P1.MONTH_ID AS MONTH_ID_P1 ,  P2.MONTH_ID AS MONTH_ID_P2
                        ,P1.MONTH_NAME_TH AS MONTH_NAME_TH_P1 ,  P2.MONTH_NAME_TH AS MONTH_NAME_TH_P2
                        ,P1.MONTH_NAME_EN AS MONTH_NAME_EN_P1 ,  P2.MONTH_NAME_EN AS MONTH_NAME_EN_P2
        --                        ,P1.MONTH_NAME_ABBR_TH AS MONTH_NAME_ABBR_TH_P1 ,  P2.MONTH_NAME_ABBR_TH AS MONTH_NAME_ABBR_TH_P2
        --                        ,P1.MONTH_NAME_ABBR_EH AS MONTH_NAME_ABBR_EH_P1 ,  P2.MONTH_NAME_ABBR_EH AS MONTH_NAME_ABBR_EH_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2   
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.MONTH_SEQ = P2.MONTH_SEQ
                    ORDER BY P1.MONTH_SEQ
                            ,P2.MONTH_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //ข้อมูลเวลา
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_ESP_MAS_TIME_RANG
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM ESP.TB_ESP_MAS_TIME_RANG
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.TIME_RANG_SEQ AS TIME_RANG_SEQ_P1 ,  P2.TIME_RANG_SEQ AS TIME_RANG_SEQ_P2
                        ,P1.TIME_RANG_ID AS TIME_RANG_ID_P1 ,  P2.TIME_RANG_ID AS TIME_RANG_ID_P2
                        ,P1.TIME_RANG_NAME AS TIME_RANG_NAME_P1 ,  P2.TIME_RANG_NAME AS TIME_RANG_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2  
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.TIME_RANG_SEQ = P2.TIME_RANG_SEQ
                    ORDER BY P1.TIME_RANG_SEQ
                            ,P2.TIME_RANG_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

    }
?>