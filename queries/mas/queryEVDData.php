<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ชื่อย่อจังหวัด
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_EVD_MAS_PROVINCE_ABBR
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM REG.TB_EVD_MAS_PROVINCE_ABBR
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.PROVINCE_ABBR_SEQ AS PROVINCE_ABBR_SEQ_P1 ,  P2.PROVINCE_ABBR_SEQ AS PROVINCE_ABBR_SEQ_P2
                        ,T1.PROVINCE_NAME AS PROVINCE_NAME_P1 , T2.PROVINCE_NAME AS PROVINCE_NAME_P2
                        ,P1.PROVINCE_ABBR_2TH AS PROVINCE_ABBR_2TH_P1 ,  P2.PROVINCE_ABBR_2TH AS PROVINCE_ABBR_2TH_P2
                        ,P1.PROVINCE_ABBR_2EN AS PROVINCE_ABBR_2EN_P1 ,  P2.PROVINCE_ABBR_2EN AS PROVINCE_ABBR_2EN_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PROVINCE_ABBR_SEQ = P2.PROVINCE_ABBR_SEQ
                    LEFT OUTER JOIN MGT1.TB_MAS_PROVINCE T1
                        ON P1.PROVINCE_SEQ = T1.PROVINCE_SEQ
                        AND T1.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MAS.TB_MAS_PROVINCE T2
                        ON P2.PROVINCE_SEQ = T2.PROVINCE_SEQ
                        AND T2.RECORD_STATUS = 'N'
                    ORDER BY P1.PROVINCE_ABBR_SEQ, NLSSORT(T1.PROVINCE_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P1.PROVINCE_ABBR_2TH, 'NLS_SORT=THAI_DICTIONARY')
                            ,P1.PROVINCE_ABBR_SEQ, NLSSORT(T2.PROVINCE_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P2.PROVINCE_ABBR_2TH, 'NLS_SORT=THAI_DICTIONARY')";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //สถานะนำเข้าภาพลักษณ์    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_EVD_MAS_IMAGE_STS
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM REG.TB_EVD_MAS_IMAGE_STS
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.IMAGE_STS_SEQ AS IMAGE_STS_SEQ_P1 ,  P2.IMAGE_STS_SEQ AS IMAGE_STS_SEQ_P2
                        ,P1.IMAGE_STS_ID AS IMAGE_STS_ID_P1 ,  P2.IMAGE_STS_ID AS IMAGE_STS_ID_P2
                        ,P1.IMAGE_STS_NAME AS IMAGE_STS_NAME_P1 ,  P2.IMAGE_STS_NAME AS IMAGE_STS_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.IMAGE_STS_SEQ = P2.IMAGE_STS_SEQ
                    ORDER BY NVL(P1.IMAGE_STS_SEQ,P2.IMAGE_STS_SEQ)";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ภาพลักษณ์ต้นร่างแบบอัตโนมัติ   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_EVD_MAS_DOCTYPE_MAPPING
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM REG.TB_EVD_MAS_DOCTYPE_MAPPING
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.DOCTYPE_MAPPING_SEQ AS DOCTYPE_MAPPING_SEQ_P1 ,  P2.DOCTYPE_MAPPING_SEQ AS DOCTYPE_MAPPING_SEQ_P2
                        ,P1.DOCTYPE_MAPPING_ID AS DOCTYPE_MAPPING_ID_P1 ,  P2.DOCTYPE_MAPPING_ID AS DOCTYPE_MAPPING_ID_P2
                        ,P1.DOCTYPE_MAPPING_NAME AS DOCTYPE_MAPPING_NAME_P1 ,  P2.DOCTYPE_MAPPING_NAME AS DOCTYPE_MAPPING_NAME_P2
        --                        ,T1.SURVEYDOCTYPE_NAME AS SURVEYDOCTYPE_NAME_P1 ,  T2.SURVEYDOCTYPE_NAME AS SURVEYDOCTYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.DOCTYPE_MAPPING_SEQ = P2.DOCTYPE_MAPPING_SEQ
                    LEFT OUTER JOIN MGT1.TB_SVA_MAS_SURVEYDOCTYPE T1
                        ON P1.SURVEYDOCTYPE_SEQ = T1.SURVEYDOCTYPE_SEQ
                    LEFT OUTER JOIN SVO.TB_SVA_MAS_SURVEYDOCTYPE T2
                        ON P2.SURVEYDOCTYPE_SEQ = T2.SURVEYDOCTYPE_SEQ
                    ORDER BY NVL(P1.DOCTYPE_MAPPING_SEQ,P2.DOCTYPE_MAPPING_SEQ)";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;


    }
?>