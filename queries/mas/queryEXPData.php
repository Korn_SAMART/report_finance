<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ข้อมูลประเภทสาเหตุที่ไม่ชอบด้วยกฏหมาย
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_EXP_MAS_ILLEGALITY_TYPE
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM EXP.TB_EXP_MAS_ILLEGALITY_TYPE 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.ILLEGALITY_TYPE_SEQ AS ILLEGALITY_TYPE_SEQ_P1 , P2.ILLEGALITY_TYPE_SEQ AS ILLEGALITY_TYPE_SEQ_P2
                        ,P1.ILLEGALITY_TYPE_NAME AS ILLEGALITY_TYPE_NAME_P1 , P2.ILLEGALITY_TYPE_NAME AS ILLEGALITY_TYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.ILLEGALITY_TYPE_SEQ = P2.ILLEGALITY_TYPE_SEQ
                        OR SUBSTR(P1.ILLEGALITY_TYPE_NAME,1,10) = SUBSTR(P2.ILLEGALITY_TYPE_NAME,1,10)
                    ORDER BY P1.ILLEGALITY_TYPE_SEQ
                            ,P2.ILLEGALITY_TYPE_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ข้อมูลประเภทงาน    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_EXP_MAS_JOB_GROUP
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM EXP.TB_EXP_MAS_JOB_GROUP 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.JOB_GROUP_SEQ AS JOB_GROUP_SEQ_P1 , P2.JOB_GROUP_SEQ AS JOB_GROUP_SEQ_P2
                        ,P1.JOB_GROUP_NAME AS JOB_GROUP_NAME_P1 , P2.JOB_GROUP_NAME AS JOB_GROUP_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.JOB_GROUP_SEQ = P2.JOB_GROUP_SEQ
                        OR P1.JOB_GROUP_NAME = P2.JOB_GROUP_NAME
                    ORDER BY P1.JOB_GROUP_SEQ
                            ,P2.JOB_GROUP_SEQ";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ข้อมูลขั้นตอนการดำเนินการ   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_EXP_MAS_PROC_TYPE
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM EXP.TB_EXP_MAS_PROC_TYPE 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.PROC_TYPE_SEQ AS PROC_TYPE_SEQ_P1 , P2.PROC_TYPE_SEQ AS PROC_TYPE_SEQ_P2
                        ,P1.PROC_TYPE_NAME AS PROC_TYPE_NAME_P1 , P2.PROC_TYPE_NAME AS PROC_TYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PROC_TYPE_SEQ = P2.PROC_TYPE_SEQ
                        -- OR P1.PROC_TYPE_NAME = P2.PROC_TYPE_NAME
                    ORDER BY P1.PROC_TYPE_SEQ
                            ,P2.PROC_TYPE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //ข้อมูลประเภทการร้องเรียน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_EXP_MAS_REQUEST_TYPE
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM EXP.TB_EXP_MAS_REQUEST_TYPE 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.REQUEST_TYPE_SEQ AS REQUEST_TYPE_SEQ_P1 , P2.REQUEST_TYPE_SEQ AS REQUEST_TYPE_SEQ_P2
                        ,P1.REQUEST_NAME AS REQUEST_NAME_P1 , P2.REQUEST_NAME AS REQUEST_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.REQUEST_TYPE_SEQ = P2.REQUEST_TYPE_SEQ
                        OR P1.REQUEST_NAME = P2.REQUEST_NAME
                    ORDER BY P1.REQUEST_TYPE_SEQ
                            ,P2.REQUEST_TYPE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '5': //ข้อมูลเรื่อง   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_EXP_MAS_SUB_JOB
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM EXP.TB_EXP_MAS_SUB_JOB 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.SUB_JOB_SEQ AS SUB_JOB_SEQ_P1 , P2.SUB_JOB_SEQ AS SUB_JOB_SEQ_P2
        --                        ,J1.JOB_GROUP_NAME AS JOB_GROUP_NAME_P1 , J2.JOB_GROUP_NAME AS JOB_GROUP_NAME_P2
                        ,P1.SUB_JOB_NAME AS SUB_JOB_NAME_P1 , P2.SUB_JOB_NAME AS SUB_JOB_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.SUB_JOB_SEQ = P2.SUB_JOB_SEQ
                        OR P1.SUB_JOB_NAME = P2.SUB_JOB_NAME
                    LEFT OUTER JOIN MGT1.TB_EXP_MAS_JOB_GROUP J1
                        ON P1.JOB_GROUP_SEQ = J1.JOB_GROUP_SEQ
                    LEFT OUTER JOIN EXP.TB_EXP_MAS_JOB_GROUP J2
                        ON P2.JOB_GROUP_SEQ = J2.JOB_GROUP_SEQ
                    ORDER BY P1.SUB_JOB_SEQ
                            ,P2.SUB_JOB_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '6': //ข้อมูลประเภทการขอใช้ประโยชน์   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_EXP_MAS_USE_TYPE
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM EXP.TB_EXP_MAS_USE_TYPE 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.USE_TYPE_SEQ AS USE_TYPE_SEQ_P1 , P2.USE_TYPE_SEQ AS USE_TYPE_SEQ_P2
                        ,P1.USE_TYPE_NAME AS USE_TYPE_NAME_P1 , P2.USE_TYPE_NAME AS USE_TYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.USE_TYPE_SEQ = P2.USE_TYPE_SEQ
                        OR P1.USE_TYPE_NAME = P2.USE_TYPE_NAME
                    ORDER BY P1.USE_TYPE_SEQ
                            ,P2.USE_TYPE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '7': //ข้อมูลสถานะงาน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_EXP_MAS_WORK_STS
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM EXP.TB_EXP_MAS_WORK_STS 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.WORK_STS_SEQ AS WORK_STS_SEQ_P1 , P2.WORK_STS_SEQ AS WORK_STS_SEQ_P2
                        ,P1.WORK_STS_NAME AS WORK_STS_NAME_P1 , P2.WORK_STS_NAME AS WORK_STS_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.WORK_STS_SEQ = P2.WORK_STS_SEQ
                    ORDER BY P1.WORK_STS_SEQ
                            ,P2.WORK_STS_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

    }
?>