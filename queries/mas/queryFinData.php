<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //การตั้งค่าในระบบ
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_FIN_MAS_CONFIG
                        WHERE RECORD_STATUS = 'N'
                        ),
                        P2 AS(
                        SELECT * 
                        FROM FIN.TB_FIN_MAS_CONFIG 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.CONFIG_NO AS CONFIG_NO_P1 , P2.CONFIG_NO AS CONFIG_NO_P2
                        ,P1.CONFIG_NAME AS CONFIG_NAME_P1 , P2.CONFIG_NAME AS CONFIG_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.CONFIG_NO = P2.CONFIG_NO
                    ORDER BY P1.CONFIG_NO, P2.CONFIG_NO";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ประเภทการขอเบิก    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_FIN_MAS_FUND_TYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM FIN.TB_FIN_MAS_FUND_TYPE 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.FUND_TYPE_ID AS FUND_TYPE_ID_P1 , P2.FUND_TYPE_ID AS FUND_TYPE_ID_P2
                        ,F1.FUND_TYPE_GROUP_NAME AS FUND_TYPE_GROUP_NAME_P1 , F2.FUND_TYPE_GROUP_NAME AS FUND_TYPE_GROUP_NAME_P2
                        ,P1.FUND_NAME AS FUND_NAME_P1 , P2.FUND_NAME AS FUND_NAME_P2
                        ,P1.FUND_DOC AS FUND_DOC_P1 , P2.FUND_DOC AS FUND_DOC_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.FUND_TYPE_ID = P2.FUND_TYPE_ID
                    LEFT OUTER JOIN MGT1.TB_FIN_MAS_FUND_TYPE_GROUP F1
                        ON P1.FUND_TYPE = F1.FUND_TYPE
                    LEFT OUTER JOIN FIN.TB_FIN_MAS_FUND_TYPE_GROUP F2
                        ON P2.FUND_TYPE = F2.FUND_TYPE
                    ORDER BY P1.FUND_TYPE_ID 
                            ,P2.FUND_TYPE_ID";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //หมวดหมู่เงิน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_FIN_MAS_INCOME
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM FIN.TB_FIN_MAS_INCOME 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.INCOME_ID AS INCOME_ID_P1 , P2.INCOME_ID AS INCOME_ID_P2
                        ,P1.INCOME_CODE AS INCOME_CODE_P1 , P2.INCOME_CODE AS INCOME_CODE_P2
                        ,P1.INCOME_NAME AS INCOME_NAME_P1 , P2.INCOME_NAME AS INCOME_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.INCOME_ID = P2.INCOME_ID
                    LEFT OUTER JOIN MGT1.TB_FIN_MAS_INCOME_GROUP F1
                        ON P1.INCOME_GROUP_ID = F1.INCOME_GROUP_ID
                    LEFT OUTER JOIN FIN.TB_FIN_MAS_INCOME_GROUP F2
                        ON P2.INCOME_GROUP_ID = F2.INCOME_GROUP_ID
                    ORDER BY P1.INCOME_ID
                            ,P2.INCOME_ID";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //รายได้   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_FIN_MAS_REV
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM FIN.TB_FIN_MAS_REV 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.REVENUE_ID AS REVENUE_ID_P1 , P2.REVENUE_ID AS REVENUE_ID_P2
                        ,P1.REVENUE_CODE AS REVENUE_CODE_P1 , P2.REVENUE_CODE AS REVENUE_CODE_P2
                        ,P1.REVENUE_NAME AS REVENUE_NAME_P1 , P2.REVENUE_NAME AS REVENUE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.REVENUE_ID = P2.REVENUE_ID
                    LEFT OUTER JOIN MGT1.TB_FIN_MAS_REV_GROUP F1
                        ON P1.REVENUE_GROUP_ID = F1.REVENUE_GROUP_ID
                    LEFT OUTER JOIN FIN.TB_FIN_MAS_REV_GROUP F2
                        ON P2.REVENUE_GROUP_ID = F2.REVENUE_GROUP_ID
                    ORDER BY P1.REVENUE_ID
                            ,P2.REVENUE_ID";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '5': //ชื่อประเภทการจัดเก็บ   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_FIN_MAS_STORE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM FIN.TB_FIN_MAS_STORE 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.STORE_ID AS STORE_ID_P1 , P2.STORE_ID AS STORE_ID_P2
                        ,P1.STORE_ABBR_NAME AS STORE_ABBR_NAME_P1 , P2.STORE_ABBR_NAME AS STORE_ABBR_NAME_P2
                        ,P1.STORE_NAME AS STORE_NAME_P1 , P2.STORE_NAME AS STORE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                        
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.STORE_ID = P2.STORE_ID
                    ORDER BY P1.STORE_ID,P2.STORE_ID";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

    }
?>