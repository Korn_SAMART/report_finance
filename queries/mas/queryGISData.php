<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ตารางตำแหน่งจังหวัด
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_GIS_MAS_PROVINCE
                    --    WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM GIS.TB_GIS_MAS_PROVINCE 
                    --    WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.PROVINCE_ID AS PROVINCE_ID_P1 , P2.PROVINCE_ID AS PROVINCE_ID_P2
                        ,P1.NAME_E AS NAME_E_P1 , P2.NAME_E AS NAME_E_P2
                        ,P1.NAME_T AS NAME_T_P1 , P2.NAME_T AS NAME_T_P2
                        ,P1.LONGTITUDE AS LONGTITUDE_P1 , P2.LONGTITUDE AS LONGTITUDE_P2
                        ,P1.LATTITUDE AS LATTITUDE_P1 , P2.LATTITUDE AS LATTITUDE_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PROVINCE_ID = P2.PROVINCE_ID
                    ORDER BY NVL(P1.PROVINCE_ID,P2.PROVINCE_ID)";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ตารางตำแหน่งอำเภอ    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_GIS_MAS_AMPHOE
                    --    WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM GIS.TB_GIS_MAS_AMPHOE 
                    --    WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.AMPHOE_ID AS AMPHOE_ID_P1 , P2.AMPHOE_ID AS AMPHOE_ID_P2
                        ,P1.PROVINCE_ID AS PROVINCE_ID_P1 , P2.PROVINCE_ID AS PROVINCE_ID_P2
                        ,G1.NAME_T AS NAME_T_P_P1 , G2.NAME_T AS NAME_T_P_P2
                        ,P1.NAME_E AS NAME_E_P1 , P2.NAME_E AS NAME_E_P2
                        ,P1.NAME_T AS NAME_T_P1 , P2.NAME_T AS NAME_T_P2
                        ,P1.LONGTITUDE AS LONGTITUDE_P1 , P2.LONGTITUDE AS LONGTITUDE_P2
                        ,P1.LATTITUDE AS LATTITUDE_P1 , P2.LATTITUDE AS LATTITUDE_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.AMPHOE_ID = P2.AMPHOE_ID
                    LEFT OUTER JOIN MGT1.TB_GIS_MAS_PROVINCE G1
                        ON P1.PROVINCE_ID = G1.PROVINCE_ID
                    LEFT OUTER JOIN GIS.TB_GIS_MAS_PROVINCE G2
                        ON P2.PROVINCE_ID = G2.PROVINCE_ID
                    ORDER BY P1.PROVINCE_ID, P1.AMPHOE_ID";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ตารางตำแหน่งตำบล   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_GIS_MAS_TAMBOL
                    --    WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM GIS.TB_GIS_MAS_TAMBOL 
                    --    WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.TAMBON_ID AS TAMBON_ID_P1 , P2.TAMBON_ID AS TAMBON_ID_P2
                        ,P1.PROVINCE_ID AS PROVINCE_ID_P1 , P2.PROVINCE_ID AS PROVINCE_ID_P2
                        ,G1.NAME_T AS NAME_T_P_P1 , G2.NAME_T AS NAME_T_P_P2
                        ,P1.AMPHOE_ID AS AMPHOE_ID_P1 , P2.AMPHOE_ID AS AMPHOE_ID_P2
                        ,A1.NAME_T AS NAME_T_A_P1 , A2.NAME_T AS NAME_T_A_P2                 
                        ,P1.NAME_E AS NAME_E_P1 , P2.NAME_E AS NAME_E_P2
                        ,P1.NAME_T AS NAME_T_P1 , P2.NAME_T AS NAME_T_P2
                        ,P1.LONGTITUDE AS LONGTITUDE_P1 , P2.LONGTITUDE AS LONGTITUDE_P2
                        ,P1.LATTITUDE AS LATTITUDE_P1 , P2.LATTITUDE AS LATTITUDE_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.TAMBON_ID = P2.TAMBON_ID
                    LEFT OUTER JOIN MGT1.TB_GIS_MAS_AMPHOE A1
                        ON P1.AMPHOE_ID = A1.AMPHOE_ID
                    LEFT OUTER JOIN GIS.TB_GIS_MAS_AMPHOE A2
                        ON P2.AMPHOE_ID = A2.AMPHOE_ID
                    LEFT OUTER JOIN MGT1.TB_GIS_MAS_PROVINCE G1
                        ON P1.PROVINCE_ID = G1.PROVINCE_ID
                    LEFT OUTER JOIN GIS.TB_GIS_MAS_PROVINCE G2
                        ON P2.PROVINCE_ID = G2.PROVINCE_ID
                    ORDER BY NVL(P1.PROVINCE_ID,P2.PROVINCE_ID), NVL(P1.AMPHOE_ID,P2.AMPHOE_ID), NVL(P1.TAMBON_ID,P2.TAMBON_ID)";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //ตารางตำแหน่งสำนักงานที่ดิน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_GIS_MAS_LANDOFFICE
                    --    WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM GIS.TB_GIS_MAS_LANDOFFICE 
                    --    WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.ID AS ID_P1 , P2.ID AS ID_P2
                        ,P1.ORG_CODE AS ORG_CODE_P1 , P2.ORG_CODE AS ORG_CODE_P2
                        ,P1.ORG_NAME AS ORG_NAME_P1 , P2.ORG_NAME AS ORG_NAME_P2
                        ,P1.ORG_SITE AS ORG_SITE_P1 , P2.ORG_SITE AS ORG_SITE_P2
                        ,P1.ORG_TEL AS ORG_TEL_P1 , P2.ORG_TEL AS ORG_TEL_P2
                        ,P1.ORG_FAX AS ORG_FAX_P1 , P2.ORG_FAX AS ORG_FAX_P2
                        ,P1.ORG_WEB AS ORG_WEB_P1 , P2.ORG_WEB AS ORG_WEB_P2
                        ,P1.ORG_MAIL AS ORG_MAIL_P1 , P2.ORG_MAIL AS ORG_MAIL_P2
                        ,P1.ORG_IMAGE AS ORG_IMAGE_P1 , P2.ORG_IMAGE AS ORG_IMAGE_P2
                        ,P1.ORG_NORTH AS ORG_NORTH_P1 , P2.ORG_NORTH AS ORG_NORTH_P2
                        ,P1.ORG_EAST AS ORG_EAST_P1 , P2.ORG_EAST AS ORG_EAST_P2
                        ,P1.ORG_LAT AS ORG_LAT_P1 , P2.ORG_LAT AS ORG_LAT_P2
                        ,P1.ORG_LONG AS ORG_LONG_P1 , P2.ORG_LONG AS ORG_LONG_P2
                        ,P1.ORG_ZONE AS ORG_ZONE_P1 , P2.ORG_ZONE AS ORG_ZONE_P2
                        ,P1.ORG_REMARKS AS ORG_REMARKS_P1 , P2.ORG_REMARKS AS ORG_REMARKS_P2
                        ,P1.ORG_CREATEDATE AS ORG_CREATEDATE_P1 , P2.ORG_CREATEDATE AS ORG_CREATEDATE_P2
                        ,P1.ORG_UPDATEDATE AS ORG_UPDATEDATE_P1 , P2.ORG_UPDATEDATE AS ORG_UPDATEDATE_P2
                        
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.ID = P2.ID
                    ORDER BY NVL(P1.ID,P2.ID)";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '5': //ตารางอ้างอิงข้อมูลพิกัด   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_GIS_MAS_CRS
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM GIS.TB_GIS_MAS_CRS 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.CRS_CODE AS CRS_CODE_P1 , P2.CRS_CODE AS CRS_CODE_P2
                        ,P1.CRS_NAME AS CRS_NAME_P1 , P2.CRS_NAME AS CRS_NAME_P2
        --                        ,CASE NVL(P1.CRS_TYPE,'99') 
        --                        WHEN '1' THEN 'Geocentric'
        --                        WHEN '2' THEN 'Geographic' 
        --                        WHEN '3' THEN 'Projection'
        --                        WHEN '4' THEN 'Vertical' 
        --                        WHEN '5' THEN 'Compound'
        --                        WHEN '6' THEN 'Engineering'   
        --                        END AS CRS_TYPE_P1
        --                        ,CASE NVL(P2.CRS_TYPE,'99') 
        --                        WHEN '1' THEN 'Geocentric'
        --                        WHEN '2' THEN 'Geographic' 
        --                        WHEN '3' THEN 'Projection'
        --                        WHEN '4' THEN 'Vertical' 
        --                        WHEN '5' THEN 'Compound'
        --                        WHEN '6' THEN 'Engineering'   
        --                        END AS CRS_TYPE_P2
        --                        ,P1.SOURCE_GEOGCRS_CODE AS SOURCE_GEOGCRS_CODE_P1 , P2.SOURCE_GEOGCRS_CODE AS SOURCE_GEOGCRS_CODE_P2
        --                        ,P1.COORD_OP_CODE AS COORD_OP_CODE_P1 , P2.COORD_OP_CODE AS COORD_OP_CODE_P2
        --                        ,P1.DATUM_CODE AS DATUM_CODE_P1 , P2.DATUM_CODE AS DATUM_CODE_P2
        --                        ,P1.CRS_REM AS CRS_REM_P1 , P2.CRS_REM AS CRS_REM_P2      
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.CRS_CODE = P2.CRS_CODE
                    ORDER BY TO_NUMBER(P1.CRS_CODE)
                            ,TO_NUMBER(P2.CRS_CODE)";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '6': //ตารางคำอธิบายข้อมูล   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_GIS_MAS_DEFINITION_TYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM GIS.TB_GIS_MAS_DEFINITION_TYPE 
                        WHERE RECORD_STATUS = 'N'
                        )
                    SELECT P1.DEFINIT_SEQ AS DEFINIT_SEQ_P1 , P2.DEFINIT_SEQ AS DEFINIT_SEQ_P2
                        ,P1.DEFINIT_CODE AS DEFINIT_CODE_P1 , P2.DEFINIT_CODE AS DEFINIT_CODE_P2
        --                        ,D1.GROUP_DEFINIT_TH AS GROUP_DEFINIT_TH_P1 , D2.GROUP_DEFINIT_TH AS GROUP_DEFINIT_TH_P2
                        ,P1.DEFENIT_NAME AS DEFENIT_NAME_P1 , P2.DEFENIT_NAME AS DEFENIT_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.DEFINIT_SEQ = P2.DEFINIT_SEQ
                    LEFT OUTER JOIN MGT1.TB_GIS_MAS_GROUP_DEFINITION D1
                        ON P1.GROUP_DEFINIT_CODE = D1.GROUP_DEFINIT_CODE
                    LEFt OUTER JOIN GIS.TB_GIS_MAS_GROUP_DEFINITION D2
                        ON P2.GROUP_DEFINIT_CODE = D2.GROUP_DEFINIT_CODE
                    ORDER BY P1.DEFINIT_SEQ
                            ,P2.DEFINIT_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '7': //ตารางกลุ่มสินค้า   
            $select = "WITH P1 AS(
                SELECT * 
                FROM MGT1.TB_GIS_MAS_PRODUCT_GROUP
                WHERE RECORD_STATUS = 'N' 
                ),
                P2 AS(
                SELECT * 
                FROM GIS.TB_GIS_MAS_PRODUCT_GROUP 
                WHERE RECORD_STATUS = 'N'
                )
            SELECT P1.PRODUCT_GROUP_SEQ AS PRODUCT_GROUP_SEQ_P1 , P2.PRODUCT_GROUP_SEQ AS PRODUCT_GROUP_SEQ_P2
                ,P1.GROUP_NAME AS GROUP_NAME_P1 , P2.GROUP_NAME AS GROUP_NAME_P2
                ,P1.PRODUCT_GROUP_TBNAME AS PRODUCT_GROUP_TBNAME_P1 , P2.PRODUCT_GROUP_TBNAME AS PRODUCT_GROUP_TBNAME_P2
--                        ,P1.FEE_MNY AS FEE_MNY_P1 , P2.FEE_MNY AS FEE_MNY_P2
--                        ,CASE P1.GROUP_TYPE_SEQ
--                        WHEN 1 THEN 'MANAGEMENT INFORMATION SYSTEM DATA'
--                        WHEN 2 THEN 'GEO GRAPHIC INFORMATION SYSTEM DATA' 
--                        WHEN 3 THEN 'PICTURE / AERIAL DATA'
--                        END AS GROUP_TYPE_NAME_P1
--                        ,CASE P2.GROUP_TYPE_SEQ
--                        WHEN 1 THEN 'MANAGEMENT INFORMATION SYSTEM DATA'
--                        WHEN 2 THEN 'GEO GRAPHIC INFORMATION SYSTEM DATA' 
--                        WHEN 3 THEN 'PICTURE / AERIAL DATA' 
--                        END AS GROUP_TYPE_NAME_P2
                ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2                     
            FROM P1
            FULL OUTER JOIN P2
                ON replace(replace(P1.GROUP_NAME,' '),'.') like replace(replace(P2.GROUP_NAME,' '),'.')||'%'
            ORDER BY P1.PRODUCT_GROUP_SEQ
                    ,P2.PRODUCT_GROUP_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

    }
?>