<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ข้อมูลชื่อประเทศ
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_COUNTRY
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_COUNTRY 
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.COUNTRY_SEQ AS COUNTRY_SEQ_P1 , P2.COUNTRY_SEQ AS COUNTRY_SEQ_P2
            //             ,P1.COUNTRY_ID AS COUNTRY_ID_P1 , P2.COUNTRY_ID AS COUNTRY_ID_P2
            //             ,P1.COUNTRY_NAME AS COUNTRY_NAME_P1 , P2.COUNTRY_NAME AS COUNTRY_NAME_P2
            //             ,P1.COUNTRY_NAME_EN AS COUNTRY_NAME_EN_P1 , P2.COUNTRY_NAME_EN AS COUNTRY_NAME_EN_P2
            //             ,P1.COUNTRY_GROUP_SEQ AS COUNTRY_GROUP_SEQ_P1 , P2.COUNTRY_GROUP_SEQ AS COUNTRY_GROUP_SEQ_P2
            //             ,G1.COUNTRY_GROUP_NAME AS COUNTRY_GROUP_NAME_P1 ,G2.COUNTRY_GROUP_NAME AS COUNTRY_GROUP_NAME_P2
            //             ,P1.RECORD_STATUS AS RECORD_STATUS_P1 , P2.RECORD_STATUS AS RECORD_STATUS_P2
            //             ,P1.CREATE_USER AS CREATE_USER_P1 , P2.CREATE_USER AS CREATE_USER_P2
            //             ,P1.CREATE_DTM AS CREATE_DTM_P1 , P2.CREATE_DTM AS CREATE_DTM_P2
            //             ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 , P2.LAST_UPD_USER AS LAST_UPD_USER_P2
            //             ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 , P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.COUNTRY_SEQ = P2.COUNTRY_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_COUNTRY_GROUP G1
            //             ON P1.COUNTRY_GROUP_SEQ = G1.COUNTRY_GROUP_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_COUNTRY_GROUP G2
            //             ON P2.COUNTRY_GROUP_SEQ = G2.COUNTRY_GROUP_SEQ
            //         ORDER BY P1.COUNTRY_GROUP_SEQ, P2.COUNTRY_GROUP_SEQ,P1.COUNTRY_NAME, P2.COUNTRY_NAME";
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_COUNTRY
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_COUNTRY 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.COUNTRY_SEQ AS COUNTRY_SEQ_P1 , P2.COUNTRY_SEQ AS COUNTRY_SEQ_P2
                        ,P1.COUNTRY_ID AS COUNTRY_ID_P1 , P2.COUNTRY_ID AS COUNTRY_ID_P2
                        ,P1.COUNTRY_NAME AS COUNTRY_NAME_P1 , P2.COUNTRY_NAME AS COUNTRY_NAME_P2
                        ,P1.COUNTRY_NAME_EN AS COUNTRY_NAME_EN_P1 , P2.COUNTRY_NAME_EN AS COUNTRY_NAME_EN_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2   
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.COUNTRY_SEQ = P2.COUNTRY_SEQ
                    ORDER BY P1.COUNTRY_SEQ, NLSSORT(P1.COUNTRY_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.COUNTRY_SEQ, NLSSORT(P2.COUNTRY_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ข้อมูลชื่อจังหวัด    
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_PROVINCE
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_PROVINCE 
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.PROVINCE_SEQ AS PROVINCE_SEQ_P1 , P2.PROVINCE_SEQ AS PROVINCE_SEQ_P2
            //             ,P1.PROVINCE_ID AS PROVINCE_ID_P1 , P2.PROVINCE_ID AS PROVINCE_ID_P2
            //             ,P1.PROVINCE_DPIS_ID AS PROVINCE_DPIS_ID_P1 , P2.PROVINCE_DPIS_ID AS PROVINCE_DPIS_ID_P2
            //             ,P1.PROVINCE_NAME AS PROVINCE_NAME_P1 , P2.PROVINCE_NAME AS PROVINCE_NAME_P2
            //             ,P1.PROVINCE_NAME_EN AS PROVINCE_NAME_EN_P1 , P2.PROVINCE_NAME_EN AS PROVINCE_NAME_EN_P2
            //             ,P1.PROVINCE_ABBR AS PROVINCE_ABBR_P1 , P2.PROVINCE_ABBR AS PROVINCE_ABBR_P2
            //             ,P1.REGION_SEQ AS REGION_SEQ_P1 , P2.REGION_SEQ AS REGION_SEQ_P2
            //             ,P1.COUNTRY_SEQ AS COUNTRY_SEQ_P1 , P2.COUNTRY_SEQ AS COUNTRY_SEQ_P2
            //             ,P1.RECORD_STATUS AS RECORD_STATUS_P1 , P2.RECORD_STATUS AS RECORD_STATUS_P2
            //             ,P1.CREATE_USER AS CREATE_USER_P1 , P2.CREATE_USER AS CREATE_USER_P2
            //             ,P1.CREATE_DTM AS CREATE_DTM_P1 , P2.CREATE_DTM AS CREATE_DTM_P2
            //             ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 , P2.LAST_UPD_USER AS LAST_UPD_USER_P2
            //             ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 , P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
            //             ,P1.PROVINCE_DOL_ID AS PROVINCE_DOL_ID_P1 , P2.PROVINCE_DOL_ID AS PROVINCE_DOL_ID_P2
            //             ,P1.PROVINCE_EVD_ID AS PROVINCE_EVD_ID_P1 , P2.PROVINCE_EVD_ID AS PROVINCE_EVD_ID_P2
            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.PROVINCE_SEQ = P2.PROVINCE_SEQ
            //         ORDER BY NVL(P1.PROVINCE_SEQ,P2.PROVINCE_SEQ)";
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_PROVINCE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_PROVINCE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.PROVINCE_SEQ AS PROVINCE_SEQ_P1 , P2.PROVINCE_SEQ AS PROVINCE_SEQ_P2
                        ,P1.PROVINCE_ID AS PROVINCE_ID_P1 , P2.PROVINCE_ID AS PROVINCE_ID_P2
                        ,P1.PROVINCE_NAME AS PROVINCE_NAME_P1 , P2.PROVINCE_NAME AS PROVINCE_NAME_P2
                        ,P1.PROVINCE_ABBR AS PROVINCE_ABBR_P1 , P2.PROVINCE_ABBR AS PROVINCE_ABBR_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2   
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PROVINCE_SEQ = P2.PROVINCE_SEQ
                    ORDER BY TO_NUMBER(P1.PROVINCE_SEQ), NLSSORT(P1.PROVINCE_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,TO_NUMBER(P1.PROVINCE_SEQ), NLSSORT(P2.PROVINCE_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ข้อมูลชื่ออำเภอ   
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_AMPHUR
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_AMPHUR 
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.AMPHUR_SEQ AS AMPHUR_SEQ_P1 , P2.AMPHUR_SEQ AS AMPHUR_SEQ_P2
            //             ,P1.AMPHUR_ID AS AMPHUR_ID_P1 , P2.AMPHUR_ID AS AMPHUR_ID_P2
            //             ,T1.PROVINCE_NAME AS PROVINCE_NAME_P1 , T2.PROVINCE_NAME AS PROVINCE_NAME_P2
            //             ,P1.AMPHUR_NAME AS AMPHUR_NAME_P1 , P2.AMPHUR_NAME AS AMPHUR_NAME_P2
            //             ,P1.AMPHUR_NAME_EN AS AMPHUR_NAME_EN_P1 , P2.AMPHUR_NAME_EN AS AMPHUR_NAME_EN_P2
            //             ,P1.PROVINCE_SEQ AS PROVINCE_SEQ_P1 , P2.PROVINCE_SEQ AS PROVINCE_SEQ_P2
            //             ,P1.RECORD_STATUS AS RECORD_STATUS_P1 , P2.RECORD_STATUS AS RECORD_STATUS_P2
            //             ,P1.CREATE_USER AS CREATE_USER_P1 , P2.CREATE_USER AS CREATE_USER_P2
            //             ,P1.CREATE_DTM AS CREATE_DTM_P1 , P2.CREATE_DTM AS CREATE_DTM_P2
            //             ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 , P2.LAST_UPD_USER AS LAST_UPD_USER_P2
            //             ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 , P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
            //             ,P1.AMPHUR_EVD_ID AS AMPHUR_EVD_ID_P1 , P2.AMPHUR_EVD_ID AS AMPHUR_EVD_ID_P2
            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.AMPHUR_SEQ = P2.AMPHUR_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_PROVINCE T1
            //             ON P1.PROVINCE_SEQ = T1.PROVINCE_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_PROVINCE T2
            //             ON P2.PROVINCE_SEQ = T2.PROVINCE_SEQ
            //         ORDER BY T1.PROVINCE_NAME, T2.PROVINCE_NAME, P1.AMPHUR_SEQ, P2.AMPHUR_SEQ";
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_AMPHUR
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_AMPHUR 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.AMPHUR_SEQ AS AMPHUR_SEQ_P1 , P2.AMPHUR_SEQ AS AMPHUR_SEQ_P2
                        ,P1.AMPHUR_ID AS AMPHUR_ID_P1 , P2.AMPHUR_ID AS AMPHUR_ID_P2
                        ,T1.PROVINCE_NAME AS PROVINCE_NAME_P1 , T2.PROVINCE_NAME AS PROVINCE_NAME_P2
                        ,P1.AMPHUR_NAME AS AMPHUR_NAME_P1 , P2.AMPHUR_NAME AS AMPHUR_NAME_P2
        --                        ,P1.AMPHUR_NAME_EN AS AMPHUR_NAME_EN_P1 , P2.AMPHUR_NAME_EN AS AMPHUR_NAME_EN_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2   
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.AMPHUR_SEQ = P2.AMPHUR_SEQ
                    LEFT OUTER JOIN MGT1.TB_MAS_PROVINCE T1
                        ON P1.PROVINCE_SEQ = T1.PROVINCE_SEQ
                        AND T1.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MAS.TB_MAS_PROVINCE T2
                        ON P2.PROVINCE_SEQ = T2.PROVINCE_SEQ
                        AND T2.RECORD_STATUS = 'N'
                    ORDER BY P1.AMPHUR_SEQ, NLSSORT(T1.PROVINCE_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P1.AMPHUR_ID, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.AMPHUR_SEQ, NLSSORT(T2.PROVINCE_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P2.AMPHUR_ID, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //ข้อมูลชื่อตำบล   
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_TAMBOL
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_TAMBOL 
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.TAMBOL_SEQ AS TAMBOL_SEQ_P1 , P2.TAMBOL_SEQ AS TAMBOL_SEQ_P2
            //             ,P1.TAMBOL_ID AS TAMBOL_ID_P1 , P2.TAMBOL_ID AS TAMBOL_ID_P2
            //             ,T1.PROVINCE_NAME AS PROVINCE_NAME_P1 , T2.PROVINCE_NAME AS PROVINCE_NAME_P2
            //             ,A1.AMPHUR_NAME AS AMPHUR_NAME_P1 , A2.AMPHUR_NAME AS AMPHUR_NAME_P2
            //             ,P1.TAMBOL_NAME AS TAMBOL_NAME_P1 , P2.TAMBOL_NAME AS TAMBOL_NAME_P2
            //             ,P1.TAMBOL_NAME_EN AS TAMBOL_NAME_EN_P1 , P2.TAMBOL_NAME_EN AS TAMBOL_NAME_EN_P2
            //             ,P1.AMPHUR_SEQ AS AMPHUR_SEQ_P1 , P2.AMPHUR_SEQ AS AMPHUR_SEQ_P2
            //             ,P1.RECORD_STATUS AS RECORD_STATUS_P1 , P2.RECORD_STATUS AS RECORD_STATUS_P2
            //             ,P1.CREATE_USER AS CREATE_USER_P1 , P2.CREATE_USER AS CREATE_USER_P2
            //             ,P1.CREATE_DTM AS CREATE_DTM_P1 , P2.CREATE_DTM AS CREATE_DTM_P2
            //             ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 , P2.LAST_UPD_USER AS LAST_UPD_USER_P2
            //             ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 , P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
            //             ,P1.ZIP_CODE AS ZIP_CODE_P1 , P2.ZIP_CODE AS ZIP_CODE_P2
            //             ,P1.TAMBOL_EVD_ID AS TAMBOL_EVD_ID_P1 , P2.TAMBOL_EVD_ID AS TAMBOL_EVD_ID_P2
            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.TAMBOL_SEQ = P2.TAMBOL_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_AMPHUR A1
            //             ON A1.AMPHUR_SEQ = P1.AMPHUR_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_AMPHUR A2
            //             ON A2.AMPHUR_SEQ = P2.AMPHUR_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_PROVINCE T1
            //             ON A1.PROVINCE_SEQ = T1.PROVINCE_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_PROVINCE T2
            //             ON A2.PROVINCE_SEQ = T2.PROVINCE_SEQ
            //         ORDER BY T1.PROVINCE_NAME, T2.PROVINCE_NAME, A1.AMPHUR_NAME, A2.AMPHUR_NAME, P1.TAMBOL_NAME, P2.TAMBOL_NAME";
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_TAMBOL
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_TAMBOL 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.TAMBOL_SEQ AS TAMBOL_SEQ_P1 , P2.TAMBOL_SEQ AS TAMBOL_SEQ_P2
                        ,P1.TAMBOL_ID AS TAMBOL_ID_P1 , P2.TAMBOL_ID AS TAMBOL_ID_P2
                        ,T1.PROVINCE_NAME AS PROVINCE_NAME_P1 , T2.PROVINCE_NAME AS PROVINCE_NAME_P2
                        ,A1.AMPHUR_NAME AS AMPHUR_NAME_P1 , A2.AMPHUR_NAME AS AMPHUR_NAME_P2
                        ,P1.TAMBOL_NAME AS TAMBOL_NAME_P1 , P2.TAMBOL_NAME AS TAMBOL_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2   
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.TAMBOL_SEQ = P2.TAMBOL_SEQ
                    LEFT OUTER JOIN MGT1.TB_MAS_AMPHUR A1
                        ON A1.AMPHUR_SEQ = P1.AMPHUR_SEQ
                        AND A1.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MAS.TB_MAS_AMPHUR A2
                        ON A2.AMPHUR_SEQ = P2.AMPHUR_SEQ
                        AND A2.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MGT1.TB_MAS_PROVINCE T1
                        ON A1.PROVINCE_SEQ = T1.PROVINCE_SEQ
                        AND T1.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MAS.TB_MAS_PROVINCE T2
                        ON A2.PROVINCE_SEQ = T2.PROVINCE_SEQ
                        AND T2.RECORD_STATUS = 'N'
                    ORDER BY P1.TAMBOL_SEQ, NLSSORT(T1.PROVINCE_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(A1.AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P1.TAMBOL_ID, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.TAMBOL_SEQ, NLSSORT(T2.PROVINCE_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(A2.AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P2.TAMBOL_ID, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '5': //ข้อมูลแสดงประเภทหน่วยงาน   
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_LANDOFFICE
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_LANDOFFICE 
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 ,  P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
            //             ,P1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 ,  P2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
            //             ,P1.LANDOFFICE_TYPE_SEQ AS LANDOFFICE_TYPE_SEQ_P1 ,  P2.LANDOFFICE_TYPE_SEQ AS LANDOFFICE_TYPE_SEQ_P2
            //             ,T1.LANDOFFICE_TYPE_NAME AS LANDOFFICE_TYPE_NAME_P1 , T2.LANDOFFICE_TYPE_NAME AS LANDOFFICE_TYPE_NAME_P2

            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE_TYPE T1
            //             ON P1.LANDOFFICE_TYPE_SEQ = T1.LANDOFFICE_TYPE_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE_TYPE T2
            //             ON P2.LANDOFFICE_TYPE_SEQ = T2.LANDOFFICE_TYPE_SEQ
            //         ORDER BY P1.LANDOFFICE_NAME_TH, P2.LANDOFFICE_NAME_TH";
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_LANDOFFICE_TYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_LANDOFFICE_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.LANDOFFICE_TYPE_SEQ AS LANDOFFICE_TYPE_SEQ_P1 , P2.LANDOFFICE_TYPE_SEQ AS LANDOFFICE_TYPE_SEQ_P2
                        ,P1.LANDOFFICE_TYPE_ID AS LANDOFFICE_TYPE_ID_P1 , P2.LANDOFFICE_TYPE_ID AS LANDOFFICE_TYPE_ID_P2
                        ,P1.LANDOFFICE_TYPE_NAME AS LANDOFFICE_TYPE_NAME_P1 , P2.LANDOFFICE_TYPE_NAME AS LANDOFFICE_TYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2   
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDOFFICE_TYPE_SEQ = P2.LANDOFFICE_TYPE_SEQ
                    ORDER BY P1.LANDOFFICE_TYPE_SEQ, NLSSORT(P1.LANDOFFICE_TYPE_ID, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P1.LANDOFFICE_TYPE_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.LANDOFFICE_TYPE_SEQ, NLSSORT(P2.LANDOFFICE_TYPE_ID, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P2.LANDOFFICE_TYPE_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '52': //ข้อมูลแสดงประเภทหน่วยงาน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_LANDOFFICE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 ,  P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
                        ,P1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 ,  P2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2   

                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE_TYPE T1
                        ON P1.LANDOFFICE_TYPE_SEQ = T1.LANDOFFICE_TYPE_SEQ
                    LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE_TYPE T2
                        ON P2.LANDOFFICE_TYPE_SEQ = T2.LANDOFFICE_TYPE_SEQ
                    ORDER BY P1.LANDOFFICE_SEQ, P2.LANDOFFICE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '6': //ข้อมูลสถาบันการเงิน
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_FINANCE
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_FINANCE 
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.FINANCE_SEQ AS FINANCE_SEQ_P1 ,  P2.FINANCE_SEQ AS FINANCE_SEQ_P2
            //             ,P1.FINANCE_THAI_NAME AS FINANCE_THAI_NAME_P1 ,  P2.FINANCE_THAI_NAME AS FINANCE_THAI_NAME_P2
            //             ,P1.FINANCE_TYPE_SEQ AS FINANCE_TYPE_SEQ_P1 ,  P2.FINANCE_TYPE_SEQ AS FINANCE_TYPE_SEQ_P2
            //             ,T1.FINANCE_TYPE_NAME AS FINANCE_TYPE_NAME_P1 , T2.FINANCE_TYPE_NAME AS FINANCE_TYPE_NAME_P2

            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.FINANCE_SEQ = P2.FINANCE_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_FINANCE_TYPE T1
            //             ON P1.FINANCE_TYPE_SEQ = T1.FINANCE_TYPE_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_FINANCE_TYPE T2
            //             ON P2.FINANCE_TYPE_SEQ = T2.FINANCE_TYPE_SEQ
            //         ORDER BY P1.FINANCE_THAI_NAME, P2.FINANCE_THAI_NAME";
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_FINANCE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_FINANCE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.FINANCE_SEQ AS FINANCE_SEQ_P1 ,  P2.FINANCE_SEQ AS FINANCE_SEQ_P2
                        ,P1.FINANCE_ID AS FINANCE_ID_P1 ,  P2.FINANCE_ID AS FINANCE_ID_P2   
                        ,P1.FINANCE_THAI_NAME AS FINANCE_THAI_NAME_P1 ,  P2.FINANCE_THAI_NAME AS FINANCE_THAI_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2  
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.FINANCE_SEQ = P2.FINANCE_SEQ
                    ORDER BY P1.FINANCE_SEQ, NLSSORT(P1.FINANCE_THAI_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.FINANCE_SEQ, NLSSORT(P2.FINANCE_THAI_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '7': //การใช้ประโยชน์ในที่ดิน    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_LANDUSED
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_LANDUSED 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.LANDUSED_SEQ AS LANDUSED_SEQ_P1 ,  P2.LANDUSED_SEQ AS LANDUSED_SEQ_P2
                        ,P1.LANDUSED_NAME AS LANDUSED_NAME_P1 ,  P2.LANDUSED_NAME AS LANDUSED_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2  
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDUSED_SEQ = P2.LANDUSED_SEQ
                    ORDER BY P1.LANDUSED_SEQ
                            ,P2.LANDUSED_SEQ";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '8': //ข้อมูล อปท.   
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_OPT
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_OPT 
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.OPT_SEQ AS OPT_SEQ_P1 ,  P2.OPT_SEQ AS OPT_SEQ_P2
            //             ,P1.OPT_NAME AS OPT_NAME_P1 ,  P2.OPT_NAME AS OPT_NAME_P2
            //             ,T1.OPT_TYPE_NAME AS OPT_TYPE_NAME_P1 , T2.OPT_TYPE_NAME AS OPT_TYPE_NAME_P2
            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.OPT_SEQ = P2.OPT_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_OPT_TYPE T1
            //             ON P1.OPT_TYPE_SEQ = T1.OPT_TYPE_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_OPT_TYPE T2
            //             ON P2.OPT_TYPE_SEQ = T2.OPT_TYPE_SEQ
            //         ORDER BY T1.OPT_TYPE_NAME, T2.OPT_TYPE_NAME, P1.OPT_NAME, P2.OPT_NAME";
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_OPT 
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_OPT 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.OPT_SEQ AS OPT_SEQ_P1 , P2.OPT_SEQ AS OPT_SEQ_P2
                        ,P1.OPT_NAME AS OPT_NAME_P1 , P2.OPT_NAME AS OPT_NAME_P2  
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.OPT_SEQ = P2.OPT_SEQ
                    ORDER BY P1.OPT_SEQ 
                            ,P2.OPT_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '9': //ประเภทเอกสารสิทธิ์   
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_REG_MAS_PRINTPLATE_TYPE
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_PRINTPLATE_TYPE 
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.PRINTPLATE_TYPE_SEQ AS PRINTPLATE_TYPE_SEQ_P1 ,  P2.PRINTPLATE_TYPE_SEQ AS PRINTPLATE_TYPE_SEQ_P2
            //             ,P1.PRINTPLATE_TYPE_ID AS PRINTPLATE_TYPE_ID_P1 ,  P2.PRINTPLATE_TYPE_ID AS PRINTPLATE_TYPE_ID_P2
            //             ,P1.PRINTPLATE_TYPE_ABBR AS PRINTPLATE_TYPE_ABBR_P1 ,  P2.PRINTPLATE_TYPE_ABBR AS PRINTPLATE_TYPE_ABBR_P2
            //             ,P1.PRINTPLATE_TYPE_NAME AS PRINTPLATE_TYPE_NAME_P1 ,  P2.PRINTPLATE_TYPE_NAME AS PRINTPLATE_TYPE_NAME_P2
            //             ,P1.PRINTPLATE_TYPE_PLATE AS PRINTPLATE_TYPE_PLATE_P1 ,  P2.PRINTPLATE_TYPE_PLATE AS PRINTPLATE_TYPE_PLATE_P2
            //             ,P1.PRINTPLATE_TYPE_SUFFIX AS PRINTPLATE_TYPE_SUFFIX_P1 ,  P2.PRINTPLATE_TYPE_SUFFIX AS PRINTPLATE_TYPE_SUFFIX_P2
            //             ,P1.PRINTPLATE_TYPE_FLAG AS PRINTPLATE_TYPE_FLAG_P1 ,  P2.PRINTPLATE_TYPE_FLAG AS PRINTPLATE_TYPE_FLAG_P2
            //             ,P1.RECORD_STATUS AS RECORD_STATUS_P1 ,  P2.RECORD_STATUS AS RECORD_STATUS_P2
            //             ,P1.CREATE_USER AS CREATE_USER_P1 ,  P2.CREATE_USER AS CREATE_USER_P2
            //             ,P1.CREATE_DTM AS CREATE_DTM_P1 ,  P2.CREATE_DTM AS CREATE_DTM_P2
            //             ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 ,  P2.LAST_UPD_USER AS LAST_UPD_USER_P2
            //             ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 ,  P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
            //             ,P1.PLATE_SEQ AS PLATE_SEQ_P1 ,  P2.PLATE_SEQ AS PLATE_SEQ_P2
            //             ,P1.PRINTPLATE_TYPE_GROUP AS PRINTPLATE_TYPE_GROUP_P1 ,  P2.PRINTPLATE_TYPE_GROUP AS PRINTPLATE_TYPE_GROUP_P2
            //             ,P1.REG_USE_FLAG AS REG_USE_FLAG_P1 ,  P2.REG_USE_FLAG AS REG_USE_FLAG_P2
            //             ,P1.PRINTPLATE_TYPE_ORDER AS PRINTPLATE_TYPE_ORDER_P1 ,  P2.PRINTPLATE_TYPE_ORDER AS PRINTPLATE_TYPE_ORDER_P2
            //             ,P1.L_SEQ AS L_SEQ_P1 ,  P2.L_SEQ AS L_SEQ_P2
            //             ,P2.SVO_USE_FLAG AS SVO_USE_FLAG_P2
            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.PRINTPLATE_TYPE_SEQ = P2.PRINTPLATE_TYPE_SEQ
            //         ORDER BY P1.PRINTPLATE_TYPE_SEQ";
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_REG_MAS_PRINTPLATE_TYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_PRINTPLATE_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.PRINTPLATE_TYPE_SEQ AS PRINTPLATE_TYPE_SEQ_P1 ,  P2.PRINTPLATE_TYPE_SEQ AS PRINTPLATE_TYPE_SEQ_P2
                        ,P1.PRINTPLATE_TYPE_ID AS PRINTPLATE_TYPE_ID_P1 ,  P2.PRINTPLATE_TYPE_ID AS PRINTPLATE_TYPE_ID_P2
                        ,P1.PRINTPLATE_TYPE_ABBR AS PRINTPLATE_TYPE_ABBR_P1 ,  P2.PRINTPLATE_TYPE_ABBR AS PRINTPLATE_TYPE_ABBR_P2
                        ,P1.PRINTPLATE_TYPE_NAME AS PRINTPLATE_TYPE_NAME_P1 ,  P2.PRINTPLATE_TYPE_NAME AS PRINTPLATE_TYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PRINTPLATE_TYPE_SEQ = P2.PRINTPLATE_TYPE_SEQ
                    ORDER BY P1.PRINTPLATE_TYPE_SEQ
                            ,P2.PRINTPLATE_TYPE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '10': //คำนำหน้าชื่อ   
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_TITLE
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_TITLE 
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.TITLE_SEQ AS TITLE_SEQ_P1 ,  P2.TITLE_SEQ AS TITLE_SEQ_P2
            //             ,P1.TITLE_TYPE AS TITLE_TYPE_P1 ,  P2.TITLE_TYPE AS TITLE_TYPE_P2
            //             ,CASE P1.TITLE_TYPE
            //             WHEN 1 THEN 'บุคคลธรรมดา'
            //             WHEN 2 THEN 'นิติบุคคล'
            //             END AS TITLE_TYPE_P1
            //             ,CASE P2.TITLE_TYPE
            //             WHEN 1 THEN 'บุคคลธรรมดา'
            //             WHEN 2 THEN 'นิติบุคคล'
            //             END AS TITLE_TYPE_P2
            //             ,P1.TITLE_ABBR AS TITLE_ABBR_P1 ,  P2.TITLE_ABBR AS TITLE_ABBR_P2
            //             ,P1.TITLE_NAME AS TITLE_NAME_P1 ,  P2.TITLE_NAME AS TITLE_NAME_P2
            //             ,T1.TITLE_GROUP_NAME AS TITLE_GROUP_NAME_P1 , T2.TITLE_GROUP_NAME AS TITLE_GROUP_NAME_P2
            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.TITLE_SEQ = P2.TITLE_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_TITLE_GROUP T1
            //             ON P1.TITLE_GROUP_SEQ = T1.TITLE_GROUP_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_TITLE_GROUP T2
            //             ON P2.TITLE_GROUP_SEQ = T2.TITLE_GROUP_SEQ
            //         ORDER BY NVL(P1.TITLE_SEQ,P2.TITLE_SEQ)";
                $select = "WITH P1 AS(
                            SELECT * 
                            FROM MGT1.TB_MAS_TITLE
                            WHERE RECORD_STATUS = 'N' 
                            ),
                            P2 AS(
                            SELECT * 
                            FROM MAS.TB_MAS_TITLE 
                            WHERE RECORD_STATUS = 'N' 
                            )
                        SELECT P1.TITLE_SEQ AS TITLE_SEQ_P1 ,  P2.TITLE_SEQ AS TITLE_SEQ_P2
                            ,CASE P1.TITLE_TYPE
                            WHEN 1 THEN 'บุคคลธรรมดา'
                            WHEN 2 THEN 'นิติบุคคล'
                            END AS TITLE_TYPE_P1
                            ,CASE P2.TITLE_TYPE
                            WHEN 1 THEN 'บุคคลธรรมดา'
                            WHEN 2 THEN 'นิติบุคคล'
                            END AS TITLE_TYPE_P2
                            ,P1.TITLE_ABBR AS TITLE_ABBR_P1 ,  P2.TITLE_ABBR AS TITLE_ABBR_P2
                            ,P1.TITLE_NAME AS TITLE_NAME_P1 ,  P2.TITLE_NAME AS TITLE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                        FROM P1
                        FULL OUTER JOIN P2
                            ON P1.TITLE_SEQ = P2.TITLE_SEQ
                        ORDER BY TO_NUMBER(P1.TITLE_SEQ)
                                ,TO_NUMBER(P2.TITLE_SEQ)";
                            
                    $stid = oci_parse($conn, $select); 
                    oci_execute($stid);
        break;

        case '11': //ข้อมูลชื่อศาล
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_COURT
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_COURT 
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.COURT_SEQ AS COURT_SEQ_P1 ,  P2.COURT_SEQ AS COURT_SEQ_P2
            //             ,TT1.PROVINCE_NAME AS PROVINCE_NAME_P1 , TT2.PROVINCE_NAME AS PROVINCE_NAME_P2
            //             ,T1.COURT_TYPE_NAME AS COURT_TYPE_NAME_P1 , T2.COURT_TYPE_NAME AS COURT_TYPE_NAME_P2
            //             ,P1.COURT_NAME AS COURT_NAME_P1 ,  P2.COURT_NAME AS COURT_NAME_P2
            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.COURT_SEQ = P2.COURT_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_COURT_TYPE T1
            //             ON P1.COURT_TYPE_SEQ = T1.COURT_TYPE_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_COURT_TYPE T2
            //             ON P2.COURT_TYPE_SEQ = T2.COURT_TYPE_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_PROVINCE TT1
            //             ON P1.PROVINCE_SEQ = TT1.PROVINCE_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_PROVINCE TT2
            //             ON P2.PROVINCE_SEQ = TT2.PROVINCE_SEQ
            //         ORDER BY TT1.PROVINCE_NAME,TT2.PROVINCE_NAME,T1.COURT_TYPE_NAME,T2.COURT_TYPE_NAME,P1.COURT_NAME,P2.COURT_NAME";

            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_COURT
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_COURT 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.COURT_SEQ AS COURT_SEQ_P1 ,  P2.COURT_SEQ AS COURT_SEQ_P2
                        ,P1.COURT_NAME AS COURT_NAME_P1 ,  P2.COURT_NAME AS COURT_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.COURT_SEQ = P2.COURT_SEQ
                    ORDER BY TO_NUMBER(P1.COURT_SEQ), NLSSORT(P1.COURT_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,TO_NUMBER(P2.COURT_SEQ), NLSSORT(P2.COURT_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '12': //ข้อมูลพื้นที่รับผิดชอบของสำนักงานที่ดิน    
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_LANDOFFICE_AREA
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_LANDOFFICE_AREA
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.LANDOFFICE_AREA_SEQ AS LANDOFFICE_AREA_SEQ_P1 ,  P2.LANDOFFICE_AREA_SEQ AS LANDOFFICE_AREA_SEQ_P2
            //             ,T1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 , T2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
            //             ,TT1.AMPHUR_NAME AS AMPHUR_NAME_P1 , TT2.AMPHUR_NAME AS AMPHUR_NAME_P2
            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.LANDOFFICE_AREA_SEQ = P2.LANDOFFICE_AREA_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE T1
            //             ON P1.LANDOFFICE_SEQ = T1.LANDOFFICE_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE T2
            //             ON P2.LANDOFFICE_SEQ = T2.LANDOFFICE_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_AMPHUR TT1
            //             ON P1.AMPHUR_SEQ = TT1.AMPHUR_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_AMPHUR TT2
            //             ON P2.AMPHUR_SEQ = TT2.AMPHUR_SEQ
            //         ORDER BY T1.LANDOFFICE_NAME_TH, TT1.AMPHUR_NAME";
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_LANDOFFICE_AREA
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_LANDOFFICE_AREA
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.LANDOFFICE_AREA_SEQ AS LANDOFFICE_AREA_SEQ_P1 ,  P2.LANDOFFICE_AREA_SEQ AS LANDOFFICE_AREA_SEQ_P2
                        ,T1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 , T2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
                        ,TT1.AMPHUR_NAME AS AMPHUR_NAME_P1 , TT2.AMPHUR_NAME AS AMPHUR_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDOFFICE_AREA_SEQ = P2.LANDOFFICE_AREA_SEQ
                    LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE T1
                        ON P1.LANDOFFICE_SEQ = T1.LANDOFFICE_SEQ
                        AND T1.RECORD_STATUS = 'N' 
                    LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE T2
                        ON P2.LANDOFFICE_SEQ = T2.LANDOFFICE_SEQ
                        AND T2.RECORD_STATUS = 'N' 
                    LEFT OUTER JOIN MGT1.TB_MAS_AMPHUR TT1
                        ON P1.AMPHUR_SEQ = TT1.AMPHUR_SEQ
                        AND TT1.RECORD_STATUS = 'N' 
                    LEFT OUTER JOIN MAS.TB_MAS_AMPHUR TT2
                        ON P2.AMPHUR_SEQ = TT2.AMPHUR_SEQ
                        AND TT2.RECORD_STATUS = 'N' 
                    ORDER BY NLSSORT(T1.LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TT1.AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,NLSSORT(T2.LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TT2.AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '13': //ข้อมูลประเภท อปท.     
            // $select = "WITH P1 AS(
            //             SELECT * 
            //             FROM MGT1.TB_MAS_OPT_AREA
            //             WHERE RECORD_STATUS = 'N' 
            //             ),
            //             P2 AS(
            //             SELECT * 
            //             FROM MAS.TB_MAS_OPT_AREA
            //             WHERE RECORD_STATUS = 'N' 
            //             )
            //         SELECT P1.OPT_AREA_SEQ AS OPT_AREA_SEQ_P1 ,  P2.OPT_AREA_SEQ AS OPT_AREA_SEQ_P2
            //             ,T1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 , T2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
            //             ,TTT1.OPT_TYPE_NAME AS OPT_TYPE_NAME_P1 , TTT2.OPT_TYPE_NAME AS OPT_TYPE_NAME_P2    
            //             ,TT1.OPT_NAME AS OPT_NAME_P1 , TT2.OPT_NAME AS OPT_NAME_P2     
            //         FROM P1
            //         FULL OUTER JOIN P2
            //             ON P1.OPT_AREA_SEQ = P2.OPT_AREA_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE T1
            //             ON P1.LANDOFFICE_SEQ = T1.LANDOFFICE_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE T2
            //             ON P2.LANDOFFICE_SEQ = T2.LANDOFFICE_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_OPT TT1
            //             ON P1.OPT_SEQ = TT1.OPT_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_OPT TT2
            //             ON P2.OPT_SEQ = TT2.OPT_SEQ
            //         LEFT OUTER JOIN MGT1.TB_MAS_OPT_TYPE TTT1
            //             ON TT1.OPT_TYPE_SEQ = TTT1.OPT_TYPE_SEQ
            //         LEFT OUTER JOIN MAS.TB_MAS_OPT_TYPE TTT2
            //             ON TT2.OPT_TYPE_SEQ = TTT2.OPT_TYPE_SEQ
            //         ORDER BY T1.LANDOFFICE_NAME_TH , T2.LANDOFFICE_NAME_TH";
            
            $select ="WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_OPT_TYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_OPT_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.OPT_TYPE_SEQ AS OPT_TYPE_SEQ_P1 , P2.OPT_TYPE_SEQ AS OPT_TYPE_SEQ_P2
                        ,P1.OPT_TYPE_NAME AS OPT_TYPE_NAME_P1 , P2.OPT_TYPE_NAME AS OPT_TYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.OPT_TYPE_SEQ = P2.OPT_TYPE_SEQ
                    ORDER BY NLSSORT(P1.OPT_TYPE_SEQ, 'NLS_SORT=THAI_DICTIONARY'),
                            NLSSORT(P1.OPT_TYPE_NAME, 'NLS_SORT=THAI_DICTIONARY'),
                            NLSSORT(P2.OPT_TYPE_SEQ, 'NLS_SORT=THAI_DICTIONARY'), 
                            NLSSORT(P2.OPT_TYPE_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '132': //ข้อมูลการแบ่งองค์กรท้องถิ่นกับสำนักงานที่ดิน 
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_OPT_AREA
                        WHERE RECORD_STATUS = 'N'  --591
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_OPT_AREA
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.OPT_AREA_SEQ AS OPT_AREA_SEQ_P1 ,  P2.OPT_AREA_SEQ AS OPT_AREA_SEQ_P2
        --                        ,TTT1.OPT_TYPE_NAME AS OPT_TYPE_NAME_P1 , TTT2.OPT_TYPE_NAME AS OPT_TYPE_NAME_P2    
                        ,T1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 , T2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
                        ,TT1.OPT_NAME AS OPT_NAME_P1 , TT2.OPT_NAME AS OPT_NAME_P2  
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.OPT_AREA_SEQ = P2.OPT_AREA_SEQ
                    LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE T1
                        ON P1.LANDOFFICE_SEQ = T1.LANDOFFICE_SEQ
                        AND T1.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE T2
                        ON P2.LANDOFFICE_SEQ = T2.LANDOFFICE_SEQ
                        AND T2.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MGT1.TB_MAS_OPT TT1
                        ON P1.OPT_SEQ = TT1.OPT_SEQ
                        AND TT1.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MAS.TB_MAS_OPT TT2
                        ON P2.OPT_SEQ = TT2.OPT_SEQ
                        AND TT2.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MGT1.TB_MAS_OPT_TYPE TTT1
                        ON TT1.OPT_TYPE_SEQ = TTT1.OPT_TYPE_SEQ
                        AND TTT1.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MAS.TB_MAS_OPT_TYPE TTT2
                        ON TT2.OPT_TYPE_SEQ = TTT2.OPT_TYPE_SEQ
                        AND TTT2.RECORD_STATUS = 'N'
                    ORDER BY NLSSORT(T1.LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TT1.OPT_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,NLSSORT(T2.LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TT2.OPT_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '14': //ตารางข้อมูลหมวดสถาบันการเงิน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_FINANCE_TYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_FINANCE_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.FINANCE_TYPE_SEQ AS FINANCE_TYPE_SEQ_P1 ,  P2.FINANCE_TYPE_SEQ AS FINANCE_TYPE_SEQ_P2
                        ,P1.FINANCE_TYPE_NAME AS FINANCE_TYPE_NAME_P1 ,  P2.FINANCE_TYPE_NAME AS FINANCE_TYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.FINANCE_TYPE_SEQ = P2.FINANCE_TYPE_SEQ
                    ORDER BY P1.FINANCE_TYPE_SEQ, P2.FINANCE_TYPE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '15': //ตารางข้อมูลประเภทศาล
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_COURT_TYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_COURT_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.COURT_TYPE_SEQ AS COURT_TYPE_SEQ_P1 ,  P2.COURT_TYPE_SEQ AS COURT_TYPE_SEQ_P2
                        ,P1.COURT_TYPE_ID AS COURT_TYPE_ID_P1 ,  P2.COURT_TYPE_ID AS COURT_TYPE_ID_P2   
                        ,P1.COURT_TYPE_NAME AS COURT_TYPE_NAME_P1 ,  P2.COURT_TYPE_NAME AS COURT_TYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.COURT_TYPE_SEQ = P2.COURT_TYPE_SEQ
                    ORDER BY P1.COURT_TYPE_SEQ
                            ,P2.COURT_TYPE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '16': //ตารางข้อมูลสัญชาติ
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_NATIONALITY
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_NATIONALITY 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.NATIONALITY_SEQ AS NATIONALITY_SEQ_P1 ,  P2.NATIONALITY_SEQ AS NATIONALITY_SEQ_P2
                        ,P1.NATIONALITY_ID AS NATIONALITY_ID_P1 ,  P2.NATIONALITY_ID AS NATIONALITY_ID_P2
                        ,P1.NATIONALITY_NAME AS NATIONALITY_NAME_P1 ,  P2.NATIONALITY_NAME AS NATIONALITY_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.NATIONALITY_SEQ = P2.NATIONALITY_SEQ
                    ORDER BY P1.NATIONALITY_SEQ, TO_NUMBER(P1.NATIONALITY_ID), NLSSORT(P1.NATIONALITY_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.NATIONALITY_SEQ, TO_NUMBER(P2.NATIONALITY_ID), NLSSORT(P2.NATIONALITY_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '17': //ตารางข้อมูลเชื้อชาติ
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_RACE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_RACE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.RACE_SEQ AS RACE_SEQ_P1 ,  P2.RACE_SEQ AS RACE_SEQ_P2
                        ,P1.RACE_ID AS RACE_ID_P1 ,  P2.RACE_ID AS RACE_ID_P2
                        ,P1.RACE_NAME AS RACE_NAME_P1 ,  P2.RACE_NAME AS RACE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.RACE_SEQ = P2.RACE_SEQ
                    ORDER BY P1.RACE_SEQ, TO_NUMBER(P1.RACE_ID), NLSSORT(P1.RACE_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P1.RACE_SEQ, TO_NUMBER(P2.RACE_ID), NLSSORT(P2.RACE_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '18': //ตารางข้อมูลศาสนา
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_RELIGION
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_RELIGION 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.RELIGION_SEQ AS RELIGION_SEQ_P1 ,  P2.RELIGION_SEQ AS RELIGION_SEQ_P2
                        ,P1.RELIGION_ID AS RELIGION_ID_P1 ,  P2.RELIGION_ID AS RELIGION_ID_P2
                        ,P1.RELIGION_NAME AS RELIGION_NAME_P1 ,  P2.RELIGION_NAME AS RELIGION_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.RELIGION_SEQ = P2.RELIGION_SEQ
                    ORDER BY P1.RELIGION_SEQ, TO_NUMBER(P1.RELIGION_ID), NLSSORT(P1.RELIGION_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.RELIGION_SEQ, TO_NUMBER(P2.RELIGION_ID), NLSSORT(P2.RELIGION_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '19': //ตารางข้อมูลหน่วยนับ
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_UNIT
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_UNIT 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.UNIT_SEQ AS UNIT_SEQ_P1 ,  P2.UNIT_SEQ AS UNIT_SEQ_P2
                        ,P1.UNIT_NAME AS UNIT_NAME_P1 ,  P2.UNIT_NAME AS UNIT_NAME_P2
                        ,P1.UNIT_ABBR AS UNIT_ABBR_P1 ,  P2.UNIT_ABBR AS UNIT_ABBR_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.UNIT_SEQ = P2.UNIT_SEQ
                    ORDER BY TO_NUMBER(P1.UNIT_SEQ), NLSSORT(P1.UNIT_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,TO_NUMBER(P2.UNIT_SEQ), NLSSORT(P2.UNIT_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '20': //ตารางกลุ่มประเทศ
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_COUNTRY_GROUP
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_COUNTRY_GROUP 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.COUNTRY_GROUP_SEQ AS COUNTRY_GROUP_SEQ_P1 , P2.COUNTRY_GROUP_SEQ AS COUNTRY_GROUP_SEQ_P2
                        ,P1.COUNTRY_GROUP_ID AS COUNTRY_GROUP_ID_P1 , P2.COUNTRY_GROUP_ID AS COUNTRY_GROUP_ID_P2
                        ,P1.COUNTRY_GROUP_NAME AS COUNTRY_GROUP_NAME_P1 , P2.COUNTRY_GROUP_NAME AS COUNTRY_GROUP_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.COUNTRY_GROUP_SEQ = P2.COUNTRY_GROUP_SEQ
                    ORDER BY TO_NUMBER(P1.COUNTRY_GROUP_SEQ), NLSSORT(P1.COUNTRY_GROUP_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,TO_NUMBER(P2.COUNTRY_GROUP_SEQ), NLSSORT(P2.COUNTRY_GROUP_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '21': //ตารางภูมิภาค
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_REGION
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_REGION 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.REGION_SEQ AS REGION_SEQ_P1 , P2.REGION_SEQ AS REGION_SEQ_P2
                        ,P1.REGION_ID AS REGION_ID_P1 , P2.REGION_ID AS REGION_ID_P2
                        ,P1.REGION_NAME AS REGION_NAME_P1 , P2.REGION_NAME AS REGION_NAME_P2
                        ,P1.REGION_NAME_EN AS REGION_NAME_EN_P1 , P2.REGION_NAME_EN AS REGION_NAME_EN_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.REGION_SEQ = P2.REGION_SEQ
                    ORDER BY TO_NUMBER(P1.REGION_SEQ), NLSSORT(P1.REGION_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,TO_NUMBER(P1.REGION_SEQ), NLSSORT(P2.REGION_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '22': //ตารางข้อมูลประเภทหน่วยงาน
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_OFFICE_TYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_OFFICE_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.OFFICE_TYPE_SEQ AS OFFICE_TYPE_SEQ_P1 , P2.OFFICE_TYPE_SEQ AS OFFICE_TYPE_SEQ_P2
                        ,P1.OFFICE_TYPE_ID AS OFFICE_TYPE_ID_P1 , P2.OFFICE_TYPE_ID AS OFFICE_TYPE_ID_P2
                        ,P1.OFFICE_TYPE_NAME AS OFFICE_TYPE_NAME_P1 , P2.OFFICE_TYPE_NAME AS OFFICE_TYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.OFFICE_TYPE_SEQ = P2.OFFICE_TYPE_SEQ
                    ORDER BY P1.OFFICE_TYPE_SEQ, NLSSORT(P1.OFFICE_TYPE_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P1.OFFICE_TYPE_SEQ, NLSSORT(P2.OFFICE_TYPE_NAME, 'NLS_SORT=THAI_DICTIONARY') ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '23': //ตารางข้อมูลระดับหน่วยงาน
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_OFFICE_LEVEL
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_OFFICE_LEVEL 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.OFFICE_LEVEL_SEQ AS OFFICE_LEVEL_SEQ_P1 , P2.OFFICE_LEVEL_SEQ AS OFFICE_LEVEL_SEQ_P2
                        ,P1.OFFICE_LEVEL_ID AS OFFICE_LEVEL_ID_P1 , P2.OFFICE_LEVEL_ID AS OFFICE_LEVEL_ID_P2
                        ,P1.OFFICE_LEVEL_NAME AS OFFICE_LEVEL_NAME_P1 , P2.OFFICE_LEVEL_NAME AS OFFICE_LEVEL_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.OFFICE_LEVEL_SEQ = P2.OFFICE_LEVEL_SEQ
                    ORDER BY P1.OFFICE_LEVEL_SEQ, NLSSORT(P1.OFFICE_LEVEL_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.OFFICE_LEVEL_SEQ, NLSSORT(P2.OFFICE_LEVEL_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '24': //ตารางข้อมูลหน่วยงาน(ภายนอก)
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (3)
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_LANDOFFICE 
                        WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (3)
                        )
                    SELECT P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
                        ,P1.LANDOFFICE_ID AS LANDOFFICE_ID_P1 , P2.LANDOFFICE_ID AS LANDOFFICE_ID_P2
                        ,P1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 , P2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    ORDER BY P1.LANDOFFICE_SEQ, NLSSORT(P1.LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.LANDOFFICE_SEQ, NLSSORT(P2.LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '25': //ตารางข้อมูลหน่วยงาน(ภายใน)
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2)
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_LANDOFFICE 
                        WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2)
                        )
                    SELECT P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
                        ,P1.LANDOFFICE_ID AS LANDOFFICE_ID_P1 , P2.LANDOFFICE_ID AS LANDOFFICE_ID_P2
                        ,P1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 , P2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    ORDER BY P1.LANDOFFICE_SEQ, NLSSORT(P1.LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.LANDOFFICE_SEQ, NLSSORT(P2.LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '26': //ตารางข้อมูลฝ่าย
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_LANDOFFICE
                        WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2) AND LANDOFFICE_TYPE_SEQ IN (11,12,13,14,15,16,17,18,19)
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_LANDOFFICE 
                        WHERE RECORD_STATUS = 'N' AND OFFICE_INTERNAL_FLAG IN (1,2) AND LANDOFFICE_TYPE_SEQ IN (11,12,13,14,15,16,17,18,19)
                        )
                    SELECT P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1 , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2
                        ,T1.LANDOFFICE_TYPE_NAME AS LANDOFFICE_TYPE_NAME_P1 , T2.LANDOFFICE_TYPE_NAME AS LANDOFFICE_TYPE_NAME_P2
                        ,P1.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P1 , P2.LANDOFFICE_NAME_TH AS LANDOFFICE_NAME_TH_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LANDOFFICE_SEQ = P2.LANDOFFICE_SEQ
                    LEFT OUTER JOIN MGT1.TB_MAS_LANDOFFICE_TYPE T1
                        ON P1.LANDOFFICE_TYPE_SEQ = T1.LANDOFFICE_TYPE_SEQ
                        AND T1.RECORD_STATUS = 'N'
                    LEFT OUTER JOIN MAS.TB_MAS_LANDOFFICE_TYPE T2
                        ON P2.LANDOFFICE_TYPE_SEQ = T2.LANDOFFICE_TYPE_SEQ
                        AND T2.RECORD_STATUS = 'N'
                    ORDER BY TO_NUMBER(P1.LANDOFFICE_TYPE_SEQ), NLSSORT(P1.LANDOFFICE_ID, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P1.LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY')
                            ,TO_NUMBER(P2.LANDOFFICE_TYPE_SEQ), NLSSORT(P2.LANDOFFICE_ID, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P2.LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY')";

                $stid = oci_parse($conn, $select); 
                oci_execute($stid);
        break;

        case '27': //ตารางข้อมูลวันหยุด
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_MAS_HOLIDAY
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM MAS.TB_MAS_HOLIDAY 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.HOLIDAY_SEQ AS HOLIDAY_SEQ_P1 ,  P2.HOLIDAY_SEQ AS HOLIDAY_SEQ_P2
                        ,P1.HOLIDAY_DESCRIPTION AS HOLIDAY_DESCRIPTION_P1 ,  P2.HOLIDAY_DESCRIPTION AS HOLIDAY_DESCRIPTION_P2
                        ,CASE WHEN SUBSTR(P1.HOLIDAY_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.HOLIDAY_DATE) ELSE TO_CHAR(P1.HOLIDAY_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS HOLIDAY_DATE_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.HOLIDAY_DATE) ELSE TO_CHAR(P2.HOLIDAY_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS HOLIDAY_DATE_P2
                        
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.HOLIDAY_DATE = P2.HOLIDAY_DATE
                    ORDER BY P1.HOLIDAY_DATE, NLSSORT(P1.HOLIDAY_DESCRIPTION, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.HOLIDAY_DATE, NLSSORT(P2.HOLIDAY_DESCRIPTION, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

    }
?>