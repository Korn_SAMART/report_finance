<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //การได้มา
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_REG_MAS_OBTAIN
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM REG.TB_REG_MAS_OBTAIN 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.OBTAIN_SEQ AS OBTAIN_SEQ_P1 ,  P2.OBTAIN_SEQ AS OBTAIN_SEQ_P2
                        ,P1.OBTAIN_NAME AS OBTAIN_NAME_P1 ,  P2.OBTAIN_NAME AS OBTAIN_NAME_P2
                        ,P1.CREATE_DTM AS CREATE_DTM_P1 
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.OBTAIN_SEQ = P2.OBTAIN_SEQ
                    ORDER BY P1.OBTAIN_SEQ, P2.OBTAIN_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ประเภทการจดทะเบียน    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_REG_MAS_REGISTER
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM REG.TB_REG_MAS_REGISTER 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.REGISTER_SEQ AS REGISTER_SEQ_P1 ,  P2.REGISTER_SEQ AS REGISTER_SEQ_P2
                        ,P1.REGISTER_ABBR AS REGISTER_ABBR_P1 ,  P2.REGISTER_ABBR AS REGISTER_ABBR_P2
                        ,P1.REGISTER_NAME AS REGISTER_NAME_P1 ,  P2.REGISTER_NAME AS REGISTER_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.REGISTER_SEQ = P2.REGISTER_SEQ
                    ORDER BY P1.REGISTER_SEQ, NLSSORT(P1.REGISTER_ABBR, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P1.REGISTER_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.REGISTER_SEQ, NLSSORT(P2.REGISTER_ABBR, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P2.REGISTER_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ชื่อประเภทบัญชีคุม   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_REG_MAS_BOOK_TYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM REG.TB_REG_MAS_BOOK_TYPE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.BOOK_TYPE_SEQ AS BOOK_TYPE_SEQ_P1 ,  P2.BOOK_TYPE_SEQ AS BOOK_TYPE_SEQ_P2
                        ,P1.BOOK_TYPE_NAME AS BOOK_TYPE_NAME_P1 ,  P2.BOOK_TYPE_NAME AS BOOK_TYPE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.BOOK_TYPE_SEQ = P2.BOOK_TYPE_SEQ
                    ORDER BY P1.BOOK_TYPE_SEQ, P2.BOOK_TYPE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //รายการเอกสารจดทะเบียน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_REG_MAS_DOCUMENT
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM REG.TB_REG_MAS_DOCUMENT
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.DOCUMENT_SEQ AS DOCUMENT_SEQ_P1 ,  P2.DOCUMENT_SEQ AS DOCUMENT_SEQ_P2
                        ,P1.DOCUMENT_ID AS DOCUMENT_ID_P1 ,  P2.DOCUMENT_ID AS DOCUMENT_ID_P2
                        ,P1.DOCUMENT_ABBR AS DOCUMENT_ABBR_P1 ,  P2.DOCUMENT_ABBR AS DOCUMENT_ABBR_P2
                        ,P1.DOCUMENT_NAME AS DOCUMENT_NAME_P1 ,  P2.DOCUMENT_NAME AS DOCUMENT_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                        ,CASE P1.DOCUMENT_STS 
                        WHEN 1 THEN 'เอกสารสิทธิ'
                        WHEN 2 THEN 'สารบบที่ดิน'
                        WHEN 3 THEN 'สารบบห้องชุด'
                        WHEN 4 THEN 'สารบบจัดสรร'
                        WHEN 9 THEN 'อื่นๆ'
                        END AS DOCUMENT_STS_P1
                        ,CASE P2.DOCUMENT_STS 
                        WHEN 1 THEN 'เอกสารสิทธิ'
                        WHEN 2 THEN 'สารบบที่ดิน'
                        WHEN 3 THEN 'สารบบห้องชุด'
                        WHEN 4 THEN 'สารบบจัดสรร'
                        WHEN 9 THEN 'อื่นๆ'
                        END AS DOCUMENT_STS_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.DOCUMENT_SEQ = P2.DOCUMENT_SEQ
                    ORDER BY P1.DOCUMENT_SEQ, TO_NUMBER(P1.DOCUMENT_ID), NLSSORT(P1.DOCUMENT_ABBR, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P1.DOCUMENT_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.DOCUMENT_SEQ, TO_NUMBER(P2.DOCUMENT_ID), NLSSORT(P2.DOCUMENT_ABBR, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P2.DOCUMENT_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

    }
?>