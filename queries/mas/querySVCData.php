<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //หมุดเส้นโครงาน
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVC_MAS_CTRLMARK
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVC_MAS_CTRLMARK
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.TV_ZONE AS TV_ZONE_P1 ,  P2.TV_ZONE AS TV_ZONE_P2
                        ,P1.TV_PROV AS TV_PROV_P1 ,  P2.TV_PROV AS TV_PROV_P2
                        ,P1.TV_NAME AS TV_NAME_P1 ,  P2.TV_NAME AS TV_NAME_P2
                        ,P1.TVC_ORDERNO AS TVC_ORDERNO_P1 ,  P2.TVC_ORDERNO AS TVC_ORDERNO_P2
                        ,P1.TVC_CTRLNAME AS TVC_CTRLNAME_P1 ,  P2.TVC_CTRLNAME AS TVC_CTRLNAME_P2
                        ,P1.CREATE_DTM AS CREATE_DTM_P1 , P2.CREATE_DTM AS CREATE_DTM_P2
                        ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 , P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2 

                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.TV_ZONE = P2.TV_ZONE
                        AND P1.TV_PROV = P2.TV_PROV
                        AND P1.TV_NAME = P2.TV_NAME
                        AND P1.TVC_ORDERNO = P2.TVC_ORDERNO
                        AND P1.TVC_CTRLNAME = P2.TVC_CTRLNAME
                    ORDER BY P1.TV_ZONE, NLSSORT(P1.TV_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P1.TVC_CTRLNAME, 'NLS_SORT=THAI_DICTIONARY'), P1.TV_PROV
                            ,P2.TV_ZONE, NLSSORT(P2.TV_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P2.TVC_CTRLNAME, 'NLS_SORT=THAI_DICTIONARY'), P2.TV_PROV";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //หมุดดาวเทียม    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVC_MAS_GPSMARK
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVC_MAS_GPSMARK
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.GPSMARK_SEQ AS GPSMARK_SEQ_P1 ,  P2.GPSMARK_SEQ AS GPSMARK_SEQ_P2
                        ,P1.GM_ZONE AS GM_ZONE_P1 ,  P2.GM_ZONE AS GM_ZONE_P2
                        ,P1.GM_NAME AS GM_NAME_P1 ,  P2.GM_NAME AS GM_NAME_P2
                        ,P1.CREATE_DTM AS CREATE_DTM_P1 , P2.CREATE_DTM AS CREATE_DTM_P2
                        ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 , P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2 
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.GPSMARK_SEQ = P2.GPSMARK_SEQ
                    ORDER BY P1.GPSMARK_SEQ, NLSSORT(P1.GM_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.GPSMARK_SEQ, NLSSORT(P2.GM_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ระวาง 4000 โซน 47   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVC_MAS_MAPSHEET4000_47
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVC_MAS_MAPSHEET4000_47
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.INDEX_SEQ AS INDEX_SEQ_P1 ,  P2.INDEX_SEQ AS INDEX_SEQ_P2
                        ,P1.INDEX_NAME AS INDEX_NAME_P1 ,  P2.INDEX_NAME AS INDEX_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.INDEX_SEQ = P2.INDEX_SEQ
                    ORDER BY P1.INDEX_SEQ, P1.INDEX_NAME
                            ,P2.INDEX_SEQ, P2.INDEX_NAME";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //ระวาง 4000 โซน 48
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVC_MAS_MAPSHEET4000_48
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVC_MAS_MAPSHEET4000_48
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.INDEX_SEQ AS INDEX_SEQ_P1 ,  P2.INDEX_SEQ AS INDEX_SEQ_P2
                        ,P1.INDEX_NAME AS INDEX_NAME_P1 ,  P2.INDEX_NAME AS INDEX_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.INDEX_SEQ = P2.INDEX_SEQ
                    ORDER BY P1.INDEX_SEQ, P1.INDEX_NAME
                            ,P2.INDEX_SEQ, P2.INDEX_NAME";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '5': //เส้นโครงงาน    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVC_MAS_TRAVERSE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVC_MAS_TRAVERSE
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.TV_ZONE AS TV_ZONE_P1 ,  P2.TV_ZONE AS TV_ZONE_P2
                        ,P1.TV_PROV AS TV_PROV_P1 ,  P2.TV_PROV AS TV_PROV_P2
                        ,P1.TV_NAME AS TV_NAME_P1 ,  P2.TV_NAME AS TV_NAME_P2
                        ,P1.TV_DIVISION AS TV_DIVISION_P1 ,  P2.TV_DIVISION AS TV_DIVISION_P2
                        ,P1.TV_AMOUNT_QTY AS TV_AMOUNT_QTY_P1 ,  P2.TV_AMOUNT_QTY AS TV_AMOUNT_QTY_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.TV_ZONE = P2.TV_ZONE
                        AND P1.TV_PROV = P2.TV_PROV
                        AND P1.TV_NAME = P2.TV_NAME
                        AND P1.TV_DIVISION = P2.TV_DIVISION
                        AND P1.TV_AMOUNT_QTY = P2.TV_AMOUNT_QTY
                    ORDER BY P1.TV_ZONE, P1.TV_PROV, NLSSORT(P1.TV_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,P2.TV_ZONE, P2.TV_PROV, NLSSORT(P2.TV_NAME, 'NLS_SORT=THAI_DICTIONARY')";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;


    }
?>