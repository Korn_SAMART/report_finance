<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ประเภทครุภัณฑ์
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_USP_MAS_TYPE_ASSET
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM USP.TB_USP_MAS_TYPE_ASSET
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.TYPE_ASSET_SEQ AS TYPE_ASSET_SEQ_P1 ,  P2.TYPE_ASSET_SEQ AS TYPE_ASSET_SEQ_P2
        --                        ,T1.GROUP_ASSET_NAME AS GROUP_ASSET_NAME_P1 ,  T2.GROUP_ASSET_NAME AS GROUP_ASSET_NAME_P2
                        ,P1.TYPE_ASSET_ID AS TYPE_ASSET_ID_P1 ,  P2.TYPE_ASSET_ID AS TYPE_ASSET_ID_P2
                        ,P1.TYPE_ASSET_NAME AS TYPE_ASSET_NAME_P1 ,  P2.TYPE_ASSET_NAME AS TYPE_ASSET_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2  
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.TYPE_ASSET_SEQ = P2.TYPE_ASSET_SEQ
                    LEFT OUTER JOIN MGT1.TB_USP_MAS_GROUP_ASSET T1
                        ON P1.GROUP_ASSET_SEQ = T1.GROUP_ASSET_SEQ
                    LEFT OUTER JOIN USP.TB_USP_MAS_GROUP_ASSET T2
                        ON P2.GROUP_ASSET_SEQ = T2.GROUP_ASSET_SEQ                        
                    ORDER BY P1.TYPE_ASSET_SEQ
                            ,P2.TYPE_ASSET_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ประเภทและชื่อวัสดุ    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_USP_MAS_MAT
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM USP.TB_USP_MAS_MAT
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.MAT_SEQ AS MAT_SEQ_P1 ,  P2.MAT_SEQ AS MAT_SEQ_P2
        --                        ,T1.MAT_TYPE_NAME AS MAT_TYPE_NAME_P1 ,  T2.MAT_TYPE_NAME AS MAT_TYPE_NAME_P2
                        ,P1.MAT_NAME AS MAT_NAME_P1 ,  P2.MAT_NAME AS MAT_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2  
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.MAT_SEQ = P2.MAT_SEQ
                    LEFT OUTER JOIN MGT1.TB_USP_MAS_MAT_TYPE T1
                        ON P1.MAT_TYPE_SEQ = T1.MAT_TYPE_SEQ
                    LEFT OUTER JOIN USP.TB_USP_MAS_MAT_TYPE T2
                        ON P2.MAT_TYPE_SEQ = T2.MAT_TYPE_SEQ
                    ORDER BY P1.MAT_SEQ
                            ,P2.MAT_SEQ";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ประเภทและชื่อแบบพิมพ์   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_USP_MAS_PLATE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM USP.TB_USP_MAS_PLATE
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.PLATE_SEQ AS PLATE_SEQ_P1 ,  P2.PLATE_SEQ AS PLATE_SEQ_P2
        --                        ,T1.PLATE_TYPE_NAME AS PLATE_TYPE_NAME_P1 ,  T2.PLATE_TYPE_NAME AS PLATE_TYPE_NAME_P2
                        ,P1.PLATE_ABBR_NAME AS PLATE_ABBR_NAME_P1 , P2.PLATE_ABBR_NAME AS PLATE_ABBR_NAME_P2
                        ,P1.PLATE_NAME AS PLATE_NAME_P1 ,  P2.PLATE_NAME AS PLATE_NAME_P2
        --                        ,TT1.UNIT_NAME AS UNIT_NAME_P1 ,  TT2.UNIT_NAME AS UNIT_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2  
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PLATE_SEQ = P2.PLATE_SEQ
                    LEFT OUTER JOIN MGT1.TB_USP_MAS_PLATE_TYPE T1
                        ON P1.PLATE_TYPE_SEQ = T1.PLATE_TYPE_SEQ
                    LEFT OUTER JOIN USP.TB_USP_MAS_PLATE_TYPE T2
                        ON P2.PLATE_TYPE_SEQ = T2.PLATE_TYPE_SEQ
                    LEFT OUTER JOIN MGT1.TB_USP_MAS_UNIT TT1
                        ON P1.UNIT_SEQ = TT1.UNIT_SEQ
                    LEFT OUTER JOIN USP.TB_USP_MAS_UNIT TT2
                        ON P2.UNIT_SEQ = TT2.UNIT_SEQ
                    ORDER BY P1.PLATE_SEQ
                            ,P2.PLATE_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //ประเภทและชื่อหลักเขต
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_USP_MAS_PIECE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM USP.TB_USP_MAS_PIECE
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.PIECE_SEQ AS PIECE_SEQ_P1 ,  P2.PIECE_SEQ AS PIECE_SEQ_P2
                        ,P1.PIECE_NAME AS PIECE_NAME_P1 ,  P2.PIECE_NAME AS PIECE_NAME_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2  
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PIECE_SEQ = P2.PIECE_SEQ
                    LEFT OUTER JOIN MGT1.TB_USP_MAS_UNIT TT1
                        ON P1.UNIT_SEQ = TT1.UNIT_SEQ
                    LEFT OUTER JOIN USP.TB_USP_MAS_UNIT TT2
                        ON P2.UNIT_SEQ = TT2.UNIT_SEQ
                    ORDER BY P1.PIECE_SEQ
                            ,P2.PIECE_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '5': //ประเภทและชื่ออาคารและสิ่งก่อสร้าง    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_USP_MAS_BLD
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM USP.TB_USP_MAS_BLD
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.BLD_SEQ AS BLD_SEQ_P1 ,  P2.BLD_SEQ AS BLD_SEQ_P2
        --                        ,T1.BLD_TYPE_NAME AS BLD_TYPE_NAME_P1 ,  T2.BLD_TYPE_NAME AS BLD_TYPE_NAME_P2
                        ,P1.BLD_NAME AS BLD_NAME_P1 ,  P2.BLD_NAME AS BLD_NAME_P2
                        ,P1.BLD_YEAR AS BLD_YEAR_P1 ,  P2.BLD_YEAR AS BLD_YEAR_P2
                        ,P1.BLD_RATE AS BLD_RATE_P1 ,  P2.BLD_RATE AS BLD_RATE_P2
                        ,CASE WHEN SUBSTR(P1.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.CREATE_DTM) ELSE TO_CHAR(P1.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P1
                        ,CASE WHEN SUBSTR(P2.CREATE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.CREATE_DTM) ELSE TO_CHAR(P2.CREATE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CREATE_DTM_P2
                        ,CASE WHEN SUBSTR(P1.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.LAST_UPD_DTM) ELSE TO_CHAR(P1.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P1
                        ,CASE WHEN SUBSTR(P2.LAST_UPD_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.LAST_UPD_DTM) ELSE TO_CHAR(P2.LAST_UPD_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS LAST_UPD_DTM_P2  
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.BLD_SEQ = P2.BLD_SEQ
                    LEFT OUTER JOIN MGT1.TB_USP_MAS_BLD_TYPE T1
                        ON P1.BLD_TYPE_SEQ = T1.BLD_TYPE_SEQ
                    LEFT OUTER JOIN USP.TB_USP_MAS_BLD_TYPE T2
                        ON P2.BLD_TYPE_SEQ = T2.BLD_TYPE_SEQ
                    ORDER BY P1.BLD_SEQ
                            ,P2.BLD_SEQ";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;


    }
?>