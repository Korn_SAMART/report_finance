<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    switch ($table) {
        case 'APS1':
            $select = "WITH SEQ AS (
                        SELECT PP.PROCESS_PARCEL_TEMP_SEQ, PP.PARCEL_SEQ, PP.PARCEL_LAND_SEQ, PP.CONDOROOM_SEQ, PP.CONSTRUCT_SEQ
                        FROM MGT1.TB_REG_PROCESS_PARCEL_TEMP PP
                        WHERE PP.RECORD_STATUS = 'N' AND PP.PROCESS_TEMP_SEQ = :processSeqP1
                    ),
                    TEMP AS (
                        SELECT SEQ.PROCESS_PARCEL_TEMP_SEQ, PC.PARCEL_SEQ, PARCEL_NO, AMPHUR_SEQ, PRINTPLATE_TYPE_SEQ, 
                            NVL(PARCEL_RAI_NUM,0)||'-'||NVL(PARCEL_NGAN_NUM,0)||'-'||NVL(PARCEL_WA_NUM,0)||'.'||NVL(PARCEL_SUBWA_NUM,0) AS AREA,
                            OPT.OPT_SEQ, PARCEL_OPT_NUME_NUM||'/'||PARCEL_OPT_DENO_NUM AS NUM_OPT
                        FROM MGT1.TB_REG_PARCEL PC
                        LEFT JOIN MGT1.TB_REG_PARCEL_OPT OPT
                            ON PC.PARCEL_SEQ = OPT.PARCEL_SEQ
                        INNER JOIN SEQ
                            ON PC.PARCEL_SEQ = SEQ.PARCEL_SEQ
                        UNION
                        SELECT SEQ.PROCESS_PARCEL_TEMP_SEQ, PCL.PARCEL_LAND_SEQ, PARCEL_LAND_NO, AMPHUR_SEQ, PRINTPLATE_TYPE_SEQ, 
                            NVL(PARCEL_LAND_RAI_NUM,0)||'-'||NVL(PARCEL_LAND_NGAN_NUM,0)||'-'||NVL(PARCEL_LAND_WA_NUM,0)||'.'||NVL(PARCEL_LAND_SUBWA_NUM,0) as AREA,
                            OPT.OPT_SEQ, PARCEL_LAND_OPT_NUME_NUM||'/'||PARCEL_LAND_OPT_DENO_NUM AS NUM_OPT
                        FROM MGT1.TB_REG_PARCEL_LAND PCL
                        LEFT JOIN MGT1.TB_REG_PARCEL_LAND_OPT OPT
                            ON PCL.PARCEL_LAND_SEQ = OPT.PARCEL_LAND_SEQ
                        INNER JOIN SEQ
                            ON PCL.PARCEL_LAND_SEQ = SEQ.PARCEL_LAND_SEQ
                        UNION
                        SELECT SEQ.PROCESS_PARCEL_TEMP_SEQ, C.CONSTRUCT_SEQ, ADDR.CONSTRUCT_ADDR_HNO, ADDR.AMPHUR_SEQ, PRINTPLATE_TYPE_SEQ, TO_CHAR(CONSTRUCT_AREA_NUM), NULL AS OPT_SEQ, NULL AS NUM_OPT
                        FROM MGT1.TB_REG_CONSTRUCT C
                        LEFT JOIN MGT1.TB_REG_CONSTRUCT_ADDR ADDR
                            ON ADDR.CONSTRUCT_ADDR_SEQ = C.CONSTRUCT_ADDR_SEQ
                        INNER JOIN SEQ
                            ON C.CONSTRUCT_SEQ = SEQ.CONSTRUCT_SEQ
                        UNION
                        SELECT SEQ.PROCESS_PARCEL_TEMP_SEQ, CDR.CONDOROOM_SEQ, CONDOROOM_RNO, AMPHUR_SEQ, PRINTPLATE_TYPE_SEQ, TO_CHAR(CONDOROOM_AREA_NUM),
                            OPT.OPT_SEQ, CONDOROOM_OPT_NUME_NUM||'/'||CONDOROOM_OPT_DENO_NUM AS NUM_OPT
                        FROM MGT1.TB_REG_CONDOROOM CDR
                        LEFT JOIN MGT1.TB_REG_CONDOROOM_OPT OPT
                            ON CDR.CONDOROOM_SEQ = OPT.CONDOROOM_SEQ
                        INNER JOIN SEQ
                            ON CDR.CONDOROOM_SEQ = SEQ.CONDOROOM_SEQ
                        INNER JOIN MGT1.TB_REG_CONDO_BLD BLD
                            ON CDR.BLD_SEQ = BLD.BLD_SEQ
                        INNER JOIN MGT1.TB_REG_CONDO CD
                            ON CD.CONDO_SEQ = BLD.CONDO_SEQ
                    )
                    SELECT PPA.PROCESS_PARCEL_TEMP_SEQ, 
                        CASE WHEN PPA.CONSTRUCT_SEQ IS NOT NULL THEN CONSTRUCTION_NAME ELSE PRINTPLATE_TYPE_NAME END AS PRINTPLATE_TYPE, 
                        CASE WHEN PPA.CONSTRUCT_SEQ IS NOT NULL THEN CONSTRUCT_ADDR_HNO ELSE PARCEL_NO END NO, 
                        AMPHUR_NAME,
                        OPT_NAME, NUM_OPT,
                        CASE WHEN PPA.CONSTRUCT_SEQ IS NOT NULL THEN TO_CHAR(CONSTRUCT_AREA_NUM) ELSE AREA END AS AREA,
                        PROCESS_PARCEL_APS_TEMP_NUM, 
                        PROCESS_PARCEL_APS_TEMP_CON, PROCESS_PARCEL_APS_TEMP_PAR, PROCESS_PARCEL_APS_TEMP_TOT,
                        PROCESS_PCDR_TEMP_DESC, PROCESS_PCDR_TEMP_AREA_NUM, PROCESS_PCDR_TEMP_APS_MNY, PROCESS_PCDR_TEMP_APST_MNY
                    FROM TEMP
                    INNER JOIN MGT1.TB_REG_MAS_PRINTPLATE_TYPE MP
                        ON MP.PRINTPLATE_TYPE_SEQ = TEMP.PRINTPLATE_TYPE_SEQ
                    INNER JOIN MGT1.TB_REG_PROCESS_PARCEL_APS_TEMP PPA
                        ON PPA.PROCESS_PARCEL_TEMP_SEQ = TEMP.PROCESS_PARCEL_TEMP_SEQ
                    LEFT JOIN MGT1.TB_MAS_OPT OPT
                        ON OPT.OPT_SEQ = TEMP.OPT_SEQ
                    LEFT JOIN MGT1.TB_REG_CONSTRUCT C
                        ON C.CONSTRUCT_SEQ = PPA.CONSTRUCT_SEQ
                    LEFT JOIN MGT1.TB_REG_CONSTRUCT_ADDR ADDR
                        ON ADDR.CONSTRUCT_ADDR_SEQ = C.CONSTRUCT_ADDR_SEQ
                    LEFT JOIN MGT1.TB_MAS_AMPHUR APT
                        ON APT.AMPHUR_SEQ = TEMP.AMPHUR_SEQ
                    LEFT JOIN MGT1.TB_REG_PROCESS_PARCEL_CDR_TEMP PPC
                        ON PPC.PROCESS_PARCEL_APS_TEMP_SEQ = PPA.PROCESS_PARCEL_APS_TEMP_SEQ
                        AND PPC.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_REG_MAS_CONSTRUCTION MC
                        ON MC.CONSTRUCTION_SEQ = C.CONSTRUCTION_SEQ
                        ";


            $stid = oci_parse($conn, $select);
            //oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($processSeqP1 != '') oci_bind_by_name($stid, ':processSeqP1', $processSeqP1);
            oci_execute($stid);
        break;

        case 'APS2':
            $select = "WITH SEQ AS (
                        SELECT PP.PROCESS_PARCEL_TEMP_SEQ, PP.PARCEL_SEQ, PP.PARCEL_LAND_SEQ, PP.CONDOROOM_SEQ, PP.CONSTRUCT_SEQ
                        FROM REG.TB_REG_PROCESS_PARCEL_TEMP PP
                        WHERE PP.RECORD_STATUS = 'N' AND PP.PROCESS_TEMP_SEQ = :processSeqP2
                    ),
                    TEMP AS (
                        SELECT SEQ.PROCESS_PARCEL_TEMP_SEQ, PC.PARCEL_SEQ, PARCEL_NO, AMPHUR_SEQ, PRINTPLATE_TYPE_SEQ, 
                            NVL(PARCEL_RAI_NUM,0)||'-'||NVL(PARCEL_NGAN_NUM,0)||'-'||NVL(PARCEL_WA_NUM,0)||'.'||NVL(PARCEL_SUBWA_NUM,0) AS AREA,
                            OPT.OPT_SEQ, PARCEL_OPT_NUME_NUM||'/'||PARCEL_OPT_DENO_NUM AS NUM_OPT
                        FROM REG.TB_REG_PARCEL PC
                        LEFT JOIN REG.TB_REG_PARCEL_OPT OPT
                            ON PC.PARCEL_SEQ = OPT.PARCEL_SEQ
                        INNER JOIN SEQ
                            ON PC.PARCEL_SEQ = SEQ.PARCEL_SEQ
                        UNION
                        SELECT SEQ.PROCESS_PARCEL_TEMP_SEQ, PCL.PARCEL_LAND_SEQ, PARCEL_LAND_NO, AMPHUR_SEQ, PRINTPLATE_TYPE_SEQ, 
                            NVL(PARCEL_LAND_RAI_NUM,0)||'-'||NVL(PARCEL_LAND_NGAN_NUM,0)||'-'||NVL(PARCEL_LAND_WA_NUM,0)||'.'||NVL(PARCEL_LAND_SUBWA_NUM,0) as AREA,
                            OPT.OPT_SEQ, PARCEL_LAND_OPT_NUME_NUM||'/'||PARCEL_LAND_OPT_DENO_NUM AS NUM_OPT
                        FROM REG.TB_REG_PARCEL_LAND PCL
                        LEFT JOIN REG.TB_REG_PARCEL_LAND_OPT OPT
                            ON PCL.PARCEL_LAND_SEQ = OPT.PARCEL_LAND_SEQ
                        INNER JOIN SEQ
                            ON PCL.PARCEL_LAND_SEQ = SEQ.PARCEL_LAND_SEQ
                        UNION
                        SELECT SEQ.PROCESS_PARCEL_TEMP_SEQ, C.CONSTRUCT_SEQ, ADDR.CONSTRUCT_ADDR_HNO, ADDR.AMPHUR_SEQ, PRINTPLATE_TYPE_SEQ, TO_CHAR(CONSTRUCT_AREA_NUM), NULL AS OPT_SEQ, NULL AS NUM_OPT
                        FROM REG.TB_REG_CONSTRUCT C
                        LEFT JOIN REG.TB_REG_CONSTRUCT_ADDR ADDR
                            ON ADDR.CONSTRUCT_ADDR_SEQ = C.CONSTRUCT_ADDR_SEQ
                        INNER JOIN SEQ
                            ON C.CONSTRUCT_SEQ = SEQ.CONSTRUCT_SEQ
                        UNION
                        SELECT SEQ.PROCESS_PARCEL_TEMP_SEQ, CDR.CONDOROOM_SEQ, CONDOROOM_RNO, AMPHUR_SEQ, PRINTPLATE_TYPE_SEQ, TO_CHAR(CONDOROOM_AREA_NUM),
                            OPT.OPT_SEQ, CONDOROOM_OPT_NUME_NUM||'/'||CONDOROOM_OPT_DENO_NUM AS NUM_OPT
                        FROM REG.TB_REG_CONDOROOM CDR
                        LEFT JOIN REG.TB_REG_CONDOROOM_OPT OPT
                            ON CDR.CONDOROOM_SEQ = OPT.CONDOROOM_SEQ
                        INNER JOIN SEQ
                            ON CDR.CONDOROOM_SEQ = SEQ.CONDOROOM_SEQ
                        INNER JOIN REG.TB_REG_CONDO_BLD BLD
                            ON CDR.BLD_SEQ = BLD.BLD_SEQ
                        INNER JOIN REG.TB_REG_CONDO CD
                            ON CD.CONDO_SEQ = BLD.CONDO_SEQ
                    )
                    SELECT PPA.PROCESS_PARCEL_TEMP_SEQ, 
                        CASE WHEN PPA.CONSTRUCT_SEQ IS NOT NULL THEN CONSTRUCTION_NAME ELSE PRINTPLATE_TYPE_NAME END AS PRINTPLATE_TYPE, 
                        CASE WHEN PPA.CONSTRUCT_SEQ IS NOT NULL THEN CONSTRUCT_ADDR_HNO ELSE PARCEL_NO END NO, 
                        AMPHUR_NAME,
                        OPT_NAME, NUM_OPT,
                        CASE WHEN PPA.CONSTRUCT_SEQ IS NOT NULL THEN TO_CHAR(CONSTRUCT_AREA_NUM) ELSE AREA END AS AREA,
                        PROCESS_PARCEL_APS_TEMP_NUM, 
                        PROCESS_PARCEL_APS_TEMP_CON, PROCESS_PARCEL_APS_TEMP_PAR, PROCESS_PARCEL_APS_TEMP_TOT,
                        PROCESS_PCDR_TEMP_DESC, PROCESS_PCDR_TEMP_AREA_NUM, PROCESS_PCDR_TEMP_APS_MNY, PROCESS_PCDR_TEMP_APST_MNY
                    FROM TEMP
                    INNER JOIN REG.TB_REG_MAS_PRINTPLATE_TYPE MP
                        ON MP.PRINTPLATE_TYPE_SEQ = TEMP.PRINTPLATE_TYPE_SEQ
                    INNER JOIN REG.TB_REG_PROCESS_PARCEL_APS_TEMP PPA
                        ON PPA.PROCESS_PARCEL_TEMP_SEQ = TEMP.PROCESS_PARCEL_TEMP_SEQ
                    LEFT JOIN MAS.TB_MAS_OPT OPT
                        ON OPT.OPT_SEQ = TEMP.OPT_SEQ
                    LEFT JOIN REG.TB_REG_CONSTRUCT C
                        ON C.CONSTRUCT_SEQ = PPA.CONSTRUCT_SEQ
                    LEFT JOIN REG.TB_REG_CONSTRUCT_ADDR ADDR
                        ON ADDR.CONSTRUCT_ADDR_SEQ = C.CONSTRUCT_ADDR_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR APT
                        ON APT.AMPHUR_SEQ = TEMP.AMPHUR_SEQ
                    LEFT JOIN REG.TB_REG_PROCESS_PARCEL_CDR_TEMP PPC
                        ON PPC.PROCESS_PARCEL_APS_TEMP_SEQ = PPA.PROCESS_PARCEL_APS_TEMP_SEQ
                        AND PPC.RECORD_STATUS = 'N'
                    LEFT JOIN REG.TB_REG_MAS_CONSTRUCTION MC
                        ON MC.CONSTRUCTION_SEQ = C.CONSTRUCTION_SEQ
                            ";
            

            $stid = oci_parse($conn, $select);
            //oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($processSeqP2 != '') oci_bind_by_name($stid, ':processSeqP2', $processSeqP2);
            oci_execute($stid);
        break;

        }

?>