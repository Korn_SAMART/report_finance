<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    ///// CHANODE /////
    $amphur_sql = $amphur == ''? $amphur : ' AND AMPHUR_SEQ = :amphur ';
    $tambon_sql = $tambon == ''? $tambon : ' AND TAMBOL_SEQ = :tambon ';
    // $parcelNo_sql = $parcelNo == ''? $parcelNo : ' WHERE NVL(P2.PARCEL_NO,P1.PARCEL_NO) LIKE :parcelNo ';

    //// 2 3 4 5 8 23///
    // $parcelLandNo_sql = $parcelNo == ''? $parcelNo : ' WHERE NVL(P2.PARCEL_LAND_NO,P1.PARCEL_LAND_NO) LIKE :parcelLandNo ';
    //$type_sql = $check == ''? $ckeck : ' AND PRINTPLATE_TYPE_SEQ = :typeSeq ';

    ///// 13 /////
    

    // $condoNo_sql = $parcelNo == ''? $parcelNo : ' WHERE NVL(P2.CONDO_ID || '/' || P2.CONDO_REG_YEAR, P1.CONDO_ID || '/' || P1.CONDO_REG_YEAR) LIKE :condoNo ';
    // $condoName_sql = $condoName == ''? $condoName : ' WHERE NVL(P2.CONDO_NAME_TH,P1.CONDO_NAME_TH) LIKE :condoName ';

    ///// 9 /////
    // $condoroomNo_sql = $condoroomNo == ''? $condoroomNo : ' WHERE NVL(P2.CONDOROOM_RNO,P1.CONDOROOM_RNO) LIKE :condoroomNo ';

    switch ($check) {
        
        case '1':   //     1 chanode
            $parcelNo_sql = $parcelNo == ''? $parcelNo : ' WHERE NVL(P2.PARCEL_NO,P1.PARCEL_NO) LIKE :parcelNo ';
            $select = "WITH P1 AS
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_SEQ, PARCEL_NO
                FROM MGT1.TB_REG_PARCEL
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = 1
                ".$amphur_sql
                 .$tambon_sql."
            ),
            P2 AS 
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_SEQ, PARCEL_NO
                FROM REG.TB_REG_PARCEL
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = 1
                ".$amphur_sql
                .$tambon_sql."
            )
            SELECT NVL(P2.LANDOFFICE_SEQ,P1.LANDOFFICE_SEQ) AS LANDOFFICE_SEQ, AP.AMPHUR_SEQ, AP.AMPHUR_NAME, TB.TAMBOL_SEQ, TB.TAMBOL_NAME
                , P1.PARCEL_SEQ AS PARCEL_SEQ_P1, P2.PARCEL_SEQ AS PARCEL_SEQ_P2, NVL(P2.PARCEL_NO,P1.PARCEL_NO) AS PARCEL_NO
                , CASE WHEN (P2.PARCEL_SEQ IS NOT NULL AND P1.PARCEL_SEQ IS NOT NULL) THEN 0
                    WHEN (P2.PARCEL_SEQ IS NULL) THEN 1
                    WHEN (P1.PARCEL_SEQ IS NULL) THEN 2 END AS STS
            FROM P1
            INNER JOIN P2
                ON P1.PARCEL_SEQ = P2.PARCEL_SEQ
            LEFT JOIN MAS.TB_MAS_AMPHUR AP
                ON NVL(P2.AMPHUR_SEQ,P1.AMPHUR_SEQ) = AP.AMPHUR_SEQ
                AND AP.RECORD_STATUS = 'N'
            LEFT JOIN MAS.TB_MAS_TAMBOL TB
                ON NVL(P2.TAMBOL_SEQ,P1.TAMBOL_SEQ) = TB.TAMBOL_SEQ
                AND TB.RECORD_STATUS = 'N'
            ".$parcelNo_sql."
            ORDER BY NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY'), TO_NUMBER(PARCEL_NO)";
      //      OFFSET 0 ROWS FETCH NEXT 15 ROWS ONLY";

            $stid = oci_parse($conn, $select);
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($amphur != '') oci_bind_by_name($stid, ':amphur', $amphur);
            if ($tambon != '') oci_bind_by_name($stid, ':tambon', $tambon);
            if ($parcelNo != '') oci_bind_by_name($stid, ':parcelNo', $parcelNo);
            oci_execute($stid);
            break;

        case '2':    //     2 chanodeTrajong
        case '3':    //     3 trajong
        case '4':    //     4 ns3a
            $parcelLandNo_sql = $parcelNo == ''? $parcelNo : ' WHERE NVL(P2.PARCEL_LAND_NO,P1.PARCEL_LAND_NO) LIKE :parcelLandNo ';
            $select = "WITH P1 AS
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_SEQ, PARCEL_LAND_NO , PRINTPLATE_TYPE_SEQ
                FROM MGT1.TB_REG_PARCEL_LAND
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :typeSeq
                ".$amphur_sql
                .$tambon_sql."
            ),
            P2 AS 
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_SEQ, PARCEL_LAND_NO , PRINTPLATE_TYPE_SEQ
                FROM REG.TB_REG_PARCEL_LAND
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :typeSeq
                ".$amphur_sql
                .$tambon_sql."
            )
            SELECT NVL(P2.LANDOFFICE_SEQ,P1.LANDOFFICE_SEQ) AS LANDOFFICE_SEQ, AP.AMPHUR_SEQ, AP.AMPHUR_NAME, TB.TAMBOL_SEQ, TB.TAMBOL_NAME
                , P1.PARCEL_LAND_SEQ AS PARCEL_LAND_SEQ_P1, P2.PARCEL_LAND_SEQ AS PARCEL_LAND_SEQ_P2, NVL(P2.PARCEL_LAND_NO,P1.PARCEL_LAND_NO) AS PARCEL_LAND_NO
                , CASE WHEN (P2.PARCEL_LAND_SEQ IS NOT NULL AND P1.PARCEL_LAND_SEQ IS NOT NULL) THEN 0
                    WHEN (P2.PARCEL_LAND_SEQ IS NULL) THEN 1
                    WHEN (P1.PARCEL_LAND_SEQ IS NULL) THEN 2 END AS STS
            FROM P1
            RIGHT JOIN P2
                ON P1.PARCEL_LAND_SEQ = P2.PARCEL_LAND_SEQ
            LEFT JOIN MAS.TB_MAS_AMPHUR AP
                ON NVL(P2.AMPHUR_SEQ,P1.AMPHUR_SEQ) = AP.AMPHUR_SEQ
                AND AP.RECORD_STATUS = 'N'
            LEFT JOIN MAS.TB_MAS_TAMBOL TB
                ON NVL(P2.TAMBOL_SEQ,P1.TAMBOL_SEQ) = TB.TAMBOL_SEQ
                AND TB.RECORD_STATUS = 'N'
                ".$parcelLandNo_sql."
            ORDER BY  NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY')";
  //          OFFSET 0 ROWS FETCH NEXT 15 ROWS ONLY";

            $stid = oci_parse($conn, $select);
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($amphur != '') oci_bind_by_name($stid, ':amphur', $amphur);
            if ($tambon != '') oci_bind_by_name($stid, ':tambon', $tambon);
            if ($parcelNo != '') oci_bind_by_name($stid, ':parcelLandNo', $parcelNo);
            oci_bind_by_name($stid, ':typeSeq', $check);
            
            oci_execute($stid);
            break;
        case '5':    //     5 ns3
            $parcelLandNo_sql = $parcelNo == ''? $parcelNo : ' WHERE NVL(P2.PARCEL_LAND_NO,P1.PARCEL_LAND_NO) LIKE :parcelLandNo ';
            $parcel_LandMoo_sql = $parcelMoo == ''? $parcelMoo : " WHERE NVL(P1.PARCEL_LAND_MOO,P2.PARCEL_LAND_MOO) LIKE '%' || :parcel_LandMoo || '%' ";
            
            if ($parcelNo != '' && $parcelMoo != '') $parcel_LandMoo_sql = str_replace("WHERE", "AND", $parcel_LandMoo_sql);

            $select = "WITH P1 AS
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_SEQ, PARCEL_LAND_NO, PRINTPLATE_TYPE_SEQ, PARCEL_LAND_MOO
                FROM MGT1.TB_REG_PARCEL_LAND
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :typeSeq
                ".$amphur_sql
                .$tambon_sql."

            ),
            P2 AS 
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_SEQ, PARCEL_LAND_NO , PRINTPLATE_TYPE_SEQ, PARCEL_LAND_MOO
                FROM REG.TB_REG_PARCEL_LAND
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :typeSeq
                ".$amphur_sql
                .$tambon_sql."
            )
            SELECT NVL(P2.LANDOFFICE_SEQ,P1.LANDOFFICE_SEQ) AS LANDOFFICE_SEQ, AP.AMPHUR_SEQ, AP.AMPHUR_NAME, TB.TAMBOL_SEQ, TB.TAMBOL_NAME, NVL(P1.PARCEL_LAND_MOO,P2.PARCEL_LAND_MOO) AS MOO
                , P1.PARCEL_LAND_SEQ AS PARCEL_LAND_SEQ_P1, P2.PARCEL_LAND_SEQ AS PARCEL_LAND_SEQ_P2, NVL(P2.PARCEL_LAND_NO,P1.PARCEL_LAND_NO) AS PARCEL_LAND_NO
                , CASE WHEN (P2.PARCEL_LAND_SEQ IS NOT NULL AND P1.PARCEL_LAND_SEQ IS NOT NULL) THEN 0
                    WHEN (P2.PARCEL_LAND_SEQ IS NULL) THEN 1
                    WHEN (P1.PARCEL_LAND_SEQ IS NULL) THEN 2 END AS STS
            FROM P1
            FULL OUTER JOIN P2
                ON P1.PARCEL_LAND_SEQ = P2.PARCEL_LAND_SEQ
            LEFT JOIN MAS.TB_MAS_AMPHUR AP
                ON NVL(P2.AMPHUR_SEQ,P1.AMPHUR_SEQ) = AP.AMPHUR_SEQ
                AND AP.RECORD_STATUS = 'N'
            LEFT JOIN MAS.TB_MAS_TAMBOL TB
                ON NVL(P2.TAMBOL_SEQ,P1.TAMBOL_SEQ) = TB.TAMBOL_SEQ
                AND TB.RECORD_STATUS = 'N'
                ".$parcelLandNo_sql
                .$parcel_LandMoo_sql."
            ORDER BY  NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY'), (PARCEL_LAND_NO)";
  //          OFFSET 0 ROWS FETCH NEXT 15 ROWS ONLY";

            $stid = oci_parse($conn, $select);
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($amphur != '') oci_bind_by_name($stid, ':amphur', $amphur);
            if ($tambon != '') oci_bind_by_name($stid, ':tambon', $tambon);
            if ($parcelNo != '') oci_bind_by_name($stid, ':parcelLandNo', $parcelNo);
            if ($parcelMoo != '') oci_bind_by_name($stid, ':parcel_LandMoo', $parcelMoo);
            oci_bind_by_name($stid, ':typeSeq', $check);
            
            oci_execute($stid);
            break;
        case '8':    //     8 nsl
            $parcelLandNo_sql = $parcelNo == ''? $parcelNo : " WHERE NVL(P2.PARCEL_LAND_NO,P1.PARCEL_LAND_NO) LIKE '%' || :parcelLandNo || '%' ";
            $parcelLandYear_sql = $parcelYear == ''? $parcelYear : ' WHERE NVL(P2.PARCEL_LAND_OBTAIN_DTM,P1.PARCEL_LAND_OBTAIN_DTM) LIKE :parcelYear ';

            if ($parcelNo != '' && $parcelYear != '') $parcelLandYear_sql = str_replace("WHERE", "AND", $parcelLandYear_sql);

            $select = "WITH P1 AS
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_SEQ, PARCEL_LAND_NO, PRINTPLATE_TYPE_SEQ, PARCEL_LAND_MOO, PARCEL_LAND_OBTAIN_DTM, PARCEL_LAND_NAME
                FROM MGT1.TB_REG_PARCEL_LAND
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :typeSeq
                ".$amphur_sql
                .$tambon_sql."
            ),
            P2 AS 
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_SEQ, PARCEL_LAND_NO, PRINTPLATE_TYPE_SEQ, PARCEL_LAND_MOO, PARCEL_LAND_OBTAIN_DTM, PARCEL_LAND_NAME
                FROM REG.TB_REG_PARCEL_LAND
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :typeSeq
                ".$amphur_sql
                .$tambon_sql."
            )
            SELECT NVL(P2.LANDOFFICE_SEQ,P1.LANDOFFICE_SEQ) AS LANDOFFICE_SEQ, AP.AMPHUR_SEQ, AP.AMPHUR_NAME, TB.TAMBOL_SEQ, TB.TAMBOL_NAME, NVL(P1.PARCEL_LAND_OBTAIN_DTM,P2.PARCEL_LAND_OBTAIN_DTM) AS YEAR
                , P1.PARCEL_LAND_SEQ AS PARCEL_LAND_SEQ_P1, P2.PARCEL_LAND_SEQ AS PARCEL_LAND_SEQ_P2, NVL(P2.PARCEL_LAND_NO,P1.PARCEL_LAND_NO) AS PARCEL_LAND_NO
                , CASE WHEN (P2.PARCEL_LAND_SEQ IS NOT NULL AND P1.PARCEL_LAND_SEQ IS NOT NULL) THEN 0
                    WHEN (P2.PARCEL_LAND_SEQ IS NULL) THEN 1
                    WHEN (P1.PARCEL_LAND_SEQ IS NULL) THEN 2 END AS STS
                , NVL(P1.PARCEL_LAND_NAME,P2.PARCEL_LAND_NAME) AS PARCEL_LAND_NAME
            FROM P1
            FULL OUTER JOIN P2
                ON P1.PARCEL_LAND_SEQ = P2.PARCEL_LAND_SEQ
            LEFT JOIN MAS.TB_MAS_AMPHUR AP
                ON NVL(P2.AMPHUR_SEQ,P1.AMPHUR_SEQ) = AP.AMPHUR_SEQ
                AND AP.RECORD_STATUS = 'N'
            LEFT JOIN MAS.TB_MAS_TAMBOL TB
                ON NVL(P2.TAMBOL_SEQ,P1.TAMBOL_SEQ) = TB.TAMBOL_SEQ
                AND TB.RECORD_STATUS = 'N'
                ".$parcelLandNo_sql
                .$parcelLandYear_sql."
            ORDER BY  NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY'), PARCEL_LAND_NO ";

            $stid = oci_parse($conn, $select);
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($amphur != '') oci_bind_by_name($stid, ':amphur', $amphur);
            if ($tambon != '') oci_bind_by_name($stid, ':tambon', $tambon);
            if ($parcelNo != '') oci_bind_by_name($stid, ':parcelLandNo', $parcelNo);
            if ($parcelYear != '') oci_bind_by_name($stid, ':parcelYear', $parcelYear);
            oci_bind_by_name($stid, ':typeSeq', $check);
            
            oci_execute($stid);
            break;

        case '23':   //     23 subNsl 
            $parcelLandNo_sql = $parcelNo == ''? $parcelNo : " WHERE NVL(P2.PARCEL_LAND_NO,P1.PARCEL_LAND_NO) LIKE '%' || :parcelLandNo || '%' ";
            $parcel_LandMoo_sql = $parcelMoo == ''? $parcelMoo : " WHERE NVL(P1.PARCEL_LAND_MOO,P2.PARCEL_LAND_MOO) LIKE '%' || :parcel_LandMoo || '%' ";
            
            if ($parcelNo != '' && $parcel_LandMoo != '') $parcel_LandMoo_sql = str_replace("WHERE", "AND", $parcel_LandMoo_sql);

            $select = "WITH P1 AS
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_SEQ, PARCEL_LAND_NO, PRINTPLATE_TYPE_SEQ, PARCEL_LAND_MOO
                FROM MGT1.TB_REG_PARCEL_LAND
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :typeSeq
                ".$amphur_sql
                .$tambon_sql."
            ),
            P2 AS 
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, PARCEL_LAND_SEQ, PARCEL_LAND_NO, PRINTPLATE_TYPE_SEQ, PARCEL_LAND_MOO
                FROM REG.TB_REG_PARCEL_LAND
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :typeSeq
                ".$amphur_sql
                .$tambon_sql."
            )
            SELECT NVL(P2.LANDOFFICE_SEQ,P1.LANDOFFICE_SEQ) AS LANDOFFICE_SEQ, AP.AMPHUR_SEQ, AP.AMPHUR_NAME, TB.TAMBOL_SEQ, TB.TAMBOL_NAME, NVL(P1.PARCEL_LAND_MOO,P2.PARCEL_LAND_MOO) AS MOO
                , P1.PARCEL_LAND_SEQ AS PARCEL_LAND_SEQ_P1, P2.PARCEL_LAND_SEQ AS PARCEL_LAND_SEQ_P2, NVL(P2.PARCEL_LAND_NO,P1.PARCEL_LAND_NO) AS PARCEL_LAND_NO
                , CASE WHEN (P2.PARCEL_LAND_SEQ IS NOT NULL AND P1.PARCEL_LAND_SEQ IS NOT NULL) THEN 0
                    WHEN (P2.PARCEL_LAND_SEQ IS NULL) THEN 1
                    WHEN (P1.PARCEL_LAND_SEQ IS NULL) THEN 2 END AS STS
            FROM P1
            FULL OUTER JOIN P2
                ON P1.PARCEL_LAND_SEQ = P2.PARCEL_LAND_SEQ
            LEFT JOIN MAS.TB_MAS_AMPHUR AP
                ON NVL(P2.AMPHUR_SEQ,P1.AMPHUR_SEQ) = AP.AMPHUR_SEQ
                AND AP.RECORD_STATUS = 'N'
            LEFT JOIN MAS.TB_MAS_TAMBOL TB
                ON NVL(P2.TAMBOL_SEQ,P1.TAMBOL_SEQ) = TB.TAMBOL_SEQ
                AND TB.RECORD_STATUS = 'N'
                ".$parcelLandNo_sql
                .$parcel_LandMoo_sql."
            ORDER BY  NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY'), PARCEL_LAND_NO";
  //          OFFSET 0 ROWS FETCH NEXT 15 ROWS ONLY";

            $stid = oci_parse($conn, $select);
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($amphur != '') oci_bind_by_name($stid, ':amphur', $amphur);
            if ($tambon != '') oci_bind_by_name($stid, ':tambon', $tambon);
            if ($parcelNo != '') oci_bind_by_name($stid, ':parcelLandNo', $parcelNo);
            if ($parcelMoo != '') oci_bind_by_name($stid, ':parcel_LandMoo', $parcelMoo);
            oci_bind_by_name($stid, ':typeSeq', $check);
            
            oci_execute($stid);
            break;
        case '13':     //     13 condo
            $condoNo_sql = $parcelNo == ''? $parcelNo : " WHERE NVL(P2.CONDO_ID || '/' || P2.CONDO_REG_YEAR, P1.CONDO_ID || '/' || P1.CONDO_REG_YEAR) LIKE '%' || :condoNo || '%' ";
            $condoName_sql = $condoName == ''? $condoName : " WHERE NVL(P2.CONDO_NAME_TH,P1.CONDO_NAME_TH) LIKE '%' || :condoName || '%' ";
            
            if ($parcelNo != '' && $condoName != '') $condoName_sql = str_replace("WHERE", "AND", $condoName_sql);
            
            $select = "WITH P1 AS
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, CONDO_ID, CONDO_REG_YEAR, CONDO_NAME_TH, CONDO_SEQ
                FROM MGT1.TB_REG_CONDO
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice
                ".$amphur_sql
                .$tambon_sql."
            ),
            P2 AS
            (
                SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, CONDO_ID, CONDO_REG_YEAR, CONDO_NAME_TH, CONDO_SEQ
                FROM REG.TB_REG_CONDO
                WHERE RECORD_STATUS = 'N' AND LANDOFFICE_SEQ = :landoffice
                ".$amphur_sql
                .$tambon_sql."
            )
            SELECT NVL(P2.LANDOFFICE_SEQ,P1.LANDOFFICE_SEQ) AS LANDOFFICE_SEQ, AP.AMPHUR_SEQ, AP.AMPHUR_NAME, TB.TAMBOL_SEQ, TB.TAMBOL_NAME 
                    , NVL(P2.CONDO_ID || '/' || P2.CONDO_REG_YEAR, P1.CONDO_ID || '/' || P1.CONDO_REG_YEAR) AS CONDO_REG_YEAR
                    , NVL(P2.CONDO_NAME_TH,P1.CONDO_NAME_TH) AS CONDO_NAME_TH
                    , P1.CONDO_SEQ AS CONDO_SEQ_P1, P2.CONDO_SEQ AS CONDO_SEQ_P2
                    , CASE WHEN (P2.CONDO_SEQ IS NOT NULL AND P1.CONDO_SEQ IS NOT NULL) THEN 0
                        WHEN (P2.CONDO_SEQ IS NULL) THEN 1
                        WHEN (P1.CONDO_SEQ IS NULL) THEN 2 END AS STS
            FROM P1
            INNER JOIN P2
                ON P1.CONDO_SEQ = P2.CONDO_SEQ
            LEFT JOIN MAS.TB_MAS_AMPHUR AP
                ON NVL(P2.AMPHUR_SEQ,P1.AMPHUR_SEQ) = AP.AMPHUR_SEQ
                AND AP.RECORD_STATUS = 'N'
            LEFT JOIN MAS.TB_MAS_TAMBOL TB
                ON NVL(P2.TAMBOL_SEQ,P1.TAMBOL_SEQ) = TB.TAMBOL_SEQ
                AND TB.RECORD_STATUS = 'N'
            ".$condoNo_sql
            .$condoName_sql."
            ORDER BY  NLSSORT(AP.AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TB.TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY'), NVL(P1.CONDO_REG_YEAR,P2.CONDO_REG_YEAR), NVL(P1.CONDO_ID,P2.CONDO_ID)";
  //          OFFSET 0 ROWS FETCH NEXT 15 ROWS ONLY";
            
            // $condoNo_sql_oci = "'%".$parcelNo."%'";
            // $condoName_sql_oci = "'%".$condoName."%'";
            $stid = oci_parse($conn, $select);
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($amphur != '') oci_bind_by_name($stid, ':amphur', $amphur);
            if ($tambon != '') oci_bind_by_name($stid, ':tambon', $tambon);
            if ($parcelNo != '')  oci_bind_by_name($stid, ':condoNo', $parcelNo);
            if ($condoName != '')  oci_bind_by_name($stid, ':condoName', $condoName);
            oci_execute($stid);
            break;

        case '9':    //     9 condoroom
            $condoNo_sql = $parcelNo == '' ? $parcelNo : " WHERE NVL(P2.CONDO_ID || '/' || P2.CONDO_REG_YEAR, P1.CONDO_ID || '/' || P1.CONDO_REG_YEAR) LIKE '%' || :condoNo || '%' ";
            $condoName_sql = $condoName == '' ? $condoName : " WHERE NVL(P2.CONDO_NAME_TH,P1.CONDO_NAME_TH) LIKE '%' || :condoName || '%' ";
            $condoroomNo_sql = $condoroomNo == '' ? $condoroomNo : " WHERE NVL(P2.CONDOROOM_RNO,P1.CONDOROOM_RNO) LIKE '%' || :condoroomNo || '%' ";
            
            if ($parcelNo != '' && $condoName != '') $condoName_sql = str_replace("WHERE", "AND", $condoName_sql);
            if (($parcelNo != '' && $condoroomNo != '') || ($condoName != '' && $condoroomNo != '')) $condoroomNo_sql = str_replace("WHERE", "AND", $condoroomNo_sql);
            
            $select = "WITH P1 AS
            (
                SELECT NVL(CR.LANDOFFICE_SEQ,C.LANDOFFICE_SEQ) AS LANDOFFICE_SEQ,
                      AMPHUR_SEQ, TAMBOL_SEQ, CONDOROOM_RNO, CONDO_ID, CONDO_REG_YEAR, CONDO_NAME_TH, CONDOROOM_SEQ
                FROM MGT1.TB_REG_CONDOROOM CR
                LEFT JOIN MGT1.TB_REG_CONDO_BLD CB
                  ON CR.BLD_SEQ = CB.BLD_SEQ
                  AND CB.RECORD_STATUS = 'N'
                LEFT JOIN MGT1.TB_REG_CONDO C
                  ON C.CONDO_SEQ = CB.CONDO_SEQ
                  AND C.RECORD_STATUS = 'N'
                WHERE CR.RECORD_STATUS = 'N' AND NVL(CR.LANDOFFICE_SEQ,C.LANDOFFICE_SEQ) = :landoffice
                ".$amphur_sql
                .$tambon_sql."
            ),
        P2 AS
            (
                SELECT NVL(CR.LANDOFFICE_SEQ,C.LANDOFFICE_SEQ) AS LANDOFFICE_SEQ,
                      AMPHUR_SEQ, TAMBOL_SEQ, CONDOROOM_RNO, CONDO_ID, CONDO_REG_YEAR, CONDO_NAME_TH, CONDOROOM_SEQ
                FROM REG.TB_REG_CONDOROOM CR
                LEFT JOIN REG.TB_REG_CONDO_BLD CB
                  ON CR.BLD_SEQ = CB.BLD_SEQ
                  AND CB.RECORD_STATUS = 'N'
                LEFT JOIN REG.TB_REG_CONDO C
                  ON C.CONDO_SEQ = CB.CONDO_SEQ
                  AND C.RECORD_STATUS = 'N'
                WHERE CR.RECORD_STATUS = 'N' AND NVL(CR.LANDOFFICE_SEQ,C.LANDOFFICE_SEQ) = :landoffice
                ".$amphur_sql
                .$tambon_sql."
            )
            SELECT NVL(P2.LANDOFFICE_SEQ,P1.LANDOFFICE_SEQ) AS LANDOFFICE_SEQ, AP.AMPHUR_SEQ, AP.AMPHUR_NAME, TB.TAMBOL_SEQ, TB.TAMBOL_NAME 
                    , NVL(P2.CONDOROOM_RNO,P1.CONDOROOM_RNO) AS CONDOROOM_RNO
                    , SUBSTR(NVL(P2.CONDOROOM_RNO,P1.CONDOROOM_RNO),0,instr(NVL(P2.CONDOROOM_RNO,P1.CONDOROOM_RNO),'/')-1) AS UNIT
                    , SUBSTR(NVL(P2.CONDOROOM_RNO,P1.CONDOROOM_RNO),instr(NVL(P2.CONDOROOM_RNO,P1.CONDOROOM_RNO),'/')+1) AS NO
                    , NVL(P2.CONDO_ID || '/' || P2.CONDO_REG_YEAR, P1.CONDO_ID || '/' || P1.CONDO_REG_YEAR) AS CONDO_REG_YEAR
                    , NVL(P2.CONDO_NAME_TH,P1.CONDO_NAME_TH) AS CONDO_NAME_TH
                    , P1.CONDOROOM_SEQ AS CONDOROOM_SEQ_P1, P2.CONDOROOM_SEQ AS CONDOROOM_SEQ_P2
                    , CASE WHEN (P2.CONDOROOM_SEQ IS NOT NULL AND P1.CONDOROOM_SEQ IS NOT NULL) THEN 0
                        WHEN (P2.CONDOROOM_SEQ IS NULL) THEN 1
                        WHEN (P1.CONDOROOM_SEQ IS NULL) THEN 2 END AS STS
            FROM P1
            INNER JOIN P2
            ON P1.CONDOROOM_SEQ = P2.CONDOROOM_SEQ
            LEFT JOIN MAS.TB_MAS_AMPHUR AP
                ON NVL(P2.AMPHUR_SEQ,P1.AMPHUR_SEQ) = AP.AMPHUR_SEQ
                AND AP.RECORD_STATUS = 'N'
            LEFT JOIN MAS.TB_MAS_TAMBOL TB
                ON NVL(P2.TAMBOL_SEQ,P1.TAMBOL_SEQ) = TB.TAMBOL_SEQ
                AND TB.RECORD_STATUS = 'N'
            ".$condoNo_sql
            .$condoName_sql
            .$condoroomNo_sql."
            ORDER BY  NLSSORT(AP.AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TB.TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY'),
                      NVL(P1.CONDOROOM_SEQ,P2.CONDOROOM_SEQ),
                      NVL(P1.CONDO_REG_YEAR,P2.CONDO_REG_YEAR), NVL(P1.CONDO_ID,P2.CONDO_ID)";
            
            // $condoNo_sql_oci = "'%".$parcelNo."%'";
            // $condoName_sql_oci = "'%".$condoName."%'";
            // $condoroomNo_sql_oci = "'%".$condoroomNo."%'";
            $stid = oci_parse($conn, $select); 
            oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($amphur != '') oci_bind_by_name($stid, ':amphur', $amphur);
            if ($tambon != '') oci_bind_by_name($stid, ':tambon', $tambon);
            if ($parcelNo != '') oci_bind_by_name($stid, ':condoNo', $parcelNo);
            if ($condoName != '') oci_bind_by_name($stid, ':condoName', $condoName);
            if ($condoroomNo != '') oci_bind_by_name($stid, ':condoroomNo', $condoroomNo);
            oci_execute($stid);
            break;


            case 'c':    //     c construct
                $amphur_con_sql = $amphur == ''? $amphur : ' AND CA.AMPHUR_SEQ = :amphur ';
                $tambon_con_sql = $tambon == ''? $tambon : ' AND CA.TAMBOL_SEQ = :tambon ';

                $constrHid_sql = $constrHid == '' ? $constrHid : " WHERE NVL(P2.CONSTRUCT_ADDR_HID,P1.CONSTRUCT_ADDR_HID) LIKE '%' || :constrHid || '%' ";
                $constrHno_sql = $constrHno == '' ? $constrHno : " WHERE NVL(P2.CONSTRUCT_ADDR_HNO,P1.CONSTRUCT_ADDR_HNO) LIKE '%' || :constrHno || '%' ";
                $constrName_sql = $constrName == '' ? $constrName : " WHERE NVL(P2.CONSTRUCTION_NAME,P1.CONSTRUCTION_NAME) LIKE '%' || :constrName || '%' ";
                $constrMoo_sql = $constrMoo == '' ? $constrMoo : " WHERE NVL(P2.CONSTRUCT_ADDR_MOO,P1.CONSTRUCT_ADDR_MOO)  LIKE '%' || :constrMoo || '%' ";

                //2 textboxs
                if ($constrHid != '' && $constrHno != '' && $constrName == '' && $constrMoo == '') $constrHno_sql = str_replace("WHERE", "AND", $constrHno_sql);
                if ($constrHid != '' && $constrHno == '' && $constrName != '' && $constrMoo == '') $constrName_sql = str_replace("WHERE", "AND", $constrName_sql);
                if ($constrHid != '' && $constrHno == '' && $constrName == '' && $constrMoo != '') $constrMoo_sql = str_replace("WHERE", "AND", $constrMoo_sql);

                if ($constrHid == '' && $constrHno != '' && $constrName != '' && $constrMoo == '') $constrName_sql = str_replace("WHERE", "AND", $constrName_sql);
                if ($constrHid == '' && $constrHno != '' && $constrName == '' && $constrMoo != '') $constrMoo_sql = str_replace("WHERE", "AND", $constrMoo_sql);

                if ($constrHid == '' && $constrHno == '' && $constrName != '' && $constrMoo != '') $constrMoo_sql = str_replace("WHERE", "AND", $constrMoo_sql);
 
                //3 textboxs
                if ($constrHid != '' && $constrHno != '' && $constrName != '' && $constrMoo == '') {
                    $constrHno_sql = str_replace("WHERE", "AND", $constrHno_sql);
                    $constrName_sql = str_replace("WHERE", "AND", $constrName_sql);
                } 
                if ($constrHid != '' && $constrHno != '' && $constrName == '' && $constrMoo != '') {
                    $constrHno_sql = str_replace("WHERE", "AND", $constrHno_sql);
                    $constrMoo_sql = str_replace("WHERE", "AND", $constrMoo_sql);
                } 
                if ($constrHid != '' && $constrHno == '' && $constrName != '' && $constrMoo != '') {
                    $constrName_sql = str_replace("WHERE", "AND", $constrName_sql);
                    $constrMoo_sql = str_replace("WHERE", "AND", $constrMoo_sql);
                } 
                if ($constrHid == '' && $constrHno != '' && $constrName != '' && $constrMoo != '') {
                    $constrName_sql = str_replace("WHERE", "AND", $constrName_sql);
                    $constrMoo_sql = str_replace("WHERE", "AND", $constrMoo_sql);
                } 

                //4 textbox
                if ($constrHid != '' && $constrHno != '' && $constrName != '' && $constrMoo != '') {
                    $constrHno_sql = str_replace("WHERE", "AND", $constrHno_sql);
                    $constrName_sql = str_replace("WHERE", "AND", $constrName_sql);
                    $constrMoo_sql = str_replace("WHERE", "AND", $constrMoo_sql);
                } 

                // if ($constrMoo != '' && $constrHno != '' && $constrHid == '' && $constrName == '') $constrHno_sql = str_replace("WHERE", "AND", $constrHno_sql);
                // if ($constrMoo != '' && $constrHid != '' && $constrHno == '' && $constrName == '') $constrHid_sql = str_replace("WHERE", "AND", $constrHid_sql);
                // if ($constrMoo != '' && $constrName != '' && $constrHid == '' && $constrHno == '') $constrName_sql = str_replace("WHERE", "AND", $constrName_sql);
                // if ($constrHno != '' && $constrHid != '' && $constrMoo == '' && $constrName == '') $constrHid_sql = str_replace("WHERE", "AND", $constrHid_sql);
                // if ($constrHno != '' && $constrName != '' && $constrMoo == '' && $constrHid == '') $constrName_sql = str_replace("WHERE", "AND", $constrName_sql);
                // if ($constrHid_sql != '' && $constrName != '' && $constrMoo == '' && $constrMoo == '') $constrName_sql = str_replace("WHERE", "AND", $constrName_sql);

                // if ($constrMoo != '' && $constrHno != '' && $constrHid != '' && $constrName == ''){
                //     $constrHno_sql = str_replace("WHERE", "AND", $constrHno_sql);
                //     $constrHid_sql  = str_replace("WHERE", "AND", $constrHid_sql);
                // }
                // if ($constrMoo != '' && $constrHno != '' && $constrHid == '' && $constrName != ''){
                //     $constrHno_sql = str_replace("WHERE", "AND", $constrHno_sql);
                //     $constrName_sql  = str_replace("WHERE", "AND", $constrName_sql);
                // }
                // if ($constrMoo != '' && $constrHno == '' && $constrHid != '' && $constrName != ''){
                //     $constrHid_sql = str_replace("WHERE", "AND", $constrHid_sql);
                //     $constrName_sql  = str_replace("WHERE", "AND", $constrName_sql);
                // }
                // if ($constrMoo == '' && $constrHno != '' && $constrHid != '' && $constrName != ''){
                //     $constrHid_sql = str_replace("WHERE", "AND", $constrHid_sql);
                //     $constrName_sql  = str_replace("WHERE", "AND", $constrName_sql);
                // }

                // if ($constrMoo != '' && $constrHno != '' && $constrHid != '' && $constrName != ''){
                //     $constrHno_sql = str_replace("WHERE", "AND", $constrHno_sql);
                //     $constrHid_sql  = str_replace("WHERE", "AND", $constrHid_sql);
                //     $constrName_sql  = str_replace("WHERE", "AND", $constrName_sql);
                // }
                
                $select = "WITH P1 AS
                (
                SELECT P.LANDOFFICE_SEQ, AP.AMPHUR_NAME, CA.AMPHUR_SEQ, TB.TAMBOL_NAME, CA.TAMBOL_SEQ, CA.CONSTRUCT_ADDR_MOO, CA.CONSTRUCT_ADDR_HNO, CA.CONSTRUCT_ADDR_HID, MC.CONSTRUCTION_NAME, C.CONSTRUCT_SEQ
                FROM MGT1.TB_REG_CONSTRUCT C
                INNER JOIN MGT1.TB_REG_PARCEL_CONSTRUCT PC
                  ON C.CONSTRUCT_SEQ = PC.CONSTRUCT_SEQ
                  AND PC.RECORD_STATUS = 'N'
                INNER JOIN MGT1.TB_REG_PARCEL P
                  ON PC.PARCEL_SEQ = P.PARCEL_SEQ
                  AND P.RECORD_STATUS = 'N'
                  AND P.LANDOFFICE_SEQ = :landoffice
                LEFT JOIN MGT1.TB_REG_CONSTRUCT_ADDR CA
                  ON C.CONSTRUCT_ADDR_SEQ = CA.CONSTRUCT_ADDR_SEQ
                  AND CA.RECORD_STATUS = 'N'
                LEFT JOIN MGT1.TB_REG_MAS_CONSTRUCTION MC
                  ON C.CONSTRUCTION_SEQ = MC.CONSTRUCTION_SEQ
                  AND MC.RECORD_STATUS = 'N'
                LEFT JOIN MGT1.TB_MAS_AMPHUR AP
                    ON CA.AMPHUR_SEQ = AP.AMPHUR_SEQ
                LEFT JOIN MGT1.TB_MAS_TAMBOL TB
                    ON CA.TAMBOL_SEQ = TB.TAMBOL_SEQ                              
                WHERE C.RECORD_STATUS = 'N' 
                ".$amphur_con_sql
                .$tambon_con_sql."
                GROUP BY P.LANDOFFICE_SEQ, AP.AMPHUR_NAME, CA.AMPHUR_SEQ, TB.TAMBOL_NAME, CA.TAMBOL_SEQ, CA.CONSTRUCT_ADDR_MOO, CA.CONSTRUCT_ADDR_HNO, CA.CONSTRUCT_ADDR_HID, MC.CONSTRUCTION_NAME, C.CONSTRUCT_SEQ
                UNION ALL
                SELECT P.LANDOFFICE_SEQ, AP.AMPHUR_NAME, CA.AMPHUR_SEQ, TB.TAMBOL_NAME, CA.TAMBOL_SEQ, CA.CONSTRUCT_ADDR_MOO, CA.CONSTRUCT_ADDR_HNO, CA.CONSTRUCT_ADDR_HID, MC.CONSTRUCTION_NAME, C.CONSTRUCT_SEQ
                FROM MGT1.TB_REG_CONSTRUCT C
                INNER JOIN MGT1.TB_REG_PARCEL_LAND_CONSTRUCT PC
                  ON C.CONSTRUCT_SEQ = PC.CONSTRUCT_SEQ
                  AND PC.RECORD_STATUS = 'N'
                INNER JOIN MGT1.TB_REG_PARCEL_LAND P
                  ON PC.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                  AND P.RECORD_STATUS = 'N'
                  AND P.LANDOFFICE_SEQ = :landoffice
                LEFT JOIN MGT1.TB_REG_CONSTRUCT_ADDR CA
                  ON C.CONSTRUCT_ADDR_SEQ = CA.CONSTRUCT_ADDR_SEQ
                  AND CA.RECORD_STATUS = 'N'
                LEFT JOIN MGT1.TB_REG_MAS_CONSTRUCTION MC
                  ON C.CONSTRUCTION_SEQ = MC.CONSTRUCTION_SEQ
                  AND MC.RECORD_STATUS = 'N'
                LEFT JOIN MGT1.TB_MAS_AMPHUR AP
                    ON CA.AMPHUR_SEQ = AP.AMPHUR_SEQ
                LEFT JOIN MGT1.TB_MAS_TAMBOL TB
                    ON CA.TAMBOL_SEQ = TB.TAMBOL_SEQ     
                WHERE C.RECORD_STATUS = 'N'
                ".$amphur_con_sql
                .$tambon_con_sql." 
                GROUP BY P.LANDOFFICE_SEQ, AP.AMPHUR_NAME, CA.AMPHUR_SEQ, TB.TAMBOL_NAME, CA.TAMBOL_SEQ, CA.CONSTRUCT_ADDR_MOO, CA.CONSTRUCT_ADDR_HNO, CA.CONSTRUCT_ADDR_HID, MC.CONSTRUCTION_NAME, C.CONSTRUCT_SEQ
                ),
            P2 AS 
                (
                SELECT P.LANDOFFICE_SEQ, AP.AMPHUR_NAME, CA.AMPHUR_SEQ, TB.TAMBOL_NAME, CA.TAMBOL_SEQ, CA.CONSTRUCT_ADDR_MOO, CA.CONSTRUCT_ADDR_HNO, CA.CONSTRUCT_ADDR_HID, MC.CONSTRUCTION_NAME, C.CONSTRUCT_SEQ
                FROM REG.TB_REG_CONSTRUCT C
                INNER JOIN REG.TB_REG_PARCEL_CONSTRUCT PC
                  ON C.CONSTRUCT_SEQ = PC.CONSTRUCT_SEQ
                  AND PC.RECORD_STATUS = 'N'
                INNER JOIN REG.TB_REG_PARCEL P
                  ON PC.PARCEL_SEQ = P.PARCEL_SEQ
                  AND P.RECORD_STATUS = 'N'
                  AND P.LANDOFFICE_SEQ = :landoffice
                LEFT JOIN REG.TB_REG_CONSTRUCT_ADDR CA
                  ON C.CONSTRUCT_ADDR_SEQ = CA.CONSTRUCT_ADDR_SEQ
                  AND CA.RECORD_STATUS = 'N'
                LEFT JOIN REG.TB_REG_MAS_CONSTRUCTION MC
                  ON C.CONSTRUCTION_SEQ = MC.CONSTRUCTION_SEQ
                  AND MC.RECORD_STATUS = 'N'
                LEFT JOIN MAS.TB_MAS_AMPHUR AP
                    ON CA.AMPHUR_SEQ = AP.AMPHUR_SEQ
                LEFT JOIN MAS.TB_MAS_TAMBOL TB
                    ON CA.TAMBOL_SEQ = TB.TAMBOL_SEQ
                WHERE C.RECORD_STATUS = 'N' 
                ".$amphur_con_sql
                .$tambon_con_sql."
                GROUP BY P.LANDOFFICE_SEQ, AP.AMPHUR_NAME, CA.AMPHUR_SEQ, TB.TAMBOL_NAME, CA.TAMBOL_SEQ, CA.CONSTRUCT_ADDR_MOO, CA.CONSTRUCT_ADDR_HNO, CA.CONSTRUCT_ADDR_HID, MC.CONSTRUCTION_NAME, C.CONSTRUCT_SEQ
                UNION ALL
                SELECT P.LANDOFFICE_SEQ, AP.AMPHUR_NAME, CA.AMPHUR_SEQ, TB.TAMBOL_NAME, CA.TAMBOL_SEQ, CA.CONSTRUCT_ADDR_MOO, CA.CONSTRUCT_ADDR_HNO, CA.CONSTRUCT_ADDR_HID, MC.CONSTRUCTION_NAME, C.CONSTRUCT_SEQ
                FROM REG.TB_REG_CONSTRUCT C
                INNER JOIN REG.TB_REG_PARCEL_LAND_CONSTRUCT PC
                  ON C.CONSTRUCT_SEQ = PC.CONSTRUCT_SEQ
                  AND PC.RECORD_STATUS = 'N'
                INNER JOIN REG.TB_REG_PARCEL_LAND P
                  ON PC.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                  AND P.RECORD_STATUS = 'N'
                  AND P.LANDOFFICE_SEQ = :landoffice
                LEFT JOIN REG.TB_REG_CONSTRUCT_ADDR CA
                  ON C.CONSTRUCT_ADDR_SEQ = CA.CONSTRUCT_ADDR_SEQ
                  AND CA.RECORD_STATUS = 'N'
                LEFT JOIN REG.TB_REG_MAS_CONSTRUCTION MC
                  ON C.CONSTRUCTION_SEQ = MC.CONSTRUCTION_SEQ
                  AND MC.RECORD_STATUS = 'N'
                LEFT JOIN MAS.TB_MAS_AMPHUR AP
                    ON CA.AMPHUR_SEQ = AP.AMPHUR_SEQ
                LEFT JOIN MAS.TB_MAS_TAMBOL TB
                    ON CA.TAMBOL_SEQ = TB.TAMBOL_SEQ
                WHERE C.RECORD_STATUS = 'N'
                ".$amphur_con_sql
                .$tambon_con_sql." 
                GROUP BY P.LANDOFFICE_SEQ, AP.AMPHUR_NAME, CA.AMPHUR_SEQ, TB.TAMBOL_NAME, CA.TAMBOL_SEQ, CA.CONSTRUCT_ADDR_MOO, CA.CONSTRUCT_ADDR_HNO, CA.CONSTRUCT_ADDR_HID, MC.CONSTRUCTION_NAME, C.CONSTRUCT_SEQ
                )
                SELECT NVL(P2.LANDOFFICE_SEQ,P1.LANDOFFICE_SEQ) AS LANDOFFICE_SEQ
                    , NVL(P1.AMPHUR_SEQ,P2.AMPHUR_SEQ) AS AMPHUR_SEQ, NVL(P1.AMPHUR_NAME,P2.AMPHUR_NAME) AS AMPHUR_NAME
                    , NVL(P1.TAMBOL_SEQ,P2.TAMBOL_SEQ) AS TAMBOL_SEQ, NVL(P1.TAMBOL_NAME,P2.TAMBOL_NAME) AS TAMBOL_NAME
                    , P1.CONSTRUCT_SEQ AS CONSTRUCT_SEQ_P1, P2.CONSTRUCT_SEQ AS CONSTRUCT_SEQ_P2
                    , NVL(P2.CONSTRUCT_ADDR_MOO,P1.CONSTRUCT_ADDR_MOO) AS CONSTRUCT_ADDR_MOO
                    , NVL(P2.CONSTRUCT_ADDR_HNO,P1.CONSTRUCT_ADDR_HNO) AS CONSTRUCT_ADDR_HNO
                    , NVL(P2.CONSTRUCT_ADDR_HID,P1.CONSTRUCT_ADDR_HID) AS CONSTRUCT_ADDR_HID
                    , NVL(P2.CONSTRUCTION_NAME,P1.CONSTRUCTION_NAME) AS CONSTRUCTION_NAME
                    , CASE WHEN (P2.CONSTRUCT_SEQ IS NOT NULL AND P1.CONSTRUCT_SEQ IS NOT NULL) THEN 0
                        WHEN (P2.CONSTRUCT_SEQ IS NULL) THEN 1
                        WHEN (P1.CONSTRUCT_SEQ IS NULL) THEN 2 END AS STS
                FROM P1
                INNER JOIN P2
                    ON P1.CONSTRUCT_SEQ = P2.CONSTRUCT_SEQ
                ".$constrHid_sql
                .$constrHno_sql
                .$constrName_sql
                .$constrMoo_sql."
                ORDER BY NLSSORT(NVL(P1.AMPHUR_NAME,P2.AMPHUR_NAME), 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(NVL(P1.TAMBOL_NAME,P2.TAMBOL_NAME), 'NLS_SORT=THAI_DICTIONARY') ";
                // echo $select;
                
                $stid = oci_parse($conn, $select); 
                oci_bind_by_name($stid, ':landoffice', $landoffice);
                if ($amphur != '') oci_bind_by_name($stid, ':amphur', $amphur);
                if ($tambon != '') oci_bind_by_name($stid, ':tambon', $tambon);
                if ($constrMoo != '') oci_bind_by_name($stid, ':constrMoo', $constrMoo);
                if ($constrHno != '') oci_bind_by_name($stid, ':constrHno', $constrHno);
                if ($constrHid != '') oci_bind_by_name($stid, ':constrHid', $constrHid);
                if ($constrName != '') oci_bind_by_name($stid, ':constrName', $constrName);
                oci_execute($stid);
                break;
            }
?>