<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice = $_POST['landoffice'];

    $select = "with recv as (
        select mp.printplate_type_seq,printplate_type_name,temp.count from reg.tb_reg_mas_printplate_type mp
        left join (
          select printplate_type_seq, count(parcel_seq) as count from mgt1.tb_reg_parcel
          where landoffice_seq = :landoffice and record_status = 'N' and printplate_type_seq = 1
          group by printplate_type_seq
          union
          select  printplate_type_seq, count(parcel_land_seq) as count from mgt1.tb_reg_parcel_land
          where landoffice_seq = :landoffice and record_status = 'N' and printplate_type_seq <> 1
          group by printplate_type_seq
          union
          select  13, count(condo_seq) as count from mgt1.tb_reg_condo
          where landoffice_seq = :landoffice and record_status = 'N'
          union
          select  10, count(condoroom_seq) as count from mgt1.tb_reg_condoroom
          where landoffice_seq = :landoffice and record_status = 'N'
          union
          select 11, sum(constr.count)from
            (select count(con.construct_seq) as count from mgt1.tb_reg_construct con
              inner join mgt1.tb_reg_parcel_construct pcon
                on pcon.construct_seq = con.construct_seq
                and pcon.record_status = 'N'
              inner join mgt1.tb_reg_parcel par
                on par.parcel_seq = pcon.parcel_seq
                and par.record_status = 'N'
            where par.landoffice_seq = :landoffice and con.record_status = 'N'
            union
            select count(distinct con.construct_seq) as count from reg.tb_reg_construct con
              inner join reg.tb_reg_parcel_land_construct pcon
                on pcon.construct_seq = con.construct_seq
                and pcon.record_status = 'N'
              inner join reg.tb_reg_parcel_land par
                on par.parcel_land_seq = pcon.parcel_land_seq
                and par.record_status = 'N'
            where par.landoffice_seq = :landoffice and con.record_status = 'N'
            ) constr
        ) temp
          on mp.printplate_type_seq = temp.printplate_type_seq
        where  mp.record_status = 'N' and mp.printplate_type_seq > 0
        order by mp.printplate_type_seq),
        ok as
        (select mp.printplate_type_seq, printplate_type_name,temp.count from reg.tb_reg_mas_printplate_type mp
        left join (
          select p2.printplate_type_seq, count(p2.parcel_seq) as count from reg.tb_reg_parcel p2
          inner join mgt1.tb_reg_parcel p1
            on p2.parcel_seq = p1.parcel_seq
          where p2.landoffice_seq = :landoffice and p2.record_status = 'N' and p2.printplate_type_seq = 1
          group by p2.printplate_type_seq
          union
          select  p2.printplate_type_seq, count(p2.parcel_land_seq) as count from reg.tb_reg_parcel_land p2
          inner join mgt1.tb_reg_parcel_land p1
            on p2.parcel_land_seq = p1.parcel_land_seq
          where p2.landoffice_seq = :landoffice and p2.record_status = 'N' and p2.printplate_type_seq <> 1
          group by p2.printplate_type_seq
          union
          select  13, count(p2.condo_seq) as count from reg.tb_reg_condo p2
          inner join mgt1.tb_reg_condo p1
            on p2.condo_seq = p1.condo_seq
          where p2.landoffice_seq = :landoffice and p2.record_status = 'N'
          union
          select  10, count(p2.condoroom_seq) as count from reg.tb_reg_condoroom p2
          inner join mgt1.tb_reg_condoroom p1
            on p2.condoroom_seq = p1.condoroom_seq
          where p2.landoffice_seq = :landoffice and p2.record_status = 'N'
          union
          select 11, sum(constr.count) from
            (select count(conp2.construct_seq) as count from reg.tb_reg_construct conp2
              inner join mgt1.tb_reg_construct p1
                on conp2.construct_seq = p1.construct_seq
              inner join reg.tb_reg_parcel_construct pcon
                on pcon.construct_seq = conp2.construct_seq
                and pcon.record_status = 'N'
              inner join reg.tb_reg_parcel par
                on par.parcel_seq = pcon.parcel_seq
                and par.record_status = 'N'
            where par.landoffice_seq = :landoffice and conp2.record_status = 'N'
            union
            select count(distinct conp2.construct_seq) as count from reg.tb_reg_construct conp2
              inner join mgt1.tb_reg_construct p1
                on conp2.construct_seq = p1.construct_seq
              inner join reg.tb_reg_parcel_land_construct pcon
                on pcon.construct_seq = conp2.construct_seq
                and pcon.record_status = 'N'
              inner join reg.tb_reg_parcel_land par
                on par.parcel_land_seq = pcon.parcel_land_seq
                and par.record_status = 'N'
            where par.landoffice_seq = :landoffice and conp2.record_status = 'N'
            ) constr
        ) temp
          on mp.printplate_type_seq = temp.printplate_type_seq
        where  mp.record_status = 'N' and mp.printplate_type_seq > 0
        order by mp.printplate_type_seq)
        select recv.printplate_type_seq, recv.printplate_type_name, nvl(recv.count,0) as RECEIVE, nvl(ok.count,0) as MIGRATE_SUCCESS, (nvl(recv.count,0)-nvl(ok.count,0)) as MIGRATE_ERROR from recv,ok
        where recv.printplate_type_seq = ok.printplate_type_seq
          and recv.printplate_type_seq in (1,2,17,3,5,4,8,23,13,10,9,7,6,11,15,16,18,19,20,21,22,28,12,14)
        order by (case printplate_type_seq 
                        when 1 then 1 when 2 then 2 when 17 then 3 when 3 then 4
                        when 5 then 5 when 4 then 6 when 8 then 7 when 23 then 8
                        when 13 then 9 when 10 then 10 when 9 then 11 when 7 then 12
                        when 6 then 13 when 11 then 14 when 15 then 15 when 16 then 16
                        when 18 then 17 when 19 then 18 when 20 then 19 when 21 then 20
                        when 22 then 21 when 28 then 22 when 12 then 23 when 14 then 24 end)";
                
    $stid = oci_parse($conn, $select);
    oci_bind_by_name($stid, ':landoffice', $landoffice);

    oci_execute($stid);

    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);

?>