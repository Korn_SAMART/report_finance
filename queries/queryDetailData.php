<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';
    
switch ($check) {
    case '1':   //     1 chanode
        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND P.PARCEL_SEQ IS NULL ';
        }else{
            $dataSeq1_sql = ' AND P.PARCEL_SEQ = :dataSeqP1 ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND P.PARCEL_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND P.PARCEL_SEQ = :dataSeqP2 ' ;
        }
        $select = "WITH P1 AS(
                        SELECT P.PARCEL_NO, TB.TAMBOL_NAME, AP.AMPHUR_NAME, AP.AMPHUR_SEQ, PARCEL_SURVEY_NO, PARCEL_BOOK, PARCEL_PAGE
                            ,NVL(PARCEL_RAI_NUM,0)||'-'||NVL(PARCEL_NGAN_NUM,0)||'-'||NVL(PARCEL_WA_NUM,0)||'.'||NVL(PARCEL_SUBWA_NUM,0) AS AREA
                            ,UTMSCALE_SEQ, TRIM(PARCEL_UTMMAP1)||'-'||TRIM(TO_CHAR(PARCEL_UTMMAP2,'RN'))||'-'||PARCEL_UTMMAP3||'-'||PARCEL_UTMMAP4 AS UTM, PARCEL_UTM_LAND_NO
                            ,ORIGINSCALE_SEQ, TRIM(PARCEL_ORIGINMAP1)||'-'||TRIM(TO_CHAR(PARCEL_ORIGINMAP2))||'-'||PARCEL_ORIGINMAP3||'-'||PARCEL_ORIGINMAP4 AS ORIGIN, PARCEL_ORIGIN_LAND_NO
                            ,TITLE_NAME||OWNER_FNAME||' '||OWNER_LNAME AS OWN, PARCEL_OWNER_NUM
                            ,PARCEL_PROCESS_DTM, PARCEL_REGIST_NAME
                            ,PARCEL_OPT_FLAG, OPT_NAME
                            ,CASE TO_NUMBER(SM.SEQUESTER_STS) WHEN 2 THEN 2 WHEN 1 THEN 3 ELSE 1 END AS PARCEL_SEQUEST_STS
                            ,PARCEL_OBLIGATION_NUM
                            ,PARCEL_LANDUSED_DESC, SM.CREATE_DTM
                        FROM MGT1.TB_REG_PARCEL P
                        LEFT JOIN MGT1.TB_MAS_AMPHUR AP
                            ON AP.AMPHUR_SEQ = P.AMPHUR_SEQ
                        LEFT JOIN MGT1.TB_MAS_TAMBOL TB
                            ON TB.TAMBOL_SEQ = P.TAMBOL_SEQ
                        LEFT JOIN MGT1.TB_REG_PARCEL_OPT POPT
                            ON POPT.PARCEL_SEQ = P.PARCEL_SEQ
                            AND POPT.RECORD_STATUS = 'N'
                        LEFT JOIN MGT1.TB_MAS_OPT OPT
                            ON OPT.OPT_SEQ = POPT.OPT_SEQ
                        LEFT JOIN MGT1.TB_REG_PARCEL_OWNER POWN
                            ON POWN.PARCEL_SEQ = P.PARCEL_SEQ
                            AND POWN.RECORD_STATUS = 'N'
                        LEFT JOIN MGT1.TB_REG_OWNER OWN
                            ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        LEFT JOIN MGT1.TB_MAS_TITLE TT
                            ON OWN.OWNER_TITLE_SEQ = TT.TITLE_SEQ
                        LEFT JOIN MGT1.TB_REG_SEQUESTER_MAPPING SM
                            ON SM.PARCEL_SEQ = P.PARCEL_SEQ
                            AND SM.PRINTPLATE_TYPE_SEQ = P.PRINTPLATE_TYPE_SEQ
                        LEFT JOIN MGT1.TB_REG_PARCEL_LANDUSED PLU
                            ON PLU.PARCEL_SEQ = P.PARCEL_SEQ
                            AND PLU.RECORD_STATUS = 'N'
                        WHERE PARCEL_OWNER_ORDER = 1 ".$dataSeq1_sql."
                        ),
                    P2 AS(
                        SELECT P.PARCEL_NO, TB.TAMBOL_NAME, AP.AMPHUR_NAME, AP.AMPHUR_SEQ, PARCEL_SURVEY_NO, PARCEL_BOOK, PARCEL_PAGE
                            ,NVL(PARCEL_RAI_NUM,0)||'-'||NVL(PARCEL_NGAN_NUM,0)||'-'||NVL(PARCEL_WA_NUM,0)||'.'||NVL(PARCEL_SUBWA_NUM,0) AS AREA
                            ,UTMSCALE_SEQ, TRIM(PARCEL_UTMMAP1)||'-'||TRIM(TO_CHAR(PARCEL_UTMMAP2,'RN'))||'-'||PARCEL_UTMMAP3||'-'||PARCEL_UTMMAP4 AS UTM, PARCEL_UTM_LAND_NO
                            ,ORIGINSCALE_SEQ, TRIM(PARCEL_ORIGINMAP1)||'-'||TRIM(TO_CHAR(PARCEL_ORIGINMAP2))||'-'||PARCEL_ORIGINMAP3||'-'||PARCEL_ORIGINMAP4 AS ORIGIN, PARCEL_ORIGIN_LAND_NO
                            ,TITLE_NAME||OWNER_FNAME||' '||OWNER_LNAME AS OWN, PARCEL_OWNER_NUM
                            ,PARCEL_PROCESS_DTM, PARCEL_REGIST_NAME
                            ,PARCEL_OPT_FLAG, OPT_NAME
                            ,CASE TO_NUMBER(SM.SEQUESTER_STS) WHEN 2 THEN 2 WHEN 1 THEN 3 ELSE 1 END AS PARCEL_SEQUEST_STS
                            ,PARCEL_OBLIGATION_NUM
                            ,PARCEL_LANDUSED_DESC, SM.CREATE_DTM
                        FROM REG.TB_REG_PARCEL P
                        LEFT JOIN MAS.TB_MAS_AMPHUR AP
                            ON AP.AMPHUR_SEQ = P.AMPHUR_SEQ
                        LEFT JOIN MAS.TB_MAS_TAMBOL TB
                            ON TB.TAMBOL_SEQ = P.TAMBOL_SEQ
                        LEFT JOIN REG.TB_REG_PARCEL_OPT POPT
                            ON POPT.PARCEL_SEQ = P.PARCEL_SEQ
                            AND POPT.RECORD_STATUS = 'N'
                        LEFT JOIN MAS.TB_MAS_OPT OPT
                            ON OPT.OPT_SEQ = POPT.OPT_SEQ
                        LEFT JOIN REG.TB_REG_PARCEL_OWNER POWN
                            ON POWN.PARCEL_SEQ = P.PARCEL_SEQ
                            AND POWN.RECORD_STATUS = 'N'
                        LEFT JOIN REG.TB_REG_OWNER OWN
                            ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        LEFT JOIN MAS.TB_MAS_TITLE TT
                            ON OWN.OWNER_TITLE_SEQ = TT.TITLE_SEQ
                        LEFT JOIN REG.TB_REG_SEQUESTER_MAPPING SM
                            ON SM.PARCEL_SEQ = P.PARCEL_SEQ
                            AND SM.PRINTPLATE_TYPE_SEQ = P.PRINTPLATE_TYPE_SEQ
                        LEFT JOIN REG.TB_REG_PARCEL_LANDUSED PLU
                            ON PLU.PARCEL_SEQ = P.PARCEL_SEQ
                            AND PLU.RECORD_STATUS = 'N'
                        WHERE PARCEL_OWNER_ORDER = 1 ".$dataSeq2_sql."
                        )
                    SELECT  P1.PARCEL_NO AS PARCEL_NO_P1, P2.PARCEL_NO AS PARCEL_NO_P2,
                            P1.TAMBOL_NAME AS TAMBOL_NAME_P1, P2.TAMBOL_NAME AS TAMBOL_NAME_P2,
                            P1.AMPHUR_NAME AS AMPHUR_NAME_P1, P2.AMPHUR_NAME AS AMPHUR_NAME_P2,
                            P1.PARCEL_SURVEY_NO AS PARCEL_SURVEY_NO_P1, P2.PARCEL_SURVEY_NO AS PARCEL_SURVEY_NO_P2,
                            P1.PARCEL_BOOK AS PARCEL_BOOK_P1, P2.PARCEL_BOOK AS PARCEL_BOOK_P2,
                            P1.PARCEL_PAGE AS PARCEL_PAGE_P1, P2.PARCEL_PAGE AS PARCEL_PAGE_P2,
                            P1.AREA AS AREA_P1, P2.AREA AS AREA_P2, 
                            P1.UTMSCALE_SEQ AS UTMSCALE_SEQ_P1, P2.UTMSCALE_SEQ AS UTMSCALE_SEQ_P2, 
                            P1.UTM AS UTM_P1, P2.UTM AS UTM_P2,
                            P1.PARCEL_UTM_LAND_NO AS PARCEL_UTM_LAND_NO_P1, P2.PARCEL_UTM_LAND_NO AS PARCEL_UTM_LAND_NO_P2,
                            P1.ORIGINSCALE_SEQ AS ORIGINSCALE_SEQ_P1, P2.ORIGINSCALE_SEQ AS ORIGINSCALE_SEQ_P2,
                            P1.ORIGIN AS ORIGIN_P1, P2.ORIGIN AS ORIGIN_P2,
                            P1.PARCEL_ORIGIN_LAND_NO AS PARCEL_ORIGIN_LAND_NO_P1, P2.PARCEL_ORIGIN_LAND_NO AS PARCEL_ORIGIN_LAND_NO_P2,
                            P1.OWN AS OWN_P1, P2.OWN AS OWN_P2,
                            P1.PARCEL_OWNER_NUM AS PARCEL_OWNER_NUM_P1, P2.PARCEL_OWNER_NUM AS PARCEL_OWNER_NUM_P2,
                            P1.PARCEL_PROCESS_DTM AS PARCEL_PROCESS_DTM_P1, P2.PARCEL_PROCESS_DTM AS PARCEL_PROCESS_DTM_P2,
                            P1.PARCEL_REGIST_NAME AS PARCEL_REGIST_NAME_P1, P2.PARCEL_REGIST_NAME AS PARCEL_REGIST_NAME_P2,
                            P1.PARCEL_OPT_FLAG AS PARCEL_OPT_FLAG_P1, P2.PARCEL_OPT_FLAG AS PARCEL_OPT_FLAG_P2,
                            P1.OPT_NAME AS OPT_NAME_P1, P2.OPT_NAME AS OPT_NAME_P2,
                            P1.PARCEL_SEQUEST_STS AS PARCEL_SEQUEST_STS_P1, P2.PARCEL_SEQUEST_STS AS PARCEL_SEQUEST_STS_P2,
                            P1.PARCEL_OBLIGATION_NUM AS PARCEL_OBLIGATION_NUM_P1, P2.PARCEL_OBLIGATION_NUM AS PARCEL_OBLIGATION_NUM_P2,
                            P1.PARCEL_LANDUSED_DESC AS PARCEL_LANDUSED_DESC_P1, P2.PARCEL_LANDUSED_DESC AS PARCEL_LANDUSED_DESC_P2 
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PARCEL_NO = P2.PARCEL_NO
                        AND P1.AMPHUR_SEQ = P2.AMPHUR_SEQ 
                        ORDER BY P1.CREATE_DTM DESC, P2.CREATE_DTM DESC";

            $stid = oci_parse($conn, $select);
            if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
            if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
            oci_execute($stid);
            break;
            
    case '2':    //     2 chanodeTrajong
    case '3':    //     3 trajong
    case '4':    //     4 ns3a
    case '5':    //     5 ns3
    case '8':    //     8 nsl
    case '23':   //     23 subNsl  
        //$dataSeq_sql = $dataSeq == ''? $dataSeq : ' AND P.PARCEL_LAND_SEQ = :dataSeq ';
        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND P.PARCEL_LAND_SEQ IS NULL ';
        }else{
            $dataSeq1_sql = ' AND P.PARCEL_LAND_SEQ = :dataSeqP1 ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND P.PARCEL_LAND_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND P.PARCEL_LAND_SEQ = :dataSeqP2 ' ;
        }
        $typeSeq_sql = $check == ''? $check : ' AND P.PRINTPLATE_TYPE_SEQ = :typeSeq ';

        $select = "WITH P1 AS(
                        SELECT P.PARCEL_LAND_NO, PRINTPLATE_TYPE_NAME, TB.TAMBOL_SEQ, TB.TAMBOL_NAME, AP.AMPHUR_SEQ, AP.AMPHUR_NAME, PARCEL_LAND_MOO, PARCEL_LAND_SURVEY_NO, PARCEL_LAND_OBTAIN_DTM, PARCEL_LAND_BOOK, PARCEL_LAND_PAGE
                            ,NVL(PARCEL_LAND_RAI_NUM,0)||'-'||NVL(PARCEL_LAND_NGAN_NUM,0)||'-'||NVL(PARCEL_LAND_WA_NUM,0)||'.'||NVL(PARCEL_LAND_SUBWA_NUM,0) AS AREA
                            ,UTMSCALE_SEQ, TRIM(PARCEL_LAND_UTMMAP1)||'-'||TRIM(TO_CHAR(PARCEL_LAND_UTMMAP2,'RN'))||'-'||PARCEL_LAND_UTMMAP3||'-'||PARCEL_LAND_UTMMAP4 AS UTM, PARCEL_LAND_UTM_LAND_NO
                            ,ORIGINSCALE_SEQ, TRIM(PARCEL_LAND_ORIGINMAP1)||'-'||TRIM(TO_CHAR(PARCEL_LAND_ORIGINMAP2))||'-'||PARCEL_LAND_ORIGINMAP3||'-'||PARCEL_LAND_ORIGINMAP4 AS ORIGIN, PARCEL_LAND_ORIGIN_LAND_NO
                            ,TITLE_NAME||OWNER_FNAME||' '||OWNER_LNAME AS OWN, PARCEL_LAND_OWNER_NUM
                            ,PARCEL_LAND_PROCESS_DTM, PARCEL_LAND_REGIST_NAME
                            ,PARCEL_LAND_OPT_FLAG, OPT_NAME
                            ,CASE TO_NUMBER(SM.SEQUESTER_STS) WHEN 2 THEN 2 WHEN 1 THEN 3 ELSE 1 END AS PARCEL_LAND_SEQUEST_STS
                            ,PARCEL_LAND_OBLIGATION_NUM
                            ,PARCEL_LAND_LANDUSED_DESC, SM.CREATE_DTM
                        FROM MGT1.TB_REG_PARCEL_LAND P
                        INNER JOIN MAS.TB_MAS_PRINTPLATE_TYPE MP
                            ON MP.PRINTPLATE_TYPE_SEQ = P.PRINTPLATE_TYPE_SEQ
                        LEFT JOIN MGT1.TB_MAS_AMPHUR AP
                            ON AP.AMPHUR_SEQ = P.AMPHUR_SEQ
                        LEFT JOIN MGT1.TB_MAS_TAMBOL TB
                            ON TB.TAMBOL_SEQ = P.TAMBOL_SEQ
                        LEFT JOIN MGT1.TB_REG_PARCEL_LAND_OPT POPT
                            ON POPT.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                            AND POPT.RECORD_STATUS = 'N'
                        LEFT JOIN MGT1.TB_MAS_OPT OPT
                            ON OPT.OPT_SEQ = POPT.OPT_SEQ
                        LEFT JOIN MGT1.TB_REG_PARCEL_LAND_OWNER POWN
                            ON POWN.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                            AND POWN.RECORD_STATUS = 'N'
                        LEFT JOIN MGT1.TB_REG_OWNER OWN
                            ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        LEFT JOIN MGT1.TB_MAS_TITLE TT
                            ON OWN.OWNER_TITLE_SEQ = TT.TITLE_SEQ
                        LEFT JOIN MGT1.TB_REG_SEQUESTER_MAPPING SM
                            ON SM.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                            AND SM.PRINTPLATE_TYPE_SEQ = P.PRINTPLATE_TYPE_SEQ
                        LEFT JOIN MGT1.TB_REG_PARCEL_LAND_LANDUSED PLU
                            ON PLU.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                            AND PLU.RECORD_STATUS = 'N'
                        WHERE NVL(PARCEL_LAND_OWNER_ORDER,1) = 1 
                            ".$typeSeq_sql
                             .$dataSeq1_sql."
                        ),
                    P2 AS(
                        SELECT P.PARCEL_LAND_NO, PRINTPLATE_TYPE_NAME, TB.TAMBOL_SEQ, TB.TAMBOL_NAME, AP.AMPHUR_NAME, AP.AMPHUR_SEQ, PARCEL_LAND_MOO, PARCEL_LAND_SURVEY_NO, PARCEL_LAND_OBTAIN_DTM, PARCEL_LAND_BOOK, PARCEL_LAND_PAGE
                            ,NVL(PARCEL_LAND_RAI_NUM,0)||'-'||NVL(PARCEL_LAND_NGAN_NUM,0)||'-'||NVL(PARCEL_LAND_WA_NUM,0)||'.'||NVL(PARCEL_LAND_SUBWA_NUM,0) AS AREA
                            ,UTMSCALE_SEQ, TRIM(PARCEL_LAND_UTMMAP1)||'-'||TRIM(TO_CHAR(PARCEL_LAND_UTMMAP2,'RN'))||'-'||PARCEL_LAND_UTMMAP3||'-'||PARCEL_LAND_UTMMAP4 AS UTM, PARCEL_LAND_UTM_LAND_NO
                            ,ORIGINSCALE_SEQ, TRIM(PARCEL_LAND_ORIGINMAP1)||'-'||TRIM(TO_CHAR(PARCEL_LAND_ORIGINMAP2))||'-'||PARCEL_LAND_ORIGINMAP3||'-'||PARCEL_LAND_ORIGINMAP4 AS ORIGIN, PARCEL_LAND_ORIGIN_LAND_NO
                            ,TITLE_NAME||OWNER_FNAME||' '||OWNER_LNAME AS OWN, PARCEL_LAND_OWNER_NUM
                            ,PARCEL_LAND_PROCESS_DTM, PARCEL_LAND_REGIST_NAME
                            ,PARCEL_LAND_OPT_FLAG, OPT_NAME
                            ,CASE TO_NUMBER(SM.SEQUESTER_STS) WHEN 2 THEN 2 WHEN 1 THEN 3 ELSE 1 END AS PARCEL_LAND_SEQUEST_STS
                            ,PARCEL_LAND_OBLIGATION_NUM
                            ,PARCEL_LAND_LANDUSED_DESC, SM.CREATE_DTM
                        FROM REG.TB_REG_PARCEL_LAND P
                        INNER JOIN MAS.TB_MAS_PRINTPLATE_TYPE MP
                            ON MP.PRINTPLATE_TYPE_SEQ = P.PRINTPLATE_TYPE_SEQ
                        LEFT JOIN MAS.TB_MAS_AMPHUR AP
                            ON AP.AMPHUR_SEQ = P.AMPHUR_SEQ
                        LEFT JOIN MAS.TB_MAS_TAMBOL TB
                            ON TB.TAMBOL_SEQ = P.TAMBOL_SEQ
                        LEFT JOIN REG.TB_REG_PARCEL_LAND_OPT POPT
                            ON POPT.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                            AND POPT.RECORD_STATUS = 'N'
                        LEFT JOIN MAS.TB_MAS_OPT OPT
                            ON OPT.OPT_SEQ = POPT.OPT_SEQ
                        LEFT JOIN REG.TB_REG_PARCEL_LAND_OWNER POWN
                            ON POWN.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                            AND POWN.RECORD_STATUS = 'N'
                        LEFT JOIN REG.TB_REG_OWNER OWN
                            ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        LEFT JOIN MAS.TB_MAS_TITLE TT
                            ON OWN.OWNER_TITLE_SEQ = TT.TITLE_SEQ
                        LEFT JOIN REG.TB_REG_SEQUESTER_MAPPING SM
                            ON SM.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                            AND SM.PRINTPLATE_TYPE_SEQ = P.PRINTPLATE_TYPE_SEQ
                        LEFT JOIN REG.TB_REG_PARCEL_LAND_LANDUSED PLU
                            ON PLU.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                            AND PLU.RECORD_STATUS = 'N'
                        WHERE NVL(PARCEL_LAND_OWNER_ORDER,1) = 1 
                            ".$typeSeq_sql
                            .$dataSeq2_sql."
                        )
                    SELECT  P1.PARCEL_LAND_NO AS PARCEL_LAND_NO_P1, P2.PARCEL_LAND_NO AS PARCEL_LAND_NO_P2, 
                            P1.PRINTPLATE_TYPE_NAME AS PRINTPLATE_TYPE_NAME_P1, P2.PRINTPLATE_TYPE_NAME AS PRINTPLATE_TYPE_NAME_P2,
                            P1.TAMBOL_NAME AS TAMBOL_NAME_P1, P2.TAMBOL_NAME AS TAMBOL_NAME_P2, 
                            P1.AMPHUR_NAME AS AMPHUR_NAME_P1, P2.AMPHUR_NAME AS AMPHUR_NAME_P2,
                            P1.PARCEL_LAND_MOO AS PARCEL_LAND_MOO_P1, P2.PARCEL_LAND_MOO AS PARCEL_LAND_MOO_P2,
                            P1.PARCEL_LAND_SURVEY_NO AS PARCEL_LAND_SURVEY_NO_P1, P2.PARCEL_LAND_SURVEY_NO AS PARCEL_LAND_SURVEY_NO_P2,
                            P1.PARCEL_LAND_OBTAIN_DTM AS PARCEL_LAND_OBTAIN_DTM_P1, P2.PARCEL_LAND_OBTAIN_DTM AS PARCEL_LAND_OBTAIN_DTM_P2,
                            P1.PARCEL_LAND_BOOK AS PARCEL_LAND_BOOK_P1, P2.PARCEL_LAND_BOOK AS PARCEL_LAND_BOOK_P2,
                            P1.PARCEL_LAND_PAGE AS PARCEL_LAND_PAGE_P1, P2.PARCEL_LAND_PAGE AS PARCEL_LAND_PAGE_P2, 
                            P1.AREA AS AREA_P1, P2.AREA AS AREA_P2,
                            P1.UTMSCALE_SEQ AS UTMSCALE_SEQ_P1, P2.UTMSCALE_SEQ AS UTMSCALE_SEQ_P2, 
                            P1.UTM AS UTM_P1, P2.UTM AS UTM_P2,
                            P1.PARCEL_LAND_UTM_LAND_NO AS PARCEL_LAND_UTM_LAND_NO_P1, P2.PARCEL_LAND_UTM_LAND_NO AS PARCEL_LAND_UTM_LAND_NO_P2,
                            P1.ORIGIN AS ORIGIN_P1, P2.ORIGIN AS ORIGIN_P2,
                            P1.ORIGINSCALE_SEQ AS ORIGINSCALE_SEQ_P1, P2.ORIGINSCALE_SEQ AS ORIGINSCALE_SEQ_P2,
                            P1.PARCEL_LAND_ORIGIN_LAND_NO AS PARCEL_LAND_ORIGIN_LAND_NO_P1, P2.PARCEL_LAND_ORIGIN_LAND_NO AS PARCEL_LAND_ORIGIN_LAND_NO_P2,
                            P1.OWN AS OWN_P1, P2.OWN AS OWN_P2,
                            P1.PARCEL_LAND_OWNER_NUM AS PARCEL_LAND_OWNER_NUM_P1, P2.PARCEL_LAND_OWNER_NUM AS PARCEL_LAND_OWNER_NUM_P2,
                            P1.PARCEL_LAND_PROCESS_DTM AS PARCEL_LAND_PROCESS_DTM_P1, P2.PARCEL_LAND_PROCESS_DTM AS PARCEL_LAND_PROCESS_DTM_P2,
                            P1.PARCEL_LAND_REGIST_NAME AS PARCEL_LAND_REGIST_NAME_P1, P2.PARCEL_LAND_REGIST_NAME AS PARCEL_LAND_REGIST_NAME_P2,
                            P1.PARCEL_LAND_OPT_FLAG AS PARCEL_LAND_OPT_FLAG_P1, P2.PARCEL_LAND_OPT_FLAG AS PARCEL_LAND_OPT_FLAG_P2,
                            P1.OPT_NAME AS OPT_NAME_P1, P2.OPT_NAME AS OPT_NAME_P2,
                            P1.PARCEL_LAND_SEQUEST_STS AS PARCEL_LAND_SEQUEST_STS_P1, P2.PARCEL_LAND_SEQUEST_STS AS PARCEL_LAND_SEQUEST_STS_P2,
                            P1.PARCEL_LAND_OBLIGATION_NUM AS PARCEL_LAND_OBLIGATION_NUM_P1, P2.PARCEL_LAND_OBLIGATION_NUM AS PARCEL_LAND_OBLIGATION_NUM_P2,
                            P1.PARCEL_LAND_LANDUSED_DESC AS PARCEL_LAND_LANDUSED_DESC_P1, P2.PARCEL_LAND_LANDUSED_DESC AS PARCEL_LAND_LANDUSED_P2        
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PARCEL_LAND_NO = P2.PARCEL_LAND_NO
                        AND P1.AMPHUR_SEQ = P2.AMPHUR_SEQ 
                    ORDER BY P1.CREATE_DTM DESC, P2.CREATE_DTM DESC";

            $stid = oci_parse($conn, $select);
            if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
            if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
            if ($check != '') oci_bind_by_name($stid, ':typeSeq', $check);
            oci_execute($stid);
            break;

    case '13':     //     13 condo
        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND CD.CONDO_SEQ IS NULL ';
        }else{
            $dataSeq1_sql = ' AND CD.CONDO_SEQ = :dataSeqP1 ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND CD.CONDO_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND CD.CONDO_SEQ = :dataSeqP2 ' ;
        }
        $select = "WITH CP1 AS (
            SELECT COUNT(*) FROM MGT1.TB_REG_CONDOROOM CDR
            INNER JOIN MGT1.TB_REG_CONDO_BLD BLD
                ON BLD.BLD_SEQ = CDR.BLD_SEQ
                AND BLD.RECORD_STATUS = 'N'
            INNER JOIN MGT1.TB_REG_CONDO CD
                ON CD.CONDO_SEQ = BLD.CONDO_SEQ
                AND CD.RECORD_STATUS = 'N'
            WHERE 1=1 ".$dataSeq1_sql."
        ),
        CP2 AS (
            SELECT COUNT(*) FROM REG.TB_REG_CONDOROOM CDR
            INNER JOIN REG.TB_REG_CONDO_BLD BLD
                ON BLD.BLD_SEQ = CDR.BLD_SEQ
                AND BLD.RECORD_STATUS = 'N'
            INNER JOIN REG.TB_REG_CONDO CD
                ON CD.CONDO_SEQ = BLD.CONDO_SEQ
                AND CD.RECORD_STATUS = 'N'
            WHERE 1=1 ".$dataSeq2_sql."
        ), 
        P1 AS(
                        SELECT CD.CONDO_SEQ, CONDO_ID||'/'||CONDO_REG_YEAR AS CONDO_ID, CONDO_NAME_TH, AP.AMPHUR_NAME,AP.AMPHUR_SEQ, TB.TAMBOL_NAME
                            ,CONDO_BLD, CONDO_ROOM_NUM||'('||(SELECT * FROM CP1)||')' as CONDO_ROOM_NUM, CONDO_RECV_DATE, CONDO_REGISTER_DATE
                            ,CONDO_CORP_ID||'/'||CONDO_CORP_REG_YEAR AS CORP_ID, CONDO_CORP_NAME, CONDO_CORP_REGDTM
                            ,NVL(CONDO_CORP_ADDR,'-')||' หมู่ '||NVL(CONDO_CORP_MOO,'-')||' ซอย'||NVL(CONDO_CORP_SOI,'-')||' ถนน'||NVL(CONDO_CORP_ROAD,'-')||' '||NVL(TB2.TAMBOL_NAME,'-')||' '||NVL(AP2.AMPHUR_NAME,'-')||' '||NVL(PROVINCE_NAME,'-') AS ADDR
                        FROM MGT1.TB_REG_CONDO CD
                        LEFT JOIN MGT1.TB_MAS_AMPHUR AP
                            ON AP.AMPHUR_SEQ = CD.AMPHUR_SEQ
                        LEFT JOIN MGT1.TB_MAS_TAMBOL TB
                            ON TB.TAMBOL_SEQ = CD.TAMBOL_SEQ
                        LEFT JOIN MGT1.TB_REG_CONDO_CORPORATE CDC
                            ON CDC.CONDO_SEQ = CD.CONDO_SEQ
                            AND CDC.RECORD_STATUS = 'N'
                        LEFT JOIN MGT1.TB_MAS_AMPHUR AP2
                            ON AP2.AMPHUR_SEQ = CDC.AMPHUR_SEQ
                        LEFT JOIN MGT1.TB_MAS_TAMBOL TB2
                            ON TB2.TAMBOL_SEQ = CDC.TAMBOL_SEQ
                        LEFT JOIN MGT1.TB_MAS_PROVINCE PV
                            ON PV.PROVINCE_SEQ = CDC.PROVINCE_SEQ
                        WHERE 1=1 ".$dataSeq1_sql."
                        ),
                    P2 AS(
                        SELECT CD.CONDO_SEQ, CONDO_ID||'/'||CONDO_REG_YEAR AS CONDO_ID, CONDO_NAME_TH, AP.AMPHUR_NAME, AP.AMPHUR_SEQ, TB.TAMBOL_NAME
                            ,CONDO_BLD, CONDO_ROOM_NUM||'('||(SELECT * FROM CP2)||')' as CONDO_ROOM_NUM, CONDO_RECV_DATE, CONDO_REGISTER_DATE
                            ,CONDO_CORP_ID||'/'||CONDO_CORP_REG_YEAR AS CORP_ID, CONDO_CORP_NAME, CONDO_CORP_REGDTM
                            ,NVL(CONDO_CORP_ADDR,'-')||' หมู่ '||NVL(CONDO_CORP_MOO,'-')||' ซอย'||NVL(CONDO_CORP_SOI,'-')||' ถนน'||NVL(CONDO_CORP_ROAD,'-')||' '||NVL(TB2.TAMBOL_NAME,'-')||' '||NVL(AP2.AMPHUR_NAME,'-')||' '||NVL(PROVINCE_NAME,'-') AS ADDR
                        FROM REG.TB_REG_CONDO CD
                        LEFT JOIN MAS.TB_MAS_AMPHUR AP
                            ON AP.AMPHUR_SEQ = CD.AMPHUR_SEQ
                        LEFT JOIN MAS.TB_MAS_TAMBOL TB
                            ON TB.TAMBOL_SEQ = CD.TAMBOL_SEQ
                        LEFT JOIN REG.TB_REG_CONDO_CORPORATE CDC
                            ON CDC.CONDO_SEQ = CD.CONDO_SEQ
                            AND CDC.RECORD_STATUS = 'N'
                        LEFT JOIN MAS.TB_MAS_AMPHUR AP2
                            ON AP2.AMPHUR_SEQ = CDC.AMPHUR_SEQ
                        LEFT JOIN MAS.TB_MAS_TAMBOL TB2
                            ON TB2.TAMBOL_SEQ = CDC.TAMBOL_SEQ
                        LEFT JOIN MAS.TB_MAS_PROVINCE PV
                            ON PV.PROVINCE_SEQ = CDC.PROVINCE_SEQ
                        WHERE 1=1 ".$dataSeq2_sql." 
                        )
                    SELECT  P1.CONDO_ID AS CONDO_ID_P1, P2.CONDO_ID AS CONDO_ID_P2,
                            P1.CONDO_NAME_TH  AS  CONDO_NAME_TH_P1, P2.CONDO_NAME_TH  AS CONDO_NAME_TH_P2,
                            P1.AMPHUR_NAME  AS  AMPHUR_NAME_P1, P2.AMPHUR_NAME  AS AMPHUR_NAME_P2,
                            P1.TAMBOL_NAME  AS  TAMBOL_NAME_P1, P2.TAMBOL_NAME  AS TAMBOL_NAME_P2,
                            P1.CONDO_BLD  AS  CONDO_BLD_P1, P2.CONDO_BLD  AS CONDO_BLD_P2,
                            P1.CONDO_ROOM_NUM  AS  CONDO_ROOM_NUM_P1, P2.CONDO_ROOM_NUM  AS CONDO_ROOM_NUM_P2,
                            CASE WHEN SUBSTR(P1.CONDO_REGISTER_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P1.CONDO_REGISTER_DATE) ELSE TO_CHAR(P1.CONDO_REGISTER_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CONDO_RECV_DATE_P1,
                            CASE WHEN SUBSTR(P2.CONDO_REGISTER_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P2.CONDO_REGISTER_DATE) ELSE TO_CHAR(P2.CONDO_REGISTER_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CONDO_RECV_DATE_P2,

                            CASE WHEN SUBSTR(P1.CONDO_REGISTER_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P1.CONDO_REGISTER_DATE) ELSE TO_CHAR(P1.CONDO_REGISTER_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CONDO_REGISTER_DATE_P1,
                            CASE WHEN SUBSTR(P2.CONDO_REGISTER_DATE, -4, 4) > 2500 
                            THEN TO_CHAR(P2.CONDO_REGISTER_DATE) ELSE TO_CHAR(P2.CONDO_REGISTER_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CONDO_REGISTER_DATE_P2,

                            P1.CORP_ID  AS  CORP_ID_P1, P2.CORP_ID  AS CORP_ID_P2,
                            P1.CONDO_CORP_NAME  AS  CONDO_CORP_NAME_P1, P2.CONDO_CORP_NAME  AS CONDO_CORP_NAME_P2,

                            CASE WHEN SUBSTR(P1.CONDO_CORP_REGDTM, -4, 4) > 2500 
                            THEN TO_CHAR(P1.CONDO_CORP_REGDTM) ELSE TO_CHAR(P1.CONDO_CORP_REGDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CONDO_CORP_REGDTM_P1,
                            CASE WHEN SUBSTR(P2.CONDO_CORP_REGDTM, -4, 4) > 2500 
                            THEN TO_CHAR(P2.CONDO_CORP_REGDTM) ELSE TO_CHAR(P2.CONDO_CORP_REGDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CONDO_CORP_REGDTM_P2,

                            P1.ADDR  AS  ADDR_P1, P2.ADDR  AS ADDR_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.CONDO_ID = P2.CONDO_ID
                        AND P1.CONDO_SEQ = P2.CONDO_SEQ";

            $stid = oci_parse($conn, $select);
            if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
            if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
            oci_execute($stid);
            break;

    case '9':    //     9 condoroom
        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND CDR.CONDOROOM_SEQ IS NULL ';
        }else{
            $dataSeq1_sql = ' AND CDR.CONDOROOM_SEQ = :dataSeqP1 ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND CDR.CONDOROOM_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND CDR.CONDOROOM_SEQ = :dataSeqP2 ' ;
        }
        $select = "WITH P1 AS(
            SELECT CDR.CONDOROOM_RNO, BLD_NAME_TH, CONDO_ID||'/'||CONDO_REG_YEAR AS CONDO_ID, CONDO_NAME_TH
                ,AP.AMPHUR_NAME, TB.TAMBOL_NAME
                ,CONDOROOM_AREA_NUM
                ,TITLE_NAME||OWNER_FNAME||' '||OWNER_LNAME AS OWN,CONDOROOM_OWNER_NUM
                ,CONDOROOM_PROCESS_DTM, CONDOROOM_REGIST_NAME
                ,CONDOROOM_OPT_FLAG, OPT_NAME
                ,CASE TO_NUMBER(SM.SEQUESTER_STS) WHEN 2 THEN 2 WHEN 1 THEN 3 ELSE 1 END AS CONDOROOM_SEQUEST_STS
                ,CONDOROOM_OBLIGATION_NUM
                ,CONDOROOM_LANDUSED_DESC, SM.CREATE_DTM
            FROM MGT1.TB_REG_CONDOROOM CDR
            INNER JOIN MGT1.TB_REG_CONDO_BLD BLD
                ON BLD.BLD_SEQ = CDR.BLD_SEQ
                AND BLD.RECORD_STATUS = 'N'
            INNER JOIN MGT1.TB_REG_CONDO CD
                ON CD.CONDO_SEQ = BLD.CONDO_SEQ
                AND CD.RECORD_STATUS = 'N'
            LEFT JOIN MGT1.TB_MAS_AMPHUR AP
                ON AP.AMPHUR_SEQ = CD.AMPHUR_SEQ
            LEFT JOIN MGT1.TB_MAS_TAMBOL TB
                ON TB.TAMBOL_SEQ = CD.TAMBOL_SEQ
            LEFT JOIN MGT1.TB_REG_CONDOROOM_OWNER CDROWN
                ON CDROWN.CONDOROOM_SEQ = CDR.CONDOROOM_SEQ
                AND CDROWN.RECORD_STATUS = 'N'
            LEFT JOIN MGT1.TB_REG_OWNER OWN
                ON OWN.OWNER_SEQ = CDROWN.OWNER_SEQ
            LEFT JOIN MGT1.TB_MAS_TITLE TT
                ON OWN.OWNER_TITLE_SEQ = TT.TITLE_SEQ
            LEFT JOIN MGT1.TB_REG_CONDOROOM_OPT CDROPT
                ON CDROPT.CONDOROOM_SEQ = CDR.CONDOROOM_SEQ
                AND CDROPT.RECORD_STATUS = 'N'
            LEFT JOIN MGT1.TB_MAS_OPT OPT
                ON OPT.OPT_SEQ = CDROPT.OPT_SEQ
            LEFT JOIN MGT1.TB_REG_SEQUESTER_MAPPING SM
                ON SM.CONDOROOM_SEQ = CDR.CONDOROOM_SEQ
                AND SM.PRINTPLATE_TYPE_SEQ = CDR.PRINTPLATE_TYPE_SEQ
            LEFT JOIN MGT1.TB_REG_CONDOROOM_LANDUSED CLU
                ON CLU.CONDOROOM_SEQ = CDR.CONDOROOM_SEQ
                AND CLU.RECORD_STATUS = 'N'
            WHERE CONDOROOM_OWNER_ORDER_NUM = 1 ".$dataSeq1_sql."
            ),
        P2 AS(
            SELECT CDR.CONDOROOM_RNO, BLD_NAME_TH, CONDO_ID||'/'||CONDO_REG_YEAR AS CONDO_ID, CONDO_NAME_TH
                ,AP.AMPHUR_NAME, TB.TAMBOL_NAME
                ,CONDOROOM_AREA_NUM
                ,TITLE_NAME||OWNER_FNAME||' '||OWNER_LNAME AS OWN,CONDOROOM_OWNER_NUM
                ,CONDOROOM_PROCESS_DTM, CONDOROOM_REGIST_NAME
                ,CONDOROOM_OPT_FLAG, OPT_NAME
                ,CASE TO_NUMBER(SM.SEQUESTER_STS) WHEN 2 THEN 2 WHEN 1 THEN 3 ELSE 1 END AS CONDOROOM_SEQUEST_STS
                ,CONDOROOM_OBLIGATION_NUM
                ,CONDOROOM_LANDUSED_DESC, SM.CREATE_DTM
            FROM REG.TB_REG_CONDOROOM CDR
            INNER JOIN REG.TB_REG_CONDO_BLD BLD
                ON BLD.BLD_SEQ = CDR.BLD_SEQ
                AND BLD.RECORD_STATUS = 'N'
            INNER JOIN REG.TB_REG_CONDO CD
                ON CD.CONDO_SEQ = BLD.CONDO_SEQ
                AND CD.RECORD_STATUS = 'N'
            LEFT JOIN MAS.TB_MAS_AMPHUR AP
                ON AP.AMPHUR_SEQ = CD.AMPHUR_SEQ
            LEFT JOIN MAS.TB_MAS_TAMBOL TB
                ON TB.TAMBOL_SEQ = CD.TAMBOL_SEQ
            LEFT JOIN REG.TB_REG_CONDOROOM_OWNER CDROWN
                ON CDROWN.CONDOROOM_SEQ = CDR.CONDOROOM_SEQ
                AND CDROWN.RECORD_STATUS = 'N'
            LEFT JOIN REG.TB_REG_OWNER OWN
                ON OWN.OWNER_SEQ = CDROWN.OWNER_SEQ
            LEFT JOIN MAS.TB_MAS_TITLE TT
                ON OWN.OWNER_TITLE_SEQ = TT.TITLE_SEQ
            LEFT JOIN REG.TB_REG_CONDOROOM_OPT CDROPT
                ON CDROPT.CONDOROOM_SEQ = CDR.CONDOROOM_SEQ
                AND CDROPT.RECORD_STATUS = 'N'
            LEFT JOIN MAS.TB_MAS_OPT OPT
                ON OPT.OPT_SEQ = CDROPT.OPT_SEQ
            LEFT JOIN REG.TB_REG_SEQUESTER_MAPPING SM
                ON SM.CONDOROOM_SEQ = CDR.CONDOROOM_SEQ
                AND SM.PRINTPLATE_TYPE_SEQ = CDR.PRINTPLATE_TYPE_SEQ
            LEFT JOIN REG.TB_REG_CONDOROOM_LANDUSED CLU
                ON CLU.CONDOROOM_SEQ = CDR.CONDOROOM_SEQ
                AND CLU.RECORD_STATUS = 'N'
            WHERE CONDOROOM_OWNER_ORDER_NUM= 1 ".$dataSeq2_sql."
            )
            SELECT  P1.CONDOROOM_RNO AS CONDOROOM_RNO_P1, P2.CONDOROOM_RNO AS CONDOROOM_RNO_P2,
                    P1.BLD_NAME_TH AS BLD_NAME_TH_P1, P2.BLD_NAME_TH AS BLD_NAME_TH_P2,
                    P1.CONDO_ID AS CONDO_ID_P1, P2.CONDO_ID AS CONDO_ID_P2,
                    P1.CONDO_NAME_TH AS CONDO_NAME_TH_P1, P2.CONDO_NAME_TH AS CONDO_NAME_TH_P2,
                    P1.AMPHUR_NAME AS AMPHUR_NAME_P1, P2.AMPHUR_NAME AS AMPHUR_NAME_P2,
                    P1.TAMBOL_NAME AS TAMBOL_NAME_P1, P2.TAMBOL_NAME AS TAMBOL_NAME_P2,
                    P1.CONDOROOM_AREA_NUM AS CONDOROOM_AREA_NUM_P1, P2.CONDOROOM_AREA_NUM AS CONDOROOM_AREA_NUM_P2,
                    P1.OWN AS OWN_P1, P2.OWN AS OWN_P2,
                    P1.CONDOROOM_OWNER_NUM AS CONDOROOM_OWNER_NUM_P1, P2.CONDOROOM_OWNER_NUM AS CONDOROOM_OWNER_NUM_P2,
                    CASE WHEN SUBSTR(P1.CONDOROOM_PROCESS_DTM, -4, 4) > 2500 
                    THEN TO_CHAR(P1.CONDOROOM_PROCESS_DTM) ELSE TO_CHAR(P1.CONDOROOM_PROCESS_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CONDOROOM_PROCESS_DTM_P1,
                    CASE WHEN SUBSTR(P2.CONDOROOM_PROCESS_DTM, -4, 4) > 2500 
                    THEN TO_CHAR(P2.CONDOROOM_PROCESS_DTM) ELSE TO_CHAR(P2.CONDOROOM_PROCESS_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CONDOROOM_PROCESS_DTM_P2,
                    P1.CONDOROOM_REGIST_NAME AS CONDOROOM_REGIST_NAME_P1, P2.CONDOROOM_REGIST_NAME AS CONDOROOM_REGIST_NAME_P2,
                    P1.CONDOROOM_OPT_FLAG AS CONDOROOM_OPT_FLAG_P1, P2.CONDOROOM_OPT_FLAG AS CONDOROOM_OPT_FLAG_P2,
                    P1.OPT_NAME AS OPT_NAME_P1, P2.OPT_NAME AS OPT_NAME_P2,
                    P1.CONDOROOM_SEQUEST_STS AS CONDOROOM_SEQUEST_STS_P1, P2.CONDOROOM_SEQUEST_STS AS CONDOROOM_SEQUEST_STS_P2,
                    P1.CONDOROOM_OBLIGATION_NUM AS CONDOROOM_OBLIGATION_NUM_P1, P2.CONDOROOM_OBLIGATION_NUM AS CONDOROOM_OBLIGATION_NUM_P2,
                    P1.CONDOROOM_LANDUSED_DESC AS CONDOROOM_LANDUESD_DESC_P1, P2.CONDOROOM_LANDUSED_DESC AS CONDOROOM_LANDUESD_DESC_P2
            FROM P1
            FULL OUTER JOIN P2
                ON P1.CONDOROOM_RNO = P2.CONDOROOM_RNO
                AND P1.CONDO_ID = P2.CONDO_ID 
            ORDER BY P1.CREATE_DTM DESC, P2.CREATE_DTM DESC";
       

            $stid = oci_parse($conn, $select);
            if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
            if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
            oci_execute($stid);
            break;

    case 'c':    //     c construct
        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND PC.CONSTRUCT_SEQ IS NULL ';
        }else{
            $dataSeq1_sql = ' AND PC.CONSTRUCT_SEQ = :dataSeqP1 ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND PC.CONSTRUCT_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND PC.CONSTRUCT_SEQ = :dataSeqP2 ' ;
        }
        $select = "WITH PP1 AS  (
            SELECT 
            ADDR1.CONSTRUCT_ADDR_HID AS CONSTRUCT_ADDR_HID_P1
            ,NVL(ADDR1.CONSTRUCT_ADDR_HNO,'-')||' หมู่ '||NVL(ADDR1.CONSTRUCT_ADDR_MOO,'-')||' ซอย'||NVL(ADDR1.CONSTRUCT_ADDR_SOI,'-')
            ||' ถนน'||NVL(ADDR1.CONSTRUCT_ADDR_ROAD,'-')||' '||NVL(TB1.TAMBOL_NAME,'-')||' '||NVL(AP1.AMPHUR_NAME,'-')||' '||NVL(OWN1.PROVINCE_NAME,'-') AS ADDR_P1
            ,STR1.CONSTRUCT_AREA_NUM AS CONSTRUCT_AREA_NUM_P1
            ,TT1.TITLE_NAME||OWN1.OWNER_FNAME||' '||OWN1.OWNER_LNAME AS OWN_P1
            ,MP1.PRINTPLATE_TYPE_NAME AS PRINTPLATE_TYPE_NAME_P1
            ,P1.PARCEL_NO AS PARCEL_NO_P1
            ,TB12.TAMBOL_NAME AS TAMBOL_NAME_P1
            ,AP12.AMPHUR_NAME AS AMPHUR_NAME_P1
            ,STR1.CONSTRUCT_PROCESS_DTM AS CONSTRUCT_PROCESS_DTM_P1
            ,STR1.CONSTRUCT_REGIST_NAME AS CONSTRUCT_REGIST_NAME_P1
            ,CASE TO_NUMBER(SM.SEQUESTER_STS) WHEN 2 THEN 2 WHEN 1 THEN 3 ELSE 1 END AS CONSTRUCT_SEQUEST_STS_P1
            FROM(
                SELECT PRINTPLATE_TYPE_SEQ, PARCEL_NO, TAMBOL_SEQ, AMPHUR_SEQ, CONSTRUCT_SEQ
                FROM MGT1.TB_REG_PARCEL_CONSTRUCT PC
                INNER JOIN MGT1.TB_REG_PARCEL P
                    ON P.PARCEL_SEQ = PC.PARCEL_SEQ
                    AND P.RECORD_STATUS = 'N'
                WHERE PC.RECORD_STATUS = 'N' ".$dataSeq1_sql."
                UNION
                SELECT PRINTPLATE_TYPE_SEQ, PARCEL_LAND_NO, TAMBOL_SEQ, AMPHUR_SEQ, CONSTRUCT_SEQ
                FROM MGT1.TB_REG_PARCEL_LAND_CONSTRUCT PC
                INNER JOIN MGT1.TB_REG_PARCEL_LAND P
                    ON P.PARCEL_LAND_SEQ = PC.PARCEL_LAND_SEQ
                    AND P.RECORD_STATUS = 'N'
                WHERE PC.RECORD_STATUS = 'N' ".$dataSeq1_sql."
            ) P1
            INNER JOIN MGT1.TB_REG_CONSTRUCT STR1
                ON P1.CONSTRUCT_SEQ = STR1.CONSTRUCT_SEQ
            INNER JOIN MAS.TB_MAS_PRINTPLATE_TYPE MP1
                ON MP1.PRINTPLATE_TYPE_SEQ = NVL(P1.PRINTPLATE_TYPE_SEQ,0)
            LEFT JOIN MGT1.TB_MAS_AMPHUR AP12
                ON AP12.AMPHUR_SEQ = NVL(P1.AMPHUR_SEQ,0)
            LEFT JOIN MGT1.TB_MAS_TAMBOL TB12
                ON TB12.TAMBOL_SEQ = NVL(P1.TAMBOL_SEQ,0)    
            LEFT JOIN MGT1.TB_REG_CONSTRUCT_ADDR ADDR1
                ON ADDR1.CONSTRUCT_ADDR_SEQ = STR1.CONSTRUCT_ADDR_SEQ   
                AND ADDR1.RECORD_STATUS = 'N'                  
            LEFT JOIN MGT1.TB_MAS_AMPHUR AP1
                ON AP1.AMPHUR_SEQ = ADDR1.AMPHUR_SEQ
            LEFT JOIN MGT1.TB_MAS_TAMBOL TB1
                ON TB1.TAMBOL_SEQ = ADDR1.TAMBOL_SEQ              
            LEFT JOIN REG.TB_REG_CONSTRUCT_OWNER STROWN1
                ON STROWN1.CONSTRUCT_SEQ = STR1.CONSTRUCT_SEQ
                AND STROWN1.RECORD_STATUS = 'N'    
            LEFT JOIN MGT1.TB_REG_OWNER OWN1
                ON OWN1.OWNER_SEQ = STROWN1.OWNER_SEQ
            LEFT JOIN MGT1.TB_MAS_TITLE TT1
                ON OWN1.OWNER_TITLE_SEQ = TT1.TITLE_SEQ
            LEFT JOIN MGT1.TB_REG_SEQUESTER_MAPPING SM
                ON SM.CONSTRUCT_SEQ = STR1.CONSTRUCT_SEQ
                AND SM.PRINTPLATE_TYPE_SEQ = STR1.PRINTPLATE_TYPE_SEQ
            WHERE CONSTRUCT_OWNER_ORDER = 1
        ),
    PP2 AS (
            SELECT 
            ADDR2.CONSTRUCT_ADDR_HID AS CONSTRUCT_ADDR_HID_P2
            ,NVL(ADDR2.CONSTRUCT_ADDR_HNO,'-')||' หมู่ '||NVL(ADDR2.CONSTRUCT_ADDR_MOO,'-')||' ซอย'||NVL(ADDR2.CONSTRUCT_ADDR_SOI,'-')
            ||' ถนน'||NVL(ADDR2.CONSTRUCT_ADDR_ROAD,'-')||' '||NVL(TB2.TAMBOL_NAME,'-')||' '||NVL(AP2.AMPHUR_NAME,'-')||' '||NVL(OWN2.PROVINCE_NAME,'-') AS ADDR_P2
            ,STR2.CONSTRUCT_AREA_NUM AS CONSTRUCT_AREA_NUM_P2
            ,TT2.TITLE_NAME||OWN2.OWNER_FNAME||' '||OWN2.OWNER_LNAME AS OWN_P2
            ,MP2.PRINTPLATE_TYPE_NAME AS PRINTPLATE_TYPE_NAME_P2
            ,P2.PARCEL_NO AS PARCEL_NO_P2
            ,TB22.TAMBOL_NAME AS TAMBOL_NAME_P2
            ,AP22.AMPHUR_NAME AS AMPHUR_NAME_P2
            ,STR2.CONSTRUCT_PROCESS_DTM AS CONSTRUCT_PROCESS_DTM_P2
            ,STR2.CONSTRUCT_REGIST_NAME AS CONSTRUCT_REGIST_NAME_P2
            ,CASE TO_NUMBER(SM.SEQUESTER_STS) WHEN 2 THEN 2 WHEN 1 THEN 3 ELSE 1 END AS CONSTRUCT_SEQUEST_STS_P2
            FROM(
            SELECT PRINTPLATE_TYPE_SEQ, PARCEL_NO, TAMBOL_SEQ, AMPHUR_SEQ, CONSTRUCT_SEQ
            FROM REG.TB_REG_PARCEL_CONSTRUCT PC
            INNER JOIN REG.TB_REG_PARCEL P
                ON P.PARCEL_SEQ = PC.PARCEL_SEQ
                AND P.RECORD_STATUS = 'N'
            WHERE PC.RECORD_STATUS = 'N' ".$dataSeq2_sql."
            UNION
            SELECT PRINTPLATE_TYPE_SEQ, PARCEL_LAND_NO, TAMBOL_SEQ, AMPHUR_SEQ, CONSTRUCT_SEQ
            FROM REG.TB_REG_PARCEL_LAND_CONSTRUCT PC
            INNER JOIN REG.TB_REG_PARCEL_LAND P
                ON P.PARCEL_LAND_SEQ = PC.PARCEL_LAND_SEQ
                AND P.RECORD_STATUS = 'N'
            WHERE PC.RECORD_STATUS = 'N' ".$dataSeq2_sql."
        ) P2
        INNER JOIN REG.TB_REG_CONSTRUCT STR2
            ON P2.CONSTRUCT_SEQ = STR2.CONSTRUCT_SEQ
        INNER JOIN MAS.TB_MAS_PRINTPLATE_TYPE MP2
            ON MP2.PRINTPLATE_TYPE_SEQ = NVL(P2.PRINTPLATE_TYPE_SEQ,0) 
        LEFT JOIN MAS.TB_MAS_AMPHUR AP22
            ON AP22.AMPHUR_SEQ = NVL(P2.AMPHUR_SEQ,0)
        LEFT JOIN MAS.TB_MAS_TAMBOL TB22
            ON TB22.TAMBOL_SEQ = NVL(P2.TAMBOL_SEQ,0)
        LEFT JOIN REG.TB_REG_CONSTRUCT_ADDR ADDR2
            ON ADDR2.CONSTRUCT_ADDR_SEQ = STR2.CONSTRUCT_ADDR_SEQ
            AND ADDR2.RECORD_STATUS = 'N'                   
        LEFT JOIN MAS.TB_MAS_AMPHUR AP2
            ON AP2.AMPHUR_SEQ = ADDR2.AMPHUR_SEQ
        LEFT JOIN MAS.TB_MAS_TAMBOL TB2
            ON TB2.TAMBOL_SEQ = ADDR2.TAMBOL_SEQ               
        LEFT JOIN REG.TB_REG_CONSTRUCT_OWNER STROWN2
            ON STROWN2.CONSTRUCT_SEQ = STR2.CONSTRUCT_SEQ
            AND STROWN2.RECORD_STATUS = 'N'
        LEFT JOIN REG.TB_REG_OWNER OWN2
            ON OWN2.OWNER_SEQ = STROWN2.OWNER_SEQ
        LEFT JOIN MAS.TB_MAS_TITLE TT2
            ON OWN2.OWNER_TITLE_SEQ = TT2.TITLE_SEQ
        LEFT JOIN MGT1.TB_REG_SEQUESTER_MAPPING SM
            ON SM.CONSTRUCT_SEQ = STR2.CONSTRUCT_SEQ
            AND SM.PRINTPLATE_TYPE_SEQ = STR2.PRINTPLATE_TYPE_SEQ
        WHERE CONSTRUCT_OWNER_ORDER = 1
        )
        SELECT 
               PP1.CONSTRUCT_ADDR_HID_P1 , PP2.CONSTRUCT_ADDR_HID_P2,
               PP1.ADDR_P1, PP2.ADDR_P2,
               PP1.CONSTRUCT_AREA_NUM_P1, PP2.CONSTRUCT_AREA_NUM_P2,
               PP1.OWN_P1, PP2.OWN_P2,
               PP1.PRINTPLATE_TYPE_NAME_P1 , PP2.PRINTPLATE_TYPE_NAME_P2,
               PP1.PARCEL_NO_P1 , PP2.PARCEL_NO_P2,
               PP1.TAMBOL_NAME_P1 , PP2.TAMBOL_NAME_P2,
               PP1.AMPHUR_NAME_P1 , PP2.AMPHUR_NAME_P2,
               CASE WHEN SUBSTR(PP1.CONSTRUCT_PROCESS_DTM_P1, -4, 4) > 2500 
               THEN TO_CHAR(PP1.CONSTRUCT_PROCESS_DTM_P1) ELSE TO_CHAR(PP1.CONSTRUCT_PROCESS_DTM_P1, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CONSTRUCT_PROCESS_DTM_P1,
               CASE WHEN SUBSTR(PP2.CONSTRUCT_PROCESS_DTM_P2, -4, 4) > 2500 
               THEN TO_CHAR(PP2.CONSTRUCT_PROCESS_DTM_P2) ELSE TO_CHAR(PP2.CONSTRUCT_PROCESS_DTM_P2, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS CONSTRUCT_PROCESS_DTM_P2,
               PP1.CONSTRUCT_REGIST_NAME_P1 , PP2.CONSTRUCT_REGIST_NAME_P2,
               PP1.CONSTRUCT_SEQUEST_STS_P1 , PP2.CONSTRUCT_SEQUEST_STS_P2
        FROM PP1
        FULL OUTER JOIN PP2 
            ON PP1.ADDR_P1 = PP2.ADDR_P2 ";

        $stid = oci_parse($conn, $select);
        if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
        if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
        oci_execute($stid);
        break;
}
?>