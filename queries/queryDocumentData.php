<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    switch ($table) {
        case 'Document1':
            $select = "SELECT DOC.DOCUMENT_ABBR, DOC.DOCUMENT_NAME, PD.PROCESS_DOCUMENT_TEMP_TITLE, PD.TITLE_EXT
            FROM MGT1.TB_REG_PROCESS_DOCUMENT_TEMP PD
            INNER JOIN MGT1.TB_REG_MAS_DOCUMENT DOC
                ON PD.DOCUMENT_SEQ = DOC.DOCUMENT_SEQ
            WHERE PD.RECORD_STATUS = 'N' AND PROCESS_TEMP_SEQ = :processSeqP1
            ORDER BY DOC.DOCUMENT_ABBR
                        ";


            $stid = oci_parse($conn, $select);
            //oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($processSeqP1 != '') oci_bind_by_name($stid, ':processSeqP1', $processSeqP1);
            oci_execute($stid);
        break;

        case 'Document2':
            $select = "SELECT DOC.DOCUMENT_ABBR, DOC.DOCUMENT_NAME, PD.PROCESS_DOCUMENT_TEMP_TITLE, PD.TITLE_EXT
                        FROM REG.TB_REG_PROCESS_DOCUMENT_TEMP PD
                        INNER JOIN REG.TB_REG_MAS_DOCUMENT DOC
                            ON PD.DOCUMENT_SEQ = DOC.DOCUMENT_SEQ
                        WHERE PD.RECORD_STATUS = 'N' AND PROCESS_TEMP_SEQ = :processSeqP2
                        ORDER BY DOC.DOCUMENT_ABBR
                    ";
            

            $stid = oci_parse($conn, $select);
            //oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($processSeqP2 != '') oci_bind_by_name($stid, ':processSeqP2', $processSeqP2);
            oci_execute($stid);
        break;

        }

?>