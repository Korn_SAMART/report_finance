<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ตำแหน่ง
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_HRM_MAS_DIV_POSITION
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM HRM.TB_HRM_MAS_DIV_POSITION 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.DIV_POSITION_SEQ AS DIV_POSITION_SEQ_P1 , P2.DIV_POSITION_SEQ AS DIV_POSITION_SEQ_P2
                        ,P1.DIV_POSITION_NAME AS DIV_POSITION_NAME_P1 , P2.DIV_POSITION_NAME AS DIV_POSITION_NAME_P2
                        ,P1.RECORD_STATUS AS RECORD_STATUS_P1 , P2.RECORD_STATUS AS RECORD_STATUS_P2
                        ,P1.CREATE_USER AS CREATE_USER_P1 , P2.CREATE_USER AS CREATE_USER_P2
                        ,P1.CREATE_DTM AS CREATE_DTM_P1 , P2.CREATE_DTM AS CREATE_DTM_P2
                        ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 , P2.LAST_UPD_USER AS LAST_UPD_USER_P2
                        ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 , P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
                        ,P1.PSN_TYPE_SEQ AS PSN_TYPE_SEQ_P1 , P2.PSN_TYPE_SEQ AS PSN_TYPE_SEQ_P2
                        ,CASE P1.PSN_TYPE_SEQ
                        WHEN 1 THEN 'ข้าราชการ'
                        WHEN 2 THEN 'ลูกจ้างประจำ' 
                        WHEN 3 THEN 'พนักงานราชการ'
                        WHEN 4 THEN 'ลูกจ้างชั่วคราว'
                        WHEN 5 THEN 'ลูกจ้างชั่วคราวแบบเหมาบริการ'
                        END AS PSN_TYPE_P1
                        ,CASE P2.PSN_TYPE_SEQ
                        WHEN 1 THEN 'ข้าราชการ'
                        WHEN 2 THEN 'ลูกจ้างประจำ' 
                        WHEN 3 THEN 'พนักงานราชการ'
                        WHEN 4 THEN 'ลูกจ้างชั่วคราว'
                        WHEN 5 THEN 'ลูกจ้างชั่วคราวแบบเหมาบริการ'
                        END AS PSN_TYPE_P2
                        ,P1.DIV_POSITION_CODE AS DIV_POSITION_CODE_P1 , P2.DIV_POSITION_CODE AS DIV_POSITION_CODE_P2
                        ,P1.DIV_POSITION_ABBR AS DIV_POSITION_ABBR_P1 , P2.DIV_POSITION_ABBR AS DIV_POSITION_ABBR_P2
                        ,P1.POS_CATAGORY_SEQ AS POS_CATAGORY_SEQ_P1 , P2.POS_CATAGORY_SEQ AS POS_CATAGORY_SEQ_P2
                        
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.DIV_POSITION_SEQ = P2.DIV_POSITION_SEQ
                    ORDER BY P1.PSN_TYPE_SEQ, P1.DIV_POSITION_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ประเภทบุคลากร    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_HRM_MAS_PSNTYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM HRM.TB_HRM_MAS_PSNTYPE 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.PSN_TYPE_SEQ AS PSN_TYPE_SEQ_P1 , P2.PSN_TYPE_SEQ AS PSN_TYPE_SEQ_P2
                        ,P1.PSN_TYPE_NAME AS PSN_TYPE_NAME_P1 , P2.PSN_TYPE_NAME AS PSN_TYPE_NAME_P2
                        ,P1.RECORD_STATUS AS RECORD_STATUS_P1 , P2.RECORD_STATUS AS RECORD_STATUS_P2
                        ,P1.CREATE_USER AS CREATE_USER_P1 , P2.CREATE_USER AS CREATE_USER_P2
                        ,P1.CREATE_DTM AS CREATE_DTM_P1 , P2.CREATE_DTM AS CREATE_DTM_P2
                        ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 , P2.LAST_UPD_USER AS LAST_UPD_USER_P2
                        ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 , P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
                        
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PSN_TYPE_SEQ = P2.PSN_TYPE_SEQ
                    ORDER BY NVL(P1.PSN_TYPE_SEQ,P2.PSN_TYPE_SEQ)";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //สิทธิ์การลา   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_HRM_MAS_LEAVE_RIGHT
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM HRM.TB_HRM_MAS_LEAVE_RIGHT 
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.LEAVE_RIGHT_SEQ AS LEAVE_RIGHT_SEQ_P1 , P2.LEAVE_RIGHT_SEQ AS LEAVE_RIGHT_SEQ_P2
                        ,L1.LEAVE_TYPE_NAME AS LEAVE_TYPE_NAME_P1 , L2.LEAVE_TYPE_NAME AS LEAVE_TYPE_NAME_P2
                        ,T1.PSN_TYPE_NAME AS PSN_TYPE_NAME_P1 , T2.PSN_TYPE_NAME AS PSN_TYPE_NAME_P2
                        ,P1.EXP_FROM AS EXP_FROM_P1 , P2.EXP_FROM AS EXP_FROM_P2
                        ,P1.EXP_TO AS EXP_TO_P1 , P2.EXP_TO AS EXP_TO_P2
                        ,P1.LEAVE_AMOUNT AS LEAVE_AMOUNT_P1 , P2.LEAVE_AMOUNT AS LEAVE_AMOUNT_P2
                        
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LEAVE_RIGHT_SEQ = P2.LEAVE_RIGHT_SEQ
                    LEFT OUTER JOIN MGT1.TB_HRM_MAS_LEAVE_TYPE L1
                        ON P1.LEAVE_TYPE_SEQ = L1.LEAVE_TYPE_SEQ
                    LEFT OUTER JOIN HRM.TB_HRM_MAS_LEAVE_TYPE L2
                        ON P2.LEAVE_TYPE_SEQ = L2.LEAVE_TYPE_SEQ
                    LEFT OUTER JOIN MGT1.TB_HRM_MAS_PSNTYPE T1
                        ON P1.PSN_TYPE_SEQ = T1.PSN_TYPE_SEQ
                    LEFT OUTER JOIN HRM.TB_HRM_MAS_PSNTYPE T2
                        ON P2.PSN_TYPE_SEQ = T2.PSN_TYPE_SEQ
                    ORDER BY P1.LEAVE_RIGHT_SEQ, P2.LEAVE_RIGHT_SEQ";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

    }
?>