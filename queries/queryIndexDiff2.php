<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';

function getData($landoffice, $printplateType, $type){
    
    include '../database/conn.php';

    if($type=='err'){
        if($printplateType == 1){
            $sql = "SELECT LANDOFFICE_SEQ, AMPHUR_NAME, TAMBOL_NAME, P.PARCEL_SEQ, P.PARCEL_NO AS NO, PARCEL_SURVEY_NO AS SURVEY
                        ,PI.PARCEL_INDEX_SEQ AS INDEX_SEQ, PARCEL_INDEX_REGDTM AS INDEX_REGDTM, PARCEL_INDEX_ORDER AS INDEX_ORDER, PARCEL_INDEX_REGNAME AS INDEX_REGNAME
                        ,PH.PARCEL_HSFS_SEQ AS HSFS_SEQ, PARCEL_HSFS_ORDER AS HSFS_ORDER, PARCEL_HSFS_PNAME AS HSFS_PNAME
                        ,PH.PARCEL_HSFS_FILENAME AS FNAME, PARCEL_HSFS_URL AS FURL
                        ,P.LOG1_MIGRATE_NOTE
                    FROM (
                        SELECT PARCEL_HSFS_SEQ
                        FROM MGT1.TB_REG_PARCEL P
                        INNER JOIN MGT1.TB_REG_PARCEL_INDEX PI
                            ON P.PARCEL_SEQ = PI.PARCEL_SEQ
                            AND PI.RECORD_STATUS = 'N'
                        INNER JOIN MGT1.TB_REG_PARCEL_HSFS PH
                            ON PI.PARCEL_INDEX_SEQ = PH.PARCEL_INDEX_SEQ
                            AND PH.RECORD_STATUS IN ('W', 'N', 'E')
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = : printplateType AND P.RECORD_STATUS = 'N'
                        MINUS
                        SELECT PARCEL_HSFS_SEQ
                        FROM REG.TB_REG_PARCEL P
                        INNER JOIN REG.TB_REG_PARCEL_INDEX PI
                            ON P.PARCEL_SEQ = PI.PARCEL_SEQ
                            AND PI.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_PARCEL_HSFS PH
                            ON PI.PARCEL_INDEX_SEQ = PH.PARCEL_INDEX_SEQ
                            AND PH.RECORD_STATUS IN ('W', 'N', 'E')
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = : printplateType AND P.RECORD_STATUS = 'N'
                    ) M
                    INNER JOIN MGT1.TB_REG_PARCEL_HSFS PH
                        ON PH.PARCEL_HSFS_SEQ = M.PARCEL_HSFS_SEQ
                        AND PH.RECORD_STATUS IN ('W', 'N', 'E')
                    INNER JOIN MGT1.TB_REG_PARCEL_INDEX PI
                        ON PI.PARCEL_INDEX_SEQ = PH.PARCEL_INDEX_SEQ
                        AND PI.RECORD_STATUS = 'N'
                    INNER JOIN MGT1.TB_REG_PARCEL P
                        ON P.PARCEL_SEQ = PI.PARCEL_SEQ
                        AND P.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = P.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = P.TAMBOL_SEQ
                    ORDER BY NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY')
                        ,TO_NUMBER(P.PARCEL_NO)
                        ,PI.PARCEL_INDEX_REGDTM, PARCEL_INDEX_ORDER, PARCEL_HSFS_ORDER, PH.PARCEL_HSFS_SEQ
                    ";
        } else if ($printplateType==10){
            $sql = "SELECT C.CONDOROOM_SEQ AS SEQ, C.CONDOROOM_RNO AS NO, AMPHUR_NAME, TAMBOL_NAME, CD.CONDO_ID, TO_NUMBER(CD.CONDO_REG_YEAR) AS YEAR, CD.CONDO_NAME_TH AS NAME, C.PRINTPLATE_TYPE_SEQ
                        ,CI.CONDOROOM_INDEX_SEQ AS INDEX_SEQ, CI.CONDOROOM_INDEX_REGDTM AS INDEX_REGDTM, CI.CONDOROOM_INDEX_ORDER AS INDEX_ORDER, CI.CONDOROOM_INDEX_REGNAME AS INDEX_REGNAME
                        ,CH.CONDOROOM_HSFS_SEQ AS HSFS_SEQ, CH.CONDOROOM_HSFS_ORDER AS HSFS_ORDER, CH.CONDOROOM_HSFS_PNAME AS HSFS_PNAME, CH.CONDOROOM_HSFS_FILENAME_ AS FNAME, CH.CONDOROOM_HSFS_URL_ AS FURL
                        ,C.LOG1_MIGRATE_NOTE
                    FROM (
                        SELECT CONDOROOM_HSFS_SEQ
                        FROM MGT1.TB_REG_CONDOROOM C
                        LEFT JOIN MGT1.TB_REG_CONDO_BLD B
                            ON B.BLD_SEQ = C.BLD_SEQ
                            AND B.RECORD_STATUS = 'N'
                        LEFT JOIN MGT1.TB_REG_CONDO CD
                            ON CD.CONDO_SEQ = B.CONDO_SEQ
                            AND CD.RECORD_STATUS = 'N'
                        INNER JOIN MGT1.TB_REG_CONDOROOM_INDEX CI
                            ON CI.CONDOROOM_SEQ = C.CONDOROOM_SEQ
                            AND CI.RECORD_STATUS = 'N'
                        INNER JOIN MGT1.TB_REG_CONDOROOM_HSFS CF
                            ON CF.CONDOROOM_INDEX_SEQ = CI.CONDOROOM_INDEX_SEQ
                            AND CF.RECORD_STATUS IN ('N', 'W', 'E')
                        WHERE C.LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND C.RECORD_STATUS = 'N'
                        MINUS
                        SELECT CONDOROOM_HSFS_SEQ
                        FROM MGT1.TB_REG_CONDOROOM C
                        LEFT JOIN REG.TB_REG_CONDO_BLD B
                            ON B.BLD_SEQ = C.BLD_SEQ
                            AND B.RECORD_STATUS = 'N'
                        LEFT JOIN REG.TB_REG_CONDO CD
                            ON CD.CONDO_SEQ = B.CONDO_SEQ
                            AND CD.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_CONDOROOM_INDEX CI
                            ON CI.CONDOROOM_SEQ = C.CONDOROOM_SEQ
                            AND CI.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_CONDOROOM_HSFS CF
                            ON CF.CONDOROOM_INDEX_SEQ = CI.CONDOROOM_INDEX_SEQ
                            AND CF.RECORD_STATUS IN ('N', 'W', 'E')
                        WHERE C.LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND C.RECORD_STATUS = 'N'
                    ) M
                    INNER JOIN MGT1.TB_REG_CONDOROOM_HSFS CH
                        ON CH.CONDOROOM_HSFS_SEQ = M.CONDOROOM_HSFS_SEQ
                        AND CH.RECORD_STATUS IN ('N', 'W', 'E')
                    INNER JOIN MGT1.TB_REG_CONDOROOM_INDEX CI
                        ON CI.CONDOROOM_INDEX_SEQ = CH.CONDOROOM_INDEX_SEQ
                        AND CI.RECORD_STATUS =  'N'
                    INNER JOIN MGT1.TB_REG_CONDOROOM C
                        ON C.CONDOROOM_SEQ = CI.CONDOROOM_SEQ
                        AND C.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_REG_CONDO_BLD B
                        ON B.BLD_SEQ = C.BLD_SEQ
                        AND B.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_REG_CONDO CD
                        ON CD.CONDO_SEQ = B.CONDO_SEQ
                        AND CD.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = CD.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = CD.TAMBOL_SEQ
                    ORDER BY NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY')
                        ,TO_NUMBER(CD.CONDO_REG_YEAR), TO_NUMBER(CD.CONDO_ID)
                        ,REGEXP_SUBSTR(C.CONDOROOM_RNO, '^\D*') NULLS FIRST, TO_NUMBER(REGEXP_SUBSTR(C.CONDOROOM_RNO, '\d+')), TO_NUMBER(SUBSTR(C.CONDOROOM_RNO, INSTR(C.CONDOROOM_RNO, '/')+1))
                        ,CI.CONDOROOM_INDEX_REGDTM, CI.CONDOROOM_INDEX_ORDER, CH.CONDOROOM_HSFS_ORDER, CH.CONDOROOM_HSFS_SEQ ";
        } else if ($printplateType==11){
    
        } else if ($printplateType==13){
            $sql = "SELECT LANDOFFICE_SEQ, AMPHUR_NAME, TAMBOL_NAME, CONDO_ID, CONDO_REG_YEAR AS YEAR, CONDO_NAME_TH AS NAME
                        ,CI.CONDO_INDEX_SEQ AS INDEX_SEQ, CONDO_INDEX_REGDTM AS INDEX_REGDTM, CONDO_INDEX_ORDER AS INDEX_ORDER, CONDO_INDEX_REGNAME AS INDEX_REGNAME
                        ,CH.CONDO_HSFS_SEQ AS HSFS_SEQ, CH.CONDO_HSFS_ORDER AS HSFS_ORDER, CONDO_HSFS_PNAME AS HSFS_PNAME
                        ,CH.CONDO_HSFS_FILENAME AS FNAME, CH.CONDO_HSFS_URL AS FURL
                        ,C.LOG1_MIGRATE_NOTE
                    FROM (
                        SELECT CONDO_HSFS_SEQ
                            FROM REG.TB_REG_CONDO C
                            INNER JOIN REG.TB_REG_CONDO_INDEX CI
                                ON CI.CONDO_SEQ = C.CONDO_SEQ
                                AND CI.RECORD_STATUS = 'N'
                            INNER JOIN REG.TB_REG_CONDO_HSFS CF
                                ON CF.CONDO_INDEX_SEQ = CI.CONDO_INDEX_SEQ
                                AND CF.RECORD_STATUS IN ('N', 'W', 'E')
                            WHERE LANDOFFICE_SEQ = :landoffice AND C.RECORD_STATUS = 'N'
                        MINUS
                            SELECT CONDO_HSFS_SEQ
                            FROM MGT1.TB_REG_CONDO C
                            INNER JOIN MGT1.TB_REG_CONDO_INDEX CI
                                ON CI.CONDO_SEQ = C.CONDO_SEQ
                                AND CI.RECORD_STATUS = 'N'
                            INNER JOIN MGT1.TB_REG_CONDO_HSFS CF
                                ON CF.CONDO_INDEX_SEQ = CI.CONDO_INDEX_SEQ
                                AND CF.RECORD_STATUS IN ('N', 'W', 'E')
                            WHERE LANDOFFICE_SEQ = :landoffice AND C.RECORD_STATUS = 'N'
                    ) M
                    INNER JOIN MGT1.TB_REG_CONDO_HSFS CH
                        ON CH.CONDO_HSFS_SEQ = M.CONDO_HSFS_SEQ
                        AND CH.RECORD_STATUS IN ('N', 'W', 'E')
                    INNER JOIN MGT1.TB_REG_CONDO_INDEX CI
                        ON CI.CONDO_INDEX_SEQ = CH.CONDO_INDEX_SEQ
                        AND CI.RECORD_STATUS = 'N'
                    INNER JOIN MGT1.TB_REG_CONDO C
                        ON C.CONDO_SEQ = CI.CONDO_SEQ
                        AND C.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = C.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = C.TAMBOL_SEQ
                    ORDER BY NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,TO_NUMBER(CONDO_REG_YEAR), TO_NUMBER(CONDO_ID)
                            ,CONDO_INDEX_REGDTM, CONDO_INDEX_ORDER, CONDO_HSFS_ORDER, CH.CONDO_HSFS_SEQ ";
        } else {
            $sql = "SELECT LANDOFFICE_SEQ, AMPHUR_NAME, TAMBOL_NAME, P.PARCEL_LAND_SEQ, P.PARCEL_LAND_NO AS NO, PARCEL_LAND_SURVEY_NO AS SURVEY
                        ,PARCEL_LAND_MOO AS MOO, PARCEL_LAND_NAME AS NAME, PARCEL_LAND_OBTAIN_DTM AS YEAR
                        ,PI.PARCEL_LAND_INDEX_SEQ AS INDEX_SEQ, PARCEL_LAND_INDEX_REGDTM AS INDEX_REGDTM, PARCEL_LAND_INDEX_ORDER AS INDEX_ORDER, PARCEL_LAND_INDEX_REGNAME AS INDEX_REGNAME
                        ,PH.PARCEL_LAND_HSFS_SEQ AS HSFS_SEQ, PARCEL_LAND_HSFS_ORDER AS HSFS_ORDER, PARCEL_LAND_HSFS_PNAME AS HSFS_PNAME
                        ,PH.PARCEL_LAND_HSFS_FILENAME_ AS FNAME, PARCEL_LAND_HSFS_URL_ AS FURL
                        ,P.LOG1_MIGRATE_NOTE
                    FROM (
                        SELECT PARCEL_LAND_HSFS_SEQ
                        FROM REG.TB_REG_PARCEL_LAND P
                        INNER JOIN REG.TB_REG_PARCEL_LAND_INDEX PI
                            ON P.PARCEL_LAND_SEQ = PI.PARCEL_LAND_SEQ
                            AND PI.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_PARCEL_LAND_HSFS PH
                            ON PI.PARCEL_LAND_INDEX_SEQ = PH.PARCEL_LAND_INDEX_SEQ
                            AND PH.RECORD_STATUS IN ('W', 'N', 'E')
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = : printplateType AND P.RECORD_STATUS = 'N'
                        MINUS
                        SELECT PARCEL_LAND_HSFS_SEQ
                        FROM MGT1.TB_REG_PARCEL_LAND P
                        INNER JOIN MGT1.TB_REG_PARCEL_LAND_INDEX PI
                            ON P.PARCEL_LAND_SEQ = PI.PARCEL_LAND_SEQ
                            AND PI.RECORD_STATUS = 'N'
                        INNER JOIN MGT1.TB_REG_PARCEL_LAND_HSFS PH
                            ON PI.PARCEL_LAND_INDEX_SEQ = PH.PARCEL_LAND_INDEX_SEQ
                            AND PH.RECORD_STATUS IN ('W', 'N', 'E')
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = : printplateType AND P.RECORD_STATUS = 'N'
                    ) M 
                    INNER JOIN MGT1.TB_REG_PARCEL_LAND_HSFS PH
                        ON PH.PARCEL_LAND_HSFS_SEQ = M.PARCEL_LAND_HSFS_SEQ
                        AND PH.RECORD_STATUS IN ('W', 'N', 'E')
                    INNER JOIN MGT1.TB_REG_PARCEL_LAND_INDEX PI
                        ON PI.PARCEL_LAND_INDEX_SEQ = PH.PARCEL_LAND_INDEX_SEQ
                        AND PI.RECORD_STATUS = 'N'
                    INNER JOIN MGT1.TB_REG_PARCEL_LAND P
                        ON P.PARCEL_LAND_SEQ = PI.PARCEL_LAND_SEQ
                        AND P.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = P.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = P.TAMBOL_SEQ
                    ORDER BY NLSSORT(AMPHUR_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(TAMBOL_NAME, 'NLS_SORT=THAI_DICTIONARY')
                        ,PARCEL_LAND_MOO
                        ,REGEXP_SUBSTR(PARCEL_LAND_NO, '^\D*') NULLS FIRST, TO_NUMBER(REGEXP_SUBSTR(PARCEL_LAND_NO, '\d+'))
                        ,PARCEL_LAND_INDEX_REGDTM, PARCEL_LAND_INDEX_ORDER, PARCEL_LAND_HSFS_ORDER, PH.PARCEL_LAND_HSFS_SEQ
                    ";
        }
        
    } else {
        $cond = $type=='e'? 'MINUS' : 'INTERSECT';

        if($printplateType == 1){
            $sql = "WITH P2 AS(
                        SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, P.PARCEL_SEQ, P.PARCEL_NO AS NO, PARCEL_SURVEY_NO AS SURVEY
                            ,PI.PARCEL_INDEX_SEQ AS INDEX_SEQ, PARCEL_INDEX_REGDTM AS INDEX_REGDTM, PARCEL_INDEX_ORDER AS INDEX_ORDER, PARCEL_INDEX_REGNAME AS INDEX_REGNAME
                            ,PH.PARCEL_HSFS_SEQ AS HSFS_SEQ, PARCEL_HSFS_ORDER AS HSFS_ORDER, PARCEL_HSFS_PNAME AS HSFS_PNAME
                            ,PH.PARCEL_HSFS_FILENAME AS FNAME, PARCEL_HSFS_URL AS FURL
                        FROM REG.TB_REG_PARCEL P
                        INNER JOIN REG.TB_REG_PARCEL_INDEX PI
                            ON P.PARCEL_SEQ = PI.PARCEL_SEQ
                            AND PI.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_PARCEL_HSFS PH
                            ON PI.PARCEL_INDEX_SEQ = PH.PARCEL_INDEX_SEQ
                            AND PH.RECORD_STATUS IN ('W', 'N', 'E')
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = : printplateType AND P.RECORD_STATUS = 'N'
                    ), P1 AS (
                        SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, P.PARCEL_SEQ, P.PARCEL_NO AS NO, PARCEL_SURVEY_NO AS SURVEY
                            ,PI.PARCEL_INDEX_SEQ AS INDEX_SEQ, PARCEL_INDEX_REGDTM AS INDEX_REGDTM, PARCEL_INDEX_ORDER AS INDEX_ORDER, PARCEL_INDEX_REGNAME AS INDEX_REGNAME
                            ,PH.PARCEL_HSFS_SEQ AS HSFS_SEQ, PARCEL_HSFS_ORDER AS HSFS_ORDER, PARCEL_HSFS_PNAME AS HSFS_PNAME
                            ,PH.PARCEL_HSFS_FILENAME_ AS FNAME, PARCEL_HSFS_URL_ AS FURL
                        FROM MGT1.TB_REG_PARCEL P
                        INNER JOIN MGT1.TB_REG_PARCEL_INDEX PI
                            ON P.PARCEL_SEQ = PI.PARCEL_SEQ
                            AND PI.RECORD_STATUS = 'N'
                        INNER JOIN MGT1.TB_REG_PARCEL_HSFS PH
                            ON PI.PARCEL_INDEX_SEQ = PH.PARCEL_INDEX_SEQ
                            AND PH.RECORD_STATUS IN ('W', 'N', 'E')
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = : printplateType AND P.RECORD_STATUS = 'N'
                    )
                    SELECT AP1.AMPHUR_NAME, TB1.TAMBOL_NAME, P1.NO AS NO, P1.SURVEY, P1.INDEX_REGDTM, P1.INDEX_ORDER, P1.INDEX_REGNAME, P1.HSFS_ORDER, P1.HSFS_PNAME, P1.FNAME, P1.FURL
                        ,AP3.AMPHUR_NAME AS AMPHUR_NAME_1, TB3.TAMBOL_NAME AS TAMBOL_NAME_1, P3.NO AS NO_1, P3.SURVEY AS SURVEY_1, P3.INDEX_REGDTM AS INDEX_REGDTM_1, P3.INDEX_ORDER AS INDEX_ORDER_1
                        ,P3.INDEX_REGNAME AS INDEX_REGNAME_1, P3.HSFS_ORDER AS HSFS_ORDER_1, P3.HSFS_PNAME AS HSFS_PNAME_1, P3.FNAME AS FNAME_1, P3.FURL AS FURL_1
                        ,P1.HSFS_SEQ, P3.HSFS_SEQ AS HSFS_SEQ_1
                    FROM (
                        SELECT * FROM P2
                        ".$cond."
                        SELECT * FROM P1
                        ) P3
                    LEFT JOIN P1
                        ON P1.HSFS_SEQ = P3.HSFS_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP1
                        ON AP1.AMPHUR_SEQ = P1.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB1
                        ON TB1.TAMBOL_SEQ = P1.TAMBOL_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP3
                        ON AP3.AMPHUR_SEQ = P3.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB3
                        ON TB3.TAMBOL_SEQ = P3.TAMBOL_SEQ
                    ORDER BY NLSSORT(NVL(AP3.AMPHUR_NAME,AP1.AMPHUR_NAME), 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(NVL(TB3.TAMBOL_NAME,TB1.TAMBOL_NAME), 'NLS_SORT=THAI_DICTIONARY')
                        ,TO_NUMBER(NVL(P3.NO,P1.NO))
                        ,NVL(P3.INDEX_REGDTM,P1.INDEX_REGDTM), NVL(P3.INDEX_ORDER,P1.INDEX_ORDER), NVL(P3.HSFS_ORDER,P1.HSFS_ORDER), NVL(P3.HSFS_SEQ,P1.HSFS_SEQ)
                    ";
        } else if ($printplateType==10){
            $sql = "WITH P1 AS (  
                        SELECT C.CONDOROOM_SEQ AS SEQ, C.CONDOROOM_RNO AS NO, CD.AMPHUR_SEQ, CD.TAMBOL_SEQ, CD.CONDO_ID, TO_NUMBER(CD.CONDO_REG_YEAR) AS YEAR, CD.CONDO_NAME_TH AS NAME, C.PRINTPLATE_TYPE_SEQ
                            ,CI.CONDOROOM_INDEX_SEQ AS INDEX_SEQ, CI.CONDOROOM_INDEX_REGDTM AS INDEX_REGDTM, CI.CONDOROOM_INDEX_ORDER AS INDEX_ORDER, CI.CONDOROOM_INDEX_REGNAME AS INDEX_REGNAME
                            ,CF.CONDOROOM_HSFS_SEQ AS HSFS_SEQ, CF.CONDOROOM_HSFS_ORDER AS HSFS_ORDER, CF.CONDOROOM_HSFS_PNAME AS HSFS_PNAME, CF.CONDOROOM_HSFS_FILENAME_ AS FNAME, CF.CONDOROOM_HSFS_URL_ AS FURL
                        FROM MGT1.TB_REG_CONDOROOM C
                        LEFT JOIN MGT1.TB_REG_CONDO_BLD B
                            ON B.BLD_SEQ = C.BLD_SEQ
                            AND B.RECORD_STATUS = 'N'
                        LEFT JOIN MGT1.TB_REG_CONDO CD
                            ON CD.CONDO_SEQ = B.CONDO_SEQ
                            AND CD.RECORD_STATUS = 'N'
                        INNER JOIN MGT1.TB_REG_CONDOROOM_INDEX CI
                            ON CI.CONDOROOM_SEQ = C.CONDOROOM_SEQ
                            AND CI.RECORD_STATUS = 'N'
                        INNER JOIN MGT1.TB_REG_CONDOROOM_HSFS CF
                            ON CF.CONDOROOM_INDEX_SEQ = CI.CONDOROOM_INDEX_SEQ
                            AND CF.RECORD_STATUS IN ('N', 'W', 'E')
                        WHERE C.LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND C.RECORD_STATUS = 'N'
                    ), P2 AS (
                        SELECT C.CONDOROOM_SEQ AS SEQ, C.CONDOROOM_RNO AS NO, CD.AMPHUR_SEQ, CD.TAMBOL_SEQ, CD.CONDO_ID, TO_NUMBER(CD.CONDO_REG_YEAR) AS YEAR, CD.CONDO_NAME_TH AS NAME, C.PRINTPLATE_TYPE_SEQ
                            ,CI.CONDOROOM_INDEX_SEQ AS INDEX_SEQ, CI.CONDOROOM_INDEX_REGDTM AS INDEX_REGDTM, CI.CONDOROOM_INDEX_ORDER AS INDEX_ORDER, CI.CONDOROOM_INDEX_REGNAME AS INDEX_REGNAME
                            ,CF.CONDOROOM_HSFS_SEQ AS HSFS_SEQ, CF.CONDOROOM_HSFS_ORDER AS HSFS_ORDER, CF.CONDOROOM_HSFS_PNAME AS HSFS_PNAME, CF.CONDOROOM_HSFS_FILENAME AS FNAME, CF.CONDOROOM_HSFS_URL AS FURL
                        FROM MGT1.TB_REG_CONDOROOM C
                        LEFT JOIN REG.TB_REG_CONDO_BLD B
                            ON B.BLD_SEQ = C.BLD_SEQ
                            AND B.RECORD_STATUS = 'N'
                        LEFT JOIN REG.TB_REG_CONDO CD
                            ON CD.CONDO_SEQ = B.CONDO_SEQ
                            AND CD.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_CONDOROOM_INDEX CI
                            ON CI.CONDOROOM_SEQ = C.CONDOROOM_SEQ
                            AND CI.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_CONDOROOM_HSFS CF
                            ON CF.CONDOROOM_INDEX_SEQ = CI.CONDOROOM_INDEX_SEQ
                            AND CF.RECORD_STATUS IN ('N', 'W', 'E')
                        WHERE C.LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = :printplateType AND C.RECORD_STATUS = 'N'
                    )
                    SELECT P1.NO, AP1.AMPHUR_NAME, TB1.TAMBOL_NAME, P1.CONDO_ID, P1.YEAR, P1.NAME, P1.PRINTPLATE_TYPE_SEQ, P1.INDEX_REGDTM, P1.INDEX_ORDER, P1.INDEX_REGNAME
                        ,P1.HSFS_ORDER, P1.HSFS_PNAME, P1.FNAME, P1.FURL
                        ,P3.NO AS NO_1, AP3.AMPHUR_NAME AS AMPHUR_NAME_1, TB3.TAMBOL_NAME AS TAMBOL_NAME_1, P3.CONDO_ID AS CONDO_ID_1
                        ,P3.YEAR AS YEAR_1, P3.NAME AS NAME_1
                        ,P3.PRINTPLATE_TYPE_SEQ AS PRINTPLATE_TYPE_SEQ_1, P3.INDEX_REGDTM AS INDEX_REGDTM_1, P3.INDEX_ORDER AS INDEX_ORDER_1, P3.INDEX_REGNAME AS INDEX_REGNAME_1
                        ,P3.HSFS_ORDER AS HSFS_ORDER_1, P3.HSFS_PNAME AS HSFS_PNAME_1, P3.FNAME AS FNAME_1, P3.FURL AS FURL_1 
                        ,P1.HSFS_SEQ, P3.HSFS_SEQ AS HSFS_SEQ_1
                    FROM (
                        SELECT * FROM P2
                        ".$cond."
                        SELECT * FROM P1
                        ) P3
                    LEFT JOIN P1
                        ON P1.HSFS_SEQ = P3.HSFS_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP1
                        ON AP1.AMPHUR_SEQ = P1.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB1
                        ON TB1.TAMBOL_SEQ = P1.TAMBOL_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP3
                        ON AP3.AMPHUR_SEQ = P3.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB3
                        ON TB3.TAMBOL_SEQ = P3.TAMBOL_SEQ
                    ORDER BY NLSSORT(NVL(AP3.AMPHUR_NAME,AP1.AMPHUR_NAME), 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(NVL(TB3.TAMBOL_NAME,TB1.TAMBOL_NAME), 'NLS_SORT=THAI_DICTIONARY')
                        ,TO_NUMBER(NVL(P3.YEAR,P1.YEAR)), TO_NUMBER(NVL(P3.CONDO_ID,P1.CONDO_ID))
                        ,REGEXP_SUBSTR(NVL(P3.NO,P1.NO), '^\D*') NULLS FIRST, TO_NUMBER(REGEXP_SUBSTR(NVL(P3.NO,P1.NO), '\d+')), LPAD(SUBSTR(NVL(P3.NO,P1.NO), INSTR(NVL(P3.NO,P1.NO), '/')+1),5,'0')
                        ,NVL(P3.INDEX_REGDTM,P1.INDEX_REGDTM), NVL(P3.INDEX_ORDER,P1.INDEX_ORDER), NVL(P3.HSFS_ORDER,P1.HSFS_ORDER), NVL(P3.HSFS_SEQ,P1.HSFS_SEQ) ";
        } else if ($printplateType==11){
    
        } else if ($printplateType==13){
            $sql = "WITH P2 AS (    
                        SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, CONDO_ID, CONDO_REG_YEAR AS YEAR, CONDO_NAME_TH AS NAME
                            ,CI.CONDO_INDEX_SEQ AS INDEX_SEQ, CONDO_INDEX_REGDTM AS INDEX_REGDTM, CONDO_INDEX_ORDER AS INDEX_ORDER, CONDO_INDEX_REGNAME AS INDEX_REGNAME
                            ,CF.CONDO_HSFS_SEQ AS HSFS_SEQ, CF.CONDO_HSFS_ORDER AS HSFS_ORDER, CONDO_HSFS_PNAME AS HSFS_PNAME
                            ,CF.CONDO_HSFS_FILENAME AS FNAME, CF.CONDO_HSFS_URL AS FURL
                        FROM REG.TB_REG_CONDO C
                        INNER JOIN REG.TB_REG_CONDO_INDEX CI
                            ON CI.CONDO_SEQ = C.CONDO_SEQ
                            AND CI.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_CONDO_HSFS CF
                            ON CF.CONDO_INDEX_SEQ = CI.CONDO_INDEX_SEQ
                            AND CF.RECORD_STATUS IN ('N', 'W', 'E')
                        WHERE LANDOFFICE_SEQ = :landoffice AND C.RECORD_STATUS = 'N'
                    ), P1 AS (
                        SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, CONDO_ID, CONDO_REG_YEAR AS YEAR, CONDO_NAME_TH AS NAME
                            ,CI.CONDO_INDEX_SEQ AS INDEX_SEQ, CONDO_INDEX_REGDTM AS INDEX_REGDTM, CONDO_INDEX_ORDER AS INDEX_ORDER, CONDO_INDEX_REGNAME AS INDEX_REGNAME
                            ,CF.CONDO_HSFS_SEQ AS HSFS_SEQ, CF.CONDO_HSFS_ORDER AS HSFS_ORDER, CONDO_HSFS_PNAME AS HSFS_PNAME
                            ,CF.CONDO_HSFS_FILENAME_ AS FNAME, CF.CONDO_HSFS_URL_ AS FURL
                        FROM MGT1.TB_REG_CONDO C
                        INNER JOIN MGT1.TB_REG_CONDO_INDEX CI
                            ON CI.CONDO_SEQ = C.CONDO_SEQ
                            AND CI.RECORD_STATUS = 'N'
                        INNER JOIN MGT1.TB_REG_CONDO_HSFS CF
                            ON CF.CONDO_INDEX_SEQ = CI.CONDO_INDEX_SEQ
                            AND CF.RECORD_STATUS IN ('N', 'W', 'E')
                        WHERE LANDOFFICE_SEQ = :landoffice AND C.RECORD_STATUS = 'N'
                    )
                    SELECT AP1.AMPHUR_NAME, TB1.TAMBOL_NAME, P1.CONDO_ID, P1.YEAR, P1.NAME, P1.INDEX_REGDTM, P1.INDEX_ORDER, P1.INDEX_REGNAME
                        ,P1.HSFS_ORDER, P1.HSFS_PNAME, P1.FNAME, P1.FURL
                        ,AP3.AMPHUR_NAME AS AMPHUR_NAME_1, TB3.TAMBOL_NAME AS TAMBOL_NAME_1, P3.CONDO_ID AS CONDO_ID_1, P3.YEAR AS YEAR_1, P3.NAME AS NAME_1
                        ,P3.INDEX_REGDTM AS INDEX_REGDTM_1, P3.INDEX_ORDER AS INDEX_ORDER_1, P3.INDEX_REGNAME AS INDEX_REGNAME_1
                        ,P3.HSFS_ORDER AS HSFS_ORDER_1, P3.HSFS_PNAME AS HSFS_PNAME, P3.FNAME AS FNAME_1, P3.FURL AS FURL_1
                        ,P1.HSFS_SEQ, P3.HSFS_SEQ AS HSFS_SEQ_1
                    FROM (
                        SELECT * FROM P2
                        ".$cond."
                        SELECT * FROM P1
                        ) P3
                    LEFT JOIN P1
                        ON P1.HSFS_SEQ = P3.HSFS_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP1
                        ON AP1.AMPHUR_SEQ = P1.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB1
                        ON TB1.TAMBOL_SEQ = P1.TAMBOL_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP3
                        ON AP3.AMPHUR_SEQ = P3.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB3
                        ON TB3.TAMBOL_SEQ = P3.TAMBOL_SEQ
                    ORDER BY NLSSORT(NVL(AP3.AMPHUR_NAME,AP1.AMPHUR_NAME), 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(NVL(TB3.TAMBOL_NAME,TB1.TAMBOL_NAME), 'NLS_SORT=THAI_DICTIONARY')
                        ,TO_NUMBER(NVL(P3.YEAR,P1.YEAR)), TO_NUMBER(NVL(P3.CONDO_ID,P1.CONDO_ID))
                        ,NVL(P3.INDEX_REGDTM,P1.INDEX_REGDTM), NVL(P3.INDEX_ORDER,P1.INDEX_ORDER), NVL(P3.HSFS_ORDER,P1.HSFS_ORDER), NVL(P3.HSFS_SEQ,P1.HSFS_SEQ) 
                    ";
        } else {
            $sql = "WITH P2 AS(
                        SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, P.PARCEL_LAND_SEQ, P.PARCEL_LAND_NO AS NO, PARCEL_LAND_SURVEY_NO AS SURVEY
                            ,PARCEL_LAND_MOO AS MOO, PARCEL_LAND_NAME AS NAME, PARCEL_LAND_OBTAIN_DTM AS YEAR
                            ,PI.PARCEL_LAND_INDEX_SEQ AS INDEX_SEQ, PARCEL_LAND_INDEX_REGDTM AS INDEX_REGDTM, PARCEL_LAND_INDEX_ORDER AS INDEX_ORDER, PARCEL_LAND_INDEX_REGNAME AS INDEX_REGNAME
                            ,PH.PARCEL_LAND_HSFS_SEQ AS HSFS_SEQ, PARCEL_LAND_HSFS_ORDER AS HSFS_ORDER, PARCEL_LAND_HSFS_PNAME AS HSFS_PNAME
                            ,PH.PARCEL_LAND_HSFS_FILENAME AS FNAME, PARCEL_LAND_HSFS_URL AS FURL
                        FROM REG.TB_REG_PARCEL_LAND P
                        INNER JOIN REG.TB_REG_PARCEL_LAND_INDEX PI
                            ON P.PARCEL_LAND_SEQ = PI.PARCEL_LAND_SEQ
                            AND PI.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_PARCEL_LAND_HSFS PH
                            ON PI.PARCEL_LAND_INDEX_SEQ = PH.PARCEL_LAND_INDEX_SEQ
                            AND PH.RECORD_STATUS IN ('W', 'N', 'E')
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = : printplateType AND P.RECORD_STATUS = 'N'
                    ), P1 AS (
                        SELECT LANDOFFICE_SEQ, AMPHUR_SEQ, TAMBOL_SEQ, P.PARCEL_LAND_SEQ, P.PARCEL_LAND_NO AS NO, PARCEL_LAND_SURVEY_NO AS SURVEY
                            ,PARCEL_LAND_MOO AS MOO, PARCEL_LAND_NAME AS NAME, PARCEL_LAND_OBTAIN_DTM AS YEAR
                            ,PI.PARCEL_LAND_INDEX_SEQ AS INDEX_SEQ, PARCEL_LAND_INDEX_REGDTM AS INDEX_REGDTM, PARCEL_LAND_INDEX_ORDER AS INDEX_ORDER, PARCEL_LAND_INDEX_REGNAME AS INDEX_REGNAME
                            ,PH.PARCEL_LAND_HSFS_SEQ AS HSFS_SEQ, PARCEL_LAND_HSFS_ORDER AS HSFS_ORDER, PARCEL_LAND_HSFS_PNAME AS HSFS_PNAME
                            ,PH.PARCEL_LAND_HSFS_FILENAME_ AS FNAME, PARCEL_LAND_HSFS_URL_ AS FURL
                        FROM MGT1.TB_REG_PARCEL_LAND P
                        INNER JOIN MGT1.TB_REG_PARCEL_LAND_INDEX PI
                            ON P.PARCEL_LAND_SEQ = PI.PARCEL_LAND_SEQ
                            AND PI.RECORD_STATUS = 'N'
                        INNER JOIN MGT1.TB_REG_PARCEL_LAND_HSFS PH
                            ON PI.PARCEL_LAND_INDEX_SEQ = PH.PARCEL_LAND_INDEX_SEQ
                            AND PH.RECORD_STATUS IN ('W', 'N', 'E')
                        WHERE LANDOFFICE_SEQ = :landoffice AND PRINTPLATE_TYPE_SEQ = : printplateType AND P.RECORD_STATUS = 'N'
                    )
                    SELECT AP1.AMPHUR_NAME, TB1.TAMBOL_NAME, P1.NO AS NO, P1.SURVEY
                        ,P1.MOO, P1.NAME, P1.YEAR
                        ,P1.INDEX_REGDTM, P1.INDEX_ORDER, P1.INDEX_REGNAME, P1.HSFS_ORDER, P1.HSFS_PNAME, P1.FNAME, P1.FURL
                        ,AP3.AMPHUR_NAME AS AMPHUR_NAME_1, TB3.TAMBOL_NAME AS TAMBOL_NAME_1, P3.NO AS NO_1, P3.SURVEY AS SURVEY_1
                        ,P3.MOO AS MOO_1, P1.NAME AS NAME_1, P1.YEAR AS YEAR, P3.INDEX_REGDTM AS INDEX_REGDTM_1, P3.INDEX_ORDER AS INDEX_ORDER_1
                        ,P3.INDEX_REGNAME AS INDEX_REGNAME_1, P3.HSFS_ORDER AS HSFS_ORDER_1, P3.HSFS_PNAME AS HSFS_PNAME_1, P3.FNAME AS FNAME_1, P3.FURL AS FURL_1
                        ,P1.HSFS_SEQ, P3.HSFS_SEQ AS HSFS_SEQ_1
                    FROM (
                        SELECT * FROM P2
                        ".$cond."
                        SELECT * FROM P1
                        ) P3
                    LEFT JOIN P1
                        ON P1.HSFS_SEQ = P3.HSFS_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP1
                        ON AP1.AMPHUR_SEQ = P1.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB1
                        ON TB1.TAMBOL_SEQ = P1.TAMBOL_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP3
                        ON AP3.AMPHUR_SEQ = P3.AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB3
                        ON TB3.TAMBOL_SEQ = P3.TAMBOL_SEQ
                    ORDER BY NLSSORT(NVL(AP3.AMPHUR_NAME,AP1.AMPHUR_NAME), 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(NVL(TB3.TAMBOL_NAME,TB1.TAMBOL_NAME), 'NLS_SORT=THAI_DICTIONARY')
                        ,NVL(P3.MOO,P1.MOO)
                        ,REGEXP_SUBSTR(NVL(P3.NO,P1.NO), '^\D*') NULLS FIRST, TO_NUMBER(REGEXP_SUBSTR(NVL(P3.NO,P1.NO), '\d+'))
                        ,NVL(P3.INDEX_REGDTM,P1.INDEX_REGDTM), NVL(P3.INDEX_ORDER,P1.INDEX_ORDER), NVL(P3.HSFS_ORDER,P1.HSFS_ORDER), NVL(P3.HSFS_SEQ,P1.HSFS_SEQ) 
                    ";
        }
        
    }
    
    $Result = array();

    // echo $sql."\n";
    if($printplateType!=11){
        $stid = oci_parse($conn, $sql);
        oci_bind_by_name($stid, ':landoffice', $landoffice);
        if($printplateType!=13) oci_bind_by_name($stid, ':printplateType', $printplateType);
        oci_execute($stid);
        while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
            $Result[] = $row;
        }
    }
    // echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);

    return $Result;

}
    // $landoffice = $_REQUEST['landoffice'];
    // $printplateType = $_REQUEST['printplateType'];
?>
