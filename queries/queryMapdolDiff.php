<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $landoffice = $_REQUEST['landoffice'];
    $printplateType = $_REQUEST['printplateType'];
    $cond = $_REQUEST['type']=='e'? 'MINUS' : 'INTERSECT';

    if($_REQUEST['temp']==0){
        $sql = "SELECT * FROM (
                    SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, P1.UTMMAP3, P1.UTMMAP4, P1.LAND_NO, P1.PARCEL_TYPE
                        , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_1, P2.MAP_PARCEL_SEQ AS MAP_PARCEL_SEQ_1, P2.UTMSCALE AS UTMSCALE_1, P2.UTMMAP1 AS UTMMAP1_1
                        , P2.UTMMAP2 AS UTMMAP2_1, P2.UTMMAP3 AS UTMMAP3_1, P2.UTMMAP4 AS UTMMAP4_1, P2.LAND_NO AS LAND_NO_1, P2.PARCEL_TYPE AS PARCEL_TYPE_1
                        , 47 AS ZONE, 'ยู ที เอ็ม' AS RAVANG
                    FROM (
                        SELECT LANDOFFICE_SEQ, MAP_PARCEL_SEQ, UTMSCALE, UTMMAP1, UTMMAP2, UTMMAP3, UTMMAP4, LAND_NO, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MAPDOL.MAP_LAND_GIS_47
                        WHERE LANDOFFICE_SEQ = :landoffice
                        ".$cond."
                        SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, P1.UTMMAP3, P1.UTMMAP4, P1.LAND_NO, NVL(P1.PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MGT1.MAP_LAND_GIS_47 P1
                        WHERE P1.LANDOFFICE_SEQ = :landoffice ) P2
                    LEFT JOIN MGT1.MAP_LAND_GIS_47 P1
                        ON P1.MAP_PARCEL_SEQ = P2.MAP_PARCEL_SEQ
                    LEFT JOIN SVO.TB_UDM_MAS_PARCELTYPE MP
                        ON NVL(P2.PARCEL_TYPE,99) = MP.PARCELTYPE_SEQ
                    WHERE PRINTPLATE_TYPE_SEQ = :printplateType
                    UNION ALL
                    SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, P1.UTMMAP3, P1.UTMMAP4, P1.LAND_NO, P1.PARCEL_TYPE
                        , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_1, P2.MAP_PARCEL_SEQ AS MAP_PARCEL_SEQ_1, P2.UTMSCALE AS UTMSCALE_1, P2.UTMMAP1 AS UTMMAP1_1
                        , P2.UTMMAP2 AS UTMMAP2_1, P2.UTMMAP3 AS UTMMAP3_1, P2.UTMMAP4 AS UTMMAP4_1, P2.LAND_NO AS LAND_NO_1, P2.PARCEL_TYPE AS PARCEL_TYPE_1
                        , 48 AS ZONE, 'ยู ที เอ็ม' AS RAVANG
                    FROM (
                        SELECT LANDOFFICE_SEQ, MAP_PARCEL_SEQ, UTMSCALE, UTMMAP1, UTMMAP2, UTMMAP3, UTMMAP4, LAND_NO, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MAPDOL.MAP_LAND_GIS_48
                        WHERE LANDOFFICE_SEQ = :landoffice
                        ".$cond."
                        SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, P1.UTMMAP3, P1.UTMMAP4, P1.LAND_NO, NVL(P1.PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MGT1.MAP_LAND_GIS_48 P1
                        WHERE P1.LANDOFFICE_SEQ = :landoffice) P2
                    LEFT JOIN MGT1.MAP_LAND_GIS_48 P1
                        ON P1.MAP_PARCEL_SEQ = P2.MAP_PARCEL_SEQ
                    LEFT JOIN SVO.TB_UDM_MAS_PARCELTYPE MP
                        ON NVL(P2.PARCEL_TYPE,99) = MP.PARCELTYPE_SEQ
                    WHERE PRINTPLATE_TYPE_SEQ = :printplateType
                    UNION ALL
                    SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, NULL AS UTMMAP3, P1.UTMMAP4, P1.LAND_NO, P1.PARCEL_TYPE
                        , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_1, P2.MAP_PARCEL_SEQ AS MAP_PARCEL_SEQ_1, P2.UTMSCALE AS UTMSCALE_1, P2.UTMMAP1 AS UTMMAP1_1
                        , P2.UTMMAP2 AS UTMMAP2_1, P2.UTMMAP3 AS UTMMAP3_1, P2.UTMMAP4 AS UTMMAP4_1, P2.LAND_NO AS LAND_NO_1, P2.PARCEL_TYPE AS PARCEL_TYPE_1
                        , 47 AS ZONE, 'ภาพถ่าย น.ส. 3ก' AS RAVANG
                    FROM (
                        SELECT LANDOFFICE_SEQ, MAP_PARCEL_SEQ, UTMSCALE, UTMMAP1, UTMMAP2, NULL AS UTMMAP3, UTMMAP4, LAND_NO, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MAPDOL.MAP_LAND_NS3K_47
                        WHERE LANDOFFICE_SEQ = :landoffice
                        ".$cond."
                        SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, NULL AS UTMMAP3, P1.UTMMAP4, P1.LAND_NO, NVL(P1.PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MGT1.MAP_LAND_NS3K_47 P1
                        WHERE P1.LANDOFFICE_SEQ = :landoffice ) P2
                    LEFT JOIN MGT1.MAP_LAND_NS3K_47 P1
                        ON P1.MAP_PARCEL_SEQ = P2.MAP_PARCEL_SEQ
                    LEFT JOIN SVO.TB_UDM_MAS_PARCELTYPE MP
                        ON NVL(P2.PARCEL_TYPE,99) = MP.PARCELTYPE_SEQ
                    WHERE PRINTPLATE_TYPE_SEQ = :printplateType
                    UNION ALL
                    SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, NULL AS UTMMAP3, P1.UTMMAP4, P1.LAND_NO, P1.PARCEL_TYPE
                        , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_1, P2.MAP_PARCEL_SEQ AS MAP_PARCEL_SEQ_1, P2.UTMSCALE AS UTMSCALE_1, P2.UTMMAP1 AS UTMMAP1_1
                        , P2.UTMMAP2 AS UTMMAP2_1, P2.UTMMAP3 AS UTMMAP3_1, P2.UTMMAP4 AS UTMMAP4_1, P2.LAND_NO AS LAND_NO_1, P2.PARCEL_TYPE AS PARCEL_TYPE_1
                        , 47 AS ZONE, 'ภาพถ่าย น.ส. 3ก' AS RAVANG
                    FROM (
                        SELECT LANDOFFICE_SEQ, MAP_PARCEL_SEQ, UTMSCALE, UTMMAP1, UTMMAP2, NULL AS UTMMAP3, UTMMAP4, LAND_NO, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MAPDOL.MAP_LAND_NS3K_48
                        WHERE LANDOFFICE_SEQ = :landoffice
                        ".$cond."
                        SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, NULL AS UTMMAP3, P1.UTMMAP4, P1.LAND_NO, NVL(P1.PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MGT1.MAP_LAND_NS3K_48 P1
                        WHERE P1.LANDOFFICE_SEQ = :landoffice ) P2
                    LEFT JOIN MGT1.MAP_LAND_NS3K_48 P1
                        ON P1.MAP_PARCEL_SEQ = P2.MAP_PARCEL_SEQ
                    LEFT JOIN SVO.TB_UDM_MAS_PARCELTYPE MP
                        ON NVL(P2.PARCEL_TYPE,99) = MP.PARCELTYPE_SEQ
                    WHERE PRINTPLATE_TYPE_SEQ = :printplateType
                )
                ORDER BY UTMMAP1_1, UTMMAP2_1, UTMMAP3_1, UTMSCALE_1, UTMMAP4_1
                    , REGEXP_SUBSTR(LAND_NO_1, '^\D*') NULLS FIRST, TO_NUMBER(REGEXP_SUBSTR(LAND_NO_1, '\d+'))
                    , MAP_PARCEL_SEQ_1
                ";
    }  else if ($_REQUEST['temp']==1){
        $sql = "SELECT * FROM (
                    SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, P1.UTMMAP3, P1.UTMMAP4, P1.LAND_NO, P1.PARCEL_TYPE
                        , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_1, P2.MAP_PARCEL_SEQ AS MAP_PARCEL_SEQ_1, P2.UTMSCALE AS UTMSCALE_1, P2.UTMMAP1 AS UTMMAP1_1
                        , P2.UTMMAP2 AS UTMMAP2_1, P2.UTMMAP3 AS UTMMAP3_1, P2.UTMMAP4 AS UTMMAP4_1, P2.LAND_NO AS LAND_NO_1, P2.PARCEL_TYPE AS PARCEL_TYPE_1
                        , 47 AS ZONE, 'ยู ที เอ็ม' AS RAVANG
                    FROM (
                        SELECT LANDOFFICE_SEQ, MAP_PARCEL_SEQ, UTMSCALE, UTMMAP1, UTMMAP2, UTMMAP3, UTMMAP4, LAND_NO, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MAPDOL.MAP_LAND_TEMP_47
                        WHERE LANDOFFICE_SEQ = :landoffice
                        ".$cond."
                        SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, P1.UTMMAP3, P1.UTMMAP4, P1.LAND_NO, NVL(P1.PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MGT1.MAP_LAND_TEMP_47 P1
                        WHERE P1.LANDOFFICE_SEQ = :landoffice ) P2
                    LEFT JOIN MGT1.MAP_LAND_TEMP_47 P1
                        ON P1.MAP_PARCEL_SEQ = P2.MAP_PARCEL_SEQ
                    LEFT JOIN SVO.TB_UDM_MAS_PARCELTYPE MP
                        ON NVL(P2.PARCEL_TYPE,99) = MP.PARCELTYPE_SEQ
                    WHERE PRINTPLATE_TYPE_SEQ = :printplateType
                    UNION ALL
                    SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, P1.UTMMAP3, P1.UTMMAP4, P1.LAND_NO, P1.PARCEL_TYPE
                        , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_1, P2.MAP_PARCEL_SEQ AS MAP_PARCEL_SEQ_1, P2.UTMSCALE AS UTMSCALE_1, P2.UTMMAP1 AS UTMMAP1_1
                        , P2.UTMMAP2 AS UTMMAP2_1, P2.UTMMAP3 AS UTMMAP3_1, P2.UTMMAP4 AS UTMMAP4_1, P2.LAND_NO AS LAND_NO_1, P2.PARCEL_TYPE AS PARCEL_TYPE_1
                        , 48 AS ZONE, 'ยู ที เอ็ม' AS RAVANG
                    FROM (
                        SELECT LANDOFFICE_SEQ, MAP_PARCEL_SEQ, UTMSCALE, UTMMAP1, UTMMAP2, UTMMAP3, UTMMAP4, LAND_NO, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MAPDOL.MAP_LAND_TEMP_48
                        WHERE LANDOFFICE_SEQ = :landoffice
                        ".$cond."
                        SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, P1.UTMMAP3, P1.UTMMAP4, P1.LAND_NO, NVL(P1.PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MGT1.MAP_LAND_TEMP_48 P1
                        WHERE P1.LANDOFFICE_SEQ = :landoffice) P2
                    LEFT JOIN MGT1.MAP_LAND_TEMP_48 P1
                        ON P1.MAP_PARCEL_SEQ = P2.MAP_PARCEL_SEQ
                    LEFT JOIN SVO.TB_UDM_MAS_PARCELTYPE MP
                        ON NVL(P2.PARCEL_TYPE,99) = MP.PARCELTYPE_SEQ
                    WHERE PRINTPLATE_TYPE_SEQ = :printplateType
                    UNION ALL
                    SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, NULL AS UTMMAP3, P1.UTMMAP4, P1.LAND_NO, P1.PARCEL_TYPE
                        , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_1, P2.MAP_PARCEL_SEQ AS MAP_PARCEL_SEQ_1, P2.UTMSCALE AS UTMSCALE_1, P2.UTMMAP1 AS UTMMAP1_1
                        , P2.UTMMAP2 AS UTMMAP2_1, P2.UTMMAP3 AS UTMMAP3_1, P2.UTMMAP4 AS UTMMAP4_1, P2.LAND_NO AS LAND_NO_1, P2.PARCEL_TYPE AS PARCEL_TYPE_1
                        , 47 AS ZONE, 'ภาพถ่าย น.ส. 3ก' AS RAVANG
                    FROM (
                        SELECT LANDOFFICE_SEQ, MAP_PARCEL_SEQ, UTMSCALE, UTMMAP1, UTMMAP2, NULL AS UTMMAP3, UTMMAP4, LAND_NO, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MAPDOL.MAP_LAND_NS3K_TEMP_47
                        WHERE LANDOFFICE_SEQ = :landoffice
                        ".$cond."
                        SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, NULL AS UTMMAP3, P1.UTMMAP4, P1.LAND_NO, NVL(P1.PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MGT1.MAP_LAND_NS3K_TEMP_47 P1
                        WHERE P1.LANDOFFICE_SEQ = :landoffice ) P2
                    LEFT JOIN MGT1.MAP_LAND_NS3K_TEMP_47 P1
                        ON P1.MAP_PARCEL_SEQ = P2.MAP_PARCEL_SEQ
                    LEFT JOIN SVO.TB_UDM_MAS_PARCELTYPE MP
                        ON NVL(P2.PARCEL_TYPE,99) = MP.PARCELTYPE_SEQ
                    WHERE PRINTPLATE_TYPE_SEQ = :printplateType
                    UNION ALL
                    SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, NULL AS UTMMAP3, P1.UTMMAP4, P1.LAND_NO, P1.PARCEL_TYPE
                        , P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_1, P2.MAP_PARCEL_SEQ AS MAP_PARCEL_SEQ_1, P2.UTMSCALE AS UTMSCALE_1, P2.UTMMAP1 AS UTMMAP1_1
                        , P2.UTMMAP2 AS UTMMAP2_1, P2.UTMMAP3 AS UTMMAP3_1, P2.UTMMAP4 AS UTMMAP4_1, P2.LAND_NO AS LAND_NO_1, P2.PARCEL_TYPE AS PARCEL_TYPE_1
                        , 48 AS ZONE, 'ภาพถ่าย น.ส. 3ก' AS RAVANG
                    FROM (
                        SELECT LANDOFFICE_SEQ, MAP_PARCEL_SEQ, UTMSCALE, UTMMAP1, UTMMAP2, NULL AS UTMMAP3, UTMMAP4, LAND_NO, NVL(PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MAPDOL.MAP_LAND_NS3K_TEMP_48
                        WHERE LANDOFFICE_SEQ = :landoffice
                        ".$cond."
                        SELECT P1.LANDOFFICE_SEQ, P1.MAP_PARCEL_SEQ, P1.UTMSCALE, P1.UTMMAP1, P1.UTMMAP2, NULL AS UTMMAP3, P1.UTMMAP4, P1.LAND_NO, NVL(P1.PARCEL_TYPE,99) AS PARCEL_TYPE
                        FROM MGT1.MAP_LAND_NS3K_TEMP_48 P1
                        WHERE P1.LANDOFFICE_SEQ = :landoffice ) P2
                    LEFT JOIN MGT1.MAP_LAND_NS3K_TEMP_48 P1
                        ON P1.MAP_PARCEL_SEQ = P2.MAP_PARCEL_SEQ
                    LEFT JOIN SVO.TB_UDM_MAS_PARCELTYPE MP
                        ON NVL(P2.PARCEL_TYPE,99) = MP.PARCELTYPE_SEQ
                    WHERE PRINTPLATE_TYPE_SEQ = :printplateType
                )
                ORDER BY UTMMAP1_1, UTMMAP2_1, UTMMAP3_1, UTMSCALE_1, UTMMAP4_1
                    , REGEXP_SUBSTR(LAND_NO_1, '^\D*') NULLS FIRST, TO_NUMBER(REGEXP_SUBSTR(LAND_NO_1, '\d+'))
                    , MAP_PARCEL_SEQ_1
                ";
        }

    // echo $sql."\n";
    $stid = oci_parse($conn, $sql);
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    oci_bind_by_name($stid, ':printplateType', $printplateType);
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    // echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
?>
