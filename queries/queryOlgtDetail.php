<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';
    
switch ($check) {
    case '1':   //     1 chanode
        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND PARCEL_SEQ IS NULL  ';
        }else{
            $dataSeq1_sql = ' AND PARCEL_SEQ = :dataSeqP1  ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND PARCEL_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND PARCEL_SEQ = :dataSeqP2 ' ;
        }

        $select = "WITH P1 AS(
                    SELECT OH.OLGT_HD_SEQ, OLGT_HD_CON_NAME, OLGT_HD_CON_NO, OLGT_HD_CON_DATE, OLGT_HD_CON_STDTM, OLGT_HD_CON_ENDTM, OLGT_HD_MNY
                        , OH.OLGT_HD_CON_PERIOD_FLG, NVL(OLGT_HD_CON_YEAR,0)||'ปี '||NVL(OLGT_HD_CON_MONTH,0)||'เดือน '||NVL(OLGT_HD_CON_DAY,0)||'วัน' AS DURATION
                        , PARTY_TYPE, OLGT_PMS_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OLGT_PMS_FNAME)||' '||OLGT_PMS_LNAME)
                        ELSE T.TITLE_NAME||OLGT_PMS_FNAME||' '||OLGT_PMS_LNAME END AS OWN
                    FROM MGT1.TB_REG_PARCEL_OLGT_DT POD
                    INNER JOIN MGT1.TB_REG_OLGT_HD OH
                        ON OH.OLGT_HD_SEQ = POD.OLGT_HD_SEQ
                        AND OH.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_REG_OLGT_PMS OP
                        ON OP.OLGT_HD_SEQ = OH.OLGT_HD_SEQ
                        AND OP.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OP.TITLE_SEQ
                    WHERE POD.RECORD_STATUS = 'N' AND OLGT_HD_STS = 1 ".$dataSeq1_sql."
                ),
                P2 AS (
                    SELECT OH.OLGT_HD_SEQ, OLGT_HD_CON_NAME, OLGT_HD_CON_NO, OLGT_HD_CON_DATE, OLGT_HD_CON_STDTM, OLGT_HD_CON_ENDTM, OLGT_HD_MNY
                        , OH.OLGT_HD_CON_PERIOD_FLG, NVL(OLGT_HD_CON_YEAR,0)||'ปี '||NVL(OLGT_HD_CON_MONTH,0)||'เดือน '||NVL(OLGT_HD_CON_DAY,0)||'วัน' AS DURATION
                        , PARTY_TYPE, OLGT_PMS_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OLGT_PMS_FNAME)||' '||OLGT_PMS_LNAME)
                        ELSE T.TITLE_NAME||OLGT_PMS_FNAME||' '||OLGT_PMS_LNAME END AS OWN
                    FROM REG.TB_REG_PARCEL_OLGT_DT POD
                    INNER JOIN REG.TB_REG_OLGT_HD OH
                        ON OH.OLGT_HD_SEQ = POD.OLGT_HD_SEQ
                        AND OH.RECORD_STATUS = 'N'
                    LEFT JOIN REG.TB_REG_OLGT_PMS OP
                        ON OP.OLGT_HD_SEQ = OH.OLGT_HD_SEQ
                        AND OP.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OP.TITLE_SEQ
                    WHERE POD.RECORD_STATUS = 'N' AND OLGT_HD_STS = 1 ".$dataSeq2_sql."
                )
                SELECT  P1.OLGT_HD_SEQ  AS OLGT_HD_SEQ_P1, P2.OLGT_HD_SEQ  AS OLGT_HD_SEQ_P2,
                        P1.OLGT_HD_CON_NAME  AS OLGT_HD_CON_NAME_P1, P2.OLGT_HD_CON_NAME  AS OLGT_HD_CON_NAME_P2,
                        P1.OLGT_HD_CON_NO  AS OLGT_HD_CON_NO_P1, P2.OLGT_HD_CON_NO  AS OLGT_HD_CON_NO_P2,
                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_DATE) ELSE TO_CHAR(P1.OLGT_HD_CON_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_DATE_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_DATE) ELSE TO_CHAR(P2.OLGT_HD_CON_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_DATE_P2,

                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_STDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_STDTM) ELSE TO_CHAR(P1.OLGT_HD_CON_STDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_STDTM_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_STDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_STDTM) ELSE TO_CHAR(P2.OLGT_HD_CON_STDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_STDTM_P2,

                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_ENDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_ENDTM) ELSE TO_CHAR(P1.OLGT_HD_CON_ENDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_ENDTM_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_ENDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_ENDTM) ELSE TO_CHAR(P2.OLGT_HD_CON_ENDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_ENDTM_P2,

                        P1.OLGT_HD_MNY  AS OLGT_HD_MNY_P1, P2.OLGT_HD_MNY  AS OLGT_HD_MNY_P2,
                        P1.OLGT_HD_CON_PERIOD_FLG  AS OLGT_HD_CON_PERIOD_FLG_P1, P2.OLGT_HD_CON_PERIOD_FLG  AS OLGT_HD_CON_PERIOD_FLG_P2,
                        P1.DURATION  AS DURATION_P1, P2.DURATION  AS DURATION_P2,
                        P1.PARTY_TYPE  AS PARTY_TYPE_P1, P2.PARTY_TYPE  AS PARTY_TYPE_P2,
                        P1.OLGT_PMS_PID  AS OLGT_PMS_PID_P1, P2.OLGT_PMS_PID  AS OLGT_PMS_PID_P2,
                        P1.OWN  AS OWN_P1, P2.OWN  AS OWN_P2 
                FROM P1
                FULL OUTER JOIN P2
                ON  P1.OLGT_HD_CON_NAME = P2.OLGT_HD_CON_NAME
                AND P1.OWN = P2.OWN ";

            $stid = oci_parse($conn, $select);
            if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
            if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
            oci_execute($stid);
            break;
            
    case '2':    //     2 chanodeTrajong
    case '3':    //     3 trajong
    case '4':    //     4 ns3a
    case '5':    //     5 ns3
    case '8':    //     8 nsl
    case '23':   //     23 subNsl  
        //$dataSeq_sql = $dataSeq == ''? $dataSeq : ' AND P.PARCEL_LAND_SEQ = :dataSeq ';
//        $typeSeq_sql = $check == ''? $check : ' AND P.PRINTPLATE_TYPE_SEQ = :typeSeq ';

        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND PARCEL_LAND_SEQ IS NULL  ';
        }else{
            $dataSeq1_sql = ' AND PARCEL_LAND_SEQ = :dataSeqP1  ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND PARCEL_LAND_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND PARCEL_LAND_SEQ = :dataSeqP2 ' ;
        }
        $select = "WITH P1 AS(
                    SELECT OH.OLGT_HD_SEQ, OLGT_HD_CON_NAME, OLGT_HD_CON_NO, OLGT_HD_CON_DATE, OLGT_HD_CON_STDTM, OLGT_HD_CON_ENDTM, OLGT_HD_MNY
                        , OH.OLGT_HD_CON_PERIOD_FLG, NVL(OLGT_HD_CON_YEAR,0)||'ปี '||NVL(OLGT_HD_CON_MONTH,0)||'เดือน '||NVL(OLGT_HD_CON_DAY,0)||'วัน' AS DURATION
                        , PARTY_TYPE, OLGT_PMS_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OLGT_PMS_FNAME)||' '||OLGT_PMS_LNAME)
                        ELSE T.TITLE_NAME||OLGT_PMS_FNAME||' '||OLGT_PMS_LNAME END AS OWN
                    FROM MGT1.TB_REG_PARCEL_OLGT_DT POD
                    INNER JOIN MGT1.TB_REG_OLGT_HD OH
                        ON OH.OLGT_HD_SEQ = POD.OLGT_HD_SEQ
                        AND OH.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_REG_OLGT_PMS OP
                        ON OP.OLGT_HD_SEQ = OH.OLGT_HD_SEQ
                        AND OP.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OP.TITLE_SEQ
                    WHERE POD.RECORD_STATUS = 'N' AND OLGT_HD_STS = 1 ".$dataSeq1_sql."
                ),
                P2 AS (
                    SELECT OH.OLGT_HD_SEQ, OLGT_HD_CON_NAME, OLGT_HD_CON_NO, OLGT_HD_CON_DATE, OLGT_HD_CON_STDTM, OLGT_HD_CON_ENDTM, OLGT_HD_MNY
                        , OH.OLGT_HD_CON_PERIOD_FLG, NVL(OLGT_HD_CON_YEAR,0)||'ปี '||NVL(OLGT_HD_CON_MONTH,0)||'เดือน '||NVL(OLGT_HD_CON_DAY,0)||'วัน' AS DURATION
                        , PARTY_TYPE, OLGT_PMS_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OLGT_PMS_FNAME)||' '||OLGT_PMS_LNAME)
                        ELSE T.TITLE_NAME||OLGT_PMS_FNAME||' '||OLGT_PMS_LNAME END AS OWN
                    FROM REG.TB_REG_PARCEL_OLGT_DT POD
                    INNER JOIN REG.TB_REG_OLGT_HD OH
                        ON OH.OLGT_HD_SEQ = POD.OLGT_HD_SEQ
                        AND OH.RECORD_STATUS = 'N'
                    LEFT JOIN REG.TB_REG_OLGT_PMS OP
                        ON OP.OLGT_HD_SEQ = OH.OLGT_HD_SEQ
                        AND OP.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OP.TITLE_SEQ
                    WHERE POD.RECORD_STATUS = 'N' AND OLGT_HD_STS = 1 ".$dataSeq2_sql."
                )
                SELECT  P1.OLGT_HD_SEQ  AS OLGT_HD_SEQ_P1, P2.OLGT_HD_SEQ  AS OLGT_HD_SEQ_P2,
                        P1.OLGT_HD_CON_NAME  AS OLGT_HD_CON_NAME_P1, P2.OLGT_HD_CON_NAME  AS OLGT_HD_CON_NAME_P2,
                        P1.OLGT_HD_CON_NO  AS OLGT_HD_CON_NO_P1, P2.OLGT_HD_CON_NO  AS OLGT_HD_CON_NO_P2,
                        
                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_DATE) ELSE TO_CHAR(P1.OLGT_HD_CON_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_DATE_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_DATE) ELSE TO_CHAR(P2.OLGT_HD_CON_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_DATE_P2,

                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_STDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_STDTM) ELSE TO_CHAR(P1.OLGT_HD_CON_STDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_STDTM_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_STDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_STDTM) ELSE TO_CHAR(P2.OLGT_HD_CON_STDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_STDTM_P2,

                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_ENDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_ENDTM) ELSE TO_CHAR(P1.OLGT_HD_CON_ENDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_ENDTM_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_ENDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_ENDTM) ELSE TO_CHAR(P2.OLGT_HD_CON_ENDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_ENDTM_P2,

                       
                        P1.OLGT_HD_MNY  AS OLGT_HD_MNY_P1, P2.OLGT_HD_MNY  AS OLGT_HD_MNY_P2,
                        P1.OLGT_HD_CON_PERIOD_FLG  AS OLGT_HD_CON_PERIOD_FLG_P1, P2.OLGT_HD_CON_PERIOD_FLG  AS OLGT_HD_CON_PERIOD_FLG_P2,
                        P1.DURATION  AS DURATION_P1, P2.DURATION  AS DURATION_P2,
                        P1.PARTY_TYPE  AS PARTY_TYPE_P1, P2.PARTY_TYPE  AS PARTY_TYPE_P2,
                        P1.OLGT_PMS_PID  AS OLGT_PMS_PID_P1, P2.OLGT_PMS_PID  AS OLGT_PMS_PID_P2,
                        P1.OWN  AS OWN_P1, P2.OWN  AS OWN_P2 
                FROM P1
                FULL OUTER JOIN P2
                ON  P1.OLGT_HD_CON_NAME = P2.OLGT_HD_CON_NAME
                AND P1.OWN = P2.OWN ";

            $stid = oci_parse($conn, $select);
            if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
            if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
            // if ($check != '') oci_bind_by_name($stid, ':typeSeq', $check);
            oci_execute($stid);
            break;

    // case '13':     //     13 condo
    //     if($dataSeqP1 == ''){
    //         $dataSeq1_sql = ' AND P.CONDO_SEQ IS NULL  ';
    //     }else{
    //         $dataSeq1_sql = ' AND P.CONDO_SEQ = :dataSeqP1  ' ;
    //     }

    //     if($dataSeqP2 == ''){
    //         $dataSeq2_sql = ' AND P.CONDO_SEQ IS NULL ';
    //     }else{
    //         $dataSeq2_sql = ' AND P.CONDO_SEQ = :dataSeqP2 ' ;
    //     }

    //     $select = "";

    //         $stid = oci_parse($conn, $select);
    //         if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
    //         if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
    //         oci_execute($stid);
    //         break;

    case '9':    //     9 condoroom
        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND CONDOROOM_SEQ IS NULL  ';
        }else{
            $dataSeq1_sql = ' AND CONDOROOM_SEQ = :dataSeqP1  ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND CONDOROOM_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND CONDOROOM_SEQ = :dataSeqP2 ' ;
        }

        $select = "WITH P1 AS(
                    SELECT OH.OLGT_HD_SEQ, OLGT_HD_CON_NAME, OLGT_HD_CON_NO, OLGT_HD_CON_DATE, OLGT_HD_CON_STDTM, OLGT_HD_CON_ENDTM, OLGT_HD_MNY
                        , OH.OLGT_HD_CON_PERIOD_FLG, NVL(OLGT_HD_CON_YEAR,0)||'ปี '||NVL(OLGT_HD_CON_MONTH,0)||'เดือน '||NVL(OLGT_HD_CON_DAY,0)||'วัน' AS DURATION
                        , PARTY_TYPE, OLGT_PMS_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OLGT_PMS_FNAME)||' '||OLGT_PMS_LNAME)
                        ELSE T.TITLE_NAME||OLGT_PMS_FNAME||' '||OLGT_PMS_LNAME END AS OWN
                    FROM MGT1.TB_REG_CROOM_OLGT_DT POD
                    INNER JOIN MGT1.TB_REG_OLGT_HD OH
                        ON OH.OLGT_HD_SEQ = POD.OLGT_HD_SEQ
                        AND OH.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_REG_OLGT_PMS OP
                        ON OP.OLGT_HD_SEQ = OH.OLGT_HD_SEQ
                        AND OP.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OP.TITLE_SEQ
                    WHERE POD.RECORD_STATUS = 'N' AND OLGT_HD_STS = 1 ".$dataSeq1_sql."
                ),
                P2 AS (
                    SELECT OH.OLGT_HD_SEQ, OLGT_HD_CON_NAME, OLGT_HD_CON_NO, OLGT_HD_CON_DATE, OLGT_HD_CON_STDTM, OLGT_HD_CON_ENDTM, OLGT_HD_MNY
                        , OH.OLGT_HD_CON_PERIOD_FLG, NVL(OLGT_HD_CON_YEAR,0)||'ปี '||NVL(OLGT_HD_CON_MONTH,0)||'เดือน '||NVL(OLGT_HD_CON_DAY,0)||'วัน' AS DURATION
                        , PARTY_TYPE, OLGT_PMS_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OLGT_PMS_FNAME)||' '||OLGT_PMS_LNAME)
                        ELSE T.TITLE_NAME||OLGT_PMS_FNAME||' '||OLGT_PMS_LNAME END AS OWN
                    FROM REG.TB_REG_CROOM_OLGT_DT POD
                    INNER JOIN REG.TB_REG_OLGT_HD OH
                        ON OH.OLGT_HD_SEQ = POD.OLGT_HD_SEQ
                        AND OH.RECORD_STATUS = 'N'
                    LEFT JOIN REG.TB_REG_OLGT_PMS OP
                        ON OP.OLGT_HD_SEQ = OH.OLGT_HD_SEQ
                        AND OP.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OP.TITLE_SEQ
                    WHERE POD.RECORD_STATUS = 'N' AND OLGT_HD_STS = 1 ".$dataSeq2_sql."
                )
                SELECT  P1.OLGT_HD_SEQ  AS OLGT_HD_SEQ_P1, P2.OLGT_HD_SEQ  AS OLGT_HD_SEQ_P2,
                        P1.OLGT_HD_CON_NAME  AS OLGT_HD_CON_NAME_P1, P2.OLGT_HD_CON_NAME  AS OLGT_HD_CON_NAME_P2,
                        P1.OLGT_HD_CON_NO  AS OLGT_HD_CON_NO_P1, P2.OLGT_HD_CON_NO  AS OLGT_HD_CON_NO_P2,

                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_DATE) ELSE TO_CHAR(P1.OLGT_HD_CON_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_DATE_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_DATE) ELSE TO_CHAR(P2.OLGT_HD_CON_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_DATE_P2,

                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_STDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_STDTM) ELSE TO_CHAR(P1.OLGT_HD_CON_STDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_STDTM_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_STDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_STDTM) ELSE TO_CHAR(P2.OLGT_HD_CON_STDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_STDTM_P2,

                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_ENDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_ENDTM) ELSE TO_CHAR(P1.OLGT_HD_CON_ENDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_ENDTM_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_ENDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_ENDTM) ELSE TO_CHAR(P2.OLGT_HD_CON_ENDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_ENDTM_P2,

                        P1.OLGT_HD_MNY  AS OLGT_HD_MNY_P1, P2.OLGT_HD_MNY  AS OLGT_HD_MNY_P2,
                        P1.OLGT_HD_CON_PERIOD_FLG  AS OLGT_HD_CON_PERIOD_FLG_P1, P2.OLGT_HD_CON_PERIOD_FLG  AS OLGT_HD_CON_PERIOD_FLG_P2,
                        P1.DURATION  AS DURATION_P1, P2.DURATION  AS DURATION_P2,
                        P1.PARTY_TYPE  AS PARTY_TYPE_P1, P2.PARTY_TYPE  AS PARTY_TYPE_P2,
                        P1.OLGT_PMS_PID  AS OLGT_PMS_PID_P1, P2.OLGT_PMS_PID  AS OLGT_PMS_PID_P2,
                        P1.OWN  AS OWN_P1, P2.OWN  AS OWN_P2 
                FROM P1
                FULL OUTER JOIN P2
                ON  P1.OLGT_HD_CON_NAME = P2.OLGT_HD_CON_NAME
                AND P1.OWN = P2.OWN ";

            $stid = oci_parse($conn, $select);
            if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
            if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
            oci_execute($stid);
            break;

    case 'c':    //     c construct

        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND CONSTR_SEQ IS NULL ';
        }else{
            $dataSeq1_sql = ' AND CONSTR_SEQ = :dataSeqP1 ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND CONSTR_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND CONSTR_SEQ = :dataSeqP2 ' ;
        }
        // $dataSeq1_sql = $dataSeqP1 == ''? $dataSeqP1 : ' AND PC.CONSTRUCT_SEQ = :dataSeqP1 ';
        // $dataSeq2_sql = $dataSeqP2 == ''? $dataSeqP2 : ' AND PC.CONSTRUCT_SEQ = :dataSeqP2 ';
        $select = "WITH P1 AS(
                    SELECT OH.OLGT_HD_SEQ, OLGT_HD_CON_NAME, OLGT_HD_CON_NO, OLGT_HD_CON_DATE, OLGT_HD_CON_STDTM, OLGT_HD_CON_ENDTM, OLGT_HD_MNY
                        , OH.OLGT_HD_CON_PERIOD_FLG, NVL(OLGT_HD_CON_YEAR,0)||'ปี '||NVL(OLGT_HD_CON_MONTH,0)||'เดือน '||NVL(OLGT_HD_CON_DAY,0)||'วัน' AS DURATION
                        , PARTY_TYPE, OLGT_PMS_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OLGT_PMS_FNAME)||' '||OLGT_PMS_LNAME)
                        ELSE T.TITLE_NAME||OLGT_PMS_FNAME||' '||OLGT_PMS_LNAME END AS OWN
                    FROM MGT1.TB_REG_CONSTR_OLGT_DT POD
                    INNER JOIN MGT1.TB_REG_OLGT_HD OH
                        ON OH.OLGT_HD_SEQ = POD.OLGT_HD_SEQ
                        AND OH.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_REG_OLGT_PMS OP
                        ON OP.OLGT_HD_SEQ = OH.OLGT_HD_SEQ
                        AND OP.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OP.TITLE_SEQ
                    WHERE POD.RECORD_STATUS = 'N' AND OLGT_HD_STS = 1 ".$dataSeq1_sql."
                ),
                P2 AS (
                    SELECT OH.OLGT_HD_SEQ, OLGT_HD_CON_NAME, OLGT_HD_CON_NO, OLGT_HD_CON_DATE, OLGT_HD_CON_STDTM, OLGT_HD_CON_ENDTM, OLGT_HD_MNY
                        , OH.OLGT_HD_CON_PERIOD_FLG, NVL(OLGT_HD_CON_YEAR,0)||'ปี '||NVL(OLGT_HD_CON_MONTH,0)||'เดือน '||NVL(OLGT_HD_CON_DAY,0)||'วัน' AS DURATION
                        , PARTY_TYPE, OLGT_PMS_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OLGT_PMS_FNAME)||' '||OLGT_PMS_LNAME)
                        ELSE T.TITLE_NAME||OLGT_PMS_FNAME||' '||OLGT_PMS_LNAME END AS OWN
                    FROM REG.TB_REG_CONSTR_OLGT_DT POD
                    INNER JOIN REG.TB_REG_OLGT_HD OH
                        ON OH.OLGT_HD_SEQ = POD.OLGT_HD_SEQ
                        AND OH.RECORD_STATUS = 'N'
                    LEFT JOIN REG.TB_REG_OLGT_PMS OP
                        ON OP.OLGT_HD_SEQ = OH.OLGT_HD_SEQ
                        AND OP.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OP.TITLE_SEQ
                    WHERE POD.RECORD_STATUS = 'N' AND OLGT_HD_STS = 1 ".$dataSeq2_sql."
                )
                SELECT  P1.OLGT_HD_SEQ  AS OLGT_HD_SEQ_P1, P2.OLGT_HD_SEQ  AS OLGT_HD_SEQ_P2,
                        P1.OLGT_HD_CON_NAME  AS OLGT_HD_CON_NAME_P1, P2.OLGT_HD_CON_NAME  AS OLGT_HD_CON_NAME_P2,
                        P1.OLGT_HD_CON_NO  AS OLGT_HD_CON_NO_P1, P2.OLGT_HD_CON_NO  AS OLGT_HD_CON_NO_P2,

                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_DATE) ELSE TO_CHAR(P1.OLGT_HD_CON_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_DATE_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_DATE) ELSE TO_CHAR(P2.OLGT_HD_CON_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_DATE_P2,

                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_STDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_STDTM) ELSE TO_CHAR(P1.OLGT_HD_CON_STDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_STDTM_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_STDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_STDTM) ELSE TO_CHAR(P2.OLGT_HD_CON_STDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_STDTM_P2,

                        CASE WHEN SUBSTR(P1.OLGT_HD_CON_ENDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OLGT_HD_CON_ENDTM) ELSE TO_CHAR(P1.OLGT_HD_CON_ENDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_ENDTM_P1,
                        CASE WHEN SUBSTR(P2.OLGT_HD_CON_ENDTM, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OLGT_HD_CON_ENDTM) ELSE TO_CHAR(P2.OLGT_HD_CON_ENDTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OLGT_HD_CON_ENDTM_P2,

                        P1.OLGT_HD_MNY  AS OLGT_HD_MNY_P1, P2.OLGT_HD_MNY  AS OLGT_HD_MNY_P2,
                        P1.OLGT_HD_CON_PERIOD_FLG  AS OLGT_HD_CON_PERIOD_FLG_P1, P2.OLGT_HD_CON_PERIOD_FLG  AS OLGT_HD_CON_PERIOD_FLG_P2,
                        P1.DURATION  AS DURATION_P1, P2.DURATION  AS DURATION_P2,
                        P1.PARTY_TYPE  AS PARTY_TYPE_P1, P2.PARTY_TYPE  AS PARTY_TYPE_P2,
                        P1.OLGT_PMS_PID  AS OLGT_PMS_PID_P1, P2.OLGT_PMS_PID  AS OLGT_PMS_PID_P2,
                        P1.OWN  AS OWN_P1, P2.OWN  AS OWN_P2 
                FROM P1
                FULL OUTER JOIN P2
                ON  P1.OLGT_HD_CON_NAME = P2.OLGT_HD_CON_NAME
                AND P1.OWN = P2.OWN ";

        $stid = oci_parse($conn, $select);
        if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
        if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);

        oci_execute($stid);
        break;
}
?>