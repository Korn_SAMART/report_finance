<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    switch ($table) {
        case 'OrderReciept1':
            $select = "WITH EP AS
                        (
                            SELECT PRO.PROCESS_SEQ, POT.PROCESS_TEMP_SEQ, POT.PROCESS_ORDER_TEMP_DTM, NVL(POT.PROCESS_ORDER_TEMP_TEXT,LPAD(LPAD(PROCESS_ORDER_TEMP_NO,3,'0'),4,'R')) AS PROCESS_ORDER_TEMP_TEXT,
                            PET.PROCESS_EXPENSES_TEMP_SEQ, E.INCOME_ID, E.EXPENSES_NAME, PET.PROCESS_EXPENSES_TEMP_MED, REGISTER_SEQ
                            FROM MGT1.TB_REG_PROCESS PRO
                            LEFT JOIN MGT1.TB_REG_PROCESS_ORDER_TEMP POT
                                ON POT.PROCESS_TEMP_SEQ = PRO.PROCESS_SEQ
                                AND POT.RECORD_STATUS = 'N'
                            LEFT JOIN MGT1.TB_REG_PROCESS_EXPENSES_TEMP PET
                                ON PET.PROCESS_TEMP_SEQ = PRO.PROCESS_SEQ
                                AND PET.PROCESS_ORDER_TEMP_SEQ = POT.PROCESS_ORDER_TEMP_SEQ
                                AND PET.RECORD_STATUS = 'N'
                            LEFT JOIN MGT1.TB_REG_MAS_EXPENSES E
                                ON E.EXPENSES_SEQ = PET.EXPENSES_SEQ
                                AND E.RECORD_STATUS = 'N'
                            WHERE PET.PROCESS_EXPENSES_TEMP_MED <> 0 AND 
                                PRO.PROCESS_SEQ = :processSeqP1
                            ORDER BY INCOME_ID
                        ),
                        RECP AS
                        (
                            SELECT R.ORDER_NO, R.ORDER_DTM, R.RECEIPT_CATEGORY, R.RECEIPT_NO, R.RECEIPT_DTM, R.TOTAL_MNY, MI.INCOME_ID, MI.INCOME_NAME, RPI.INCOME_MNY 
                            FROM MGT1.TB_REG_PROCESS_ORDER_TEMP POT
                            INNER JOIN MGT1.TB_REG_PROCESS PRO
                                ON PRO.PROCESS_SEQ = POT.PROCESS_TEMP_SEQ
                                AND PRO.RECORD_STATUS = 'N'
                            INNER JOIN MGT1.TB_REG_REQUEST REQ
                                ON REQ.REQUEST_SEQ = PRO.REQUEST_SEQ
                                AND REQ.RECORD_STATUS = 'N'
                            LEFT JOIN MGT1.TB_FIN_PAY_RCPT R
                                ON POT.PROCESS_ORDER_TEMP_DTM = R.ORDER_DTM
                                AND NVL(POT.PROCESS_ORDER_TEMP_TEXT,LPAD(LPAD(PROCESS_ORDER_TEMP_NO,3,'0'),4,'R')) = R.ORDER_NO
                                AND R.LANDOFFICE_SEQ = REQ.LANDOFFICE_SEQ
                            LEFT JOIN MGT1.TB_FIN_PAY_RCPT_PART_INCOME RPI
                                ON RPI.RECEIPT_SEQ = R.RECEIPT_SEQ
                                AND RPI.INCOME_MNY <> 0
                                AND RPI.EXC_FLAG = 1
                                AND RPI.RECORD_STATUS = 'N'
                            LEFT JOIN MGT1.TB_FIN_MAS_INCOME MI
                                ON MI.INCOME_ID = RPI.INCOME_ID
                                AND MI.RECORD_STATUS = 'N'
                            WHERE POT.RECORD_STATUS = 'N' AND POT.PROCESS_TEMP_SEQ = :processSeqP1
                            ORDER BY INCOME_ID
                        )
                        SELECT PROCESS_ORDER_TEMP_DTM AS ORDER_DTM, PROCESS_ORDER_TEMP_TEXT AS ORDER_NO, RECP.RECEIPT_DTM, RECP.RECEIPT_NO,
                            EP.INCOME_ID AS REG_INCOME_ID, EP.EXPENSES_NAME AS REG_EXPENSES, EP.PROCESS_EXPENSES_TEMP_MED AS REG_MNY,
                            RECP.INCOME_ID AS FIN_INCOME_ID, RECP.INCOME_NAME AS FIN_EXPENSES, RECP.INCOME_MNY AS FIN_MNY,
                            CASE WHEN (EP.INCOME_ID <> RECP.INCOME_ID) THEN 1 --ประเภทค่าธรรมเนียมไม่ตรง
                            WHEN (EP.PROCESS_EXPENSES_TEMP_MED <> RECP.INCOME_MNY) THEN 2 --ค่าธรรมเนียมไม่ตรง
                            ELSE 0 END AS STS
                        FROM EP
                        LEFT JOIN REG.TB_REG_MAS_REGISTER REG
                            ON EP.REGISTER_SEQ = REG.REGISTER_SEQ
                        FULL OUTER JOIN RECP
                            ON CASE WHEN (REG.REGISTER_CAL_PLOT IS NULL AND EP.INCOME_ID = 7) THEN 8 ELSE EP.INCOME_ID END = RECP.INCOME_ID
                        ORDER BY NVL(EP.INCOME_ID,RECP.INCOME_ID)
                        ";


            $stid = oci_parse($conn, $select);
            //oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($processSeqP1 != '') oci_bind_by_name($stid, ':processSeqP1', $processSeqP1);
            oci_execute($stid);
        break;

        case 'OrderReciept2':
            $select = "WITH EP AS
                    (
                        SELECT PRO.PROCESS_SEQ, POT.PROCESS_TEMP_SEQ, POT.PROCESS_ORDER_TEMP_DTM,  NVL(POT.PROCESS_ORDER_TEMP_TEXT,LPAD(LPAD(PROCESS_ORDER_TEMP_NO,3,'0'),4,'R')) AS PROCESS_ORDER_TEMP_TEXT, 
                        PET.PROCESS_EXPENSES_TEMP_SEQ, E.INCOME_ID, E.EXPENSES_NAME, PET.PROCESS_EXPENSES_TEMP_MED, REGISTER_SEQ
                        FROM REG.TB_REG_PROCESS PRO
                        LEFT JOIN REG.TB_REG_PROCESS_ORDER_TEMP POT
                            ON POT.PROCESS_TEMP_SEQ = PRO.PROCESS_SEQ
                            AND POT.RECORD_STATUS = 'N'
                        LEFT JOIN REG.TB_REG_PROCESS_EXPENSES_TEMP PET
                            ON PET.PROCESS_TEMP_SEQ = PRO.PROCESS_SEQ
                            AND PET.PROCESS_ORDER_TEMP_SEQ = POT.PROCESS_ORDER_TEMP_SEQ
                            AND PET.RECORD_STATUS = 'N'
                        LEFT JOIN REG.TB_REG_MAS_EXPENSES E
                            ON E.EXPENSES_SEQ = PET.EXPENSES_SEQ
                            AND E.RECORD_STATUS = 'N'
                        WHERE PET.PROCESS_EXPENSES_TEMP_MED <> 0 AND 
                            PRO.PROCESS_SEQ = :processSeqP2
                        ORDER BY INCOME_ID
                    ),
                    RECP AS
                    (
                        SELECT R.ORDER_NO, R.ORDER_DTM, R.RECEIPT_CATEGORY, R.RECEIPT_NO, R.RECEIPT_DTM, R.TOTAL_MNY, MI.INCOME_ID, MI.INCOME_NAME, RPI.INCOME_MNY 
                        FROM REG.TB_REG_PROCESS_ORDER_TEMP POT
                        INNER JOIN REG.TB_REG_PROCESS PRO
                            ON PRO.PROCESS_SEQ = POT.PROCESS_TEMP_SEQ
                            AND PRO.RECORD_STATUS = 'N'
                        INNER JOIN REG.TB_REG_REQUEST REQ
                            ON REQ.REQUEST_SEQ = PRO.REQUEST_SEQ
                            AND REQ.RECORD_STATUS = 'N'
                        LEFT JOIN FIN.TB_FIN_PAY_RCPT R
                            ON POT.PROCESS_ORDER_TEMP_DTM = R.ORDER_DTM
                            AND NVL(POT.PROCESS_ORDER_TEMP_TEXT,LPAD(LPAD(PROCESS_ORDER_TEMP_NO,3,'0'),4,'R')) = R.ORDER_NO
                            AND R.LANDOFFICE_SEQ = REQ.LANDOFFICE_SEQ
                        LEFT JOIN FIN.TB_FIN_PAY_RCPT_PART_INCOME RPI
                            ON RPI.RECEIPT_SEQ = R.RECEIPT_SEQ
                            AND RPI.INCOME_MNY <> 0
                            AND RPI.EXC_FLAG = 1
                            AND RPI.RECORD_STATUS = 'N'
                        LEFT JOIN FIN.TB_FIN_MAS_INCOME MI
                            ON MI.INCOME_ID = RPI.INCOME_ID
                            AND MI.RECORD_STATUS = 'N'
                        WHERE POT.RECORD_STATUS = 'N' AND POT.PROCESS_TEMP_SEQ = :processSeqP2
                        ORDER BY MI.INCOME_ID
                    )
                    SELECT PROCESS_ORDER_TEMP_DTM AS ORDER_DTM, PROCESS_ORDER_TEMP_TEXT AS ORDER_NO, RECP.RECEIPT_DTM, RECP.RECEIPT_NO,
                        EP.INCOME_ID AS REG_INCOME_ID, EP.EXPENSES_NAME AS REG_EXPENSES, EP.PROCESS_EXPENSES_TEMP_MED AS REG_MNY,
                        RECP.INCOME_ID AS FIN_INCOME_ID, RECP.INCOME_NAME AS FIN_EXPENSES, RECP.INCOME_MNY AS FIN_MNY,
                        CASE WHEN (NVL(EP.INCOME_ID,0) <> NVL(RECP.INCOME_ID,0)) THEN 1 --ประเภทค่าธรรมเนียมไม่ตรง
                        WHEN (NVL(EP.PROCESS_EXPENSES_TEMP_MED,0) <> NVL(RECP.INCOME_MNY,0)) THEN 2 --ค่าธรรมเนียมไม่ตรง
                        ELSE 0 END AS STS
                    FROM EP
                    LEFT JOIN REG.TB_REG_MAS_REGISTER REG
                        ON EP.REGISTER_SEQ = REG.REGISTER_SEQ
                    FULL OUTER JOIN RECP
                        ON CASE WHEN (REG.REGISTER_CAL_PLOT IS NULL AND EP.INCOME_ID = 7) THEN 8 ELSE EP.INCOME_ID END = RECP.INCOME_ID
                    ORDER BY NVL(EP.INCOME_ID,RECP.INCOME_ID)
                    ";
            

            $stid = oci_parse($conn, $select);
            //oci_bind_by_name($stid, ':landoffice', $landoffice);
            if ($processSeqP2 != '') oci_bind_by_name($stid, ':processSeqP2', $processSeqP2);
            oci_execute($stid);
        break;

        }

?>