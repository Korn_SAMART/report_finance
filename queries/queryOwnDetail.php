<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';
    
switch ($check) {
    case '1':   //     1 chanode
        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND POWN.PARCEL_SEQ IS NULL  ';
        }else{
            $dataSeq1_sql = ' AND POWN.PARCEL_SEQ = :dataSeqP1  ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND POWN.PARCEL_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND POWN.PARCEL_SEQ = :dataSeqP2 ' ;
        }

        $select = "WITH P1 AS( 
                    SELECT PARCEL_OWNER_ORDER AS OWNER_ORDER, OWN.OWNER_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OWN.OWNER_FNAME)||' '||OWN.OWNER_LNAME)
                        ELSE T.TITLE_NAME||OWN.OWNER_FNAME||' '||OWN.OWNER_LNAME END AS OWN, OWN.OWNER_GENDER, OWN.OWNER_BDATE
                        , OWNER_PN_STS, NATIONALITY_NAME, RACE_NAME, RELIGION_NAME
                        , TRIM(TRAILING FROM TF.TITLE_NAME||OWN.OWNER_FATHER_FNAME||' '||OWN.OWNER_FATHER_LNAME) AS FAT
                        , TRIM(TRAILING FROM TM.TITLE_NAME||OWN.OWNER_MOTHER_FNAME||' '||OWN.OWNER_MOTHER_LNAME) AS MOT
                        , NVL(OWNER_REG_HNO,'-')||' หมู่ '||NVL(OWNER_REG_MOO,'-')||' ซอย'||NVL(OWNER_REG_SOI,'-')||' ถนน'||NVL(OWNER_REG_ROAD,'-')||' '||NVL(TB.TAMBOL_NAME,'-')||' '||NVL(AP.AMPHUR_NAME,'-')||' '||NVL(PV.PROVINCE_NAME,'-') AS REG_ADDR
                        , PARCEL_OWNER_REC_DATE AS RECV_DATE, OBTAIN_NAME, PARCEL_OWNER_NUME_NUM||'/'||PARCEL_OWNER_DENO_NUM AS NUM
                    FROM MGT1.TB_REG_PARCEL_OWNER POWN 
                    INNER JOIN MGT1.TB_REG_OWNER OWN
                        ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        AND OWN.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OWN.OWNER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_NATIONALITY MN
                        ON MN.NATIONALITY_SEQ = OWN.NATIONALITY_SEQ
                    LEFT JOIN MGT1.TB_MAS_RACE MR
                        ON MR.RACE_SEQ = OWN.RACE_SEQ
                    LEFT JOIN MGT1.TB_MAS_RELIGION RL
                        ON RL.RELIGION_SEQ = OWN.RELIGION_SEQ
                    LEFT JOIN MGT1.TB_MAS_TITLE TF
                        ON TF.TITLE_SEQ = OWN.OWNER_FATHER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_TITLE TM
                        ON TM.TITLE_SEQ = OWN.OWNER_MOTHER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = OWN.OWNER_REG_TAMBOL_SEQ
                    LEFT JOIN MGT1.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = OWN.OWNER_REG_AMPHUR_SEQ
                    LEFT JOIN MGT1.TB_MAS_PROVINCE PV
                        ON PV.PROVINCE_SEQ = OWN.OWNER_REG_PROVINCE_SEQ
                    LEFT JOIN MGT1.TB_REG_MAS_OBTAIN OB
                        ON OB.OBTAIN_SEQ = POWN.OBTAIN_SEQ
                    WHERE POWN.RECORD_STATUS = 'N' ".$dataSeq1_sql."
            ),
            P2 AS (
                    SELECT PARCEL_OWNER_ORDER AS OWNER_ORDER, OWN.OWNER_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OWN.OWNER_FNAME)||' '||OWN.OWNER_LNAME)
                        ELSE T.TITLE_NAME||OWN.OWNER_FNAME||' '||OWN.OWNER_LNAME END AS OWN, OWN.OWNER_GENDER, OWN.OWNER_BDATE
                        , OWNER_PN_STS, NATIONALITY_NAME, RACE_NAME, RELIGION_NAME
                        , TRIM(TRAILING FROM TF.TITLE_NAME||OWN.OWNER_FATHER_FNAME||' '||OWN.OWNER_FATHER_LNAME) AS FAT
                        , TRIM(TRAILING FROM TM.TITLE_NAME||OWN.OWNER_MOTHER_FNAME||' '||OWN.OWNER_MOTHER_LNAME) AS MOT
                        , NVL(OWNER_REG_HNO,'-')||' หมู่ '||NVL(OWNER_REG_MOO,'-')||' ซอย'||NVL(OWNER_REG_SOI,'-')||' ถนน'||NVL(OWNER_REG_ROAD,'-')||' '||NVL(TB.TAMBOL_NAME,'-')||' '||NVL(AP.AMPHUR_NAME,'-')||' '||NVL(PV.PROVINCE_NAME,'-') AS REG_ADDR
                        , PARCEL_OWNER_REC_DATE AS RECV_DATE, OBTAIN_NAME, PARCEL_OWNER_NUME_NUM||'/'||PARCEL_OWNER_DENO_NUM AS NUM
                    FROM REG.TB_REG_PARCEL_OWNER POWN 
                    INNER JOIN REG.TB_REG_OWNER OWN
                        ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        AND OWN.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OWN.OWNER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_NATIONALITY MN
                        ON MN.NATIONALITY_SEQ = OWN.NATIONALITY_SEQ
                    LEFT JOIN MAS.TB_MAS_RACE MR
                        ON MR.RACE_SEQ = OWN.RACE_SEQ
                    LEFT JOIN MAS.TB_MAS_RELIGION RL
                        ON RL.RELIGION_SEQ = OWN.RELIGION_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TF
                        ON TF.TITLE_SEQ = OWN.OWNER_FATHER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TM
                        ON TM.TITLE_SEQ = OWN.OWNER_MOTHER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = OWN.OWNER_REG_TAMBOL_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = OWN.OWNER_REG_AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_PROVINCE PV
                        ON PV.PROVINCE_SEQ = OWN.OWNER_REG_PROVINCE_SEQ
                    LEFT JOIN REG.TB_REG_MAS_OBTAIN OB
                        ON OB.OBTAIN_SEQ = POWN.OBTAIN_SEQ
                    WHERE POWN.RECORD_STATUS = 'N' ".$dataSeq2_sql."
            )
            SELECT  P1.OWNER_ORDER AS OWNER_ORDER_P1, P2.OWNER_ORDER AS OWNER_ORDER_P2,
                    P1.OWNER_PID AS OWNER_PID_P1, P2.OWNER_PID AS OWNER_PID_P2,
                    P1.OWN AS OWN_P1, P2.OWN AS OWN_P2,
                    P1.OWNER_GENDER  AS OWNER_GENDER_P1, P2.OWNER_GENDER  AS OWNER_GENDER_P2,
                    CASE WHEN SUBSTR(P1.OWNER_BDATE, -4, 4) > 2500 
                    THEN TO_CHAR(P1.OWNER_BDATE) ELSE TO_CHAR(P1.OWNER_BDATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OWNER_BDATE_P1,
                    CASE WHEN SUBSTR(P2.OWNER_BDATE, -4, 4) > 2500 
                    THEN TO_CHAR(P2.OWNER_BDATE) ELSE TO_CHAR(P2.OWNER_BDATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OWNER_BDATE_P2,
                    P1.OWNER_PN_STS  AS  OWNER_PN_STS_P1,  P2.OWNER_PN_STS  AS OWNER_PN_STS_P2,
                    P1.NATIONALITY_NAME  AS  NATIONALITY_NAME_P1,  P2.NATIONALITY_NAME  AS NATIONALITY_NAME_P2,
                    P1.RACE_NAME  AS  RACE_NAME_P1,  P2.RACE_NAME  AS RACE_NAME_P2,
                    P1.RELIGION_NAME  AS  RELIGION_NAME_P1,  P2.RELIGION_NAME  AS RELIGION_NAME_P2,
                    P1.FAT  AS  FAT_P1,  P2.FAT  AS FAT_P2,
                    P1.MOT  AS  MOT_P1,  P2.MOT  AS MOT_P2,
                    P1.REG_ADDR  AS  REG_ADDR_P1,  P2.REG_ADDR  AS REG_ADDR_P2,
                    CASE WHEN SUBSTR(P1.RECV_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P1.RECV_DATE) ELSE TO_CHAR(P1.RECV_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECV_DATE_P1,
                    CASE WHEN SUBSTR(P2.RECV_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P2.RECV_DATE) ELSE TO_CHAR(P2.RECV_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECV_DATE_P2,
                    P1.OBTAIN_NAME  AS  OBTAIN_NAME_P1,  P2.OBTAIN_NAME  AS OBTAIN_NAME_P2,
                    P1.NUM  AS  NUM_P1,  P2.NUM  AS NUM_P2
            FROM P1
            FULL OUTER JOIN P2
                ON  P1.OWN = P2.OWN 
            ORDER BY NVL(P1.OWNER_ORDER,P2.OWNER_ORDER) ";

            $stid = oci_parse($conn, $select);
            if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
            if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
            oci_execute($stid);
            break;
            
    case '2':    //     2 chanodeTrajong
    case '3':    //     3 trajong
    case '4':    //     4 ns3a
    case '5':    //     5 ns3
    case '8':    //     8 nsl
    case '23':   //     23 subNsl  
        //$dataSeq_sql = $dataSeq == ''? $dataSeq : ' AND P.PARCEL_LAND_SEQ = :dataSeq ';
//        $typeSeq_sql = $check == ''? $check : ' AND P.PRINTPLATE_TYPE_SEQ = :typeSeq ';

        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND POWN.PARCEL_LAND_SEQ IS NULL  ';
        }else{
            $dataSeq1_sql = ' AND POWN.PARCEL_LAND_SEQ = :dataSeqP1  ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND POWN.PARCEL_LAND_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND POWN.PARCEL_LAND_SEQ = :dataSeqP2 ' ;
        }
        $select = "WITH P1 AS(
                    SELECT PARCEL_LAND_OWNER_ORDER AS OWNER_ORDER, OWN.OWNER_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OWN.OWNER_FNAME)||' '||OWN.OWNER_LNAME)
                        ELSE T.TITLE_NAME||OWN.OWNER_FNAME||' '||OWN.OWNER_LNAME END AS OWN, OWN.OWNER_GENDER, OWN.OWNER_BDATE
                        , OWNER_PN_STS, NATIONALITY_NAME, RACE_NAME, RELIGION_NAME
                        , TRIM(TRAILING FROM TF.TITLE_NAME||OWN.OWNER_FATHER_FNAME||' '||OWN.OWNER_FATHER_LNAME) AS FAT
                        , TRIM(TRAILING FROM TM.TITLE_NAME||OWN.OWNER_MOTHER_FNAME||' '||OWN.OWNER_MOTHER_LNAME) AS MOT
                        , NVL(OWNER_REG_HNO,'-')||' หมู่ '||NVL(OWNER_REG_MOO,'-')||' ซอย'||NVL(OWNER_REG_SOI,'-')||' ถนน'||NVL(OWNER_REG_ROAD,'-')||' '||NVL(TB.TAMBOL_NAME,'-')||' '||NVL(AP.AMPHUR_NAME,'-')||' '||NVL(PV.PROVINCE_NAME,'-') AS REG_ADDR
                        , PARCEL_LAND_OWNER_REC_DATE AS RECV_DATE, OBTAIN_NAME, PARCEL_LAND_OWNER_NUME_NUM||'/'||PARCEL_LAND_OWNER_DENO_NUM AS NUM
                    FROM MGT1.TB_REG_PARCEL_LAND_OWNER POWN 
                    INNER JOIN MGT1.TB_REG_OWNER OWN
                        ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        AND OWN.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OWN.OWNER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_NATIONALITY MN
                        ON MN.NATIONALITY_SEQ = OWN.NATIONALITY_SEQ
                    LEFT JOIN MGT1.TB_MAS_RACE MR
                        ON MR.RACE_SEQ = OWN.RACE_SEQ
                    LEFT JOIN MGT1.TB_MAS_RELIGION RL
                        ON RL.RELIGION_SEQ = OWN.RELIGION_SEQ
                    LEFT JOIN MGT1.TB_MAS_TITLE TF
                        ON TF.TITLE_SEQ = OWN.OWNER_FATHER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_TITLE TM
                        ON TM.TITLE_SEQ = OWN.OWNER_MOTHER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = OWN.OWNER_REG_TAMBOL_SEQ
                    LEFT JOIN MGT1.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = OWN.OWNER_REG_AMPHUR_SEQ
                    LEFT JOIN MGT1.TB_MAS_PROVINCE PV
                        ON PV.PROVINCE_SEQ = OWN.OWNER_REG_PROVINCE_SEQ
                    LEFT JOIN MGT1.TB_REG_MAS_OBTAIN OB
                        ON OB.OBTAIN_SEQ = POWN.OBTAIN_SEQ
                    WHERE POWN.RECORD_STATUS = 'N' ".$dataSeq1_sql."
                ),
                P2 AS (
                        SELECT PARCEL_LAND_OWNER_ORDER AS OWNER_ORDER, OWN.OWNER_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OWN.OWNER_FNAME)||' '||OWN.OWNER_LNAME)
                        ELSE T.TITLE_NAME||OWN.OWNER_FNAME||' '||OWN.OWNER_LNAME END AS OWN, OWN.OWNER_GENDER, OWN.OWNER_BDATE
                        , OWNER_PN_STS, NATIONALITY_NAME, RACE_NAME, RELIGION_NAME
                        , TRIM(TRAILING FROM TF.TITLE_NAME||OWN.OWNER_FATHER_FNAME||' '||OWN.OWNER_FATHER_LNAME) AS FAT
                        , TRIM(TRAILING FROM TM.TITLE_NAME||OWN.OWNER_MOTHER_FNAME||' '||OWN.OWNER_MOTHER_LNAME) AS MOT
                        , NVL(OWNER_REG_HNO,'-')||' หมู่ '||NVL(OWNER_REG_MOO,'-')||' ซอย'||NVL(OWNER_REG_SOI,'-')||' ถนน'||NVL(OWNER_REG_ROAD,'-')||' '||NVL(TB.TAMBOL_NAME,'-')||' '||NVL(AP.AMPHUR_NAME,'-')||' '||NVL(PV.PROVINCE_NAME,'-') AS REG_ADDR
                        , PARCEL_LAND_OWNER_REC_DATE AS RECV_DATE, OBTAIN_NAME, PARCEL_LAND_OWNER_NUME_NUM||'/'||PARCEL_LAND_OWNER_DENO_NUM AS NUM
                    FROM REG.TB_REG_PARCEL_LAND_OWNER POWN 
                    INNER JOIN REG.TB_REG_OWNER OWN
                        ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        AND OWN.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OWN.OWNER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_NATIONALITY MN
                        ON MN.NATIONALITY_SEQ = OWN.NATIONALITY_SEQ
                    LEFT JOIN MAS.TB_MAS_RACE MR
                        ON MR.RACE_SEQ = OWN.RACE_SEQ
                    LEFT JOIN MAS.TB_MAS_RELIGION RL
                        ON RL.RELIGION_SEQ = OWN.RELIGION_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TF
                        ON TF.TITLE_SEQ = OWN.OWNER_FATHER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TM
                        ON TM.TITLE_SEQ = OWN.OWNER_MOTHER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = OWN.OWNER_REG_TAMBOL_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = OWN.OWNER_REG_AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_PROVINCE PV
                        ON PV.PROVINCE_SEQ = OWN.OWNER_REG_PROVINCE_SEQ
                    LEFT JOIN REG.TB_REG_MAS_OBTAIN OB
                        ON OB.OBTAIN_SEQ = POWN.OBTAIN_SEQ
                    WHERE POWN.RECORD_STATUS = 'N' ".$dataSeq2_sql."
                )
                SELECT  P1.OWNER_ORDER AS OWNER_ORDER_P1, P2.OWNER_ORDER AS OWNER_ORDER_P2,
                        P1.OWNER_PID AS OWNER_PID_P1, P2.OWNER_PID AS OWNER_PID_P2,
                        P1.OWN AS OWN_P1, P2.OWN AS OWN_P2,
                        P1.OWNER_GENDER  AS OWNER_GENDER_P1, P2.OWNER_GENDER  AS OWNER_GENDER_P2,
                        CASE WHEN SUBSTR(P1.OWNER_BDATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OWNER_BDATE) ELSE TO_CHAR(P1.OWNER_BDATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OWNER_BDATE_P1,
                        CASE WHEN SUBSTR(P2.OWNER_BDATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OWNER_BDATE) ELSE TO_CHAR(P2.OWNER_BDATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OWNER_BDATE_P2,    
                        P1.OWNER_PN_STS  AS  OWNER_PN_STS_P1,  P2.OWNER_PN_STS  AS OWNER_PN_STS_P2,
                        P1.NATIONALITY_NAME  AS  NATIONALITY_NAME_P1,  P2.NATIONALITY_NAME  AS NATIONALITY_NAME_P2,
                        P1.RACE_NAME  AS  RACE_NAME_P1,  P2.RACE_NAME  AS RACE_NAME_P2,
                        P1.RELIGION_NAME  AS  RELIGION_NAME_P1,  P2.RELIGION_NAME  AS RELIGION_NAME_P2,
                        P1.FAT  AS  FAT_P1,  P2.FAT  AS FAT_P2,
                        P1.MOT  AS  MOT_P1,  P2.MOT  AS MOT_P2,
                        P1.REG_ADDR  AS  REG_ADDR_P1,  P2.REG_ADDR  AS REG_ADDR_P2,
                        CASE WHEN SUBSTR(P1.RECV_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.RECV_DATE) ELSE TO_CHAR(P1.RECV_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECV_DATE_P1,
                        CASE WHEN SUBSTR(P2.RECV_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.RECV_DATE) ELSE TO_CHAR(P2.RECV_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECV_DATE_P2,   
                        P1.OBTAIN_NAME  AS  OBTAIN_NAME_P1,  P2.OBTAIN_NAME  AS OBTAIN_NAME_P2,
                        P1.NUM  AS  NUM_P1,  P2.NUM  AS NUM_P2
                FROM P1
                FULL OUTER JOIN P2
                ON  P1.OWN = P2.OWN 
                ORDER BY NVL(P1.OWNER_ORDER,P2.OWNER_ORDER)";

            $stid = oci_parse($conn, $select);
            if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
            if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
            // if ($check != '') oci_bind_by_name($stid, ':typeSeq', $check);
            oci_execute($stid);
            break;

    // case '13':     //     13 condo
    //     if($dataSeqP1 == ''){
    //         $dataSeq1_sql = ' AND P.CONDO_SEQ IS NULL  ';
    //     }else{
    //         $dataSeq1_sql = ' AND P.CONDO_SEQ = :dataSeqP1  ' ;
    //     }

    //     if($dataSeqP2 == ''){
    //         $dataSeq2_sql = ' AND P.CONDO_SEQ IS NULL ';
    //     }else{
    //         $dataSeq2_sql = ' AND P.CONDO_SEQ = :dataSeqP2 ' ;
    //     }

    //     $select = "";

    //         $stid = oci_parse($conn, $select);
    //         if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
    //         if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
    //         oci_execute($stid);
    //         break;

    case '9':    //     9 condoroom
        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND POWN.CONDOROOM_SEQ IS NULL  ';
        }else{
            $dataSeq1_sql = ' AND POWN.CONDOROOM_SEQ = :dataSeqP1  ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND POWN.CONDOROOM_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND POWN.CONDOROOM_SEQ = :dataSeqP2 ' ;
        }

        $select = "WITH P1 AS(
                    SELECT CONDOROOM_OWNER_ORDER_NUM AS OWNER_ORDER, OWN.OWNER_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OWN.OWNER_FNAME)||' '||OWN.OWNER_LNAME)
                        ELSE T.TITLE_NAME||OWN.OWNER_FNAME||' '||OWN.OWNER_LNAME END AS OWN, OWN.OWNER_GENDER, OWN.OWNER_BDATE
                        , OWNER_PN_STS, NATIONALITY_NAME, RACE_NAME, RELIGION_NAME
                        , TRIM(TRAILING FROM TF.TITLE_NAME||OWN.OWNER_FATHER_FNAME||' '||OWN.OWNER_FATHER_LNAME) AS FAT
                        , TRIM(TRAILING FROM TM.TITLE_NAME||OWN.OWNER_MOTHER_FNAME||' '||OWN.OWNER_MOTHER_LNAME) AS MOT
                        , NVL(OWNER_REG_HNO,'-')||' หมู่ '||NVL(OWNER_REG_MOO,'-')||' ซอย'||NVL(OWNER_REG_SOI,'-')||' ถนน'||NVL(OWNER_REG_ROAD,'-')||' '||NVL(TB.TAMBOL_NAME,'-')||' '||NVL(AP.AMPHUR_NAME,'-')||' '||NVL(PV.PROVINCE_NAME,'-') AS REG_ADDR
                        , CONDOROOM_OWNER_RECDATE AS RECV_DATE, OBTAIN_NAME, CONDOROOM_OWNER_NUME_NUM||'/'||CONDOROOM_OWNER_DENO_NUM AS NUM
                    FROM MGT1.TB_REG_CONDOROOM_OWNER POWN 
                    INNER JOIN MGT1.TB_REG_OWNER OWN
                        ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        AND OWN.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OWN.OWNER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_NATIONALITY MN
                        ON MN.NATIONALITY_SEQ = OWN.NATIONALITY_SEQ
                    LEFT JOIN MGT1.TB_MAS_RACE MR
                        ON MR.RACE_SEQ = OWN.RACE_SEQ
                    LEFT JOIN MGT1.TB_MAS_RELIGION RL
                        ON RL.RELIGION_SEQ = OWN.RELIGION_SEQ
                    LEFT JOIN MGT1.TB_MAS_TITLE TF
                        ON TF.TITLE_SEQ = OWN.OWNER_FATHER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_TITLE TM
                        ON TM.TITLE_SEQ = OWN.OWNER_MOTHER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = OWN.OWNER_REG_TAMBOL_SEQ
                    LEFT JOIN MGT1.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = OWN.OWNER_REG_AMPHUR_SEQ
                    LEFT JOIN MGT1.TB_MAS_PROVINCE PV
                        ON PV.PROVINCE_SEQ = OWN.OWNER_REG_PROVINCE_SEQ
                    LEFT JOIN MGT1.TB_REG_MAS_OBTAIN OB
                        ON OB.OBTAIN_SEQ = POWN.OBTAIN_SEQ
                    WHERE POWN.RECORD_STATUS = 'N' ".$dataSeq1_sql."
                ),
                P2 AS (
                    SELECT CONDOROOM_OWNER_ORDER_NUM AS OWNER_ORDER, OWN.OWNER_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OWN.OWNER_FNAME)||' '||OWN.OWNER_LNAME)
                        ELSE T.TITLE_NAME||OWN.OWNER_FNAME||' '||OWN.OWNER_LNAME END AS OWN, OWN.OWNER_GENDER, OWN.OWNER_BDATE
                        , OWNER_PN_STS, NATIONALITY_NAME, RACE_NAME, RELIGION_NAME
                        , TRIM(TRAILING FROM TF.TITLE_NAME||OWN.OWNER_FATHER_FNAME||' '||OWN.OWNER_FATHER_LNAME) AS FAT
                        , TRIM(TRAILING FROM TM.TITLE_NAME||OWN.OWNER_MOTHER_FNAME||' '||OWN.OWNER_MOTHER_LNAME) AS MOT
                        , NVL(OWNER_REG_HNO,'-')||' หมู่ '||NVL(OWNER_REG_MOO,'-')||' ซอย'||NVL(OWNER_REG_SOI,'-')||' ถนน'||NVL(OWNER_REG_ROAD,'-')||' '||NVL(TB.TAMBOL_NAME,'-')||' '||NVL(AP.AMPHUR_NAME,'-')||' '||NVL(PV.PROVINCE_NAME,'-') AS REG_ADDR
                        , CONDOROOM_OWNER_RECDATE AS RECV_DATE, OBTAIN_NAME, CONDOROOM_OWNER_NUME_NUM||'/'||CONDOROOM_OWNER_DENO_NUM AS NUM
                    FROM REG.TB_REG_CONDOROOM_OWNER POWN 
                    INNER JOIN REG.TB_REG_OWNER OWN
                        ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        AND OWN.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OWN.OWNER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_NATIONALITY MN
                        ON MN.NATIONALITY_SEQ = OWN.NATIONALITY_SEQ
                    LEFT JOIN MAS.TB_MAS_RACE MR
                        ON MR.RACE_SEQ = OWN.RACE_SEQ
                    LEFT JOIN MAS.TB_MAS_RELIGION RL
                        ON RL.RELIGION_SEQ = OWN.RELIGION_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TF
                        ON TF.TITLE_SEQ = OWN.OWNER_FATHER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TM
                        ON TM.TITLE_SEQ = OWN.OWNER_MOTHER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = OWN.OWNER_REG_TAMBOL_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = OWN.OWNER_REG_AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_PROVINCE PV
                        ON PV.PROVINCE_SEQ = OWN.OWNER_REG_PROVINCE_SEQ
                    LEFT JOIN REG.TB_REG_MAS_OBTAIN OB
                        ON OB.OBTAIN_SEQ = POWN.OBTAIN_SEQ
                    WHERE POWN.RECORD_STATUS = 'N' ".$dataSeq2_sql."
                )
                SELECT  P1.OWNER_ORDER AS OWNER_ORDER_P1, P2.OWNER_ORDER AS OWNER_ORDER_P2,
                        P1.OWNER_PID AS OWNER_PID_P1, P2.OWNER_PID AS OWNER_PID_P2,
                        P1.OWN AS OWN_P1, P2.OWN AS OWN_P2,
                        P1.OWNER_GENDER  AS OWNER_GENDER_P1, P2.OWNER_GENDER  AS OWNER_GENDER_P2,
                        CASE WHEN SUBSTR(P1.OWNER_BDATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OWNER_BDATE) ELSE TO_CHAR(P1.OWNER_BDATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OWNER_BDATE_P1,
                        CASE WHEN SUBSTR(P2.OWNER_BDATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OWNER_BDATE) ELSE TO_CHAR(P2.OWNER_BDATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OWNER_BDATE_P2,
    
                        P1.OWNER_PN_STS  AS  OWNER_PN_STS_P1,  P2.OWNER_PN_STS  AS OWNER_PN_STS_P2,
                        P1.NATIONALITY_NAME  AS  NATIONALITY_NAME_P1,  P2.NATIONALITY_NAME  AS NATIONALITY_NAME_P2,
                        P1.RACE_NAME  AS  RACE_NAME_P1,  P2.RACE_NAME  AS RACE_NAME_P2,
                        P1.RELIGION_NAME  AS  RELIGION_NAME_P1,  P2.RELIGION_NAME  AS RELIGION_NAME_P2,
                        P1.FAT  AS  FAT_P1,  P2.FAT  AS FAT_P2,
                        P1.MOT  AS  MOT_P1,  P2.MOT  AS MOT_P2,
                        P1.REG_ADDR  AS  REG_ADDR_P1,  P2.REG_ADDR  AS REG_ADDR_P2,
                        CASE WHEN SUBSTR(P1.RECV_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.RECV_DATE) ELSE TO_CHAR(P1.RECV_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECV_DATE_P1,
                        CASE WHEN SUBSTR(P2.RECV_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.RECV_DATE) ELSE TO_CHAR(P2.RECV_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECV_DATE_P2,    
                        P1.OBTAIN_NAME  AS  OBTAIN_NAME_P1,  P2.OBTAIN_NAME  AS OBTAIN_NAME_P2,
                        P1.NUM  AS  NUM_P1,  P2.NUM  AS NUM_P2
                FROM P1
                FULL OUTER JOIN P2
                ON  P1.OWN = P2.OWN 
                ORDER BY NVL(P1.OWNER_ORDER,P2.OWNER_ORDER)";

            $stid = oci_parse($conn, $select);
            if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
            if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);
            oci_execute($stid);
            break;

    case 'c':    //     c construct

        if($dataSeqP1 == ''){
            $dataSeq1_sql = ' AND POWN.CONSTRUCT_SEQ IS NULL ';
        }else{
            $dataSeq1_sql = ' AND POWN.CONSTRUCT_SEQ = :dataSeqP1 ' ;
        }

        if($dataSeqP2 == ''){
            $dataSeq2_sql = ' AND POWN.CONSTRUCT_SEQ IS NULL ';
        }else{
            $dataSeq2_sql = ' AND POWN.CONSTRUCT_SEQ = :dataSeqP2 ' ;
        }
        // $dataSeq1_sql = $dataSeqP1 == ''? $dataSeqP1 : ' AND PC.CONSTRUCT_SEQ = :dataSeqP1 ';
        // $dataSeq2_sql = $dataSeqP2 == ''? $dataSeqP2 : ' AND PC.CONSTRUCT_SEQ = :dataSeqP2 ';
        $select = "WITH P1 AS(
                    SELECT CONSTRUCT_OWNER_ORDER AS OWNER_ORDER, OWN.OWNER_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OWN.OWNER_FNAME)||' '||OWN.OWNER_LNAME)
                        ELSE T.TITLE_NAME||OWN.OWNER_FNAME||' '||OWN.OWNER_LNAME END AS OWN, OWN.OWNER_GENDER, OWN.OWNER_BDATE
                        , OWNER_PN_STS, NATIONALITY_NAME, RACE_NAME, RELIGION_NAME
                        , TRIM(TRAILING FROM TF.TITLE_NAME||OWN.OWNER_FATHER_FNAME||' '||OWN.OWNER_FATHER_LNAME) AS FAT
                        , TRIM(TRAILING FROM TM.TITLE_NAME||OWN.OWNER_MOTHER_FNAME||' '||OWN.OWNER_MOTHER_LNAME) AS MOT
                        , NVL(OWNER_REG_HNO,'-')||' หมู่ '||NVL(OWNER_REG_MOO,'-')||' ซอย'||NVL(OWNER_REG_SOI,'-')||' ถนน'||NVL(OWNER_REG_ROAD,'-')||' '||NVL(TB.TAMBOL_NAME,'-')||' '||NVL(AP.AMPHUR_NAME,'-')||' '||NVL(PV.PROVINCE_NAME,'-') AS REG_ADDR
                        , CONSTRUCT_OWNER_REC_DATE AS RECV_DATE, OBTAIN_NAME, CONSTRUCT_OWNER_NUME_NUM||'/'||CONSTRUCT_OWNER_DENO_NUM AS NUM
                    FROM MGT1.TB_REG_CONSTRUCT_OWNER POWN 
                    INNER JOIN MGT1.TB_REG_OWNER OWN
                        ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        AND OWN.RECORD_STATUS = 'N'
                    LEFT JOIN MGT1.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OWN.OWNER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_NATIONALITY MN
                        ON MN.NATIONALITY_SEQ = OWN.NATIONALITY_SEQ
                    LEFT JOIN MGT1.TB_MAS_RACE MR
                        ON MR.RACE_SEQ = OWN.RACE_SEQ
                    LEFT JOIN MGT1.TB_MAS_RELIGION RL
                        ON RL.RELIGION_SEQ = OWN.RELIGION_SEQ
                    LEFT JOIN MGT1.TB_MAS_TITLE TF
                        ON TF.TITLE_SEQ = OWN.OWNER_FATHER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_TITLE TM
                        ON TM.TITLE_SEQ = OWN.OWNER_MOTHER_TITLE_SEQ
                    LEFT JOIN MGT1.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = OWN.OWNER_REG_TAMBOL_SEQ
                    LEFT JOIN MGT1.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = OWN.OWNER_REG_AMPHUR_SEQ
                    LEFT JOIN MGT1.TB_MAS_PROVINCE PV
                        ON PV.PROVINCE_SEQ = OWN.OWNER_REG_PROVINCE_SEQ
                    LEFT JOIN MGT1.TB_REG_MAS_OBTAIN OB
                        ON OB.OBTAIN_SEQ = POWN.OBTAIN_SEQ
                    WHERE POWN.RECORD_STATUS = 'N' ".$dataSeq1_sql."
                ),
                P2 AS (
                    SELECT CONSTRUCT_OWNER_ORDER AS OWNER_ORDER, OWN.OWNER_PID
                        , CASE WHEN T.TITLE_NAME LIKE '%...%' THEN TRIM(TRAILING ' ' FROM REPLACE(T.TITLE_NAME, '...', OWN.OWNER_FNAME)||' '||OWN.OWNER_LNAME)
                        ELSE T.TITLE_NAME||OWN.OWNER_FNAME||' '||OWN.OWNER_LNAME END AS OWN, OWN.OWNER_GENDER, OWN.OWNER_BDATE
                        , OWNER_PN_STS, NATIONALITY_NAME, RACE_NAME, RELIGION_NAME
                        , TRIM(TRAILING FROM TF.TITLE_NAME||OWN.OWNER_FATHER_FNAME||' '||OWN.OWNER_FATHER_LNAME) AS FAT
                        , TRIM(TRAILING FROM TM.TITLE_NAME||OWN.OWNER_MOTHER_FNAME||' '||OWN.OWNER_MOTHER_LNAME) AS MOT
                        , NVL(OWNER_REG_HNO,'-')||' หมู่ '||NVL(OWNER_REG_MOO,'-')||' ซอย'||NVL(OWNER_REG_SOI,'-')||' ถนน'||NVL(OWNER_REG_ROAD,'-')||' '||NVL(TB.TAMBOL_NAME,'-')||' '||NVL(AP.AMPHUR_NAME,'-')||' '||NVL(PV.PROVINCE_NAME,'-') AS REG_ADDR
                        , CONSTRUCT_OWNER_REC_DATE AS RECV_DATE, OBTAIN_NAME, CONSTRUCT_OWNER_NUME_NUM||'/'||CONSTRUCT_OWNER_DENO_NUM AS NUM
                    FROM REG.TB_REG_CONSTRUCT_OWNER POWN 
                    INNER JOIN REG.TB_REG_OWNER OWN
                        ON OWN.OWNER_SEQ = POWN.OWNER_SEQ
                        AND OWN.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = OWN.OWNER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_NATIONALITY MN
                        ON MN.NATIONALITY_SEQ = OWN.NATIONALITY_SEQ
                    LEFT JOIN MAS.TB_MAS_RACE MR
                        ON MR.RACE_SEQ = OWN.RACE_SEQ
                    LEFT JOIN MAS.TB_MAS_RELIGION RL
                        ON RL.RELIGION_SEQ = OWN.RELIGION_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TF
                        ON TF.TITLE_SEQ = OWN.OWNER_FATHER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TM
                        ON TM.TITLE_SEQ = OWN.OWNER_MOTHER_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TAMBOL TB
                        ON TB.TAMBOL_SEQ = OWN.OWNER_REG_TAMBOL_SEQ
                    LEFT JOIN MAS.TB_MAS_AMPHUR AP
                        ON AP.AMPHUR_SEQ = OWN.OWNER_REG_AMPHUR_SEQ
                    LEFT JOIN MAS.TB_MAS_PROVINCE PV
                        ON PV.PROVINCE_SEQ = OWN.OWNER_REG_PROVINCE_SEQ
                    LEFT JOIN REG.TB_REG_MAS_OBTAIN OB
                        ON OB.OBTAIN_SEQ = POWN.OBTAIN_SEQ
                    WHERE POWN.RECORD_STATUS = 'N' ".$dataSeq2_sql."
                )
                SELECT  P1.OWNER_ORDER AS OWNER_ORDER_P1, P2.OWNER_ORDER AS OWNER_ORDER_P2,
                        P1.OWNER_PID AS OWNER_PID_P1, P2.OWNER_PID AS OWNER_PID_P2,
                        P1.OWN AS OWN_P1, P2.OWN AS OWN_P2,
                        P1.OWNER_GENDER  AS OWNER_GENDER_P1, P2.OWNER_GENDER  AS OWNER_GENDER_P2,
                        CASE WHEN SUBSTR(P1.OWNER_BDATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.OWNER_BDATE) ELSE TO_CHAR(P1.OWNER_BDATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OWNER_BDATE_P1,
                        CASE WHEN SUBSTR(P2.OWNER_BDATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.OWNER_BDATE) ELSE TO_CHAR(P2.OWNER_BDATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS OWNER_BDATE_P2,    
                        P1.OWNER_PN_STS  AS  OWNER_PN_STS_P1,  P2.OWNER_PN_STS  AS OWNER_PN_STS_P2,
                        P1.NATIONALITY_NAME  AS  NATIONALITY_NAME_P1,  P2.NATIONALITY_NAME  AS NATIONALITY_NAME_P2,
                        P1.RACE_NAME  AS  RACE_NAME_P1,  P2.RACE_NAME  AS RACE_NAME_P2,
                        P1.RELIGION_NAME  AS  RELIGION_NAME_P1,  P2.RELIGION_NAME  AS RELIGION_NAME_P2,
                        P1.FAT  AS  FAT_P1,  P2.FAT  AS FAT_P2,
                        P1.MOT  AS  MOT_P1,  P2.MOT  AS MOT_P2,
                        P1.REG_ADDR  AS  REG_ADDR_P1,  P2.REG_ADDR  AS REG_ADDR_P2,
                        CASE WHEN SUBSTR(P1.RECV_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P1.RECV_DATE) ELSE TO_CHAR(P1.RECV_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECV_DATE_P1,
                        CASE WHEN SUBSTR(P2.RECV_DATE, -4, 4) > 2500 
                        THEN TO_CHAR(P2.RECV_DATE) ELSE TO_CHAR(P2.RECV_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS RECV_DATE_P2,    
                        P1.OBTAIN_NAME  AS  OBTAIN_NAME_P1,  P2.OBTAIN_NAME  AS OBTAIN_NAME_P2,
                        P1.NUM  AS  NUM_P1,  P2.NUM  AS NUM_P2
                FROM P1
                FULL OUTER JOIN P2
                ON  P1.OWN = P2.OWN 
                ORDER BY NVL(P1.OWNER_ORDER,P2.OWNER_ORDER)";

        $stid = oci_parse($conn, $select);
        if ($dataSeqP1 != '') oci_bind_by_name($stid, ':dataSeqP1', $dataSeqP1);
        if ($dataSeqP2 != '') oci_bind_by_name($stid, ':dataSeqP2', $dataSeqP2);

        oci_execute($stid);
        break;
}
?>