<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $dataSeq_sql = '';
    
    switch ($table) {
        case 'P1':
            if ($check == '13'){
                $dataSeq_sql = ' AND CDC.CONDO_SEQ = :parcelSeqP1 ';
                $select = "WITH PROC AS (
                                SELECT PROCESS_SEQ FROM MGT1.TB_REG_CONDO CDC
                                WHERE nvl(cdc.process_seq,0) <> 0 AND RECORD_STATUS = 'N' ".$dataSeq_sql."
                                UNION  
                                SELECT PROCESS_SEQ FROM MGT1.TB_REG_CONDO_CORPORATE CDC
                                WHERE nvl(cdc.process_seq,0) <> 0 AND RECORD_STATUS = 'N' ".$dataSeq_sql."
                                UNION 
                                SELECT CDR.PROCESS_SEQ FROM MGT1.TB_REG_CONDO_RULE CDR
                                INNER JOIN MGT1.TB_REG_CONDO_CORPORATE CDC
                                    ON CDC.CONDO_CORP_SEQ = CDR.CONDO_CORP_SEQ
                                    AND CDC.RECORD_STATUS = 'N'
                                WHERE nvl(cdr.process_seq,0) <> 0 AND CDR.RECORD_STATUS = 'N' ".$dataSeq_sql."
                                UNION 
                                SELECT CDCT.PROCESS_SEQ FROM MGT1.TB_REG_CONDO_COMMITTEE CDCT
                                INNER JOIN MGT1.TB_REG_CONDO_CORPORATE CDC
                                    ON CDC.CONDO_CORP_SEQ = CDCT.CONDO_CORP_SEQ
                                    AND CDC.RECORD_STATUS = 'N'
                                WHERE nvl(cdct.process_seq,0) <> 0 AND CDCT.RECORD_STATUS = 'N' ".$dataSeq_sql."
                                UNION 
                                SELECT CDA.PROCESS_SEQ FROM MGT1.TB_REG_CONDO_AGENT CDA
                                INNER JOIN MGT1.TB_REG_CONDO_CORPORATE CDC
                                    ON CDC.CONDO_CORP_SEQ = CDA.CONDO_CORP_SEQ
                                    AND CDC.RECORD_STATUS = 'N'
                                WHERE nvl(cda.process_seq,0) <> 0 AND CDA.RECORD_STATUS = 'N' ".$dataSeq_sql."
                            )
                            SELECT CASE WHEN SUBSTR(MQ.MANAGE_QUEUE_DTM, -4, 4) > 2500 
                            THEN TO_CHAR(MQ.MANAGE_QUEUE_DTM) ELSE TO_CHAR(MQ.MANAGE_QUEUE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS MANAGE_QUEUE_DTM,
                            MQ.MANAGE_QUEUE_NO, REQ.REQUEST_TYPE, PRO.PROCESS_SEQ, PRO.PROCESS_ORDER, PRO.PROCESS_REGIST_NAME, TO_NUMBER('1') AS KEY
                            FROM PROC
                            LEFT JOIN MGT1.TB_REG_PROCESS PRO
                                ON PRO.PROCESS_SEQ = PROC.PROCESS_SEQ
                                AND PRO.RECORD_STATUS = 'N'
                            LEFT JOIN MGT1.TB_REG_MANAGE_QUEUE MQ
                                ON MQ.REQUEST_TEMP_SEQ = PRO.REQUEST_SEQ
                                AND MQ.RECORD_STATUS = 'N'
                            LEFT JOIN MGT1.TB_REG_REQUEST REQ
                                ON REQ.REQUEST_SEQ = PRO.REQUEST_SEQ
                                AND REQ.RECORD_STATUS = 'N'
                            ORDER BY TO_DATE(MQ.MANAGE_QUEUE_DTM), MQ.MANAGE_QUEUE_NO, PRO.PROCESS_ORDER, PRO.PROCESS_SEQ ";

                $stid = oci_parse($conn, $select);
                if ($parcelSeqP1 != '') oci_bind_by_name($stid, ':parcelSeqP1', $parcelSeqP1);
                oci_execute($stid);
                break;
            }
            else{
                if ($check == '1') $dataSeq_sql = ' WHERE PPT.PARCEL_SEQ = :parcelSeqP1 ';
                if ($check == '2' || $check == '3' || $check == '4' || $check == '5' || $check == '8' || $check == '23') $dataSeq_sql = ' WHERE PPT.PARCEL_LAND_SEQ = :parcelSeqP1 ';
                if ($check == '9') $dataSeq_sql = ' WHERE PPT.CONDOROOM_SEQ = :parcelSeqP1 ';
                if ($check == 'c') $dataSeq_sql = ' WHERE PPT.CONSTRUCT_SEQ = :parcelSeqP1 ';    
                $select = "SELECT CASE WHEN SUBSTR(MQ.MANAGE_QUEUE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(MQ.MANAGE_QUEUE_DTM) ELSE TO_CHAR(MQ.MANAGE_QUEUE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS MANAGE_QUEUE_DTM
                            , MQ.MANAGE_QUEUE_NO, REQ.REQUEST_TYPE, PRO.PROCESS_SEQ, PRO.PROCESS_ORDER, PRO.PROCESS_REGIST_NAME, TO_NUMBER('1') AS KEY
                            FROM MGT1.TB_REG_PROCESS_PARCEL_TEMP PPT
                            INNER JOIN MGT1.TB_REG_PROCESS PRO
                                ON PRO.PROCESS_SEQ = PPT.PROCESS_TEMP_SEQ
                                AND PRO.RECORD_STATUS = 'N'
                            INNER JOIN MGT1.TB_REG_MANAGE_QUEUE MQ
                                ON MQ.REQUEST_TEMP_SEQ = PRO.REQUEST_SEQ
                                AND MQ.RECORD_STATUS = 'N'
                            INNER JOIN MGT1.TB_REG_REQUEST REQ
                                ON REQ.REQUEST_SEQ = PRO.REQUEST_SEQ
                                AND REQ.RECORD_STATUS = 'N'
                                ".$dataSeq_sql."
                            ORDER BY TO_DATE(MQ.MANAGE_QUEUE_DTM), MQ.MANAGE_QUEUE_NO, PRO.PROCESS_ORDER, PRO.PROCESS_SEQ";

                $stid = oci_parse($conn, $select);
                if ($parcelSeqP1 != '') oci_bind_by_name($stid, ':parcelSeqP1', $parcelSeqP1);
                oci_execute($stid);
            break;
            }
        case 'P2':
            if ($check == '13'){
                $dataSeq_sql = ' AND CDC.CONDO_SEQ = :parcelSeqP2 ';
                $select = "WITH PROC AS (
                            SELECT PROCESS_SEQ FROM REG.TB_REG_CONDO CDC
                            WHERE nvl(cdc.process_seq,0) <> 0 AND RECORD_STATUS = 'N' ".$dataSeq_sql."
                            UNION 
                            SELECT PROCESS_SEQ FROM REG.TB_REG_CONDO_CORPORATE CDC
                            WHERE nvl(cdc.process_seq,0) <> 0 AND RECORD_STATUS = 'N' ".$dataSeq_sql."
                            UNION 
                            SELECT CDR.PROCESS_SEQ FROM REG.TB_REG_CONDO_RULE CDR
                            INNER JOIN REG.TB_REG_CONDO_CORPORATE CDC
                                ON CDC.CONDO_CORP_SEQ = CDR.CONDO_CORP_SEQ
                                AND CDC.RECORD_STATUS = 'N'
                            WHERE nvl(cdr.process_seq,0) <> 0 AND CDR.RECORD_STATUS = 'N' ".$dataSeq_sql."
                            UNION 
                            SELECT CDCT.PROCESS_SEQ FROM REG.TB_REG_CONDO_COMMITTEE CDCT
                            INNER JOIN REG.TB_REG_CONDO_CORPORATE CDC
                                ON CDC.CONDO_CORP_SEQ = CDCT.CONDO_CORP_SEQ
                                AND CDC.RECORD_STATUS = 'N'
                            WHERE nvl(cdct.process_seq,0) <> 0 AND CDCT.RECORD_STATUS = 'N' ".$dataSeq_sql."
                            UNION 
                            SELECT CDA.PROCESS_SEQ FROM REG.TB_REG_CONDO_AGENT CDA
                            INNER JOIN REG.TB_REG_CONDO_CORPORATE CDC
                                ON CDC.CONDO_CORP_SEQ = CDA.CONDO_CORP_SEQ
                                AND CDC.RECORD_STATUS = 'N'
                            WHERE nvl(cda.process_seq,0) <> 0 AND CDA.RECORD_STATUS = 'N' ".$dataSeq_sql."
                        )
                       SELECT CASE WHEN SUBSTR(MQ.MANAGE_QUEUE_DTM, -4, 4) > 2500 
                        THEN TO_CHAR(MQ.MANAGE_QUEUE_DTM) ELSE TO_CHAR(MQ.MANAGE_QUEUE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS MANAGE_QUEUE_DTM
                        , MQ.MANAGE_QUEUE_NO, REQ.REQUEST_TYPE, PRO.PROCESS_SEQ, PRO.PROCESS_ORDER, PRO.PROCESS_REGIST_NAME, TO_NUMBER('2') AS KEY
                        FROM PROC
                        LEFT JOIN REG.TB_REG_PROCESS PRO
                            ON PRO.PROCESS_SEQ = PROC.PROCESS_SEQ
                            AND PRO.RECORD_STATUS = 'N'
                        LEFT JOIN REG.TB_REG_MANAGE_QUEUE MQ
                            ON MQ.REQUEST_TEMP_SEQ = PRO.REQUEST_SEQ
                            AND MQ.RECORD_STATUS = 'N'
                        LEFT JOIN REG.TB_REG_REQUEST REQ
                            ON REQ.REQUEST_SEQ = PRO.REQUEST_SEQ
                            AND REQ.RECORD_STATUS = 'N'
                        ORDER BY TO_DATE(MQ.MANAGE_QUEUE_DTM), MQ.MANAGE_QUEUE_NO, PRO.PROCESS_ORDER, PRO.PROCESS_SEQ ";

                $stid = oci_parse($conn, $select);
                if ($parcelSeqP2 != '') oci_bind_by_name($stid, ':parcelSeqP2', $parcelSeqP2);
                oci_execute($stid);
                break;
            }
            else{
                if ($check == '1') $dataSeq_sql = ' WHERE PPT.PARCEL_SEQ = :parcelSeqP2 ';
                if ($check == '2' || $check == '3' || $check == '4' || $check == '5' || $check == '8' || $check == '23') $dataSeq_sql = ' WHERE PPT.PARCEL_LAND_SEQ = :parcelSeqP2 ';
                if ($check == '9') $dataSeq_sql = ' WHERE PPT.CONDOROOM_SEQ = :parcelSeqP2 ';
                if ($check == 'c') $dataSeq_sql = ' WHERE PPT.CONSTRUCT_SEQ = :parcelSeqP2 ';
                
                $select = " SELECT CASE WHEN SUBSTR(MQ.MANAGE_QUEUE_DTM, -4, 4) > 2500 
                                    THEN TO_CHAR(MQ.MANAGE_QUEUE_DTM) ELSE TO_CHAR(MQ.MANAGE_QUEUE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS MANAGE_QUEUE_DTM
                    , MQ.MANAGE_QUEUE_NO, REQ.REQUEST_TYPE, PRO.PROCESS_SEQ, PRO.PROCESS_ORDER, PRO.PROCESS_REGIST_NAME, TO_NUMBER('2') AS KEY
                            FROM REG.TB_REG_PROCESS_PARCEL_TEMP PPT
                            INNER JOIN REG.TB_REG_PROCESS PRO
                                ON PRO.PROCESS_SEQ = PPT.PROCESS_TEMP_SEQ
                                AND PRO.RECORD_STATUS = 'N'
                            INNER JOIN REG.TB_REG_MANAGE_QUEUE MQ
                                ON MQ.REQUEST_TEMP_SEQ = PRO.REQUEST_SEQ
                                AND MQ.RECORD_STATUS = 'N'
                            INNER JOIN REG.TB_REG_REQUEST REQ
                                ON REQ.REQUEST_SEQ = PRO.REQUEST_SEQ
                                AND REQ.RECORD_STATUS = 'N'
                                ".$dataSeq_sql."
                            ORDER BY TO_DATE(MQ.MANAGE_QUEUE_DTM), MQ.MANAGE_QUEUE_NO, PRO.PROCESS_ORDER, PRO.PROCESS_SEQ ";

                $stid = oci_parse($conn, $select);
                if ($parcelSeqP2 != '') oci_bind_by_name($stid, ':parcelSeqP2', $parcelSeqP2);
                oci_execute($stid);
                break;
            }
        }

?>