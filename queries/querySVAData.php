<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../../database/conn.php';


    switch ($checknum) {
        case '1': //ประเภทการรังวัด
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVA_MAS_TYPEOFSURVEY
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVA_MAS_TYPEOFSURVEY
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.TYPEOFSURVEY_SEQ AS TYPEOFSURVEY_SEQ_P1 ,  P2.TYPEOFSURVEY_SEQ AS TYPEOFSURVEY_SEQ_P2
                        ,TT1.MAINTYPEOFSURVEY_NAME AS MAINTYPEOFSURVEY_NAME_P1 , TT2.MAINTYPEOFSURVEY_NAME AS MAINTYPEOFSURVEY_NAME_P2
                        ,P1.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P1 ,  P2.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P2
                        ,P1.TYPEOFSURVEY_SHORTNAME AS TYPEOFSURVEY_SHORTNAME_P1 ,  P2.TYPEOFSURVEY_SHORTNAME AS TYPEOFSURVEY_SHORTNAME_P2  
                        ,P1.CREATE_DTM AS CREATE_DTM_P1 , P2.CREATE_DTM AS CREATE_DTM_P2
                        ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 , P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.TYPEOFSURVEY_SEQ = P2.TYPEOFSURVEY_SEQ
                    LEFT OUTER JOIN MGT1.TB_SVA_MAS_GROUPTYPE T1
                        ON P1.GROUPTYPE_SEQ = T1.GROUPTYPE_SEQ
                    LEFT OUTER JOIN SVO.TB_SVA_MAS_GROUPTYPE T2
                        ON P1.GROUPTYPE_SEQ = T2.GROUPTYPE_SEQ
                    LEFT OUTER JOIN MGT1.TB_SVA_MAS_MAINTYPEOFSURVEY TT1
                        ON P1.MAINTYPEOFSURVEY_SEQ = TT1.MAINTYPEOFSURVEY_SEQ
                    LEFT OUTER JOIN SVO.TB_SVA_MAS_MAINTYPEOFSURVEY TT2
                        ON P2.MAINTYPEOFSURVEY_SEQ = TT2.MAINTYPEOFSURVEY_SEQ
                    ORDER BY TO_NUMBER(P1.TYPEOFSURVEY_SEQ) ,NLSSORT(TT1.MAINTYPEOFSURVEY_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P1.TYPEOFSURVEY_NAME, 'NLS_SORT=THAI_DICTIONARY')
                            ,TO_NUMBER(P2.TYPEOFSURVEY_SEQ), NLSSORT(TT2.MAINTYPEOFSURVEY_NAME, 'NLS_SORT=THAI_DICTIONARY'), NLSSORT(P2.TYPEOFSURVEY_NAME, 'NLS_SORT=THAI_DICTIONARY')";
             
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '2': //ประเภทหลักฐานการรังวัด    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVA_MAS_SURVEYDOCTYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVA_MAS_SURVEYDOCTYPE
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.SURVEYDOCTYPE_SEQ AS SURVEYDOCTYPE_SEQ_P1 ,  P2.SURVEYDOCTYPE_SEQ AS SURVEYDOCTYPE_SEQ_P2
                        ,P1.SURVEYDOCTYPE_NAME AS SURVEYDOCTYPE_NAME_P1 ,  P2.SURVEYDOCTYPE_NAME AS SURVEYDOCTYPE_NAME_P2
                        ,CASE P1.SURVEYDOCTYPE_GROUP
                        WHEN 'A' THEN 'ต้นร่าง'
                        WHEN 'B' THEN 'รายการรังวัด'
                        WHEN 'C' THEN 'รายการคำนวณ'
                        WHEN 'D' THEN 'อื่นๆ'
                        END AS SURVEYDOCTYPE_GROUP_P1
                        ,CASE P2.SURVEYDOCTYPE_GROUP
                        WHEN 'A' THEN 'ต้นร่าง'
                        WHEN 'B' THEN 'รายการรังวัด'
                        WHEN 'C' THEN 'รายการคำนวณ'
                        WHEN 'D' THEN 'อื่นๆ'
                        END AS SURVEYDOCTYPE_GROUP_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.SURVEYDOCTYPE_SEQ = P2.SURVEYDOCTYPE_SEQ
                    ORDER BY P1.SURVEYDOCTYPE_NAME, P2.SURVEYDOCTYPE_NAME";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '3': //ประเภทเครื่องมือรังวัด   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVA_MAS_EQUIPMENTTYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVA_MAS_EQUIPMENTTYPE
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.EQUIPMENTTYPE_SEQ AS EQUIPMENTTYPE_SEQ_P1 ,  P2.EQUIPMENTTYPE_SEQ AS EQUIPMENTTYPE_SEQ_P2
                        ,P1.EQUIPMENTTYPE_NAME AS EQUIPMENTTYPE_NAME_P1 ,  P2.EQUIPMENTTYPE_NAME AS EQUIPMENTTYPE_NAME_P2
                        ,P1.RECORD_STATUS AS RECORD_STATUS_P1 ,  P2.RECORD_STATUS AS RECORD_STATUS_P2
                        ,P1.CREATE_USER AS CREATE_USER_P1 ,  P2.CREATE_USER AS CREATE_USER_P2
                        ,P1.CREATE_DTM AS CREATE_DTM_P1 ,  P2.CREATE_DTM AS CREATE_DTM_P2
                        ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 ,  P2.LAST_UPD_USER AS LAST_UPD_USER_P2
                        ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 ,  P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
                        ,P2.EQUIPMENTTYPE_ORDER AS EQUIPMENTTYPE_ORDER_P2
                        ,P2.EQUIPMENTTYPE_FULL_NAME AS EQUIPMENTTYPE_FULL_NAME_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.EQUIPMENTTYPE_SEQ = P2.EQUIPMENTTYPE_SEQ
                    ORDER BY NVL(P1.EQUIPMENTTYPE_SEQ,P2.EQUIPMENTTYPE_SEQ)";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '4': //สำนักงานเอกชนรังวัด
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVA_MAS_PRIVATE_OFFICE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVA_MAS_PRIVATE_OFFICE
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.PRIVATE_OFFICE_SEQ AS PRIVATE_OFFICE_SEQ_P1 ,  P2.PRIVATE_OFFICE_SEQ AS PRIVATE_OFFICE_SEQ_P2
                        ,P1.PRIVATE_OFFICE_NAME AS PRIVATE_OFFICE_NAME_P1 ,  P2.PRIVATE_OFFICE_NAME AS PRIVATE_OFFICE_NAME_P2
                        ,P1.PRIVATE_OFFICE_CERTIFICATE AS PRIVATE_OFFICE_CERTIFICATE_P1 ,  P2.PRIVATE_OFFICE_CERTIFICATE AS PRIVATE_OFFICE_CERTIFICATE_P2
                        ,P1.PRIVATE_OFFICE_REGISTERDATE AS PRIVATE_OFFICE_REGIST_DATE_P1 ,  P2.PRIVATE_OFFICE_REGIST_DATE AS PRIVATE_OFFICE_REGIST_DATE_P2
                        ,CASE P1.PRIVATE_OFFICE_STS
                        WHEN '0' THEN 'ไม่สามารถรรับงานได้'
                        WHEN '1' THEN 'รับงานได้'
                        END AS PRIVATE_OFFICE_STS_P1
                        ,CASE P2.PRIVATE_OFFICE_STS
                        WHEN '0' THEN 'ไม่สามารถรรับงานได้'
                        WHEN '1' THEN 'รับงานได้'
                        END AS PRIVATE_OFFICE_STS_P2
                        ,T1.REGION_NAME AS REGION_NAME_P1 , T2.REGION_NAME AS REGION_NAME_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.PRIVATE_OFFICE_SEQ = P2.PRIVATE_OFFICE_SEQ
                    LEFT OUTER JOIN MGT1.TB_MAS_REGION T1
                        ON P1.PRIVATE_OFFICE_REGION_SEQ = T1.REGION_SEQ
                    LEFT OUTER JOIN MAS.TB_MAS_REGION T2
                        ON P2.PRIVATE_OFFICE_REGION_SEQ = T2.REGION_SEQ
                    ORDER BY P1.PRIVATE_OFFICE_SEQ, P2.PRIVATE_OFFICE_SEQ";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '5': //ข้อมูลระวาง    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVA_MAS_SHEET
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVA_MAS_SHEET
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.SHEET_SEQ AS SHEET_SEQ_P1 ,  P2.SHEET_SEQ AS SHEET_SEQ_P2
                        ,T1.SHEETTYPE_NAME AS SHEETTYPE_NAME_P1 ,  T2.SHEETTYPE_NAME AS SHEETTYPE_NAME_P2
                        ,P1.SHEET_UTMMAP1 || ' ' || P1.SHEET_UTMMAP2 || ' ' || P1.SHEET_UTMMAP3 AS UTM_P1
                        ,P2.SHEET_UTMMAP1 || ' ' || P2.SHEET_UTMMAP2 || ' ' || P2.SHEET_UTMMAP3 AS UTM_P2
                        ,P1.SHEET_UTMMAP4 AS SHEET_UTMMAP4_P1 ,  P2.SHEET_UTMMAP4 AS SHEET_UTMMAP4_P2
                        ,TT1.SCALE_NAME AS SCALE_NAME_P1 , TT2.SCALE_NAME AS SCALE_NAME_P2 
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.SHEET_SEQ = P2.SHEET_SEQ
                    LEFT OUTER JOIN MGT1.TB_SVA_MAS_SHEETTYPE T1
                        ON P1.SHEETTYPE_SEQ = T1.SHEETTYPE_SEQ
                    LEFT OUTER JOIN MGT1.TB_SVA_MAS_SHEETTYPE T2
                        ON P2.SHEETTYPE_SEQ = T2.SHEETTYPE_SEQ
                    LEFT OUTER JOIN MGT1.TB_SVA_MAS_SCALE TT1
                        ON P1.SHEET_SCALE_SEQ = TT1.SCALE_SEQ
                    LEFT OUTER JOIN MGT1.TB_SVA_MAS_SCALE TT2
                        ON P2.SHEET_SCALE_SEQ = TT2.SCALE_SEQ
                    ORDER BY NVL(P1.SHEET_SEQ,P2.SHEET_SEQ)";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '6': //สถานะงานรังวัด   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVA_MAS_JOBSTATUS
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVA_MAS_JOBSTATUS
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.JOBSTATUS_SEQ AS JOBSTATUS_SEQ_P1 ,  P2.JOBSTATUS_SEQ AS JOBSTATUS_SEQ_P2
                        ,P1.JOBSTATUS_NAME AS JOBSTATUS_NAME_P1 ,  P2.JOBSTATUS_NAME AS JOBSTATUS_NAME_P2
                        ,P1.JOBSTATUS_ORDER AS JOBSTATUS_ORDER_P1 ,  P2.JOBSTATUS_ORDER AS JOBSTATUS_ORDER_P2
                        ,P1.RECORD_STATUS AS RECORD_STATUS_P1 ,  P2.RECORD_STATUS AS RECORD_STATUS_P2
                        ,P1.CREATE_USER AS CREATE_USER_P1 ,  P2.CREATE_USER AS CREATE_USER_P2
                        ,P1.CREATE_DTM AS CREATE_DTM_P1 ,  P2.CREATE_DTM AS CREATE_DTM_P2
                        ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 ,  P2.LAST_UPD_USER AS LAST_UPD_USER_P2
                        ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 ,  P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.JOBSTATUS_SEQ = P2.JOBSTATUS_SEQ
                    ORDER BY NVL(P1.JOBSTATUS_SEQ,P2.JOBSTATUS_SEQ)";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '7': //ประเภทจดหมายแจ้ง
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVA_MAS_LETTERTYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVA_MAS_LETTERTYPE
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.LETTERTYPE_SEQ AS LETTERTYPE_SEQ_P1 ,  P2.LETTERTYPE_SEQ AS LETTERTYPE_SEQ_P2
                        ,P1.LETTERTYPE_NAME AS LETTERTYPE_NAME_P1 ,  P2.LETTERTYPE_NAME AS LETTERTYPE_NAME_P2
                        ,P1.LETTERTYPE_SHORTNAME AS LETTERTYPE_SHORTNAME_P1 ,  P2.LETTERTYPE_SHORTNAME AS LETTERTYPE_SHORTNAME_P2
                        ,CASE P1.LETTERTYPE_TIME_FLAG 
                        WHEN '1' THEN 'ก่อนรังวัด'
                        WHEN '2' THEN 'หลังรังวัด'
                        WHEN '3' THEN 'เตือน'
                        END AS LETTERTYPE_TIME_FLAG_P1
                        ,CASE P2.LETTERTYPE_TIME_FLAG 
                        WHEN '1' THEN 'ก่อนรังวัด'
                        WHEN '2' THEN 'หลังรังวัด'
                        WHEN '3' THEN 'เตือน'
                        END AS LETTERTYPE_TIME_FLAG_P2                 
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.LETTERTYPE_SEQ = P2.LETTERTYPE_SEQ
                    ORDER BY P1.LETTERTYPE_NAME, P2.LETTERTYPE_NAME";
                     
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '8': //ประเภทกลุ่มงานรังวัด    
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVA_MAS_JOBGROUP
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVA_MAS_JOBGROUP
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.JOBGROUP_SEQ AS JOBGROUP_SEQ_P1 ,  P2.JOBGROUP_SEQ AS JOBGROUP_SEQ_P2
                        ,P1.JOBGROUPNAME AS JOBGROUPNAME_P1 ,  P2.JOBGROUPNAME AS JOBGROUPNAME_P2
                        ,P1.RECORD_STATUS AS RECORD_STATUS_P1 ,  P2.RECORD_STATUS AS RECORD_STATUS_P2
                        ,P1.CREATE_USER AS CREATE_USER_P1 ,  P2.CREATE_USER AS CREATE_USER_P2
                        ,P1.CREATE_DTM AS CREATE_DTM_P1 ,  P2.CREATE_DTM AS CREATE_DTM_P2
                        ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 ,  P2.LAST_UPD_USER AS LAST_UPD_USER_P2
                        ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 ,  P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.JOBGROUP_SEQ = P2.JOBGROUP_SEQ
                    ORDER BY NVL(P1.JOBGROUP_SEQ,P2.JOBGROUP_SEQ)";
                        
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;

        case '9': //ประเภทกลุ่มงานเอกสารสิทธิในที่ดิน   
            $select = "WITH P1 AS(
                        SELECT * 
                        FROM MGT1.TB_SVA_MAS_GROUPTYPE
                        WHERE RECORD_STATUS = 'N' 
                        ),
                        P2 AS(
                        SELECT * 
                        FROM SVO.TB_SVA_MAS_GROUPTYPE
                        WHERE RECORD_STATUS = 'N' 
                        )
                    SELECT P1.GROUPTYPE_SEQ AS GROUPTYPE_SEQ_P1 ,  P2.GROUPTYPE_SEQ AS GROUPTYPE_SEQ_P2
                        ,P1.GROUPTYPE_NAME AS GROUPTYPE_NAME_P1 ,  P2.GROUPTYPE_NAME AS GROUPTYPE_NAME_P2
                        ,P1.RECORD_STATUS AS RECORD_STATUS_P1 ,  P2.RECORD_STATUS AS RECORD_STATUS_P2
                        ,P1.CREATE_USER AS CREATE_USER_P1 ,  P2.CREATE_USER AS CREATE_USER_P2
                        ,P1.CREATE_DTM AS CREATE_DTM_P1 ,  P2.CREATE_DTM AS CREATE_DTM_P2
                        ,P1.LAST_UPD_USER AS LAST_UPD_USER_P1 ,  P2.LAST_UPD_USER AS LAST_UPD_USER_P2
                        ,P1.LAST_UPD_DTM AS LAST_UPD_DTM_P1 ,  P2.LAST_UPD_DTM AS LAST_UPD_DTM_P2
                    FROM P1
                    FULL OUTER JOIN P2
                        ON P1.GROUPTYPE_SEQ = P2.GROUPTYPE_SEQ
                    ORDER BY NVL(P1.GROUPTYPE_SEQ,P2.GROUPTYPE_SEQ)";
                    
            $stid = oci_parse($conn, $select); 
            oci_execute($stid);
        break;


    }
?>