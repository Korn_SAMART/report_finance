<?php
    header('Content-Type: application/json; charset=utf-8');
    // require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';
    
    $landoffice = $_POST['landoffice'];

    $select =  "SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM REG.TB_REG_PARCEL HD
                WHERE HD.LANDOFFICE_SEQ = :landoffice AND HD.RECORD_STATUS = 'N'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.CAVEAT C
                WHERE C.LANDOFFICE_SEQ = :landoffice AND C.CHANODETYPE = 0 AND C.CAVEAT_FLG = '1'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.CAVEAT C
                WHERE C.LANDOFFICE_SEQ = :landoffice AND C.CHANODETYPE = 0 AND C.CAVEAT_FLG = 'D'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM REG.TB_REG_PARCEL_LAND HD
                WHERE HD.LANDOFFICE_SEQ = :landoffice AND HD.RECORD_STATUS = 'N' AND PRINTPLATE_TYPE_SEQ = 2
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.CAVEAT C
                WHERE C.LANDOFFICE_SEQ = :landoffice AND C.CHANODETYPE = 2 AND C.CAVEAT_FLG = '1'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.CAVEAT C
                WHERE C.LANDOFFICE_SEQ = :landoffice AND C.CHANODETYPE = 2 AND C.CAVEAT_FLG = 'D'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM REG.TB_REG_PARCEL_LAND HD
                WHERE HD.LANDOFFICE_SEQ = :landoffice AND HD.RECORD_STATUS = 'N' AND PRINTPLATE_TYPE_SEQ = 3
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.CAVEAT C
                WHERE C.LANDOFFICE_SEQ = :landoffice AND C.CHANODETYPE = 1 AND C.CAVEAT_FLG = '1'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.CAVEAT C
                WHERE C.LANDOFFICE_SEQ = :landoffice AND C.CHANODETYPE = 1 AND C.CAVEAT_FLG = 'D'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM REG.TB_REG_PARCEL_LAND HD
                WHERE HD.LANDOFFICE_SEQ = :landoffice AND HD.RECORD_STATUS = 'N' AND PRINTPLATE_TYPE_SEQ = 5
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.NS3_CAVEAT C
                WHERE C.LANDOFFICE_SEQ = :landoffice AND C.CAVEAT_FLG = '1'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.NS3_CAVEAT C
                WHERE C.LANDOFFICE_SEQ = :landoffice AND C.CAVEAT_FLG = 'D'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM REG.TB_REG_PARCEL_LAND HD
                WHERE HD.LANDOFFICE_SEQ = :landoffice AND HD.RECORD_STATUS = 'N' AND PRINTPLATE_TYPE_SEQ = 4
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.NS3A_CAVEAT C
                WHERE C.LANDOFFICE_SEQ = :landoffice AND C.CAVEAT_FLG = '1'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.NS3A_CAVEAT C
                WHERE C.LANDOFFICE_SEQ = :landoffice AND C.CAVEAT_FLG = 'D'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM REG.TB_REG_PARCEL_LAND HD
                WHERE HD.LANDOFFICE_SEQ = :landoffice AND HD.RECORD_STATUS = 'N' AND PRINTPLATE_TYPE_SEQ = 8
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM REG.TB_REG_PARCEL_LAND HD
                WHERE HD.LANDOFFICE_SEQ = :landoffice AND HD.RECORD_STATUS = 'N' AND PRINTPLATE_TYPE_SEQ = 23
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM REG.TB_REG_CONDO
                WHERE LANDOFFICE_SEQ = :landoffice
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.CONDO_CAVEAT CV
                WHERE CV.LANDOFFICE_SEQ = :landoffice AND CV.CAVEAT_FLG = '1'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM DATAM.CONDO_CAVEAT CV
                WHERE CV.LANDOFFICE_SEQ = :landoffice AND CV.CAVEAT_FLG = 'D'
                UNION ALL
                SELECT DISTINCT COUNT(*) AS COUNT_OVERALL
                FROM REG.TB_REG_CONDOROOM
                WHERE LANDOFFICE_SEQ = :landoffice
                
                ";

    $stid = oci_parse($conn, $select);
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    oci_execute($stid);

    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
