<?php
require '../plugins/vendor/autoload.php';
include '../database/conn.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$Result = array();




$amphur = !isset($_GET['amphur']) ? '' : $_GET['amphur'];
$tambon = !isset($_GET['tambon']) ? '' : $_GET['tambon'];
$parcelNo = !isset($_GET['parcelNo']) ? '' : $_GET['parcelNo']; //รับตัวเดียว

$check = explode('=', $argv[1])[1];
$sts = explode('=', $argv[2])[1];
$landoffice = explode('=', $argv[3])[1];
$branchName = explode('=', $argv[4])[1];

include 'queryAllData.php';
while (($row = oci_fetch_array($stid, OCI_ASSOC))) {

    $Result[] = $row;
}


$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();

if ($check != 13) {
    $spreadsheet->getActiveSheet()->setTitle('ข้อมูลเอกสารสิทธิ');
    $spreadsheet->createSheet(1)->setTitle('ภาระผูกพัน');
    $spreadsheet->createSheet(2)->setTitle('ผู้ถือกรรมสิทธิ์');
    $spreadsheet->createSheet(3)->setTitle('รายการจดทะเบียน');
    $spreadsheet->createSheet(4)->setTitle('ข้อมูลอายัด');
} else {
    $spreadsheet->getActiveSheet()->setTitle('ข้อมูลเอกสารสิทธิ');
    $spreadsheet->createSheet(1)->setTitle('รายการจดทะเบียน');
}


switch ($check) {
    case '1':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $ownerCellIndex = 0;
        $transactionCellIndex = 0;
        $sequesterCellIndex = 0;

        $sequester_seq = array();
        $sequester_index = array();

        $errorParcel = 0;
        $errorOlgt = 0;
        $errorOwner = 0;
        $errortransaction = 0;
        $errorSequester = 0;
        $tempIndex = 0;



        $current = 0;
        //===================PARCEL DETAIL=====================================
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();

            $dataSeqP1 = $Result[$j]['PARCEL_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_SEQ_P2'];

            include 'queryDetailData.php';
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);



            $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));



            if ($Result[$j]['PARCEL_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                if (empty($Result[$j]['PARCEL_NO'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_NO']);
                }
                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
            }

            $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('E' . (6 + $current), 'พัฒน์ฯ 2');
            if (empty($ResultDetail[0]['PARCEL_NO_P1'])) {
                $sheet->setCellValue('F' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (5 + $current), $ResultDetail[0]['PARCEL_NO_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AREA_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultDetail[0]['AREA_P1']);
            }
            if (empty($ResultDetail[0]['UTM_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultDetail[0]['UTM_P1']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultDetail[0]['ORIGIN_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                if (strpos($ResultDetail[0]['OWN_P1'], 'สมเด็จพระ') !== false || strpos($ResultDetail[0]['OWN_P1'], 'พระบาทสม') !== false) {
                    $sheet->setCellValue('L' . (5 + $current), '-------');
                } else {
                    $sheet->setCellValue('L' . (5 + $current), $ResultDetail[0]['OWN_P1']);
                }
            }
            if (empty($ResultDetail[0]['PARCEL_OWNER_NUM_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultDetail[0]['PARCEL_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_OPT_FLAG_P1'])) {
                $sheet->setCellValue('N' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (5 + $current), $ResultDetail[0]['PARCEL_OPT_FLAG_P1'] == 0 ? 'นอกเขตเทศบาล' : 'ในเขตเทศบาล');
            }
            if (empty($ResultDetail[0]['PARCEL_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('O' . (5 + $current), NULL);
            } else {

                $sheet->setCellValue('O' . (5 + $current), $ResultDetail[0]['PARCEL_SEQUEST_STS_P1'] == 1 ? 'ไม่มี' : 'ติดอายัด');

                if ($ResultDetail[0]['PARCEL_SEQUEST_STS_P1'] != 1) {
                    array_push($sequester_seq, $dataSeqP1);
                    array_push($sequester_index, $j);
                }
            }
            if (empty($ResultDetail[0]['PARCELUSED_DESC_P1'])) {
                $sheet->setCellValue('P' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (5 + $current), $ResultDetail[0]['PARCEL_LANDUSED_DESC_P1']);
            }


            if (empty($ResultDetail[0]['PARCEL_NO_P2'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $ResultDetail[0]['PARCEL_NO_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AREA_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultDetail[0]['AREA_P2']);
            }
            if (empty($ResultDetail[0]['UTM_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultDetail[0]['UTM_P2']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultDetail[0]['ORIGIN_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                if (strpos($ResultDetail[0]['OWN_P2'], 'สมเด็จพระ') !== false || strpos($ResultDetail[0]['OWN_P2'], 'พระบาทสม') !== false) {
                    $sheet->setCellValue('L' . (6 + $current), '-------');
                } else {
                    $sheet->setCellValue('L' . (6 + $current), $ResultDetail[0]['OWN_P2']);
                }
            }
            if (empty($ResultDetail[0]['PARCEL_OWNER_NUM_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultDetail[0]['PARCEL_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_OPT_FLAG_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultDetail[0]['PARCEL_OPT_FLAG_P2'] == 0 ? 'นอกเขตเทศบาล' : 'ในเขตเทศบาล');
            }
            if (empty($ResultDetail[0]['PARCEL_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $ResultDetail[0]['PARCEL_SEQUEST_STS_P2'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if (empty($ResultDetail[0]['PARCELUSED_DESC_P2'])) {
                $sheet->setCellValue('P' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (6 + $current), $ResultDetail[0]['PARCEL_LANDUSED_DESC_P2']);
            }

            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒฯ 2";
                } else {
                    if ($ResultDetail[0]['PARCEL_NO_P1'] != $ResultDetail[0]['PARCEL_NO_P2']) $problemDesc .= "เลขเอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['TAMBOL_NAME_P1'] != $ResultDetail[0]['TAMBOL_NAME_P2']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AMPHUR_NAME_P1'] != $ResultDetail[0]['AMPHUR_NAME_P2']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AREA_P1'] != $ResultDetail[0]['AREA_P2']) $problemDesc .= "ขนาดพื้นที่ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['UTM_P1'] != $ResultDetail[0]['UTM_P2']) $problemDesc .= "ระวาง UTM ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['ORIGIN_P1'] != $ResultDetail[0]['ORIGIN_P2']) $problemDesc .= "ระวางศูนย์กำเนิดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['OWN_P1'] != $ResultDetail[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_OWNER_NUM_P1'] != $ResultDetail[0]['PARCEL_OWNER_NUM_P2']) $problemDesc .= "จำนวนผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_OPT_FLAG_P1'] != $ResultDetail[0]['PARCEL_OPT_FLAG_P2']) $problemDesc .= "สถานะ อปท. ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_SEQUEST_STS_P1'] != $ResultDetail[0]['PARCEL_SEQUEST_STS_P2']) $problemDesc .= "สถานะการอายัดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCELUSED_DESC_P1'] != $ResultDetail[0]['PARCELUSED_DESC_P2']) $problemDesc .= "การใช้ประโยชน์ไม่ตรงกัน\n";
                }

                $sheet->mergeCells('Q' . (5 + $current) . ':Q' . (6 + $current));
                $sheet->setCellValue('Q' . (5 + $current), rtrim($problemDesc));
                $problemDesc = '';
            }




            $current += 2;
        }
        $current = 0;
        //=================================== OLGT =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        for ($j = 0; $j < $last; $j++) {

            $ResultOlgt = array();

            $dataSeqP1 = $Result[$j]['PARCEL_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_SEQ_P2'];

            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }


            if ($Result[$j]['PARCEL_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                if (empty($Result[$j]['PARCEL_NO'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_NO']);
                }
                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
            }



            $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('E' . (6 + $current), 'พัฒน์ฯ 2');
            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('F' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('L' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P1'])) {
                    $sheet->setCellValue('M' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('L' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (6 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('M' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (6 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'] != $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']) $problemDesc .= "ชื่อหนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'] != $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']) $problemDesc .= "วันที่หนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'] != $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']) $problemDesc .= "วันที่เริ่มสัญญา\n";
                    if ($ResultOlgt[0]['OLGT_HD_MNY_P1'] != $ResultOlgt[0]['OLGT_HD_MNY_P2']) $problemDesc .= "ราคาทุนทรัพย์ไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['DURATION_P1'] != $ResultOlgt[0]['DURATION_P2']) $problemDesc .= "ระยะเวลาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_PMS_PID_P1'] != $ResultOlgt[0]['OLGT_PMS_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) && $ResultOlgt[0]['OWN_P1'] != $ResultOlgt[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ให้สัญญาไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) && $ResultOlgt[1]['OWN_P1'] != $ResultOlgt[1]['OWN_P2']) $problemDesc .= "ชื่อผู้รับสัญญาไม่ตรงกัน\n";
                }

                $sheet->mergeCells('N' . (5 + $current) . ':N' . (6 + $current));
                $sheet->setCellValue('N' . (5 + $current), rtrim($problemDesc));
                $problemDesc = '';
            }

            $current += 2;
        }

        $current = 0;
        //=================================== OWNER =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        for ($j = 0; $j < $last; $j++) {
            $ResultOwner = array();

            $dataSeqP1 = $Result[$j]['PARCEL_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_SEQ_P2'];


            include 'queryOwnDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOwner[] = $row;
            }


            if (count($ResultOwner) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['PARCEL_SEQ_P2']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['PARCEL_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_NO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultOwner) * 2) - 1));

                $sheet->mergeCells('E' . (5 + $current + (count($ResultOwner))) . ':E' . (5 + $current + (count($ResultOwner) * 2) - 1));

                if ($Result[$j]['PARCEL_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['PARCEL_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_NO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }

                $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('E' . (5 + $current + count($ResultOwner)), 'พัฒน์ฯ 2');
                $ownerDesc = '';

                for ($i = 0; $i < count($ResultOwner); $i++) {
                    if (empty($ResultOwner[$i]['OWNER_ORDER_P1'])) {
                        $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current + $i), $ResultOwner[$i]['OWNER_ORDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P1'])) {
                        $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PID_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P1'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        if (strpos($ResultOwner[$i]['OWN_P1'], 'สมเด็จพระ') !== false || strpos($ResultOwner[$i]['OWN_P1'], 'พระบาทสม') !== false) {
                            $sheet->setCellValue('H' . (5 + $current + $i), '------------');
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultOwner[$i]['OWN_P1']);
                        }
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultOwner[$i]['OWNER_GENDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultOwner[$i]['OWNER_BDATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PN_STS_P1']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultOwner[$i]['NATIONALITY_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P1'])) {
                        $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + $i), $ResultOwner[$i]['RACE_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P1'])) {
                        $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + $i), $ResultOwner[$i]['RELIGION_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P1'])) {
                        $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + $i), $ResultOwner[$i]['FAT_P1']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P1'])) {
                        $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + $i), $ResultOwner[$i]['MOT_P1']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P1'])) {
                        $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + $i), $ResultOwner[$i]['REG_ADDR_P1']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P1'])) {
                        $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + $i), $ResultOwner[$i]['RECV_DATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P1'])) {
                        $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + $i), $ResultOwner[$i]['OBTAIN_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P1'])) {
                        $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + $i), $ResultOwner[$i]['NUM_P1']);
                    }



                    if (empty($ResultOwner[$i]['OWNER_ORDER_P2'])) {
                        $sheet->setCellValue('F' . (5 + $current + count($ResultOwner)  + $i), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_ORDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P2'])) {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PID_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        if (strpos($ResultOwner[$i]['OWN_P2'], 'สมเด็จพระ') !== false || strpos($ResultOwner[$i]['OWN_P2'], 'พระบาทสม') !== false) {
                            $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), '-------');
                        } else {
                            $sheet->setCellValue('H' . (5 + $current +  count($ResultOwner) + $i), $ResultOwner[$i]['OWN_P2']);
                        }
                    }

                    if (empty($ResultOwner[$i]['OWNER_GENDER_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_GENDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_BDATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PN_STS_P2']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NATIONALITY_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P2'])) {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RACE_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P2'])) {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RELIGION_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P2'])) {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['FAT_P2']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P2'])) {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['MOT_P2']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P2'])) {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['REG_ADDR_P2']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P2'])) {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RECV_DATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P2'])) {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OBTAIN_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P2'])) {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NUM_P2']);
                    }

                    $ownerCellIndex = (5 + $current + count($ResultOwner) + $i);

                    if ($sts == 'e') {
                        $problemDesc = '';
                        if (!$dataSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒฯ 1";
                        } else if (!$dataSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒฯ 2";
                        } else {
                            if ($ResultOwner[$i]['OWNER_ORDER_P1'] != $ResultOwner[$i]['OWNER_ORDER_P2']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PID_P1'] != $ResultOwner[$i]['OWNER_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWN_P1'] != $ResultOwner[$i]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_GENDER_P1'] != $ResultOwner[$i]['OWNER_GENDER_P2']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_BDATE_P1'] != $ResultOwner[$i]['OWNER_BDATE_P2']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PN_STS_P1'] != $ResultOwner[$i]['OWNER_PN_STS_P2']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NATIONALITY_NAME_P1'] != $ResultOwner[$i]['NATIONALITY_NAME_P2']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RACE_NAME_P1'] != $ResultOwner[$i]['RACE_NAME_P2']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RELIGION_NAME_P1'] != $ResultOwner[$i]['RELIGION_NAME_P2']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['FAT_P1'] != $ResultOwner[$i]['FAT_P2']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['MOT_P1'] != $ResultOwner[$i]['MOT_P2']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['REG_ADDR_P1'] != $ResultOwner[$i]['REG_ADDR_P2']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RECV_DATE_P1'] != $ResultOwner[$i]['RECV_DATE_P2']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OBTAIN_NAME_P1'] != $ResultOwner[$i]['OBTAIN_NAME_P2']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NUM_P1'] != $ResultOwner[$i]['NUM_P2']) $problemDesc .= "สัดส่วนการได้มาไม่ตรงกัน\n";
                        }
                        $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultOwner) * 2) - 1));
                        $sheet->setCellValue('U' . (5 + $current + $i), rtrim($problemDesc));
                        $problemDesc = '';
                    }
                }

                $current += count($ResultOwner) * 2;
            }
        }

        //=========================== PROCESS TRANSACTION ==========================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(3);

        $processP1 = array();
        $processP2 = array();
        for ($j = 0; $j < $last; $j++) {
            $ResultProcess = array();

            $parcelSeqP1 = $Result[$j]['PARCEL_SEQ_P1'];
            $parcelSeqP2 = $Result[$j]['PARCEL_SEQ_P2'];


            if ($parcelSeqP1 != null) {
                $table = 'P1';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }
            if ($parcelSeqP2 != null) {
                $table = 'P2';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }


            if (count($ResultProcess) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['PARCEL_SEQ_P2']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['PARCEL_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_NO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + count($ResultProcess) - 1));
                //   $sheet->mergeCells('E' . (5 + $current + (count($ResultProcess) / 2)) . ':E' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('J' . (5 + $current) . ':J' . (5 + $current + count($ResultProcess) - 1));
                if ($Result[$j]['PARCEL_SEQ_P2']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['PARCEL_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_NO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }
                $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
                //                $sheet->setCellValue('E' . (5 + $current + (count($ResultProcess) / 2)), 'พัฒน์ฯ 2');
                $ownerDesc = '';


                for ($i = 0; $i < count($ResultProcess); $i++) {
                    if ($ResultProcess[$i]['KEY'] == '1') {
                        array_push($processP1, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('F' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    } else if ($ResultProcess[$i]['KEY'] == '2') {
                        array_push($processP2, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('F' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    }
                    $transactionCellIndex = (5 + $current + $i);
                }

                $current += count($ResultProcess);
            }
        }

        $current = 0;



        if ($sts == 'e') {
            for ($i = 0; $i < $last; $i++) {
                $problemDesc = '';
                if (!$parcelSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$parcelSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($processP1[$i]['MANAGE_QUEUE_DTM'] != $processP2[$i]['MANAGE_QUEUE_DTM']) $problemDesc .= "วันที่จดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['MANAGE_QUEUE_NO'] != $processP2[$i]['MANAGE_QUEUE_NO']) $problemDesc .= "เลขคิวไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_ORDER'] != $processP2[$i]['PROCESS_ORDER']) $problemDesc .= "ลำดับการจดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_REGIST_NAME'] != $processP2[$i]['PROCESS_REGIST_NAME']) $problemDesc .= "ชื่อรายการจดทะเบียนไม่ตรงกัน\n";
                }
                $sheet->setCellValue('J' . (5 + $current), rtrim($problemDesc));
                $current += 2;
            }
        }



        //================== SEQUESTER ===============================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        for ($j = 0; $j < count($sequester_seq); $j++) {

            $ResultSequester = array();


            $parcelSeqP1 = $sequester_seq[$j];
            $parcelSeqP2 = $sequester_seq[$j];
            include 'sequesterEachParcelExcel.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultSequester[] = $row;
            }




            $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));



            $sheet->setCellValue('A' . (5 + $current), ($j + 1));
            $sheet->setCellValue('B' . (5 + $current), $Result[$sequester_index[$j]]['PARCEL_NO']);
            $sheet->setCellValue('C' . (5 + $current), $Result[$sequester_index[$j]]['AMPHUR_NAME']);
            $sheet->setCellValue('D' . (5 + $current), $Result[$sequester_index[$j]]['TAMBOL_NAME']);



            $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('T' . (5 + $current) . ':T' . (5 + $current + (count($ResultSequester) * 2) + 1));


            // $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultSequester) / 2) - 1));
            // $sheet->mergeCells('E' . (5 + $current + (count($ResultSequester) / 2)) . ':E' . (5 + $current + (count($ResultSequester)*2) - 1));


            $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('E' . (5 + $current + count($ResultSequester)), 'พัฒน์ฯ 2');
            $ownerDesc = '';
            //  $current += 2;

            for ($i = 0; $i < count($ResultSequester); $i++) {
                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO'])) {
                    $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO'])) {
                    $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM'])) {
                    $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT'])) {
                    $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_FNAME'])) {
                    $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_FNAME']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'])) {
                    $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM'])) {
                    $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM'])) {
                    $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'])) {
                    $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'])) {
                    $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM'])) {
                    $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'])) {
                    $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME'])) {
                    $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('s' . (5 + $current + $i), $ResultSequester[$i]['TITLE_NAME_OWN'] . $ResultSequester[$i]['OWNER_FNAME'] . ' ' . $ResultSequester[$i]['OWNER_LNAME']);
                }

                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO_1'])) {
                    $sheet->setCellValue('F' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO_1'])) {
                    $sheet->setCellValue('G' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM_1'])) {
                    $sheet->setCellValue('H' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT_1'])) {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_FNAME'])) {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_FNAME']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1'])) {
                    $sheet->setCellValue('L' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM_1'])) {
                    $sheet->setCellValue('M' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM_1'])) {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1'])) {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1'])) {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM_1'])) {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1'])) {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME_1'])) {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('s' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['TITLE_NAME_OWN_1'] . $ResultSequester[$i]['OWNER_FNAME_1'] . ' ' . $ResultSequester[$i]['OWNER_LNAME_1']);
                }

                $tempIndex = 0;


                if ($sts == 'e') {

                    $problemDesc = '';
                    if (!$dataSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$dataSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                    } else {
                        if ($ResultSequester[$i]['SEQUESTER_ORDER_NO'] != $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_NO'] != $ResultSequester[$i]['SEQUESTER_DOC_NO_1']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_DTM'] != $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DEPT'] != $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_PID'] != $ResultSequester[$i]['SEQUESTER_REQ_PID_1']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_FNAME'] != $ResultSequester[$i]['OWNER_FNAME_1']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DTM'] != $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REC_DTM'] != $ResultSequester[$i]['SEQUESTER_REC_DTM_1']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REM'] != $ResultSequester[$i]['SEQUESTER_REM_1']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'] != $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_NAME'] != $ResultSequester[$i]['OWNER_NAME_1']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                    }

                    $sheet->setCellValue('S' . (5 + $current + $i), rtrim($problemDesc));


                    $problemDesc = '';
                }
            }

            $current += count($ResultSequester) * 2;
            $sequesterCellIndex = $current;
        }

        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $sheet->mergeCells('A1:Q1');
        $sheet->mergeCells('A2:Q2');
        $sheet->mergeCells('A3:Q3');


        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('E4', 'พัฒน์ฯ 1/2')
            ->setCellValue('F4', 'เลขที่เอกสารสิทธิ')
            ->setCellValue('G4', 'ตำบล/แขวง')
            ->setCellValue('H4', 'อำเภอ/เขต')
            ->setCellValue('I4', 'ขนาดพื้นที่')
            ->setCellValue('J4', 'ระวาง UTM')
            ->setCellValue('K4', 'ระวางศูนย์กำเนิด')
            ->setCellValue('L4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'สถานะ อปท.')
            ->setCellValue('O4', 'สถานะการอายัด')
            ->setCellValue('P4', 'การใช้ประโยชน์')
            ->setCellValue('Q4', 'หมายเหตุ')
            ->getStyle('A1:Q4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('Q4:Q' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);

        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->mergeCells('A1:N1');
        $sheet->mergeCells('A2:N2');
        $sheet->mergeCells('A3:N3');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->setCellValue('E4', 'พัฒน์ฯ 1/2')
            ->setCellValue('F4', 'ชื่อหนังสือสัญญา')
            ->setCellValue('G4', 'วันที่หนังสือสัญญา')
            ->setCellValue('H4', 'วันที่เริ่มสัญญา')
            ->setCellValue('I4', 'ราคาทุนทรัพย์')
            ->setCellValue('J4', 'ระยะเวลา')
            ->setCellValue('K4', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('L4', 'ผู้ให้สัญญา')
            ->setCellValue('M4', 'ผู้รับสัญญา')
            ->setCellValue('N4', 'หมายเหตุ')
            ->getStyle('A1:N4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$sheet->getStyle('N4:N' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        $sheet->mergeCells('A1:U1');
        $sheet->mergeCells('A2:U2');
        $sheet->mergeCells('A3:U3');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('G4:G' . $ownerCellIndex)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);

        $sheet->setCellValue('E4', 'พัฒน์ฯ 1/2')
            ->setCellValue('F4', 'ลำดับผู้ถือกรรมสิทธิ์')
            ->setCellValue('G4', 'เลขบัตรประจำตัวประชาชน')
            ->setCellValue('H4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('I4', 'เพศผู้ถือกรรมสิทธิ์')
            ->setCellValue('J4', 'วันเกิดผู้ถือกรรมสิทธิ์')
            ->setCellValue('K4', 'สถานะสมรสผู้ถือกรรมสิทธิ์')
            ->setCellValue('L4', 'สัญชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'เชื้อชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'ศาสนาผู้ถือกรรมสิทธิ์')
            ->setCellValue('O4', 'ชื่อบิดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('P4', 'ชื่อมารดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('Q4', 'ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์')
            ->setCellValue('R4', 'วันที่ได้มา')
            ->setCellValue('S4', 'การได้มา')
            ->setCellValue('T4', 'สัดส่วนการได้มา')
            ->setCellValue('U4', 'หมายเหตุ')
            ->getStyle('A1:U4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('U4:U' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        $sheet->mergeCells('A3:J3');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->setCellValue('E4', 'พัฒน์ฯ 1/2')
            ->setCellValue('F4', 'วันที่จดทะเบียน')
            ->setCellValue('G4', 'เลขคิว')
            ->setCellValue('H4', 'ลำดับการจดทะเบียน')
            ->setCellValue('I4', 'ชื่อรายการจดทะเบียน')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A1:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J4:J' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        $sheet->mergeCells('A1:T1');
        $sheet->mergeCells('A2:T2');
        $sheet->mergeCells('A3:T3');
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดที่ดิน';
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->setCellValue('E4', 'พัฒน์ฯ 1/2')
            ->setCellValue('F4', 'อายัดลำดับที่')
            ->setCellValue('G4', 'เลขที่คำสั่ง/หนังสือ')
            ->setCellValue('H4', 'วันที่คำสั่ง/หนังสือ')
            ->setCellValue('I4', 'หน่วยงานที่ขออายัด')
            ->setCellValue('J4', 'เลขที่บัตรประชาชนของผู้ขออายัด')
            ->setCellValue('K4', 'ชื่อ-สกุลผู้ขออายัด')
            ->setCellValue('L4', 'สถานะกำหนดอายัด')
            ->setCellValue('M4', 'วันที่ขออายัด')
            ->setCellValue('N4', 'วันที่รับอายัด')
            ->setCellValue('O4', 'จำนวนวันที่รับอายัด')
            ->setCellValue('P4', 'วันที่สิ้นสุดอายัด')
            ->setCellValue('Q4', 'เหตุผลที่ขออายัด')
            ->setCellValue('R4', 'เงื่อนไขการอายัด')
            ->setCellValue('S4', 'ชื่อผู้ถูกอายัด')
            ->setCellValue('T4', 'หมายเหตุ')
            ->getStyle('A1:T4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('T4:T' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);


        for ($j = 0; $j < 5; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);
            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
                'font' => array(
                    'bold'  => false,
                    'color' => array('rgb' => '000000'),
                    'size'  => 16,
                    'name'  => 'TH SarabunPSK'
                )
            ];
            if ($j == 0) {
                if ($sts == 'e') $last = $errorParcel;
                $sheet->getStyle('A1:Q' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:Q4')->getFont()->setBold(true);

                $sheet->getStyle('A4:Q' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 1) {
                if ($sts == 'e') $last = $errorOlgt;
                $sheet->getStyle('A1:N' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:N4')->getFont()->setBold(true);

                $sheet->getStyle('A4:N' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 2) {
                if ($sts == 'e') $last = $errorOwner;
                $sheet->getStyle('A1:U' . $ownerCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:U4')->getFont()->setBold(true);

                $sheet->getStyle('A4:U' .  $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 3) {
                if ($sts == 'e') $last = $errortransaction;
                $sheet->getStyle('A1:J' . $transactionCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:J4')->getFont()->setBold(true);

                $sheet->getStyle('A4:J' .  $transactionCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else {
                if ($sts == 'e') $last = $errorSequester;
                $sheet->getStyle('A1:T4')->applyFromArray($styleArray);
                $sheet->getStyle('A1:T' . $sequesterCellIndex == 0 ? 4 : $sequesterCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:T4')->getFont()->setBold(true);


                $sheet->getStyle('A4:T' . $sequesterCellIndex == 0 ? 4 : $sequesterCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //  $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        // if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
        //      $fileName = urlencode($fileName);
        // }
        // $fileName = urlencode($fileName);
        //
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save("C:\Users\MGT1\Desktop\htdoc\\" . $landoffice . '-' . $fileName . ".xlsx");
        echo 'Done !!!!' . PHP_EOL;
        echo 'Export success at ' . date('Y/m/d H:i:s') . PHP_EOL;
        //$writer->save("C:\Users\MGT1\Desktop\htdoc\\" . $landoffice . '-' . $fileName . ".xlsx);


        break;
    case '2':
    case '3':
    case '4': //ns3a
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $ownerCellIndex = 0;
        $transactionCellIndex = 0;
        $sequesterCellIndex = 0;

        $errorParcel = 0;
        $errorOlgt = 0;
        $errorOwner = 0;
        $errortransaction = 0;
        $errorSequester = 0;

        $sequester_seq = array();
        $sequester_index = array();


        $current = 0;
        //===================PARCEL DETAIL=====================================
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];

            include 'queryDetailData.php';
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);



            if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }

                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
            }

            $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('E' . (6 + $current), 'พัฒน์ฯ 2');

            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('F' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (5 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AREA_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultDetail[0]['AREA_P1']);
            }
            if (empty($ResultDetail[0]['UTM_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultDetail[0]['UTM_P1']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultDetail[0]['ORIGIN_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'])) {
                $sheet->setCellValue('N' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('O' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'] == 1 ? 'ไม่มี' : 'ติดอายัด');
                if ($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'] != 1) {
                    array_push($sequester_seq, $dataSeqP1);
                    array_push($sequester_index, $j);
                }
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'])) {
                $sheet->setCellValue('P' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1']);
            }


            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AREA_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultDetail[0]['AREA_P2']);
            }
            if (empty($ResultDetail[0]['UTM_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultDetail[0]['UTM_P2']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultDetail[0]['ORIGIN_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2'])) {
                $sheet->setCellValue('P' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']);
            }

            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultDetail[0]['PARCEL_LAND_NO_P1'] != $ResultDetail[0]['PARCEL_LAND_NO_P2']) $problemDesc .= "เลขเอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'] != $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']) $problemDesc .= "ชื่อประเภทเอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['TAMBOL_NAME_P1'] != $ResultDetail[0]['TAMBOL_NAME_P2']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AMPHUR_NAME_P1'] != $ResultDetail[0]['AMPHUR_NAME_P2']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AREA_P1'] != $ResultDetail[0]['AREA_P2']) $problemDesc .= "ขนาดพื้นที่ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['UTM_P1'] != $ResultDetail[0]['UTM_P2']) $problemDesc .= "ระวาง UTM ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['ORIGIN_P1'] != $ResultDetail[0]['ORIGIN_P2']) $problemDesc .= "ระวางศูนย์กำเนิดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['OWN_P1'] != $ResultDetail[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'] != $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']) $problemDesc .= "จำนวนผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'] != $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']) $problemDesc .= "สถานะ อปท. ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'] != $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2']) $problemDesc .= "สถานะการอายัดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'] != $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']) $problemDesc .= "การใช้ประโยชน์ไม่ตรงกัน\n";
                }
                $sheet->setCellValue('Q' . (5 + $current), rtrim($problemDesc));
            }



            $current += 2;
        }
        $current = 0;
        //=================================== OLGT =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        for ($j = 0; $j < $last; $j++) {

            $ResultOlgt = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }


            if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }

                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
            }


            $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('E' . (6 + $current), 'พัฒน์ฯ 2');
            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('F' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('L' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P1'])) {
                    $sheet->setCellValue('M' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('F' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('F' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('L' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('L' . (6 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('M' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (6 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            if ($sts == 'e') {
				$problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'] != $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']) $problemDesc .= "ชื่อหนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'] != $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']) $problemDesc .= "วันที่หนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'] != $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']) $problemDesc .= "วันที่เริ่มสัญญา\n";
                    if ($ResultOlgt[0]['OLGT_HD_MNY_P1'] != $ResultOlgt[0]['OLGT_HD_MNY_P2']) $problemDesc .= "ราคาทุนทรัพย์ไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['DURATION_P1'] != $ResultOlgt[0]['DURATION_P2']) $problemDesc .= "ระยะเวลาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_PMS_PID_P1'] != $ResultOlgt[0]['OLGT_PMS_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) && $ResultOlgt[0]['OWN_P1'] != $ResultOlgt[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ให้สัญญาไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) && $ResultOlgt[1]['OWN_P1'] != $ResultOlgt[1]['OWN_P2']) $problemDesc .= "ชื่อผู้รับสัญญาไม่ตรงกัน\n";
                }

                $sheet->setCellValue('N' . (5 + $current), rtrim($problemDesc));
                $problemDesc = '';
            }

            $current += 2;
        }

        $current = 0;
        //=================================== OWNER =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        for ($j = 0; $j < $last; $j++) {
            $ResultOwner = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            include 'queryOwnDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOwner[] = $row;
            }



            if (count($ResultOwner) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current + (count($ResultOwner))) . ':E' . (5 + $current + (count($ResultOwner) * 2) - 1));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    $ownerCellIndex = (5 + $current);
                }

                $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('E' . (5 + $current + count($ResultOwner)), 'พัฒน์ฯ 2');
                $ownerDesc = '';

                for ($i = 0; $i < count($ResultOwner); $i++) {
                    if (empty($ResultOwner[$i]['OWNER_ORDER_P1'])) {
                        $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current + $i), $ResultOwner[$i]['OWNER_ORDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P1'])) {
                        $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PID_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P1'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultOwner[$i]['OWN_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultOwner[$i]['OWNER_GENDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultOwner[$i]['OWNER_BDATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PN_STS_P1']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultOwner[$i]['NATIONALITY_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P1'])) {
                        $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + $i), $ResultOwner[$i]['RACE_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P1'])) {
                        $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + $i), $ResultOwner[$i]['RELIGION_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P1'])) {
                        $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + $i), $ResultOwner[$i]['FAT_P1']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P1'])) {
                        $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + $i), $ResultOwner[$i]['MOT_P1']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P1'])) {
                        $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + $i), $ResultOwner[$i]['REG_ADDR_P1']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P1'])) {
                        $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + $i), $ResultOwner[$i]['RECV_DATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P1'])) {
                        $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + $i), $ResultOwner[$i]['OBTAIN_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P1'])) {
                        $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + $i), $ResultOwner[$i]['NUM_P1']);
                    }



                    if (empty($ResultOwner[$i]['OWNER_ORDER_P2'])) {
                        $sheet->setCellValue('F' . (5 + $current + count($ResultOwner)  + $i), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_ORDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P2'])) {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PID_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWN_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_GENDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_BDATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PN_STS_P2']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NATIONALITY_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P2'])) {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RACE_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P2'])) {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RELIGION_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P2'])) {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['FAT_P2']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P2'])) {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['MOT_P2']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P2'])) {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['REG_ADDR_P2']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P2'])) {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RECV_DATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P2'])) {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OBTAIN_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P2'])) {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NUM_P2']);
                    }

                    $ownerCellIndex = (5 + $current + count($ResultOwner) + $i);



                    if ($sts == 'e') {
						$problemDesc = '';
                        if (!$dataSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                        } else if (!$dataSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                        } else {
                            if ($ResultOwner[$i]['OWNER_ORDER_P1'] != $ResultOwner[$i]['OWNER_ORDER_P2']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PID_P1'] != $ResultOwner[$i]['OWNER_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWN_P1'] != $ResultOwner[$i]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_GENDER_P1'] != $ResultOwner[$i]['OWNER_GENDER_P2']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_BDATE_P1'] != $ResultOwner[$i]['OWNER_BDATE_P2']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PN_STS_P1'] != $ResultOwner[$i]['OWNER_PN_STS_P2']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NATIONALITY_NAME_P1'] != $ResultOwner[$i]['NATIONALITY_NAME_P2']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RACE_NAME_P1'] != $ResultOwner[$i]['RACE_NAME_P2']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RELIGION_NAME_P1'] != $ResultOwner[$i]['RELIGION_NAME_P2']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['FAT_P1'] != $ResultOwner[$i]['FAT_P2']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['MOT_P1'] != $ResultOwner[$i]['MOT_P2']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['REG_ADDR_P1'] != $ResultOwner[$i]['REG_ADDR_P2']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RECV_DATE_P1'] != $ResultOwner[$i]['RECV_DATE_P2']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OBTAIN_NAME_P1'] != $ResultOwner[$i]['OBTAIN_NAME_P2']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NUM_P1'] != $ResultOwner[$i]['NUM_P2']) $problemDesc .= "สัดส่วนการได้มาไม่ตรงกัน\n";
                        }

                        $sheet->setCellValue('U' . (5 + $current + $i), rtrim($problemDesc));


                        $problemDesc = '';
                    }
                }

                $current += count($ResultOwner) * 2;
            }
        }

        //=========================== PROCESS TRANSACTION ==========================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $processP1 = array();
        $processP2 = array();


        for ($j = 0; $j < $last; $j++) {
            $ResultProcess = array();

            $parcelSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $parcelSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            if ($parcelSeqP1 != null) {
                $table = 'P1';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }
            if ($parcelSeqP2 != null) {
                $table = 'P2';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }

    


            if (count($ResultProcess) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + count($ResultProcess) - 1));
				$sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultProcess) / 2) - 1));
                $sheet->mergeCells('E' . (5 + $current + (count($ResultProcess) / 2)) . ':E' . (5 + $current + count($ResultProcess) - 1));
                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {

                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));
                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }
                $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('E' . (6 + $current + (count($ResultProcess) / 2)), 'พัฒน์ฯ 2');
                $ownerDesc = '';


                for ($i = 0; $i < count($ResultProcess); $i++) {
                    if ($ResultProcess[$i]['KEY'] == '1') {
                        array_push($processP1, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('F' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    } else if ($ResultProcess[$i]['KEY'] == '2') {
                        array_push($processP2, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('F' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                        $transactionCellIndex = (5 + $current + count($ResultProcess) + $i);
                    }
                }

                $current += count($ResultProcess);
            }
        }
        $current = 0;


        if ($sts == 'e') {
            for ($i = 0; $i < $last; $i++) {
                $problemDesc = '';
                if (!$parcelSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$parcelSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($processP1[$i]['MANAGE_QUEUE_DTM'] != $processP2[$i]['MANAGE_QUEUE_DTM']) $problemDesc .= "วันที่จดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['MANAGE_QUEUE_NO'] != $processP2[$i]['MANAGE_QUEUE_NO']) $problemDesc .= "เลขคิวไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_ORDER'] != $processP2[$i]['PROCESS_ORDER']) $problemDesc .= "ลำดับการจดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_REGIST_NAME'] != $processP2[$i]['PROCESS_REGIST_NAME']) $problemDesc .= "ชื่อรายการจดทะเบียนไม่ตรงกัน\n";
                }
                $sheet->setCellValue('J' . (5 + $current), rtrim($problemDesc));
                $current += 2;
            }
        }



        //================== SEQUESTER ===============================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        for ($j = 0; $j < count($sequester_seq); $j++) {

            $ResultSequester = array();


            $parcelSeqP1 = $sequester_seq[$j];
            $parcelSeqP2 = $sequester_seq[$j];
            include 'sequesterEachParcel.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultSequester[] = $row;
            }






            $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));



            $sheet->setCellValue('A' . (5 + $current), ($j + 1));
            $sheet->setCellValue('B' . (5 + $current), $Result[$sequester_index[$j]]['PARCEL_LAND_NO']);
            $sheet->setCellValue('C' . (5 + $current), $Result[$sequester_index[$j]]['AMPHUR_NAME']);
            $sheet->setCellValue('D' . (5 + $current), $Result[$sequester_index[$j]]['TAMBOL_NAME']);



            $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('T' . (5 + $current) . ':T' . (5 + $current + (count($ResultSequester) * 2) + 1));


            // $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultSequester) / 2) - 1));
            // $sheet->mergeCells('E' . (5 + $current + (count($ResultSequester) / 2)) . ':E' . (5 + $current + (count($ResultSequester)*2) - 1));


            $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('E' . (5 + $current + count($ResultSequester)), 'พัฒน์ฯ 2');
            $ownerDesc = '';
            //  $current += 2;

            for ($i = 0; $i < count($ResultSequester); $i++) {
                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO'])) {
                    $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO'])) {
                    $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM'])) {
                    $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT'])) {
                    $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_FNAME'])) {
                    $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_FNAME']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'])) {
                    $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM'])) {
                    $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM'])) {
                    $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'])) {
                    $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'])) {
                    $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM'])) {
                    $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'])) {
                    $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME'])) {
                    $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('S' . (5 + $current + $i), $ResultSequester[$i]['TITLE_NAME_OWN'] . $ResultSequester[$i]['OWNER_FNAME'] . ' ' . $ResultSequester[$i]['OWNER_LNAME']);
                }

                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO_1'])) {
                    $sheet->setCellValue('F' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO_1'])) {
                    $sheet->setCellValue('G' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM_1'])) {
                    $sheet->setCellValue('H' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT_1'])) {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_FNAME'])) {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_FNAME']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1'])) {
                    $sheet->setCellValue('L' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM_1'])) {
                    $sheet->setCellValue('M' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM_1'])) {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1'])) {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1'])) {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM_1'])) {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1'])) {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME_1'])) {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['TITLE_NAME_OWN_1'] . $ResultSequester[$i]['OWNER_FNAME_1'] . ' ' . $ResultSequester[$i]['OWNER_LNAME_1']);
                }
                if ($sts == 'e') {

                    $problemDesc = '';
                    if (!$dataSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$dataSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                    } else {
                        if ($ResultSequester[$i]['SEQUESTER_ORDER_NO'] != $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_NO'] != $ResultSequester[$i]['SEQUESTER_DOC_NO_1']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_DTM'] != $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DEPT'] != $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_PID'] != $ResultSequester[$i]['SEQUESTER_REQ_PID_1']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_FNAME'] != $ResultSequester[$i]['OWNER_FNAME_1']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DTM'] != $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REC_DTM'] != $ResultSequester[$i]['SEQUESTER_REC_DTM_1']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REM'] != $ResultSequester[$i]['SEQUESTER_REM_1']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'] != $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_NAME'] != $ResultSequester[$i]['OWNER_NAME_1']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                    }

                    $sheet->setCellValue('T' . (5 + $current + $i), rtrim($problemDesc));
                    $problemDesc = '';
                }
            }

            $current += count($ResultSequester) * 2;
            $sequesterCellIndex = $current;
        }

        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $sheet->mergeCells('A1:Q1');
        $sheet->mergeCells('A2:Q2');
        $sheet->mergeCells('A3:Q3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
            }
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('E4', 'พัฒน์ฯ 1/2')
            ->setCellValue('F4', 'เลขที่เอกสารสิทธิ')
            ->setCellValue('G4', 'ตำบล/แขวง')
            ->setCellValue('H4', 'อำเภอ/เขต')
            ->setCellValue('I4', 'ขนาดพื้นที่')
            ->setCellValue('J4', 'ระวาง UTM')
            ->setCellValue('K4', 'ระวางศูนย์กำเนิด')
            ->setCellValue('L4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'สถานะ อปท.')
            ->setCellValue('O4', 'สถานะการอายัด')
            ->setCellValue('P4', 'การใช้ประโยชน์')
            ->setCellValue('Q4', 'หมายเหตุ')
            ->getStyle('A1:Q4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('Q4:Q' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);

        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->mergeCells('A1:N1');
        $sheet->mergeCells('A2:N2');
        $sheet->mergeCells('A3:N3');
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
            }
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->setCellValue('E4', 'พัฒน์ฯ 1/2')
            ->setCellValue('F4', 'ชื่อหนังสือสัญญา')
            ->setCellValue('G4', 'วันที่หนังสือสัญญา')
            ->setCellValue('H4', 'วันที่เริ่มสัญญา')
            ->setCellValue('I4', 'ราคาทุนทรัพย์')
            ->setCellValue('J4', 'ระยะเวลา')
            ->setCellValue('K4', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('L4', 'ผู้ให้สัญญา')
            ->setCellValue('M4', 'ผู้รับสัญญา')
            ->setCellValue('N4', 'หมายเหตุ')
            ->getStyle('A1:N4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('N4:N' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        $sheet->mergeCells('A1:U1');
        $sheet->mergeCells('A2:U2');
        $sheet->mergeCells('A3:U3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
            }
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('G4:G' . $ownerCellIndex)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);

        $sheet->setCellValue('E4', 'พัฒน์ฯ 1/2')
            ->setCellValue('F4', 'ลำดับผู้ถือกรรมสิทธิ์')
            ->setCellValue('G4', 'เลขบัตรประจำตัวประชาชน')
            ->setCellValue('H4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('I4', 'เพศผู้ถือกรรมสิทธิ์')
            ->setCellValue('J4', 'วันเกิดผู้ถือกรรมสิทธิ์')
            ->setCellValue('K4', 'สถานะสมรสผู้ถือกรรมสิทธิ์')
            ->setCellValue('L4', 'สัญชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'เชื้อชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'ศาสนาผู้ถือกรรมสิทธิ์')
            ->setCellValue('O4', 'ชื่อบิดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('P4', 'ชื่อมารดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('Q4', 'ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์')
            ->setCellValue('R4', 'วันที่ได้มา')
            ->setCellValue('S4', 'การได้มา')
            ->setCellValue('T4', 'สัดส่วนการได้มา')
            ->setCellValue('U4', 'หมายเหตุ')
            ->getStyle('A1:U4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('U4:U' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $sheet->mergeCells('A1:J1');
        $sheet->mergeCells('A2:J2');
        $sheet->mergeCells('A3:J3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
            }
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->setCellValue('E4', 'พัฒน์ฯ 1/2')
            ->setCellValue('F4', 'วันที่จดทะเบียน')
            ->setCellValue('G4', 'เลขคิว')
            ->setCellValue('H4', 'ลำดับการจดทะเบียน')
            ->setCellValue('I4', 'ชื่อรายการจดทะเบียน')
            ->setCellValue('J4', 'หมายเหตุ')
            ->getStyle('A1:J4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J4:J' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        $sheet->mergeCells('A1:T1');
        $sheet->mergeCells('A2:T2');
        $sheet->mergeCells('A3:T3');
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - โฉนดตราจอง';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ตราจองที่ได้ทำประโยชน์แล้ว';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
            }
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'อำเภอ')
            ->setCellValue('D4', 'ตำบล')
            ->getStyle('A1:D4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->setCellValue('E4', 'พัฒน์ฯ 1/2')
            ->setCellValue('F4', 'อายัดลำดับที่')
            ->setCellValue('G4', 'เลขที่คำสั่ง/หนังสือ')
            ->setCellValue('H4', 'วันที่คำสั่ง/หนังสือ')
            ->setCellValue('I4', 'หน่วยงานที่ขออายัด')
            ->setCellValue('J4', 'เลขที่บัตรประชาชนของผู้ขออายัด')
            ->setCellValue('K4', 'ชื่อ-สกุลผู้ขออายัด')
            ->setCellValue('L4', 'สถานะกำหนดอายัด')
            ->setCellValue('M4', 'วันที่ขออายัด')
            ->setCellValue('N4', 'วันที่รับอายัด')
            ->setCellValue('O4', 'จำนวนวันที่รับอายัด')
            ->setCellValue('P4', 'วันที่สิ้นสุดอายัด')
            ->setCellValue('Q4', 'เหตุผลที่ขออายัด')
            ->setCellValue('R4', 'เงื่อนไขการอายัด')
            ->setCellValue('S4', 'ชื่อผู้ถูกอายัด')
            ->setCellValue('T4', 'หมายเหตุ')
            ->getStyle('A1:T4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('T4:T' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        for ($j = 0; $j < 4; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);
            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
                'font' => array(
                    'bold'  => false,
                    'color' => array('rgb' => '000000'),
                    'size'  => 16,
                    'name'  => 'TH SarabunPSK'
                )
            ];
            if ($j == 0) {
                $sheet->getStyle('A1:Q' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:Q4')->getFont()->setBold(true);

                $sheet->getStyle('A4:Q' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 1) {
                $sheet->getStyle('A1:N' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:N4')->getFont()->setBold(true);

                $sheet->getStyle('A4:N' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 2) {
                $sheet->getStyle('A1:U' . $ownerCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:U4')->getFont()->setBold(true);

                $sheet->getStyle('A4:U' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 3) {
                $sheet->getStyle('A1:J' . 2500)->applyFromArray($styleArray);
                $sheet->getStyle('A1:J4')->getFont()->setBold(true);
            } else {
                $sheet->getStyle('A1:T' . $sequesterCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:T4')->getFont()->setBold(true);

                $sheet->getStyle('A4:T' . $sequesterCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save("C:\Users\MGT1\Desktop\htdoc\\" . $landoffice . '-' . $fileName . ".xlsx");


        break;
        // ======================== NS3 ========================
    case '5':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $ownerCellIndex = 0;
        $transactionCellIndex = 0;
        $sequesterCellIndex = 0;

        $errorParcel = 0;
        $errorOlgt = 0;
        $errorOwner = 0;
        $errortransaction = 0;
        $errorSequester = 0;

        $sequester_seq = array();
        $sequester_index = array();


        $current = 0;
        //===================PARCEL DETAIL=====================================
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];

            include 'queryDetailData.php';
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);



            if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$j]['MOO'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                }
                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
            }

            $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('F' . (6 + $current), 'พัฒน์ฯ 2');

            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AREA_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultDetail[0]['AREA_P1']);
            }
            if (empty($ResultDetail[0]['UTM_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultDetail[0]['UTM_P1']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultDetail[0]['ORIGIN_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'])) {
                $sheet->setCellValue('N' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'])) {
                $sheet->setCellValue('O' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('P' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'])) {
                $sheet->setCellValue('Q' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1']);
            }


            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AREA_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultDetail[0]['AREA_P2']);
            }
            if (empty($ResultDetail[0]['UTM_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultDetail[0]['UTM_P2']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultDetail[0]['ORIGIN_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('P' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2'])) {
                $sheet->setCellValue('Q' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']);
            }

            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultDetail[0]['PARCEL_LAND_NO_P1'] != $ResultDetail[0]['PARCEL_LAND_NO_P2']) $problemDesc .= "เลขเอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'] != $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']) $problemDesc .= "ชื่อประเภทเอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['TAMBOL_NAME_P1'] != $ResultDetail[0]['TAMBOL_NAME_P2']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AMPHUR_NAME_P1'] != $ResultDetail[0]['AMPHUR_NAME_P2']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AREA_P1'] != $ResultDetail[0]['AREA_P2']) $problemDesc .= "ขนาดพื้นที่ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['UTM_P1'] != $ResultDetail[0]['UTM_P2']) $problemDesc .= "ระวาง UTM ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['ORIGIN_P1'] != $ResultDetail[0]['ORIGIN_P2']) $problemDesc .= "ระวางศูนย์กำเนิดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['OWN_P1'] != $ResultDetail[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'] != $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']) $problemDesc .= "จำนวนผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'] != $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']) $problemDesc .= "สถานะ อปท. ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'] != $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2']) $problemDesc .= "สถานะการอายัดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'] != $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']) $problemDesc .= "การใช้ประโยชน์ไม่ตรงกัน\n";
                }

                $sheet->setCellValue('R' . (5 + $current), rtrim($problemDesc));
            }



            $current += 2;
        }
        $current = 0;
        //=================================== OLGT =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        for ($j = 0; $j < $last; $j++) {

            $ResultOlgt = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }



            if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$j]['MOO'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                }
                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
            }



            $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('F' . (6 + $current), 'พัฒน์ฯ 2');
            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('M' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P1'])) {
                    $sheet->setCellValue('N' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('M' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (6 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('N' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (6 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }

            if ($sts == 'e') {
				$problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'] != $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']) $problemDesc .= "ชื่อหนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'] != $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']) $problemDesc .= "วันที่หนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'] != $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']) $problemDesc .= "วันที่เริ่มสัญญา\n";
                    if ($ResultOlgt[0]['OLGT_HD_MNY_P1'] != $ResultOlgt[0]['OLGT_HD_MNY_P2']) $problemDesc .= "ราคาทุนทรัพย์ไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['DURATION_P1'] != $ResultOlgt[0]['DURATION_P2']) $problemDesc .= "ระยะเวลาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_PMS_PID_P1'] != $ResultOlgt[0]['OLGT_PMS_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) && $ResultOlgt[0]['OWN_P1'] != $ResultOlgt[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ให้สัญญาไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) && $ResultOlgt[1]['OWN_P1'] != $ResultOlgt[1]['OWN_P2']) $problemDesc .= "ชื่อผู้รับสัญญาไม่ตรงกัน\n";
                }
                $sheet->setCellValue('O' . (5 + $current), rtrim($problemDesc));
            }
            $current += 2;
        }

        $current = 0;
        //=================================== OWNER =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        for ($j = 0; $j < $last; $j++) {
            $ResultOwner = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            include 'queryOwnDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOwner[] = $row;
            }
            $tempIndex = 0;





            if (count($ResultOwner) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['MOO'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }

                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current + (count($ResultOwner))) . ':E' . (5 + $current + (count($ResultOwner) * 2) - 1));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['MOO'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    $ownerCellIndex = (5 + $current);
                }



                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (5 + $current + count($ResultOwner)), 'พัฒน์ฯ 2');
                $ownerDesc = '';

                for ($i = 0; $i < count($ResultOwner); $i++) {
                    if (empty($ResultOwner[$i]['OWNER_ORDER_P1'])) {
                        $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + $i), $ResultOwner[$i]['OWNER_ORDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P1'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PID_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultOwner[$i]['OWN_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultOwner[$i]['OWNER_GENDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultOwner[$i]['OWNER_BDATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PN_STS_P1']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P1'])) {
                        $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + $i), $ResultOwner[$i]['NATIONALITY_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P1'])) {
                        $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + $i), $ResultOwner[$i]['RACE_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P1'])) {
                        $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + $i), $ResultOwner[$i]['RELIGION_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P1'])) {
                        $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + $i), $ResultOwner[$i]['FAT_P1']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P1'])) {
                        $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + $i), $ResultOwner[$i]['MOT_P1']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P1'])) {
                        $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + $i), $ResultOwner[$i]['REG_ADDR_P1']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P1'])) {
                        $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + $i), $ResultOwner[$i]['RECV_DATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P1'])) {
                        $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + $i), $ResultOwner[$i]['OBTAIN_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P1'])) {
                        $sheet->setCellValue('U' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + $i), $ResultOwner[$i]['NUM_P1']);
                    }



                    if (empty($ResultOwner[$i]['OWNER_ORDER_P2'])) {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultOwner)  + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_ORDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PID_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWN_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_GENDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_BDATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PN_STS_P2']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P2'])) {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NATIONALITY_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P2'])) {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RACE_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P2'])) {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RELIGION_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P2'])) {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['FAT_P2']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P2'])) {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['MOT_P2']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P2'])) {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['REG_ADDR_P2']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P2'])) {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RECV_DATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P2'])) {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OBTAIN_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P2'])) {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NUM_P2']);
                    }

                    $ownerCellIndex = (5 + $current + count($ResultOwner) + $i);

                    if ($sts == 'e') {
						$problemDesc = '';
                        if (!$dataSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                        } else if (!$dataSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                        } else {
                            if ($ResultOwner[$i]['OWNER_ORDER_P1'] != $ResultOwner[$i]['OWNER_ORDER_P2']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PID_P1'] != $ResultOwner[$i]['OWNER_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWN_P1'] != $ResultOwner[$i]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_GENDER_P1'] != $ResultOwner[$i]['OWNER_GENDER_P2']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_BDATE_P1'] != $ResultOwner[$i]['OWNER_BDATE_P2']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PN_STS_P1'] != $ResultOwner[$i]['OWNER_PN_STS_P2']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NATIONALITY_NAME_P1'] != $ResultOwner[$i]['NATIONALITY_NAME_P2']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RACE_NAME_P1'] != $ResultOwner[$i]['RACE_NAME_P2']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RELIGION_NAME_P1'] != $ResultOwner[$i]['RELIGION_NAME_P2']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['FAT_P1'] != $ResultOwner[$i]['FAT_P2']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['MOT_P1'] != $ResultOwner[$i]['MOT_P2']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['REG_ADDR_P1'] != $ResultOwner[$i]['REG_ADDR_P2']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RECV_DATE_P1'] != $ResultOwner[$i]['RECV_DATE_P2']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OBTAIN_NAME_P1'] != $ResultOwner[$i]['OBTAIN_NAME_P2']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NUM_P1'] != $ResultOwner[$i]['NUM_P2']) $problemDesc .= "สัดส่วนการได้มาไม่ตรงกัน\n";
                        }
                        $sheet->setCellValue('V' . (5 + $current + $i), rtrim($problemDesc));
                    }
                }

                $current += count($ResultOwner) * 2;
            }
        }

        //=========================== PROCESS TRANSACTION ==========================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $processP1 = array();
        $processP2 = array();
        for ($j = 0; $j < $last; $j++) {
            $ResultProcess = array();

            $parcelSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $parcelSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            if ($parcelSeqP1 != null) {
                $table = 'P1';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }
            if ($parcelSeqP2 != null) {
                $table = 'P2';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }




            if (count($ResultProcess) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));


                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['MOO'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }

                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultProcess) / 2) - 1));
                $sheet->mergeCells('E' . (5 + $current + (count($ResultProcess) / 2)) . ':E' . (5 + $current + count($ResultProcess) - 1));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['MOO'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    $ownerCellIndex = (5 + $current);
                }

                $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('E' . (5 + $current + (count($ResultProcess) / 2)), 'พัฒน์ฯ 2');
                $ownerDesc = '';


                for ($i = 0; $i < count($ResultProcess); $i++) {
                    if ($ResultProcess[$i]['KEY'] == '1') {
                        array_push($processP1, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('F' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    } else if ($ResultProcess[$i]['KEY'] == '2') {
                        array_push($processP2, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('F' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    }
                    $transactionCellIndex = (5 + $current + count($ResultProcess) + $i);
                }

                $current += count($ResultProcess);
            }
        }
        $current = 0;
       
        if ($sts == 'e') {
            for ($i = 0; $i < $last; $i++) {
                $problemDesc = '';
                if (!$parcelSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$parcelSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($processP1[$i]['MANAGE_QUEUE_DTM'] != $processP2[$i]['MANAGE_QUEUE_DTM']) $problemDesc .= "วันที่จดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['MANAGE_QUEUE_NO'] != $processP2[$i]['MANAGE_QUEUE_NO']) $problemDesc .= "เลขคิวไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_ORDER'] != $processP2[$i]['PROCESS_ORDER']) $problemDesc .= "ลำดับการจดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_REGIST_NAME'] != $processP2[$i]['PROCESS_REGIST_NAME']) $problemDesc .= "ชื่อรายการจดทะเบียนไม่ตรงกัน\n";
                }
                $sheet->setCellValue('J' . (5 + $current), rtrim($problemDesc));
                $current += 2;
            }
        }


        //================== SEQUESTER ===============================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        for ($j = 0; $j < count($sequester_seq); $j++) {

            $ResultSequester = array();


            $parcelSeqP1 = $sequester_seq[$j];
            $parcelSeqP2 = $sequester_seq[$j];
            include 'sequesterEachParcelExcel.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultSequester[] = $row;
            }




            $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));



            $sheet->setCellValue('A' . (5 + $current), ($j + 1));
            $sheet->setCellValue('B' . (5 + $current), $Result[$sequester_index[$j]]['PARCEL_LAND_NO']);
            $sheet->setCellValue('C' . (5 + $current), $Result[$sequester_index[$j]]['AMPHUR_NAME']);
            $sheet->setCellValue('D' . (5 + $current), $Result[$sequester_index[$j]]['TAMBOL_NAME']);



            $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultSequester) * 2) - 1));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultSequester) * 2) - 1));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultSequester) * 2) - 1));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultSequester) * 2) - 1));
            $sheet->mergeCells('T' . (5 + $current) . ':T' . (5 + $current + (count($ResultSequester) * 2) - 1));


            // $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultSequester) / 2) - 1));
            // $sheet->mergeCells('E' . (5 + $current + (count($ResultSequester) / 2)) . ':E' . (5 + $current + (count($ResultSequester)*2) - 1));



            $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('E' . (5 + $current + count($ResultSequester)), 'พัฒน์ฯ 2');
            $ownerDesc = '';
            $current += 2;


            for ($i = 0; $i < count($ResultSequester); $i++) {
                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO'])) {
                    $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO'])) {
                    $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM'])) {
                    $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT'])) {
                    $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME'])) {
                    $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + $i), $ResultSequester[$i]['TITLE_NAME_OWN'] . $ResultSequester[$i]['OWNER_FNAME'] . ' ' . $ResultSequester[$i]['OWNER_LNAME']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'])) {
                    $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM'])) {
                    $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM'])) {
                    $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'])) {
                    $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'])) {
                    $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM'])) {
                    $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'])) {
                    $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION']);
                }
                if (empty($ResultSequester[$i]['OWNER_NAME'])) {
                    $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('S' . (5 + $current + $i), $ResultSequester[$i]['OWNER_NAME']);
                }

                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO_1'])) {
                    $sheet->setCellValue('F' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO_1'])) {
                    $sheet->setCellValue('G' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM_1'])) {
                    $sheet->setCellValue('H' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT_1'])) {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME_1'])) {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['TITLE_NAME_OWN_1'] . $ResultSequester[$i]['OWNER_FNAME_1'] . ' ' . $ResultSequester[$i]['OWNER_LNAME_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1'])) {
                    $sheet->setCellValue('L' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM_1'])) {
                    $sheet->setCellValue('M' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM_1'])) {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1'])) {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1'])) {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM_1'])) {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1'])) {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']);
                }
                if (empty($ResultSequester[$i]['OWNER_NAME_1'])) {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['OWNER_NAME_1']);
                }
                $sequesterCellIndex = (5 + $current + count($ResultSequester) + $i);

                $tempIndex = 0;

                if ($sts == 'e') {

                    $problemDesc = '';
                    if (!$dataSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$dataSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                    } else {
                        if ($ResultSequester[$i]['SEQUESTER_ORDER_NO'] != $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_NO'] != $ResultSequester[$i]['SEQUESTER_DOC_NO_1']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_DTM'] != $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DEPT'] != $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_PID'] != $ResultSequester[$i]['SEQUESTER_REQ_PID_1']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_FNAME'] != $ResultSequester[$i]['OWNER_FNAME_1']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DTM'] != $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REC_DTM'] != $ResultSequester[$i]['SEQUESTER_REC_DTM_1']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REM'] != $ResultSequester[$i]['SEQUESTER_REM_1']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'] != $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_NAME'] != $ResultSequester[$i]['OWNER_NAME_1']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                    }

                    $sheet->setCellValue('S' . (5 + $current + $i), rtrim($problemDesc));
                }
            }

            $current += count($ResultSequester) * 2;
        }

        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $sheet->mergeCells('A1:R1');
        $sheet->mergeCells('A2:R2');
        $sheet->mergeCells('A3:R3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            }
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'หมู่')
            ->setCellValue('D4', 'อำเภอ')
            ->setCellValue('E4', 'ตำบล')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'เลขที่เอกสารสิทธิ')
            ->setCellValue('H4', 'ตำบล/แขวง')
            ->setCellValue('I4', 'อำเภอ/เขต')
            ->setCellValue('J4', 'ขนาดพื้นที่')
            ->setCellValue('K4', 'ระวาง UTM')
            ->setCellValue('L4', 'ระวางศูนย์กำเนิด')
            ->setCellValue('M4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('O4', 'สถานะ อปท.')
            ->setCellValue('P4', 'สถานะการอายัด')
            ->setCellValue('Q4', 'การใช้ประโยชน์')
            ->setCellValue('R4', 'หมายเหตุ')
            ->getStyle('A1:R4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('R4:R' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);


        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->mergeCells('A1:O1');
        $sheet->mergeCells('A2:O2');
        $sheet->mergeCells('A3:O3');
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            }
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'หมู่')
            ->setCellValue('D4', 'อำเภอ')
            ->setCellValue('E4', 'ตำบล')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'ชื่อหนังสือสัญญา')
            ->setCellValue('H4', 'วันที่หนังสือสัญญา')
            ->setCellValue('I4', 'วันที่เริ่มสัญญา')
            ->setCellValue('J4', 'ราคาทุนทรัพย์')
            ->setCellValue('K4', 'ระยะเวลา')
            ->setCellValue('L4', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('M4', 'ผู้ให้สัญญา')
            ->setCellValue('N4', 'ผู้รับสัญญา')
            ->setCellValue('O4', 'หมายเหตุ')
            ->getStyle('A1:O4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('O4:O' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        $sheet->mergeCells('A1:V1');
        $sheet->mergeCells('A2:V2');
        $sheet->mergeCells('A3:V3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            }
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'หมู่')
            ->setCellValue('D4', 'อำเภอ')
            ->setCellValue('E4', 'ตำบล')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('H4:H' . $ownerCellIndex)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);


        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'ลำดับผู้ถือกรรมสิทธิ์')
            ->setCellValue('H4', 'เลขบัตรประจำตัวประชาชน')
            ->setCellValue('I4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('J4', 'เพศผู้ถือกรรมสิทธิ์')
            ->setCellValue('K4', 'วันเกิดผู้ถือกรรมสิทธิ์')
            ->setCellValue('L4', 'สถานะสมรสผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'สัญชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'เชื้อชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('O4', 'ศาสนาผู้ถือกรรมสิทธิ์')
            ->setCellValue('P4', 'ชื่อบิดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('Q4', 'ชื่อมารดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('R4', 'ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์')
            ->setCellValue('S4', 'วันที่ได้มา')
            ->setCellValue('T4', 'การได้มา')
            ->setCellValue('U4', 'สัดส่วนการได้มา')
            ->setCellValue('V4', 'หมายเหตุ')
            ->getStyle('A1:V4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('V4:V' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $sheet->mergeCells('A1:K1');
        $sheet->mergeCells('A2:K2');
        $sheet->mergeCells('A3:K3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            }
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'หมู่')
            ->setCellValue('D4', 'อำเภอ')
            ->setCellValue('E4', 'ตำบล')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'วันที่จดทะเบียน')
            ->setCellValue('H4', 'เลขคิว')
            ->setCellValue('I4', 'ลำดับการจดทะเบียน')
            ->setCellValue('J4', 'ชื่อรายการจดทะเบียน')
            ->setCellValue('K4', 'หมายเหตุ')
            ->getStyle('A1:K4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K4:K' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        $sheet->mergeCells('A1:U1');
        $sheet->mergeCells('A2:U2');
        $sheet->mergeCells('A3:U3');
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3';
            }
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'หมู่')
            ->setCellValue('D4', 'อำเภอ')
            ->setCellValue('E4', 'ตำบล')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K4:K' . $sequesterCellIndex)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'อายัดลำดับที่')
            ->setCellValue('H4', 'เลขที่คำสั่ง/หนังสือ')
            ->setCellValue('I4', 'วันที่คำสั่ง/หนังสือ')
            ->setCellValue('J4', 'หน่วยงานที่ขออายัด')
            ->setCellValue('K4', 'เลขที่บัตรประชาชนของผู้ขออายัด')
            ->setCellValue('L4', 'ชื่อ-สกุลผู้ขออายัด')
            ->setCellValue('M4', 'สถานะกำหนดอายัด')
            ->setCellValue('N4', 'วันที่ขออายัด')
            ->setCellValue('O4', 'วันที่รับอายัด')
            ->setCellValue('P4', 'จำนวนวันที่รับอายัด')
            ->setCellValue('Q4', 'วันที่สิ้นสุดอายัด')
            ->setCellValue('R4', 'เหตุผลที่ขออายัด')
            ->setCellValue('S4', 'เงื่อนไขการอายัด')
            ->setCellValue('T4', 'ชื่อผู้ถูกอายัด')
            ->setCellValue('U4', 'หมายเหตุ')
            ->getStyle('A1:U4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('U4:U' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        for ($j = 0; $j < 4; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);
            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
                'font' => array(
                    'bold'  => false,
                    'color' => array('rgb' => '000000'),
                    'size'  => 16,
                    'name'  => 'TH SarabunPSK'
                )
            ];
            if ($j == 0) {
                $sheet->getStyle('A1:R' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:R4')->getFont()->setBold(true);

                $sheet->getStyle('A4:R' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 1) {
                $sheet->getStyle('A1:O' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:O4')->getFont()->setBold(true);

                $sheet->getStyle('A4:O' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 2) {
                $sheet->getStyle('A1:V' . $ownerCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:V4')->getFont()->setBold(true);

                $sheet->getStyle('A4:V' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 3) {
                $sheet->getStyle('A1:K' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:K4')->getFont()->setBold(true);
            } else {
                $sheet->getStyle('A1:U' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:U4')->getFont()->setBold(true);

                $sheet->getStyle('A4:U' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save("C:\Users\MGT1\Desktop\htdoc\\" . $landoffice . '-' . $fileName . ".xlsx");


        break;


        // ======================== NSL ========================
    case '8':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $ownerCellIndex = 0;
        $transactionCellIndex = 0;
        $sequesterCellIndex = 0;

        $errorParcel = 0;
        $errorOlgt = 0;
        $errorOwner = 0;
        $errortransaction = 0;
        $errorSequester = 0;

        $sequester_seq = array();
        $sequester_index = array();


        $current = 0;
        //===================PARCEL DETAIL=====================================
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];

            include 'queryDetailData.php';
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);




            if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$j]['PARCEL_LAND_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['PARCEL_LAND_NAME']);
                }
                if (empty($Result[$j]['YEAR'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['YEAR']);
                }
                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('F' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
            }

            $sheet->setCellValue('G' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('G' . (6 + $current), 'พัฒน์ฯ 2');

            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AREA_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultDetail[0]['AREA_P1']);
            }
            if (empty($ResultDetail[0]['UTM_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultDetail[0]['UTM_P1']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultDetail[0]['ORIGIN_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('N' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (5 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'])) {
                $sheet->setCellValue('O' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'])) {
                $sheet->setCellValue('P' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('Q' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'])) {
                $sheet->setCellValue('R' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1']);
            }


            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AREA_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultDetail[0]['AREA_P2']);
            }
            if (empty($ResultDetail[0]['UTM_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultDetail[0]['UTM_P2']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultDetail[0]['ORIGIN_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2'])) {
                $sheet->setCellValue('P' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('Q' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2'])) {
                $sheet->setCellValue('R' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']);
            }
            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultDetail[0]['PARCEL_LAND_NO_P1'] != $ResultDetail[0]['PARCEL_LAND_NO_P2']) $problemDesc .= "เลขเอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'] != $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']) $problemDesc .= "ชื่อประเภทเอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['TAMBOL_NAME_P1'] != $ResultDetail[0]['TAMBOL_NAME_P2']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AMPHUR_NAME_P1'] != $ResultDetail[0]['AMPHUR_NAME_P2']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AREA_P1'] != $ResultDetail[0]['AREA_P2']) $problemDesc .= "ขนาดพื้นที่ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['UTM_P1'] != $ResultDetail[0]['UTM_P2']) $problemDesc .= "ระวาง UTM ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['ORIGIN_P1'] != $ResultDetail[0]['ORIGIN_P2']) $problemDesc .= "ระวางศูนย์กำเนิดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['OWN_P1'] != $ResultDetail[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'] != $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']) $problemDesc .= "จำนวนผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'] != $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']) $problemDesc .= "สถานะ อปท. ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'] != $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2']) $problemDesc .= "สถานะการอายัดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'] != $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']) $problemDesc .= "การใช้ประโยชน์ไม่ตรงกัน\n";
                }

                $sheet->setCellValue('S' . (5 + $current), rtrim($problemDesc));
            }



            $current += 2;
        }
        $current = 0;
        //=================================== OLGT =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        for ($j = 0; $j < $last; $j++) {

            $ResultOlgt = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }



            if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$j]['PARCEL_LAND_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['PARCEL_LAND_NAME']);
                }
                if (empty($Result[$j]['YEAR'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['YEAR']);
                }
                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('F' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
            }

            $sheet->setCellValue('G' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('G' . (6 + $current), 'พัฒน์ฯ 2');
            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('N' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P1'])) {
                    $sheet->setCellValue('O' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('N' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (6 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('O' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('O' . (6 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            if ($sts == 'e') {
				$problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'] != $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']) $problemDesc .= "ชื่อหนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'] != $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']) $problemDesc .= "วันที่หนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'] != $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']) $problemDesc .= "วันที่เริ่มสัญญา\n";
                    if ($ResultOlgt[0]['OLGT_HD_MNY_P1'] != $ResultOlgt[0]['OLGT_HD_MNY_P2']) $problemDesc .= "ราคาทุนทรัพย์ไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['DURATION_P1'] != $ResultOlgt[0]['DURATION_P2']) $problemDesc .= "ระยะเวลาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_PMS_PID_P1'] != $ResultOlgt[0]['OLGT_PMS_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) && $ResultOlgt[0]['OWN_P1'] != $ResultOlgt[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ให้สัญญาไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) && $ResultOlgt[1]['OWN_P1'] != $ResultOlgt[1]['OWN_P2']) $problemDesc .= "ชื่อผู้รับสัญญาไม่ตรงกัน\n";
                }

                $sheet->setCellValue('P' . (5 + $current), rtrim($problemDesc));
            }

            $current += 2;
        }

        $current = 0;
        //=================================== OWNER =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        for ($j = 0; $j < $last; $j++) {
            $ResultOwner = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            include 'queryOwnDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOwner[] = $row;
            }
            $tempIndex = 0;




            if (count($ResultOwner) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (6 + $current));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['PARCEL_LAND_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['PARCEL_LAND_NAME']);
                    }
                    if (empty($Result[$j]['YEAR'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['YEAR']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }

                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current + (count($ResultOwner))) . ':E' . (5 + $current + (count($ResultOwner) * 2) - 1));


                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['PARCEL_LAND_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['PARCEL_LAND_NAME']);
                    }
                    if (empty($Result[$j]['YEAR'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['YEAR']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    $ownerCellIndex = (5 + $current);
                }


                $sheet->setCellValue('G' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('G' . (5 + $current + count($ResultOwner)), 'พัฒน์ฯ 2');
                $ownerDesc = '';

                for ($i = 0; $i < count($ResultOwner); $i++) {
                    if (empty($ResultOwner[$i]['OWNER_ORDER_P1'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultOwner[$i]['OWNER_ORDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PID_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultOwner[$i]['OWN_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultOwner[$i]['OWNER_GENDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultOwner[$i]['OWNER_BDATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P1'])) {
                        $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PN_STS_P1']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P1'])) {
                        $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + $i), $ResultOwner[$i]['NATIONALITY_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P1'])) {
                        $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + $i), $ResultOwner[$i]['RACE_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P1'])) {
                        $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + $i), $ResultOwner[$i]['RELIGION_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P1'])) {
                        $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + $i), $ResultOwner[$i]['FAT_P1']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P1'])) {
                        $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + $i), $ResultOwner[$i]['MOT_P1']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P1'])) {
                        $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + $i), $ResultOwner[$i]['REG_ADDR_P1']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P1'])) {
                        $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + $i), $ResultOwner[$i]['RECV_DATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P1'])) {
                        $sheet->setCellValue('U' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + $i), $ResultOwner[$i]['OBTAIN_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P1'])) {
                        $sheet->setCellValue('V' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('V' . (5 + $current + $i), $ResultOwner[$i]['NUM_P1']);
                    }



                    if (empty($ResultOwner[$i]['OWNER_ORDER_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner)  + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_ORDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PID_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWN_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_GENDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_BDATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P2'])) {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PN_STS_P2']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P2'])) {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NATIONALITY_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P2'])) {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RACE_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P2'])) {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RELIGION_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P2'])) {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['FAT_P2']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P2'])) {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['MOT_P2']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P2'])) {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['REG_ADDR_P2']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P2'])) {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RECV_DATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P2'])) {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OBTAIN_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P2'])) {
                        $sheet->setCellValue('V' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('V' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NUM_P2']);
                    }

                    $ownerCellIndex = (5 + $current + count($ResultOwner) + $i);

                    if ($sts == 'e') {
							$problemDesc = '';
                        if (!$dataSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                        } else if (!$dataSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                        } else {
                            if ($ResultOwner[$i]['OWNER_ORDER_P1'] != $ResultOwner[$i]['OWNER_ORDER_P2']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PID_P1'] != $ResultOwner[$i]['OWNER_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWN_P1'] != $ResultOwner[$i]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_GENDER_P1'] != $ResultOwner[$i]['OWNER_GENDER_P2']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_BDATE_P1'] != $ResultOwner[$i]['OWNER_BDATE_P2']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PN_STS_P1'] != $ResultOwner[$i]['OWNER_PN_STS_P2']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NATIONALITY_NAME_P1'] != $ResultOwner[$i]['NATIONALITY_NAME_P2']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RACE_NAME_P1'] != $ResultOwner[$i]['RACE_NAME_P2']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RELIGION_NAME_P1'] != $ResultOwner[$i]['RELIGION_NAME_P2']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['FAT_P1'] != $ResultOwner[$i]['FAT_P2']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['MOT_P1'] != $ResultOwner[$i]['MOT_P2']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['REG_ADDR_P1'] != $ResultOwner[$i]['REG_ADDR_P2']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RECV_DATE_P1'] != $ResultOwner[$i]['RECV_DATE_P2']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OBTAIN_NAME_P1'] != $ResultOwner[$i]['OBTAIN_NAME_P2']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NUM_P1'] != $ResultOwner[$i]['NUM_P2']) $problemDesc .= "สัดส่วนการได้มาไม่ตรงกัน\n";
                        }

                        $sheet->setCellValue('W' . (5 + $current + $i), rtrim($problemDesc));
                    }
                }

                $current += count($ResultOwner) * 2;
            }
        }

        //=========================== PROCESS TRANSACTION ==========================
        $current = 0;
        $processP1 = array();
        $processP2 = array();
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        for ($j = 0; $j < $last; $j++) {
            $ResultProcess = array();

            $parcelSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $parcelSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            if ($parcelSeqP1 != null) {
                $table = 'P1';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }
            if ($parcelSeqP2 != null) {
                $table = 'P2';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }

    

            if (count($ResultProcess) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));


                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['PARCEL_LAND_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['PARCEL_LAND_NAME']);
                    }
                    if (empty($Result[$j]['YEAR'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['YEAR']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    $ownerCellIndex = (5 + $current);
                }

                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('G' . (5 + $current) . ':G' . (5 + $current + (count($ResultProcess) / 2) - 1));
                $sheet->mergeCells('G' . (5 + $current + (count($ResultProcess) / 2)) . ':G' . (5 + $current + count($ResultProcess) - 1));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['PARCEL_LAND_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['PARCEL_LAND_NAME']);
                    }
                    if (empty($Result[$j]['YEAR'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['YEAR']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    $ownerCellIndex = (5 + $current);
                }


                $sheet->setCellValue('G' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('G' . (5 + $current + (count($ResultProcess) / 2)), 'พัฒน์ฯ 2');
                $ownerDesc = '';
  

                for ($i = 0; $i < count($ResultProcess); $i++) {
                    if ($ResultProcess[$i]['KEY'] == '1') {
                        array_push($processP1, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    } else if ($ResultProcess[$i]['KEY'] == '2') {
                        array_push($processP2, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    }
                    $transactionCellIndex = (5 + $current + count($ResultProcess) + $i);
                }

                $current += count($ResultProcess);
            }
        }
       
        if ($sts == 'e') {
            for ($i = 0; $i < $last; $i++) {
                $problemDesc = '';
                if (!$parcelSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$parcelSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($processP1[$i]['MANAGE_QUEUE_DTM'] != $processP2[$i]['MANAGE_QUEUE_DTM']) $problemDesc .= "วันที่จดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['MANAGE_QUEUE_NO'] != $processP2[$i]['MANAGE_QUEUE_NO']) $problemDesc .= "เลขคิวไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_ORDER'] != $processP2[$i]['PROCESS_ORDER']) $problemDesc .= "ลำดับการจดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_REGIST_NAME'] != $processP2[$i]['PROCESS_REGIST_NAME']) $problemDesc .= "ชื่อรายการจดทะเบียนไม่ตรงกัน\n";
                }
                $sheet->setCellValue('L' . (5 + $current), rtrim($problemDesc));
                $current += 2;
            }
        }


        //================== SEQUESTER ===============================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        for ($j = 0; $j < count($sequester_seq); $j++) {

            $ResultSequester = array();


            $parcelSeqP1 = $sequester_seq[$j];
            $parcelSeqP2 = $sequester_seq[$j];
            include 'sequesterEachParcelExcel.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultSequester[] = $row;
            }



            $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));



            $sheet->setCellValue('A' . (5 + $current), ($j + 1));
            $sheet->setCellValue('B' . (5 + $current), $Result[$sequester_index[$j]]['PARCEL_LAND_NO']);
            $sheet->setCellValue('C' . (5 + $current), $Result[$sequester_index[$j]]['AMPHUR_NAME']);
            $sheet->setCellValue('D' . (5 + $current), $Result[$sequester_index[$j]]['TAMBOL_NAME']);



            $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultSequester) * 2) - 1));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultSequester) * 2) - 1));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultSequester) * 2) - 1));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultSequester) * 2) - 1));
            $sheet->mergeCells('T' . (5 + $current) . ':T' . (5 + $current + (count($ResultSequester) * 2) - 1));


            // $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultSequester) / 2) - 1));
            // $sheet->mergeCells('E' . (5 + $current + (count($ResultSequester) / 2)) . ':E' . (5 + $current + (count($ResultSequester)*2) - 1));


            $sheet->setCellValue('E' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('E' . (5 + $current + count($ResultSequester)), 'พัฒน์ฯ 2');
            $ownerDesc = '';
            $current += 2;

            for ($i = 0; $i < count($ResultSequester); $i++) {
                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO'])) {
                    $sheet->setCellValue('F' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO'])) {
                    $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM'])) {
                    $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT'])) {
                    $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME'])) {
                    $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + $i), $ResultSequester[$i]['TITLE_NAME_OWN'] . $ResultSequester[$i]['OWNER_FNAME'] . ' ' . $ResultSequester[$i]['OWNER_LNAME']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'])) {
                    $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM'])) {
                    $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM'])) {
                    $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'])) {
                    $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'])) {
                    $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM'])) {
                    $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'])) {
                    $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION']);
                }
                if (empty($ResultSequester[$i]['OWNER_NAME'])) {
                    $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('S' . (5 + $current + $i), $ResultSequester[$i]['OWNER_NAME']);
                }

                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO_1'])) {
                    $sheet->setCellValue('F' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO_1'])) {
                    $sheet->setCellValue('G' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM_1'])) {
                    $sheet->setCellValue('H' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT_1'])) {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME_1'])) {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['TITLE_NAME_OWN_1'] . $ResultSequester[$i]['OWNER_FNAME_1'] . ' ' . $ResultSequester[$i]['OWNER_LNAME_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1'])) {
                    $sheet->setCellValue('L' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM_1'])) {
                    $sheet->setCellValue('M' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM_1'])) {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1'])) {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1'])) {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM_1'])) {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1'])) {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']);
                }
                if (empty($ResultSequester[$i]['OWNER_NAME_1'])) {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['OWNER_NAME_1']);
                }
                $sequesterCellIndex = (5 + $current + count($ResultSequester) + $i);

                $tempIndex = 0;


                if ($sts == 'e') {
                    $problemDesc = '';
                    if (!$dataSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$dataSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                    } else {
                        if ($ResultSequester[$i]['SEQUESTER_ORDER_NO'] != $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_NO'] != $ResultSequester[$i]['SEQUESTER_DOC_NO_1']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_DTM'] != $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DEPT'] != $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_PID'] != $ResultSequester[$i]['SEQUESTER_REQ_PID_1']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_FNAME'] != $ResultSequester[$i]['OWNER_FNAME_1']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DTM'] != $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REC_DTM'] != $ResultSequester[$i]['SEQUESTER_REC_DTM_1']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REM'] != $ResultSequester[$i]['SEQUESTER_REM_1']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'] != $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_NAME'] != $ResultSequester[$i]['OWNER_NAME_1']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                    }
                    $sheet->setCellValue('S' . (5 + $current + $tempIndex), rtrim($problemDesc));
                }
            }

            $current += count($ResultSequester) * 2;
        }

        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $sheet->mergeCells('A1:S1');
        $sheet->mergeCells('A2:S2');
        $sheet->mergeCells('A3:S3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.3ก';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            }
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'ชื่อ น.ส.ล.')
            ->setCellValue('D4', 'ปีที่ออก')
            ->setCellValue('E4', 'อำเภอ')
            ->setCellValue('F4', 'ตำบล')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2')
            ->setCellValue('H4', 'ชื่อประเภทเอกสารสิทธิ')
            ->setCellValue('I4', 'ตำบล/แขวง')
            ->setCellValue('J4', 'อำเภอ/เขต')
            ->setCellValue('K4', 'ขนาดพื้นที่')
            ->setCellValue('L4', 'ระวาง UTM')
            ->setCellValue('M4', 'ระวางศูนย์กำเนิด')
            ->setCellValue('N4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('O4', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('P4', 'สถานะ อปท.')
            ->setCellValue('Q4', 'สถานะการอายัด')
            ->setCellValue('R4', 'การใช้ประโยชน์')
            ->setCellValue('S4', 'หมายเหตุ')
            ->getStyle('A1:S4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('S4:S' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);

        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->mergeCells('A1:P1');
        $sheet->mergeCells('A2:P2');
        $sheet->mergeCells('A3:P3');
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            }
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'ชื่อ น.ส.ล.')
            ->setCellValue('D4', 'ปีที่ออก')
            ->setCellValue('E4', 'อำเภอ')
            ->setCellValue('F4', 'ตำบล')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2')
            ->setCellValue('H4', 'ชื่อหนังสือสัญญา')
            ->setCellValue('I4', 'วันที่หนังสือสัญญา')
            ->setCellValue('J4', 'วันที่เริ่มสัญญา')
            ->setCellValue('K4', 'ราคาทุนทรัพย์')
            ->setCellValue('L4', 'ระยะเวลา')
            ->setCellValue('M4', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('N4', 'ผู้ให้สัญญา')
            ->setCellValue('O4', 'ผู้รับสัญญา')
            ->setCellValue('P4', 'หมายเหตุ')
            ->getStyle('A1:P4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('P4:P' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        $sheet->mergeCells('A1:W1');
        $sheet->mergeCells('A2:W2');
        $sheet->mergeCells('A3:W3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            }
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'ชื่อ น.ส.ล.')
            ->setCellValue('D4', 'ปีที่ออก')
            ->setCellValue('E4', 'อำเภอ')
            ->setCellValue('F4', 'ตำบล')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('I4:I' . $ownerCellIndex)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);

        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2')
            ->setCellValue('H4', 'ลำดับผู้ถือกรรมสิทธิ์')
            ->setCellValue('I4', 'เลขบัตรประจำตัวประชาชน')
            ->setCellValue('J4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('K4', 'เพศผู้ถือกรรมสิทธิ์')
            ->setCellValue('L4', 'วันเกิดผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'สถานะสมรสผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'สัญชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('O4', 'เชื้อชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('P4', 'ศาสนาผู้ถือกรรมสิทธิ์')
            ->setCellValue('Q4', 'ชื่อบิดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('R4', 'ชื่อมารดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('S4', 'ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์')
            ->setCellValue('T4', 'วันที่ได้มา')
            ->setCellValue('U4', 'การได้มา')
            ->setCellValue('V4', 'สัดส่วนการได้มา')
            ->setCellValue('W4', 'หมายเหตุ')
            ->getStyle('A1:W4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('W4:W' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $sheet->mergeCells('A1:L1');
        $sheet->mergeCells('A2:L2');
        $sheet->mergeCells('A3:L3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            }
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'ชื่อ น.ส.ล.')
            ->setCellValue('D4', 'ปีที่ออก')
            ->setCellValue('E4', 'อำเภอ')
            ->setCellValue('F4', 'ตำบล')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2')
            ->setCellValue('H4', 'วันที่จดทะเบียน')
            ->setCellValue('I4', 'เลขคิว')
            ->setCellValue('J4', 'ลำดับการจดทะเบียน')
            ->setCellValue('K4', 'ชื่อรายการจดทะเบียน')
            ->setCellValue('L4', 'หมายเหตุ')
            ->getStyle('A1:L4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L4:L' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        $sheet->mergeCells('A1:V1');
        $sheet->mergeCells('A2:V2');
        $sheet->mergeCells('A3:V3');
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - น.ส.ล.';
            }
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'ชื่อ น.ส.ล.')
            ->setCellValue('D4', 'ปีที่ออก')
            ->setCellValue('E4', 'อำเภอ')
            ->setCellValue('F4', 'ตำบล')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2')
            ->setCellValue('H4', 'อายัดลำดับที่')
            ->setCellValue('I4', 'เลขที่คำสั่ง/หนังสือ')
            ->setCellValue('J4', 'วันที่คำสั่ง/หนังสือ')
            ->setCellValue('K4', 'หน่วยงานที่ขออายัด')
            ->setCellValue('L4', 'เลขที่บัตรประชาชนของผู้ขออายัด')
            ->setCellValue('M4', 'ชื่อ-สกุลผู้ขออายัด')
            ->setCellValue('N4', 'สถานะกำหนดอายัด')
            ->setCellValue('O4', 'วันที่ขออายัด')
            ->setCellValue('P4', 'วันที่รับอายัด')
            ->setCellValue('Q4', 'จำนวนวันที่รับอายัด')
            ->setCellValue('R4', 'วันที่สิ้นสุดอายัด')
            ->setCellValue('S4', 'เหตุผลที่ขออายัด')
            ->setCellValue('T4', 'เงื่อนไขการอายัด')
            ->setCellValue('U4', 'ชื่อผู้ถูกอายัด')
            ->setCellValue('V4', 'หมายเหตุ')
            ->getStyle('A1:V4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('V4:V' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        for ($j = 0; $j < 4; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);
            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
                'font' => array(
                    'bold'  => false,
                    'color' => array('rgb' => '000000'),
                    'size'  => 16,
                    'name'  => 'TH SarabunPSK'
                )
            ];
            if ($j == 0) {
                $sheet->getStyle('A1:S' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:S4')->getFont()->setBold(true);

                $sheet->getStyle('A4:S' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 1) {
                $sheet->getStyle('A1:P' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:P4')->getFont()->setBold(true);

                $sheet->getStyle('A4:P' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 2) {
                $sheet->getStyle('A1:W' . $ownerCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:W4')->getFont()->setBold(true);

                $sheet->getStyle('A4:W' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 3) {
                $sheet->getStyle('A1:L' . $transactionCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:L4')->getFont()->setBold(true);
            } else {
                $sheet->getStyle('A1:V' . $transactionCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:V4')->getFont()->setBold(true);

                $sheet->getStyle('A4:V' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save("C:\Users\MGT1\Desktop\htdoc\\" . $landoffice . '-' . $fileName . ".xlsx");


        break;




        break;
    case '23':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $ownerCellIndex = 0;
        $transactionCellIndex = 0;
        $sequesterCellIndex = 0;

        $errorParcel = 0;
        $errorOlgt = 0;
        $errorOwner = 0;
        $errortransaction = 0;
        $errorSequester = 0;

        $sequester_seq = array();
        $sequester_index = array();


        $current = 0;
        //===================PARCEL DETAIL=====================================
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];

            include 'queryDetailData.php';
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);



            if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$j]['MOO'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                }
                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
            }

            $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('F' . (6 + $current), 'พัฒน์ฯ 2');

            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AREA_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultDetail[0]['AREA_P1']);
            }
            if (empty($ResultDetail[0]['UTM_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultDetail[0]['UTM_P1']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultDetail[0]['ORIGIN_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'])) {
                $sheet->setCellValue('N' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'])) {
                $sheet->setCellValue('O' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('P' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'])) {
                $sheet->setCellValue('Q' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (5 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1']);
            }


            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AREA_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultDetail[0]['AREA_P2']);
            }
            if (empty($ResultDetail[0]['UTM_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultDetail[0]['UTM_P2']);
            }
            if (empty($ResultDetail[0]['ORIGIN_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultDetail[0]['ORIGIN_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('P' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if (empty($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2'])) {
                $sheet->setCellValue('Q' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (6 + $current), $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']);
            }
            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultDetail[0]['PARCEL_LAND_NO_P1'] != $ResultDetail[0]['PARCEL_LAND_NO_P2']) $problemDesc .= "เลขเอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'] != $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']) $problemDesc .= "ชื่อประเภทเอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['TAMBOL_NAME_P1'] != $ResultDetail[0]['TAMBOL_NAME_P2']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AMPHUR_NAME_P1'] != $ResultDetail[0]['AMPHUR_NAME_P2']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AREA_P1'] != $ResultDetail[0]['AREA_P2']) $problemDesc .= "ขนาดพื้นที่ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['UTM_P1'] != $ResultDetail[0]['UTM_P2']) $problemDesc .= "ระวาง UTM ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['ORIGIN_P1'] != $ResultDetail[0]['ORIGIN_P2']) $problemDesc .= "ระวางศูนย์กำเนิดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['OWN_P1'] != $ResultDetail[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P1'] != $ResultDetail[0]['PARCEL_LAND_OWNER_NUM_P2']) $problemDesc .= "จำนวนผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P1'] != $ResultDetail[0]['PARCEL_LAND_OPT_FLAG_P2']) $problemDesc .= "สถานะ อปท. ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P1'] != $ResultDetail[0]['PARCEL_LAND_SEQUEST_STS_P2']) $problemDesc .= "สถานะการอายัดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P1'] != $ResultDetail[0]['PARCEL_LAND_LANDUSED_DESC_P2']) $problemDesc .= "การใช้ประโยชน์ไม่ตรงกัน\n";
                }
                $sheet->setCellValue('R' . (5 + $current), rtrim($problemDesc));
            }



            $current += 2;
        }
        $current = 0;
        //=================================== OLGT =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        for ($j = 0; $j < $last; $j++) {

            $ResultOlgt = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }


            if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                }
                if (empty($Result[$j]['MOO'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                }
                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
            }

            $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('F' . (6 + $current), 'พัฒน์ฯ 2');
            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('M' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P1'])) {
                    $sheet->setCellValue('N' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('M' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('M' . (6 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('N' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (6 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }
            if ($sts == 'e') {
				$problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'] != $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']) $problemDesc .= "ชื่อหนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'] != $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']) $problemDesc .= "วันที่หนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'] != $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']) $problemDesc .= "วันที่เริ่มสัญญา\n";
                    if ($ResultOlgt[0]['OLGT_HD_MNY_P1'] != $ResultOlgt[0]['OLGT_HD_MNY_P2']) $problemDesc .= "ราคาทุนทรัพย์ไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['DURATION_P1'] != $ResultOlgt[0]['DURATION_P2']) $problemDesc .= "ระยะเวลาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_PMS_PID_P1'] != $ResultOlgt[0]['OLGT_PMS_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) && $ResultOlgt[0]['OWN_P1'] != $ResultOlgt[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ให้สัญญาไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) && $ResultOlgt[1]['OWN_P1'] != $ResultOlgt[1]['OWN_P2']) $problemDesc .= "ชื่อผู้รับสัญญาไม่ตรงกัน\n";
                }

                $sheet->setCellValue('O' . (5 + $current), rtrim($problemDesc));
            }

            $current += 2;
        }

        $current = 0;
        //=================================== OWNER =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        for ($j = 0; $j < $last; $j++) {
            $ResultOwner = array();

            $dataSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            include 'queryOwnDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOwner[] = $row;
            }
            $tempIndex = 0;



            if (count($ResultOwner) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (6 + $current));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['MOO'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }

                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('U' . (5 + $current) . ':U' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current + (count($ResultOwner))) . ':E' . (5 + $current + (count($ResultOwner) * 2) - 1));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['MOO'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    $ownerCellIndex = (5 + $current);
                }



                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (5 + $current + count($ResultOwner)), 'พัฒน์ฯ 2');
                $ownerDesc = '';

                for ($i = 0; $i < count($ResultOwner); $i++) {
                    if (empty($ResultOwner[$i]['OWNER_ORDER_P1'])) {
                        $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + $i), $ResultOwner[$i]['OWNER_ORDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P1'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PID_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultOwner[$i]['OWN_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultOwner[$i]['OWNER_GENDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultOwner[$i]['OWNER_BDATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PN_STS_P1']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P1'])) {
                        $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + $i), $ResultOwner[$i]['NATIONALITY_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P1'])) {
                        $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + $i), $ResultOwner[$i]['RACE_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P1'])) {
                        $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + $i), $ResultOwner[$i]['RELIGION_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P1'])) {
                        $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + $i), $ResultOwner[$i]['FAT_P1']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P1'])) {
                        $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + $i), $ResultOwner[$i]['MOT_P1']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P1'])) {
                        $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + $i), $ResultOwner[$i]['REG_ADDR_P1']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P1'])) {
                        $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + $i), $ResultOwner[$i]['RECV_DATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P1'])) {
                        $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + $i), $ResultOwner[$i]['OBTAIN_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P1'])) {
                        $sheet->setCellValue('U' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + $i), $ResultOwner[$i]['NUM_P1']);
                    }



                    if (empty($ResultOwner[$i]['OWNER_ORDER_P2'])) {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultOwner)  + $i), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_ORDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PID_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWN_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_GENDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_BDATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PN_STS_P2']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P2'])) {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NATIONALITY_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P2'])) {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RACE_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P2'])) {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RELIGION_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P2'])) {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['FAT_P2']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P2'])) {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['MOT_P2']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P2'])) {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['REG_ADDR_P2']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P2'])) {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RECV_DATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P2'])) {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OBTAIN_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P2'])) {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NUM_P2']);
                    }

                    $ownerCellIndex = (5 + $current + count($ResultOwner) + $i);

                    if ($sts == 'e') {
$problemDesc = '';
                        if (!$dataSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                        } else if (!$dataSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                        } else {
                            if ($ResultOwner[$i]['OWNER_ORDER_P1'] != $ResultOwner[$i]['OWNER_ORDER_P2']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PID_P1'] != $ResultOwner[$i]['OWNER_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWN_P1'] != $ResultOwner[$i]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_GENDER_P1'] != $ResultOwner[$i]['OWNER_GENDER_P2']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_BDATE_P1'] != $ResultOwner[$i]['OWNER_BDATE_P2']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PN_STS_P1'] != $ResultOwner[$i]['OWNER_PN_STS_P2']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NATIONALITY_NAME_P1'] != $ResultOwner[$i]['NATIONALITY_NAME_P2']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RACE_NAME_P1'] != $ResultOwner[$i]['RACE_NAME_P2']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RELIGION_NAME_P1'] != $ResultOwner[$i]['RELIGION_NAME_P2']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['FAT_P1'] != $ResultOwner[$i]['FAT_P2']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['MOT_P1'] != $ResultOwner[$i]['MOT_P2']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['REG_ADDR_P1'] != $ResultOwner[$i]['REG_ADDR_P2']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RECV_DATE_P1'] != $ResultOwner[$i]['RECV_DATE_P2']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OBTAIN_NAME_P1'] != $ResultOwner[$i]['OBTAIN_NAME_P2']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NUM_P1'] != $ResultOwner[$i]['NUM_P2']) $problemDesc .= "สัดส่วนการได้มาไม่ตรงกัน\n";
                        }

                        $sheet->setCellValue('V' . (5 + $current + $i), rtrim($problemDesc));
                    }
                }

                $current += count($ResultOwner) * 2;
            }
        }

        //=========================== PROCESS TRANSACTION ==========================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $processP1 = array();
        $processP2 = array();

        for ($j = 0; $j < $last; $j++) {
            $ResultProcess = array();

            $parcelSeqP1 = $Result[$j]['PARCEL_LAND_SEQ_P1'];
            $parcelSeqP2 = $Result[$j]['PARCEL_LAND_SEQ_P2'];


            if ($parcelSeqP1 != null) {
                $table = 'P1';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }
            if ($parcelSeqP2 != null) {
                $table = 'P2';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }



            if (count($ResultProcess) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));


                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['MOO'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }

                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('G' . (5 + $current) . ':G' . (5 + $current + (count($ResultProcess) / 2) - 1));
                $sheet->mergeCells('G' . (5 + $current + (count($ResultProcess) / 2)) . ':G' . (5 + $current + count($ResultProcess) - 1));

                if ($Result[$j]['PARCEL_LAND_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['PARCEL_LAND_NO'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['PARCEL_LAND_NO']);
                    }
                    if (empty($Result[$j]['MOO'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['MOO']);
                    }
                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                }

                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (5 + $current + (count($ResultProcess) / 2)), 'พัฒน์ฯ 2');
                $ownerDesc = '';


                for ($i = 0; $i < count($ResultProcess); $i++) {
                    if ($ResultProcess[$i]['KEY'] == '1') {
                        array_push($processP1, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    } else if ($ResultProcess[$i]['KEY'] == '2') {
                        array_push($processP2, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    }
                }

                $current += count($ResultProcess);
            }
        }
        $current = 0;

        if ($sts == 'e') {
            for ($i = 0; $i < $last; $i++) {
                $problemDesc = '';
                if (!$parcelSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$parcelSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($processP1[$i]['MANAGE_QUEUE_DTM'] != $processP2[$i]['MANAGE_QUEUE_DTM']) $problemDesc .= "วันที่จดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['MANAGE_QUEUE_NO'] != $processP2[$i]['MANAGE_QUEUE_NO']) $problemDesc .= "เลขคิวไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_ORDER'] != $processP2[$i]['PROCESS_ORDER']) $problemDesc .= "ลำดับการจดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_REGIST_NAME'] != $processP2[$i]['PROCESS_REGIST_NAME']) $problemDesc .= "ชื่อรายการจดทะเบียนไม่ตรงกัน\n";
                }
                $sheet->setCellValue('K' . (5 + $current), rtrim($problemDesc));
                $current += 2;
            }
        }




        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $sheet->mergeCells('A1:R1');
        $sheet->mergeCells('A2:R2');
        $sheet->mergeCells('A3:R3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            }
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'หมู่')
            ->setCellValue('D4', 'อำเภอ')
            ->setCellValue('E4', 'ตำบล')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'เลขที่เอกสารสิทธิ')
            ->setCellValue('H4', 'ตำบล/แขวง')
            ->setCellValue('I4', 'อำเภอ/เขต')
            ->setCellValue('J4', 'ขนาดพื้นที่')
            ->setCellValue('K4', 'ระวาง UTM')
            ->setCellValue('L4', 'ระวางศูนย์กำเนิด')
            ->setCellValue('M4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('O4', 'สถานะ อปท.')
            ->setCellValue('P4', 'สถานะการอายัด')
            ->setCellValue('Q4', 'การใช้ประโยชน์')
            ->setCellValue('R4', 'หมายเหตุ')
            ->getStyle('A1:R4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('R4:R' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);

        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->mergeCells('A1:P1');
        $sheet->mergeCells('A2:P2');
        $sheet->mergeCells('A3:P3');
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            }
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'หมู่')
            ->setCellValue('D4', 'อำเภอ')
            ->setCellValue('E4', 'ตำบล')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'ชื่อหนังสือสัญญา')
            ->setCellValue('H4', 'วันที่หนังสือสัญญา')
            ->setCellValue('I4', 'วันที่เริ่มสัญญา')
            ->setCellValue('J4', 'ราคาทุนทรัพย์')
            ->setCellValue('K4', 'ระยะเวลา')
            ->setCellValue('L4', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('M4', 'ผู้ให้สัญญา')
            ->setCellValue('N4', 'ผู้รับสัญญา')
            ->setCellValue('O4', 'หมายเหตุ')
            ->getStyle('A1:O4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('O4:O' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        $sheet->mergeCells('A1:W1');
        $sheet->mergeCells('A2:W2');
        $sheet->mergeCells('A3:W3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            }
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'หมู่')
            ->setCellValue('D4', 'อำเภอ')
            ->setCellValue('E4', 'ตำบล')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('H4:H' . $last)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'ลำดับผู้ถือกรรมสิทธิ์')
            ->setCellValue('H4', 'เลขบัตรประจำตัวประชาชน')
            ->setCellValue('I4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('J4', 'เพศผู้ถือกรรมสิทธิ์')
            ->setCellValue('K4', 'วันเกิดผู้ถือกรรมสิทธิ์')
            ->setCellValue('L4', 'สถานะสมรสผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'สัญชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'เชื้อชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('O4', 'ศาสนาผู้ถือกรรมสิทธิ์')
            ->setCellValue('P4', 'ชื่อบิดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('Q4', 'ชื่อมารดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('R4', 'ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์')
            ->setCellValue('S4', 'วันที่ได้มา')
            ->setCellValue('T4', 'การได้มา')
            ->setCellValue('U4', 'สัดส่วนการได้มา')
            ->setCellValue('V4', 'หมายเหตุ')
            ->getStyle('A1:V4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('V4:V' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $sheet->mergeCells('A1:L1');
        $sheet->mergeCells('A2:L2');
        $sheet->mergeCells('A3:L3');

        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            }
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            if ($check == '2') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else if ($check == '3') {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            } else {
                $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต');
                $sheet->setCellValue('A3', $branchName);
                $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
                $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - หนังสืออนุญาต';
            }
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'เลขเอกสารสิทธิ')
            ->setCellValue('C4', 'หมู่')
            ->setCellValue('D4', 'อำเภอ')
            ->setCellValue('E4', 'ตำบล')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'วันที่จดทะเบียน')
            ->setCellValue('H4', 'เลขคิว')
            ->setCellValue('I4', 'ลำดับการจดทะเบียน')
            ->setCellValue('J4', 'ชื่อรายการจดทะเบียน')
            ->setCellValue('K4', 'หมายเหตุ')
            ->getStyle('A1:K4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K4:K' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);

        for ($j = 0; $j < 3; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);
            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
                'font' => array(
                    'bold'  => false,
                    'color' => array('rgb' => '000000'),
                    'size'  => 16,
                    'name'  => 'TH SarabunPSK'
                )
            ];
            if ($j == 0) {
                $sheet->getStyle('A1:R' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:R4')->getFont()->setBold(true);

                $sheet->getStyle('A4:R' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 1) {
                $sheet->getStyle('A1:O' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:O4')->getFont()->setBold(true);

                $sheet->getStyle('A4:O' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 2) {
                $sheet->getStyle('A1:V' . $ownerCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:V4')->getFont()->setBold(true);

                $sheet->getStyle('A4:V' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 3) {
                $sheet->getStyle('A1:K' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:K4')->getFont()->setBold(true);
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save("C:\Users\MGT1\Desktop\htdoc\\" . $landoffice . '-' . $fileName . ".xlsx");


        break;
        // CONDO
    case '13':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";

        $ownerCellIndex = 0;
        $transactionCellIndex = 0;
        $sequesterCellIndex = 0;

        $sequester_seq = array();
        $sequester_index = array();


        $errorParcel = 0;
        $errorOlgt = 0;
        $errorOwner = 0;
        $errortransaction = 0;
        $errorSequester = 0;


        $current = 0;
        //===================PARCEL DETAIL=====================================
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();

            $dataSeqP1 = $Result[$j]['CONDO_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['CONDO_SEQ_P2'];

            include 'queryDetailData.php';
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);





            $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
            $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

            if ($Result[$j]['CONDO_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }

                if (empty($Result[$j]['CONDO_REG_YEAR'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONDO_REG_YEAR']);
                }
                if (empty($Result[$j]['CONDO_NAME_TH'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONDO_NAME_TH']);
                }
            }

            $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('F' . (6 + $current), 'พัฒน์ฯ 2');

            if (empty($ResultDetail[0]['CONDO_ID_P1'])) {
                $sheet->setCellValue('G' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (5 + $current), $ResultDetail[0]['CONDO_ID_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_NAME_TH_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultDetail[0]['CONDO_NAME_TH_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_ROOM_NUM_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultDetail[0]['CONDO_ROOM_NUM_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_RECV_DATE_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultDetail[0]['CONDO_RECV_DATE_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_REGISTER_DATE_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultDetail[0]['CONDO_REGISTER_DATE_P1']);
            }
            if (empty($ResultDetail[0]['CORP_ID_P1'])) {
                $sheet->setCellValue('N' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (5 + $current), $ResultDetail[0]['CORP_ID_P1']);
            }
            if (empty($ResultDetail[0]['ADDR_P1'])) {
                $sheet->setCellValue('O' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (5 + $current), $ResultDetail[0]['ADDR_P1']);
            }



            if (empty($ResultDetail[0]['CONDO_ID_P2'])) {
                $sheet->setCellValue('G' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('G' . (6 + $current), $ResultDetail[0]['CONDO_ID_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_NAME_TH_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultDetail[0]['CONDO_NAME_TH_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_ROOM_NUM_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultDetail[0]['CONDO_ROOM_NUM_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_RECV_DATE_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultDetail[0]['CONDO_RECV_DATE_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_REGISTER_DATE_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultDetail[0]['CONDO_REGISTER_DATE_P2']);
            }
            if (empty($ResultDetail[0]['CORP_ID_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultDetail[0]['CORP_ID_P2']);
            }
            if (empty($ResultDetail[0]['ADDR_P2'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $ResultDetail[0]['ADDR_P2']);
            }

            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                } else {

                    if ($ResultDetail[0]['CONDO_ID_P1'] != $ResultDetail[0]['CONDO_ID_P2']) $problemDesc .= "เลขที่อาคารชุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONDO_NAME_TH_P1'] != $ResultDetail[0]['CONDO_NAME_TH_P2']) $problemDesc .= "ชื่ออาคารชุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AMPHUR_NAME_P1'] != $ResultDetail[0]['AMPHUR_NAME_P2']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                    if ($ResultDetail[0]['TAMBOL_NAME_P1'] != $ResultDetail[0]['TAMBOL_NAME_P2']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONDO_ROOM_NUM_P1'] != $ResultDetail[0]['CONDO_ROOM_NUM_P2']) $problemDesc .= "จำนวนห้องชุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONDO_RECV_DATE_P1'] != $ResultDetail[0]['CONDO_RECV_DATE_P2']) $problemDesc .= "วันที่ได้รับอนุญาตไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONDO_REGISTER_DATE_P1'] != $ResultDetail[0]['CONDO_REGISTER_DATE_P2']) $problemDesc .= "วันที่จดทะเบียนอาคารชุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CORP_ID_P1'] != $ResultDetail[0]['CORP_ID_P2']) $problemDesc .= "เลขที่นิติบุคคลอาคารชุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['ADDR_P1'] != $ResultDetail[0]['ADDR_P2']) $problemDesc .= "ที่ตั้งนิติบุคคลอาคารชุดไม่ตรงกัน\n";
                }
                $sheet->setCellValue('P' . (5 + $current), rtrim($problemDesc));
            }



            $current += 2;
        }
        //=========================== PROCESS TRANSACTION ==========================
        $current = 0;
        $processP1 = array();
        $processP2 = array();
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        for ($j = 0; $j < $last; $j++) {
            $ResultProcess = array();

            $parcelSeqP1 = $Result[$j]['CONDO_SEQ_P1'];
            $parcelSeqP2 = $Result[$j]['CONDO_SEQ_P2'];


            if ($parcelSeqP1 != null) {
                $table = 'P1';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }
            if ($parcelSeqP2 != null) {
                $table = 'P2';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }

            if (count($ResultProcess) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));



                if ($Result[$j]['CONDO_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }

                    if (empty($Result[$j]['CONDO_REG_YEAR'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONDO_REG_YEAR']);
                    }
                    if (empty($Result[$j]['CONDO_NAME_TH'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONDO_NAME_TH']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultProcess) / 2) - 1));
                $sheet->mergeCells('E' . (5 + $current + (count($ResultProcess) / 2)) . ':E' . (5 + $current + count($ResultProcess) - 1));
                // $sheet->mergeCells('K' . (5 + $current) . ':K' . (5 + $current + count($ResultProcess) - 1));
                if ($Result[$j]['CONDO_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }

                    if (empty($Result[$j]['CONDO_REG_YEAR'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONDO_REG_YEAR']);
                    }
                    if (empty($Result[$j]['CONDO_NAME_TH'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONDO_NAME_TH']);
                    }
                    $current += 2;
                }
                $sheet->setCellValue('F' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('F' . (5 + $current + (count($ResultProcess) / 2)), 'พัฒน์ฯ 2');
                $ownerDesc = '';
            

                for ($i = 0; $i < count($ResultProcess); $i++) {
                    if ($ResultProcess[$i]['KEY'] == '1') {
                        array_push($processP1, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    } else if ($ResultProcess[$i]['KEY'] == '2') {
                        array_push($processP2, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('G' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('G' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    }
                }

                $current += count($ResultProcess);
                $transactionCellIndex = (5 + $current + count($ResultProcess) + $i);
            }
        }
        $current = 0;


        if ($sts == 'e') {
            for ($i = 0; $i < $last; $i++) {
                $problemDesc = '';
                if (!$parcelSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$parcelSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($processP1[$i]['MANAGE_QUEUE_DTM'] != $processP2[$i]['MANAGE_QUEUE_DTM']) $problemDesc .= "วันที่จดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['MANAGE_QUEUE_NO'] != $processP2[$i]['MANAGE_QUEUE_NO']) $problemDesc .= "เลขคิวไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_ORDER'] != $processP2[$i]['PROCESS_ORDER']) $problemDesc .= "ลำดับการจดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_REGIST_NAME'] != $processP2[$i]['PROCESS_REGIST_NAME']) $problemDesc .= "ชื่อรายการจดทะเบียนไม่ตรงกัน\n";
                }
                $sheet->setCellValue('K' . (5 + $current), rtrim($problemDesc));
                $current += 2;
            }
        }

        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $sheet->mergeCells('A1:P1');
        $sheet->mergeCells('A2:P2');
        $sheet->mergeCells('A3:P3');


        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด';
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'เลขที่อาคารชุด')
            ->setCellValue('E4', 'ชื่ออาคารชุด')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2');
        $sheet->setCellValue('G4', 'เลขที่อาคารชุด');
        $sheet->setCellValue('H4', 'ชื่ออาคารชุด')
            ->setCellValue('I4', 'อำเภอ/เขต')
            ->setCellValue('J4', 'ตำบล/แขวง')
            ->setCellValue('K4', 'จำนวนห้องชุด')
            ->setCellValue('L4', 'วันที่ได้รับอนุญาต')
            ->setCellValue('M4', 'วันที่จดทะเบียนอาคารชุด')
            ->setCellValue('N4', 'เลขที่นิติบุคคลอาคารชุด')
            ->setCellValue('O4', 'ที่ตั้งนิติบุคคลอาคารชุด')
            ->setCellValue('P4', 'หมายเหตุ')
            ->getStyle('A1:P4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('P4:P' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);

        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->mergeCells('A1:K1');
        $sheet->mergeCells('A2:K2');
        $sheet->mergeCells('A3:K3');

        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารชุด';
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'เลขที่อาคารชุด')
            ->setCellValue('E4', 'ชื่ออาคารชุด')
            ->getStyle('A1:E4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->setCellValue('F4', 'พัฒน์ฯ 1/2')
            ->setCellValue('G4', 'วันที่จดทะเบียน')
            ->setCellValue('H4', 'เลขคิว')
            ->setCellValue('I4', 'ลำดับการจดทะเบียน')
            ->setCellValue('J4', 'ชื่อรายการจดทะเบียน')
            ->setCellValue('K4', 'หมายเหตุ')
            ->getStyle('A1:K4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('K4:K' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);

        for ($j = 0; $j < 2; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);
            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
                'font' => array(
                    'bold'  => false,
                    'color' => array('rgb' => '000000'),
                    'size'  => 16,
                    'name'  => 'TH SarabunPSK'
                )
            ];
            if ($j == 0) {
                $sheet->getStyle('A1:P' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:P4')->getFont()->setBold(true);

                $sheet->getStyle('A4:P' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else {
                $sheet->getStyle('A1:K' . $transactionCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:K4')->getFont()->setBold(true);

                $sheet->getStyle('A4:K' . $transactionCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //    $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        // if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
        //      $fileName = urlencode($fileName);
        // }
        // $fileName = urlencode($fileName);
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save("C:\Users\MGT1\Desktop\htdoc\\" . $landoffice . '-' . $fileName . ".xlsx");


        break;

    case '9':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $ownerCellIndex = 0;
        $transactionCellIndex = 0;
        $sequesterCellIndex = 0;

        $sequester_seq = array();
        $sequester_index = array();
        $errorParcel = 0;
        $errorOlgt = 0;
        $errorOwner = 0;
        $errortransaction = 0;
        $errorSequester = 0;


        $current = 0;
        //===================PARCEL DETAIL=====================================
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();




            $dataSeqP1 = $Result[$j]['CONDOROOM_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['CONDOROOM_SEQ_P2'];

            include 'queryDetailData.php';
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);




            if ($Result[$j]['CONDOROOM_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
                if (empty($Result[$j]['CONDOROOM_RNO'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONDOROOM_RNO']);
                }
                if (empty($Result[$j]['CONDO_REG_YEAR'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONDO_REG_YEAR']);
                }
                if (empty($Result[$j]['CONDO_NAME_TH'])) {
                    $sheet->setCellValue('F' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONDO_NAME_TH']);
                }
            }

            $sheet->setCellValue('G' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('G' . (6 + $current), 'พัฒน์ฯ 2');

            if (empty($ResultDetail[0]['CONDOROOM_RNO_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultDetail[0]['CONDOROOM_RNO_P1']);
            }
            if (empty($ResultDetail[0]['BLD_NAME_TH_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultDetail[0]['BLD_NAME_TH_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_ID_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultDetail[0]['CONDO_ID_P1']);
            }
            if (empty($ResultDetail[0]['CONDO_NAME_TH_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultDetail[0]['CONDO_NAME_TH_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_AREA_NUM_P1'])) {
                $sheet->setCellValue('N' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (5 + $current), $ResultDetail[0]['CONDOROOM_AREA_NUM_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('O' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (5 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_OWNER_NUM_P1'])) {
                $sheet->setCellValue('P' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (5 + $current), $ResultDetail[0]['CONDOROOM_OWNER_NUM_P1']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('Q' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (5 + $current), $ResultDetail[0]['CONDOROOM_SEQUEST_STS_P1'] == 1 ? 'ไม่มี' : 'ติดอายัด');

                if ($ResultDetail[0]['CONDOROOM_SEQUEST_STS_P1'] != 1) {
                    array_push($sequester_seq, $dataSeqP1);
                    array_push($sequester_index, $j);
                }
            }


            if (empty($ResultDetail[0]['CONDOROOM_RNO_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultDetail[0]['CONDOROOM_RNO_P2']);
            }
            if (empty($ResultDetail[0]['BLD_NAME_TH_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultDetail[0]['BLD_NAME_TH_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_ID_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultDetail[0]['CONDO_ID_P2']);
            }
            if (empty($ResultDetail[0]['CONDO_NAME_TH_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultDetail[0]['CONDO_NAME_TH_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_AREA_NUM_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultDetail[0]['CONDOROOM_AREA_NUM_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_OWNER_NUM_P2'])) {
                $sheet->setCellValue('P' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (6 + $current), $ResultDetail[0]['CONDOROOM_OWNER_NUM_P2']);
            }
            if (empty($ResultDetail[0]['CONDOROOM_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('Q' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (6 + $current), $ResultDetail[0]['CONDOROOM_SEQUEST_STS_P2'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }
            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                } else {
                    if ($ResultDetail[0]['CONDOROOM_RNO_P1'] != $ResultDetail[0]['CONDOROOM_RNO_P2']) $problemDesc .= "เลขที่ห้องชุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['BLD_NAME_TH_P1'] != $ResultDetail[0]['BLD_NAME_TH_P2']) $problemDesc .= "ชื่ออาคารไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONDO_ID_P1'] != $ResultDetail[0]['CONDO_ID_P2']) $problemDesc .= "เลขที่อาคารชุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONDO_NAME_TH_P1'] != $ResultDetail[0]['CONDO_NAME_TH_P2']) $problemDesc .= "ชื่ออาคารชุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AMPHUR_NAME_P1'] != $ResultDetail[0]['AMPHUR_NAME_P2']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                    if ($ResultDetail[0]['TAMBOL_NAME_P1'] != $ResultDetail[0]['TAMBOL_NAME_P2']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONDOROOM_AREA_NUM_P1'] != $ResultDetail[0]['CONDOROOM_AREA_NUM_P2']) $problemDesc .= "ขนาดพื้นที่ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['OWN_P1'] != $ResultDetail[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONDOROOM_OWNER_NUM_P1'] != $ResultDetail[0]['CONDOROOM_OWNER_NUM_P2']) $problemDesc .= "จำนวนผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONDOROOM_SEQUEST_STS_P1'] != $ResultDetail[0]['CONDOROOM_SEQUEST_STS_P2']) $problemDesc .= "สถานะการอายัดไม่ตรงกัน\n";
                }
                $sheet->setCellValue('R' . (5 + $current), rtrim($problemDesc));
            }



            $current += 2;
        }
        $current = 0;
        //=================================== OLGT =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        for ($j = 0; $j < $last; $j++) {

            $ResultOlgt = array();

            $dataSeqP1 = $Result[$j]['CONDOROOM_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['CONDOROOM_SEQ_P2'];


            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }



            if ($Result[$j]['CONDOROOM_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
                if (empty($Result[$j]['CONDOROOM_RNO'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONDOROOM_RNO']);
                }
                if (empty($Result[$j]['CONDO_REG_YEAR'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONDO_REG_YEAR']);
                }
                if (empty($Result[$j]['CONDO_NAME_TH'])) {
                    $sheet->setCellValue('F' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONDO_NAME_TH']);
                }
            }


            $sheet->setCellValue('G' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('G' . (6 + $current), 'พัฒน์ฯ 2');
            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('H' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('N' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P1'])) {
                    $sheet->setCellValue('O' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('H' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('H' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('N' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('N' . (6 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('O' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('O' . (6 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }

            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'] != $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']) $problemDesc .= "ชื่อหนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'] != $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']) $problemDesc .= "วันที่หนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'] != $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']) $problemDesc .= "วันที่เริ่มสัญญา\n";
                    if ($ResultOlgt[0]['OLGT_HD_MNY_P1'] != $ResultOlgt[0]['OLGT_HD_MNY_P2']) $problemDesc .= "ราคาทุนทรัพย์ไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['DURATION_P1'] != $ResultOlgt[0]['DURATION_P2']) $problemDesc .= "ระยะเวลาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_PMS_PID_P1'] != $ResultOlgt[0]['OLGT_PMS_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) && $ResultOlgt[0]['OWN_P1'] != $ResultOlgt[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ให้สัญญาไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) && $ResultOlgt[1]['OWN_P1'] != $ResultOlgt[1]['OWN_P2']) $problemDesc .= "ชื่อผู้รับสัญญาไม่ตรงกัน\n";
                }
                $sheet->setCellValue('P' . (5 + $current), rtrim($problemDesc));
            }

            $current += 2;
        }

        $current = 0;
        //=================================== OWNER =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        for ($j = 0; $j < $last; $j++) {
            $ResultOwner = array();

            $dataSeqP1 = $Result[$j]['CONDOROOM_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['CONDOROOM_SEQ_P2'];


            include 'queryOwnDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOwner[] = $row;
            }



            if (count($ResultOwner) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (6 + $current));

                if ($Result[$j]['CONDOROOM_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    if (empty($Result[$j]['CONDOROOM_RNO'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONDOROOM_RNO']);
                    }
                    if (empty($Result[$j]['CONDO_REG_YEAR'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONDO_REG_YEAR']);
                    }
                    if (empty($Result[$j]['CONDO_NAME_TH'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONDO_NAME_TH']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('W' . (5 + $current) . ':W' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current + (count($ResultOwner))) . ':E' . (5 + $current + (count($ResultOwner) * 2) - 1));

                if ($Result[$j]['CONDOROOM_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    if (empty($Result[$j]['CONDOROOM_RNO'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONDOROOM_RNO']);
                    }
                    if (empty($Result[$j]['CONDO_REG_YEAR'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONDO_REG_YEAR']);
                    }
                    if (empty($Result[$j]['CONDO_NAME_TH'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONDO_NAME_TH']);
                    }
                    $ownerCellIndex = (5 + $current);
                }

                $sheet->setCellValue('G' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('G' . (5 + $current + count($ResultOwner)), 'พัฒน์ฯ 2');
                $ownerDesc = '';

                for ($i = 0; $i < count($ResultOwner); $i++) {
                    if (empty($ResultOwner[$i]['OWNER_ORDER_P1'])) {
                        $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + $i), $ResultOwner[$i]['OWNER_ORDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PID_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultOwner[$i]['OWN_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultOwner[$i]['OWNER_GENDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultOwner[$i]['OWNER_BDATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P1'])) {
                        $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PN_STS_P1']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P1'])) {
                        $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + $i), $ResultOwner[$i]['NATIONALITY_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P1'])) {
                        $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + $i), $ResultOwner[$i]['RACE_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P1'])) {
                        $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + $i), $ResultOwner[$i]['RELIGION_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P1'])) {
                        $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + $i), $ResultOwner[$i]['FAT_P1']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P1'])) {
                        $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + $i), $ResultOwner[$i]['MOT_P1']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P1'])) {
                        $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + $i), $ResultOwner[$i]['REG_ADDR_P1']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P1'])) {
                        $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + $i), $ResultOwner[$i]['RECV_DATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P1'])) {
                        $sheet->setCellValue('U' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + $i), $ResultOwner[$i]['OBTAIN_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P1'])) {
                        $sheet->setCellValue('V' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('V' . (5 + $current + $i), $ResultOwner[$i]['NUM_P1']);
                    }



                    if (empty($ResultOwner[$i]['OWNER_ORDER_P2'])) {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner)  + $i), NULL);
                    } else {
                        $sheet->setCellValue('H' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_ORDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PID_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWN_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_GENDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_BDATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P2'])) {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PN_STS_P2']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P2'])) {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NATIONALITY_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P2'])) {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RACE_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P2'])) {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RELIGION_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P2'])) {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['FAT_P2']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P2'])) {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['MOT_P2']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P2'])) {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['REG_ADDR_P2']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P2'])) {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RECV_DATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P2'])) {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OBTAIN_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P2'])) {
                        $sheet->setCellValue('V' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('V' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NUM_P2']);
                    }

                    $ownerCellIndex = (5 + $current + count($ResultOwner) + $i);

                    $tempIndex = 0;


                    if ($sts == 'e') {
                        $problemDesc = '';
                        if (!$dataSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                        } else if (!$dataSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                        } else {
                            if ($ResultOwner[$i]['OWNER_ORDER_P1'] != $ResultOwner[$i]['OWNER_ORDER_P2']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PID_P1'] != $ResultOwner[$i]['OWNER_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWN_P1'] != $ResultOwner[$i]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_GENDER_P1'] != $ResultOwner[$i]['OWNER_GENDER_P2']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_BDATE_P1'] != $ResultOwner[$i]['OWNER_BDATE_P2']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PN_STS_P1'] != $ResultOwner[$i]['OWNER_PN_STS_P2']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NATIONALITY_NAME_P1'] != $ResultOwner[$i]['NATIONALITY_NAME_P2']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RACE_NAME_P1'] != $ResultOwner[$i]['RACE_NAME_P2']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RELIGION_NAME_P1'] != $ResultOwner[$i]['RELIGION_NAME_P2']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['FAT_P1'] != $ResultOwner[$i]['FAT_P2']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['MOT_P1'] != $ResultOwner[$i]['MOT_P2']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['REG_ADDR_P1'] != $ResultOwner[$i]['REG_ADDR_P2']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RECV_DATE_P1'] != $ResultOwner[$i]['RECV_DATE_P2']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OBTAIN_NAME_P1'] != $ResultOwner[$i]['OBTAIN_NAME_P2']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NUM_P1'] != $ResultOwner[$i]['NUM_P2']) $problemDesc .= "สัดส่วนการได้มาไม่ตรงกัน\n";
                        }

                        $sheet->setCellValue('W' . (5 + $current + $i), rtrim($problemDesc));
                    }
                }

                $current += count($ResultOwner) * 2;
            }
        }

        //=========================== PROCESS TRANSACTION ==========================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $processP1 = array();
        $processP2 = array();
        for ($j = 0; $j < $last; $j++) {
            $ResultProcess = array();

            $parcelSeqP1 = $Result[$j]['CONDOROOM_SEQ_P1'];
            $parcelSeqP2 = $Result[$j]['CONDOROOM_SEQ_P2'];


            if ($parcelSeqP1 != null) {
                $table = 'P1';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }
            if ($parcelSeqP2 != null) {
                $table = 'P2';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }


            if (count($ResultProcess) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                if ($Result[$j]['CONDOROOM_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    if (empty($Result[$j]['CONDOROOM_RNO'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONDOROOM_RNO']);
                    }
                    if (empty($Result[$j]['CONDO_REG_YEAR'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONDO_REG_YEAR']);
                    }
                    if (empty($Result[$j]['CONDO_NAME_TH'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONDO_NAME_TH']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('L' . (5 + $current) . ':L' . (5 + $current + count($ResultProcess) - 1));
                if ($Result[$j]['CONDOROOM_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    if (empty($Result[$j]['CONDOROOM_RNO'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONDOROOM_RNO']);
                    }
                    if (empty($Result[$j]['CONDO_REG_YEAR'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONDO_REG_YEAR']);
                    }
                    if (empty($Result[$j]['CONDO_NAME_TH'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONDO_NAME_TH']);
                    }
                }
                $sheet->setCellValue('G' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('G' . (6 + $current), 'พัฒน์ฯ 2');
                $ownerDesc = '';
              

                for ($i = 0; $i < count($ResultProcess); $i++) {
                    if ($ResultProcess[$i]['KEY'] == '1') {
                        array_push($processP1, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    } else if ($ResultProcess[$i]['KEY'] == '2') {
                        array_push($processP2, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('H' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    }
                }

                $current += count($ResultProcess);
            }
        }
        $current = 0;
        if ($sts == 'e') {
            for ($i = 0; $i < $last; $i++) {
                $problemDesc = '';
                if (!$parcelSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$parcelSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($processP1[$i]['MANAGE_QUEUE_DTM'] != $processP2[$i]['MANAGE_QUEUE_DTM']) $problemDesc .= "วันที่จดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['MANAGE_QUEUE_NO'] != $processP2[$i]['MANAGE_QUEUE_NO']) $problemDesc .= "เลขคิวไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_ORDER'] != $processP2[$i]['PROCESS_ORDER']) $problemDesc .= "ลำดับการจดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_REGIST_NAME'] != $processP2[$i]['PROCESS_REGIST_NAME']) $problemDesc .= "ชื่อรายการจดทะเบียนไม่ตรงกัน\n";
                }
                $sheet->setCellValue('J' . (5 + $current), rtrim($problemDesc));
                $current += 2;
            }
        }

        //================== SEQUESTER ===============================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        for ($j = 0; $j < count($sequester_seq); $j++) {

            $ResultSequester = array();


            $parcelSeqP1 = $sequester_seq[$j];
            $parcelSeqP2 = $sequester_seq[$j];
            include 'sequesterEachParcelExcel.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultSequester[] = $row;
            }
            $tempIndex = 0;






            $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
			$sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));
			$sheet->mergeCells('F' . (5 + $current) . ':F' . (6 + $current));


            $sheet->setCellValue('A' . (5 + $current), ($j + 1));
            $sheet->setCellValue('B' . (5 + $current), $Result[$sequester_index[$j]]['AMPHUR_NAME']);
            $sheet->setCellValue('C' . (5 + $current), $Result[$sequester_index[$j]]['TAMBOL_NAME']);
			$sheet->setCellValue('D' . (5 + $current), $Result[$sequester_index[$j]]['CONDOROOM_RNO']);
			$sheet->setCellValue('E' . (5 + $current), $Result[$sequester_index[$j]]['CONDO_REG_YEAR']);
			$sheet->setCellValue('F' . (5 + $current), $Result[$sequester_index[$j]]['CONDO_NAME_TH']);
			



            $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultSequester) * 2) + 1));
			$sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultSequester) * 2) + 1));
			$sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('V' . (5 + $current) . ':V' . (5 + $current + (count($ResultSequester) * 2) + 1));


            // $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultSequester) / 2) - 1));
            // $sheet->mergeCells('E' . (5 + $current + (count($ResultSequester) / 2)) . ':E' . (5 + $current + (count($ResultSequester)*2) - 1));


            $sheet->setCellValue('G' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('G' . (5 + $current + count($ResultSequester)), 'พัฒน์ฯ 2');
            $ownerDesc = '';
            $current += 2;

            for ($i = 0; $i < count($ResultSequester); $i++) {
                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO'])) {
                    $sheet->setCellValue('H' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO'])) {
                    $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM'])) {
                    $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT'])) {
                    $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME'])) {
                    $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + $i), $ResultSequester[$i]['TITLE_NAME_OWN'] . $ResultSequester[$i]['OWNER_FNAME'] . ' ' . $ResultSequester[$i]['OWNER_LNAME']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'])) {
                    $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM'])) {
                    $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM'])) {
                    $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'])) {
                    $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'])) {
                    $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM'])) {
                    $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('S' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'])) {
                    $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('T' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION']);
                }
                if (empty($ResultSequester[$i]['OWNER_NAME'])) {
                    $sheet->setCellValue('U' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('U' . (5 + $current + $i), $ResultSequester[$i]['OWNER_NAME']);
                }

                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO_1'])) {
                    $sheet->setCellValue('H' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('H' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO_1'])) {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM_1'])) {
                    $sheet->setCellValue('J' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT_1'])) {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME_1'])) {
                    $sheet->setCellValue('M' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['TITLE_NAME_OWN_1'] . $ResultSequester[$i]['OWNER_FNAME_1'] . ' ' . $ResultSequester[$i]['OWNER_LNAME_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1'])) {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM_1'])) {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM_1'])) {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1'])) {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1'])) {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM_1'])) {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1'])) {
                    $sheet->setCellValue('T' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('T' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']);
                }
                if (empty($ResultSequester[$i]['OWNER_NAME_1'])) {
                    $sheet->setCellValue('U' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('U' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['OWNER_NAME_1']);
                }

                if ($sts == 'e') {
                    $problemDesc = '';
                    if (!$dataSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$dataSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                    } else {
                        if ($ResultSequester[$i]['SEQUESTER_ORDER_NO'] != $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_NO'] != $ResultSequester[$i]['SEQUESTER_DOC_NO_1']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_DTM'] != $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DEPT'] != $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_PID'] != $ResultSequester[$i]['SEQUESTER_REQ_PID_1']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_FNAME'] != $ResultSequester[$i]['OWNER_FNAME_1']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DTM'] != $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REC_DTM'] != $ResultSequester[$i]['SEQUESTER_REC_DTM_1']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REM'] != $ResultSequester[$i]['SEQUESTER_REM_1']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'] != $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_NAME'] != $ResultSequester[$i]['OWNER_NAME_1']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                    }

                    $sheet->setCellValue('V' . (5 + $current + $i), rtrim($problemDesc));
                }
            }

            $current += count($ResultSequester) * 2;
        }

        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $sheet->mergeCells('A1:R1');
        $sheet->mergeCells('A2:R2');
        $sheet->mergeCells('A3:R3');


        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'เลขที่ห้อง')
            ->setCellValue('E4', 'เลขที่อาคารชุด')
            ->setCellValue('F4', 'ชื่ออาคารชุด')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2');
        $sheet->setCellValue('H4', 'เลขที่ห้องชุด');
        $sheet->setCellValue('I4', 'ชื่ออาคาร')
            ->setCellValue('J4', 'เลขที่อาคารชุด')
            ->setCellValue('K4', 'ชื่ออาคารชุด')
            ->setCellValue('L4', 'ตำบล/แขวง')
            ->setCellValue('M4', 'อำเภอ/เขต')
            ->setCellValue('N4', 'ขนาดพื้นที่')
            ->setCellValue('O4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('P4', 'จำนวนผู้ถือกรรมสิทธิ์')
            ->setCellValue('Q4', 'สถานะการอายัด')
            ->setCellValue('R4', 'หมายเหตุ')
            ->getStyle('A1:R4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('R4:R' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);


        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->mergeCells('A1:P1');
        $sheet->mergeCells('A2:P2');
        $sheet->mergeCells('A3:P3');
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
        }
    $sheet->getStyle('h4:H' . $last)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'เลขที่ห้อง')
            ->setCellValue('E4', 'เลขที่อาคารชุด')
            ->setCellValue('F4', 'ชื่ออาคารชุด')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2')
            ->setCellValue('H4', 'ชื่อหนังสือสัญญา')
            ->setCellValue('I4', 'วันที่หนังสือสัญญา')
            ->setCellValue('J4', 'วันที่เริ่มสัญญา')
            ->setCellValue('K4', 'ราคาทุนทรัพย์')
            ->setCellValue('L4', 'ระยะเวลา')
            ->setCellValue('M4', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('N4', 'ผู้ให้สัญญา')
            ->setCellValue('O4', 'ผู้รับสัญญา')
            ->setCellValue('P4', 'หมายเหตุ')
            ->getStyle('A1:P4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('P4:P' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        $sheet->mergeCells('A1:W1');
        $sheet->mergeCells('A2:W2');
        $sheet->mergeCells('A3:W3');

        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $errorOwner . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'เลขที่ห้อง')
            ->setCellValue('E4', 'เลขที่อาคารชุด')
            ->setCellValue('F4', 'ชื่ออาคารชุด')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->getStyle('I4:I' . $last)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);

        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2')
            ->setCellValue('H4', 'ลำดับผู้ถือกรรมสิทธิ์')
            ->setCellValue('I4', 'เลขบัตรประจำตัวประชาชน')
            ->setCellValue('J4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('K4', 'เพศผู้ถือกรรมสิทธิ์')
            ->setCellValue('L4', 'วันเกิดผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'สถานะสมรสผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'สัญชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('O4', 'เชื้อชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('P4', 'ศาสนาผู้ถือกรรมสิทธิ์')
            ->setCellValue('Q4', 'ชื่อบิดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('R4', 'ชื่อมารดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('S4', 'ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์')
            ->setCellValue('T4', 'วันที่ได้มา')
            ->setCellValue('U4', 'การได้มา')
            ->setCellValue('V4', 'สัดส่วนการได้มา')
            ->setCellValue('W4', 'หมายเหตุ')
            ->getStyle('A1:W4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('W4:W' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $sheet->mergeCells('A1:L1');
        $sheet->mergeCells('A2:L2');
        $sheet->mergeCells('A3:L3');

        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ -ห้องชุด';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'เลขที่ห้อง')
            ->setCellValue('E4', 'เลขที่อาคารชุด')
            ->setCellValue('F4', 'ชื่ออาคารชุด')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2')
            ->setCellValue('H4', 'วันที่จดทะเบียน')
            ->setCellValue('I4', 'เลขคิว')
            ->setCellValue('J4', 'ลำดับการจดทะเบียน')
            ->setCellValue('K4', 'ชื่อรายการจดทะเบียน')
            ->setCellValue('L4', 'หมายเหตุ')
            ->getStyle('A1:L4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L4:L' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        $sheet->mergeCells('A1:V1');
        $sheet->mergeCells('A2:V2');
        $sheet->mergeCells('A3:V3');
        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - ห้องชุด';
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'เลขที่ห้อง')
            ->setCellValue('E4', 'เลขที่อาคารชุด')
            ->setCellValue('F4', 'ชื่ออาคารชุด')
            ->getStyle('A1:F4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('G4', 'พัฒน์ฯ 1/2')
            ->setCellValue('H4', 'อายัดลำดับที่')
            ->setCellValue('I4', 'เลขที่คำสั่ง/หนังสือ')
            ->setCellValue('J4', 'วันที่คำสั่ง/หนังสือ')
            ->setCellValue('K4', 'หน่วยงานที่ขออายัด')
            ->setCellValue('L4', 'เลขที่บัตรประชาชนของผู้ขออายัด')
            ->setCellValue('M4', 'ชื่อ-สกุลผู้ขออายัด')
            ->setCellValue('N4', 'สถานะกำหนดอายัด')
            ->setCellValue('O4', 'วันที่ขออายัด')
            ->setCellValue('P4', 'วันที่รับอายัด')
            ->setCellValue('Q4', 'จำนวนวันที่รับอายัด')
            ->setCellValue('R4', 'วันที่สิ้นสุดอายัด')
            ->setCellValue('S4', 'เหตุผลที่ขออายัด')
            ->setCellValue('T4', 'เงื่อนไขการอายัด')
            ->setCellValue('U4', 'ชื่อผู้ถูกอายัด')
            ->setCellValue('V4', 'หมายเหตุ')
            ->getStyle('A1:V4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('V4:V' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
	    $sheet->getStyle('L4:L' . $last)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        for ($j = 0; $j < 5; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);
            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
                'font' => array(
                    'bold'  => false,
                    'color' => array('rgb' => '000000'),
                    'size'  => 16,
                    'name'  => 'TH SarabunPSK'
                )
            ];
            if ($j == 0) {
                $sheet->getStyle('A1:R' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:R4')->getFont()->setBold(true);

                $sheet->getStyle('A4:R' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 1) {
                $sheet->getStyle('A1:P' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:P4')->getFont()->setBold(true);

                $sheet->getStyle('A4:P' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 2) {
                $sheet->getStyle('A1:W' . $ownerCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:W4')->getFont()->setBold(true);

                $sheet->getStyle('A4:W' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 3) {
                $sheet->getStyle('A1:L' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:L4')->getFont()->setBold(true);
            } else {
                $sheet->getStyle('A1:V' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:V4')->getFont()->setBold(true);

                $sheet->getStyle('A4:V' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //      $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        // if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
        //      $fileName = urlencode($fileName);
        // }
        // $fileName = urlencode($fileName);
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save("C:\Users\MGT1\Desktop\htdoc\\" . $landoffice . '-' . $fileName . ".xlsx");


        break;





    case 'c':
        $last = count($Result);
        $current = 0;
        $problemDesc = "";
        $ownerCellIndex = 0;
        $transactionCellIndex = 0;
        $sequesterCellIndex = 0;

        $sequester_seq = array();
        $sequester_index = array();
        $errorParcel = 0;
        $errorOlgt = 0;
        $errorOwner = 0;
        $errortransaction = 0;
        $errorSequester = 0;


        $current = 0;
        //===================PARCEL DETAIL=====================================
        $sheet = $spreadsheet->setActiveSheetIndex(0);
        for ($j = 0; $j < $last; $j++) {

            $ResultDetail = array();




            $dataSeqP1 = $Result[$j]['CONSTRUCT_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['CONSTRUCT_SEQ_P2'];

            include 'queryDetailData.php';
            $ResultDetail[] = oci_fetch_array($stid, OCI_ASSOC);




            if ($Result[$j]['CONSTRUCT_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
                if (empty($Result[$j]['CONSTRUCT_ADDR_HID'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HID']);
                }
                if (empty($Result[$j]['CONSTRUCT_ADDR_HNO'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HNO']);
                }
                if (empty($Result[$j]['CONSTRUCT_ADDR_MOO'])) {
                    $sheet->setCellValue('F' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_MOO']);
                }
                if (empty($Result[$j]['CONSTRUCTION_NAME'])) {
                    $sheet->setCellValue('G' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current), $Result[$j]['CONSTRUCTION_NAME']);
                }
            }

            $sheet->setCellValue('H' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('H' . (6 + $current), 'พัฒน์ฯ 2');

            if (empty($ResultDetail[0]['CONSTRUCT_ADDR_HID_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultDetail[0]['CONSTRUCT_ADDR_HID_P1']);
            }
            if (empty($ResultDetail[0]['ADDR_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultDetail[0]['ADDR_P1']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_AREA_NUM_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultDetail[0]['CONSTRUCT_AREA_NUM_P1']);
            }
            if (empty($ResultDetail[0]['OWN_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultDetail[0]['OWN_P1']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1']);
            }
            if (empty($ResultDetail[0]['PARCEL_NO_P1'])) {
                $sheet->setCellValue('N' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (5 + $current), $ResultDetail[0]['PARCEL_NO_P1']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P1'])) {
                $sheet->setCellValue('O' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (5 + $current), $ResultDetail[0]['TAMBOL_NAME_P1']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P1'])) {
                $sheet->setCellValue('P' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (5 + $current), $ResultDetail[0]['AMPHUR_NAME_P1']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_PROCESS_DTM_P1'])) {
                $sheet->setCellValue('Q' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (5 + $current), $ResultDetail[0]['CONSTRUCT_PROCESS_DTM_P1']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_REGIST_NAME_P1'])) {
                $sheet->setCellValue('R' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (5 + $current), $ResultDetail[0]['CONSTRUCT_REGIST_NAME_P1']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_SEQUEST_STS_P1'])) {
                $sheet->setCellValue('S' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (5 + $current), $ResultDetail[0]['CONSTRUCT_SEQUEST_STS_P1'] == 1 ? 'ไม่มี' : 'ติดอายัด');

                if ($ResultDetail[0]['CONSTRUCT_SEQUEST_STS_P1'] != 1) {
                    array_push($sequester_seq, $dataSeqP1);
                    array_push($sequester_index, $j);
                }
            }


            if (empty($ResultDetail[0]['CONSTRUCT_ADDR_HID_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultDetail[0]['CONSTRUCT_ADDR_HID_P2']);
            }
            if (empty($ResultDetail[0]['ADDR_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultDetail[0]['ADDR_P2']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_AREA_NUM_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultDetail[0]['CONSTRUCT_AREA_NUM_P2']);
            }
            if (empty($ResultDetail[0]['OWN_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultDetail[0]['OWN_P2']);
            }
            if (empty($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']);
            }
            if (empty($ResultDetail[0]['PARCEL_NO_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultDetail[0]['PARCEL_NO_P2']);
            }
            if (empty($ResultDetail[0]['TAMBOL_NAME_P2'])) {
                $sheet->setCellValue('O' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('O' . (6 + $current), $ResultDetail[0]['TAMBOL_NAME_P2']);
            }
            if (empty($ResultDetail[0]['AMPHUR_NAME_P2'])) {
                $sheet->setCellValue('P' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('P' . (6 + $current), $ResultDetail[0]['AMPHUR_NAME_P2']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_PROCESS_DTM_P2'])) {
                $sheet->setCellValue('Q' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('Q' . (6 + $current), $ResultDetail[0]['CONSTRUCT_PROCESS_DTM_P2']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_REGIST_NAME_P2'])) {
                $sheet->setCellValue('R' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('R' . (6 + $current), $ResultDetail[0]['CONSTRUCT_REGIST_NAME_P2']);
            }
            if (empty($ResultDetail[0]['CONSTRUCT_SEQUEST_STS_P2'])) {
                $sheet->setCellValue('S' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('S' . (6 + $current), $ResultDetail[0]['CONSTRUCT_SEQUEST_STS_P2'] == 1 ? 'ไม่มี' : 'ติดอายัด');
            }

            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 2";
                } else {
                    if ($ResultDetail[0]['CONSTRUCT_ADDR_HID_P1'] != $ResultDetail[0]['CONSTRUCT_ADDR_HID_P2']) $problemDesc .= "รหัสประจำบ้านไม่ตรงกัน\n";
                    if ($ResultDetail[0]['ADDR_P1'] != $ResultDetail[0]['ADDR_P2']) $problemDesc .= "ที่ตั้งนิติบุคคลอาคารชุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONSTRUCT_AREA_NUM_P1'] != $ResultDetail[0]['CONSTRUCT_AREA_NUM_P2']) $problemDesc .= "เนื้อที่ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['OWN_P1'] != $ResultDetail[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PRINTPLATE_TYPE_NAME_P1'] != $ResultDetail[0]['PRINTPLATE_TYPE_NAME_P2']) $problemDesc .= "ชื่อประเภทเอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['PARCEL_NO_P1'] != $ResultDetail[0]['PARCEL_NO_P2']) $problemDesc .= "เลขที่เอกสารสิทธิไม่ตรงกัน\n";
                    if ($ResultDetail[0]['TAMBOL_NAME_P1'] != $ResultDetail[0]['TAMBOL_NAME_P2']) $problemDesc .= "ตำบลไม่ตรงกัน\n";
                    if ($ResultDetail[0]['AMPHUR_NAME_P1'] != $ResultDetail[0]['AMPHUR_NAME_P2']) $problemDesc .= "อำเภอไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONSTRUCT_PROCESS_DTM_P1'] != $ResultDetail[0]['CONSTRUCT_PROCESS_DTM_P2']) $problemDesc .= "วันที่จดทะเบียนล่าสุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONSTRUCT_REGIST_NAME_P1'] != $ResultDetail[0]['CONSTRUCT_REGIST_NAME_P2']) $problemDesc .= "ประเภทจดทะเบียนล่าสุดไม่ตรงกัน\n";
                    if ($ResultDetail[0]['CONSTRUCT_SEQUEST_STS_P1'] != $ResultDetail[0]['CONSTRUCT_SEQUEST_STS_P2']) $problemDesc .= "สถานะการอายัดไม่ตรงกัน\n";
                }
                $sheet->setCellValue('T' . (5 + $current), rtrim($problemDesc));
            }




            $current += 2;
        }
        $current = 0;
        //=================================== OLGT =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(1);
        for ($j = 0; $j < $last; $j++) {

            $ResultOlgt = array();

            $dataSeqP1 = $Result[$j]['CONSTRUCT_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['CONSTRUCT_SEQ_P2'];


            include 'queryOlgtDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOlgt[] = $row;
            }



            if ($Result[$j]['CONSTRUCT_SEQ_P2']) {
                $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                if (empty($Result[$j]['AMPHUR_NAME'])) {
                    $sheet->setCellValue('B' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                }
                if (empty($Result[$j]['TAMBOL_NAME'])) {
                    $sheet->setCellValue('C' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                }
                if (empty($Result[$j]['CONSTRUCT_ADDR_HID'])) {
                    $sheet->setCellValue('D' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HID']);
                }
                if (empty($Result[$j]['CONSTRUCT_ADDR_HNO'])) {
                    $sheet->setCellValue('E' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HNO']);
                }
                if (empty($Result[$j]['CONSTRUCT_ADDR_MOO'])) {
                    $sheet->setCellValue('F' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_MOO']);
                }
                if (empty($Result[$j]['CONSTRUCTION_NAME'])) {
                    $sheet->setCellValue('G' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('G' . (5 + $current), $Result[$j]['CONSTRUCTION_NAME']);
                }
            }


            $sheet->setCellValue('H' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('H' . (6 + $current), 'พัฒน์ฯ 2');
            //===================================OLGT=====================================================
            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'])) {
                $sheet->setCellValue('I' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'])) {
                $sheet->setCellValue('J' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'])) {
                $sheet->setCellValue('K' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (5 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P1'])) {
                $sheet->setCellValue('L' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (5 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P1']);
            }
            if (empty($ResultOlgt[0]['DURATION_P1'])) {
                $sheet->setCellValue('M' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (5 + $current), $ResultOlgt[0]['DURATION_P1']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P1'])) {
                $sheet->setCellValue('N' . (5 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (5 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P1']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P1'])) {
                    $sheet->setCellValue('O' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current), $ResultOlgt[0]['OWN_P1']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P1'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P1'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P1'])) {
                    $sheet->setCellValue('P' . (5 + $current), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current), $ResultOlgt[1]['OWN_P1']);
                }
            }

            if (empty($ResultOlgt[0]['OLGT_HD_CON_NAME_P2'])) {
                $sheet->setCellValue('I' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('I' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_DATE_P2'])) {
                $sheet->setCellValue('J' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('J' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_CON_STDTM_P2'])) {
                $sheet->setCellValue('K' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('K' . (6 + $current), $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_HD_MNY_P2'])) {
                $sheet->setCellValue('L' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('L' . (6 + $current), $ResultOlgt[0]['OLGT_HD_MNY_P2']);
            }
            if (empty($ResultOlgt[0]['DURATION_P2'])) {
                $sheet->setCellValue('M' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('M' . (6 + $current), $ResultOlgt[0]['DURATION_P2']);
            }
            if (empty($ResultOlgt[0]['OLGT_PMS_PID_P2'])) {
                $sheet->setCellValue('N' . (6 + $current), NULL);
            } else {
                $sheet->setCellValue('N' . (6 + $current), $ResultOlgt[0]['OLGT_PMS_PID_P2']);
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) { //ผู้ให้สัญญา
                if (empty($ResultOlgt[0]['OWN_P2'])) {
                    $sheet->setCellValue('O' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('O' . (6 + $current), $ResultOlgt[0]['OWN_P2']);
                }
            }
            if ($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) { //ผู้รับสัญญา
                if (empty($ResultOlgt[1]['OWN_P2'])) {
                    $sheet->setCellValue('P' . (6 + $current), NULL);
                } else {
                    $sheet->setCellValue('P' . (6 + $current), $ResultOlgt[1]['OWN_P2']);
                }
            }

            if ($sts == 'e') {
                $problemDesc = '';
                if (!$dataSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$dataSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($ResultOlgt[0]['OLGT_HD_CON_NAME_P1'] != $ResultOlgt[0]['OLGT_HD_CON_NAME_P2']) $problemDesc .= "ชื่อหนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_DATE_P1'] != $ResultOlgt[0]['OLGT_HD_CON_DATE_P2']) $problemDesc .= "วันที่หนังสือสัญญาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_HD_CON_STDTM_P1'] != $ResultOlgt[0]['OLGT_HD_CON_STDTM_P2']) $problemDesc .= "วันที่เริ่มสัญญา\n";
                    if ($ResultOlgt[0]['OLGT_HD_MNY_P1'] != $ResultOlgt[0]['OLGT_HD_MNY_P2']) $problemDesc .= "ราคาทุนทรัพย์ไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['DURATION_P1'] != $ResultOlgt[0]['DURATION_P2']) $problemDesc .= "ระยะเวลาไม่ตรงกัน\n";
                    if ($ResultOlgt[0]['OLGT_PMS_PID_P1'] != $ResultOlgt[0]['OLGT_PMS_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 1 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 4) && $ResultOlgt[0]['OWN_P1'] != $ResultOlgt[0]['OWN_P2']) $problemDesc .= "ชื่อผู้ให้สัญญาไม่ตรงกัน\n";
                    if (($ResultOlgt[0]['PARTY_TYPE_P2'] == 2 || $ResultOlgt[0]['PARTY_TYPE_P2'] == 3) && $ResultOlgt[1]['OWN_P1'] != $ResultOlgt[1]['OWN_P2']) $problemDesc .= "ชื่อผู้รับสัญญาไม่ตรงกัน\n";
                }
                $sheet->setCellValue('Q' . (5 + $current), rtrim($problemDesc));
            }

            $current += 2;
        }

        $current = 0;
        //=================================== OWNER =========================================
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        for ($j = 0; $j < $last; $j++) {
            $ResultOwner = array();

            $dataSeqP1 = $Result[$j]['CONSTRUCT_SEQ_P1'];
            $dataSeqP2 = $Result[$j]['CONSTRUCT_SEQ_P2'];


            include 'queryOwnDetail.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultOwner[] = $row;
            }



            if (count($ResultOwner) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));

                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (6 + $current));

                if ($Result[$j]['CONSTRUCT_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_HID'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HID']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_HNO'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HNO']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_MOO'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_MOO']);
                    }
                    if (empty($Result[$j]['CONSTRUCTION_NAME'])) {
                        $sheet->setCellValue('G' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current), $Result[$j]['CONSTRUCTION_NAME']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('W' . (5 + $current) . ':W' . (5 + $current + (count($ResultOwner) * 2) - 1));
                $sheet->mergeCells('E' . (5 + $current + (count($ResultOwner))) . ':E' . (5 + $current + (count($ResultOwner) * 2) - 1));

                if ($Result[$j]['CONSTRUCT_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_HID'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HID']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_HNO'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HNO']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_MOO'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_MOO']);
                    }
                    if (empty($Result[$j]['CONSTRUCTION_NAME'])) {
                        $sheet->setCellValue('G' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current), $Result[$j]['CONSTRUCTION_NAME']);
                    }
                    $ownerCellIndex = (5 + $current);
                }


                $sheet->setCellValue('H' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('H' . (5 + $current + count($ResultOwner)), 'พัฒน์ฯ 2');
                $ownerDesc = '';

                for ($i = 0; $i < count($ResultOwner); $i++) {
                    if (empty($ResultOwner[$i]['OWNER_ORDER_P1'])) {
                        $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + $i), $ResultOwner[$i]['OWNER_ORDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P1'])) {
                        $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PID_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P1'])) {
                        $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + $i), $ResultOwner[$i]['OWN_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P1'])) {
                        $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + $i), $ResultOwner[$i]['OWNER_GENDER_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P1'])) {
                        $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + $i), $ResultOwner[$i]['OWNER_BDATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P1'])) {
                        $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + $i), $ResultOwner[$i]['OWNER_PN_STS_P1']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P1'])) {
                        $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + $i), $ResultOwner[$i]['NATIONALITY_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P1'])) {
                        $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + $i), $ResultOwner[$i]['RACE_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P1'])) {
                        $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + $i), $ResultOwner[$i]['RELIGION_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P1'])) {
                        $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + $i), $ResultOwner[$i]['FAT_P1']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P1'])) {
                        $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + $i), $ResultOwner[$i]['MOT_P1']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P1'])) {
                        $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + $i), $ResultOwner[$i]['REG_ADDR_P1']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P1'])) {
                        $sheet->setCellValue('U' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + $i), $ResultOwner[$i]['RECV_DATE_P1']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P1'])) {
                        $sheet->setCellValue('V' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('V' . (5 + $current + $i), $ResultOwner[$i]['OBTAIN_NAME_P1']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P1'])) {
                        $sheet->setCellValue('W' . (5 + $current + $i), NULL);
                    } else {
                        $sheet->setCellValue('W' . (5 + $current + $i), $ResultOwner[$i]['NUM_P1']);
                    }



                    if (empty($ResultOwner[$i]['OWNER_ORDER_P2'])) {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner)  + $i), NULL);
                    } else {
                        $sheet->setCellValue('I' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_ORDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PID_P2'])) {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('J' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PID_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWN_P2'])) {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('K' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWN_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_GENDER_P2'])) {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('L' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_GENDER_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_BDATE_P2'])) {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('M' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_BDATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OWNER_PN_STS_P2'])) {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('N' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OWNER_PN_STS_P2']);
                    }
                    if (empty($ResultOwner[$i]['NATIONALITY_NAME_P2'])) {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('O' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NATIONALITY_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RACE_NAME_P2'])) {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('P' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RACE_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['RELIGION_NAME_P2'])) {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('Q' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RELIGION_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['FAT_P2'])) {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('R' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['FAT_P2']);
                    }
                    if (empty($ResultOwner[$i]['MOT_P2'])) {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('S' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['MOT_P2']);
                    }
                    if (empty($ResultOwner[$i]['REG_ADDR_P2'])) {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('T' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['REG_ADDR_P2']);
                    }
                    if (empty($ResultOwner[$i]['RECV_DATE_P2'])) {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('U' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['RECV_DATE_P2']);
                    }
                    if (empty($ResultOwner[$i]['OBTAIN_NAME_P2'])) {
                        $sheet->setCellValue('V' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('V' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['OBTAIN_NAME_P2']);
                    }
                    if (empty($ResultOwner[$i]['NUM_P2'])) {
                        $sheet->setCellValue('W' . (5 + $current + count($ResultOwner) + $i), NULL);
                    } else {
                        $sheet->setCellValue('W' . (5 + $current + count($ResultOwner) + $i), $ResultOwner[$i]['NUM_P2']);
                    }

                    $ownerCellIndex = (5 + $current + count($ResultOwner) + $i);

                    $tempIndex = 0;

                    if ($sts == 'e') {

                        $problemDesc = '';
                        if (!$dataSeqP1) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                        } else if (!$dataSeqP2) {
                            $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                        } else {
                            if ($ResultOwner[$i]['OWNER_ORDER_P1'] != $ResultOwner[$i]['OWNER_ORDER_P2']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PID_P1'] != $ResultOwner[$i]['OWNER_PID_P2']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWN_P1'] != $ResultOwner[$i]['OWN_P2']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_GENDER_P1'] != $ResultOwner[$i]['OWNER_GENDER_P2']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_BDATE_P1'] != $ResultOwner[$i]['OWNER_BDATE_P2']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OWNER_PN_STS_P1'] != $ResultOwner[$i]['OWNER_PN_STS_P2']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NATIONALITY_NAME_P1'] != $ResultOwner[$i]['NATIONALITY_NAME_P2']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RACE_NAME_P1'] != $ResultOwner[$i]['RACE_NAME_P2']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RELIGION_NAME_P1'] != $ResultOwner[$i]['RELIGION_NAME_P2']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['FAT_P1'] != $ResultOwner[$i]['FAT_P2']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['MOT_P1'] != $ResultOwner[$i]['MOT_P2']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['REG_ADDR_P1'] != $ResultOwner[$i]['REG_ADDR_P2']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['RECV_DATE_P1'] != $ResultOwner[$i]['RECV_DATE_P2']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['OBTAIN_NAME_P1'] != $ResultOwner[$i]['OBTAIN_NAME_P2']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                            if ($ResultOwner[$i]['NUM_P1'] != $ResultOwner[$i]['NUM_P2']) $problemDesc .= "สัดส่วนการได้มาไม่ตรงกัน\n";
                        }

                        $sheet->setCellValue('W' . (5 + $current + $tempIndex), rtrim($problemDesc));
                    }
                }

                $current += count($ResultOwner) * 2;
            }
        }

        //=========================== PROCESS TRANSACTION ==========================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $processP1 = array();
        $processP2 = array();
        for ($j = 0; $j < $last; $j++) {
            $ResultProcess = array();

            $parcelSeqP1 = $Result[$j]['CONSTRUCT_SEQ_P1'];
            $parcelSeqP2 = $Result[$j]['CONSTRUCT_SEQ_P2'];


            if ($parcelSeqP1 != null) {
                $table = 'P1';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }
            if ($parcelSeqP2 != null) {
                $table = 'P2';
                include 'queryProcessData.php';
                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $ResultProcess[] = $row;
                }
            }


            if (count($ResultProcess) <= 0) {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (6 + $current));
                $sheet->mergeCells('G' . (5 + $current) . ':G' . (6 + $current));
                if ($Result[$j]['CONSTRUCT_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_HID'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HID']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_HNO'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HNO']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_MOO'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_MOO']);
                    }
                    if (empty($Result[$j]['CONSTRUCTION_NAME'])) {
                        $sheet->setCellValue('G' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current), $Result[$j]['CONSTRUCTION_NAME']);
                    }
                }
                $current += 2;
            } else {
                $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('G' . (5 + $current) . ':G' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('H' . (5 + $current) . ':H' . (5 + $current + count($ResultProcess) - 1));
             //   $sheet->mergeCells('H' . (5 + $current + (count($ResultProcess) / 2)) . ':H' . (5 + $current + count($ResultProcess) - 1));
                $sheet->mergeCells('L' . (5 + $current) . ':L' . (5 + $current + count($ResultProcess) - 1));
                if ($Result[$j]['CONSTRUCT_SEQ_P2']) {
                    $sheet->setCellValue('A' . (5 + $current), ($j + 1));

                    if (empty($Result[$j]['AMPHUR_NAME'])) {
                        $sheet->setCellValue('B' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('B' . (5 + $current), $Result[$j]['AMPHUR_NAME']);
                    }
                    if (empty($Result[$j]['TAMBOL_NAME'])) {
                        $sheet->setCellValue('C' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('C' . (5 + $current), $Result[$j]['TAMBOL_NAME']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_HID'])) {
                        $sheet->setCellValue('D' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('D' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HID']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_HNO'])) {
                        $sheet->setCellValue('E' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('E' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_HNO']);
                    }
                    if (empty($Result[$j]['CONSTRUCT_ADDR_MOO'])) {
                        $sheet->setCellValue('F' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('F' . (5 + $current), $Result[$j]['CONSTRUCT_ADDR_MOO']);
                    }
                    if (empty($Result[$j]['CONSTRUCTION_NAME'])) {
                        $sheet->setCellValue('G' . (5 + $current), NULL);
                    } else {
                        $sheet->setCellValue('G' . (5 + $current), $Result[$j]['CONSTRUCTION_NAME']);
                    }
                }
                $sheet->setCellValue('H' . (5 + $current), 'พัฒน์ฯ 1');
                $sheet->setCellValue('H' . (5 + $current + (count($ResultProcess) / 2)), 'พัฒน์ฯ 2');
                $ownerDesc = '';
          

                for ($i = 0; $i < count($ResultProcess); $i++) {
                    if ($ResultProcess[$i]['KEY'] == '1') {
                        array_push($processP1, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    } else if ($ResultProcess[$i]['KEY'] == '2') {
                        array_push($processP2, $ResultProcess[$i]);
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_DTM'])) {
                            $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('I' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_DTM']);
                        }
                        if (empty($ResultProcess[$i]['MANAGE_QUEUE_NO'])) {
                            $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('J' . (5 + $current + $i), $ResultProcess[$i]['MANAGE_QUEUE_NO']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_ORDER'])) {
                            $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('K' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_ORDER']);
                        }
                        if (empty($ResultProcess[$i]['PROCESS_REGIST_NAME'])) {
                            $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                        } else {
                            $sheet->setCellValue('L' . (5 + $current + $i), $ResultProcess[$i]['PROCESS_REGIST_NAME']);
                        }
                    }
                    $transactionCellIndex = (5 + $current + count($ResultProcess) + $i);
                }

                $current += count($ResultProcess);
            }
        }
        $current = 0;

        if ($sts == 'e') {
            for ($i = 0; $i < $last; $i++) {
                $problemDesc = '';
                if (!$parcelSeqP1) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                } else if (!$parcelSeqP2) {
                    $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                } else {
                    if ($processP1[$i]['MANAGE_QUEUE_DTM'] != $processP2[$i]['MANAGE_QUEUE_DTM']) $problemDesc .= "วันที่จดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['MANAGE_QUEUE_NO'] != $processP2[$i]['MANAGE_QUEUE_NO']) $problemDesc .= "เลขคิวไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_ORDER'] != $processP2[$i]['PROCESS_ORDER']) $problemDesc .= "ลำดับการจดทะเบียนไม่ตรงกัน\n";
                    if ($processP1[$i]['PROCESS_REGIST_NAME'] != $processP2[$i]['PROCESS_REGIST_NAME']) $problemDesc .= "ชื่อรายการจดทะเบียนไม่ตรงกัน\n";
                }
                $sheet->setCellValue('K' . (5 + $current), rtrim($problemDesc));
                $current += 2;
            }
        }


        //================== SEQUESTER ===============================
        $current = 0;
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        for ($j = 0; $j < count($sequester_seq); $j++) {

            $ResultSequester = array();


            $parcelSeqP1 = $sequester_seq[$j];
            $parcelSeqP2 = $sequester_seq[$j];
            include 'sequesterEachParcelExcel.php';
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $ResultSequester[] = $row;
            }



            $sheet->mergeCells('A' . (5 + $current) . ':A' . (6 + $current));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (6 + $current));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (6 + $current));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (6 + $current));
            $sheet->mergeCells('E' . (5 + $current) . ':E' . (6 + $current));
            $sheet->mergeCells('F' . (5 + $current) . ':F' . (6 + $current));
            $sheet->mergeCells('G' . (5 + $current) . ':G' . (6 + $current));


            $sheet->setCellValue('A' . (5 + $current), ($j + 1));
            $sheet->setCellValue('B' . (5 + $current), $Result[$sequester_index[$j]]['AMPHUR_NAME']);
            $sheet->setCellValue('C' . (5 + $current), $Result[$sequester_index[$j]]['TAMBOL_NAME']);
            $sheet->setCellValue('D' . (5 + $current), $Result[$sequester_index[$j]]['CONSTRUCT_ADDR_HID']);
            $sheet->setCellValue('E' . (5 + $current), $Result[$sequester_index[$j]]['CONSTRUCT_ADDR_HNO']);
            $sheet->setCellValue('F' . (5 + $current), $Result[$sequester_index[$j]]['CONSTRUCT_ADDR_MOO']);
            $sheet->setCellValue('G' . (5 + $current), $Result[$sequester_index[$j]]['CONSTRUCTION_NAME']);


            $sheet->mergeCells('A' . (5 + $current) . ':A' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('B' . (5 + $current) . ':B' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('C' . (5 + $current) . ':C' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('D' . (5 + $current) . ':D' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('F' . (5 + $current) . ':F' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('G' . (5 + $current) . ':G' . (5 + $current + (count($ResultSequester) * 2) + 1));
            $sheet->mergeCells('W' . (5 + $current) . ':W' . (5 + $current + (count($ResultSequester) * 2) + 1));


            // $sheet->mergeCells('E' . (5 + $current) . ':E' . (5 + $current + (count($ResultSequester) / 2) - 1));
            // $sheet->mergeCells('E' . (5 + $current + (count($ResultSequester) / 2)) . ':E' . (5 + $current + (count($ResultSequester)*2) - 1));


            $sheet->setCellValue('H' . (5 + $current), 'พัฒน์ฯ 1');
            $sheet->setCellValue('H' . (5 + $current + count($ResultSequester)), 'พัฒน์ฯ 2');
            $ownerDesc = '';
            $current += 2;

            for ($i = 0; $i < count($ResultSequester); $i++) {
                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO'])) {
                    $sheet->setCellValue('I' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO'])) {
                    $sheet->setCellValue('J' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM'])) {
                    $sheet->setCellValue('K' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT'])) {
                    $sheet->setCellValue('L' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME'])) {
                    $sheet->setCellValue('N' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + $i), $ResultSequester[$i]['TITLE_NAME_OWN'] . $ResultSequester[$i]['OWNER_FNAME'] . ' ' . $ResultSequester[$i]['OWNER_LNAME']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'])) {
                    $sheet->setCellValue('O' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM'])) {
                    $sheet->setCellValue('P' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM'])) {
                    $sheet->setCellValue('Q' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'])) {
                    $sheet->setCellValue('R' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'])) {
                    $sheet->setCellValue('S' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('S' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM'])) {
                    $sheet->setCellValue('T' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('T' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REM']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'])) {
                    $sheet->setCellValue('U' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('U' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION']);
                }
                if (empty($ResultSequester[$i]['OWNER_NAME'])) {
                    $sheet->setCellValue('V' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('V' . (5 + $current + $i), $ResultSequester[$i]['OWNER_NAME']);
                }

                if (empty($ResultSequester[$i]['SEQUESTER_ORDER_NO_1'])) {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('I' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_NO_1'])) {
                    $sheet->setCellValue('J' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('J' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_NO_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_DOC_DTM_1'])) {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('K' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DEPT_1'])) {
                    $sheet->setCellValue('L' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('L' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_PID'])) {
                    $sheet->setCellValue('M' . (5 + $current + $i), NULL);
                } else {
                    $sheet->setCellValue('M' . (5 + $current + $i), $ResultSequester[$i]['SEQUESTER_REQ_PID']);
                }
                if (empty($ResultSequester[$i]['OWNER_FNAME_1'])) {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('N' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['TITLE_NAME_OWN_1'] . $ResultSequester[$i]['OWNER_FNAME_1'] . ' ' . $ResultSequester[$i]['OWNER_LNAME_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1'])) {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('O' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_DTM_1'])) {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('P' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REC_DTM_1'])) {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('Q' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REC_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1'])) {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('R' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1'])) {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('S' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REM_1'])) {
                    $sheet->setCellValue('T' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('T' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REM_1']);
                }
                if (empty($ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1'])) {
                    $sheet->setCellValue('U' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('U' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']);
                }
                if (empty($ResultSequester[$i]['OWNER_NAME_1'])) {
                    $sheet->setCellValue('V' . (5 + $current + count($ResultSequester) + $i), NULL);
                } else {
                    $sheet->setCellValue('V' . (5 + $current + count($ResultSequester) + $i), $ResultSequester[$i]['OWNER_NAME_1']);
                }

                if ($sts == 'e') {

                    $problemDesc = '';
                    if (!$dataSeqP1) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ฯ 1";
                    } else if (!$dataSeqP2) {
                        $problemDesc .= "ไม่มีข้อมูลในโครงการพัฒน์ 2";
                    } else {
                        if ($ResultSequester[$i]['SEQUESTER_ORDER_NO'] != $ResultSequester[$i]['SEQUESTER_ORDER_NO_1']) $problemDesc .= "ลำดับผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_NO'] != $ResultSequester[$i]['SEQUESTER_DOC_NO_1']) $problemDesc .= "เลขบัตรประจำตัวประชาชนไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_DOC_DTM'] != $ResultSequester[$i]['SEQUESTER_DOC_DTM_1']) $problemDesc .= "ชื่อผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DEPT'] != $ResultSequester[$i]['SEQUESTER_REQ_DEPT_1']) $problemDesc .= "เพศผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_PID'] != $ResultSequester[$i]['SEQUESTER_REQ_PID_1']) $problemDesc .= "วันเกิดผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_FNAME'] != $ResultSequester[$i]['OWNER_FNAME_1']) $problemDesc .= "สถานะสมรสผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_STS'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_STS_1']) $problemDesc .= "สัญชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_DTM'] != $ResultSequester[$i]['SEQUESTER_REQ_DTM_1']) $problemDesc .= "เชื้อชาติผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REC_DTM'] != $ResultSequester[$i]['SEQUESTER_REC_DTM_1']) $problemDesc .= "ศาสนาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_NUM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_NUM_1']) $problemDesc .= "ชื่อบิดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_EXPIRE_DTM'] != $ResultSequester[$i]['SEQUESTER_EXPIRE_DTM_1']) $problemDesc .= "ชื่อมารดาผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REM'] != $ResultSequester[$i]['SEQUESTER_REM_1']) $problemDesc .= "ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์ไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['SEQUESTER_REQ_CONDITION'] != $ResultSequester[$i]['SEQUESTER_REQ_CONDITION_1']) $problemDesc .= "วันที่ได้มาไม่ตรงกัน\n";
                        if ($ResultSequester[$i]['OWNER_NAME'] != $ResultSequester[$i]['OWNER_NAME_1']) $problemDesc .= "การได้มาไม่ตรงกัน\n";
                    }

                    $sheet->setCellValue('W' . (5 + $current + $j), rtrim($problemDesc));
                }
            }

            $current += count($ResultSequester) * 2;
            $sequesterCellIndex = (5 + $current + count($ResultSequester) + $i);
        }






        $sheet = $spreadsheet->setActiveSheetIndex(0);
        $sheet->mergeCells('A1:T1');
        $sheet->mergeCells('A2:T2');
        $sheet->mergeCells('A3:T3');


        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน';
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'รหัสประจำบ้าน')
            ->setCellValue('E4', 'เลขที่บ้าน')
            ->setCellValue('F4', 'หมู่')
            ->setCellValue('G4', 'ชื่อสิ่งปลูกสร้าง')
            ->getStyle('A1:G4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);


        $sheet->setCellValue('H4', 'พัฒน์ฯ 1/2');
        $sheet->setCellValue('I4', 'รหัสประจำบ้าน');
        $sheet->setCellValue('J4', 'ที่ตั้งนิติบุคคลอาคารชุด');
        $sheet->setCellValue('K4', 'เนื้อที่')
            ->setCellValue('L4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'ชื่อประเภทเอกสารสิทธิ')
            ->setCellValue('N4', 'เลขที่เอกสารสิทธิ')
            ->setCellValue('O4', 'ตำบล/แขวง')
            ->setCellValue('P4', 'อำเภอ/เขต')
            ->setCellValue('Q4', 'วันที่จดทะเบียนล่าสุด')
            ->setCellValue('R4', 'ประเภทจดทะเบียนล่าสุด')
            ->setCellValue('S4', 'สถานะการอายัด')
            ->setCellValue('T4', 'หมายเหตุ')
            ->getStyle('A1:T4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('T4:T' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);


        $sheet = $spreadsheet->setActiveSheetIndex(1);
        $sheet->mergeCells('A1:Q1');
        $sheet->mergeCells('A2:Q2');
        $sheet->mergeCells('A3:Q3');
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน';
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'รหัสประจำบ้าน')
            ->setCellValue('E4', 'เลขที่บ้าน')
            ->setCellValue('F4', 'หมู่')
            ->setCellValue('G4', 'ชื่อสิ่งปลูกสร้าง')
            ->getStyle('A1:G4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('N4:N' . $last)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);

        $sheet->setCellValue('H4', 'พัฒน์ฯ 1/2')
            ->setCellValue('I4', 'ชื่อหนังสือสัญญา')
            ->setCellValue('J4', 'วันที่หนังสือสัญญา')
            ->setCellValue('K4', 'วันที่เริ่มสัญญา')
            ->setCellValue('L4', 'ราคาทุนทรัพย์')
            ->setCellValue('M4', 'ระยะเวลา')
            ->setCellValue('N4', 'เลขบัตรประจำตัวประชาชนคู่สัญญา')
            ->setCellValue('O4', 'ผู้ให้สัญญา')
            ->setCellValue('P4', 'ผู้รับสัญญา')
            ->setCellValue('Q4', 'หมายเหตุ')
            ->getStyle('A1:Q4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('Q4:Q' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(2);
        $sheet->mergeCells('A1:X1');
        $sheet->mergeCells('A2:X2');
        $sheet->mergeCells('A3:X3');



        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $errorOwner . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน';
        }


        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'รหัสประจำบ้าน')
            ->setCellValue('E4', 'เลขที่บ้าน')
            ->setCellValue('F4', 'หมู่')
            ->setCellValue('G4', 'ชื่อสิ่งปลูกสร้าง')
            ->getStyle('A1:G4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->getStyle('J4:J' . $ownerCellIndex)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $sheet->getStyle('T4:T' . $ownerCellIndex)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);

        $sheet->setCellValue('H4', 'พัฒน์ฯ 1/2')
            ->setCellValue('I4', 'ลำดับผู้ถือกรรมสิทธิ์')
            ->setCellValue('J4', 'เลขบัตรประจำตัวประชาชน')
            ->setCellValue('K4', 'ชื่อผู้ถือกรรมสิทธิ์')
            ->setCellValue('L4', 'เพศผู้ถือกรรมสิทธิ์')
            ->setCellValue('M4', 'วันเกิดผู้ถือกรรมสิทธิ์')
            ->setCellValue('N4', 'สถานะสมรสผู้ถือกรรมสิทธิ์')
            ->setCellValue('O4', 'สัญชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('P4', 'เชื้อชาติผู้ถือกรรมสิทธิ์')
            ->setCellValue('Q4', 'ศาสนาผู้ถือกรรมสิทธิ์')
            ->setCellValue('R4', 'ชื่อบิดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('S4', 'ชื่อมารดาผู้ถือกรรมสิทธิ์')
            ->setCellValue('T4', 'ที่อยู่ตามทะเบียนบ้านผู้ถือกรรมสิทธิ์')
            ->setCellValue('U4', 'วันที่ได้มา')
            ->setCellValue('V4', 'การได้มา')
            ->setCellValue('W4', 'สัดส่วนการได้มา')
            ->setCellValue('X4', 'หมายเหตุ')
            ->getStyle('A1:X4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('X4:X' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(3);
        $sheet->mergeCells('A1:M1');
        $sheet->mergeCells('A2:M2');
        $sheet->mergeCells('A3:M3');

        if ($sts == 'e') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ -อาคารโรงเรือน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน';
        }

        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'รหัสประจำบ้าน')
            ->setCellValue('E4', 'เลขที่บ้าน')
            ->setCellValue('F4', 'หมู่')
            ->setCellValue('G4', 'ชื่อสิ่งปลูกสร้าง')
            ->getStyle('A1:G4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('H4', 'พัฒน์ฯ 1/2')
            ->setCellValue('I4', 'วันที่จดทะเบียน')
            ->setCellValue('J4', 'เลขคิว')
            ->setCellValue('K4', 'ลำดับการจดทะเบียน')
            ->setCellValue('L4', 'ชื่อรายการจดทะเบียน')
            ->setCellValue('M4', 'หมายเหตุ')
            ->getStyle('A1:M4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('M4:M' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        $sheet = $spreadsheet->setActiveSheetIndex(4);
        $sheet->mergeCells('A1:W1');
        $sheet->mergeCells('A2:W2');
        $sheet->mergeCells('A3:W3');
		
		  $sheet->getStyle('M4:M' . $last)
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        if ($sts == 'e') {
             $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนสำเร็จ - อาคารโรงเรือน';
        } else if ($sts == 's') {
            $sheet->setCellValue('A1', 'จำนวนรายการ: ' . $last . ' รายการ');
            $sheet->setCellValue('A2', 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน');
            $sheet->setCellValue('A3', $branchName);
            $bn = 'ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน';
            $fileName =  $branchName . '-ข้อมูลทะเบียนที่ดินที่ถ่ายโอนไม่สำเร็จ - อาคารโรงเรือน';
        }
        $sheet->setCellValue('A4', '')
            ->setCellValue('B4', 'อำเภอ')
            ->setCellValue('C4', 'ตำบล')
            ->setCellValue('D4', 'รหัสประจำบ้าน')
            ->setCellValue('E4', 'เลขที่บ้าน')
            ->setCellValue('F4', 'หมู่')
            ->setCellValue('G4', 'ชื่อสิ่งปลูกสร้าง')
            ->getStyle('A1:G4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $sheet->setCellValue('H4', 'พัฒน์ฯ 1/2')
            ->setCellValue('I4', 'อายัดลำดับที่')
            ->setCellValue('J4', 'เลขที่คำสั่ง/หนังสือ')
            ->setCellValue('K4', 'วันที่คำสั่ง/หนังสือ')
            ->setCellValue('L4', 'หน่วยงานที่ขออายัด')
            ->setCellValue('M4', 'เลขที่บัตรประชาชนของผู้ขออายัด')
            ->setCellValue('N4', 'ชื่อ-สกุลผู้ขออายัด')
            ->setCellValue('O4', 'สถานะกำหนดอายัด')
            ->setCellValue('P4', 'วันที่ขออายัด')
            ->setCellValue('Q4', 'วันที่รับอายัด')
            ->setCellValue('R4', 'จำนวนวันที่รับอายัด')
            ->setCellValue('S4', 'วันที่สิ้นสุดอายัด')
            ->setCellValue('T4', 'เหตุผลที่ขออายัด')
            ->setCellValue('U4', 'เงื่อนไขการอายัด')
            ->setCellValue('V4', 'ชื่อผู้ถูกอายัด')
            ->setCellValue('W4', 'หมายเหตุ')
            ->getStyle('A1:W4')
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('W4:W' . $last * 2)
            ->getAlignment()
            ->setWrapText(true);
        for ($j = 0; $j < 5; $j++) {
            $sheet = $spreadsheet->setActiveSheetIndex($j);
            $styleArray = [
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => ['argb' => '00000000'],
                    ],
                ],
                'font' => array(
                    'bold'  => false,
                    'color' => array('rgb' => '000000'),
                    'size'  => 16,
                    'name'  => 'TH SarabunPSK'
                )
            ];
            if ($j == 0) {
                $sheet->getStyle('A1:T' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:T4')->getFont()->setBold(true);

                $sheet->getStyle('A4:T' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 1) {
                $sheet->getStyle('A1:Q' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:Q4')->getFont()->setBold(true);

                $sheet->getStyle('A4:Q' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 2) {
                $sheet->getStyle('A1:X' . $ownerCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:X4')->getFont()->setBold(true);

                $sheet->getStyle('A4:X' . $ownerCellIndex)
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            } else if ($j == 3) {
                $sheet->getStyle('A1:M' . $transactionCellIndex)->applyFromArray($styleArray);
                $sheet->getStyle('A1:M4')->getFont()->setBold(true);
            } else {
                $sheet->getStyle('A1:W' . ($last * 2 + 4))->applyFromArray($styleArray);
                $sheet->getStyle('A1:W4')->getFont()->setBold(true);

                $sheet->getStyle('A4:W' . ($last * 2 + 4))
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
        }


        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //  $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        // if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false)) {
        //      $fileName = urlencode($fileName);
        // }
        // $fileName = urlencode($fileName);
        header('Content-Disposition: attachment; filename=' . $landoffice . '-' . $fileName . '.xlsx');
        $writer->save("C:\Users\MGT1\Desktop\htdoc\\" . $landoffice . '-' . $fileName . ".xlsx");


        break;
    default:
        # code...
        break;
}
