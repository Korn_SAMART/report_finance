<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include 'conn.php';

    $landoffice = $_POST['landoffice'];
    $landoffice = 561;

    $typ = $_POST['typ'];
    switch ($typ) {
	      case 'img-0': $table = 'datam.vw_tb_reg_parcel_image'; break;
	      case 'img-1': $table = 'datam.vw_tb_reg_parcel_land_image'; break;
        case 'reg-1': $table = 'datam.parcel_hd'; break;
	default: break;
    }

    $temp = explode('-', $typ);
    switch ($temp[0]) {
	case 'img':
		$selectAmphur = "select amphur_name, amphur_seq
                    from ".$table."
                    where landoffice_seq = :landoffice
                    GROUP BY amphur_name, amphur_seq";
		break;
    case 'reg':
        $selectAmphur = "SELECT
                            am.amphur_seq,am.amphur_name,l.landoffice_seq
                        FROM MAS.tb_mas_amphur am
                        INNER JOIN(
                            SELECT DISTINCT c.changwat,a.amphur_tcode,c.landoffice_seq,
                            LPAD(TRIM(c.changwat),2,0)||LPAD(TRIM(a.amphur_tcode),2,0) AS amphur_id
                            FROM DATAM.chang_branc c
                            INNER JOIN DATAM.area a
                                ON c.landoffice_seq=a.landoffice_seq
                        )l
                            ON am.amphur_id = l.amphur_id
                        WHERE l.landoffice_seq = :landoffice
                        ";
                        break;
    default: break;
    
    
    
}
    $stid = oci_parse($conn, $selectAmphur);
    oci_bind_by_name($stid, ':landoffice', $landoffice);
    oci_execute($stid);
    $Result = array();
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
    echo json_encode($Result, JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
