<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';
    include '../database/conn.php';

    $check = $_POST['check'];
    $parcelSeqP1 = !isset($_POST['parcelSeqP1'])? '' : $_POST['parcelSeqP1'];
    $parcelSeqP2 = !isset($_POST['parcelSeqP2'])? '' : $_POST['parcelSeqP2'];
    $Result = array();
    
    $parcel_seq_sql1 = $parcelSeqP1 == '' ? 'WHERE P.PARCEL_SEQ IS NULL' : " WHERE P.PARCEL_SEQ = :parcelSeqP1 ";
    $parcel_seq_sql2 = $parcelSeqP1 == '' ? 'WHERE P.PARCEL_SEQ IS NULL' : " WHERE P.PARCEL_SEQ = :parcelSeqP2 ";

    $parcel_land_seq_sql1 = $parcelSeqP1 == '' ? 'WHERE P.PARCEL_LAND_SEQ IS NULL' : " WHERE P.PARCEL_LAND_SEQ = :parcelSeqP1 ";
    $parcel_land_seq_sql2 = $parcelSeqP2 == '' ? 'WHERE P.PARCEL_LAND_SEQ IS NULL' : " WHERE P.PARCEL_LAND_SEQ = :parcelSeqP2 ";

    $constr_seq_sql1 = $parcelSeqP1 == '' ? 'WHERE P.CONSTRUCT_SEQ IS NULL' : " WHERE P.CONSTRUCT_SEQ = :parcelSeqP1 ";
    $constr_seq_sql2 = $parcelSeqP2 == '' ? 'WHERE P.CONSTRUCT_SEQ IS NULL' : " WHERE P.CONSTRUCT_SEQ = :parcelSeqP2 ";
 
    $condo_room_seq_sql1 = $parcelSeqP1 == '' ? 'WHERE P.CONDOROOM_SEQ IS NULL' : " WHERE P.CONDOROOM_SEQ = :parcelSeqP1 ";
    $condo_room_seq_sql2 = $parcelSeqP2 == '' ? 'WHERE P.CONDOROOM_SEQ IS NULL' : " WHERE P.CONDOROOM_SEQ = :parcelSeqP2 ";

    
    switch ($check) {
        case '1': //1
            $select = "WITH P1 AS ( SELECT P.PARCEL_SEQ, S.SEQUESTER_ORDER_NO, SEQUESTER_DOC_NO, SEQUESTER_DOC_DTM, SEQUESTER_REQ_TYPE, REQ_TITLE_SEQ, SEQUESTER_REQ_DEPT
                            , S.SEQUESTER_REQ_PID, T.TITLE_NAME, SEQUESTER_REQ_FNAME, SEQUESTER_REQ_MNAME, SEQUESTER_REQ_LNAME
                            , SEQUESTER_EXPIRE_STS, SEQUESTER_REQ_DTM, SEQUESTER_REC_DTM, SEQUESTER_EXPIRE_NUM, SEQUESTER_EXPIRE_DTM
                            , SEQUESTER_REM
                            , SEQUESTER_REQ_CONDITION, TON.TITLE_NAME AS TITLE_NAME_OWN, OWNER_FNAME, OWNER_MNAME, OWNER_LNAME
                        FROM MGT1.TB_REG_PARCEL P
                        INNER JOIN MGT1.TB_REG_SEQUESTER_MAPPING SM
                            ON SM.PARCEL_SEQ = P.PARCEL_SEQ
                            AND SM.RECORD_STATUS = 'N'
                            AND SM.SEQUESTER_STS IN (1,2)
                        INNER JOIN MGT1.TB_REG_SEQUESTER S
                            ON S.SEQUESTER_SEQ = SM.SEQUESTER_SEQ
                            AND S.RECORD_STATUS = 'N'
                            AND S.SEQUESTER_STS IN (1,2)
                        LEFT JOIN MGT1.TB_REG_SEQUESTER_OWNER SO
                            ON SO.SEQUESTER_MAPPING_SEQ = SM.SEQUESTER_MAPPING_SEQ
                            AND SO.RECORD_STATUS = 'N'
                        LEFT JOIN MAS.TB_MAS_TITLE T
                            ON T.TITLE_SEQ = S.REQ_TITLE_SEQ
                        LEFT JOIN MAS.TB_MAS_TITLE TON
                            ON TON.TITLE_SEQ = SO.OWNER_TITLE_SEQ"
                        .$parcel_seq_sql1."
                        ),
                P2 AS (SELECT P.PARCEL_SEQ ,S.SEQUESTER_ORDER_NO, SEQUESTER_DOC_NO, SEQUESTER_DOC_DTM, SEQUESTER_REQ_TYPE, REQ_TITLE_SEQ, SEQUESTER_REQ_DEPT
                        , S.SEQUESTER_REQ_PID, T.TITLE_NAME, SEQUESTER_REQ_FNAME, SEQUESTER_REQ_MNAME, SEQUESTER_REQ_LNAME
                        , SEQUESTER_EXPIRE_STS, SEQUESTER_REQ_DTM, SEQUESTER_REC_DTM, SEQUESTER_EXPIRE_NUM, SEQUESTER_EXPIRE_DTM
                        , SEQUESTER_REM
                        , SEQUESTER_REQ_CONDITION, TON.TITLE_NAME AS TITLE_NAME_OWN, OWNER_FNAME, OWNER_MNAME, OWNER_LNAME
                    FROM REG.TB_REG_PARCEL P
                    INNER JOIN REG.TB_REG_SEQUESTER_MAPPING SM
                        ON SM.PARCEL_SEQ = P.PARCEL_SEQ
                        AND SM.RECORD_STATUS = 'N'
                        AND SM.SEQUESTER_STS IN (1,2)
                    INNER JOIN REG.TB_REG_SEQUESTER S
                        ON S.SEQUESTER_SEQ = SM.SEQUESTER_SEQ
                        AND S.RECORD_STATUS = 'N'
                        AND S.SEQUESTER_STS IN (1,2)
                    LEFT JOIN REG.TB_REG_SEQUESTER_OWNER SO
                        ON SO.SEQUESTER_MAPPING_SEQ = SM.SEQUESTER_MAPPING_SEQ
                        AND SO.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = S.REQ_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TON
                        ON TON.TITLE_SEQ = SO.OWNER_TITLE_SEQ"
                        .$parcel_seq_sql2."
                    )
                SELECT P1.*, P2.SEQUESTER_ORDER_NO AS SEQUESTER_ORDER_NO_1, P2.SEQUESTER_DOC_NO AS SEQUESTER_DOC_NO_1, P2.SEQUESTER_DOC_DTM AS SEQUESTER_DOC_DTM_1,
                P2.SEQUESTER_REQ_TYPE AS SEQUESTER_REQ_TYPE_1, P2.REQ_TITLE_SEQ AS REQ_TITLE_SEQ_1, P2.SEQUESTER_REQ_DEPT AS SEQUESTER_REQ_DEPT_1,
                P2.SEQUESTER_REQ_PID AS SEQUESTER_REQ_PID_1, P2.TITLE_NAME AS TITLE_NAME_1, P2.SEQUESTER_REQ_FNAME AS SEQUESTER_REQ_FNAME_1,
                P2.SEQUESTER_REQ_MNAME AS SEQUESTER_REQ_MNAME_1, P2.SEQUESTER_REQ_LNAME AS SEQUESTER_REQ_LNAME_1, P2.SEQUESTER_EXPIRE_STS AS SEQUESTER_EXPIRE_STS_1,
                P2.SEQUESTER_REQ_DTM AS SEQUESTER_REQ_DTM_1, P2.SEQUESTER_REC_DTM AS SEQUESTER_REC_DTM_1, P2.SEQUESTER_EXPIRE_NUM AS SEQUESTER_EXPIRE_NUM_1,
                P2.SEQUESTER_EXPIRE_DTM AS SEQUESTER_EXPIRE_DTM_1, P2.SEQUESTER_REM AS SEQUESTER_REM_1, P2.SEQUESTER_REQ_CONDITION AS SEQUESTER_REQ_CONDITION_1,
                P2.TITLE_NAME_OWN AS TITLE_NAME_OWN_1, P2.OWNER_FNAME AS OWNER_FNAME_1, P2.OWNER_MNAME AS OWNER_MNAME_1, P2.OWNER_LNAME AS OWNER_LNAME_1
                FROM P1
                FULL OUTER JOIN P2
                ON P1.PARCEL_SEQ = P2.PARCEL_SEQ";

            $stid = oci_parse($conn, $select); 
            if ($parcelSeqP1 != '') oci_bind_by_name($stid, ':parcelSeqP1', $parcelSeqP1);
            if ($parcelSeqP2 != '') oci_bind_by_name($stid, ':parcelSeqP2', $parcelSeqP2);
            oci_execute($stid);

            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;

        case '2':    //     2 chanodeTrajong
        case '3':    //     3 trajong
        case '4':    //     4 ns3a
        case '5':    //     5 ns3
        case '8':    //     8 nsl
        case '23':   //     23 subNsl 
            $select = "WITH P1 AS ( SELECT P.PARCEL_LAND_SEQ, S.SEQUESTER_ORDER_NO, SEQUESTER_DOC_NO, SEQUESTER_DOC_DTM, SEQUESTER_REQ_TYPE, REQ_TITLE_SEQ, SEQUESTER_REQ_DEPT
                            , S.SEQUESTER_REQ_PID, T.TITLE_NAME, SEQUESTER_REQ_FNAME, SEQUESTER_REQ_MNAME, SEQUESTER_REQ_LNAME
                            , SEQUESTER_EXPIRE_STS, SEQUESTER_REQ_DTM, SEQUESTER_REC_DTM, SEQUESTER_EXPIRE_NUM, SEQUESTER_EXPIRE_DTM
                            , SEQUESTER_REM
                            , SEQUESTER_REQ_CONDITION, TON.TITLE_NAME AS TITLE_NAME_OWN, OWNER_FNAME, OWNER_MNAME, OWNER_LNAME
                        FROM MGT1.TB_REG_PARCEL_LAND P
                        INNER JOIN MGT1.TB_REG_SEQUESTER_MAPPING SM
                            ON SM.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                            AND SM.RECORD_STATUS = 'N'
                            AND SM.SEQUESTER_STS IN (1,2)
                        INNER JOIN MGT1.TB_REG_SEQUESTER S
                            ON S.SEQUESTER_SEQ = SM.SEQUESTER_SEQ
                            AND S.RECORD_STATUS = 'N'
                            AND S.SEQUESTER_STS IN (1,2)
                        LEFT JOIN MGT1.TB_REG_SEQUESTER_OWNER SO
                            ON SO.SEQUESTER_MAPPING_SEQ = SM.SEQUESTER_MAPPING_SEQ
                            AND SO.RECORD_STATUS = 'N'
                        LEFT JOIN MAS.TB_MAS_TITLE T
                            ON T.TITLE_SEQ = S.REQ_TITLE_SEQ
                        LEFT JOIN MAS.TB_MAS_TITLE TON
                            ON TON.TITLE_SEQ = SO.OWNER_TITLE_SEQ
                            ".$parcel_land_seq_sql1."
                        ),
                P2 AS (SELECT P.PARCEL_LAND_SEQ ,S.SEQUESTER_ORDER_NO, SEQUESTER_DOC_NO, SEQUESTER_DOC_DTM, SEQUESTER_REQ_TYPE, REQ_TITLE_SEQ, SEQUESTER_REQ_DEPT
                        , S.SEQUESTER_REQ_PID, T.TITLE_NAME, SEQUESTER_REQ_FNAME, SEQUESTER_REQ_MNAME, SEQUESTER_REQ_LNAME
                        , SEQUESTER_EXPIRE_STS, SEQUESTER_REQ_DTM, SEQUESTER_REC_DTM, SEQUESTER_EXPIRE_NUM, SEQUESTER_EXPIRE_DTM
                        , SEQUESTER_REM
                        , SEQUESTER_REQ_CONDITION, TON.TITLE_NAME AS TITLE_NAME_OWN, OWNER_FNAME, OWNER_MNAME, OWNER_LNAME
                    FROM REG.TB_REG_PARCEL_LAND P
                    INNER JOIN REG.TB_REG_SEQUESTER_MAPPING SM
                        ON SM.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ
                        AND SM.RECORD_STATUS = 'N'
                        AND SM.SEQUESTER_STS IN (1,2)
                    INNER JOIN REG.TB_REG_SEQUESTER S
                        ON S.SEQUESTER_SEQ = SM.SEQUESTER_SEQ
                        AND S.RECORD_STATUS = 'N'
                        AND S.SEQUESTER_STS IN (1,2)
                    LEFT JOIN REG.TB_REG_SEQUESTER_OWNER SO
                        ON SO.SEQUESTER_MAPPING_SEQ = SM.SEQUESTER_MAPPING_SEQ
                        AND SO.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = S.REQ_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TON
                        ON TON.TITLE_SEQ = SO.OWNER_TITLE_SEQ
                        ".$parcel_land_seq_sql2."
                    )
                SELECT P1.*, P2.SEQUESTER_ORDER_NO AS SEQUESTER_ORDER_NO_1, P2.SEQUESTER_DOC_NO AS SEQUESTER_DOC_NO_1, P2.SEQUESTER_DOC_DTM AS SEQUESTER_DOC_DTM_1,
                P2.SEQUESTER_REQ_TYPE AS SEQUESTER_REQ_TYPE_1, P2.REQ_TITLE_SEQ AS REQ_TITLE_SEQ_1, P2.SEQUESTER_REQ_DEPT AS SEQUESTER_REQ_DEPT_1,
                P2.SEQUESTER_REQ_PID AS SEQUESTER_REQ_PID_1, P2.TITLE_NAME AS TITLE_NAME_1, P2.SEQUESTER_REQ_FNAME AS SEQUESTER_REQ_FNAME_1,
                P2.SEQUESTER_REQ_MNAME AS SEQUESTER_REQ_MNAME_1, P2.SEQUESTER_REQ_LNAME AS SEQUESTER_REQ_LNAME_1, P2.SEQUESTER_EXPIRE_STS AS SEQUESTER_EXPIRE_STS_1,
                P2.SEQUESTER_REQ_DTM AS SEQUESTER_REQ_DTM_1, P2.SEQUESTER_REC_DTM AS SEQUESTER_REC_DTM_1, P2.SEQUESTER_EXPIRE_NUM AS SEQUESTER_EXPIRE_NUM_1,
                P2.SEQUESTER_EXPIRE_DTM AS SEQUESTER_EXPIRE_DTM_1, P2.SEQUESTER_REM AS SEQUESTER_REM_1, P2.SEQUESTER_REQ_CONDITION AS SEQUESTER_REQ_CONDITION_1,
                P2.TITLE_NAME_OWN AS TITLE_NAME_OWN_1, P2.OWNER_FNAME AS OWNER_FNAME_1, P2.OWNER_MNAME AS OWNER_MNAME_1, P2.OWNER_LNAME AS OWNER_LNAME_1
                FROM P1
                FULL OUTER JOIN P2
                ON P1.PARCEL_LAND_SEQ = P2.PARCEL_LAND_SEQ";

            $stid = oci_parse($conn, $select); 
            if ($parcelSeqP1 != '') oci_bind_by_name($stid, ':parcelSeqP1', $parcelSeqP1);
            if ($parcelSeqP2 != '') oci_bind_by_name($stid, ':parcelSeqP2', $parcelSeqP2);
            oci_execute($stid);

            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }
        break;
        case 'c':  //     c construct
             $select = "WITH P1 AS ( SELECT P.CONSTRUCT_SEQ, S.SEQUESTER_ORDER_NO, SEQUESTER_DOC_NO, SEQUESTER_DOC_DTM, SEQUESTER_REQ_TYPE, REQ_TITLE_SEQ, SEQUESTER_REQ_DEPT
                            , S.SEQUESTER_REQ_PID, T.TITLE_NAME, SEQUESTER_REQ_FNAME, SEQUESTER_REQ_MNAME, SEQUESTER_REQ_LNAME
                            , SEQUESTER_EXPIRE_STS, SEQUESTER_REQ_DTM, SEQUESTER_REC_DTM, SEQUESTER_EXPIRE_NUM, SEQUESTER_EXPIRE_DTM
                            , SEQUESTER_REM
                            , SEQUESTER_REQ_CONDITION, TON.TITLE_NAME AS TITLE_NAME_OWN, OWNER_FNAME, OWNER_MNAME, OWNER_LNAME
                        FROM MGT1.TB_REG_CONSTRUCT P
                        INNER JOIN MGT1.TB_REG_SEQUESTER_MAPPING SM
                            ON SM.CONSTRUCT_SEQ = P.CONSTRUCT_SEQ
                            AND SM.RECORD_STATUS = 'N'
                            AND SM.SEQUESTER_STS IN (1,2)
                        INNER JOIN MGT1.TB_REG_SEQUESTER S
                            ON S.SEQUESTER_SEQ = SM.SEQUESTER_SEQ
                            AND S.RECORD_STATUS = 'N'
                            AND S.SEQUESTER_STS IN (1,2)
                        LEFT JOIN MGT1.TB_REG_SEQUESTER_OWNER SO
                            ON SO.SEQUESTER_MAPPING_SEQ = SM.SEQUESTER_MAPPING_SEQ
                            AND SO.RECORD_STATUS = 'N'
                        LEFT JOIN MAS.TB_MAS_TITLE T
                            ON T.TITLE_SEQ = S.REQ_TITLE_SEQ
                        LEFT JOIN MAS.TB_MAS_TITLE TON
                            ON TON.TITLE_SEQ = SO.OWNER_TITLE_SEQ
                            ".$constr_seq_sql1."
                        
                        ),
                P2 AS (SELECT P.CONSTRUCT_SEQ ,S.SEQUESTER_ORDER_NO, SEQUESTER_DOC_NO, SEQUESTER_DOC_DTM, SEQUESTER_REQ_TYPE, REQ_TITLE_SEQ, SEQUESTER_REQ_DEPT
                        , S.SEQUESTER_REQ_PID, T.TITLE_NAME, SEQUESTER_REQ_FNAME, SEQUESTER_REQ_MNAME, SEQUESTER_REQ_LNAME
                        , SEQUESTER_EXPIRE_STS, SEQUESTER_REQ_DTM, SEQUESTER_REC_DTM, SEQUESTER_EXPIRE_NUM, SEQUESTER_EXPIRE_DTM
                        , SEQUESTER_REM
                        , SEQUESTER_REQ_CONDITION, TON.TITLE_NAME AS TITLE_NAME_OWN, OWNER_FNAME, OWNER_MNAME, OWNER_LNAME
                    FROM REG.TB_REG_CONSTRUCT P
                    INNER JOIN REG.TB_REG_SEQUESTER_MAPPING SM
                        ON SM.CONSTRUCT_SEQ = P.CONSTRUCT_SEQ
                        AND SM.RECORD_STATUS = 'N'
                        AND SM.SEQUESTER_STS IN (1,2)
                    INNER JOIN REG.TB_REG_SEQUESTER S
                        ON S.SEQUESTER_SEQ = SM.SEQUESTER_SEQ
                        AND S.RECORD_STATUS = 'N'
                        AND S.SEQUESTER_STS IN (1,2)
                    LEFT JOIN REG.TB_REG_SEQUESTER_OWNER SO
                        ON SO.SEQUESTER_MAPPING_SEQ = SM.SEQUESTER_MAPPING_SEQ
                        AND SO.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = S.REQ_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TON
                        ON TON.TITLE_SEQ = SO.OWNER_TITLE_SEQ
                        ".$constr_seq_sql2."
                    )
                SELECT P1.*, P2.SEQUESTER_ORDER_NO AS SEQUESTER_ORDER_NO_1, P2.SEQUESTER_DOC_NO AS SEQUESTER_DOC_NO_1, P2.SEQUESTER_DOC_DTM AS SEQUESTER_DOC_DTM_1,
                P2.SEQUESTER_REQ_TYPE AS SEQUESTER_REQ_TYPE_1, P2.REQ_TITLE_SEQ AS REQ_TITLE_SEQ_1, P2.SEQUESTER_REQ_DEPT AS SEQUESTER_REQ_DEPT_1,
                P2.SEQUESTER_REQ_PID AS SEQUESTER_REQ_PID_1, P2.TITLE_NAME AS TITLE_NAME_1, P2.SEQUESTER_REQ_FNAME AS SEQUESTER_REQ_FNAME_1,
                P2.SEQUESTER_REQ_MNAME AS SEQUESTER_REQ_MNAME_1, P2.SEQUESTER_REQ_LNAME AS SEQUESTER_REQ_LNAME_1, P2.SEQUESTER_EXPIRE_STS AS SEQUESTER_EXPIRE_STS_1,
                P2.SEQUESTER_REQ_DTM AS SEQUESTER_REQ_DTM_1, P2.SEQUESTER_REC_DTM AS SEQUESTER_REC_DTM_1, P2.SEQUESTER_EXPIRE_NUM AS SEQUESTER_EXPIRE_NUM_1,
                P2.SEQUESTER_EXPIRE_DTM AS SEQUESTER_EXPIRE_DTM_1, P2.SEQUESTER_REM AS SEQUESTER_REM_1, P2.SEQUESTER_REQ_CONDITION AS SEQUESTER_REQ_CONDITION_1,
                P2.TITLE_NAME_OWN AS TITLE_NAME_OWN_1, P2.OWNER_FNAME AS OWNER_FNAME_1, P2.OWNER_MNAME AS OWNER_MNAME_1, P2.OWNER_LNAME AS OWNER_LNAME_1
                FROM P1
                FULL OUTER JOIN P2
                ON P1.CONSTRUCT_SEQ = P2.CONSTRUCT_SEQ";

            $stid = oci_parse($conn, $select); 
            if ($parcelSeqP1 != '') oci_bind_by_name($stid, ':parcelSeqP1', $parcelSeqP1);
            if ($parcelSeqP2 != '') oci_bind_by_name($stid, ':parcelSeqP2', $parcelSeqP2);
            oci_execute($stid);

            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            } 
        break; 
        case '9': //     9 condoroom 
             $select = "WITH P1 AS ( SELECT P.CONDOROOM_SEQ, S.SEQUESTER_ORDER_NO, SEQUESTER_DOC_NO, SEQUESTER_DOC_DTM, SEQUESTER_REQ_TYPE, REQ_TITLE_SEQ, SEQUESTER_REQ_DEPT
                            , S.SEQUESTER_REQ_PID, T.TITLE_NAME, SEQUESTER_REQ_FNAME, SEQUESTER_REQ_MNAME, SEQUESTER_REQ_LNAME
                            , SEQUESTER_EXPIRE_STS, SEQUESTER_REQ_DTM, SEQUESTER_REC_DTM, SEQUESTER_EXPIRE_NUM, SEQUESTER_EXPIRE_DTM
                            , SEQUESTER_REM
                            , SEQUESTER_REQ_CONDITION, TON.TITLE_NAME AS TITLE_NAME_OWN, OWNER_FNAME, OWNER_MNAME, OWNER_LNAME
                        FROM MGT1.TB_REG_CONDOROOM P
                        INNER JOIN MGT1.TB_REG_SEQUESTER_MAPPING SM
                            ON SM.CONDOROOM_SEQ = P.CONDOROOM_SEQ
                            AND SM.RECORD_STATUS = 'N'
                            AND SM.SEQUESTER_STS IN (1,2)
                        INNER JOIN MGT1.TB_REG_SEQUESTER S
                            ON S.SEQUESTER_SEQ = SM.SEQUESTER_SEQ
                            AND S.RECORD_STATUS = 'N'
                            AND S.SEQUESTER_STS IN (1,2)
                        LEFT JOIN MGT1.TB_REG_SEQUESTER_OWNER SO
                            ON SO.SEQUESTER_MAPPING_SEQ = SM.SEQUESTER_MAPPING_SEQ
                            AND SO.RECORD_STATUS = 'N'
                        LEFT JOIN MAS.TB_MAS_TITLE T
                            ON T.TITLE_SEQ = S.REQ_TITLE_SEQ
                        LEFT JOIN MAS.TB_MAS_TITLE TON
                            ON TON.TITLE_SEQ = SO.OWNER_TITLE_SEQ
                            ".$condo_room_seq_sql1."
                        ),
                P2 AS (SELECT P.CONDOROOM_SEQ ,S.SEQUESTER_ORDER_NO, SEQUESTER_DOC_NO, SEQUESTER_DOC_DTM, SEQUESTER_REQ_TYPE, REQ_TITLE_SEQ, SEQUESTER_REQ_DEPT
                        , S.SEQUESTER_REQ_PID, T.TITLE_NAME, SEQUESTER_REQ_FNAME, SEQUESTER_REQ_MNAME, SEQUESTER_REQ_LNAME
                        , SEQUESTER_EXPIRE_STS, SEQUESTER_REQ_DTM, SEQUESTER_REC_DTM, SEQUESTER_EXPIRE_NUM, SEQUESTER_EXPIRE_DTM
                        , SEQUESTER_REM
                        , SEQUESTER_REQ_CONDITION, TON.TITLE_NAME AS TITLE_NAME_OWN, OWNER_FNAME, OWNER_MNAME, OWNER_LNAME
                    FROM REG.TB_REG_CONDOROOM P
                    INNER JOIN REG.TB_REG_SEQUESTER_MAPPING SM
                        ON SM.CONDOROOM_SEQ = P.CONDOROOM_SEQ
                        AND SM.RECORD_STATUS = 'N'
                        AND SM.SEQUESTER_STS IN (1,2)
                    INNER JOIN REG.TB_REG_SEQUESTER S
                        ON S.SEQUESTER_SEQ = SM.SEQUESTER_SEQ
                        AND S.RECORD_STATUS = 'N'
                        AND S.SEQUESTER_STS IN (1,2)
                    LEFT JOIN REG.TB_REG_SEQUESTER_OWNER SO
                        ON SO.SEQUESTER_MAPPING_SEQ = SM.SEQUESTER_MAPPING_SEQ
                        AND SO.RECORD_STATUS = 'N'
                    LEFT JOIN MAS.TB_MAS_TITLE T
                        ON T.TITLE_SEQ = S.REQ_TITLE_SEQ
                    LEFT JOIN MAS.TB_MAS_TITLE TON
                        ON TON.TITLE_SEQ = SO.OWNER_TITLE_SEQ
                        ".$condo_room_seq_sql2."
                    )
                SELECT P1.*, P2.SEQUESTER_ORDER_NO AS SEQUESTER_ORDER_NO_1, P2.SEQUESTER_DOC_NO AS SEQUESTER_DOC_NO_1, P2.SEQUESTER_DOC_DTM AS SEQUESTER_DOC_DTM_1,
                P2.SEQUESTER_REQ_TYPE AS SEQUESTER_REQ_TYPE_1, P2.REQ_TITLE_SEQ AS REQ_TITLE_SEQ_1, P2.SEQUESTER_REQ_DEPT AS SEQUESTER_REQ_DEPT_1,
                P2.SEQUESTER_REQ_PID AS SEQUESTER_REQ_PID_1, P2.TITLE_NAME AS TITLE_NAME_1, P2.SEQUESTER_REQ_FNAME AS SEQUESTER_REQ_FNAME_1,
                P2.SEQUESTER_REQ_MNAME AS SEQUESTER_REQ_MNAME_1, P2.SEQUESTER_REQ_LNAME AS SEQUESTER_REQ_LNAME_1, P2.SEQUESTER_EXPIRE_STS AS SEQUESTER_EXPIRE_STS_1,
                P2.SEQUESTER_REQ_DTM AS SEQUESTER_REQ_DTM_1, P2.SEQUESTER_REC_DTM AS SEQUESTER_REC_DTM_1, P2.SEQUESTER_EXPIRE_NUM AS SEQUESTER_EXPIRE_NUM_1,
                P2.SEQUESTER_EXPIRE_DTM AS SEQUESTER_EXPIRE_DTM_1, P2.SEQUESTER_REM AS SEQUESTER_REM_1, P2.SEQUESTER_REQ_CONDITION AS SEQUESTER_REQ_CONDITION_1,
                P2.TITLE_NAME_OWN AS TITLE_NAME_OWN_1, P2.OWNER_FNAME AS OWNER_FNAME_1, P2.OWNER_MNAME AS OWNER_MNAME_1, P2.OWNER_LNAME AS OWNER_LNAME_1
                FROM P1
                LEFT OUTER JOIN P2
                ON P1.CONDOROOM_SEQ = P2.CONDOROOM_SEQ";

            $stid = oci_parse($conn, $select); 
            if ($parcelSeqP1 != '') oci_bind_by_name($stid, ':parcelSeqP1', $parcelSeqP1);
            if ($parcelSeqP2 != '') oci_bind_by_name($stid, ':parcelSeqP2', $parcelSeqP2);
            oci_execute($stid);

            while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
                $Result[] = $row;
            }  
        break;

    }


    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);


?>

