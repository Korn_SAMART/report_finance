<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';

    $landoffice = !isset($_POST['landoffice'])? '' : $_POST['landoffice'];

    $no = !isset($_POST['no']) ? '' : $_POST['no'];
    $date = !isset($_POST['date'])? '' : $_POST['date'];
    $status = !isset($_POST['status'])? '' : $_POST['status'];

    //ปรับปรุงรูปแปลงดิจิทัล
    $utmmap1 = !isset($_POST['utmmap1']) ? '' : $_POST['utmmap1'];
    $utmmap2 = !isset($_POST['utmmap2'])? '' : $_POST['utmmap2'];
    $utmmap3 = !isset($_POST['utmmap3'])? '' : $_POST['utmmap3'];
    $utmmap4 = !isset($_POST['utmmap4']) ? '' : $_POST['utmmap4'];
    $scale = !isset($_POST['scale'])? '' : $_POST['scale'];
    $landno = !isset($_POST['landno'])? '' : $_POST['landno'];
    $parceltype = !isset($_POST['parceltype'])? '' : $_POST['parceltype'];

    $jobgroup_seq = !isset($_POST['jobgroup_seq'])? '' : $_POST['jobgroup_seq'];
    $check = !isset($_POST['check'])? '' : $_POST['check'];
    
    $Result = array();

    include 'querySvaData.php';
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }
        
    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
