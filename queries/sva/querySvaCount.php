<?php
header('Content-Type: application/json; charset=utf-8');
//require_once 'isXmlHttpRequest.php';
include '../../database/conn.php';


switch ($check) {
    case 'overall':
        $select = " with recv as(
                -----------------------------------------------------------
                ------ ระบบบริหารงานรังวัดในสำนักงาน ------
                -----------------------------------------------------------
                ----- งานสำนักงานที่ดิน -----
                SELECT TO_NUMBER('1') as topic, count(T1.SURVEYJOB_SEQ) as recv_count
                FROM MGT1.TB_SVA_SURVEYJOB T1
                LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                
                WHERE T1.LANDOFFICE_SEQ = :landoffice  AND T1.RECORD_STATUS = 'N'  and JOBGROUP_SEQ =1 
                
                ----- งานรังวัดเอกชน -----
                UNION
                SELECT TO_NUMBER('2') as topic, count(T1.SURVEYJOB_SEQ) as recv_count
                FROM MGT1.TB_SVA_SURVEYJOB T1
                LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  and JOBGROUP_SEQ =2
                
                    
                ----- งานโครงการ -----
                UNION
                SELECT TO_NUMBER('3') as topic, count(T1.SURVEYJOB_SEQ) as recv_count
                FROM MGT1.TB_SVA_SURVEYJOB T1
                LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  and JOBGROUP_SEQ =3
                
                ----- งานรังวัดเดินสำรวจ -----
                UNION
                SELECT TO_NUMBER('4') as topic, count(T1.SURVEYJOB_SEQ) as recv_count
                FROM MGT1.TB_SVA_SURVEYJOB T1
                LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  and JOBGROUP_SEQ =4
                
                -----------------------------------------------------------
                ------ ระบบคำนวณรังวัดในสำนักงานที่ดิน ------
                -----------------------------------------------------------
                
                ----- งานสำนักงานที่ดิน -----
                UNION
                SELECT TO_NUMBER('5') as topic, count(T1.SURVEYJOB_SEQ) as recv_count
                FROM MGT1.TB_SVA_SURVEYJOB T1
                    INNER JOIN MGT1.TB_SVC_SURVEYDESC T2
                    ON T1.SURVEYJOB_SEQ  =  T2.SURVEYJOB_SEQ 
                LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                    WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  AND T2.RECORD_STATUS = 'N' and JOBGROUP_SEQ =1
                
                ----- งานรังวัดเอกชน -----
                UNION
                SELECT TO_NUMBER('6') as topic, count(T1.SURVEYJOB_SEQ) as recv_count
                FROM MGT1.TB_SVA_SURVEYJOB T1
                    INNER JOIN MGT1.TB_SVC_SURVEYDESC T2
                    ON T1.SURVEYJOB_SEQ  =  T2.SURVEYJOB_SEQ 
                    LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                    WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  AND T2.RECORD_STATUS = 'N' and JOBGROUP_SEQ =2
                
                ----- งานโครงการ -----
                UNION
                SELECT TO_NUMBER('7') as topic, count(T1.SURVEYJOB_SEQ) as recv_count
                FROM MGT1.TB_SVA_SURVEYJOB T1
                    INNER JOIN MGT1.TB_SVC_SURVEYDESC T2
                    ON T1.SURVEYJOB_SEQ  =  T2.SURVEYJOB_SEQ 
                LEFT OUTER JOIN
                (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                ON p1.landoffice_seq = T1.landoffice_seq
                    WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  AND T2.RECORD_STATUS = 'N' and JOBGROUP_SEQ =3
                
                ----- งานรังวัดเดินสำรวจ -----
                UNION
                SELECT TO_NUMBER('8') as topic, count(T1.SURVEYJOB_SEQ) as recv_count
                FROM MGT1.TB_SVA_SURVEYJOB T1
                    INNER JOIN MGT1.TB_SVC_SURVEYDESC T2
                    ON T1.SURVEYJOB_SEQ  =  T2.SURVEYJOB_SEQ 
                LEFT OUTER JOIN
                (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                ON p1.landoffice_seq = T1.landoffice_seq
                    WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  AND T2.RECORD_STATUS = 'N' and JOBGROUP_SEQ =4
                
                -----------------------------------------------------------
                ------ ระบบปรับปรุงรูปแผนที่ดิจิทัล ------
                -----------------------------------------------------------
                UNION
                SELECT TO_NUMBER('9') as topic, count(T1.UDM_PARCEL_SEQ) as recv_count
                FROM MGT1.TB_UDM_PARCEL T1
                LEFT OUTER JOIN
                (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                ON p1.landoffice_seq = T1.landoffice_seq
                WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  and AUTHOR_TYPE =3
                
                ),
                
                
                
                ok as(
                -----------------------------------------------------------
                ------ ระบบบริหารงานรังวัดในสำนักงาน ------
                -----------------------------------------------------------
                ----- งานสำนักงานที่ดิน -----
                SELECT TO_NUMBER('1') as topic, count(T1.SURVEYJOB_SEQ) as ok_count
                FROM SVO.TB_SVA_SURVEYJOB T1
                LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                WHERE T1.LANDOFFICE_SEQ = :landoffice  AND T1.RECORD_STATUS = 'N'  and JOBGROUP_SEQ =1
                
                ----- งานรังวัดเอกชน -----
                UNION
                SELECT TO_NUMBER('2') as topic, count(T1.SURVEYJOB_SEQ) as ok_count
                FROM SVO.TB_SVA_SURVEYJOB T1
                LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  and JOBGROUP_SEQ =2
                
                ----- งานโครงการ -----
                UNION
                SELECT TO_NUMBER('3') as topic, count(T1.SURVEYJOB_SEQ) as ok_count
                FROM SVO.TB_SVA_SURVEYJOB T1
                LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  and JOBGROUP_SEQ =3
                
                ----- งานรังวัดเดินสำรวจ -----
                UNION
                SELECT TO_NUMBER('4') as topic, count(T1.SURVEYJOB_SEQ) as ok_count
                FROM SVO.TB_SVA_SURVEYJOB T1
                LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  and JOBGROUP_SEQ =4
                
                -----------------------------------------------------------
                ------ ระบบคำนวณรังวัดในสำนักงานที่ดิน ------
                -----------------------------------------------------------
                
                ----- งานสำนักงานที่ดิน -----
                UNION
                SELECT TO_NUMBER('5') as topic, count(T1.SURVEYJOB_SEQ) as ok_count
                FROM SVO.TB_SVA_SURVEYJOB T1
                    INNER JOIN SVO.TB_SVC_SURVEYDESC T2
                    ON T1.SURVEYJOB_SEQ  =  T2.SURVEYJOB_SEQ 
                LEFT OUTER JOIN
                    (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                    ON p1.landoffice_seq = T1.landoffice_seq
                    WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  AND T2.RECORD_STATUS = 'N' and JOBGROUP_SEQ =1
                
                ----- งานรังวัดเอกชน -----
                UNION
                SELECT TO_NUMBER('6') as topic, count(T1.SURVEYJOB_SEQ) as ok_count
                    FROM SVO.TB_SVA_SURVEYJOB T1
                    INNER JOIN SVO.TB_SVC_SURVEYDESC T2
                    ON T1.SURVEYJOB_SEQ  =  T2.SURVEYJOB_SEQ 
                LEFT OUTER JOIN
                (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                ON p1.landoffice_seq = T1.landoffice_seq
                    WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  AND T2.RECORD_STATUS = 'N' and JOBGROUP_SEQ =2
                
                ----- งานโครงการ -----
                UNION
                SELECT TO_NUMBER('7') as topic, count(T1.SURVEYJOB_SEQ) as ok_count
                FROM SVO.TB_SVA_SURVEYJOB T1
                    INNER JOIN SVO.TB_SVC_SURVEYDESC T2
                    ON T1.SURVEYJOB_SEQ  =  T2.SURVEYJOB_SEQ 
                LEFT OUTER JOIN
                (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                ON p1.landoffice_seq = T1.landoffice_seq
                    WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  AND T2.RECORD_STATUS = 'N' and JOBGROUP_SEQ =3
                    
                ----- งานรังวัดเดินสำรวจ -----
                UNION
                SELECT TO_NUMBER('8') as topic, count(T1.SURVEYJOB_SEQ) as ok_count
                FROM SVO.TB_SVA_SURVEYJOB T1
                    INNER JOIN SVO.TB_SVC_SURVEYDESC T2
                    ON T1.SURVEYJOB_SEQ  =  T2.SURVEYJOB_SEQ
                LEFT OUTER JOIN
                (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                ON p1.landoffice_seq = T1.landoffice_seq
                    WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  AND T2.RECORD_STATUS = 'N' and JOBGROUP_SEQ =4
                
                -----------------------------------------------------------
                ------ ระบบปรับปรุงรูปแผนที่ดิจิทัล ------
                -----------------------------------------------------------
                UNION
                SELECT TO_NUMBER('9') as topic, count(T1.UDM_PARCEL_SEQ) as ok_count
                FROM SVO.TB_UDM_PARCEL T1
                LEFT OUTER JOIN
                (SELECT landoffice_seq , landoffice_name_th FROM MAS.tb_mas_landoffice) p1
                ON p1.landoffice_seq = T1.landoffice_seq
                WHERE T1.LANDOFFICE_SEQ = :landoffice AND T1.RECORD_STATUS = 'N'  and AUTHOR_TYPE =3
                
                )
                select 
                CASE recv.topic when 1 then 'งานสำนักงานที่ดิน'
                            when 2 then 'งานรังวัดเอกชน'
                            when 3 then 'งานโครงการ'
                            when 4 then 'งานรังวัดเดินสำรวจ'
                            when 5 then 'งานสำนักงานที่ดิน'
                            when 6 then 'งานรังวัดเอกชน'
                            when 7 then 'งานโครงการ'
                            when 8 then 'งานรังวัดเดินสำรวจ'
                            when 9 then 'ระบบปรับปรุงรูปแผนที่ดิจิทัล' end as TYPE,
                recv.recv_count as RECV,
                ok.ok_count as OK,
                (recv.recv_count - ok.ok_count) as ERROR
                
                from recv, ok
                where recv.topic = ok.topic";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);

        break;
    case 'local':
        $select = "WITH COUNTROW_P1 AS (
            SELECT T1.SURVEYJOB_SEQ, T2.JOBSTATUS_SEQ FROM MGT1.TB_SVA_SURVEYJOB T1
            LEFT OUTER JOIN MGT1.TB_SVA_MAS_JOBSTATUS T2
            ON T1.JOBSTATUS_SEQ =  T2.JOBSTATUS_SEQ
            WHERE T1.LANDOFFICE_SEQ = :landoffice  AND T1.RECORD_STATUS = 'N' AND T1.JOBGROUP_SEQ = :jobgroup_seq
        ),
        COUNTROW_P2 AS (
    SELECT T1.SURVEYJOB_SEQ, T2.JOBSTATUS_SEQ FROM SVO.TB_SVA_SURVEYJOB T1
            LEFT OUTER JOIN SVO.TB_SVA_MAS_JOBSTATUS T2
            ON T1.JOBSTATUS_SEQ =  T2.JOBSTATUS_SEQ
            WHERE T1.LANDOFFICE_SEQ = :landoffice  AND T1.RECORD_STATUS = 'N' AND T1.JOBGROUP_SEQ = :jobgroup_seq
        )
        SELECT JOBSTATUS.JOBSTATUS_SEQ, JOBSTATUS.JOBSTATUS_NAME AS NAME,  COUNT(P1.SURVEYJOB_SEQ) AS COUNT_P1, COUNT(P2.SURVEYJOB_SEQ) AS COUNT_P2
        FROM COUNTROW_P1 P1
        FULL OUTER JOIN COUNTROW_P2 P2
        ON P1.SURVEYJOB_SEQ = P2.SURVEYJOB_SEQ
        FULL OUTER JOIN SVO.TB_SVA_MAS_JOBSTATUS JOBSTATUS
        ON P1.JOBSTATUS_SEQ = JOBSTATUS.JOBSTATUS_SEQ
        AND P2.JOBSTATUS_SEQ = JOBSTATUS.JOBSTATUS_SEQ
        GROUP BY JOBSTATUS.JOBSTATUS_SEQ, JOBSTATUS.JOBSTATUS_NAME
        ORDER BY JOBSTATUS.JOBSTATUS_SEQ";

        $stid = oci_parse($conn, $select);
        if ($jobgroup_seq != '') oci_bind_by_name($stid, ':jobgroup_seq', $jobgroup_seq);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);

        break;
    case 'calculate':
        $select = "WITH COUNTROW_P1 AS (
                        SELECT T1.SURVEYJOB_SEQ, T2.JOBSTATUS_SEQ FROM MGT1.TB_SVA_SURVEYJOB T1
                        INNER JOIN MGT1.TB_SVC_SURVEYDESC SURVEYDESC
                        ON T1.SURVEYJOB_SEQ  =  SURVEYDESC.SURVEYJOB_SEQ 
                        LEFT OUTER JOIN MGT1.TB_SVA_MAS_JOBSTATUS T2
                        ON T1.JOBSTATUS_SEQ =  T2.JOBSTATUS_SEQ
                        WHERE T1.LANDOFFICE_SEQ = :landoffice  AND T1.RECORD_STATUS = 'N' AND SURVEYDESC.RECORD_STATUS = 'N' AND T1.JOBGROUP_SEQ = :jobgroup_seq
                    ),
                    COUNTROW_P2 AS (
                SELECT T1.SURVEYJOB_SEQ, T2.JOBSTATUS_SEQ FROM SVO.TB_SVA_SURVEYJOB T1
                        INNER JOIN SVO.TB_SVC_SURVEYDESC SURVEYDESC
                        ON T1.SURVEYJOB_SEQ  =  SURVEYDESC.SURVEYJOB_SEQ 
                        LEFT OUTER JOIN SVO.TB_SVA_MAS_JOBSTATUS T2
                        ON T1.JOBSTATUS_SEQ =  T2.JOBSTATUS_SEQ
                        WHERE T1.LANDOFFICE_SEQ = :landoffice  AND T1.RECORD_STATUS = 'N' AND SURVEYDESC.RECORD_STATUS = 'N' AND T1.JOBGROUP_SEQ = :jobgroup_seq
                    )
                    SELECT JOBSTATUS.JOBSTATUS_SEQ, JOBSTATUS.JOBSTATUS_NAME AS NAME,  COUNT(P1.SURVEYJOB_SEQ) AS COUNT_P1, COUNT(P2.SURVEYJOB_SEQ) AS COUNT_P2
                    FROM COUNTROW_P1 P1
                    FULL OUTER JOIN COUNTROW_P2 P2
                    ON  P1.SURVEYJOB_SEQ = P2.SURVEYJOB_SEQ
                    FULL OUTER JOIN SVO.TB_SVA_MAS_JOBSTATUS JOBSTATUS
                    ON P1.JOBSTATUS_SEQ = JOBSTATUS.JOBSTATUS_SEQ
                    AND P2.JOBSTATUS_SEQ = JOBSTATUS.JOBSTATUS_SEQ
                    GROUP BY JOBSTATUS.JOBSTATUS_SEQ, JOBSTATUS.JOBSTATUS_NAME
                    ORDER BY JOBSTATUS.JOBSTATUS_SEQ";

        $stid = oci_parse($conn, $select);
        if ($jobgroup_seq != '') oci_bind_by_name($stid, ':jobgroup_seq', $jobgroup_seq);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);

        break;
    case 'digital_map':
        $select = "WITH COUNTROW_P1 AS (
                    SELECT UDM_PARCEL.UDM_PARCEL_SEQ, PARCELTYPE.PARCELTYPE_SEQ FROM MGT1.TB_UDM_PARCEL UDM_PARCEL
                    LEFT OUTER JOIN MGT1.TB_UDM_MAS_PARCELTYPE PARCELTYPE
                    ON UDM_PARCEL.PARCELTYPE_SEQ = PARCELTYPE.PARCELTYPE_SEQ
                    WHERE UDM_PARCEL.LANDOFFICE_SEQ = :landoffice AND UDM_PARCEL.RECORD_STATUS = 'N' and AUTHOR_TYPE = 3
                ),
                COUNTROW_P2 AS (
                SELECT UDM_PARCEL.UDM_PARCEL_SEQ, PARCELTYPE.PARCELTYPE_SEQ FROM SVO.TB_UDM_PARCEL UDM_PARCEL
                    LEFT OUTER JOIN SVO.TB_UDM_MAS_PARCELTYPE PARCELTYPE
                    ON UDM_PARCEL.PARCELTYPE_SEQ = PARCELTYPE.PARCELTYPE_SEQ
                    WHERE UDM_PARCEL.LANDOFFICE_SEQ = :landoffice AND UDM_PARCEL.RECORD_STATUS = 'N' and AUTHOR_TYPE = 3
                )
                SELECT PARCELTYPE.PARCELTYPE_SEQ, PARCELTYPE.PARCELTYPE_DESC AS NAME,  COUNT(P1.UDM_PARCEL_SEQ) AS COUNT_P1, COUNT(P2.UDM_PARCEL_SEQ) AS COUNT_P2
                FROM COUNTROW_P1 P1
                FULL OUTER JOIN COUNTROW_P2 P2
                ON  P1.UDM_PARCEL_SEQ = P2.UDM_PARCEL_SEQ
                FULL OUTER JOIN SVO.TB_UDM_MAS_PARCELTYPE PARCELTYPE
                ON P1.PARCELTYPE_SEQ = PARCELTYPE.PARCELTYPE_SEQ
                AND P2.PARCELTYPE_SEQ = PARCELTYPE.PARCELTYPE_SEQ
                GROUP BY PARCELTYPE.PARCELTYPE_SEQ, PARCELTYPE.PARCELTYPE_DESC
                ORDER BY PARCELTYPE.PARCELTYPE_SEQ";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);

        oci_execute($stid);

        break;
}
