<?php
    header('Content-Type: application/json; charset=utf-8');
    //require_once 'isXmlHttpRequest.php';

    $landoffice = !isset($_POST['landoffice'])? '' : $_POST['landoffice'];

    $surveyjobSeqP1 = !isset($_POST['surveyjobSeqP1']) ? '' : $_POST['surveyjobSeqP1'];
    $surveyjobSeqP2 = !isset($_POST['surveyjobSeqP2']) ? '' : $_POST['surveyjobSeqP2'];

    $processSeqP1 = !isset($_POST['processSeqP1']) ? '' : $_POST['processSeqP1'];
    $processSeqP2 = !isset($_POST['processSeqP2']) ? '' : $_POST['processSeqP2'];

    $udmparcelSeqP1 = !isset($_POST['udmparcelSeqP1']) ? '' : $_POST['udmparcelSeqP1'];
    $udmparcelSeqP2 = !isset($_POST['udmparcelSeqP2']) ? '' : $_POST['udmparcelSeqP2'];

    
    $check = !isset($_POST['check'])? '' : $_POST['check'];
    $private = !isset($_POST['private'])? '' : $_POST['private'];
    
    $Result = array();

    include 'querySvaDetailData.php';
    while(($row = oci_fetch_array ($stid, OCI_ASSOC)) != false){
        $Result[] = $row;
    }

    
        
    echo json_encode(array($Result), JSON_UNESCAPED_UNICODE);
    oci_free_statement($stid);
    oci_close($conn);
