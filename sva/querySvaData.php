<?php
header('Content-Type: application/json; charset=utf-8');
//require_once 'isXmlHttpRequest.php';
include '../database/conn.php';

$sva_date_sql = $date == '' ? '' : " AND SURVEYJOB.SURVEYJOB_DATE = to_date(:date,'YYYY-MM-DD')";
$sva_jobstatus_seq_sql = $status == '' ? '' : " AND SURVEYJOB.JOBSTATUS_SEQ = :status";
$sva_parceltype_seq_sql = $parceltype == '' ? '' : " AND UDM_PARCEL.PARCELTYPE_SEQ = :parceltype";
$sva_no_sql = $no == '' ? '' : " WHERE NVL(P2.SURVEYJOB_NO,P1.SURVEYJOB_NO) = :no ";

switch ($check) {
    case 'local':
        $select = "WITH P1 AS
            (
                SELECT SURVEYJOB.SURVEYJOB_SEQ, SURVEYJOB.SURVEYJOB_NO, SURVEYJOB.SURVEYJOB_DATE, SURVEYJOB.LANDOFFICE_SEQ, 
                SURVEYJOB.PROCESS_SEQ, SURVEYJOB.JOBGROUP_SEQ, SURVEYJOB.PRIVATE_OFFICE_SEQ, SURVEYJOB.PROJECT_SEQ,
                SURVEYJOB.PRINTPLATE_TYPE_SEQ, SURVEYJOB.METHODOFSURVEY_SEQ, SURVEYJOB.TYPEOFSURVEY_SEQ, TYPEOFSURVEY.TYPEOFSURVEY_NAME, 
                SURVEYJOB.JOBSTATUS_SEQ, JOBSTATUS.JOBSTATUS_NAME, SURVEYJOB.SURVEYJOB_DESC
                FROM MGT1.TB_SVA_SURVEYJOB SURVEYJOB
                LEFT OUTER JOIN MGT1.TB_SVA_MAS_TYPEOFSURVEY TYPEOFSURVEY
                ON SURVEYJOB.TYPEOFSURVEY_SEQ = TYPEOFSURVEY.TYPEOFSURVEY_SEQ
                LEFT OUTER JOIN MGT1.TB_SVA_MAS_JOBSTATUS JOBSTATUS
                ON SURVEYJOB.JOBSTATUS_SEQ = JOBSTATUS.JOBSTATUS_SEQ
                LEFT OUTER JOIN MGT1.TB_SVA_MAS_JOBSTATUS JOBSTATUS
                ON SURVEYJOB.JOBSTATUS_SEQ = JOBSTATUS.JOBSTATUS_SEQ
                WHERE SURVEYJOB.LANDOFFICE_SEQ = :landoffice  AND SURVEYJOB.RECORD_STATUS = 'N' AND SURVEYJOB.JOBGROUP_SEQ = :jobgroup_seq" . $sva_jobstatus_seq_sql . $sva_date_sql . "
            ),
            P2 AS 
            (
                SELECT SURVEYJOB.SURVEYJOB_SEQ, SURVEYJOB.SURVEYJOB_NO, SURVEYJOB.SURVEYJOB_DATE, SURVEYJOB.LANDOFFICE_SEQ, 
                SURVEYJOB.PROCESS_SEQ, SURVEYJOB.JOBGROUP_SEQ, SURVEYJOB.PRIVATE_OFFICE_SEQ, SURVEYJOB.PROJECT_SEQ,
                SURVEYJOB.PRINTPLATE_TYPE_SEQ, SURVEYJOB.METHODOFSURVEY_SEQ, SURVEYJOB.TYPEOFSURVEY_SEQ, TYPEOFSURVEY.TYPEOFSURVEY_NAME, 
                SURVEYJOB.JOBSTATUS_SEQ, JOBSTATUS.JOBSTATUS_NAME, SURVEYJOB.SURVEYJOB_DESC
                FROM SVO.TB_SVA_SURVEYJOB SURVEYJOB
                LEFT OUTER JOIN SVO.TB_SVA_MAS_TYPEOFSURVEY TYPEOFSURVEY
                ON SURVEYJOB.TYPEOFSURVEY_SEQ = TYPEOFSURVEY.TYPEOFSURVEY_SEQ
                LEFT OUTER JOIN SVO.TB_SVA_MAS_JOBSTATUS JOBSTATUS
                ON SURVEYJOB.JOBSTATUS_SEQ = JOBSTATUS.JOBSTATUS_SEQ
                LEFT OUTER JOIN SVO.TB_SVA_MAS_JOBSTATUS JOBSTATUS
                ON SURVEYJOB.JOBSTATUS_SEQ = JOBSTATUS.JOBSTATUS_SEQ
                WHERE SURVEYJOB.LANDOFFICE_SEQ = :landoffice  AND SURVEYJOB.RECORD_STATUS = 'N' AND SURVEYJOB.JOBGROUP_SEQ = :jobgroup_seq" . $sva_jobstatus_seq_sql . $sva_date_sql . "
            )
            SELECT P1.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P1, P2.LANDOFFICE_SEQ AS LANDOFFICE_SEQ_P2,
                    P1.SURVEYJOB_SEQ AS SURVEYJOB_SEQ_P1, P2.SURVEYJOB_SEQ AS SURVEYJOB_SEQ_P2,
                    P1.PROCESS_SEQ AS PROCESS_SEQ_P1, P2.PROCESS_SEQ AS PROCESS_SEQ_P2,
                    P1.PRINTPLATE_TYPE_SEQ AS PRINTPLATE_TYPE_SEQ_P1, P2.PRINTPLATE_TYPE_SEQ AS PRINTPLATE_TYPE_SEQ_P2,
                    P1.METHODOFSURVEY_SEQ AS METHODOFSURVEY_SEQ_P1, P2.METHODOFSURVEY_SEQ AS METHODOFSURVEY_SEQ_P2,
                    P1.SURVEYJOB_NO AS SURVEYJOB_NO_P1, P2.SURVEYJOB_NO AS SURVEYJOB_NO_P2,
                    CASE WHEN SUBSTR(P1.SURVEYJOB_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P1.SURVEYJOB_DATE) ELSE TO_CHAR(P1.SURVEYJOB_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS SURVEYJOB_DATE_P1,
                    CASE WHEN SUBSTR(P2.SURVEYJOB_DATE, -4, 4) > 2500 
                    THEN TO_CHAR(P2.SURVEYJOB_DATE) ELSE TO_CHAR(P2.SURVEYJOB_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS SURVEYJOB_DATE_P2,
                    P1.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P1, P2.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P2,
                    P1.JOBSTATUS_NAME AS JOBSTATUS_NAME_P1, P2.JOBSTATUS_NAME AS JOBSTATUS_NAME_P2
                    , CASE WHEN (P2.SURVEYJOB_SEQ IS NOT NULL AND P1.SURVEYJOB_SEQ IS NOT NULL) THEN 0
                        WHEN (P2.SURVEYJOB_SEQ IS NULL) THEN 1
                        WHEN (P1.SURVEYJOB_SEQ IS NULL) THEN 2 END AS STS
            FROM P1
            INNER JOIN P2
            ON P1.SURVEYJOB_SEQ = P2.SURVEYJOB_SEQ"
            . $sva_no_sql
            . " ORDER BY P1.SURVEYJOB_NO, P2.SURVEYJOB_NO";
        $stid = oci_parse($conn, $select);
        if ($jobgroup_seq != '') oci_bind_by_name($stid, ':jobgroup_seq', $jobgroup_seq);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($no != '') oci_bind_by_name($stid, ':no', $no);
        if ($date != '') oci_bind_by_name($stid, ':date', $date);
        if ($status != '') oci_bind_by_name($stid, ':status', $status);


        oci_execute($stid);

        break;
    case 'calculate':
        $select = "WITH P1 AS (
            SELECT SURVEYJOB.SURVEYJOB_SEQ, SURVEYJOB.SURVEYJOB_NO, SURVEYJOB.SURVEYJOB_DATE, TYPEOFSURVEY.TYPEOFSURVEY_NAME,  JOBSTATUS.JOBSTATUS_NAME
                FROM MGT1.TB_SVA_SURVEYJOB SURVEYJOB
                INNER JOIN MGT1.TB_SVC_SURVEYDESC SURVEYDESC
                ON SURVEYJOB.SURVEYJOB_SEQ  =  SURVEYDESC.SURVEYJOB_SEQ 
                LEFT OUTER JOIN MGT1.TB_SVA_MAS_TYPEOFSURVEY TYPEOFSURVEY
                ON SURVEYJOB.TYPEOFSURVEY_SEQ = TYPEOFSURVEY.TYPEOFSURVEY_SEQ
                LEFT OUTER JOIN MGT1.TB_SVA_MAS_JOBSTATUS JOBSTATUS
                ON SURVEYJOB.JOBSTATUS_SEQ = JOBSTATUS.JOBSTATUS_SEQ
                WHERE SURVEYJOB.LANDOFFICE_SEQ = :landoffice  AND SURVEYJOB.RECORD_STATUS = 'N' AND SURVEYDESC.RECORD_STATUS = 'N' AND SURVEYJOB.JOBGROUP_SEQ = :jobgroup_seq" . $sva_jobstatus_seq_sql . $sva_date_sql . "
            ),
            P2 AS (
            SELECT SURVEYJOB.SURVEYJOB_SEQ, SURVEYJOB.SURVEYJOB_NO, SURVEYJOB.SURVEYJOB_DATE, TYPEOFSURVEY.TYPEOFSURVEY_NAME,  JOBSTATUS.JOBSTATUS_NAME
                FROM SVO.TB_SVA_SURVEYJOB SURVEYJOB
                INNER JOIN SVO.TB_SVC_SURVEYDESC SURVEYDESC
                ON SURVEYJOB.SURVEYJOB_SEQ  =  SURVEYDESC.SURVEYJOB_SEQ 
                LEFT OUTER JOIN SVO.TB_SVA_MAS_TYPEOFSURVEY TYPEOFSURVEY
                ON SURVEYJOB.TYPEOFSURVEY_SEQ = TYPEOFSURVEY.TYPEOFSURVEY_SEQ
                LEFT OUTER JOIN SVO.TB_SVA_MAS_JOBSTATUS JOBSTATUS
                ON SURVEYJOB.JOBSTATUS_SEQ = JOBSTATUS.JOBSTATUS_SEQ
                WHERE SURVEYJOB.LANDOFFICE_SEQ = :landoffice  AND SURVEYJOB.RECORD_STATUS = 'N' AND SURVEYDESC.RECORD_STATUS = 'N' AND SURVEYJOB.JOBGROUP_SEQ = :jobgroup_seq" . $sva_jobstatus_seq_sql . $sva_date_sql . "
            )
            SELECT P1.SURVEYJOB_SEQ AS SURVEYJOB_SEQ_P1, P2.SURVEYJOB_SEQ AS SURVEYJOB_SEQ_P2,
            P1.SURVEYJOB_NO AS SURVEYJOB_NO_P1, P2.SURVEYJOB_NO AS SURVEYJOB_NO_P2,
            CASE WHEN SUBSTR(P1.SURVEYJOB_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P1.SURVEYJOB_DATE) ELSE TO_CHAR(P1.SURVEYJOB_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS SURVEYJOB_DATE_P1,
            CASE WHEN SUBSTR(P2.SURVEYJOB_DATE, -4, 4) > 2500 
            THEN TO_CHAR(P2.SURVEYJOB_DATE) ELSE TO_CHAR(P2.SURVEYJOB_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS SURVEYJOB_DATE_P2,
            P1.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P1, P2.TYPEOFSURVEY_NAME AS TYPEOFSURVEY_NAME_P2,
            P1.JOBSTATUS_NAME AS JOBSTATUS_NAME_P1, P2.JOBSTATUS_NAME AS JOBSTATUS_NAME_P2
            FROM P1
            INNER JOIN P2
            ON P1.SURVEYJOB_SEQ = P2.SURVEYJOB_SEQ"
            . $sva_no_sql
            . " ORDER BY P1.SURVEYJOB_NO, P2.SURVEYJOB_NO";

        $stid = oci_parse($conn, $select);
        if ($jobgroup_seq != '') oci_bind_by_name($stid, ':jobgroup_seq', $jobgroup_seq);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($no != '') oci_bind_by_name($stid, ':no', $no);
        if ($date != '') oci_bind_by_name($stid, ':date', $date);
        if ($status != '') oci_bind_by_name($stid, ':status', $status);


        oci_execute($stid);

        break;
    case 'digital_map':
        $sva_utmmap1_sql = $utmmap1 == '' ? '' : " WHERE NVL(P2.UTMMAP1,P1.UTMMAP1) = :utmmap1";
        $sva_utmmap2_sql = $utmmap2 == '' ? '' : " WHERE NVL(P2.UTMMAP2,P1.UTMMAP2) = :utmmap2";
        $sva_utmmap3_sql = $utmmap3 == '' ? '' : " WHERE NVL(P2.UTMMAP3,P1.UTMMAP3) = :utmmap3";
        $sva_utmmap4_sql = $utmmap4 == '' ? '' : " WHERE NVL(P2.UTMMAP4,P1.UTMMAP4) = :utmmap4";
        $sva_scale_sql = $scale == '' ? '' : " WHERE NVL(P2.SCALE_SEQ,P1.SCALE_SEQ) = :scale ";
        $sva_landno_sql = $landno == '' ? '' : " WHERE NVL(P2.LAND_NO,P1.LAND_NO) = :landno ";

        if ($utmmap1 != '' && $utmmap2 != '')  $sva_utmmap2_sql = str_replace("WHERE", "AND", $sva_utmmap2_sql);
        if ($utmmap1 != '' && $utmmap3 != '')  $sva_utmmap3_sql = str_replace("WHERE", "AND", $sva_utmmap3_sql);
        if ($utmmap1 != '' && $utmmap4 != '')  $sva_utmmap4_sql = str_replace("WHERE", "AND", $sva_utmmap4_sql);

        if ($utmmap2 != '' && $utmmap3 != '')  $sva_utmmap3_sql = str_replace("WHERE", "AND", $sva_utmmap3_sql);
        if ($utmmap2 != '' && $utmmap4 != '')  $sva_utmmap4_sql = str_replace("WHERE", "AND", $sva_utmmap4_sql);

        if ($utmmap3 != '' && $utmmap4 != '')  $sva_utmmap4_sql = str_replace("WHERE", "WHERE", $sva_utmmap4_sql);

        if (($utmmap1 != '' || $utmmap2 != '' || $utmmap3 != '' || $utmmap4 != '') && $scale != '')  $sva_scale_sql = str_replace("WHERE", "AND", $sva_scale_sql);

        if (($utmmap1 != '' || $utmmap2 != '' || $utmmap3 != '' || $utmmap4 != '') && $landno != '')  $sva_landno_sql = str_replace("WHERE", "AND", $sva_landno_sql);

        if ($scale != '' && $landno != '')  $sva_landno_sql = str_replace("WHERE", "AND", $sva_landno_sql);


        $select = "WITH P1 AS (
                SELECT UDM_PARCEL.UDM_PARCEL_SEQ, UDM_PARCEL.UTMMAP1,UDM_PARCEL.UTMMAP2,UDM_PARCEL.UTMMAP3, TRIM(UDM_PARCEL.UTMMAP1)||' '||TRIM(TO_CHAR(UDM_PARCEL.UTMMAP2,'RN'))||' '||UDM_PARCEL.UTMMAP3 AS UTM, SCALE.SCALE_SEQ, SCALE.SCALE_NAME, UDM_PARCEL.UTMMAP4, UDM_PARCEL.LAND_NO, PARCELTYPE.PARCELTYPE_DESC
                     FROM MGT1.TB_UDM_PARCEL UDM_PARCEL
                     LEFT OUTER JOIN MGT1.TB_SVA_MAS_SCALE SCALE
                     ON UDM_PARCEL.SCALE_SEQ = SCALE.SCALE_SEQ
                     LEFT OUTER JOIN MGT1.TB_UDM_MAS_PARCELTYPE PARCELTYPE
                     ON UDM_PARCEL.PARCELTYPE_SEQ = PARCELTYPE.PARCELTYPE_SEQ
                    WHERE UDM_PARCEL.LANDOFFICE_SEQ = :landoffice AND UDM_PARCEL.RECORD_STATUS = 'N' AND AUTHOR_TYPE = 3" . $sva_parceltype_seq_sql . "
                ),
                P2 AS (
                     SELECT UDM_PARCEL.UDM_PARCEL_SEQ, UDM_PARCEL.UTMMAP1,UDM_PARCEL.UTMMAP2,UDM_PARCEL.UTMMAP3, TRIM(UDM_PARCEL.UTMMAP1)||' '||TRIM(TO_CHAR(UDM_PARCEL.UTMMAP2,'RN'))||' '||UDM_PARCEL.UTMMAP3 AS UTM, SCALE.SCALE_SEQ, SCALE.SCALE_NAME, UDM_PARCEL.UTMMAP4, UDM_PARCEL.LAND_NO, PARCELTYPE.PARCELTYPE_DESC
                     FROM SVO.TB_UDM_PARCEL UDM_PARCEL
                     LEFT OUTER JOIN SVO.TB_SVA_MAS_SCALE SCALE
                     ON UDM_PARCEL.SCALE_SEQ = SCALE.SCALE_SEQ
                     LEFT OUTER JOIN SVO.TB_UDM_MAS_PARCELTYPE PARCELTYPE
                     ON UDM_PARCEL.PARCELTYPE_SEQ = PARCELTYPE.PARCELTYPE_SEQ
                    WHERE UDM_PARCEL.LANDOFFICE_SEQ = :landoffice AND UDM_PARCEL.RECORD_STATUS = 'N' AND AUTHOR_TYPE = 3" . $sva_parceltype_seq_sql . "
                    )
                SELECT P1.UDM_PARCEL_SEQ AS UDM_PARCEL_SEQ_P1, P2.UDM_PARCEL_SEQ AS UDM_PARCEL_SEQ_P2,
                P1.UTM AS UTM_P1, P2.UTM AS UTM_P2,
                P1.SCALE_NAME AS SCALE_NAME_P1, P2.SCALE_NAME AS SCALE_NAME_P2,
                P1.UTMMAP4 AS UTMMAP4_P1, P2.UTMMAP4 AS UTMMAP4_P2,
                P1.LAND_NO AS LAND_NO_P1, P2.LAND_NO AS LAND_NO_P2,
                P1.PARCELTYPE_DESC AS PARCELTYPE_DESC_P1, P2.PARCELTYPE_DESC AS PARCELTYPE_DESC_P2
                FROM P1
                INNER JOIN P2
                ON P1.UDM_PARCEL_SEQ = P2.UDM_PARCEL_SEQ"
            . $sva_utmmap1_sql
            . $sva_utmmap2_sql
            . $sva_utmmap3_sql
            . $sva_utmmap4_sql
            . $sva_scale_sql
            . $sva_landno_sql
            . " ORDER BY NVL(P1.UDM_PARCEL_SEQ, P2.UDM_PARCEL_SEQ)";

        $stid = oci_parse($conn, $select);
        if ($landoffice != '') oci_bind_by_name($stid, ':landoffice', $landoffice);
        if ($utmmap1 != '') oci_bind_by_name($stid, ':utmmap1', $utmmap1);
        if ($utmmap2 != '') oci_bind_by_name($stid, ':utmmap2', $utmmap2);
        if ($utmmap3 != '') oci_bind_by_name($stid, ':utmmap3', $utmmap3);
        if ($utmmap4 != '') oci_bind_by_name($stid, ':utmmap4', $utmmap4);
        if ($landno != '') oci_bind_by_name($stid, ':landno', $landno);
        if ($scale != '') oci_bind_by_name($stid, ':scale', $scale);
        if ($parceltype != '') oci_bind_by_name($stid, ':parceltype', $parceltype);

        oci_execute($stid);

        break;
}
