<?php
header('Content-Type: application/json; charset=utf-8');
//require_once 'isXmlHttpRequest.php';
include '../database/conn.php';

switch ($table) {
    case 'P1':
        $select = " SELECT SURVEYJOB.SURVEYJOB_SEQ, FEESURVEY.FEESURVEY_NAME, CHARGE_DETAIL.FEESURVEY_QTY, FEESURVEY.FEESURVEY_UNITTEXT, CHARGE_DETAIL.FEESURVEY_AMOUNT, TO_NUMBER('1') AS KEY
        FROM MGT1.TB_SVA_SURVEYJOB SURVEYJOB
        LEFT JOIN MGT1.TB_SVA_SURVEYCHARGE CHARGE
        ON CHARGE.SURVEYJOB_SEQ = SURVEYJOB.SURVEYJOB_SEQ
        LEFT JOIN MGT1.TB_SVA_SURVEYCHARGE_DETAIL CHARGE_DETAIL
        ON CHARGE.SURVEYCHARGE_SEQ = CHARGE_DETAIL.SURVEYCHARGE_SEQ
        LEFT JOIN MGT1.TB_SVA_MAS_FEESURVEY FEESURVEY
        ON CHARGE_DETAIL.FEESURVEY_SEQ = FEESURVEY.FEESURVEY_SEQ
        WHERE SURVEYJOB.SURVEYJOB_SEQ = :surveyjobSeqP1 AND SURVEYJOB.RECORD_STATUS = 'N' AND CHARGE_DETAIL.FEESURVEY_AMOUNT <> 0
        ORDER BY FEESURVEY.ORDER_FIELD";
        $stid = oci_parse($conn, $select);

        if ($surveyjobSeqP1 != '') oci_bind_by_name($stid, ':surveyjobSeqP1', $surveyjobSeqP1);


        oci_execute($stid);
        break;
    case 'P2':
        $select = " SELECT SURVEYJOB.SURVEYJOB_SEQ, FEESURVEY.FEESURVEY_NAME, CHARGE_DETAIL.FEESURVEY_QTY, FEESURVEY.FEESURVEY_UNITTEXT, CHARGE_DETAIL.FEESURVEY_AMOUNT, TO_NUMBER('2') AS KEY
        FROM SVO.TB_SVA_SURVEYJOB SURVEYJOB
        LEFT JOIN SVO.TB_SVA_SURVEYCHARGE CHARGE
        ON CHARGE.SURVEYJOB_SEQ = SURVEYJOB.SURVEYJOB_SEQ
        LEFT JOIN SVO.TB_SVA_SURVEYCHARGE_DETAIL CHARGE_DETAIL
        ON CHARGE.SURVEYCHARGE_SEQ = CHARGE_DETAIL.SURVEYCHARGE_SEQ
        LEFT JOIN SVO.TB_SVA_MAS_FEESURVEY FEESURVEY
        ON CHARGE_DETAIL.FEESURVEY_SEQ = FEESURVEY.FEESURVEY_SEQ
        WHERE SURVEYJOB.SURVEYJOB_SEQ = :surveyjobSeqP2 AND SURVEYJOB.RECORD_STATUS = 'N' AND CHARGE_DETAIL.FEESURVEY_AMOUNT <> 0
        ORDER BY FEESURVEY.ORDER_FIELD";
        $stid = oci_parse($conn, $select);

        if ($surveyjobSeqP2 != '') oci_bind_by_name($stid, ':surveyjobSeqP2', $surveyjobSeqP2);


        oci_execute($stid);
        break;

    case 'withdraw':
        $select = "WITH P1 AS (
            SELECT SURVEYJOB.SURVEYJOB_SEQ, WITHDRAW.BTD59_NO, WITHDRAW.WITHDRAW_DATE, WITHDRAW.WITHDRAW_AMOUNT,WITHDRAW.WITHDRAW_EXPENSE, WITHDRAW.WITHDRAW_ADJUST_AMOUNT,
                CASE WITHDRAW.WITHDRAW_ADJUST_TYPE WHEN '0' THEN 'ไม่มีการเพิ่ม/คืน' WHEN '1' THEN 'มีการเพิ่ม' WHEN '2' THEN 'มีการคืนผู้ขอ' ELSE '-' END AS WITHDRAW_ADJUST_TYPE, 
                STATUS.WITHDRAWSTATUS_NAME
               
               FROM MGT1.TB_SVA_SURVEYJOB SURVEYJOB
               LEFT JOIN MGT1.TB_SVA_WITHDRAW WITHDRAW
               ON SURVEYJOB.SURVEYJOB_SEQ = WITHDRAW.SURVEYJOB_SEQ
               LEFT JOIN MGT1.TB_SVA_MAS_WITHDRAWSTATUS STATUS
               ON STATUS.WITHDRAWSTATUS_SEQ = WITHDRAW.WITHDRAWSTATUS_SEQ
               WHERE SURVEYJOB.SURVEYJOB_SEQ = :surveyjobSeqP1 AND SURVEYJOB.RECORD_STATUS = 'N'
           ),
           P2 AS (
                SELECT SURVEYJOB.SURVEYJOB_SEQ, WITHDRAW.BTD59_NO, WITHDRAW.WITHDRAW_DATE, WITHDRAW.WITHDRAW_AMOUNT,WITHDRAW.WITHDRAW_EXPENSE, WITHDRAW.WITHDRAW_ADJUST_AMOUNT,
                CASE WITHDRAW.WITHDRAW_ADJUST_TYPE WHEN '0' THEN 'ไม่มีการเพิ่ม/คืน' WHEN '1' THEN 'มีการเพิ่ม' WHEN '2' THEN 'มีการคืนผู้ขอ' ELSE '-' END AS WITHDRAW_ADJUST_TYPE, 
                STATUS.WITHDRAWSTATUS_NAME
               
               FROM SVO.TB_SVA_SURVEYJOB SURVEYJOB
               LEFT JOIN SVO.TB_SVA_WITHDRAW WITHDRAW
               ON SURVEYJOB.SURVEYJOB_SEQ = WITHDRAW.SURVEYJOB_SEQ
               LEFT JOIN SVO.TB_SVA_MAS_WITHDRAWSTATUS STATUS
               ON STATUS.WITHDRAWSTATUS_SEQ = WITHDRAW.WITHDRAWSTATUS_SEQ
               WHERE SURVEYJOB.SURVEYJOB_SEQ = :surveyjobSeqP2 AND SURVEYJOB.RECORD_STATUS = 'N'
           )
           SELECT P1.BTD59_NO AS BTD59_NO_P1, P2.BTD59_NO AS BTD59_NO_P2,
           CASE WHEN SUBSTR(P1.WITHDRAW_DATE, -4, 4) > 2500 
           THEN TO_CHAR(P1.WITHDRAW_DATE) ELSE TO_CHAR(P1.WITHDRAW_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS WITHDRAW_DATE_P1,
           CASE WHEN SUBSTR(P2.WITHDRAW_DATE, -4, 4) > 2500 
           THEN TO_CHAR(P2.WITHDRAW_DATE) ELSE TO_CHAR(P2.WITHDRAW_DATE, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS WITHDRAW_DATE_P2,
           P1.WITHDRAW_AMOUNT AS WITHDRAW_AMOUNT_P1, P2.WITHDRAW_AMOUNT AS WITHDRAW_AMOUNT_P2,
           P1.WITHDRAW_EXPENSE AS WITHDRAW_EXPENSE_P1, P2.WITHDRAW_EXPENSE AS WITHDRAW_EXPENSE_P2,
           P1.WITHDRAW_ADJUST_AMOUNT AS WITHDRAW_ADJUST_AMOUNT_P1, P2.WITHDRAW_ADJUST_AMOUNT AS WITHDRAW_ADJUST_AMOUNT_P2,
           P1.WITHDRAW_ADJUST_TYPE AS WITHDRAW_ADJUST_TYPE_P1, P2.WITHDRAW_ADJUST_TYPE AS WITHDRAW_ADJUST_TYPE_P2,
           P1.WITHDRAWSTATUS_NAME AS WITHDRAWSTATUS_NAME_P1, P2.WITHDRAWSTATUS_NAME AS WITHDRAWSTATUS_NAME_P2
           FROM P1
           INNER JOIN P2
           ON P1.SURVEYJOB_SEQ = P2.SURVEYJOB_SEQ";

            $stid = oci_parse($conn, $select);

            if ($surveyjobSeqP1 != '') oci_bind_by_name($stid, ':surveyjobSeqP1', $surveyjobSeqP1);
            if ($surveyjobSeqP2 != '') oci_bind_by_name($stid, ':surveyjobSeqP2', $surveyjobSeqP2);


            oci_execute($stid);
            break;

    default:
        # code...
        break;
}
