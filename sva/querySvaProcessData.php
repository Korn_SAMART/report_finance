<?php
header('Content-Type: application/json; charset=utf-8');
//require_once 'isXmlHttpRequest.php';
include '../database/conn.php';


switch ($table) {
    case 'P1':
        $select = "SELECT CASE WHEN SUBSTR(MQ.MANAGE_QUEUE_DTM, -4, 4) > 2500 
        THEN TO_CHAR(MQ.MANAGE_QUEUE_DTM) ELSE TO_CHAR(MQ.MANAGE_QUEUE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS MANAGE_QUEUE_DTM,
        MQ.MANAGE_QUEUE_NO, REQ.REQUEST_TYPE, PRO.PROCESS_SEQ, PRO.PROCESS_ORDER, PRO.PROCESS_REGIST_NAME, TO_NUMBER('1') AS KEY
        FROM MGT1.TB_REG_PROCESS_PARCEL_TEMP PPT
        INNER JOIN MGT1.TB_REG_PROCESS PRO
        ON PRO.PROCESS_SEQ = PPT.PROCESS_TEMP_SEQ
        AND PRO.RECORD_STATUS = 'N'
        INNER JOIN MGT1.TB_REG_MANAGE_QUEUE MQ
        ON MQ.REQUEST_TEMP_SEQ = PRO.REQUEST_SEQ
        AND MQ.RECORD_STATUS = 'N'
        INNER JOIN MGT1.TB_REG_REQUEST REQ
        ON REQ.REQUEST_SEQ = PRO.REQUEST_SEQ
        AND REQ.RECORD_STATUS = 'N'
        WHERE PPT.PROCESS_TEMP_SEQ = :processSeqP1";

        $stid = oci_parse($conn, $select);

        if ($processSeqP1 != '') oci_bind_by_name($stid, ':processSeqP1', $processSeqP1);
        oci_execute($stid);

        break;
    case 'P2':
        $select = "SELECT CASE WHEN SUBSTR(MQ.MANAGE_QUEUE_DTM, -4, 4) > 2500 
        THEN TO_CHAR(MQ.MANAGE_QUEUE_DTM) ELSE TO_CHAR(MQ.MANAGE_QUEUE_DTM, 'dd MON yyyy', 'NLS_CALENDAR=''THAI BUDDHA'' NLS_DATE_LANGUAGE=THAI') END AS MANAGE_QUEUE_DTM,
        MQ.MANAGE_QUEUE_NO, REQ.REQUEST_TYPE, PRO.PROCESS_SEQ, PRO.PROCESS_ORDER, PRO.PROCESS_REGIST_NAME, TO_NUMBER('2') AS KEY
        FROM REG.TB_REG_PROCESS_PARCEL_TEMP PPT
        INNER JOIN REG.TB_REG_PROCESS PRO
        ON PRO.PROCESS_SEQ = PPT.PROCESS_TEMP_SEQ
        AND PRO.RECORD_STATUS = 'N'
        INNER JOIN REG.TB_REG_MANAGE_QUEUE MQ
        ON MQ.REQUEST_TEMP_SEQ = PRO.REQUEST_SEQ
        AND MQ.RECORD_STATUS = 'N'
        INNER JOIN REG.TB_REG_REQUEST REQ
        ON REQ.REQUEST_SEQ = PRO.REQUEST_SEQ
        AND REQ.RECORD_STATUS = 'N'
        WHERE PPT.PROCESS_TEMP_SEQ = :processSeqP2";

        $stid = oci_parse($conn, $select);

        if ($processSeqP2 != '') oci_bind_by_name($stid, ':processSeqP2', $processSeqP2);
        oci_execute($stid);

        break;
 
}


