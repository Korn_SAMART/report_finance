<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ข้อมูลงานรังวัดและแผนที่</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="./style/style.css">
    <link rel="stylesheet" href="./dist/css/dash.css">
    <link rel="stylesheet" href="./plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="./plugins/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./dist/css/adminlte.min.css">
    <link rel="stylesheet" href="./dist/DataTables/datatables.min.css" />
    <link rel="stylesheet" href="./dist/DataTables/jquery.datatables.min.css" />

    <script src="./plugins/jquery/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./style/login.css">


    <link rel="shortcut icon" href="#" />

    <style>
        select {
            border: 1px solid #ccc;
            vertical-align: top;
            min-height: 20px;
        }
    </style>

</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div>
        <img id="main-bg" src="./img/dol.png" style="position: absolute;left: 31%;opacity: 0.15;width: 55vw;z-index: -1;">
    </div>
    <div class="page-wrapper chiller-theme toggled">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper navbar-expand" style="overflow: scroll;">
            <div class="sidebar-content">
                <div class="sidebar-brand">
                    <a href="#" id="landoffice_name"></a>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name">Admin</span>
                        <span class="user-role">Administrator</span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
                <!-- sidebar-header  -->
                <nav class="mt-auto sidebar-menu">
                    <ul class="nav-pills-main nav nav-pills nav-sidebar flex-column" style="overflow-x: hidden;" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item " role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#" class="nav-link">
                                <i class="nav-icon fa fa-book"></i>
                                <p>
                                    ภาพรวมการถ่ายโอนข้อมูล
                                    <i class="fas fa-lg fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a data-toggle="pill" href="#tab-sva-overall" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>ภาพรวมการถ่ายโอนข้อมูล</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item has-treeview " data-widget="treeview" role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#" class="nav-link">
                                <i class="nav-icon fa fa-book"></i>
                                <p>
                                    ระบบบริหารงานรังวัดในสำนักงาน
                                    <i class="fas fa-lg fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a data-toggle="pill" href="#tab-sva-landoffice_local" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>งานสำนักงานที่ดิน</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a data-toggle="pill" href="#tab-sva-private_local" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>งานรังวัดเอกชน</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a data-toggle="pill" href="#tab-sva-project_local" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>งานโครงการ</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a data-toggle="pill" href="#tab-sva-survey_local" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>งานรังวัดเดินสำรวจ</p>
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li class="nav-item has-treeview " data-widget="treeview" role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#" class="nav-link">
                                <i class="nav-icon fa fa-calculator"></i>
                                <p>
                                    ระบบคำนวณรังวัดในสำนักงานที่ดิน
                                    <i class="fas fa-lg  fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a data-toggle="pill" href="#tab-sva-landoffice_calculate" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>งานสำนักงานที่ดิน</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a data-toggle="pill" href="#tab-sva-private_calculate" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>งานรังวัดเอกชน</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a data-toggle="pill" href="#tab-sva-project_calculate" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>งานโครงการ</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a data-toggle="pill" href="#tab-sva-survey_calculate" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>งานรังวัดเดินสำรวจ</p>
                                    </a>
                                </li>


                            </ul>
                        </li>
                        <li class="nav-item has-treeview " data-widget="treeview" role="menu" data-accordion="true">
                            <a data-toggle="pill" href="#" class="nav-link">
                                <i class="nav-icon fa fa-map"></i>
                                <p>
                                    ระบบปรับปรุงรูปแผนที่ดิจิทัล
                                    <i class="fas fa-lg  fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a data-toggle="pill" href="#tab-sva-digital_map" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>ระบบปรับปรุงรูปแผนที่ดิจิทัล</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </nav>
                <div class="sidebar-footer">
                    <button class="btn btn-block" id="sign-out-btn" onclick="location.href='./portal.php';">กลับสู่หน้า Portal</button>
                </div>

        </nav>
        <!-- sidebar-wrapper  -->
        <main class="page-content">
            <div class="tab-pane fade" id="tab-sva-overall">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="card" style="margin-top: 10px;">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-sva-overall">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th style="vertical-align: middle;">ลำดับ</th>
                                        <th style="vertical-align: middle;text-align: center;">ประเภทงาน</th>
                                        <th style="vertical-align: middle;">รับมอบ</th>
                                        <th style="vertical-align: middle;">ถ่ายโอนสำเร็จ</th>
                                        <th style="vertical-align: middle;">ถ่ายโอนไม่สำเร็จ</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- งานสำนักงานที่ดิน -->
            <div class="tab-pane fade" id="tab-sva-landoffice_local">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-sva-1" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-warning" type="submit" id="export-diff-sva-1" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ข้อมูลแตกต่างกัน</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-sva-1" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>

                        <div class="card" id="tab-sva-landoffice_local-table">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-sva-landoffice_local-count">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th colspan="4">สรุปยอดการถ่ายโอนข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่คำขอรังวัด</label>
                                    <input type="text" name="no-sva-landoffice_local" id="no-sva-landoffice_local" class="form-control">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">วันที่รับคำขอ</label>
                                    <input type="date" name="date-sva-landoffice_local" id="date-sva-landoffice_local" class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">สถานะเรื่องรังวัด</label>
                                    <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="status-sva-landoffice_local"></select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-sva-landoffice_local">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-sva-landoffice_local">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <!-- <tr style="text-align: center;">
                                        <th colspan="5" class="th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th colspan="4" class="th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr> -->
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto">รายละเอียด</th>
                                        <!-- <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th> -->
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="tabPanel card" id="tabPanel-landoffice_local">

                            <ul class="nav nav-tabs nav-pills detail-pills detailTab-landoffice_local">
                                <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-landoffice_local">ตรวจสอบและติดตามสถานะเรื่องรังวัด</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-landoffice_local">ผู้ถือกรรมสิทธิ์</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#status-detailTab-landoffice_local">สถานะงานรังวัด</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#expense-detailTab-landoffice_local">ค่าใช้จ่าย</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#appointment-detailTab-landoffice_local">รายละเอียดการนัดรังวัด</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="parcel-detailTab-landoffice_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-landoffice_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="owner-detailTab-landoffice_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-landoffice_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="status-detailTab-landoffice_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="status-detailTab-landoffice_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">สถานะงานรังวัด</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="expense-detailTab-landoffice_local" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="withdraw-detailTab-landoffice_local-table">
                                                <thead>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                                    </tr>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-landoffice_local-table-p1">
                                                <thead style="background-color: rgb(18 160 210);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    </tr>

                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th>
                                                         -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-landoffice_local-table-p2">
                                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>

                                                    <tr style="text-align: center">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="appointment-detailTab-landoffice_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="appointment-detailTab-landoffice_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="overlay overlayP2 dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- งานรังวัดเอกชน -->
            <div class="tab-pane fade" id="tab-sva-private_local">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-sva-2" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-warning" type="submit" id="export-diff-sva-2" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ข้อมูลแตกต่างกัน</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-sva-2" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>

                        <div class="card" id="tab-sva-private_local-table">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-sva-private_local-count">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th colspan="4">สรุปยอดการถ่ายโอนข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่คำขอรังวัด</label>
                                    <input type="text" name="no-sva-private_local" id="no-sva-private_local" class="form-control">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">วันที่รับคำขอ</label>
                                    <input type="date" name="date-sva-private_local" id="date-sva-private_local" class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">สถานะเรื่องรังวัด</label>
                                    <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="status-sva-private_local"></select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-sva-private_local">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-sva-private_local">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <!-- <tr style="text-align: center;">
                                        <th colspan="5" class="th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th colspan="4" class="th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr> -->
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto">รายละเอียด</th>
                                        <!-- <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th> -->
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="tabPanel card" id="tabPanel-private_local">
                            <ul class="nav nav-tabs nav-pills detail-pills detailTab-private_local">

                                <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-private_local">ตรวจสอบและติดตามสถานะเรื่องรังวัด</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-private_local">ผู้ถือกรรมสิทธิ์</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#status-detailTab-private_local">สถานะงานรังวัด</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#expense-detailTab-private_local">ค่าใช้จ่าย</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#appointment-detailTab-private_local">รายละเอียดการนัดรังวัด</a></li>

                            </ul>
                            <div class="tab-content">
                                <div id="parcel-detailTab-private_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-private_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="owner-detailTab-private_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-private_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="status-detailTab-private_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="status-detailTab-private_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">สถานะงานรังวัด</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="expense-detailTab-private_local" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="withdraw-detailTab-private_local-table">
                                                <thead>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                                    </tr>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-private_local-table-p1">
                                                <thead style="background-color: rgb(18 160 210);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    </tr>

                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-private_local-table-p2">
                                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>

                                                    <tr style="text-align: center">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="appointment-detailTab-private_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="appointment-detailTab-private_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                            <div class="overlay overlayP2 dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- งานโครงการ -->
            <div class="tab-pane fade" id="tab-sva-project_local">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-sva-3" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-warning" type="submit" id="export-diff-sva-3" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ข้อมูลแตกต่างกัน</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-sva-3" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>

                        <div class="card" id="tab-sva-project_local-table">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-sva-project_local-count">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th colspan="4">สรุปยอดการถ่ายโอนข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่คำขอรังวัด</label>
                                    <input type="text" name="no-sva-project_local" id="no-sva-project_local" class="form-control">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">วันที่รับคำขอ</label>
                                    <input type="date" name="date-sva-project_local" id="date-sva-project_local" class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">สถานะเรื่องรังวัด</label>
                                    <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="status-sva-project_local"></select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-sva-project_local">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-sva-project_local">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <!-- <tr style="text-align: center;">
                                        <th colspan="5" class="th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th colspan="4" class="th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr> -->
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto">รายละเอียด</th>
                                        <!-- <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th> -->
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="tabPanel card" id="tabPanel-project_local">
                            <ul class="nav nav-tabs nav-pills detail-pills detailTab-project_local">
                                <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-project_local">ตรวจสอบและติดตามสถานะเรื่องรังวัด</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#project-detailTab-project_local">งานโครงการ</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#expense-detailTab-project_local">ค่าใช้จ่าย</a></li>
                                <!-- <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-project_local">รายการจดทะเบียน</a></li> -->
                            </ul>
                            <div class="tab-content">
                                <div id="parcel-detailTab-project_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="parcel-detailTab-project_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="owner-detailTab-project_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-project_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="project-detailTab-project_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="project-detailTab-project_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="expense-detailTab-project_local" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-project_local-table-p1">
                                                <thead style="background-color: rgb(18 160 210);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    </tr>

                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-project_local-table-p2">
                                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>

                                                    <tr style="text-align: center">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                        <!-- </tr> -->
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="transaction-detailTab-project_local" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="transaction-detailTab-project_local-table-p1">
                                                <thead style="background-color: rgb(18 160 210);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    </tr>
                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">วันที่</th>
                                                        <th class="w-auto">เลขคิว</th>
                                                        <th class="w-auto">ลำดับ</th>
                                                        <th class="w-auto">รายการจดทะเบียน</th>
                                                        <!-- <th class="w-auto status_header">รายละเอียด</th> -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="transaction-detailTab-project_local-table-p2">
                                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>
                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">วันที่</th>
                                                        <th class="w-auto">เลขคิว</th>
                                                        <th class="w-auto">ลำดับ</th>
                                                        <th class="w-auto">รายการจดทะเบียน</th>
                                                        <!-- <th class="w-auto status_header">รายละเอียด</th> -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay overlayP2 dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- งานเดินรังวัดสำรวจ -->
            <div class="tab-pane fade" id="tab-sva-survey_local">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="card" id="tab-sva-survey_local-table">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-sva-survey_local-count">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th colspan="4">สรุปยอดการถ่ายโอนข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่คำขอรังวัด</label>
                                    <input type="text" name="no-sva-survey_local" id="no-sva-survey_local" class="form-control">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">วันที่รับคำขอ</label>
                                    <input type="date" name="date-sva-survey_local" id="date-sva-survey_local" class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">สถานะเรื่องรังวัด</label>
                                    <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="status-sva-survey_local"></select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-sva-survey_local">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-sva-survey_local">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <!-- <tr style="text-align: center;">
                                        <th colspan="5" class="th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th colspan="4" class="th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr> -->
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto">รายละเอียด</th>
                                        <!-- <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th> -->
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="tabPanel card" id="tabPanel-survey_local">
                            <ul class="nav nav-tabs nav-pills detail-pills detailTab-survey_local">
                                <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-survey_local">ตรวจสอบและติดตามสถานะเรื่องรังวัด</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-survey_local">ผู้มีสิทธิ์ในที่ดิน</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#expense-detailTab-survey_local">ค่าใช้จ่าย</a></li>
                                <!-- <li><a data-toggle="tab" class="nav-link" href="#transaction-detailTab-survey_local">รายการจดทะเบียน</a></li> -->
                            </ul>
                            <div class="tab-content">
                                <div id="parcel-detailTab-survey_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="parcel-detailTab-survey_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="owner-detailTab-survey_local" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-survey_local-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="expense-detailTab-survey_local" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="withdraw-detailTab-survey_local-table">
                                                <thead>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                                    </tr>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-survey_local-table-p1">
                                                <thead style="background-color: rgb(18 160 210);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    </tr>

                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-survey_local-table-p2">
                                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>

                                                    <tr style="text-align: center">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div id="transaction-detailTab-survey_local" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="transaction-detailTab-survey_local-table-p1">
                                                <thead style="background-color: rgb(18 160 210);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4" style="border-bottom:1px solid white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    </tr>
                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">วันที่</th>
                                                        <th class="w-auto">เลขคิว</th>
                                                        <th class="w-auto">ลำดับ</th>
                                                        <th class="w-auto">รายการจดทะเบียน</th>
                                                        <!-- <th class="w-auto status_header">รายละเอียด</th> -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="transaction-detailTab-survey_local-table-p2">
                                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>
                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">วันที่</th>
                                                        <th class="w-auto">เลขคิว</th>
                                                        <th class="w-auto">ลำดับ</th>
                                                        <th class="w-auto">รายการจดทะเบียน</th>
                                                        <!-- <th class="w-auto status_header">รายละเอียด</th> -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay overlayP2 dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!------------------------------------------------------ งานรังวัดเอกชน ------------------------------------------->
            <!-- งานสำนักงานที่ดิน -->
            <div class="tab-pane fade" id="tab-sva-landoffice_calculate">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-svc-1" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-warning" type="submit" id="export-diff-svc-1" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ข้อมูลแตกต่างกัน</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-svc-1" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>

                        <div class="card" id="tab-sva-landoffice_calculate-table">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-sva-landoffice_calculate-count">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th colspan="4">สรุปยอดการถ่ายโอนข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่คำขอรังวัด</label>
                                    <input type="text" name="no-sva-landoffice_calculate" id="no-sva-landoffice_calculate" class="form-control">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">วันที่รับคำขอ</label>
                                    <input type="date" name="date-sva-landoffice_calculate" id="date-sva-landoffice_calculate" class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">สถานะเรื่องรังวัด</label>
                                    <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="status-sva-landoffice_calculate"></select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-sva-landoffice_calculate">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-sva-landoffice_calculate">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <!-- <tr style="text-align: center;">
                                        <th colspan="5" class="th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th colspan="4" class="th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr> -->
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto">รายละเอียด</th>
                                        <!-- <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th> -->
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="tabPanel card" id="tabPanel-landoffice_calculate">
                            <ul class="nav nav-tabs nav-pills detail-pills detailTab-landoffice_calculate">
                                <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-landoffice_calculate">ข้อมูลรายละเอียดแปลงที่ดิน</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-landoffice_calculate">ข้อมูลรายละเอียดงานรังวัด</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#expense-detailTab-landoffice_calculate">ค่าใช้จ่าย</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="parcel-detailTab-landoffice_calculate" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-landoffice_calculate-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>
                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="owner-detailTab-landoffice_calculate" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-landoffice_calculate-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="expense-detailTab-landoffice_calculate" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="withdraw-detailTab-landoffice_calculate-table">
                                                <thead>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                                    </tr>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-landoffice_calculate-table-p1">
                                                <thead style="background-color: rgb(18 160 210);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    </tr>

                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-landoffice_calculate-table-p2">
                                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>

                                                    <tr style="text-align: center">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay overlayP2 dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- งานรังวัดเอกชน -->
            <div class="tab-pane fade" id="tab-sva-private_calculate">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-svc-2" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-warning" type="submit" id="export-diff-svc-2" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ข้อมูลแตกต่างกัน</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-svc-2" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="card" id="tab-sva-private_calculate-table">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-sva-private_calculate-count">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th colspan="4">สรุปยอดการถ่ายโอนข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่คำขอรังวัด</label>
                                    <input type="text" name="no-sva-private_calculate" id="no-sva-private_calculate" class="form-control">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">วันที่รับคำขอ</label>
                                    <input type="date" name="date-sva-private_calculate" id="date-sva-private_calculate" class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">สถานะเรื่องรังวัด</label>
                                    <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="status-sva-private_calculate"></select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-sva-private_calculate">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-sva-private_calculate">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <!-- <tr style="text-align: center;">
                                        <th colspan="5" class="th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th colspan="4" class="th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr> -->
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto">รายละเอียด</th>
                                        <!-- <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th> -->
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="tabPanel card" id="tabPanel-private_calculate">
                            <ul class="nav nav-tabs nav-pills detail-pills detailTab-private_calculate">
                                <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-private_calculate">ข้อมูลรายละเอียดแปลงที่ดิน</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-private_calculate">ผู้มีสิทธิ์ในที่ดิน</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#expense-detailTab-private_calculate">ค่าใช้จ่าย</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="parcel-detailTab-private_calculate" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="parcel-detailTab-private_calculate-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="owner-detailTab-private_calculate" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-private_calculate-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="expense-detailTab-private_calculate" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="withdraw-detailTab-private_calculate-table">
                                                <thead>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                                    </tr>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-private_calculate-table-p1">
                                                <thead style="background-color: rgb(18 160 210);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    </tr>

                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>

                                        </div>
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-private_calculate-table-p2">
                                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>

                                                    <tr style="text-align: center">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay overlayP2 dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- งานโครงการ -->
            <div class="tab-pane fade" id="tab-sva-project_calculate">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-svc-3" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-warning" type="submit" id="export-diff-svc-3" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ข้อมูลแตกต่างกัน</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-svc-3" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="card" id="tab-sva-project_calculate-table">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-sva-project_calculate-count">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th colspan="4">สรุปยอดการถ่ายโอนข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่คำขอรังวัด</label>
                                    <input type="text" name="no-sva-project_calculate" id="no-sva-project_calculate" class="form-control">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">วันที่รับคำขอ</label>
                                    <input type="date" name="date-sva-project_calculate" id="date-sva-project_calculate" class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">สถานะเรื่องรังวัด</label>
                                    <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="status-sva-project_calculate"></select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-sva-project_calculate">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-sva-project_calculate">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <!-- <tr style="text-align: center;">
                                        <th colspan="5" class="th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th colspan="4" class="th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr> -->
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto">รายละเอียด</th>
                                        <!-- <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th> -->
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="tabPanel card" id="tabPanel-project_calculate">
                            <ul class="nav nav-tabs nav-pills detail-pills detailTab-project_calculate">
                                <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-project_calculate">ตรวจสอบและติดตามสถานะเรื่องรังวัด</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#project-detailTab-project_calculate">งานโครงการ</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#expense-detailTab-project_calculate">ค่าใช้จ่าย</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="parcel-detailTab-project_calculate" class="tab-pane fade">
                                    <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="parcel-detailTab-project_calculate-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="project-detailTab-project_calculate" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="project-detailTab-project_calculate-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="owner-detailTab-project_calculate" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-project_calculate-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="expense-detailTab-project_calculate" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="withdraw-detailTab-project_calculate-table">
                                                <thead>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                                    </tr>
                                                    <tr style="text-align: center;background-color:#e8e8e8;">
                                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                        <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-project_calculate-table-p1">
                                                <thead style="background-color: rgb(18 160 210);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    </tr>

                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-project_calculate-table-p2">
                                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>

                                                    <tr style="text-align: center">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay overlayP2 dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- งานเดินรังวัดสำรวจ -->
            <div class="tab-pane fade" id="tab-sva-survey_calculate">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="card" id="tab-sva-survey_calculate-table">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-sva-survey_calculate-count">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th colspan="4">สรุปยอดการถ่ายโอนข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่คำขอรังวัด</label>
                                    <input type="text" name="no-sva-survey_calculate" id="no-sva-survey_calculate" class="form-control">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">วันที่รับคำขอ</label>
                                    <input type="date" name="date-sva-survey_calculate" id="date-sva-survey_calculate" class="form-control">
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">สถานะเรื่องรังวัด</label>
                                    <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="status-sva-survey_calculate"></select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-sva-survey_calculate">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-sva-survey_calculate">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <!-- <tr style="text-align: center;">
                                        <th colspan="5" class="th-p1">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th colspan="4" class="th-p2">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr> -->
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th>
                                        <th class="w-auto">รายละเอียด</th>
                                        <!-- <th class="w-auto">เลขที่คำขอรังวัด</th>
                                        <th class="w-auto">วันที่รับคำขอ</th>
                                        <th class="w-auto">ประเภทการรังวัด</th>
                                        <th class="w-auto">สถานะเรื่องรังวัด</th> -->
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="tabPanel card" id="tabPanel-survey_calculate">
                            <ul class="nav nav-tabs nav-pills detail-pills detailTab-survey_calculate">
                                <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-survey_calculate">ตรวจสอบและติดตามสถานะเรื่องรังวัด</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#owner-detailTab-survey_calculate">ผู้มีสิทธิ์ในที่ดิน</a></li>
                                <li><a data-toggle="tab" class="nav-link" href="#expense-detailTab-survey_calculate">ค่าใช้จ่าย</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="parcel-detailTab-survey_calculate" class="tab-pane fade">
                                    <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="parcel-detailTab-survey_calculate-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="owner-detailTab-survey_calculate" class="tab-pane fade">
                                    <table class="table table-hover cell-border" style="width:100%;border:none;" id="owner-detailTab-survey_calculate-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="2" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div id="expense-detailTab-survey_calculate" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-survey_calculate-table-p1">
                                                <thead style="background-color: rgb(18 160 210);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                    </tr>
                                                    <tr style="text-align: center;">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col">
                                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table" id="expense-detailTab-survey_calculate-table-p2">
                                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                                    <tr style="text-align: center;">
                                                        <th colspan="4">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                    </tr>
                                                    <tr style="text-align: center">
                                                        <th class="w-auto">รายการค่าใช้จ่าย</th>
                                                        <th class="w-auto">จำนวน</th>
                                                        <th class="w-auto">หน่วย</th>
                                                        <th class="w-auto">รวม (บาท)</th>

                                                        <!-- <th class="w-auto" style="border-left:1px solid black;">รายการค่าใช้จ่าย</th> -->
                                                        <!--  -->
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="overlay overlayP2 dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ระบบปรับปรุงรูปแปลงดิจิทัล -->
            <div class="tab-pane fade" id="tab-sva-digital_map">
                <div class="row justify-content-around">
                    <div class="col">
                        <div class="form-inline d-flex justify-content-end">
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-success" type="submit" id="export-success-udm" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนสำเร็จ</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-warning" type="submit" id="export-diff-udm" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ข้อมูลแตกต่างกัน</button>
                            </div>
                            <div class="col-auto form-group">
                                <button class="btn btn-outline-danger" type="submit" id="export-error-udm" style="margin-top: 5px"><i class="fas fa-file-excel"></i> ถ่ายโอนไม่สำเร็จ</button>
                            </div>
                        </div>
                        <div class="card" id="tab-sva-digital_map-table">
                            <table class="table table-hover table-sm table-responsive-sm w-100 d-block d-sm-table table-striped" id="table-sva-digital_map-count">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th colspan="4">สรุปยอดการถ่ายโอนข้อมูล</th>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">ประเภทที่ดิน</th>
                                        <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                        <th class="w-auto">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-count dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <label for="">หมายเลขระวางแผนที่</label>
                                <div class="form-inline">

                                    <input type="text" class="form-control select2 select2-danger section-select" id='utmmap1-sva-digital_map' maxlength="4" size="4"></input>
                                    <select class="form-control select2 select2-danger section-select" id="utmmap2-sva-digital_map">
                                        <option value=""></option>
                                        <option value="1">I</option>
                                        <option value="2">II</option>
                                        <option value="3">III</option>
                                        <option value="4">IV</option>
                                    </select>
                                    <input type="text" class="form-control select2 select2-danger section-select" id='utmmap3-sva-digital_map' maxlength="4" size="4"></input>
                                    <b>-</b>
                                    <input type="text" class="form-control select2 select2-danger section-select" id='utmmap4-sva-digital_map' maxlength="3" size="3"></input>
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">มาตราส่วน</label>
                                    <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="scale-sva-digital_map"></select>
                                </div>
                            </div>

                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="">เลขที่ดิน</label>
                                    <input type="text" name="landno-sva-digital_map" id="landno-sva-digital_map" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">ประเภทที่ดิน</label>
                                    <select class="form-control select2 select2-danger section-select" style="margin-top: 8px;" id="parceltype-sva-digital_map"></select>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for=""></label>
                                    <button class="btn btn-default searchBtn" type="submit" id="search-sva-digital_map">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table class="table table-hover table-bordered table-sm table-responsive-sm w-100 d-block d-sm-table" id="table-sva-digital_map">
                                <thead style="background-color: rgb(46, 132, 39);color:white;">
                                    <tr style="text-align: center;">
                                        <th class="w-auto">ลำดับ</th>
                                        <th class="w-auto">หมายเลขระวางแผนที่</th>
                                        <th class="w-auto">แผ่นที่</th>
                                        <th class="w-auto">มาตราส่วน</th>
                                        <th class="w-auto">เลขที่ดิน</th>
                                        <th class="w-auto">ประเภทที่ดิน</th>
                                        <th class="w-auto">รายละเอียด</th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="overlay overlay-main dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                        <div class="tabPanel card" id="tabPanel-digital_map">
                            <ul class="nav nav-tabs nav-pills detail-pills detailTab-survey_calculate">
                                <li class="active"><a data-toggle="tab" class="nav-link active" href="#parcel-detailTab-digital_map">ข้อมูลปรับปรุงรูปแปลง</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="parcel-detailTab-digital_map" class="tab-pane fade">
                                    <table class="table table-hover cell-border table-striped" style="width:100%;border:none;" id="parcel-detailTab-digital_map-table">
                                        <thead>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th rowspan="3" style="text-align: center;vertical-align: middle;border: 1px solid black;border-width: 0px 1px 1px 1px;" class="w-auto">รายการข้อมูล</th>

                                            </tr>
                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th colspan="2">ข้อมูลก่อนปรับปรุงรูปแปลง</th>
                                                <th colspan="2">ข้อมูลหลังปรับปรุงรูปแปลง</th>
                                            </tr>

                                            <tr style="text-align: center;background-color:#e8e8e8;">
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                                <th class="w-auto" style="background-color: rgb(18 160 210);color:white;">ข้อมูลรับมอบจากโครงการพัฒฯ 1</th>
                                                <th class="w-auto" style="background-color: rgb(46, 132, 39);color:white;">ถ่ายโอนสำเร็จโครงการพัฒฯ 2</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div class="overlay overlayP2 dark">
                                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- page-content" -->
    </div>

    <!-- jQuery -->
    <script src="./plugins/jquery/jquery.min.js"></script>
    <!-- <script src="plugins/vendor/jquery/jquery-3.2.1.min.js"></script> -->
    <!-- jQuery UI 1.11.4 -->
    <script src="./dist/js/adminlte.js"></script>
    <script src="./plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="./plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="./js/sva.js"></script>
    <script src="./js/select.js"></script>
    <script src="./js/global.js"></script>
    <script src="./js/mas.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="./plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script>
        $(document).ready(function() {


            $(".form-row").keypress(function(event) {
                if (event.keyCode == 13) {
                    $("#search-" + type).click();
                }
            })
            $('.detail-pills a').on('click', function(e) {

                e.preventDefault()
                $('.tabPanel > .tab-content .tab-pane').css("display", "none")
                console.log(this);

                $($(this).attr("href")).show();


            })
            $('.process-pills a').on('click', function(e) {

                e.preventDefault()
                $('.tab6Panel > .tab-content .tab-pane').css("display", "none")
                console.log(this);

                $($(this).attr("href")).show();


            })
        });
    </script>
</body>

</html>